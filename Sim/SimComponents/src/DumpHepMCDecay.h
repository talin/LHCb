/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/HepMCEvent.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleID.h"
#include "LHCbAlgs/MergingTransformer.h"
#include <string>
#include <vector>

namespace LHCb {
  class IParticlePropertySvc;
}

namespace HepMC {
  class GenParticle;
  class GenVertex;
} // namespace HepMC

/** @class DumpHepMCDecay DumpHepMCDecay.h
 *
 *  Dump the decays of certain particles
 *  This class was previously called DumpMCDecay (Gen/Generators package)
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2004-02-18
 */
class DumpHepMCDecay
    : public LHCb::Algorithm::MergingConsumer<void( Gaudi::Functional::vector_of_const_<LHCb::HepMCEvents> const& )> {

  using base_t =
      LHCb::Algorithm::MergingConsumer<void( Gaudi::Functional::vector_of_const_<LHCb::HepMCEvents> const& )>;

public:
  DumpHepMCDecay( std::string const& name, ISvcLocator* isvc )
      : base_t{name, isvc, {"Addresses", {LHCb::HepMCEventLocation::Default}}} {}

  void operator()( Gaudi::Functional::vector_of_const_<LHCb::HepMCEvents> const& ) const override;

  std::string particleName( HepMC::GenParticle const& particle ) const;
  void        printDecay( HepMC::GenParticle const& particle, std::ostream& stream, unsigned int level ) const;

protected:
  Gaudi::Property<std::vector<int>> m_particles{this, "Particles", {}, "particles to be printed"};
  Gaudi::Property<std::vector<int>> m_quarks{
      this,
      "Quarks",
      {LHCb::ParticleID::bottom},
      [=]( auto&& ) {
        auto i = std::find_if( m_quarks.begin(), m_quarks.end(), []( auto const& q ) {
          return q < LHCb::ParticleID::down || LHCb::ParticleID::top < q;
        } );
        if ( i == m_quarks.end() ) throw GaudiException{" Invalid Quark ID", "DumpHepMCDecay", StatusCode::FAILURE};
      },
      "quarks to be printed"};
  Gaudi::Property<int> m_levels{this, "MaxLevels", 4, "maximal number of levels"};

private:
  ServiceHandle<LHCb::IParticlePropertySvc> m_ppSvc{this, "ParticlePropertySvc", "LHCb::ParticlePropertySvc",
                                                    "pointer to particle property service"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_not_found{this, "No specified Particles/Quarks are found!"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_pdg_not_found{
      this, "particleName(): ParticleProperty* points to NULL"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_quark_not_found{this, " Invalid Quark ID"};
};
