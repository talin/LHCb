/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DumpHepMCDecay.h"
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"
#include "Kernel/ParticleProperty.h"
#include "Kernel/STLExtensions.h"
#include "fmt/format.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DumpHepMCDecay
//
// 2004-02-18 : Ivan Belyaev
//-----------------------------------------------------------------------------
namespace {

  /*
   * @fn adjust
   * adjust the particle name to the desired format
   *  @param name name to be adjusted
   *  @return adjusted name
   */
  std::string adjust( const std::string& name ) {
    constexpr size_t s_maxSize = 12;
    std::string      tmp( name );
    auto             size = tmp.size();
    if ( s_maxSize > size ) {
      tmp += std::string( s_maxSize - size, ' ' );
    } else if ( s_maxSize < size ) {
      tmp                = std::string( tmp.begin(), tmp.begin() + s_maxSize );
      tmp[s_maxSize - 1] = '#';
    }
    return tmp;
  }
} // namespace

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DumpHepMCDecay )

void DumpHepMCDecay::operator()( Gaudi::Functional::vector_of_const_<LHCb::HepMCEvents> const& containers ) const {
  bool found = false;
  if ( !m_particles.empty() || !m_quarks.empty() ) {
    info() << " Decay dump [cut-off at " << m_levels << " levels] " << endmsg;
    for ( const auto& [n, events] : LHCb::range::enumerate( containers ) ) {
      info() << " Container '" << inputLocation( n ) << "' " << endmsg;
      for ( const auto* event : events ) {
        if ( !event ) { continue; } // CONTINUE
        const HepMC::GenEvent* evt = event->pGenEvt();
        if ( !evt ) { continue; } // CONTINUE
        for ( auto ip = evt->particles_begin(); evt->particles_end() != ip; ++ip ) {
          const HepMC::GenParticle* particle = *ip;
          if ( !particle ) { continue; } // CONTINUE
          bool print =
              ( m_particles.end() != std::find( m_particles.begin(), m_particles.end(), particle->pdg_id() ) ) ||
              std::any_of( m_quarks.begin(), m_quarks.end(),
                           // use LHCb flavour of ParticleID class !                  // NB
                           [p = LHCb::ParticleID( particle->pdg_id() )]( int q ) {
                             return p.hasQuark( LHCb::ParticleID::Quark( q ) );
                           } );
          if ( print ) {
            found = true;
            if ( info().isActive() ) {
              info() << std::endl;
              printDecay( *particle, info().stream(), 0 );
            }
          }
        }
      }
    }
    info() << endmsg;
  }
  if ( !found ) ++m_not_found;
}

//=============================================================================
// get the particle name in the string fixed form
//=============================================================================
std::string DumpHepMCDecay::particleName( HepMC::GenParticle const& particle ) const {
  const int                     pdg_id = particle.pdg_id();
  const LHCb::ParticleProperty* pp     = nullptr;
  pp                                   = m_ppSvc->find( LHCb::ParticleID( pdg_id ) );
  if ( pp ) { return adjust( pp->particle() ); }
  ++m_pdg_not_found;
  return adjust( "#UNKNOWN****" );
}

//=============================================================================
// print the decay tree of the particle
//=============================================================================
void DumpHepMCDecay::printDecay( HepMC::GenParticle const& particle, std::ostream& stream, unsigned int level ) const {
  stream << fmt::format( "{3d}", level );
  {
    const unsigned int s_maxLevel = 10;
    const std::string  pName      = particleName( particle );
    if ( level < s_maxLevel ) {
      stream << " " << std::string( level * 2, ' ' ) << "|-> " << pName
             << std::string( ( s_maxLevel - level ) * 2, ' ' );
    } else {
      stream << " " << std::string( 2 * s_maxLevel, ' ' ) << "|-> " << pName;
    }
  }
  // print the particle itself
  particle.print( stream );
  const HepMC::GenVertex* vertex = particle.end_vertex();
  if ( vertex && m_levels > int( level ) ) {
    // loop over all daughters
    for ( auto ip = vertex->particles_out_const_begin(); vertex->particles_out_const_end() != ip; ++ip ) {
      if ( *ip ) {
        printDecay( **ip, stream, level + 1 ); // RECURSION
      }
    }
  }
}
