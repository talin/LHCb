/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "Event/RawEvent.h"

#include "LHCbAlgs/MergingTransformer.h"

#include "GaudiKernel/ISvcLocator.h"

#include <map>
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : RawEventCombiner
//
// 2020-10-07 : Dorothea vom Bruch
//-----------------------------------------------------------------------------

/**
 *  Combines disparate raw events into one new location.
 *
 *  Based on RawEventSimpleCombiner
 *  If the same raw bank exists in several raw events, the merge will fail,
 *  except in the two following cases.
 *  - HltDecReports banks from different raw events are merged as long as their
 *    source ID is different.
 *  - The HltRoutingBits bank is always taken from the first raw event where it
 *    appears. Subsequent appearances are silently ignored.
 *
 *  @author Dorothea vom Bruch
 *  @date   2020-10-07
 */

template <typename T>
using VOC = Gaudi::Functional::vector_of_const_<T>;

class RawEventCombiner final
    : public LHCb::Algorithm::MergingTransformer<LHCb::RawEvent( VOC<LHCb::RawEvent*> const& )> {
public:
  /// Standard constructor
  RawEventCombiner( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode     initialize() override;
  LHCb::RawEvent operator()( VOC<LHCb::RawEvent*> const& rawEvents ) const override;

private:
  // By default skip all non-Run 3 raw banks
  std::vector<LHCb::RawBank::BankType>                  m_bankTypes;
  Gaudi::Property<std::vector<LHCb::RawBank::BankType>> m_banksToSkip{
      this,
      "BankTypesToSkip",
      {LHCb::RawBank::BankType::L0Calo,
       LHCb::RawBank::BankType::L0DU,
       LHCb::RawBank::BankType::PrsE,
       LHCb::RawBank::BankType::PrsTrig,
       LHCb::RawBank::BankType::L0PU,
       LHCb::RawBank::BankType::TT,
       LHCb::RawBank::BankType::IT,
       LHCb::RawBank::BankType::OT,
       LHCb::RawBank::BankType::TTFull,
       LHCb::RawBank::BankType::ITFull,
       LHCb::RawBank::BankType::PrsPacked,
       LHCb::RawBank::BankType::L0Muon,
       LHCb::RawBank::BankType::ITError,
       LHCb::RawBank::BankType::TTError,
       LHCb::RawBank::BankType::ITPedestal,
       LHCb::RawBank::BankType::TTPedestal,
       LHCb::RawBank::BankType::PrsPackedError,
       LHCb::RawBank::BankType::L0CaloFull,
       LHCb::RawBank::BankType::L0CaloError,
       LHCb::RawBank::BankType::L0MuonCtrlAll,
       LHCb::RawBank::BankType::L0MuonProcCand,
       LHCb::RawBank::BankType::L0MuonProcData,
       LHCb::RawBank::BankType::L0MuonRaw,
       LHCb::RawBank::BankType::L0MuonError,
       LHCb::RawBank::BankType::TTProcFull,
       LHCb::RawBank::BankType::ITProcFull,
       LHCb::RawBank::BankType::L0DUError,
       LHCb::RawBank::BankType::L0PUFull,
       LHCb::RawBank::BankType::L0PUError},
      [=]( const auto& ) {
        // set up m_bankTypes for later use
        m_bankTypes.clear();
        constexpr auto bts = LHCb::RawBank::types();
        std::copy_if( bts.begin(), bts.end(), std::back_inserter( m_bankTypes ), [&]( auto bt ) {
          return std::find( m_banksToSkip.begin(), m_banksToSkip.end(), bt ) == m_banksToSkip.end();
        } );
      },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{true}};

  mutable Gaudi::Accumulators::StatCounter<> m_dstdatasize{this, "DstData bank size (bytes)"};
  mutable Gaudi::Accumulators::StatCounter<> m_evtsize{this, "Event size (bytes)"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RawEventCombiner )

RawEventCombiner::RawEventCombiner( const std::string& name, ISvcLocator* pSvcLocator )
    : MergingTransformer( name, pSvcLocator,
                          // Inputs
                          KeyValues{"RawEventLocations", {LHCb::RawEventLocation::Default}},
                          // Output
                          KeyValue{"RawEvent", "Event/DAQ/MergedEvent"} ) {}

StatusCode RawEventCombiner::initialize() {
  return MergingTransformer::initialize().andThen( [&] {
    if ( msgLevel( MSG::INFO ) )
      info() << m_bankTypes.size() << " / " << (int)LHCb::RawBank::LastType
             << " possible RawBank types will be copied from inputs to output RawEvent " << endmsg;
  } );
}

LHCb::RawEvent RawEventCombiner::operator()( VOC<LHCb::RawEvent*> const& rawEvents ) const {

  LHCb::RawEvent outputRawEvent;
  unsigned int   eventsize   = 0;
  unsigned int   dstdatasize = 0;

  bool first_rbBank = true;
  for ( auto const [i_rawEvent, rawEvent] : LHCb::range::enumerate( rawEvents ) ) {
    for ( auto ib : m_bankTypes ) {
      const auto input_banks = rawEvent->banks( ib );
      if ( input_banks.empty() ) continue;

      // Only write the first HltRoutingBits bank
      if ( ib == LHCb::RawBank::BankType::HltRoutingBits ) {
        if ( first_rbBank ) {
          first_rbBank = false;
        } else {
          debug() << "Ignoring HltRoutingBits bank from input #" << i_rawEvent << endmsg;
          continue;
        }
      }

      // Check if ib already exists in output raw event
      // In general, exits with error if yes
      // An exception is added for HltDecReports
      const auto output_banks = outputRawEvent.banks( ib );
      if ( !output_banks.empty() ) {
        // HltDecReports: Re-adding is allowed as long as new the bank has a different sourceID
        if ( ib == LHCb::RawBank::BankType::HltDecReports ) {
          for ( const LHCb::RawBank* oBank : output_banks ) {
            for ( const LHCb::RawBank* iBank : input_banks ) {
              if ( oBank->sourceID() == iBank->sourceID() ) {
                throw GaudiException( "Raw bank type " + toString( ib ) + " with sourceID " +
                                          std::to_string( oBank->sourceID() ) + " from input #" +
                                          std::to_string( i_rawEvent ) + " already added to output raw event.",
                                      name(), StatusCode::FAILURE );
              }
            }
          }
        }
        // General case
        else {
          throw GaudiException( "Raw bank type " + toString( ib ) + " from input #" + std::to_string( i_rawEvent ) +
                                    " already added to output raw event.",
                                name(), StatusCode::FAILURE );
        }
      }

      // If it does not yet exist, add it to the outputRawEvent
      for ( const LHCb::RawBank* b : rawEvent->banks( ib ) ) {
        if ( !b ) continue;
        outputRawEvent.adoptBank( outputRawEvent.createBank( b->sourceID(), ib, b->version(), b->size(), b->data() ),
                                  true );

        if ( b->type() == LHCb::RawBank::BankType::DstData ) dstdatasize += b->size();
        eventsize += b->size();

        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << "Copied RawBank: type =" << toString( ib ) << ", version = " << b->version()
                    << ", sourceID = " << b->sourceID() << ", size (bytes) = " << b->size() << endmsg;
        }
      }
    }
  }
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Persisted event of size (bytes):" << eventsize << endmsg;

  m_evtsize += eventsize;
  m_dstdatasize += dstdatasize;

  return outputRawEvent;
}
