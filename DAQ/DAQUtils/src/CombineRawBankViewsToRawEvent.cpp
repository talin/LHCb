/*****************************************************************************\
* (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "Event/RawEvent.h"

#include "LHCbAlgs/MergingTransformer.h"

#include "GaudiKernel/ISvcLocator.h"

#include <map>
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : CombineRawBankViewsToRawEvent
//
// 2021-11-04 : Nicole Skidmore
//-----------------------------------------------------------------------------

/**
 *  Combines vector of RawBank::View and returns *new* RawEvent
 *  Note there is currently no check for if 2 RawBanks of the same type are being added to the new RawEvent. For the
 * current implementation this is justified but should be revised if usage of this extends
 *
 *  @author Nicole Skidmore
 *  @date   2021-11-04
 */

template <typename T>
using VOC = Gaudi::Functional::vector_of_const_<T>;

struct CombineRawBankViewsToRawEvent final
    : LHCb::Algorithm::MergingTransformer<LHCb::RawEvent( VOC<LHCb::RawBank::View> const& )> {

  CombineRawBankViewsToRawEvent( const std::string& name, ISvcLocator* pSvcLocator )
      : MergingTransformer( name, pSvcLocator,
                            // Inputs
                            KeyValues{"RawBankViews", {}},
                            // Output
                            KeyValue{"RawEvent", "/Event/DAQ/MergedEvent"} ) {}

  StatusCode initialize() override {
    return MergingTransformer::initialize().andThen( [&] {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << inputLocationSize() << " views being copied to  " << outputLocation() << endmsg;
    } );
  }

  LHCb::RawEvent operator()( VOC<LHCb::RawBank::View> const& views ) const override {

    LHCb::RawEvent outputRawEvent;
    unsigned int   eventsize   = 0;
    unsigned int   dstdatasize = 0;
    for ( auto const& view : views ) {
      for ( const LHCb::RawBank* b : view ) {
        if ( !b ) continue;

        // Add it to the new outputRawEvent
        outputRawEvent.addBank( b->sourceID(), b->type(), b->version(), b->range<std::byte>() );

        if ( b->type() == LHCb::RawBank::BankType::DstData ) dstdatasize += b->size();
        eventsize += b->size();

        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << "Copied RawBank: type =" << b->type() << ", version = " << b->version()
                    << ", sourceID = " << b->sourceID() << ", size (bytes) = " << b->size() << endmsg;
        }
      }
    }
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Persisted event of size (bytes):" << eventsize << endmsg;

    m_evtsize += eventsize;
    m_dstdatasize += dstdatasize;
    return outputRawEvent;
  }
  mutable Gaudi::Accumulators::StatCounter<> m_dstdatasize{this, "DstData bank size (bytes)"};
  mutable Gaudi::Accumulators::StatCounter<> m_evtsize{this, "Event size (bytes)"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CombineRawBankViewsToRawEvent )