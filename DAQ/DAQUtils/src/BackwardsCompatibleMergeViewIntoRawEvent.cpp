/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "Event/RawEvent.h"

#include "LHCbAlgs/MergingTransformer.h"

#include "GaudiKernel/ISvcLocator.h"

#include <map>
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : BackwardsCompatibleMergeViewIntoRawEvent
//
// 2021-11-04 : Nicole Skidmore
//-----------------------------------------------------------------------------

/**
 * TO BE REMOVED!
 *
 * For backwards compatibility -
 * takes vector of RawBank::View and appends (copies) the banks the views
 * point at into an existing RawEvent -- i.e. modifies a RawEvent that is already in the TES.
 * This is needed to (also) put the HltDecReports in the 'default' RawEvent, so that
   the combination of the modified HltDecReportsWriter + BackwardsCompatibleMergeViewIntoRawEvent is a
   drop-in replacement for the old HltDecReportsWriter.
 *
 *  @author Nicole Skidmore
 *  @date   2021-11-04
 */

template <typename T>
using VOC = Gaudi::Functional::vector_of_const_<T>;

struct BackwardsCompatibleMergeViewIntoRawEvent final
    : LHCb::Algorithm::MergingTransformer<void( VOC<LHCb::RawBank::View> const& )> {

  // Get EXISTING default RawEvent
  DataObjectReadHandle<LHCb::RawEvent> m_raw{this, "RawEvent", LHCb::RawEventLocation::Default};

  BackwardsCompatibleMergeViewIntoRawEvent( const std::string& name, ISvcLocator* pSvcLocator )
      : MergingTransformer( name, pSvcLocator,
                            // Specify Inputs only
                            KeyValues{"RawBankViews", {}} ) {}

  StatusCode initialize() override {
    return MergingTransformer::initialize().andThen( [&] {
      if ( msgLevel( MSG::INFO ) ) info() << inputLocationSize() << " views being copied to  " << m_raw << endmsg;
    } );
  }

  void operator()( VOC<LHCb::RawBank::View> const& views ) const override {

    // get RawEvent
    auto outputRawEvent = m_raw.getOrCreate();
    for ( auto const& view : views ) {
      for ( const LHCb::RawBank* b : view ) {
        if ( !b ) continue;

        // Add it to the outputRawEvent
        outputRawEvent->addBank( b->sourceID(), b->type(), b->version(), b->range<std::byte>() );
        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << " Copied RawBank type=" << b->type() << " version= " << b->version()
                    << " sourceID= " << b->sourceID() << " size (bytes) = " << b->size() << endmsg;
        }
      }
    }
  }
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( BackwardsCompatibleMergeViewIntoRawEvent )