/*****************************************************************************\
* (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReports.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Gaudi/Parsers/Factory.h"
#include "LHCbAlgs/MergingTransformer.h"
#include <algorithm>
#include <map>
#include <numeric>
#include <string>
#include <type_traits>

//-----------------------------------------------------------------------------
// Implementation file for class : SelectiveCombineRawBankViewsToRawEvent
// Selects and combines RawBanks into a new RawEvent
// based on the list of requested RawBanks to be persisted
// 01/2023
//-----------------------------------------------------------------------------
namespace Gaudi::Parsers {
  StatusCode parse( std::map<std::string, DataObjIDColl>& result, const std::string& s ) {
    std::map<std::string, std::vector<std::string>> t;
    return parse( t, s ).andThen( [&] {
      result.clear();
      for ( const auto& [k, v] : t ) {
        DataObjIDColl col;
        std::transform( v.begin(), v.end(), std::inserter( col, col.end() ),
                        []( const auto& i ) { return DataObjID{i}; } );
        result.emplace( k, col );
      }
    } );
  }
} // namespace Gaudi::Parsers

template <typename T>
using VOC = Gaudi::Functional::vector_of_const_<T>;

class SelectiveCombineRawBankViewsToRawEvent final
    : public LHCb::Algorithm::MergingTransformer<LHCb::RawEvent( VOC<LHCb::RawBank::View> const&,
                                                                 VOC<LHCb::RawBank::View> const& )> {

  DataObjectReadHandle<LHCb::HltDecReports>             m_decrep{this, "DecReports", ""};
  Gaudi::Property<std::map<std::string, DataObjIDColl>> m_map{this, "MapLinesRawBanks", {}};

public:
  SelectiveCombineRawBankViewsToRawEvent( const std::string& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::MergingTransformer<LHCb::RawEvent( VOC<LHCb::RawBank::View> const&,
                                                            VOC<LHCb::RawBank::View> const& )>{
            name,
            pSvcLocator,
            // Inputs
            {KeyValues{"SelectiveRawBanks", {}}, KeyValues{"RequiredRawBanks", {}}},
            // Output
            KeyValue{"RawEvent", "/Event/DAQ/MergedEvent"}} {}

  StatusCode initialize() override {
    return MergingTransformer::initialize().andThen( [&] {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << inputLocationSize() << " views being copied to  " << outputLocation() << endmsg;
    } );
  }

  LHCb::RawEvent operator()( VOC<LHCb::RawBank::View> const& selective_banks,
                             VOC<LHCb::RawBank::View> const& required_banks ) const override {

    LHCb::RawEvent rawEvent;
    unsigned int   eventsize   = 0;
    unsigned int   dstdatasize = 0;

    for ( auto const& [j, view] : LHCb::range::enumerate( required_banks ) ) {
      if ( view.empty() ) {
        ++m_emptyview;
        this->debug() << "required bank at " << inputLocation( 1, j ) << " is empty" << endmsg;
        continue;
      }
      this->debug() << " Required raw bank " << inputLocation( 1, j ) << endmsg;
      LHCb::RawBank::BankType t = view.front()->type();
      // paranoia check that all types inside a given view are the same
      if ( !std::all_of( std::next( view.begin() ), view.end(), [t]( const auto* b ) { return b->type() == t; } ) ) {
        // if inconsistent types inside a view are found an exceptions is thrown -- this should never happen
        throw GaudiException( "Inconsistent RawBank type inside a view", __PRETTY_FUNCTION__, StatusCode::FAILURE );
      }
      for ( const LHCb::RawBank* bank : view ) {
        rawEvent.addBank( bank->sourceID(), bank->type(), bank->version(), bank->range<std::byte>() );
        if ( bank->type() == LHCb::RawBank::BankType::DstData ) dstdatasize += bank->size();
        eventsize += bank->size();
      }
    }

    const auto& dec = m_decrep.get();

    // figure out the subset of containers to actually persist
    DataObjIDColl containers;
    for ( auto const& [k, rb] : m_map ) {
      if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "line " << k << endmsg;
      if ( !dec->hasDecisionName( k ) ) continue;
      if ( !dec->decReport( k ) ) continue;
      if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "got positive decision for line  " << k << endmsg;
      ;
      containers.insert( rb.begin(), rb.end() );
    }

    if ( containers.size() > 0 ) {
      for ( auto const& [j, view] : LHCb::range::enumerate( selective_banks ) ) {
        if ( auto in = containers.find( this->inputLocation( 0, j ) ); in != containers.end() ) {
          if ( view.empty() ) {
            this->debug() << " Selective raw bank at " << inputLocation( 0, j ) << " is empty" << endmsg;
            ++m_emptyview;
            continue;
          }
          this->debug() << "selective  bank at " << inputLocation( 0, j ) << endmsg;

          LHCb::RawBank::BankType t = view.front()->type();
          // paranoia check that all types inside a given view are the same
          if ( !std::all_of( std::next( view.begin() ), view.end(),
                             [t]( const auto* b ) { return b->type() == t; } ) ) {
            // if inconsistent types inside a view are found an exceptions is thrown -- this should never happen
            throw GaudiException( "Inconsistent RawBank type inside a view", __PRETTY_FUNCTION__, StatusCode::FAILURE );
          }
          for ( const LHCb::RawBank* bank : view ) {
            rawEvent.addBank( bank->sourceID(), bank->type(), bank->version(), bank->range<std::byte>() );
            if ( bank->type() == LHCb::RawBank::BankType::DstData ) dstdatasize += bank->size();
            eventsize += bank->size();
          };
        };
      };
    };

    m_evtsize += eventsize;
    m_dstdatasize += dstdatasize;
    return rawEvent;
  };
  mutable Gaudi::Accumulators::StatCounter<>         m_dstdatasize{this, "DstData bank size (bytes)"};
  mutable Gaudi::Accumulators::StatCounter<>         m_evtsize{this, "Event size (bytes)"};
  mutable Gaudi::Accumulators::StatCounter<>         m_emptyview{this, "Empty RawBank::View"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::INFO> m_absentBank{this, "Absent BankType"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SelectiveCombineRawBankViewsToRawEvent )
