###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Database of standard decoders, can be added to or manipulated in user code.

This database is normally eaten by the DAQSys configurables to organize which data will go where.
"""
from DAQSys.DecoderClass import Decoder

DecoderDB = {}

#===========ODIN===========
Decoder(
    "LHCb__UnpackRawEvent",
    properties={"BankTypes": None},
    inputs={"RawEventLocation": None},
    outputs={"RawBankLocations": None},
    active=True,
    conf=DecoderDB)

Decoder(
    "createODIN",
    active=True,
    banks=["ODIN"],
    inputs={"RawBanks": None},
    outputs={"ODIN": None},
    required=["LHCb__UnpackRawEvent"],
    publicTools=["OdinTimeDecoder/ToolSvc.OdinTimeDecoder"],
    conf=DecoderDB)

Decoder(
    "OdinTimeDecoder/ToolSvc.OdinTimeDecoder",
    active=False,
    privateTools=["ODINDecodeTool"],
    conf=DecoderDB)

Decoder(
    "ODINDecodeTool",
    active=False,  #tool handle??
    inputs={"RawEventLocations": None},
    outputs={"ODINLocation": None},
    conf=DecoderDB)
