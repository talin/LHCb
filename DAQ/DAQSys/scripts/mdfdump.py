#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import hashlib

import GaudiPython
import cppyy
import cppyy.ll
from pprint import pformat
from PRConfig.TestFileDB import test_file_db
from DDDB.CheckDD4Hep import UseDD4Hep

parser = argparse.ArgumentParser(description='Dump MDF file')
parser.add_argument('file')
parser.add_argument(
    "--db-entry",
    dest="db_entry",
    action="store_true",
    help="Interpret the file as a TestFileDB entry")
parser.add_argument(
    "-n", "--nevents", type=int, default=-1, help="Maximum number of events")
args = parser.parse_args()

from GaudiConf import IOHelper
if args.db_entry:
    IOHelper('MDF').inputFiles([test_file_db[args.file].filenames[0]])
else:
    IOHelper('MDF').inputFiles(['mdf:' + args.file])

from Configurables import LHCbApp
LHCbApp(DataType='Upgrade')
if not UseDD4Hep:
    from Configurables import CondDB
    CondDB(EnableRunStampCheck=False, IgnoreHeartBeat=True)


def events(appmgr):
    TES = appmgr.evtsvc()
    appmgr.run(1)
    while TES['/Event']:
        yield TES
        appmgr.run(1)


def _RawBank_bytes(self):
    """Return the RawBank payload as a bytes object."""
    data = bytes(self.range[cppyy.gbl.std.byte]())
    assert len(data) == self.size()
    return data


def _RawBank_padding(self):
    """Return the padding bytes in a raw bank."""
    full_data_size = self.totalSize() - self.hdrSize()  # incl. padding bytes
    data = cppyy.ll.cast['std::byte*'](self.data())
    data.reshape((full_data_size, ))
    data = bytes(data)
    return data[self.size():]


LHCb = GaudiPython.gbl.LHCb
LHCb.RawBank.bytes = _RawBank_bytes
LHCb.RawBank.padding = _RawBank_padding

RAW_BANK_TYPES = [(i, LHCb.RawBank.typeName(i))
                  for i in range(LHCb.RawBank.LastType)]


def chunk(s, n=16):
    return [s[i:i + n] for i in range(0, len(s), n)]


def rawbank_dict(rb, fulldata=False):
    data = rb.bytes()
    if fulldata:
        data_repr = list(enumerate(chunk(data)))
    else:
        # do not use hash() as it is not stable across runs
        data_repr = hashlib.blake2b(data, digest_size=4).hexdigest()

    return {
        'magic': rb.magic(),
        'size': rb.size(),
        'totalSize': rb.totalSize(),
        'type': rb.type(),
        'version': rb.version(),
        'sourceID': rb.sourceID(),
        'data': data_repr,
        'padding': rb.padding()
    }


def unzip(data):
    import ROOT
    if not hasattr(ROOT, 'unzip'):
        code = '''
#include "RZip.h"

std::vector<char> unzip(std::string data) {
  std::vector<char> buffer;
  buffer.resize(data.size() * 5); // TODO get rid of unneeded initialization
  while (true) {
    int output_size = 0;
    int input_size = data.size();
    int buffer_size = buffer.size();
    R__unzip(&input_size, (unsigned char*)&data[0],
             &buffer_size, (unsigned char*)&buffer[0], &output_size);
    if (output_size > 0) {
      buffer.resize(output_size);
      break;
    } else if (buffer.size() < 64 * data.size()) {
      // Maybe the buffer is not big enough?
      buffer.resize(2 * buffer.size());
    } else {
      // Failed to decompress data, quitting
      buffer.resize(0);
      break;
    }
  }
  return buffer;
}
'''
        ROOT.gInterpreter.Declare(code)
    return ''.join(ROOT.unzip(data))


appMgr = GaudiPython.AppMgr()
for i_event, TES in enumerate(events(appMgr)):
    if (args.nevents != -1 and i_event >= args.nevents):
        break
    raw = TES['DAQ/RawEvent']
    for bank_type, name in RAW_BANK_TYPES:
        banks = raw.banks(bank_type)
        if not banks.size():
            continue
        print("{}:{}".format(i_event, name))
        # fulldata = (name == 'DstData')
        fulldata = False
        print(''.join([
            "    " + x
            for x in pformat([rawbank_dict(rb, fulldata)
                              for rb in banks]).splitlines(True)
        ]))
        if bank_type == LHCb.RawBank.DstData:
            data = b''.join(rb.bytes() for rb in banks)
            print("    Uncompressed DstData:\n{}".format(
                pformat(list(enumerate(chunk(unzip(data)))))))
