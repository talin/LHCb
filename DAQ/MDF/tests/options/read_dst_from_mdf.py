###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Read a DST (ROOT file) which was produced from an MDF."""
import os
from Configurables import LHCbApp
from GaudiConf import IOHelper
import GaudiPython as GP
from PRConfig.TestFileDB import test_file_db

test_file_db["MiniBrunel_2018_MinBias_FTv4_MDF"].setqualifiers(
    configurable=LHCbApp())

# Read the output of the dst_from_mdf test
IOHelper("ROOT").inputFiles(
    [os.path.join(os.getenv("PREREQUISITE_0", ""), "output.dst")], clear=True)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

for i in range(1000):
    print(i)
    # Query the LHCb::RawEvent, which is likely to fail if it was corrupt in
    # the read/write cycle
    TES["/Event/DAQ/RawEvent"].banks(GP.gbl.LHCb.RawBank.DstData)
    appMgr.run(1)
