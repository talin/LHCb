/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <DetDesc/GenericConditionAccessorHolder.h>
#include <Detector/UT/ChannelID.h>
#include <Event/UTCluster.h>
#include <GaudiAlg/GaudiTool.h>
#include <Kernel/IUTReadoutTool.h>
#include <Kernel/UTDAQBoard.h>
#include <Kernel/UTDAQDefinitions.h>
#include <Kernel/UTDAQID.h>
#include <Kernel/UTIDMapping.h>
#include <Kernel/UTTell1Board.h>
#include <Kernel/UTTell1ID.h>
#include <Kernel/UTXMLUtils.h>
#include <UTDet/DeUTDetector.h>
//#include "fmt/format.h"
#include <algorithm>
#include <fmt/format.h>
#include <fstream>
#include <string>
#include <vector>
#include <yaml-cpp/yaml.h>

/**
 *  Concrete class for things related to the Readout of the UT Tell1 Boards
 */

class UTReadoutTool : public extends<LHCb::DetDesc::ConditionAccessorHolder<GaudiTool>, IUTReadoutTool> {

public:
  /// Constructer
  UTReadoutTool( const std::string& type, const std::string& name, const IInterface* parent );

  std::string getReadoutInfoKey() const override { return m_cache.key(); }

  /// nBoard
  unsigned int nBoard( const ReadoutInfo* roInfo ) const override;

  /// return vector of Tell1IDs
  std::vector<UTTell1ID> boardIDs( const ReadoutInfo* roInfo ) const override;

  // convert offline Detector::UT::ChannelID to readout UTDAQID
  UTDAQID channelIDToDAQID( const LHCb::Detector::UT::ChannelID offlineChan, const ReadoutInfo* roInfo ) const override;

  // convert readout UTDAQID to offline Detector::UT::ChannelID
  LHCb::Detector::UT::ChannelID daqIDToChannelID( const UTDAQID daqid, const ReadoutInfo* roInfo ) const override;

  /// convert ITChannelID to DAQ ChannelID
  UTDAQ::chanPair offlineChanToDAQ( const LHCb::Detector::UT::ChannelID aOfflineChan, double isf,
                                    const ReadoutInfo* roInfo ) const override;

  /// convert offline interStripFraction to DAQ interStripFraction
  double interStripToDAQ( const LHCb::Detector::UT::ChannelID aOfflineChan, const UTTell1ID aBoardID, const double isf,
                          const ReadoutInfo* roInfo ) const override;

  bool ADCOfflineToDAQ( const LHCb::Detector::UT::ChannelID aOfflineChan, const UTTell1ID aBoardID,
                        LHCb::UTCluster::ADCVector& adcs, const ReadoutInfo* roInfo ) const override;

  /// find the Tell1 board given a board ID
  const UTTell1Board* findByBoardID( const UTTell1ID aBoardID, const ReadoutInfo* roInfo ) const override;

  /// find Tell1 board by storage order
  const UTTell1Board* findByOrder( const unsigned int aValue, const ReadoutInfo* roInfo ) const override;

  /// find DAQ board by storage order
  UTDAQ::Board* findByDAQOrder( const unsigned int aValue, const ReadoutInfo* roInfo ) const override;

  boost::container::static_vector<LHCb::Detector::UT::ChannelID, 6>
  findBySourceID( const unsigned int aValue, const ReadoutInfo* roInfo ) const override;

  /// Add the mapping of source ID to TELL1 board number
  unsigned int SourceIDToTELLNumber( unsigned int sourceID, const ReadoutInfo* roInfo ) const override;

  /// list of the readout sector ids on the board
  std::vector<LHCb::Detector::UT::ChannelID> sectorIDs( const UTTell1ID    board,
                                                        const ReadoutInfo* roInfo ) const override;

  /// apply function to all sectors on board
  void applyToAllBoardSectors( const UTTell1ID board, const std::function<void( DeUTSector const& )>& func,
                               const ReadoutInfo* roInfo ) const override {
    const auto& cache = getCache( roInfo );
    cache.det().applyToSectors( sectorIDs( board, &cache ), func );
  }

  /// service box
  unsigned int nServiceBox( const ReadoutInfo* roInfo ) const override;

  /// service box number
  std::string serviceBox( const LHCb::Detector::UT::ChannelID& aChan, const ReadoutInfo* roInfo ) const override;

  /// list of the readout sectors ids in a service box
  std::vector<LHCb::Detector::UT::ChannelID> sectorIDsOnServiceBox( const std::string& serviceBox,
                                                                    const ReadoutInfo* roInfo ) const override;

  /// list of service boxes
  const std::vector<std::string>& serviceBoxes( const ReadoutInfo* roInfo ) const override;

  /// Add the mapping of TELL1 board number to source ID
  unsigned int TELLNumberToSourceID( unsigned int TELL, const ReadoutInfo* roInfo ) const override;

  std::vector<unsigned int> allTELLNumbers( const ReadoutInfo* roInfo ) const override;

  /// print mapping
  void printMapping( const ReadoutInfo* roInfo ) const override;

  /// init
  StatusCode initialize() override;

  bool getstripflip( const ReadoutInfo* roInfo ) const override;

private:
  std::string footer() const;
  std::string header( const std::string& conString ) const;
  std::string strip( const std::string& conString ) const;

  Gaudi::Property<bool> m_printMapping{this, "printMapping", false};

  ConditionAccessor<YAML::Node> m_readoutMap{this, "conditionLocation",
#ifdef USE_DD4HEP
                                             "/world/BeforeMagnetRegion/UT:ReadoutMap"
#else
                                             "/dd/Conditions/ReadoutConf/UT/ReadoutMap"
#endif
  };

  Gaudi::Property<std::string>  m_footer{this, "footer", "</DDDB>"};
  Gaudi::Property<std::string>  m_startTag{this, "startTag", "<condition"};
  Gaudi::Property<std::string>  m_author{this, "author", "Joe Bloggs"};
  Gaudi::Property<std::string>  m_tag{this, "tag", "None"};
  Gaudi::Property<std::string>  m_desc{this, "description", "BlahBlahBlah"};
  Gaudi::Property<bool>         m_removeCondb{this, "removeCondb", false};
  Gaudi::Property<unsigned int> m_precision{this, "precision", 16u};
  Gaudi::Property<unsigned int> m_depth{this, "depths", 3u};

private:
  using Cache = ReadoutInfo;
  /// Create a ReadoutInfo instance for the current conditions
  ReadoutInfo                    makeCache( const YAML::Node& readoutMap, const DeUTDetector& det ) const;
  ConditionAccessor<ReadoutInfo> m_cache{this, fmt::format( "{}-ReadoutInfo", name() )};

  void createBoards( const YAML::Node& readoutMap, Cache& cache ) const;
  void createTell1Map( const YAML::Node& readoutMap, Cache& cache ) const;

  void validate( const Cache& roInfo ) const;

  const Cache& getCache( const ReadoutInfo* roInfo ) const { return roInfo ? *roInfo : m_cache.get(); }
};

using namespace LHCb;
// using namespace LHCb::DAQUT;

DECLARE_COMPONENT( UTReadoutTool )

UTReadoutTool::UTReadoutTool( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {
  // constructor
}

UTReadoutTool::ReadoutInfo UTReadoutTool::makeCache( const YAML::Node& readoutMap, const DeUTDetector& det ) const {
  Cache cache( det );
  createTell1Map( readoutMap, cache );
  createBoards( readoutMap, cache );
  validate( cache );
  if ( m_printMapping ) printMapping( &cache );
  return cache;
}

StatusCode UTReadoutTool::initialize() {
  return base_class::initialize().andThen( [&]() {
    addConditionDerivation(
        {m_readoutMap.key(), DeUTDetLocation::location()}, m_cache.key(),
        [&]( const YAML::Node& readoutMap, const DeUTDetector& det ) { return makeCache( readoutMap, det ); } );
  } );
}

unsigned int UTReadoutTool::nBoard( const ReadoutInfo* roInfo ) const {
  // number of boards
  return getCache( roInfo ).nBoards;
}

unsigned int UTReadoutTool::nServiceBox( const ReadoutInfo* roInfo ) const {
  return getCache( roInfo ).serviceBoxes.size();
}

std::string UTReadoutTool::serviceBox( const LHCb::Detector::UT::ChannelID& aChan, const ReadoutInfo* roInfo ) const {

  const auto& cache = getCache( roInfo );
  // find the board

  static const std::string InValidBox = "Unknown";
  bool                     isFound    = false;
  unsigned int             waferIndex = 999u;
  unsigned int             iBoard     = cache.firstBoardInRegion[0];
  while ( ( iBoard != cache.nBoards ) && !isFound ) {
    if ( cache.boards[iBoard]->isInside( aChan, waferIndex ) ) {
      isFound = true;
    } else {
      ++iBoard;
    }
  } // iBoard
  return ( isFound ? cache.boards[iBoard]->serviceBoxes()[waferIndex] : InValidBox );
}

std::vector<UTTell1ID> UTReadoutTool::boardIDs( const ReadoutInfo* roInfo ) const {
  const auto& cache = getCache( roInfo );

  std::vector<UTTell1ID> ids;
  ids.reserve( cache.boards.size() );
  std::transform( cache.boards.begin(), cache.boards.end(), std::back_inserter( ids ),
                  []( const auto& b ) { return b->boardID(); } );
  return ids;
}

UTDAQID UTReadoutTool::channelIDToDAQID( const Detector::UT::ChannelID aOfflineChan, const ReadoutInfo* roInfo ) const {

  // need to rebuild the ChannelID without the strip number.
  // unsigned int secnum(aOfflineChan.uniqueSector());

  // info() << "Input channel ID=", aOfflineChan

  Detector::UT::ChannelID secID( aOfflineChan.channelID() - aOfflineChan.strip() );

  // while channelID is offset by 1
  unsigned int tempchan = aOfflineChan.strip();

  auto channelNum = static_cast<UTDAQID::ChannelID>( tempchan );

  auto board = getCache( roInfo ).sectorToBoardMap.at( secID );

  // need to search for the sector in the boards list: but if there are *two* matches the sector is split in half
  size_t lfound   = 0;
  size_t nmatches = 0;

  for ( size_t si = 0; si < board->sectorIDs().size(); ++si ) {

    if ( board->sectorIDs()[si] == secID ) {
      nmatches++;
      lfound = si;
    }
  }

  // lfound will be set to the higher matching lane number

  return UTDAQID( board->boardID().board(),
                  static_cast<UTDAQID::LaneID>( nmatches > 1 && tempchan < 256 ? lfound - 1 : lfound ), channelNum );
}

Detector::UT::ChannelID UTReadoutTool::daqIDToChannelID( const UTDAQID daqid, const ReadoutInfo* roInfo ) const {
  const auto& cache = getCache( roInfo );

  UTDAQID laneOnly( daqid.board(), daqid.lane() );

  // while channelID is offset by 1
  unsigned int strip = static_cast<unsigned int>( daqid.channel() );

  Detector::UT::ChannelID secID = cache.daqToSectorMap.at( laneOnly );

  return Detector::UT::ChannelID( secID.channelID() + strip );
}

UTDAQ::chanPair UTReadoutTool::offlineChanToDAQ( const Detector::UT::ChannelID aOfflineChan, double isf,
                                                 const ReadoutInfo* roInfo ) const {
  const auto& cache = getCache( roInfo );

  // look up region start.....
  unsigned int iBoard     = cache.firstBoardInRegion[0];
  unsigned int waferIndex = 999u;

  bool isFound = false;
  while ( ( iBoard != cache.nBoards ) && !isFound ) {
    if ( cache.boards[iBoard]->isInside( aOfflineChan, waferIndex ) ) {
      isFound = true;
    } else {
      ++iBoard;
    }
  } // iBoard

  if ( !isFound ) {
    return {UTTell1ID( UTTell1ID::nullBoard, false ), 0};
  } else {
    return {cache.boards[iBoard]->boardID(), cache.boards[iBoard]->offlineToDAQ( aOfflineChan, waferIndex, isf )};
  }
}

double UTReadoutTool::interStripToDAQ( const Detector::UT::ChannelID aOfflineChan, const UTTell1ID aBoardID,
                                       const double isf, const ReadoutInfo* roInfo ) const {
  unsigned int waferIndex = 999u;

  auto   aBoard = findByBoardID( aBoardID, roInfo );
  double newisf = 0;

  if ( aBoard->isInside( aOfflineChan, waferIndex ) ) {
    unsigned int orientation = aBoard->orientation()[waferIndex];
    if ( orientation == 0 && isf > 0.01 ) {
      newisf = 1 - isf;
    } else {
      newisf = isf;
    }
  } else { // Can not find board!
    newisf = -1;
  }

  return newisf;
}

bool UTReadoutTool::ADCOfflineToDAQ( const Detector::UT::ChannelID aOfflineChan, const UTTell1ID aBoardID,
                                     UTCluster::ADCVector& adcs, const ReadoutInfo* roInfo ) const {
  unsigned int waferIndex = 999u;
  auto         aBoard     = findByBoardID( aBoardID, roInfo );

  if ( !aBoard->isInside( aOfflineChan, waferIndex ) ) return false; // can not find board!

  if ( aBoard->orientation()[waferIndex] == 0 ) { std::reverse( std::begin( adcs ), std::end( adcs ) ); }
  return true;
}

const UTTell1Board* UTReadoutTool::findByBoardID( const UTTell1ID aBoardID, const ReadoutInfo* roInfo ) const {
  const auto& cache = getCache( roInfo );
  // find by board id
  auto i = cache.boardsMap.find( aBoardID );
  return i != cache.boardsMap.end() ? i->second : nullptr;
}

const UTTell1Board* UTReadoutTool::findByOrder( const unsigned int aValue, const ReadoutInfo* roInfo ) const {
  const auto& cache = getCache( roInfo );
  // find by order
  return aValue < cache.nBoards ? cache.boards[aValue].get() : nullptr;
}

UTDAQ::Board* UTReadoutTool::findByDAQOrder( const unsigned int aValue, const ReadoutInfo* roInfo ) const {
  const auto& cache = getCache( roInfo );
  // find by order
  return aValue < cache.nBoards ? cache.daqBoards[aValue].get() : nullptr;
}

boost::container::static_vector<LHCb::Detector::UT::ChannelID, 6>
UTReadoutTool::findBySourceID( const unsigned int aValue, const ReadoutInfo* roInfo ) const {
  const auto& cache = getCache( roInfo );

  boost::container::static_vector<LHCb::Detector::UT::ChannelID, 6> mBoardMap;
  if ( cache.nBoards == 216 && aValue < cache.nBoards )
    mBoardMap = ( cache.daqBoards[aValue].get() )->UTDAQ::Board::sectorIDs(); // for new ReadoutMap
  else if ( cache.nBoards != 216 && aValue < cache.nBoards )
    mBoardMap = ( cache.boards[aValue].get() )->sectorIDs(); // for old ReadoutMap
  return mBoardMap;
}

void UTReadoutTool::printMapping( const ReadoutInfo* roInfo ) const {
  const auto& cache = getCache( roInfo );
  // dump out the readout mapping
  info() << "print mapping for: " << name() << " tool" << endmsg;
  info() << " Number of boards " << cache.nBoards << endmsg;
  for ( const auto& b : cache.boards ) info() << *b << endmsg;
}

/// Add the mapping of source ID to TELL1 board number
unsigned int UTReadoutTool::SourceIDToTELLNumber( unsigned int sourceID, const ReadoutInfo* roInfo ) const {
  return getCache( roInfo ).boardMap[UT::Utils::SourceId{sourceID}].value();
}

/// Add the mapping of TELL1 board number to source ID
unsigned int UTReadoutTool::TELLNumberToSourceID( unsigned int TELL, const ReadoutInfo* roInfo ) const {
  const auto& cache = getCache( roInfo );
  return ( cache.nBoards == 216 ) ? TELL - 1
                                  : static_cast<unsigned int>( cache.boardMap[UT::Utils::BoardNum{TELL}].value() );
}

std::vector<unsigned int> UTReadoutTool::allTELLNumbers( const ReadoutInfo* roInfo ) const {
  const auto                mapping = getCache( roInfo ).boardMap.getAll<UT::Utils::BoardNum>();
  std::vector<unsigned int> numbers;
  numbers.reserve( mapping.size() );
  std::transform( begin( mapping ), end( mapping ), std::back_inserter( numbers ), []( auto k ) { return k; } );
  return numbers;
}

void UTReadoutTool::validate( const Cache& roInfo ) const {
  // validate the map - every sector must go somewhere !
  if ( roInfo.det().none_of_sectors( [this, &roInfo]( auto const& s ) {
         Detector::UT::ChannelID chan     = s.elementID();
         auto                    chanPair = offlineChanToDAQ( chan, 0.0, &roInfo );
         return chanPair.first == UTTell1ID( UTTell1ID::nullBoard, false );
       } ) ) {
    return;
  }
  throw GaudiException( "failed to create IUTReadoutTool::ReadoutInfo", name(), StatusCode::FAILURE );
}

std::vector<LHCb::Detector::UT::ChannelID> UTReadoutTool::sectorIDs( const UTTell1ID    board,
                                                                     const ReadoutInfo* roInfo ) const {

  std::vector<LHCb::Detector::UT::ChannelID> sectors;
  sectors.reserve( 8 );
  auto theBoard = findByBoardID( board, roInfo );
  if ( theBoard ) {
    sectors.insert( sectors.begin(), theBoard->sectorIDs().begin(), theBoard->sectorIDs().end() );
  } else {
    Error( "Failed to find Board", StatusCode::SUCCESS, 100 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  return sectors;
}

std::vector<LHCb::Detector::UT::ChannelID> UTReadoutTool::sectorIDsOnServiceBox( const std::string& serviceBox,
                                                                                 const ReadoutInfo* roInfo ) const {
  // loop over all boards
  std::vector<LHCb::Detector::UT::ChannelID> sectors;
  sectors.reserve( 16 );
  for ( const auto& board : getCache( roInfo ).boards ) {
    const auto& sectorVec = board->sectorIDs();
    const auto& sBoxes    = board->serviceBoxes();
    for ( unsigned int iS = 0u; iS < board->nSectors(); ++iS ) {
      if ( sBoxes[iS] == serviceBox ) sectors.push_back( sectorVec[iS] );
    } // iS
  }   // iterB
  return sectors;
}

const std::vector<std::string>& UTReadoutTool::serviceBoxes( const ReadoutInfo* roInfo ) const {
  return getCache( roInfo ).serviceBoxes;
}

std::string UTReadoutTool::footer() const {
  std::string temp = m_footer;
  temp.insert( 0, "</catalog>" );
  return temp;
}

std::string UTReadoutTool::header( const std::string& conString ) const {
  // get the header
  auto startpos = conString.find( m_startTag );
  auto temp     = conString.substr( 0, startpos );
  temp.insert( startpos, "<catalog name=\"ReadoutSectors\">" );

  // correct the location of the DTD
  if ( m_removeCondb ) {
    UT::XMLUtils::replace( temp, "conddb:", "" );
    std::string location;
    for ( unsigned int i = 0; i < m_depth; ++i ) location += "../";
    auto pos = temp.find( "/DTD/" );
    temp.insert( pos, location );
    UT::XMLUtils::replace( temp, "//", "/" );
  }

  return temp;
}

std::string UTReadoutTool::strip( const std::string& conString ) const {
  auto startpos = conString.find( m_startTag );
  auto endpos   = conString.find( m_footer );
  return conString.substr( startpos, endpos - startpos );
}

void UTReadoutTool::createTell1Map( const YAML::Node& readoutMap, Cache& cache ) const {
  cache.boardMap.clear();
  if ( readoutMap["nTell40InUT"].IsDefined() ) {
    const auto ntell = readoutMap["nTell40InUT"].as<unsigned int>();
    for ( unsigned int i = 0; i < 2 * ntell; ++i ) { cache.boardMap.add<UT::Utils::SourceId>( i, i ); }
  } else {
    const auto layers = readoutMap["layers"].as<std::vector<std::string>>();

    unsigned int sourceIDBase = 0;
    for ( const auto& l : layers ) {
      std::string tell1Loc = l + "TELL1";
      if ( readoutMap[tell1Loc].IsDefined() ) {
        //      printf("Extracting TELL1 map from %s\n", tell1Loc.c_str());
        const auto tell1 = readoutMap[tell1Loc].as<std::vector<unsigned int>>();
        for ( unsigned int i = 0; i < tell1.size(); ++i ) {
          cache.boardMap.add<UT::Utils::SourceId>( sourceIDBase + i, tell1.at( i ) );
        }
      }
      sourceIDBase += 64;
    }
  }
}

void UTReadoutTool::createBoards( const YAML::Node& readoutMap, Cache& cache ) const {
  if ( readoutMap["nTell40InUT"].IsDefined() ) {
    // New style conditions map
    const auto ntell = readoutMap["nTell40InUT"].as<unsigned int>();
    cache.nBoards    = ntell * 2;

    // fill the new style map in this tool
    for ( unsigned int i = 0; i < cache.nBoards; ++i ) {

      const auto sectorlist = readoutMap[fmt::format( "UTTell{}Map", i )].as<std::vector<int>>();
      assert( !sectorlist.empty() );

      auto daqBoard = std::make_unique<UTDAQ::Board>( static_cast<UTDAQID::BoardID>( i ) );
      cache.daqBoards.push_back( std::move( daqBoard ) );

      for ( auto& s : sectorlist ) {
        unsigned int s_id = (unsigned int)s;
        if ( cache.det().version() != DeUTDetector::GeoVersion::v1 ) { // old version of UT
                                                                       // geometry and old
                                                                       // channelID
          s_id = LHCb::UTIDMapping::ReconvertID( s_id );
        }
        Detector::UT::ChannelID sectorID( s_id );
        UTDAQID                 thisDAQID = cache.daqBoards.back()->addSector( sectorID, 1 );
        cache.sectorToBoardMap[sectorID]  = cache.daqBoards.back().get();
        cache.daqToSectorMap[thisDAQID]   = sectorID;
      }
    }

    // this is the old style map, which we keep for now to put off breaking things
    const auto   nBoards = readoutMap["nBoardsPerQuarter"].as<std::vector<int>>();
    unsigned int i       = 0;
    for ( unsigned int iReg = 0; iReg < nBoards.size(); ++iReg ) {

      cache.firstBoardInRegion.push_back( cache.boards.size() );

      for ( int j = 0; j < nBoards[iReg]; ++j ) {
        const auto& sectorlist = readoutMap[fmt::format( "UTTell{}Map", i )].as<std::vector<int>>();
        assert( !sectorlist.empty() );

        const UTTell1ID anID( iReg, j, true );
        auto            aBoard = std::make_unique<UTTell1Board>( anID, 512 );
        // info() << "Make board for " << iReg << ", " << j << ", " << i << " = " << anID << ". sectors:" << endmsg;

        for ( auto& s : sectorlist ) {
          unsigned int s_id = (unsigned int)s;
          if ( cache.det().version() != DeUTDetector::GeoVersion::v1 ) {
            // old version of UT geometry and old channelID
            s_id = LHCb::UTIDMapping::ReconvertID( s_id );
          }
          Detector::UT::ChannelID sectorID( s_id );
          // info() << "  " << s <<" "<<sectorID.station()<<" ; "<<sectorID.layer()<<" ; "<<sectorID.detRegion()<<" ;
          // "<<sectorID.sector()<< endmsg;
          aBoard->addSector( sectorID, 1, "CB3" );
        }

        cache.boards.push_back( std::move( aBoard ) );

        if ( cache.boardsMap.find( anID ) == cache.boardsMap.end() ) {
          cache.boardsMap[anID] = cache.boards.back().get();
        }

        ++i;
      }
    }

    cache.serviceBoxes.emplace_back( "CB3" );
  } else {
    // old style conditions map

    const auto layers  = readoutMap["layers"].as<std::vector<std::string>>();
    const auto nBoards = readoutMap["nBoardsPerLayer"].as<std::vector<int>>();
    cache.stripflip    = false;

    cache.hybridsPerBoard       = readoutMap["hybridsPerBoard"].as<int>();
    const auto nStripsPerHybrid = UTDAQ::nStripsPerBoard / cache.hybridsPerBoard;

    for ( unsigned int iReg = 0; iReg < layers.size(); ++iReg ) {
      assert( iReg < layers.size() );
      assert( iReg < nBoards.size() );

      cache.firstBoardInRegion.push_back( cache.boards.size() );
      cache.nBoards += nBoards[iReg];

      const auto& tMap         = readoutMap[layers[iReg]].as<std::vector<int>>();
      const auto& orientation  = readoutMap[layers[iReg] + "HybridOrientation"].as<std::vector<int>>();
      const auto& serviceBoxes = readoutMap[layers[iReg] + "ServiceBox"].as<std::vector<std::string>>();

      unsigned int vecLoc = 0;
      assert( !tMap.empty() );

      for ( unsigned int iBoard = 0; iBoard < (unsigned int)nBoards[iReg]; ++iBoard ) {

        // make new board for old style map
        const UTTell1ID anID( iReg, iBoard, true );
        auto            aBoard = std::make_unique<UTTell1Board>( anID, nStripsPerHybrid );

        // make new board for new style map
        auto daqBoard = std::make_unique<UTDAQ::Board>( static_cast<UTDAQID::BoardID>( anID.id() ) );
        cache.daqBoards.push_back( std::move( daqBoard ) );

        for ( unsigned iH = 0; iH < cache.hybridsPerBoard; ++iH, ++vecLoc ) {
          assert( vecLoc < tMap.size() );
          assert( vecLoc < orientation.size() );
          assert( vecLoc < serviceBoxes.size() );
          if ( 0 != tMap[vecLoc] ) { // skip strange 0's in conditions vector !!

            // old style
            unsigned int s_id = (unsigned int)tMap[vecLoc];
            if ( cache.det().version() != DeUTDetector::GeoVersion::v1 ) {
              // old version of UT geometry and old  channelID
              s_id = LHCb::UTIDMapping::ReconvertID( s_id );
            }
            Detector::UT::ChannelID sectorID( s_id );
            aBoard->addSector( sectorID, (unsigned int)orientation[vecLoc], serviceBoxes[vecLoc] );

            // new style
            UTDAQID thisDAQID                = cache.daqBoards.back()->addSector( sectorID, 1 );
            cache.sectorToBoardMap[sectorID] = cache.daqBoards.back().get();
            cache.daqToSectorMap[thisDAQID]  = sectorID;

            // add to the list of service boxs if not already there
            if ( std::find( cache.serviceBoxes.begin(), cache.serviceBoxes.end(), serviceBoxes[vecLoc] ) ==
                 cache.serviceBoxes.end() ) {
              cache.serviceBoxes.push_back( serviceBoxes[vecLoc] );
            }
          }
        } // iH

        cache.boards.push_back( std::move( aBoard ) );

        if ( cache.boardsMap.find( anID ) == cache.boardsMap.end() ) {
          cache.boardsMap[anID] = cache.boards.back().get();
        }

      } // boards per region
    }   // regions
  }
}

bool UTReadoutTool::getstripflip( const ReadoutInfo* roInfo ) const { return getCache( roInfo ).stripflip; }
