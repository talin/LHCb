/******************************************************************************\
 * (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
#pragma once
#include <array>
#include <cassert>
#include <iostream>
#include <sstream>
#include <string>

/** @class UTHeaderWord UTHeaderWord.h "UTDAQ/UTHeaderWord.h"
 *
 *  Class for encapsulating header word in RAW data format
 *  for the UT Si detectors.
 *
 *  @author Xuhao Yuan ( based on codes by M.Needham)
 *  @date   2020-06-17
 */

class UTHeaderWord final {
private:
  enum class mask : unsigned int {
    EventIDFromTFC = 0xFF000000,
    paddedBank     = 0x00FF0000,
    lane5          = 0x0000FF00,
    lane4          = 0x000000FF,
    lane3          = 0xFF000000,
    lane2          = 0x00FF0000,
    lane1          = 0x0000FF00,
    lane0          = 0x000000FF
  };

  template <mask m>
  [[nodiscard]] static constexpr unsigned int extract( unsigned int i ) {
    constexpr auto b =
        __builtin_ctz( static_cast<unsigned int>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
    return ( i & static_cast<unsigned int>( m ) ) >> b;
  }

  template <mask m>
  [[nodiscard]] static constexpr unsigned int shift( unsigned int i ) {
    constexpr auto b =
        __builtin_ctz( static_cast<unsigned int>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
    auto v = ( i << static_cast<unsigned int>( b ) );
    assert( extract<m>( v ) == i );
    return v;
  }

public:
  /** constructer
    @param value
    */
  constexpr UTHeaderWord() = default;
  constexpr explicit UTHeaderWord( unsigned int value ) : m_value( value ) {}
  constexpr UTHeaderWord( std::array<unsigned int, 6> nHitsInLanes, unsigned int n_lane ) {
    switch ( n_lane ) {
    case 1:
      m_value = ( shift<mask::EventIDFromTFC>( EventIDFromTFC ) | shift<mask::paddedBank>( paddedValue ) |
                  shift<mask::lane5>( nHitsInLanes[5] ) | shift<mask::lane4>( nHitsInLanes[4] ) );
      break;
    case 2:
      m_value = ( shift<mask::lane3>( nHitsInLanes[3] ) | shift<mask::lane2>( nHitsInLanes[2] ) |
                  shift<mask::lane1>( nHitsInLanes[1] ) | shift<mask::lane0>( nHitsInLanes[0] ) );
      break;
    default:
      throw std::domain_error( "invalid UT lane" );
    }
  }

  /** The actual value
    @return value
    */
  [[nodiscard]] constexpr unsigned int value() const { return m_value; }

  [[nodiscard]] constexpr unsigned int nClustersLane0() const { return extract<mask::lane0>( m_value ); };
  [[nodiscard]] constexpr unsigned int nClustersLane1() const { return extract<mask::lane1>( m_value ); };
  [[nodiscard]] constexpr unsigned int nClustersLane2() const { return extract<mask::lane2>( m_value ); };
  [[nodiscard]] constexpr unsigned int nClustersLane3() const { return extract<mask::lane3>( m_value ); };
  [[nodiscard]] constexpr unsigned int nClustersLane4() const { return extract<mask::lane4>( m_value ); };
  [[nodiscard]] constexpr unsigned int nClustersLane5() const { return extract<mask::lane5>( m_value ); };

  [[nodiscard]] constexpr unsigned int getEventIDFromTFC() const { return extract<mask::EventIDFromTFC>( m_value ); }

  /** Operator overloading for stringoutput */
  friend std::ostream& operator<<( std::ostream& s, UTHeaderWord obj ) { return s << obj.value() << "\n"; }

private:
  static constexpr unsigned int EventIDFromTFC = 255;
  static constexpr unsigned int paddedValue    = 0;
  unsigned int                  m_value        = 0;
};
