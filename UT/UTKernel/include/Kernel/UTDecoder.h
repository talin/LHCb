/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/RawBank.h"
#include "Kernel/STLExtensions.h"
#include "SiDAQ/SiRawBankDecoder.h"
#include "SiDAQ/SiRawBufferWord.h"
#include "UTClusterWord.h"
#include "UTDAQ/UTADCWord.h"
#include "UTDAQ/UTHeaderWord.h"
#include "UTDAQDefinitions.h"
#include <cassert>

namespace UTDAQ {
  /// max Num of Lane in one Board/RawBank
  constexpr unsigned int max_nlane{6};

  typedef std::array<unsigned int, UTDAQ::max_nlane> digiVec;
} // namespace UTDAQ

template <UTDAQ::version>
class UTDecoder;

template <>
class UTDecoder<UTDAQ::version::v4> : public SiRawBankDecoder<UTClusterWord> {
public:
  explicit UTDecoder( const LHCb::RawBank& bank ) : SiRawBankDecoder<UTClusterWord>{bank.data()} {
    assert( UTDAQ::version{bank.version()} == UTDAQ::version::v4 );
  }
};

template <>
class UTDecoder<UTDAQ::version::v5> final {
  enum class banksize { left_bit = 10, center_bit = 16, right_bit = 15 };

  constexpr static unsigned int nskip( unsigned int digLane ) {
    return static_cast<unsigned int>( banksize::center_bit ) * ( ( digLane + 1 ) / 2 - 1 ) + ( 1 - ( digLane % 2 ) );
  }

  constexpr static UTDAQ::digiVec make_digiVec( LHCb::span<const uint32_t, 2> header ) {
    UTHeaderWord headerL{header[0]}, headerR{header[1]};
    return {headerR.nClustersLane0(), headerR.nClustersLane1(), headerR.nClustersLane2(),
            headerR.nClustersLane3(), headerL.nClustersLane4(), headerL.nClustersLane5()};
  }

public:
  explicit UTDecoder( const LHCb::RawBank& bank )
      : m_bank{bank.range<uint16_t>().subspan( 4 )}, m_nDigits{make_digiVec( bank.range<uint32_t>().first<2>() )} {
    assert( UTDAQ::version{bank.version()} == UTDAQ::version::v5 );
    if ( bank.size() / sizeof( unsigned int ) != ( ( nClusters() + 1 ) / 2 ) * 8 ) {
      // TODO: add a dedicated UT DAQ StatusCode category, and throw a GaudiException with a value inside that category
      throw std::runtime_error{"Error: unexpected UT RawBank size"};
    }
  }

  class pos_range final {

  private:
    const LHCb::span<const uint16_t> m_bank;
    UTDAQ::digiVec                   m_Digit;
    struct Sentinel                  final {};

    class Iterator final {
      const LHCb::span<const uint16_t> m_bank;
      UTDAQ::digiVec                   m_iterDigit;
      unsigned int                     m_lane   = UTDAQ::max_nlane;
      unsigned int                     m_pos    = 0;
      unsigned int                     m_maxpos = 0;

    public:
      Iterator( LHCb::span<const uint16_t> bank, const UTDAQ::digiVec m_Digit ) : m_bank{bank}, m_iterDigit{m_Digit} {
        auto PosLane = std::find_if( m_iterDigit.begin(), m_iterDigit.end(),
                                     []( unsigned int& element ) { return element != 0; } );
        if ( PosLane != m_iterDigit.end() ) {
          m_lane   = std::distance( m_iterDigit.begin(), PosLane ); // first non-zero lane
          m_pos    = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane;
          m_maxpos = m_pos + nskip( m_iterDigit[m_lane] );
        }
      }

      // dereferencing
      [[nodiscard]] constexpr UTADCWord operator*() const { return UTADCWord{m_bank[m_pos], m_lane}; }

      constexpr Iterator& operator++() {
        m_pos += ( ( m_pos % 2 ) ? static_cast<unsigned int>( banksize::right_bit ) : 1u );
        if ( m_pos > m_maxpos ) {
          ++m_lane;
          while ( m_lane < UTDAQ::max_nlane && m_iterDigit[m_lane] == 0 ) { ++m_lane; }
          if ( m_lane < UTDAQ::max_nlane ) {
            m_pos    = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane;
            m_maxpos = m_pos + nskip( m_iterDigit[m_lane] );
          }
        }
        return *this;
      }

      constexpr bool operator!=( Sentinel ) const { return m_lane < UTDAQ::max_nlane; }
    };

  public:
    constexpr pos_range( LHCb::span<const uint16_t> bank, const UTDAQ::digiVec& ClusterVec )
        : m_bank{std::move( bank )}, m_Digit{ClusterVec} {}
    [[nodiscard]] auto           begin() const { return Iterator{m_bank, m_Digit}; }
    [[nodiscard]] constexpr auto end() const { return Sentinel{}; }
  };

  class posadc_range final {

  private:
    const LHCb::span<const uint16_t> m_bank;
    UTDAQ::digiVec                   m_Digit;
    struct Sentinel                  final {};
    bool                             m_isMax;
    unsigned int                     m_stripMax;

    class Iterator final {
      const LHCb::span<const uint16_t> m_bank;
      UTDAQ::digiVec                   m_iterDigit;
      unsigned int                     m_lane          = UTDAQ::max_nlane;
      unsigned int                     m_pos           = 0;
      unsigned int                     m_maxpos        = 0;
      unsigned int                     m_pos_nextBegin = 0;
      unsigned int                     m_fracStrip     = 0;
      bool                             m_isMax;
      unsigned int                     m_stripMax;
      enum class adcbits { adc = 0, strip = 5 };
      enum class adcmask { adc = 0x1F, strip = 0XFFE0 };
      constexpr unsigned int adcValue( unsigned int pos ) const {
        return ( m_bank[pos] & static_cast<unsigned int>( adcmask::adc ) ) >> static_cast<unsigned int>( adcbits::adc );
      }
      constexpr unsigned int strip( unsigned int pos ) const {
        return ( m_bank[pos] & static_cast<unsigned int>( adcmask::strip ) ) >>
               static_cast<unsigned int>( adcbits::strip );
      }
      constexpr static unsigned int nextPos( unsigned int pos ) {
        return pos + ( ( pos % 2 ) ? static_cast<unsigned int>( banksize::right_bit ) : 1u );
      }
      bool keepClustering() {
        unsigned int Optpos = m_pos, maxAdc = adcValue( m_pos ), cstrip = strip( m_pos );
        unsigned int Optpos_cal( 0u ), adccount( 0u ), numstrip( 1u );
        while ( true ) {
          m_pos = nextPos( m_pos );
          if ( m_pos > m_maxpos ) break;
          auto stripPrev = std::exchange( cstrip, strip( m_pos ) );
          if ( stripPrev + 1 != cstrip ) break;
          numstrip++;
          unsigned int adcvalue = adcValue( m_pos );
          if ( adcvalue > maxAdc ) {
            maxAdc = adcvalue;
            Optpos = m_pos;
          }
          if ( m_isMax ) {
            Optpos_cal += m_pos * adcvalue;
            adccount += adcvalue;
          }
        }
        if ( m_isMax ) Optpos = static_cast<unsigned int>( Optpos_cal / adccount );
        if ( numstrip <= m_stripMax ) {
          m_pos_nextBegin = std::exchange( m_pos, Optpos );
          return true;
        } else {
          m_pos_nextBegin = m_pos;
          return false; // can't make the cluster, even the bank is not empty.
        }
      }

    public:
      Iterator( LHCb::span<const uint16_t> bank, const UTDAQ::digiVec m_Digit, const bool iMax,
                const unsigned int stripMax )
          : m_bank{bank}, m_iterDigit{m_Digit}, m_isMax{iMax}, m_stripMax{stripMax} {
        auto PosLane =
            std::find_if( m_iterDigit.begin(), m_iterDigit.end(), []( unsigned int element ) { return element != 0; } );
        if ( PosLane != m_iterDigit.end() ) {
          m_lane            = std::distance( m_iterDigit.begin(), PosLane ); // first non-zero lane
          m_pos             = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane;
          m_maxpos          = m_pos + nskip( m_iterDigit[m_lane] );
          bool clusterFound = false;
          while ( !clusterFound ) {
            if ( m_pos <= m_maxpos ) { clusterFound = keepClustering(); }
            if ( m_pos > m_maxpos ) {
              ++m_lane;
              while ( m_lane < UTDAQ::max_nlane && m_iterDigit[m_lane] == 0 ) ++m_lane;
              if ( m_lane < UTDAQ::max_nlane ) {
                m_pos_nextBegin = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane;
                m_maxpos        = m_pos_nextBegin + nskip( m_iterDigit[m_lane] );
                m_pos           = m_pos_nextBegin;
                clusterFound    = keepClustering();
              } else {
                break;
              }
            }
          }
        }
      }

      // dereferencing
      [[nodiscard]] constexpr UTADCWord operator*() const { return {m_bank[m_pos], m_lane, m_fracStrip}; }

      Iterator& operator++() {
        bool clusterFound = false;
        while ( !clusterFound ) {
          m_pos = m_pos_nextBegin;
          // if m_pos is still in range there could be more hits so we call keepClustering
          if ( m_pos <= m_maxpos ) { clusterFound = keepClustering(); }
          // if keepClustering found a cluster m_pos will point to the maxiumum
          // adc value now and will still be smaller than m_maxpos. if it is
          // greater that means there wasn't a new cluster found and we are ready
          // to jump to the next lane
          if ( m_pos > m_maxpos ) {
            ++m_lane;
            while ( m_lane < UTDAQ::max_nlane && m_iterDigit[m_lane] == 0 ) ++m_lane;
            if ( m_lane < UTDAQ::max_nlane ) {
              m_pos_nextBegin = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane;
              m_maxpos        = m_pos_nextBegin + nskip( m_iterDigit[m_lane] );
              m_pos           = m_pos_nextBegin;
              clusterFound    = keepClustering();
            } else {
              break;
            }
          }
        }
        return *this;
      }

      [[nodiscard]] constexpr bool operator!=( Sentinel ) const { return m_lane < UTDAQ::max_nlane; }
    };

  public:
    constexpr posadc_range( LHCb::span<const uint16_t> bank, const UTDAQ::digiVec& ClusterVec, bool isMax,
                            unsigned int stripMax )
        : m_bank{std::move( bank )}, m_Digit{ClusterVec}, m_isMax{isMax}, m_stripMax{stripMax} {}
    [[nodiscard]] auto           begin() const { return Iterator{m_bank, m_Digit, m_isMax, m_stripMax}; }
    [[nodiscard]] constexpr auto end() const { return Sentinel{}; }
  };

  [[nodiscard]] constexpr unsigned int nClusters() const {
    static_assert( UTDAQ::max_nlane == 6 );
    return std::max( std::max( std::max( m_nDigits[0], m_nDigits[1] ), std::max( m_nDigits[2], m_nDigits[3] ) ),
                     std::max( m_nDigits[4], m_nDigits[5] ) );
  }
  [[nodiscard]] constexpr unsigned int nClusters( unsigned int laneID ) const { return m_nDigits[laneID]; }

  [[nodiscard]] auto posRange() const {
    return pos_range{m_bank.first( ( ( nClusters() + 1 ) / 2 ) * 16 - 4 ), m_nDigits};
  }

  [[nodiscard]] auto posAdcRange( bool isMax, unsigned int stripMax = 252 ) const {
    return posadc_range{m_bank.first( ( ( nClusters() + 1 ) / 2 ) * 16 - 4 ), m_nDigits, isMax, stripMax};
  }

private:
  LHCb::span<const uint16_t> m_bank;
  UTDAQ::digiVec             m_nDigits;
};
