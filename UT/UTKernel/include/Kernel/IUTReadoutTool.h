/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Detector/UT/ChannelID.h>
#include <Event/UTCluster.h>
#include <GaudiKernel/GaudiException.h>
#include <GaudiKernel/IAlgTool.h>
#include <Kernel/UTDAQBoard.h>
#include <Kernel/UTDAQDefinitions.h>
#include <Kernel/UTDAQID.h>
#include <Kernel/UTTell1Board.h>
#include <Kernel/UTTell1ID.h>
#include <Kernel/UTUtils.h>
#include <UTDet/DeUTDetector.h>
#include <UTDet/DeUTSector.h>
#include <boost/container/static_vector.hpp>
#include <memory>
#include <string>
#include <vector>

/** @class IUTReadoutTool IUTReadoutTool.h  UTDAQ/IUTReadoutTool
 *
 *  Interface Class for things related to the Readout - Tell1 Board
 *
 *  @author A. Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

// Declaration of the interface ID ( interface id, major version, minor version)

struct IUTReadoutTool : extend_interfaces<IAlgTool> {

  /// Derived condition for geometry info needed by the tool
  struct ReadoutInfo {
    // Because of a technical detail in ConditionAccessor (DetDesc), we have to be
    // very explicit when an object is movable and not copyable.
    ReadoutInfo()                = default;
    ReadoutInfo( ReadoutInfo&& ) = default;
    ReadoutInfo& operator=( ReadoutInfo&& ) = default;
    ReadoutInfo( const DeUTDetector& utdet )
        :
#ifdef USE_DD4HEP
        m_det {
      utdet
    }
#else
        m_det {
      &utdet
    }
#endif
    {}

    UT::Utils::BoardMapping                                boardMap;
    std::vector<std::unique_ptr<UTTell1Board>>             boards;
    unsigned int                                           nBoards{0};
    std::vector<std::unique_ptr<UTDAQ::Board>>             daqBoards;
    std::map<LHCb::Detector::UT::ChannelID, UTDAQ::Board*> sectorToBoardMap;
    std::map<UTDAQID, LHCb::Detector::UT::ChannelID>       daqToSectorMap;
    unsigned int                                           hybridsPerBoard{0};
    std::map<UTTell1ID, UTTell1Board*>                     boardsMap;
    std::vector<std::string>                               serviceBoxes;
    std::vector<unsigned int>                              firstBoardInRegion;
    bool                                                   stripflip = true;

#ifdef USE_DD4HEP
    DeUTDetector m_det;
#else
    const DeUTDetector* m_det = nullptr;
#endif

    const DeUTDetector& det() const {
      if ( !m_det ) {
        throw GaudiException( "null DeUTDetector pointer", "IUTReadoutTool::ReadoutInfo", StatusCode::FAILURE );
      };
#ifdef USE_DD4HEP
      return m_det;
#else
      return *m_det;
#endif
    }
  };

  /// Static access to interface id
  DeclareInterfaceID( IUTReadoutTool, 2, 0 );

  /// Get the condition key for the ReadoutInfo instance used by the tool.
  virtual std::string getReadoutInfoKey() const = 0;

  /// number of boards
  virtual unsigned int nBoard( const ReadoutInfo* = nullptr ) const = 0;

  /// return vector of Tell1IDs
  virtual std::vector<UTTell1ID> boardIDs( const ReadoutInfo* = nullptr ) const = 0;

  // convert offline Detector::UT::ChannelID to readout UTDAQID
  virtual UTDAQID channelIDToDAQID( const LHCb::Detector::UT::ChannelID offlineChan,
                                    const ReadoutInfo* = nullptr ) const = 0;

  // convert readout UTDAQID to offline Detector::UT::ChannelID
  virtual LHCb::Detector::UT::ChannelID daqIDToChannelID( const UTDAQID daqid, const ReadoutInfo* = nullptr ) const = 0;

  /// convert Detector::UT::ChannelID to DAQ ChannelID
  virtual UTDAQ::chanPair offlineChanToDAQ( const LHCb::Detector::UT::ChannelID aOfflineChan, double isf,
                                            const ReadoutInfo* = nullptr ) const = 0;

  /// convert offline interStripFraction to DAQ interStripFraction
  virtual double interStripToDAQ( const LHCb::Detector::UT::ChannelID aOfflineChan, const UTTell1ID aBoardID,
                                  const double isf, const ReadoutInfo* = nullptr ) const = 0;

  virtual bool ADCOfflineToDAQ( const LHCb::Detector::UT::ChannelID aOfflineChan, const UTTell1ID aBoardID,
                                LHCb::UTCluster::ADCVector& adcs, const ReadoutInfo* = nullptr ) const = 0;

  /// find the Tell1 board given a board ID
  virtual const UTTell1Board* findByBoardID( const UTTell1ID aBoardID, const ReadoutInfo* = nullptr ) const = 0;

  /// find a Tell board by storage order
  virtual const UTTell1Board* findByOrder( const unsigned int aValue, const ReadoutInfo* = nullptr ) const = 0;

  /// find a DAQ board by storage order
  virtual const UTDAQ::Board* findByDAQOrder( const unsigned int aValue, const ReadoutInfo* = nullptr ) const = 0;

  virtual boost::container::static_vector<LHCb::Detector::UT::ChannelID, 6>
  findBySourceID( const unsigned int aValue, const ReadoutInfo* = nullptr ) const = 0;

  /// service box
  virtual unsigned int nServiceBox( const ReadoutInfo* = nullptr ) const = 0;

  /// service box number
  virtual std::string serviceBox( const LHCb::Detector::UT::ChannelID& aChan, const ReadoutInfo* = nullptr ) const = 0;

  /// printout
  virtual void printMapping( const ReadoutInfo* = nullptr ) const = 0;

  /// list of the readout sector ids on the board in a board
  virtual std::vector<LHCb::Detector::UT::ChannelID> sectorIDs( const UTTell1ID board,
                                                                const ReadoutInfo* = nullptr ) const = 0;

  /// apply given callable to all sectors in a board
  virtual void applyToAllBoardSectors( const UTTell1ID board, const std::function<void( DeUTSector const& )>& func,
                                       const ReadoutInfo* = nullptr ) const = 0;

  /// list of the readout sectors ids in a service box
  virtual std::vector<LHCb::Detector::UT::ChannelID> sectorIDsOnServiceBox( const std::string& serviceBox,
                                                                            const ReadoutInfo* = nullptr ) const = 0;

  /// list of service boxes
  virtual const std::vector<std::string>& serviceBoxes( const ReadoutInfo* = nullptr ) const = 0;

  /// Add the mapping of source ID to TELL1 board number
  virtual unsigned int SourceIDToTELLNumber( unsigned int sourceID, const ReadoutInfo* = nullptr ) const = 0;

  /// Add the mapping of TELL1 board number to source ID
  virtual unsigned int TELLNumberToSourceID( unsigned int sourceID, const ReadoutInfo* = nullptr ) const = 0;

  /// Get a list of all TELL board numbers
  virtual std::vector<unsigned int> allTELLNumbers( const ReadoutInfo* = nullptr ) const = 0;

  virtual bool getstripflip( const ReadoutInfo* = nullptr ) const = 0;
};
