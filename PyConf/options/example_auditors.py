###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""An example application configured using PyConf components with auditors."""
from PyConf.application import ApplicationOptions, configure
from PyConf.control_flow import CompositeNode
from PyConf.Algorithms import Gaudi__Examples__IntDataProducer as IntDataProducer

line1 = CompositeNode("line1", children=[IntDataProducer()], force_order=True)

# Define the application environment and run it
options = ApplicationOptions(_enabled=False)
options.evt_max = 10
# Although we have no input data, must specify input-like metadata
options.input_type = "NONE"
options.dddb_tag = "dummy"
options.conddb_tag = "dummy"
options.simulation = False
# Enable the NameAuditor
options.auditors = ["NameAuditor"]

config = configure(options, line1)
