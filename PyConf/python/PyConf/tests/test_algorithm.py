###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from typing import OrderedDict
import pytest
import re
from dataclasses import dataclass

import Configurables
from PyConf import ConfigurationError
from PyConf.dataflow import DataHandle, force_location, GaudiDataHandle
from PyConf.components import Algorithm
from PyConf.Algorithms import (
    AnyDataPutAlgorithm,
    Gaudi__Examples__IntDataConsumer as IntDataConsumer,
    Gaudi__Examples__IntDataProducer as IntDataProducer,
    Gaudi__Examples__IntToFloatData as IntToFloatData,
    Gaudi__Examples__THDataConsumer as ToolConsumer,
    Gaudi__Examples__VectorDataProducer as VectorDataProducer,
    Gaudi__Examples__IntVectorsToIntVector as IntVectorsToIntVector,
    Gaudi__Examples__SDataProducer as SDataProducer,
    Gaudi__Examples__SRangesToIntVector as SRangesToIntVector,
    MyGaudiAlgorithm,
    Gaudi__Examples__CountSelectedTracks as CountSelectedTracks,
    Gaudi__Examples__SelectTracks as SelectTracks,
    Gaudi__Hive__FetchDataFromFile as FetchDataFromFile,
)
from PyConf.Tools import (
    Gaudi__Examples__FloatTool as FloatTool,
    MyTool,
)


class MockDataHandle(DataHandle):
    def __init__(self, location, id=None, type='unknown_t'):
        self._location = location
        self._type = type
        self._id = id or location
        # a dummy "producer", could also be replaced by a mock
        self._producer = IntDataProducer()

    @property
    def type(self):
        return self._type


def reset_global_store(new={}):
    old = Algorithm._algorithm_store
    Algorithm._algorithm_store = new
    return old


def test_init():
    # Fail on nonexistent properties
    with pytest.raises(ConfigurationError) as e:
        IntDataProducer(NotAProp=1)
    assert re.search(r'NotAProp.*not propert.* of.*IntDataProducer', str(e))

    # Output property is detected
    producer = IntDataProducer()
    assert len(producer.outputs) == 1
    assert len(producer.inputs) == 0
    data = producer.outputs['OutputLocation']
    assert isinstance(data, DataHandle)
    assert data.producer == producer

    # initializing an algorithm twice should give the same instance
    producer2 = IntDataProducer()
    assert producer is producer2, "algorithms instantiation doesn't seem to be cached correctly"

    with pytest.raises(ConfigurationError):
        # same algorithms shouldn't have different names
        IntDataProducer(name="wurst")

    # Cannot set outputs explicitly
    with pytest.raises(
            ConfigurationError,
            match=r'Cannot set output properties.*OutputLocation.*explicitly'):
        producer = IntDataProducer(OutputLocation='test')

    # Must always provide all inputs
    with pytest.raises(ConfigurationError) as e:
        consumer = IntDataConsumer()
    assert re.match(r'.*provide all inputs.*InputLocation.*', str(e))

    # Type of inputs
    with pytest.raises(TypeError) as e:
        consumer = IntDataConsumer(InputLocation='test')
    assert re.search(r'Expected DataHandle.*InputLocation.*test', str(e))

    consumer = IntDataConsumer(InputLocation=data)
    assert consumer.inputs['InputLocation'] == data

    consumer = IntDataConsumer(InputLocation=producer)
    assert consumer.inputs['InputLocation'] == data

    with pytest.raises(ConfigurationError):
        # cannot change property after instantiation
        consumer.OutputLevel = 2


def test_init_input_location_composite():
    """Test properties that are composites of DataHandles."""
    # To really understand the behaviour/API here, see LHCb#112
    multitransformer_bare = IntVectorsToIntVector()
    # FIXME: It is a bug that Algorithm cannot detect an input property for an
    # algorithm that accepts N inputs. This occurs because there's no API in
    # Gaudi for asking if a property is an input, only the heurisitcs of seeing
    # if a default value is a DataHandle. Because the default value here is an
    # empty list, the heuristics fail
    # assert len(multitransformer_bare.inputs) == 1
    assert len(multitransformer_bare.outputs) == 1

    vdp1 = VectorDataProducer()
    vdp2 = VectorDataProducer()
    mt = IntVectorsToIntVector(InputLocations=[vdp1, vdp2])
    mt2 = IntVectorsToIntVector(
        InputLocations=[vdp1.OutputLocation, vdp2.OutputLocation])
    # We should be able to handle a mix of algorithms and data handles
    mt3 = IntVectorsToIntVector(InputLocations=[vdp1, vdp2.OutputLocation])

    # If we initialize the MultiTransformer with inputs, then we can detect the
    # DataHandles and everything looks sensible (wrt. the FIXME above)
    assert len(mt.inputs) == 1
    assert len(mt2.inputs) == 1
    assert len(mt.inputs['InputLocations']) == 2
    assert len(mt2.inputs['InputLocations']) == 2
    assert len(mt3.inputs['InputLocations']) == 2
    assert all(isinstance(x, DataHandle) for x in mt.inputs['InputLocations'])
    assert all(isinstance(x, DataHandle) for x in mt2.inputs['InputLocations'])

    # Nested collections should also be supported
    @dataclass
    class FunctorLike:
        input_data: DataHandle

        def to_json(self):
            return self.input_data.location

        def data_dependencies(self):
            return [self.input_data]

    # The property value below does not make sense, but since we don't
    # configure the application, Gaudi does not complain.
    mt4 = IntVectorsToIntVector(InputLocations=[
        [FunctorLike(vdp1.OutputLocation)],
        {
            "key": FunctorLike(vdp2.OutputLocation)
        },
    ])
    assert len(mt4.inputs) == 1
    # won't check key in inputs, see _DATA_DEPENDENCY_KEY_SUFFIX hack
    inputs = list(mt4.inputs.values())[0]
    assert len(inputs) == 2
    assert all(isinstance(x, DataHandle) for x in inputs)


def test_tool_compat():
    producer = IntDataProducer()
    float_producer = IntToFloatData(InputLocation=producer)
    alg = ToolConsumer(
        FloatTool=FloatTool(Input=float_producer), InputLocation=producer)
    assert len(alg.tools) == 1

    with pytest.raises(TypeError):
        ToolConsumer(
            FloatTool=Configurables.Gaudi__Examples__FloatTool(),
            InputLocation=producer)  # don't be a fool, wrap your tool

    # A Tool instance should be usable in multiple (different) Algorithms
    tool = FloatTool(Input=float_producer)
    alg1 = ToolConsumer(FloatTool=tool, InputLocation=producer)
    alg2 = ToolConsumer(
        FloatTool=tool, InputLocation=producer,
        OutputLevel=0)  # make the second alg slightly different
    assert alg1.tools == alg2.tools


def test_list_of_tools_compat():
    float_producer = IntToFloatData(InputLocation=IntDataProducer())
    # MyGaudiAlgorithm expects DataObject handles but we don't have a
    # dummy producer for these, so "erase" the type to avoid the check.
    data = IntDataProducer().OutputLocation.untyped()
    alg = MyGaudiAlgorithm(
        raw=data,
        tracks=data,
        hits=data,
        MyPublicToolHandleArrayProperty=[
            FloatTool(public=True, Input=float_producer),
            FloatTool(public=True, Input=float_producer)
        ])
    assert len(alg.tools["MyPublicToolHandleArrayProperty"]) == 2


def test_public_tool_init():
    """Public tool properties are configurable only with public tools."""
    public_tool = MyTool(public=True)
    # Tools are private by default
    private_tool = MyTool()

    # MyGaudiAlgorithm expects DataObject handles but we don't have a
    # dummy producer for these, so "erase" the type to avoid the check.
    data = IntDataProducer().OutputLocation.untyped()
    producer_kwargs = dict(raw=data, tracks=data, hits=data)

    MyGaudiAlgorithm(PubToolHandle=public_tool, **producer_kwargs)
    with pytest.raises(ConfigurationError):
        MyGaudiAlgorithm(PubToolHandle=private_tool, **producer_kwargs)


def test_datahandle_type_check():
    # Passing the same type is fine
    int_data = IntDataProducer().OutputLocation
    IntDataConsumer(InputLocation=int_data)

    # Passing the wrong type fails
    float_data = IntToFloatData(InputLocation=IntDataProducer()).OutputLocation
    with pytest.raises(ConfigurationError) as excinfo:
        IntDataConsumer(InputLocation=float_data)
    assert (re.search(r'IntDataConsumer\.InputLocation.*int.*float',
                      str(excinfo.value), re.DOTALL) is not None)

    # Passing an "untyped" handle is fine
    untyped_data = float_data.untyped()
    assert untyped_data.type == 'unknown_t'
    assert untyped_data.location == float_data.location
    IntDataConsumer(InputLocation=untyped_data)

    # KeyedContainer can be given to a Range
    keyed_container = SDataProducer()
    SRangesToIntVector(InputRanges=[keyed_container])
    # but not if the contained type does not match
    with pytest.raises(ConfigurationError):
        CountSelectedTracks(InputData=keyed_container)

    # SharedObjectsContainer (aka Selection) can be given to a Range
    shared_objects_container = SelectTracks(
        InputData=MockDataHandle('/Event/Range')).OutputData
    SelectTracks(InputData=shared_objects_container)


def algorithm_equal_hashes():
    """Two Algorithms should have equal hashes if their properties and inputs are equal."""
    pass


def algorithm_unequal_hashes():
    """Two Algorithms should not have equal hashes if their properties and inputs are not equal."""
    pass


def algorithm_hash_forced_outputs():
    """An Algorithm with a forced output should have a different hash to one without."""
    pass


def test_all_producers():
    idp1 = IntDataProducer()
    idc1 = IntDataConsumer(InputLocation=idp1.OutputLocation)
    assert idc1.all_producers() == set([idp1, idc1])

    vdp1 = VectorDataProducer()
    vdp2 = VectorDataProducer()
    mt = IntVectorsToIntVector(InputLocations=[vdp1, vdp2])
    mt2 = IntVectorsToIntVector(InputLocations=[mt])

    assert mt.all_producers() == set([vdp1, vdp2, mt])
    assert mt2.all_producers() == set([vdp1, vdp2, mt, mt2])


# Output property exists, but is a string
def test_custom_output():
    reset_global_store()

    # AnyDataPutAlgorithm has two properties
    # - Output: DataObjectWriteHandle
    # - Location: a string
    # The latter is used to put two objects on the TES at
    # "<Location>/One" and "<Location>/Two".
    # This makes it a good example for patching on the python side.
    # NB: we use the NeededResources property to create algorithms with
    # different identities (avoids reseting the global registry).

    props = AnyDataPutAlgorithm.getDefaultProperties()
    assert isinstance(props["Location"], str)
    assert isinstance(props["Output"], GaudiDataHandle)
    assert "NotAProp" not in props

    alg1 = AnyDataPutAlgorithm()
    assert set(alg1.outputs.keys()) == {"Output"}

    # It's deprecated to give only an output_transform
    with pytest.warns(
            DeprecationWarning, match=r"output_transform without output"):
        alg1_again = AnyDataPutAlgorithm(
            output_transform=lambda Output: {"Output": Output})
    assert alg1_again == alg1

    # We can force a location to a data handle and we'll get another instance
    alg2 = AnyDataPutAlgorithm(
        outputs={"Output": force_location("/Event/Output")})
    assert alg2 is not alg1

    # Forcing the same again gives us the same instance
    alg2_again = AnyDataPutAlgorithm(
        outputs={"Output": force_location("/Event/Output")})
    assert alg2_again is alg2

    # Forcing the same again and setting an "identity" output_transform
    # gives us the same instance
    alg2_again = AnyDataPutAlgorithm(
        outputs={"Output": force_location("/Event/Output")},
        output_transform=lambda Output: {"Output": Output})
    assert alg2_again is alg2

    # Doing it yet another time with another function (instance) that
    # has the same behaviour as the previous one gives us the same instance.
    alg2_again = AnyDataPutAlgorithm(
        outputs={"Output": force_location("/Event/Output")},
        output_transform=lambda Output: {"Output": Output})
    assert alg2_again is alg2

    # Forcing with another location gives another instance
    alg3 = AnyDataPutAlgorithm(
        outputs={"Output": force_location("/Event/OtherOutput")})
    assert alg3 is not alg2

    # outputs is the complete list, so it may hide data handles
    with pytest.raises(
            ConfigurationError, match=r"outputs.*are hidden.*Output"):
        AnyDataPutAlgorithm(
            NeededResources=['alg5'],
            outputs={"Location": None},
        )

    # Cannot define an output that's not a property without output_transform
    with pytest.raises(
            ConfigurationError,
            match=(r"NotAProp.*not propert.*Check.*output_transform")):
        AnyDataPutAlgorithm(
            NeededResources=['alg6'],
            outputs={
                "NotAProp": None,
                "Output": None
            },
        )

    # output_transform has the right signature (matches output.keys())
    with pytest.raises(
            TypeError,
            match="parameters of output_transform do not match output.keys()"):
        AnyDataPutAlgorithm(
            NeededResources=['alg6'],
            outputs={
                "NotAProp": None,
                "Output": None
            },
            output_transform=lambda NotAProp: [],
        )

    # check that output_transform does not overwrite other set properties
    # FIXME we can probably check this earlier, at instantiation.
    alg7 = AnyDataPutAlgorithm(
        NeededResources=['alg7'],
        OutputLevel=3,
        outputs={"Output": None},
        output_transform=lambda Output: {"OutputLevel": 5},
    )
    with pytest.raises(
            ConfigurationError, match=r"output.*overwrite.*OutputLevel"):
        alg7.configuration()

    # Check that we can force a location for existing and non-existing
    # properties.
    alg8 = AnyDataPutAlgorithm(
        NeededResources=['alg8'],
        outputs={
            "Output": force_location("/Event/Output"),
            "NotAProp": force_location("/Event/NotAProp"),
        },
        output_transform=lambda NotAProp, Output: {},
    )
    assert alg8.Output.location == "/Event/Output"
    assert alg8.Output.type != "unknown_t"
    assert alg8.NotAProp.location == "/Event/NotAProp"
    assert alg8.NotAProp.type == "unknown_t"

    # Check that we cannot use force_location on non-string properties
    with pytest.raises(ValueError, match=r"cannot force.*OutputLevel"):
        AnyDataPutAlgorithm(
            NeededResources=['alg9'],
            outputs={"OutputLevel": force_location("/Event/Dummy3")})

    # TODO implement forcing location on data handles like this:
    with pytest.raises(ConfigurationError):
        IntDataProducer(OutputLocation=force_location("/Event/Dummy0"))


def test_outputs_examples():
    # An algorithm where Output is a write data handle and Location is
    # a string property but we want to expose it as output. We must set
    # ExtraOutputs to inform the HiveDataBroker about that location.
    def transform(Output, Location):
        return {
            "Output": Output,
            "Location": Location,
            "ExtraOutputs": [Location],
        }

    reset_global_store()
    alg = AnyDataPutAlgorithm(
        outputs={
            "Output": None,
            "Location": None
        },
        output_transform=transform,
    )
    alg_conf, = alg.configuration().values()
    assert alg_conf["Location"] == alg.Location.location
    assert alg_conf["ExtraOutputs"] == [alg.Location.location]

    location = '/Event/Data'  # location in input file

    # FetchDataFromFile without forcing a location does not make sense
    # in practice as the object will always be put at `location` whereas
    # alg.Output would be generated by PyConf.

    # FetchDataFromFile with forcing a location
    alg = FetchDataFromFile(
        outputs={'Output': force_location(location)},
        output_transform=lambda Output: {"DataKeys": [location]},
    )
    assert alg.Output.location == location
    alg_conf, = alg.configuration().values()
    assert alg_conf["DataKeys"] == [location]

    # check forcing of C++ type of previously untyped datahandle
    dh = alg.Output
    assert dh.type == 'unknown_t'
    dh.force_type("float")
    assert dh.type == 'float'
    # calling it with the same type again is a no-op and is allowed!
    dh.force_type("float")
    with pytest.raises(ConfigurationError):
        # once typed you can't change the type anymore
        dh.force_type("int")

    # Dynamically-sized list of outputs
    location_map = OrderedDict([
        # ("exposed attribute", "location/branch in input file")
        ("VeloRawEvent", "/Event/Velo/RawEvent"),
        ("CaloRawEvent", "/Event/Calo/RawEvent"),
    ])

    # FetchDataFromFile with a dynamic number of outputs
    def FetchDataFromFile_transform(**outputs):
        # We ignore the output values as we're forcing the locations.
        return {"DataKeys": [location_map[k] for k in outputs]}

    alg = FetchDataFromFile(
        outputs={
            prop: force_location(loc)
            for prop, loc in location_map.items()
        },
        output_transform=FetchDataFromFile_transform,
    )
    assert isinstance(alg.VeloRawEvent, DataHandle)
    assert isinstance(alg.CaloRawEvent, DataHandle)
    alg_conf, = alg.configuration().values()
    assert alg_conf["DataKeys"] == [
        alg.VeloRawEvent.location, alg.CaloRawEvent.location
    ]


def test_deterministic_hashes():
    # "Magic" numbers below depend on the implementation of the
    # utilities.hash_object function and how it's used in the Algorithm,
    # Tool and DataHandle classes.

    producer = IntDataProducer()
    assert hash(producer) == 7231163653109153080
    assert hash(producer.OutputLocation) == 8582423681402497604

    # Test something with inputs and tools
    float_producer = IntToFloatData(InputLocation=producer)
    alg = ToolConsumer(
        FloatTool=FloatTool(Input=float_producer), InputLocation=producer)
    assert hash(alg) == 3202585037371445041

    tool = FloatTool(Input=IntToFloatData(InputLocation=producer))
    assert hash(tool) == 2038213875666448834


def test_duplicate_instances_with_distinct_names():
    # sometimes, algorithms are configured indentically (modulo their name)
    # for spurious reasons -- and in those cases, it should be possible to
    # have multiple instances which happen to 'do the same thing'

    first = IntDataProducer(name="FirstCompositeInteger", Value=4)
    # not opting-in must generate a ConfigurationError
    with pytest.raises(ConfigurationError):
        second = IntDataProducer(name="ThirdPrime", Value=4)
    # but with an explicit opt-in it is allowed:
    second = IntDataProducer(
        name="ThirdPrime",
        Value=4,
        allow_duplicate_instances_with_distinct_names=True)

    assert second.properties == first.properties
    assert second is not first
    assert second.name != first.name
