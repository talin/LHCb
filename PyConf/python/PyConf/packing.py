##############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Helpers for configuring packing and unpacking."""

from PyConf.components import force_location
from PyConf.location_prefix import prefix
from PyConf import configurable

_reco_locations = {
    "ChargedProtos": ("/Event/Rec/ProtoP/Charged", "ProtoParticles"),
    "NeutralProtos": ("/Event/Rec/ProtoP/Neutrals", "ProtoParticles"),
    "Tracks": ("/Event/Rec/Track/Best", "Tracks"),
    "MuonPIDTracks": ("/Event/Rec/Muon/MuonTracks", "Tracks"),
    "PVs": ("/Event/Rec/Vertex/Primary", "PVs"),
    "CaloElectrons": ("/Event/Rec/Calo/Electrons", "CaloHypos"),
    "CaloPhotons": ("/Event/Rec/Calo/Photons", "CaloHypos"),
    "CaloMergedPi0s": ("/Event/Rec/Calo/MergedPi0s", "CaloHypos"),
    "CaloSplitPhotons": ("/Event/Rec/Calo/SplitPhotons", "CaloHypos"),
    "MuonPIDs": ("/Event/Rec/Muon/MuonPID", "MuonPIDs"),
    "RichPIDs": ("/Event/Rec/Rich/PIDs", "RichPIDs"),
    "RecSummary": ("/Event/Rec/Summary", "RecSummary")
}

_default_persisted_locations = {k: v[0] for k, v in _reco_locations.items()}

_pp2mcp_locations = {
    "ChargedPP2MC": ("/Event/Relations/ChargedPP2MCP", "PP2MCPRelations"),
    "NeutralPP2MC": ("/Event/Relations/NeutralPP2MCP", "PP2MCPRelations"),
}

_default_persisted_pp2mcp_locations = {
    k: v[0]
    for k, v in _pp2mcp_locations.items()
}


@configurable
def reco_locations(stream="/Event/HLT2"):
    return {
        k: (prefix(v[0], stream), v[1])
        for k, v in _reco_locations.items()
    }


@configurable
def pp2mcp_locations(stream="/Event/HLT2"):
    return {
        k: (prefix(v[0], stream), v[1])
        for k, v in _pp2mcp_locations.items()
    }


@configurable
def default_persisted_locations(locations=_default_persisted_locations,
                                stream="/Event"):
    return {k: prefix(v, stream) for k, v in locations.items()}


@configurable
def persisted_location(k, force=True, locations=default_persisted_locations):
    return force_location(locations()[k]) if force else None


def packers_map():

    from PyConf.Algorithms import (
        SOATrackPacker,
        SOACaloClusterPacker,
        SOACaloHypoPacker,
    )

    return {
        "Tracks_v2": SOATrackPacker,
        "CaloClusters_v2": SOACaloClusterPacker,
        "CaloHypos_v2": SOACaloHypoPacker
    }


def unpackers_map():

    from PyConf.Algorithms import (
        RecVertexUnpacker,
        TrackUnpacker,
        RichPIDUnpacker,
        MuonPIDUnpacker,
        CaloHypoUnpacker,
        CaloClusterUnpacker,
        CaloDigitUnpacker,
        CaloAdcUnpacker,
        ProtoParticleUnpacker,
        ParticleUnpacker,
        VertexUnpacker,
        FlavourTagUnpacker,
        P2VRelationUnpacker,
        P2MCPRelationUnpacker,
        PP2MCPRelationUnpacker,
        P2IntRelationUnpacker,
        P2InfoRelationUnpacker,
        RecSummaryUnpacker,
        SOATrackUnpacker,
        SOACaloClusterUnpacker,
        SOACaloHypoUnpacker,
    )

    return {
        "Tracks": TrackUnpacker,
        "RichPIDs": RichPIDUnpacker,
        "MuonPIDs": MuonPIDUnpacker,
        "CaloHypos": CaloHypoUnpacker,
        "CaloClusters": CaloClusterUnpacker,
        "CaloDigits": CaloDigitUnpacker,
        "CaloAdcs": CaloAdcUnpacker,
        "PVs": RecVertexUnpacker,
        "ProtoParticles": ProtoParticleUnpacker,
        "Particles": ParticleUnpacker,
        "Vertices": VertexUnpacker,
        "FlavourTags": FlavourTagUnpacker,
        "P2VRelations": P2VRelationUnpacker,
        "P2MCPRelations": P2MCPRelationUnpacker,
        "P2IntRelations": P2IntRelationUnpacker,
        "P2InfoRelations": P2InfoRelationUnpacker,
        "PP2MCPRelations": PP2MCPRelationUnpacker,
        "RecSummary": RecSummaryUnpacker,
        "Tracks_v2": SOATrackUnpacker,
        "CaloClusters_v2": SOACaloClusterUnpacker,
        "CaloHypos_v2": SOACaloHypoUnpacker
    }
