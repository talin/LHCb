###############################################################################
# (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.packing import reco_locations, unpackers_map
from PyConf.components import force_location
from PyConf.dataflow import DataHandle
from PyConf.tonic import configurable
from PyConf.Algorithms import (UnpackMCParticle, UnpackMCVertex,
                               HltSelReportsDecoder, RawBankSizeFilter)
from PyConf.application import (default_raw_banks, make_odin,
                                make_data_with_FetchDataFromFile)
from GaudiConf.LbExec import InputProcessTypes, HltSourceID


@configurable
def get_tes_root(*, input_process: InputProcessTypes):
    """
  Get the ROOT_TES location from the input_process type
  """
    if input_process == InputProcessTypes.Spruce:
        return '/Event/Spruce/HLT2'
    elif input_process in [
            InputProcessTypes.Hlt2, InputProcessTypes.TurboPass
    ]:
        return '/Event/HLT2'
    else:
        raise NotImplementedError(
            f"The specified 'input_process' {input_process} is not recognised. Can only be 'Hlt2' or 'Spruce' or 'TurboPass'. Please check!"
        )


@configurable
def dstdata_filter(source: HltSourceID, raw_banks=default_raw_banks):
    """
    Setting a `RawBankSizeFilter` filter on the DstData bank size.

    This filter will skip events with empty DstData but a positive line decision.
    This may occur in events where DstData is a-priori huge (>16MB), and cannot be saved.

    Returns:
    The `RawBankSizeFilter` algorithm instance.
    """
    # TODO: should be able to specify the SourceID of the DstData raw bank...
    # and thus require a HltSourceID as arg to this function -- augment the Filter to check the
    #  sourceID...
    return RawBankSizeFilter(
        name='FilterDstDataSize_{}'.format(source),
        RawBankLocation=default_raw_banks("DstData"))


@configurable
def upfront_decoder(*, source: HltSourceID, raw_banks=default_raw_banks):
    """Return a DataHandle for  HltPackedBufferDecoder output
       This is input to all unpacking algorithms except MC and old unpacker
    """
    from PyConf.Algorithms import HltPackedBufferDecoder
    source = HltSourceID(source)
    return HltPackedBufferDecoder(
        name="PackedBufferDecoder_{}".format(source.name),
        SourceID=source.name,
        DecReports=raw_banks("HltDecReports"),
        RawBanks=raw_banks("DstData")).OutputBuffers


def _get_unpacked(type_name, location, **kwargs):
    mapped_dst_banks = upfront_decoder()
    if 'name' not in kwargs:
        sourceID = mapped_dst_banks.producer.name.split('_')[
            -1]  # see above `upfront_decoder()`
        kwargs['name'] = 'Unpack_{}_{} '.format(sourceID,
                                                location.replace('/', '_'))
    return unpackers_map()[type_name](
        InputName=mapped_dst_banks,
        outputs={
            "OutputName": force_location(location)
        },
        **kwargs).OutputName


def get_particles(location):
    """
    Unpacking of particles. This will also unpack the decay vertices associated with these particles implicitely
    """
    return _get_unpacked("Particles", location)


def get_pp2mcp_relations(location):
    """
    Unpacking of ProtoParticle to MCParticle relations. These are needed for truth matching
    MC Particles need to be unpacked first, proto particles are unpacked within relation unpacking
    Assume relations and MC particles are under same TES root
    """

    mc_location = location.replace(
        "Relations/ChargedPP2MCP" if "Charged" in location else
        "Relations/NeutralPP2MCP", "MC/Particles")
    mc_parts = get_mc_particles(mc_location)

    return _get_unpacked("PP2MCPRelations", location, ExtraInputs=[mc_parts])


def get_p2v_relations(location):
    """
    Unpacking of P2V relations. Make sure particles are unpacked
    """
    return _get_unpacked("P2VPRelations", location)


def get_mc_particles(location, mc_vertices: DataHandle = None):
    """
    Unpacking of MCParticles.
    Chainning MCVertices as dependency is needed for truth matching, if mc_vertices is None, it will first unpack MCVertices

    Args:
        location (str): Location of the LHCb::MCParticles
        mc_vertices (DataHandle, optional): unpacked `LHCb::MCVertices`, defaults to None.

    Returns:
        DataHandle: DataHandle of `LHCb::MCParticles`.
    """

    sim_location = location.replace("MC/Particles", "pSim/MCParticles")

    if mc_vertices is None:
        mc_vertices = get_mc_vertices(
            location.replace("MC/Particles", "MC/Vertices"))

    return UnpackMCParticle(
        name='Unpack_{}'.format(location.replace('/', '_')),
        InputName=make_data_with_FetchDataFromFile(sim_location),
        outputs={
            "OutputName": force_location(location)
        },
        ExtraInputs=[mc_vertices],
    ).OutputName


def get_mc_vertices(location):
    """
    Unpacking of MCVertices.

    Args:
        location (str): Location of the LHCb::MCParticles

    Returns:
        mc_vertices (DataHandle): DataHandle of `LHCb::MCVertices`.
    """
    sim_location = location.replace("MC/Vertices", "pSim/MCVertices")

    return UnpackMCVertex(
        name='Unpack_{}'.format(location.replace('/', '_')),
        InputName=make_data_with_FetchDataFromFile(sim_location),
        outputs={
            "OutputName": force_location(location)
        },
    ).OutputName


@configurable
def reconstruction(*, input_process: InputProcessTypes, obj: str, reco: str):
    stream = get_tes_root(input_process=input_process)
    return _get_unpacked(reco, reco_locations(stream)[obj][0])


def get_rich_pids():
    return reconstruction(obj='RichPIDs', reco="RichPIDs")


def get_muon_pids():
    return reconstruction(obj='MuonPIDs', reco="MuonPIDs")


def get_charged_protoparticles():
    return reconstruction(obj="ChargedProtos", reco="ProtoParticles")


def get_neutral_protoparticles():
    return reconstruction(obj="NeutralProtos", reco="ProtoParticles")


def get_pvs_v1():
    return reconstruction(obj="PVs", reco="PVs")


def get_pvs():
    ### Temporary: as long as we persist v1, we need to insert a converter for the new PVs
    from PyConf.Algorithms import RecV1ToPVConverter
    return RecV1ToPVConverter(InputVertices=get_pvs_v1()).OutputVertices


def get_rec_summary():
    return reconstruction(obj="RecSummary", reco="RecSummary")


def get_mc_track_info():
    mc_track_info = make_data_with_FetchDataFromFile('/Event/MC/TrackInfo')
    mc_track_info.force_type('LHCb::MCProperty')
    return mc_track_info


def get_mc_header(*, location='/Event/MC/Header', extra_inputs: list = []):
    """Fetch LHCb::MCHeader object from file, this object contains:
        * Pointer to primary vertices
        * Event time
        * Event number
    .. note: Accessing the primary vertices information from pointer requires the associated LHCb::MCVertex is properly unpacked, if
    extra_inputs is [], this function defaults to unpack '/Event/MC/Vertices'.

    Args:
        location (str, optional): Location of the LHCb::MCHeader, defaults to '/Event/MC/Header'.
        extra_inputs (list, optional): Extra dependencies, defaults to []

    Returns:
        DataHandle: DataHandle of the `LHCb::MCHeader`.
    """

    if not extra_inputs:
        mc_vertices = get_mc_vertices('/Event/MC/Vertices')
        extra_inputs = [mc_vertices]

    mc_header = make_data_with_FetchDataFromFile(
        location,
        ExtraInputs=extra_inputs,
        name='FetchDataFromFile_GetMCHeader')
    mc_header.force_type('LHCb::MCHeader')
    return mc_header


def get_generator_header(*, location='/Event/Gen/Header'):
    """Fetch LHCb::GenHeader object from file

    Args:
        location (str, optional): Location of the `LHCb::GenHeader`, defaults to '/Event/Gen/Header'.

    Returns:
        DataHandle: DataHandle of the `LHCb::GenHeader`.
    """
    gen_header = make_data_with_FetchDataFromFile(location)
    gen_header.force_type('LHCb::GenHeader')
    return gen_header


def get_odin():
    """
    Function to get the LHCb::ODIN location

    Args:
        options (DaVinci.Options): lbexec provided options object
    Returns:
        odin_loc: Location of the LHCb::ODIN
    """
    return make_odin()


@configurable
def get_hlt_reports(*, source: HltSourceID, raw_banks=default_raw_banks):
    """
    Set the Hlt service and algorithms.

    Args:
      source (HltSourceID): source ID or selection stage. It can be "Hlt1" or "Hlt2" or "Spruce".

    Returns:
      HltDecReportsDecoder containing the configuration for Hlt1, Hlt2 and Spruce lines.
    """

    source = HltSourceID(source)
    output_loc = "/Event/%s/DecReports" % source.name

    from PyConf.Algorithms import HltDecReportsDecoder
    return HltDecReportsDecoder(
        name=source.name + "DecReportsDecoder_{hash}",
        SourceID=source.name,
        RawBanks=raw_banks("HltDecReports"),
        outputs={'OutputHltDecReportsLocation': force_location(output_loc)},
    )


def get_decreports(source: HltSourceID):
    """
    Function to get the LHCb::DecReports for HLT1, HLT2 or Sprucing.

    Args:
        source (str): Selection stage can be "Hlt1" or "Hlt2" or "Spruce"
    Returns:
        dec_loc: Location of the LHCb::DecReports for HLT1 or Hlt2 or Spruce
    """
    return get_hlt_reports(source).OutputHltDecReportsLocation


@configurable
def get_hlt1_selreports(raw_banks=default_raw_banks):
    return HltSelReportsDecoder(
        DecReports=raw_banks("HltDecReports"),
        RawBanks=raw_banks("HltSelReports"),
        SourceID=HltSourceID.Hlt1.name).OutputHltSelReportsLocation
