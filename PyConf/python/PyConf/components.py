###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Wrappers and helpers for Configurable algorithms and tools.

There are three core components that are provided for defining a data flow:

    1. Algorithm
    2. Tool
    3. DataHandle

DataHandles are inputs and outputs of Algorithms and Tools.
"""
import inspect
import re
import importlib
import warnings
from collections import OrderedDict
from functools import lru_cache
from html import escape as html_escape

import GaudiKernel
import pydot

from . import ConfigurationError
from .utilities import hash_object
from .dataflow import (
    DataHandle,
    GaudiDataHandle,
    configurable_inputs,
    dataflow_config,
    is_datahandle,
    force_location,
    is_convertible_type,
    getDefaultProperties,
    getConfigurableOutputs,
)

__all__ = [
    'Algorithm',
    'Tool',
    'force_location',
    'is_algorithm',
    'is_tool',
    'setup_component',
]

# String that separates a name from its unique ID
_UNIQUE_SEPARATOR = '_'
_IDENTITY_LENGTH = 8
_IDENTITY_TABLE = {}

# If a property ends with this key, it is a pseudo-property which exists to
# hold the data dependencies of another property value
_DATA_DEPENDENCY_KEY_SUFFIX = "_PyConfDataDependencies"

_FLOW_GRAPH_NODE_COLOUR = 'aliceblue'
_FLOW_GRAPH_INPUT_COLOUR = 'deepskyblue1'
_FLOW_GRAPH_OUTPUT_COLOUR = 'coral1'


def _keyword_params(func):
    """Return the set of keyword parameter names of a function."""
    try:
        sig = inspect.signature(func)
        kinds = (inspect.Parameter.POSITIONAL_OR_KEYWORD,
                 inspect.Parameter.KEYWORD_ONLY)
        return set(n for n, p in sig.parameters.items() if p.kind in kinds)
    except AttributeError:
        return set(inspect.getargspec(func).args)


def _safe_name(string):
    """Return `string` with :: replaced with __."""
    if not string:
        return
    return string.replace('::', '__')


def unique_name_ext_re():
    return "(?:%s[1234567890abcdef]{%s})?" % (_UNIQUE_SEPARATOR,
                                              _IDENTITY_LENGTH)


def findRootObjByName(rootFile, name):
    '''
    Finds the object with given name in the given root file,
    where name is a regular expression or a set of them separated by '/' in case directories are used
    '''
    curObj = rootFile
    for n in name.split('/'):
        matches = [
            k.GetName() for k in curObj.GetListOfKeys()
            if re.fullmatch(n, k.GetName())
        ]
        if len(matches) > 1:
            raise Exception(
                "Collision of names in Root : found several objects with name matching %s inside %s : %s"
                % (n, curObj.GetName(), matches))
        if len(matches) == 0:
            raise Exception("Failed to find object with name %s inside %s" %
                            (n, curObj.GetName()))
        curObj = curObj.Get(matches[0])
    return curObj


def findRootObjByDir(rootFile, directory, name):
    '''
    Finds an object with given name in given directory from the given rootFile
    Deals with the fact that the directory may have an extra hash suffix
    '''
    return findRootObjByName(rootFile,
                             directory + unique_name_ext_re() + '/' + name)


def _amend_name(name, identity):
    """replaces {hash} (if present) with the first 8 chars of the hexadecimal version of identiy"""
    shortID = hex(identity)[2:_IDENTITY_LENGTH + 2]
    return name.format(hash=shortID)


def _get_unique_name(name, algType, identity):
    """Return a unique name for the component with name name, type algType,
    and given identity (supposed to be an int uniquely identifying the component)

    If name is none, uses algType_{hash} as name.
    Then replaces {hash} with the first 8 chars of the hexadecimal version of identiy if present.
    Finally checks that there is no collision with an existing name and returns
    """
    if not name:
        name = algType + "_{hash}"
    name = _amend_name(name, identity)
    if name in _IDENTITY_TABLE and _IDENTITY_TABLE[name] != identity:
        raise ConfigurationError(
            "Name collision detected for %s, identities are %s and %s. Please make names unique. One possibility is to use a '_{hash}' suffix that will be automatically replaced by a unique hash"
            % (name, hex(identity), hex(_IDENTITY_TABLE[name])))
    _IDENTITY_TABLE[name] = identity
    return name


def findCountersForRe(counterDict, expr, simpleExpr=None, maxCandidates=1):
    """ Find the set of counters for the algo matching the given regular expression within the dictionnary of counters given
    in case too many sets of counters are found, raises an exception
    in case none is found, also raises an exception
    simpleExpr is just the displayed version of expr. In some cases it avoid making error messages too complicated
    """
    if simpleExpr is None: simpleExpr = expr
    candidates = [v for c, v in counterDict.items() if re.fullmatch(expr, c)]
    if len(candidates) > maxCandidates:
        raise Exception(
            "Too many matching algorithms in findCounters (looking for %s) : %s"
            % (simpleExpr, [c for c in counterDict if re.fullmatch(expr, c)]))
    if len(candidates) == 0:
        raise Exception(
            "No algorithm found in findCounters for %s" % simpleExpr)
    if maxCandidates == 1:
        return candidates[0]
    else:
        return candidates[:maxCandidates]


def findCounters(counterDict, algo, maxCandidates=1):
    """ Find the set of counters for the given algo within the dictionnary of counters given
    provided that the given algo name was made unique by adding some extra suffix before counters were registered
    in case too many sets of counters are found, raises an exception
    in case none is found, also raises an exception
    """
    return findCountersForRe(counterDict, algo + unique_name_ext_re(), algo,
                             maxCandidates)


def matchAlgoName(s, name):
    """checks whether s matches the algo name given in name provided that it was made unique by adding some extra suffix"""
    return re.fullmatch(name + unique_name_ext_re(), s)


def _is_configurable_algorithm(t):
    try:
        return t.getGaudiType() == 'Algorithm'
    except AttributeError:
        return False


def _is_configurable_tool(t):
    try:
        return t.getGaudiType() == 'AlgTool'
    except AttributeError:
        return False


def _check_input_integrity(t, inputs, other_args, input_transform=None):
    dh_inputs = configurable_inputs(t)
    dh_inputs = {
        k: v
        for k, v in dh_inputs.items()
        # exclude masks from the DataHandle inputs to consider
        if v.type() != "mask_t"
        # exclude forced input locations
        and not isinstance(other_args.get(k), force_location)
    }

    # mutate other_args to "unwrap" force_locations to the underlying string
    # FIXME once _pop_inputs and this is refactored (RM)
    for k, v in other_args.items():
        if isinstance(v, force_location):
            other_args[k] = str(v)

    not_datahandles = {k: v for k, v in other_args.items() if k in dh_inputs}
    if not_datahandles:
        raise TypeError(
            'Expected DataHandle (or Algorithm) properties for {} but got {}'.
            format(t, not_datahandles))
    if not set(dh_inputs).issubset(inputs):
        raise ConfigurationError(
            'Please provide all inputs for {}. The ones missing here are: {}'.
            format(t,
                   set(dh_inputs).difference(inputs)))

    for name, dh in dh_inputs.items():
        expected_type = dh.type()
        if expected_type == "unknown_t":
            continue
        actual_type = inputs[name].type
        if not is_convertible_type(actual_type, expected_type):
            raise ConfigurationError(
                '{}.{} is of type\n    {}\nbut got\n    {}\nfrom {}\n'.format(
                    t.getType(), name, expected_type, actual_type,
                    inputs[name].producer))

    if input_transform:
        input_transform_params = _keyword_params(input_transform)
        assert set(inputs).issubset(
            input_transform_params), 'input signatures do not match'


def _check_public_tool_configuration(cls, tools):
    """Assert public tool properties are configured consistently.

    Every property of ``cls`` whose default value is a ``PublicToolHandle``
    or ``PublicToolHandleArray`` must be configured with tools whose
    ``public`` property is ``True``.

    Args:
        cls (Configurable): Class whose default properties will be queried.
        tools (dict of str to Tool): Tools that have been assigned to
            properties on ``cls``.
    Raises:
        ConfigurationError: If a value in ``tools`` is not public when the
            corresponding key in ``tools`` is a public tool handle on ``cls``.
    """
    for prop, tool in tools.items():
        default = cls.getDefaultProperty(prop)
        if isinstance(default, GaudiKernel.GaudiHandles.PublicToolHandle):
            if not tool.public:
                raise ConfigurationError(
                    ("Assignment to PublicToolHandle property `{}` of "
                     "configurable `{}` must be made with a public tool, "
                     "but tool `{}` is configured as `public=False`.").format(
                         prop, cls, tool))
        elif isinstance(default,
                        GaudiKernel.GaudiHandles.PublicToolHandleArray):
            # If the property is an array, `tool` must be a list
            msg = ", ".join(t._name for t in tool if not t.public)
            if msg:
                raise ConfigurationError((
                    "Assignment to PublicToolHandleArray property `{}` of "
                    "configurable `{}` must be made with a list of public tools, "
                    "but tools {} are configured as `public=False`.").format(
                        prop, cls, msg))


def _ids_from_list(handles):
    """Return a tuple of IDs of the input data handles.

    If a single handle is passed, a one-tuple of its ID is returned.
    """
    if not isinstance(handles, list):
        return (handles.id, )
    else:
        return tuple(h.id for h in handles)


def _gather_extra_inputs(io_dict):
    """Returns a list of extra_inputs contained in io_dict"""
    d = []
    for k, v in io_dict.items():
        if k.endswith(_DATA_DEPENDENCY_KEY_SUFFIX):
            if isinstance(v, list):
                d += [vv.location for vv in v]
            else:
                d.append(v.location)
    return d


def _gather_locations(io_dict):
    """Return the dictionary with all values mapped to their `.location` property.

    Lists values have `.location` accessed on each of their members.
    """
    d = {}
    for k, v in io_dict.items():
        # Ignore pseudo-properties; the actual properties that generated these
        # will be handled as regular properties
        if k.endswith(_DATA_DEPENDENCY_KEY_SUFFIX):
            continue
        if isinstance(v, list):
            d[k] = [vv.location for vv in v]
        else:
            d[k] = v.location
    return d


def _gather_tool_names(tool_dict):
    """Return the dictionary with all values mapped to their `.property_name` property.

    Lists values have `.property_name` accessed on each of their members.
    """
    d = {}
    for k, v in tool_dict.items():
        if isinstance(v, list):
            d[k] = [vv.property_name for vv in v]
        else:
            d[k] = v.property_name
    return d


def is_algorithm(arg):
    """Returns True if arg is of type Algorithm"""
    return isinstance(arg, Algorithm)


def contains_tool(arg):
    """Return True if arg is a Tool or list of Tools, or list of lists of tools etc."""
    return is_tool(arg) or _is_list_of_tools(arg)


def _is_list_of_tools(iterable):
    """Return True if all elements are Tool instances.

    Returns False if the iterable is empty.
    """
    return isinstance(iterable, list) and len(iterable) > 0 and all(
        map(contains_tool, iterable))


def is_tool(arg):
    """Return True if arg is a Tool."""
    return isinstance(arg, Tool)


def _get_data_dependencies(arg):
    """Return the data dependencies of the given argument.

    The argument can be a single object or a nested list of objects.

    returns: flat list of data dependencies
    """
    if isinstance(arg, (tuple, list)):
        return [x for a in arg for x in _get_data_dependencies(a)]
    elif isinstance(arg, dict):
        return [x for v in arg.values() for x in _get_data_dependencies(v)]
    else:
        return arg.data_dependencies()


def _is_input(arg):
    """Return True if arg is something that produces output."""
    try:
        # The semantics are a bit fuzzy here (see LHCb#112); these properties
        # don't *provide* input, but they require input. PyConf currently
        # supports this by 'hoisting' the data dependencies of the property up
        # to the owning Algorithm
        arg = _get_data_dependencies(arg)
    except AttributeError:
        arg = arg if isinstance(arg, list) else [arg]
    return all(is_datahandle(get_output(a)) for a in arg)


def get_output(arg):
    """Return the single output (datahandle) defined on arg.

    Raises an AssertionError if arg is an Algorithm with multiple outputs.

    Can be used to handle mix of datahandles and algs used in reconstruction
    """
    if is_algorithm(arg):
        outputs = list(arg.outputs.values())
        assert len(outputs) == 1, 'Expected a single output on {}'.format(arg)
        try:
            return outputs[0]
        except KeyError:
            # outputs is a dict, so return the first and only value
            return next(iter(outputs.values()))
    return arg


def _pop_inputs(props):
    """Return a dict of all properties that are inputs or lists of inputs."""
    inputs = OrderedDict()
    for k, v in list(props.items()):
        if _is_input(v):
            inp = props.pop(k)
            try:
                # Properties can declare that they 'inject' data dependencies
                # into their owning Algorithms. This is somewhat of a hack, see
                # LHCb#112 before extending this.
                inp = _get_data_dependencies(v)
                # We want to keep the property value for configuration
                props[k] = v
                # Store the dependencies for this property on a pseudo-property
                k += _DATA_DEPENDENCY_KEY_SUFFIX
            except AttributeError:
                pass
            if isinstance(inp, list):
                inp = list(map(get_output, inp))
            else:
                inp = get_output(inp)
            inputs[k] = inp

    return inputs


def _is_optional(key, alg_type):
    if hasattr(alg_type, "aggregates"):  #TODO review this logic
        return key in alg_type.aggregates
    return False


def _pop_tools(props):
    """Return a dict of all properties that are Tools.

    Raises a TypeError if a tool is not wrapped by our Tool class (e.g. a bare
    Configurable).
    """
    tools = OrderedDict()
    for k, v in list(props.items()):
        if contains_tool(v):
            tools[k] = props.pop(k)
        elif _is_configurable_tool(v):
            raise TypeError(" ".join((
                "Please either wrap your configurable with the PyConf.components.Tool wrapper",
                "or import it from PyConf.Tools if possible")))
    return tools


def _get_and_check_properties(t, props):
    """Return an OrderedDict of props.

    Raises a ConfigurationError if any of the keys in props are not
    properties of the Algorithm/Tool t.
    """
    default_props = getDefaultProperties(t)
    missing = set(props) - default_props.keys()
    if missing:
        raise ConfigurationError('{} are not properties of {}'.format(
            missing, t.getType()))
    return OrderedDict(props)


def _format_property(name, value, max_len=100, placeholder="[...]"):
    assert max_len > 15, "max_len should be at least 15"
    # If it's a functor, display it's pretty representation, otherwise convert
    # to str
    try:
        value = value.code_repr()
    except AttributeError:
        value = str(value)
    if len(value) > (max_len + len(placeholder)):
        pivot = max_len // 2
        value = "{}{}{}".format(value[:pivot], placeholder, value[-pivot:])
    return '{} = {}'.format(name, value)


def _dict_add(x, y):
    """Update x with y only if not overwriting existing values."""
    would_overwrite = set(y).intersection(x)
    if not would_overwrite:
        x.update(y)
    return would_overwrite


class Algorithm(object):
    """An immutable wrapper around a Configurable for a Gaudi algorithm.

    An Algorithm is immutable after instantiation, so all non-default
    properties and inputs must be defined upfront. A name can be given but is
    used only as a label, not an identifier.

    Output locations are defined dynamically using a combination of the
    algorithm class name and a hash that is generated based on the algorithm
    properties and inputs. Instanting a new Algorithm that is configured
    identically to a previous Algorithm will result in the same first instance
    being returned.

    Importing Configurable classes from the `PyConf.Algorithms` module will
    return a version wrapped by this class.
    """
    _algorithm_store = dict()

    _readonly = False

    def __new__(cls,
                alg_type,
                name=None,
                outputs=None,
                input_transform=None,
                output_transform=None,
                is_barrier=False,
                allow_duplicate_instances_with_distinct_names=False,
                weight=1,
                average_eff=0.5,
                **kwargs):
        """
        Args:
            alg_type: the configurable you want to be instantiated
            name: The name to be used for the algorithm. A hash will be appended.
            input_transform: Transforms inputs to properties.
                A function to transform inputkeys/locations into actual properties.
                In case your input locations are translated into properties differently than
                {'input_key' : '<location>'},
                for instance in LoKi Functors:
                {'CODE' : 'SIZE('<location>'},
                you can specify the behaviour in input_transform.
                The arguments for input_transform are all the input keys, and the output is a
                dictionary of the resulting actual properties:
                Example: Some alg has two inputs called 'Input1' and 'Input2', and needs these
                locations to go into the 'CODE' property to check for the bank sizes
                def input_transform_for_some_alg(Input1, Input2):
                    return {'CODE' : 'SIZE({}) > 5 & SIZE({}) < 3'.format(Input1, Input2)}

                The function should not have side affects.

            outputs (dict): Complete set of exposed outputs.
                This is not mandatory if the `alg_type` corresponds to an
                Algorithm where all outputs are declared via DataHandles.

                The dictionary can look like
                    {
                        'MyLocation': None,
                        'MyOutput': forced_location('/Event/Output'),
                    }
                where the actual location for 'MyLocation' would be
                automatically set by the framework whereas the exact
                location of 'MyOutput' is forced by the caller. Forcing
                an exact location is only needed in special cases and is
                not recommended for general use as it may lead to conflicts.

                If `output_transform` is not given, the dictionary keys
                are used as the properties to set for the Configurable
                `alg_type`.  When some of keys in the dict does not
                correspond to a string (or data handle) property of the
                Configurable, it is mandatory to also pass `output_transform`
                so that the exposed outputs are transformed to properties.

            output_transform (func): Transforms outputs to properties.
                The parameter names of `output_transform` must match the
                keys of `outputs`. The function should not have any side
                effects and should return a dict of the properties to be
                set to the Configurable.

                The function is not allowed to modify the string that is
                interpreted as the location. It may only define how a
                location is put into the job options. For example,

                    # Not allowed as it changes the output path
                    def transform(Output):
                        return {'Output': Output + '/Particles'}

                    # OK, the location is used in a list:
                    def transform(Particles):
                        return {'Output': Particles,
                                'ExtraOutputs': [Particles]}

            is_barrier:
                is this algorithm a 'barrier algorithm', i.e. should it be declared to the
                control flow manager as such, i.e. should its data dependecies force all producers
                to be executed first?
            weight:
                A measure for how expensive the execution of this algorithm is.
                This weight with respect to weights of other algorithm in an
                executable sequence will affect the order in which they are
                executed, if there is a freedom of choice.
            average_eff:
                If the algorithm makes a control flow decision (e.g. a
                MultiTransformerFilter or the global event cut), setting the
                average efficiency (i.e.  the probability of the algorithm
                passing) helps to find an efficient order of execution.
            kwargs:
                All the properties you want to set in the configurable (besides outputs)
                Every kwarg that has a DataHandle as value will be interpreted as input and
                the list of inputs needs to match the signature of 'input_transform', in case you provide it.
                Every input needs to be provided in the kwargs
                Every kwarg that has a Tool as value will be interpreted as private tool of this instance.
                Tools that have some kind of TES interation (or tools of these tools) need to be
                specified, otherwise the framework cannot know what locations to set.

        returns:
            instance of type Algorithm. It can be taken from the class store
            in case it has already been instantiated with the same configuration
        """
        alg_type_name = alg_type.getType()
        alg_properties = getDefaultProperties(alg_type)

        if not _is_configurable_algorithm(alg_type):
            raise TypeError(f'cannot declare an Algorithm with {alg_type}, '
                            f'which is of type {alg_type_name}')

        #INPUTS ###########
        # TODO when everything is functional, input keys can be checked!
        _inputs = _pop_inputs(kwargs)
        _check_input_integrity(alg_type, _inputs, kwargs, input_transform)

        #OUTPUTS ##########
        # The "shape" of the outputs participates in the algorithm
        # identity. This includes the list of exposed outputs and also
        # any forced locations.
        if output_transform and not outputs:
            warnings.warn(
                "passing only output_transform without output"
                "is deprecated", DeprecationWarning)
            outputs = {k: None for k in _keyword_params(output_transform)}
        known_alg_outputs = getConfigurableOutputs(alg_type)
        if not outputs:
            outputs = {k: None for k in known_alg_outputs}
        else:
            if not isinstance(outputs, dict):
                raise TypeError("outputs must be a dict")

        # The output_transform may define behaviour, so it also must
        # participate in the algorithm identity. We call it with some
        # dummy locations and the hash of the result goes into the ID.
        dummy_output_props = {k: k + 'Dummy' for k in outputs}
        if output_transform:
            try:
                dummy_output_props = output_transform(**dummy_output_props)
            except TypeError:
                raise TypeError("parameters of output_transform "
                                "do not match output.keys()")
            if not isinstance(dummy_output_props, dict):
                raise TypeError("output_transform must return a dict")

        #TOOLS ##############
        _tools = _pop_tools(kwargs)
        _check_public_tool_configuration(alg_type, _tools)

        #PROPERTIES ############
        _properties = _get_and_check_properties(alg_type, kwargs)

        #HASH ###########
        _id = cls._calc_id(
            alg_type.getType(),
            _properties,
            _inputs,
            outputs,
            _tools,
            input_transform,
            output_transform=dummy_output_props)

        # given the idenity, we can now determine the unique name
        _name = _get_unique_name(name, alg_type.getType(), _id)

        # return the class if it exists already, otherwise create it
        # note that we _may_ have multiple algorithms with the same identity, but distinct names
        instances = cls._algorithm_store.get(_id, [])
        if instances:
            # to explicitly keep the previous behavior, if no name is specified, and there already is an
            # existing instance, then 'just' return that existing instance... regardless of the name(s)
            # FIXME: it is not clear that this is the right thing to do -- it is just done
            #        this way here to maintain backwards compatibility
            #        An alternative way would be to require the names to match as well, i.e.
            #            instance = next(filter(lambda i: i._name == _name, instances), None)
            #        instead...
            instance = next(
                filter(lambda i: not name or i._name == _name, instances),
                None)
            if instance:
                return instance
            if not allow_duplicate_instances_with_distinct_names:
                raise ConfigurationError(
                    f"Not allowed to instantiate {alg_type_name} multiple times with different "
                    f"names, {_safe_name(_name)} versus {[i.name for i in instances]}, but otherwise identical properties."
                )

        instance = super(Algorithm, cls).__new__(cls)
        instance._id = _id
        instance._weight = weight
        instance._average_eff = average_eff
        instance._alg_type = alg_type
        instance._name = _name
        instance._inputs = _inputs
        instance._input_transform = input_transform
        instance._outputs_shape = outputs
        instance._output_transform = output_transform
        instance._properties = _properties
        instance._tools = _tools
        # Allen wrapper algorithms have a property to flag whether they must be treated as Barriers
        instance._is_barrier = is_barrier or alg_properties.get(
            'hasOptionals', False)

        if any(not isinstance(v, (type(None), force_location))
               for v in outputs.values()):
            raise ValueError('outputs values must be None or force_location')

        explicitly_set_outputs = set(kwargs).intersection(outputs)
        if explicitly_set_outputs:
            raise ConfigurationError(
                f"Cannot set output properties {explicitly_set_outputs}"
                " explicitly. Use the outputs property instead.")

        # Verify that we would be able to set the Configurable properties
        unknown_props = set(dummy_output_props).difference(alg_properties)
        if unknown_props:
            raise ConfigurationError(
                f"{unknown_props} are not properties of "
                f"{alg_type_name}. Check outputs and output_transform.")
        # TODO check that types are compatible, too

        # FIXME I can force a location and then transform it so the next
        # check is not okay
        forced_outputs = [
            key for key, output in outputs.items()
            if isinstance(output, force_location)
        ]
        not_str = [
            key for key in forced_outputs if key in alg_properties
            and not isinstance(alg_properties[key], (GaudiDataHandle, str))
        ]
        if not_str:
            raise ValueError(
                "cannot force locations to non-string/non-DataHandle "
                "properties ({}) for {}".format(
                    {k: type(alg_properties[k])
                     for k in not_str}, alg_type_name))

        # `outputs` is the complete set of exposed outputs so we might
        # end up hiding some of the algorithm's write data handles.
        ignored_outputs = set(known_alg_outputs).difference(outputs)
        if ignored_outputs:
            raise ConfigurationError(
                f"Some outputs of {alg_type_name} are hidden: " +
                ", ".join(ignored_outputs))

        # Set _outputs and expose them via attributes
        instance._outputs = {
            k: DataHandle(instance, k, v)
            for k, v in outputs.items()
        }
        for key, src in instance._outputs.items():
            setattr(instance, key, src)

        instance._readonly = True
        if instance._id in cls._algorithm_store:
            cls._algorithm_store[instance._id].append(instance)
        else:
            cls._algorithm_store[instance._id] = [instance]
        return instance  # the newly created instance

    @staticmethod
    def _calc_id(typename, props, inputs, outputs, tools, input_transform,
                 output_transform):
        if input_transform is not None:
            props_from_transform = input_transform(**_gather_locations(inputs))
        else:
            props_from_transform = {}
        inputs_repr = tuple(
            (key, _ids_from_list(handles)) for key, handles in inputs.items())
        tools_repr = tuple(
            (key, _ids_from_list(tool)) for key, tool in tools.items())
        return hash_object((
            typename,
            props,
            inputs_repr,
            outputs,
            tools_repr,
            props_from_transform,
            output_transform,
        ))

    @property
    def average_eff(self):
        return self._average_eff

    @property
    def inputs(self):
        return self._inputs

    @lru_cache(None)
    def all_inputs(self, include_optionals=True):
        """Return the set of all direct and indirect inputs of this algorithm."""
        inputs = set()
        for k, inp in self.inputs.items():
            if not include_optionals and _is_optional(k, self.type):
                continue
            if isinstance(inp, list):
                inputs.update(set(inp))
                # Traverse up the data flow tree
                for sinp in inp:
                    inputs.update(sinp.producer.all_inputs(include_optionals))
            else:
                inputs.add(inp)
                # Traverse up the data flow tree
                inputs.update(inp.producer.all_inputs(include_optionals))
        # Add inputs to tools held by this algorithm
        for tool_config in self.tool_inputs().values():
            for inp in filter(None, tool_config.values()):
                # also for case if input is like a list (e.g. MergingTransformer)
                for ele in [inp] if type(inp) != list else inp:
                    inputs.add(ele)
                    inputs.update(ele.producer.all_inputs(include_optionals))
        return inputs

    @lru_cache(None)
    def all_producers(self, include_optionals=True):
        """Return the set of all producers in this algorithm's data dependency tree including self."""
        return {dh.producer
                for dh in self.all_inputs(include_optionals)} | {self}

    @property
    def outputs(self):
        return self._outputs

    @property
    def properties(self):
        return self._properties

    @property
    def name(self):
        return _safe_name(self._name)

    @property
    def fullname(self):
        return self.typename + '/' + self.name

    @property
    def id(self):
        return self._id

    @property
    def type(self):
        return self._alg_type

    @property
    def is_barrier(self):
        return self._is_barrier

    @property
    def tools(self):
        return self._tools

    @property
    def typename(self):
        return _safe_name(self.type.getType())

    def __repr__(self):
        return self.fullname

    def __hash__(self):
        return self._id

    def __eq__(self, other):
        return isinstance(other, Algorithm) and self.id == other.id

    def tool_inputs(self):
        """Return the transitive closure of tool inputs."""

        def _inputs_from_tool(tool):
            if isinstance(tool, list):
                inputs = OrderedDict()
                for t in tool:
                    inputs.update(_inputs_from_tool(t))
            else:
                inputs = {tool.name(self.name): tool.inputs}
                for _tool in tool.tools.values():
                    inputs.update(_inputs_from_tool(_tool))
            return inputs

        all_inputs = OrderedDict()
        for tool in self.tools.values():
            all_inputs.update(_inputs_from_tool(tool))
        return all_inputs

    @lru_cache(None)
    def configuration(self):
        config = dataflow_config()

        #children
        for inputs in self.inputs.values():
            inputs = inputs if isinstance(inputs, list) else [inputs]
            for inp in inputs:
                config.update(inp.producer.configuration())

        for tool in self.tools.values():
            tool = tool if isinstance(tool, list) else [tool]
            for t in tool:
                config.update(t.configuration(self.name))

        #props
        cfg = config[(self.type, self.name)] = self.properties.copy()

        #io
        input_dict = _gather_locations(self.inputs)
        output_dict = _gather_locations(self.outputs)

        # get the functor data dependencies to add them to ExtraInputs
        extra_inputs = _gather_extra_inputs(self.inputs)
        if extra_inputs:
            # ExtraInputs is also a property so we need to be careful to not
            # overwrite it if its there, but only extend it.
            cfg['ExtraInputs'] = cfg.get('ExtraInputs', []) + extra_inputs
            # I don't think duplicates would matter but it's easy to declutter
            # here so let's quickly do it
            cfg['ExtraInputs'] = sorted(set(cfg['ExtraInputs']))

        if self._input_transform:
            input_dict = self._input_transform(**input_dict)

        if self._output_transform:
            output_dict = self._output_transform(**output_dict)

        tools_dict = _gather_tool_names(self.tools)

        if conflict := _dict_add(cfg, input_dict):
            raise ConfigurationError(f"input properties try to overwrite "
                                     f"already set properties: {conflict}")
        if conflict := _dict_add(cfg, output_dict):
            raise ConfigurationError(f"output properties try to overwrite "
                                     f"already set properties: {conflict}")
        if conflict := _dict_add(cfg, tools_dict):
            raise ConfigurationError(f"tool properties try to overwrite "
                                     f"already set properties: {conflict}")
        return config

    def _graph(self, graph, visited=None):
        """Add our data flow as a `pydot.Subgraph` to graph.

        The `visited` set keeps a flat record of components already drawn in
        `graph` and all subgraphs. This is done to prevent re-drawing
        components that have already been drawn, which can occur due to diamond
        structures in the dependency tree.

        Args:
            graph (pydot.Graph): Graph to draw this component in to.
            visited (set): Names of components already drawn in `graph`. If
            `None`, an empty set is used.
        """
        if visited is None:
            visited = set()
        if self.fullname in visited:
            return
        else:
            visited.add(self.fullname)

        #inner part ########
        own_name = html_escape(self.fullname)
        sg = pydot.Subgraph(graph_name='cluster_' + own_name)
        sg.set_label('')
        sg.set_bgcolor(_FLOW_GRAPH_NODE_COLOUR)

        props = self.properties
        # Include output locations when they define algorithm behaviour
        forced_outputs = {
            key: str(output)
            for key, output in self._outputs_shape.items()
            if isinstance(output, force_location)
        }
        if forced_outputs:
            props = dict(list(props.items()) + list(forced_outputs.items()))
        props_str = '<BR/>'.join(
            html_escape(_format_property(k, v)) for k, v in props.items())
        label = ('<<B>{}</B><BR/>{}>'.format(own_name, props_str
                                             or 'defaults-only'))
        # Protect against names that may contain colons (e.g. from C++
        # namespaces in algorithm/tool names)
        # https://github.com/pydot/pydot/issues/38
        gnode = pydot.Node(
            '"{}"'.format(own_name), label=label, shape='plaintext')
        sg.add_node(gnode)

        #IO for the inner part
        for name in self.inputs:
            input_id = html_escape('{}_in_{}'.format(self.fullname, name))
            node = pydot.Node(
                '"{}"'.format(input_id),
                label=html_escape(name),
                fillcolor=_FLOW_GRAPH_INPUT_COLOUR,
                style='filled')
            edge = pydot.Edge(gnode, node, style='invis', minlen='0')
            sg.add_node(node)
            sg.add_edge(edge)
        for name in self.outputs:
            output_id = html_escape('{}_out_{}'.format(self.fullname, name))
            node = pydot.Node(
                '"{}"'.format(output_id),
                label=html_escape(name),
                fillcolor=_FLOW_GRAPH_OUTPUT_COLOUR,
                style='filled')
            edge = pydot.Edge(gnode, node, style='invis', minlen='0')
            sg.add_node(node)
            sg.add_edge(edge)

        # tool inputs
        tool_inputs = self.tool_inputs()

        for toolname, inputs in tool_inputs.items():
            for name in inputs:
                input_id = html_escape('{}_in_{}'.format(toolname, name))
                label = ('<<B>{}</B><BR/>from {}>'.format(
                    html_escape(name), html_escape(toolname)))
                node = pydot.Node(
                    '"{}"'.format(input_id),
                    label=label,
                    fillcolor=_FLOW_GRAPH_INPUT_COLOUR,
                    style='filled')
                edge = pydot.Edge(
                    input_id, own_name, style='invis', minlen='0')
                sg.add_node(node)
                sg.add_edge(edge)

        graph.add_subgraph(sg)

        # external links #######
        for key, handles in self.inputs.items():
            handles = handles if isinstance(handles, list) else [handles]
            for handle in handles:
                edge = pydot.Edge(
                    html_escape('{}_out_{}'.format(handle.producer.fullname,
                                                   handle.key)),
                    html_escape('{}_in_{}'.format(self.fullname, key)))
                graph.add_edge(edge)
                handle.producer._graph(graph, visited)

        for toolname, inputs in tool_inputs.items():
            for name, handles in inputs.items():
                handles = handles if isinstance(handles, list) else [handles]
                for handle in handles:
                    edge = pydot.Edge(
                        html_escape('{}_out_{}'.format(
                            handle.producer.fullname, handle.key)),
                        html_escape('{}_in_{}'.format(toolname, name)))
                    graph.add_edge(edge)
                    handle.producer._graph(graph, visited)

    def plot_dataflow(self):
        """Return a `pydot.Dot` of the data flow defined by this Algorithm."""
        top = pydot.Dot(graph_name=self.fullname, strict=True)
        top.set_node_defaults(shape='box')
        self._graph(top)
        return top

    def __setitem__(self, k, v):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__setitem__(self, k, v)

    def __setattr__(self, k, v):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__setattr__(self, k, v)

    def __delitem__(self, i):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__delitem__(self, i)

    def __delattr__(self, a):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__delattr__(self, a)


class Tool(object):
    """An immutable wrapper around a Configurable for a Gaudi tool.

    A Tool is immutable after instantiation, so all non-default properties and
    inputs must be defined upfront. A name can be given but is used only as a
    label, not an identifier.

    Instanting a new Tool that is configured identically to a previous
    Tool will result in the same first instance being returned.

    Importing Configurable classes from the `PyConf.Tools` module will return a
    version wrapped by this class.
    """

    _tool_store = dict()

    _readonly = False

    def __new__(cls, tool_type, name=None, public=False, **kwargs):
        """
        Args:
            tool_type: the configurable you want to be instantiated
            name: The name to be used for the Tool. A hash will be appended.
            public: True if the tool will belong to the ToolSvc, False
                otherwise (i.e. the tool is 'private'; unique Configurable
                instances will be given to individual algorithms).
            kwargs:
                All the properties you want to set in the configurable (besides outputs)
                Every kwarg that has a DataHandle as value will be interpreted as input.
                Every input needs to be provided in the kwargs.
                Every kwarg that has a Tool as value will be interpreted as private tool of this instance.
                Tools that have some kind of TES interation (or tools of these tools) need to be
                specified, otherwise the framework cannot know what locations to set.
        returns:
            instance of type Tool, maybe taken from the tool store (in case of same configuration)
        """
        if not _is_configurable_tool(tool_type):
            raise TypeError(
                'cannot declare a Tool with {}, which is of type {}'.format(
                    tool_type, tool_type.getGaudiType()))

        #inputs
        _inputs = _pop_inputs(kwargs)
        _check_input_integrity(tool_type, _inputs, kwargs)

        #tools
        _tools = _pop_tools(kwargs)
        _check_public_tool_configuration(tool_type, _tools)

        #properties
        _properties = _get_and_check_properties(tool_type, kwargs)

        #calculate the id, this determines whether we can take an already created instance
        identity = cls._calc_id(tool_type.getType(), _properties, _inputs,
                                _tools, public)

        name = name or tool_type.getType()

        if public:
            try:
                instance = cls._tool_store[name]
                if instance._id != identity:
                    raise ConfigurationError(
                        f"Public Tool {name} already configured with different properties"
                    )
                return instance

            except KeyError:
                pass

        instance = super(Tool, cls).__new__(cls)
        instance._id = identity
        instance._tool_type = tool_type
        instance._name = name
        instance._public = public
        instance._inputs = _inputs
        instance._properties = _properties
        instance._tools = _tools
        instance._readonly = True

        if public:
            cls._tool_store[name] = instance

        return instance

    @staticmethod
    def _calc_id(typename, props, inputs, tools, public):
        inputs_repr = tuple(
            (key, _ids_from_list(handles)) for key, handles in inputs.items())
        tools_repr = tuple(
            (key, _ids_from_list(tool)) for key, tool in tools.items())
        return hash_object((
            typename,
            props,
            inputs_repr,
            tools_repr,
            public,
        ))

    @property
    def public(self):
        """Return True if this tool is public.

        A private tool is configured uniquely in association with a specific
        algorithm, to which it belongs.
        """
        return self._public

    @property
    def private(self):
        """Return True if this tool is private.

        A public tool is configured uniquely in association with the ToolSvc,
        to which it belongs.
        """
        return not self.public

    @property
    def inputs(self):
        return self._inputs

    @property
    def properties(self):
        return self._properties

    @property
    def id(self):
        return self._id

    @property
    def tools(self):
        return self._tools

    @property
    def property_name(self):
        """Return name used in component properties to refer to Tool."""
        return (self.typename + '/' + self._name +
                (":PUBLIC" if self.public else ""))

    def name(self, parent=None):
        """Configuration name of the tool itself.

        If the tool is public, `parent` is ignored, and the name will be
        prepended with `ToolSvc`. Otherwise, the name will be prepended
        with `parent` or an exception is raised if it is None.

        """
        if not self.public and parent is None:
            raise ConfigurationError("Parent unspecified for a private tool")

        return "{}.{}".format("ToolSvc" if self.public else parent, self._name)

    @property
    def type(self):
        return self._tool_type

    @property
    def typename(self):
        return self.type.getType()

    def __repr__(self):
        return 'Tool({})'.format(self.typename)

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return isinstance(other, Tool) and self.id == other.id

    @lru_cache(None)
    def configuration(self, parent=None):
        name = self.name(parent)
        config = dataflow_config()

        for inputs in self.inputs.values():
            inputs = inputs if isinstance(inputs, list) else [inputs]
            for inp in inputs:
                config.update(inp.producer.configuration())

        for tool in self.tools.values():
            tool = tool if isinstance(tool, list) else [tool]
            for t in tool:
                config.update(t.configuration(name))

        cfg = config[(self.type, name)] = self.properties.copy()
        input_dict = _gather_locations(self.inputs)
        tools_dict = _gather_tool_names(self.tools)
        # get the functor data dependencies to add them to ExtraInputs
        extra_inputs = _gather_extra_inputs(self.inputs)
        if extra_inputs:
            # ExtraInputs is also a property so we need to be careful to not
            # overwrite it if its there, but only extend it.
            cfg['ExtraInputs'] = cfg.get('ExtraInputs', []) + extra_inputs
            # I don't think duplicates would matter but it's easy to declutter
            # here so let's quickly do it
            cfg['ExtraInputs'] = sorted(set(cfg['ExtraInputs']))

        if conflict := _dict_add(cfg, input_dict):
            raise ConfigurationError(f"input properties try to overwrite "
                                     f"already set properties: {conflict}")
        if conflict := _dict_add(cfg, tools_dict):
            raise ConfigurationError(f"tool properties try to overwrite "
                                     f"already set properties: {conflict}")
        return config

    def __setitem__(self, k, v):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__setitem__(self, k, v)

    def __setattr__(self, k, v):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__setattr__(self, k, v)

    def __delitem__(self, i):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__delitem__(self, i)

    def __delattr__(self, a):
        if self._readonly:
            raise ConfigurationError(
                'cannot change member after initialization')
        return object.__delattr__(self, a)


def setup_component(alg,
                    instance_name=None,
                    package_name='Configurables',
                    require_IOVLock=False,
                    **kwargs):
    """Return an instance of the class alg.

    If alg is a string, import the named class from `package_name`.

    If `require_IOVLock` is True, a dependency on `/Event/IOVLock` is added to the
    instance's ExtraInputs property.

    Additional keyword arguments are forwarded to the alg constructor.
    """
    if isinstance(alg, str):
        alg = getattr(importlib.import_module(package_name), alg)

    name = instance_name or getattr(alg, "DefaultedName", None) or alg.__name__
    try:
        old_props = setup_component.config[name]
        if kwargs != old_props and name != "EventDataSvc":
            from pprint import pformat
            raise ConfigurationError(
                f"Attempt to configure '{name}' with different config:\n\n"
                f"{pformat(kwargs)}\n\nwas\n\n{pformat(old_props)}")
    except KeyError:
        setup_component.config[name] = kwargs

    instance = alg(name, **kwargs)
    if (require_IOVLock and hasattr(instance, 'ExtraInputs')
            and '/Event/IOVLock' not in instance.ExtraInputs):
        instance.ExtraInputs.append('/Event/IOVLock')  # FIXME
    return instance


setup_component.config = {}
