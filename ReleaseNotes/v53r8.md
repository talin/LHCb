2022-04-14 LHCb v53r8
===

This version uses
Detector [v1r1](../../../../Detector/-/tags/v1r1),
Gaudi [v36r5](../../../../Gaudi/-/tags/v36r5) and
LCG [101](http://lcginfo.cern.ch/release/101/) with ROOT 6.24.06.

This version is released on `master` branch.
Built relative to LHCb [v53r7](/../../tags/v53r7), with the following changes:

### New features ~"new feature"

- ~Muon ~"Event model" | SIMD field for flags / Adapt MuonPID for it, !3468 (@decianm)
- ~"Event model" | Added new event model object PrimaryVertexContainer, !3459 (@wouter)
- ~Persistency | New persistency model for packing, !3268 (@sesen) [#151,Moore#248,Moore#354,lhcb-dpa/project#120] :star:


### Fixes ~"bug fix" ~workaround

- ~Tracking | Update of LHCb::HitPattern, !3500 (@wouter)
- ~Build | Make sure Detector is added to ROOT_INCLUDE_PATH and PATH, !3508 (@clemenci) [Moore#417]
- ~Build | Move legacy MuonDet files to a dedicated subdir to avoid unused file warning, !3503 (@clemenci)
- Fixed ref file for dd4hep_sharedconditions test, !3504 (@sponce)
- Fixed warning in DD4hep mode ofr RichDetectors/RichX.h, !3501 (@sponce)


### Enhancements ~enhancement

- ~Tracking | Streamline hits used by pattern recognition, !3473 (@gunther)
- ~Persistency | Allow event sizes counters in raweventcombiner, !3497 (@nskidmor)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Calo ~Simulation | Readded a parser for Properties Calo::CellID, !3499 (@sponce)
- ~RICH | Remove RICH DD4HEP include hack, now that Detector is available in all builds, !3487 (@jonrob)
- Make sure mdfwriter test does not fail on a second run, !3509 (@clemenci)
- Suppress clang warnings, !3506 (@jonrob)
- Remove unused header (follow up !3486), !3505 (@rmatev)
- Removed duplication of channelIDs between LHCb and Detector. Kept Detector ones, !3486 (@sponce)
- Refactoring of IGeometryInfo, !3455 (@bcouturi)
