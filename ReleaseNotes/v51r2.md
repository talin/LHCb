2020-10-19 LHCb v51r2
===

This version uses
Gaudi [v34r1](../../../../Gaudi/-/tags/v34r1) and
LCG [97a](http://lcginfo.cern.ch/release/97a/) with ROOT 6.20.06.

This version is released on `master` branch.
Built relative to LHCb [v51r1](../-/tags/v51r1), with the following changes:

### New features ~"new feature"

- ~Decoding ~RICH | Add support for realistic RICH PMT encoding and decoding, !2664 (@jonrob) :star:
- ~Decoding ~Monitoring | Add decoding for SciFi NZS dataformat, !2648 (@legreeve) :star:
- ~Tracking | Added specialized similarity transform, !2763 (@ldufour) :star:
- ~Calo | Continuation of  !2267 (Add Area accessor for calorimeter parts ), !2745 (@ahennequ)
- ~Composites ~Functors ~"Event model" ~Utilities | Changes supporting the new ThOr::Combiner<T>, !2668 (@nnolte) [#96,#97] :star:


### Fixes ~"bug fix" ~workaround

- ~Tracking ~Utilities | Fix undefined behaviour sanitiser errors (avx2 + LHCbMath/Similarity), !2721 (@olupton)
- ~Calo | Size inconsistency fix of calo objects, !2798 (@mveghel)
- ~RICH ~Conditions ~Utilities | Minor fixes to restore builds using VE SIMD abstraction layer, !2762 (@jonrob)
- ~Conditions | FTDet: Fix platform diffs due to floating point bug, !2679 (@sesen) :star:
- ~Build | Add missing inline to please gcc10, !2717 (@cattanem)


### Enhancements ~enhancement

- ~Tracking | Template function that finds sectors in UT, !2673 (@decianm)
- ~Tracking ~"Event model" | Make the sizes of Long/Velo/Upstream track container flexible, !2733 (@peilian) :star:
- ~Tracking ~"Event model" | Make the size of Pr::Seeding::Tracks container flexible, !2702 (@peilian) :star:
- ~Calo | Added functionality for CaloHypos for Rec!2216, !2787 (@mveghel)
- ~Calo | Improve computation of X,Y position of Calo clusters., !2678 (@graven)
- ~Calo | Additional Calo cluster changes required to fully migrate to v2 clusters, add v2 hypos, !2582 (@graven)
- ~Functors | Python 3 compatibility for functor division, !2694 (@rmatev) [LHCBPS-1858]
- ~Core ~Conditions ~Utilities ~Build | ARM support, !2756 (@ahennequ) :star:
- ~Conditions | Various fixes for DD4Hep build, !2690 (@sponce)
- ~Utilities | Add `enumerate`, !2768 (@graven)
- ~Build | Add gcc10 platforms to PlatformInfo, !2726 (@cattanem)
- ~Build | Remove trailing whitespaces in new references, !2693 (@rmatev)
- ~Build | Fix Graphviz cmake module, !2633 (@rmatev)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Decoding ~"Event model" ~Conditions | Remove deprecated code, !2749 (@graven)
- ~Tracking ~Utilities | Similarity: remove bounds-checking in non-debug mode for GSL3, !2696 (@graven)
- ~Calo | Simplify ICaloFutureElectron interface + miscellaneous cleanup, !2782 (@graven)
- ~Calo | Remove unused code in CaloFutureUtils, !2743 (@graven)
- ~Calo | Dropped unused CaloDigitFilterTool, !2729 (@sponce)
- ~Calo | Cleanup CaloFutureUtils, !2701 (@graven)
- ~Calo | Changes to CaloClusters v2, !2672 (@graven)
- ~Calo | Cleanup CaloUtils, !2667 (@graven)
- ~Calo ~Functors ~"Event model" | Remove L0 related code, !2735 (@pkoppenb) :star:
- ~RICH | Fix clang10 compilation warning, !2707 (@cattanem)
- ~"Event model" | Remove obsolete SwimmingEvent, !2719 (@cattanem) [LHCBPS-1867]
- ~"MC checking" | MCReconstructible: use toolhandle, !2753 (@graven)
- ~"MC checking" ~Simulation | Modernize SimComponents, !2520 (@graven)
- ~Core | Prefer Counters to accumulators in user code, !2803 (@sponce)
- ~Core | Tweak DeterministicMixer to work with gsl3, !2796 (@graven)
- ~Core | Modernize deterministic mixer code, !2794 (@graven)
- ~Core | New round of cleanup of old style counters, !2754 (@sponce)
- ~Core | Modernized DQFilter and reduced drastically use of EventCountAlg, !2720 (@sponce)
- ~Core | QMTest of CopyInputStream, !2639 (@axu)
- ~Core ~Utilities | Adapted code to newest gsl::span, !2695 (@sponce)
- ~Conditions | Update test to latest physics constants in Gaudi, !2755 (@clemenci)
- ~Utilities | Prefer GSL version of make_span over custom version, !2760 (@graven)
- ~Utilities | Allow LHCb::UT::UTDAQ::findSectors to (implicitly) deduce the template arguments, !2680 (@graven)
- ~Simulation | VisPrimVertTool: Prefer DataHandle over using evtSvc(), !2750 (@graven)
- ~Build | Clean up dependency of functor cache on GaudiConfig2, !2692 (@rmatev)
- ~Build | Fix cppyy incompatibilities introduced with ROOT 6.22, !2638 (@cattanem) [ROOT-10769]
- Remove obsolete configurables test, !2764 (@cattanem) [#100]
- Adapt to new Gaudi Monitoring, !2761 (@sponce)
- Towards dropping old counters, !2725 (@sponce)
- Migrate to Gaudi/Property.h, !2674 (@jonrob)


### Documentation ~Documentation

- Add warning about fromSignal flag in MCParticle, !2802 (@chasse)
