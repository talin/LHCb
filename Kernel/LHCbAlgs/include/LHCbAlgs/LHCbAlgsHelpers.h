/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <array>
#include <cstddef>
#include <string>
#include <tuple>
#include <utility>

namespace LHCb::Algorithm {

  template <typename Base, std::size_t... index>
  auto NamesHelper( std::array<std::string, sizeof...( index )> const& names, std::index_sequence<index...> ) {
    return std::make_tuple( typename Base::KeyValue{names[index], ""}... );
  }

  template <typename Base, std::size_t... index>
  auto IndexingNamesHelper( std::string const& name, std::index_sequence<index...> ) {
    return std::make_tuple( typename Base::KeyValue{name + std::to_string( index + 1 ), ""}... );
  }

  template <typename InputType, typename OutputType, typename Base>
  struct IOHelper {
    static const std::size_t size = std::tuple_size_v<OutputType>;
    static auto              OutputNames( std::array<std::string, size> const& names ) {
      return NamesHelper<Base>( names, std::make_index_sequence<size>{} );
    }
    static auto InputNames( std::array<std::string, size> const& names ) {
      return NamesHelper<Base>( names, std::make_index_sequence<size>{} );
    }
    static auto IndexedOutputNames( std::array<std::string, size> const& names ) {
      return IndexingNamesHelper<Base>( "Output", std::make_index_sequence<size>{} );
    }
    static auto IndexedInputNames( std::array<std::string, size> const& names ) {
      return IndexingNamesHelper<Base>( "Input", std::make_index_sequence<size>{} );
    }
  };
} // namespace LHCb::Algorithm
