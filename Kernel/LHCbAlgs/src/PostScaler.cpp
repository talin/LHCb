/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Gaudi/Accumulators.h"
#include "Gaudi/Property.h"
#include "GaudiKernel/RndmGenerators.h"
#include "Kernel/IEventCounter.h"
#include "LHCbAlgs/FilterPredicate.h"
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : PostScaler
//
// 2003-03-16 : Gloria Corti
//-----------------------------------------------------------------------------

/**
 *  Allows to prescale events at a given rate suing a random number (as Prescaler.cpp)
 *  or to force the fraction of accepted events to be as given by options (or both).
 *
 *  @author P. Koppenburg
 *  @date   2004-06-15
 */
struct PostScaler final : LHCb::Algorithm::FilterPredicate<bool()> {

  PostScaler( const std::string& name, ISvcLocator* pSvcLocator ) : FilterPredicate{name, pSvcLocator} {
    m_percentPass.verifier().setBounds( 0.0, 100.0 );
  }
  bool operator()() const override;

  mutable Gaudi::Accumulators::Counter<Gaudi::Accumulators::atomicity::full, unsigned long long> m_nEvents{
      this, "Event Accepted"}; /// Counter of events accepted
  mutable Gaudi::Accumulators::Counter<Gaudi::Accumulators::atomicity::full, unsigned long long> m_nEventsAll{
      this, "Event Filtered"}; /// Counter of events entering

  Gaudi::CheckedProperty<double> m_percentPass{this, "PercentPass", 100.0,
                                               "Minimal reduction rate to achieve (statistics mode)"};
  Gaudi::Property<double>        m_forcedReduction{this, "ForcedReduction", 1.,
                                            "Percentage of events that should be passed (random number mode)"};
  Gaudi::Property<double>        m_margin{this, "SafetyMargin", 0.5,
                                   "Safety margin (accept if acc events < m_event/m_forcedReduction + m_margin)"};

  ToolHandle<IEventCounter> m_eventCounter{this, "EventCounter", "EvtCounter"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PostScaler )

bool PostScaler::operator()() const {
  bool accepted = true;
  ++m_nEventsAll;

  // random number method
  if ( m_percentPass < 100. ) {
    Rndm::Numbers random( randSvc(), Rndm::Flat( 0.0, 100. ) );
    if ( random ) {
      const auto r = random();
      if ( r > m_percentPass ) { accepted = false; }
    }
  }

  // Event number fraction method
  if ( m_forcedReduction > 1. ) {
    const auto max_evts = (double)m_eventCounter->getEventCounter() / m_forcedReduction + m_margin;
    //  max_evts = max_evts + 3*sqrt(max_evts);  /// 3 sigma security
    if ( m_nEvents.nEntries() > max_evts ) { accepted = false; }
  }

  // event passed
  if ( accepted ) ++m_nEvents;
  return accepted;
}
