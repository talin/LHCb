/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/Environment.h"
#include "GaudiKernel/IFileAccess.h"
#include "GaudiKernel/Service.h"
#include <fmt/format.h>
#include <fstream>
#include <git2.h>

namespace {
  /// Vector of supported protocols.
  static const std::vector<std::string> s_protocols = {{"paramfile"}, {"file"}};
  enum struct Protocol { paramfile, file };

  Protocol to_protocol( std::string_view sv ) {
    auto i = std::find( s_protocols.begin(), s_protocols.end(), sv );
    if ( i == s_protocols.end() ) {
      throw GaudiException( std::string{"invalid protocol: "}.append( sv ), __func__, StatusCode::FAILURE );
    }
    return Protocol( i - s_protocols.begin() );
  }

  std::string to_string( const git_blob* blob ) {
    assert( blob != nullptr );
    return std::string{reinterpret_cast<const char*>( git_blob_rawcontent( blob ) ), git_blob_rawsize( blob )};
  }

  class git {
    git_repository* m_repo = nullptr;

    struct object {
      git_object*     obj = nullptr;
                      operator bool() const { return obj != nullptr; }
                      operator git_object**() { return &obj; }
      const git_blob* as_blob() const {
        return ( git_object_type( obj ) == GIT_OBJECT_BLOB ? reinterpret_cast<const git_blob*>( obj ) : nullptr );
      }
      std::optional<std::string> as_string() const {
        auto* blob = as_blob();
        if ( !blob ) return std::nullopt; // can we set git_error_last to something usefull??
        return to_string( blob );
      }
      ~object() { git_object_free( obj ); }
    };

  public:
    git( std::string const& path ) {
      git_libgit2_init();
      if ( git_repository_open_bare( &m_repo, path.c_str() ) < 0 ) { m_repo = nullptr; };
    }

    operator bool() const { return m_repo != nullptr; }

    std::optional<std::string> read( std::string_view rev, std::string_view path ) const {
      assert( m_repo != nullptr );
      auto   rev_path = fmt::format( "{}:{}", rev, path );
      object obj;
      if ( git_revparse_single( obj, m_repo, rev_path.c_str() ) < 0 ) { return std::nullopt; }
      return obj.as_string();
    }

    ~git() {
      git_repository_free( m_repo );
      git_libgit2_shutdown();
    }
  };

  struct Read {
    static std::optional<std::string> invalid() { return std::nullopt; }
    static std::optional<std::string> from_file( std::string const& fname ) {
      std::string s;
      System::resolveEnv( fname, s ).orThrow( "ParamFileSvc", "Cannot resolveEnv " + fname );
      if ( std::ifstream is{s}; is && is.good() ) {
        return std::string{std::istreambuf_iterator<char>{is}, std::istreambuf_iterator<char>{}};
      }
      return invalid();
    }
    static std::optional<std::string> from_git( std::string const& repo, std::string const& commit,
                                                std::string const& fname ) {
      if ( auto g = git{repo}; g ) return g.read( commit, fname );
      return invalid();
    }
  };

  struct Open {
    static std::unique_ptr<std::istream> invalid() { return nullptr; }
    static std::unique_ptr<std::istream> from_file( std::string const& fname ) {
      std::string s;
      System::resolveEnv( fname, s ).orThrow( "ParamFileSvc", "Cannot resolveEnv " + fname );
      return std::make_unique<std::ifstream>( s );
    }
    static std::unique_ptr<std::istream> from_git( std::string const& repo, std::string const& commit,
                                                   std::string const& fname ) {
      if ( auto content = Read::from_git( repo, commit, fname ); content ) {
        return std::make_unique<std::istringstream>( std::move( content ).value() );
      }
      return invalid();
    }
  };

  auto parse_query_string( std::string qs ) {
    struct Range {
      std::string qs;
      struct Sentinel {};
      auto  begin() const { return *this; }
      auto  end() const { return Sentinel{}; }
      bool  operator!=( Sentinel ) const { return !qs.empty(); }
      auto& operator++() {
        auto amp = qs.find( '&' );
        qs       = ( amp == qs.npos ? std::string{} : qs.substr( amp + 1 ) );
        return *this;
      }
      auto operator*() const {
        auto token = qs.substr( 0, qs.find( '&' ) );
        auto eq    = token.find( '=' );
        if ( eq == token.npos ) {
          throw GaudiException( std::string{"bad key-value pair: "}.append( token ), __func__, StatusCode::FAILURE );
        }
        return std::pair{token.substr( 0, eq ), token.substr( eq + 1 )};
      }
    };
    return Range{std::move( qs )};
  }

  struct SplitResult {
    Protocol    protocol;
    std::string path;
    std::string query;
  };
  SplitResult split_url( std::string const& url ) {
    auto i = url.find( "://" );
    if ( i == url.npos ) { throw GaudiException( "invalid url: " + url, __func__, StatusCode::FAILURE ); }
    auto j = url.find( '?' );
    return {to_protocol( url.substr( 0, i ) ), url.substr( i + 3, j != url.npos ? j - ( i + 3 ) : url.npos ),
            j == url.npos ? std::string{} : url.substr( j + 1 )};
  }

  struct ParseResult {
    Protocol    protocol;
    std::string path;
    std::string version;
    std::string root;
  };
  ParseResult parse_url( std::string const& url ) {
    auto [protocol, path, query_string] = split_url( url );
    auto result                         = ParseResult{protocol, path, {}, {}};
    for ( auto const& [k, v] : parse_query_string( query_string ) ) {
      if ( k == "version" ) {
        result.version = v;
      } else if ( k == "root" ) {
        result.root = v;
      } else {
        throw GaudiException( std::string{"Unknown key "}.append( k ), __func__, StatusCode::FAILURE );
      }
    }
    return result;
  }

} // namespace

/** @class ParamFilesAccessTool FileReadTool.h
 *
 */
class ParamFileSvc : public extends<Service, IFileAccess> {

  std::string                  m_cached_root;
  Gaudi::Property<std::string> m_root{
      this, "Root", "${PARAMFILESROOT}",
      [=]( auto const& ) {
        System::resolveEnv( m_root, m_cached_root ).orThrow( "ParamFileSvc", "Cannot resolve  " + m_root );
      },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{true}};

  Gaudi::Property<std::string> m_version{this, "Version",
                                         ""}; // "HEAD"}; //  "be245b670317b10ac8802a3be16bf9816af52503"};

  void check( int err ) const {
    if ( err >= 0 ) return;
    const git_error* e = git_error_last();
    error() << fmt::format( "Error {}/{}: {}", err, e->klass, e->message ) << endmsg;
  }

  template <typename Action>
  auto i_execute( std::string const& url ) const {
    auto [protocol, path, version, root] = parse_url( url );
    switch ( protocol ) {
    case Protocol::file:
      if ( !version.empty() ) {
        throw GaudiException( "version only allowed for paramfile://", __func__, StatusCode::FAILURE );
      }
      if ( !root.empty() ) {
        throw GaudiException( "root only allowed for paramfile://", __func__, StatusCode::FAILURE );
      }
      return Action::from_file( path );
    case Protocol::paramfile:
      // TODO: find a better way to decide -- eg. check whether  m_cached_root is a repo? or
      // m_cached_root/.git is a repo??
      if ( version.empty() && m_version.empty() ) {
        return Action::from_file( fmt::format( "{}/{}", root.empty() ? m_cached_root : root, path ) );
      } else {
        return Action::from_git( fmt::format( "{}/.git", root.empty() ? m_cached_root : root ),
                                 version.empty() ? m_version.value() : version, path );
      }
    }
    return Action::invalid(); // cannot happen...
  }

public:
  using extends::extends;
  std::unique_ptr<std::istream> open( std::string const& url ) override {
    auto r = i_execute<Open>( url );
    if ( !r.get() ) { error() << "Failed to open URL '" << url << "'" << endmsg; }
    return std::unique_ptr<std::istream>( r.release() );
  }
  std::optional<std::string> read( std::string const& url ) override {
    auto r = i_execute<Read>( url );
    if ( !r.has_value() ) { error() << "Failed to read URL '" << url << "'" << endmsg; }
    return r;
  }
  const std::vector<std::string>& protocols() const override { return s_protocols; }
};

DECLARE_COMPONENT( ParamFileSvc )
