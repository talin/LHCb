/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RecHeader.h"
#include "LHCbAlgs/FilterPredicate.h"
#include "LHCbMath/DeterministicMixer.h"
#include <cstdint>
#include <limits>
#include <string>

class OfflineDeterministicPrescaler : public LHCb::Algorithm::FilterPredicate<bool( const LHCb::RecHeader& )> {

public:
  OfflineDeterministicPrescaler( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  bool       operator()( const LHCb::RecHeader& ) const override;

protected:
  void update( double newAccFrac ) {
    m_accFrac = newAccFrac;
    update();
  }

  void update() {
    constexpr auto mx = std::numeric_limits<uint32_t>::max();
    m_acc             = ( m_accFrac <= 0 ? 0 : m_accFrac >= 1 ? mx : uint32_t( m_accFrac * mx ) );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "frac: " << m_accFrac.value() << " acc: 0x" << std::hex << m_acc << endmsg;
  }

  Gaudi::Property<double> m_accFrac{this, "AcceptFraction", 1., [=]( Gaudi::Details::PropertyBase& ) {
                                      update();
                                      if ( msgLevel( MSG::DEBUG ) )
                                        debug() << "frac: " << m_accFrac.value() << " acc: 0x" << std::hex << m_acc
                                                << endmsg;
                                    }}; // fraction of input events to accept...
private:
  bool                     accept( const LHCb::RecHeader& header ) const;
  LHCb::DeterministicMixer m_initial{0}; // initial seed unique to this instance (computed from the name)
  uint32_t                 m_acc{std::numeric_limits<uint32_t>::max()}; // integer representation of the accept rate
  mutable Gaudi::Accumulators::BinomialCounter<> m_counter{this, "#accept"};
};
