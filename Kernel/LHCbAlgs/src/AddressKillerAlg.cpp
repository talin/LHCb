/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiKernel/IOpaqueAddress.h"
#include "LHCbAlgs/Consumer.h"

namespace LHCb {

  /**
   * Small algorithm to make the entire datastore anonymous.
   * All persistent info of all addresses is entirely removed.
   *
   * @author:  M.Frank
   * @version: 1.0
   */
  class AddressKillerAlg : public Algorithm::Consumer<void()> {

  public:
    using Consumer::Consumer;

    void operator()() const override {
      m_dataSvc
          ->traverseTree( [this]( IRegistry* reg, int ) {
            if ( const IOpaqueAddress* addr = reg->address(); addr ) {
              // NOT for MDF top level address!!!!
              if ( !( addr->svcType() == RAWDATA_StorageType && reg->identifier() == m_dataSvc->rootName() ) ) {
                if ( msgLevel( MSG::DEBUG ) )
                  debug() << "Remove store address \"" << reg->identifier() << "\"." << endmsg;
                reg->setAddress( nullptr );
              }
            }
            return true; // go recursively down
          } )
          .ignore();
    }

  private:
    ServiceHandle<IDataManagerSvc> m_dataSvc{this, "DataSvc", "EventDataSvc", "data provider service"};
  };

  DECLARE_COMPONENT_WITH_ID( AddressKillerAlg, "AddressKillerAlg" )
} // namespace LHCb
