/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Gaudi/Algorithm.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/SmartDataPtr.h"

/**
 *  Simple class to check the existence of
 *  objects in Gaudi Transient stores
 *  The actual object type is not checked.
 *
 *  Also the algorithm  could be used to "preload" objects.
 *  For MSG::DEBUG level the printout of object location and the actula type
 *  is performed
 *
 *  Algorithm properties:
 *
 *   @li <tt>Inputs</tt>  vector of locations to be checked,
 *                        the default value is empty vector
 *
 *   @li <tt>Store</tt>   numerical identifier for Gaudi Transient store,
 *                        0 : Event Store ( default )
 *                        1 : Detector Store
 *                        2 : Histogram Store
 *
 *   @li <tt>Stop</tt>    flag which dictates to return error code if
 *                        data is not available (default) or just
 *                        print a warning message. The second options
 *                        can be useful for interactive environment
 *
 *  Usage:
 *
 *  @code
 *  ApplicationMgr.TopAlg += [ 'TESCheck/EvtCheck' ]
 *  EvtCheck.Inputs = { "MC/Calo" } ;
 *  EvtCheck.Inputs = { "MC/Velo" } ;
 *
 *  // check detector element
 *  ApplicationMgr.TopAlg += [ 'TESCheck/DetCheck' ]
 *  DetCheck.Inputs = { LHCb::standard_geometry_top } ;
 *  // SWITCH to use Detector Data Store instead of Event Data Store
 *  DetCheck.DettectorStore = true ;
 *  @endcode
 *
 *  Indeed it is a bit updated version of
 *     CaloCheckObjectsAlg from Calo/CaloReco package
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2004-09-16
 */
struct TESCheck final : Gaudi::Algorithm {
  using Algorithm::Algorithm;

  StatusCode execute( const EventContext& ) const override;

  enum Stores { EvtStore = 0, DetStore, HstStore };

  StringArrayProperty   m_inputs{this, "Inputs", {}};
  Gaudi::Property<int>  m_store{this, "Store", Stores::EvtStore};
  Gaudi::Property<bool> m_stop{this, "Stop", true};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TESCheck )

StatusCode TESCheck::execute( const EventContext& evtCtx ) const {
  for ( const auto& address : m_inputs.value() ) {
    auto*                    dp = ( m_store == Stores::DetStore ? detSvc().get()
                                             : m_store == Stores::HstStore ? histoSvc().get() : evtSvc().get() );
    SmartDataPtr<DataObject> obj( dp, address );
    DataObject*              o = obj;
    if ( !o ) {
      if ( m_stop.value() ) {
        error() << "Check failed for '" << address << "'" << endmsg;
        return StatusCode::FAILURE;
      }
      warning() << "Check failed for '" << address << "'" << endmsg;
      execState( evtCtx ).setFilterPassed( false );
    } else {
      execState( evtCtx ).setFilterPassed( true );
    }
  }
  return StatusCode::SUCCESS;
}
