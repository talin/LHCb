/*****************************************************************************\
* (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ODIN.h"

#include "GaudiKernel/GaudiException.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/DeterministicMixer.h"
#include <algorithm>
#include <limits>
#include <map>

namespace LHCb {

  using EventTypes = ODIN::EventTypes;

  namespace {
    static const auto table =
        std::map<EventTypes, const char*>{{EventTypes::NoBias, "NoBias"}, {EventTypes::Lumi, "Lumi"}};

    using accfrac_t                          = std::map<std::string, std::vector<double>>;
    const accfrac_t DEFAULT_ACCEPT_FRACTIONS = {{table.at( EventTypes::NoBias ), {0, 0, 0, 0}},
                                                {table.at( EventTypes::Lumi ), {0, 0, 0, 0}}};

    EventTypes parse( std::string_view s ) {
      for ( const auto& p : table ) {
        if ( s == p.second ) return p.first;
      }
      throw GaudiException( "Could not parse string to EventTypes value.", "ODINEmulator", StatusCode::FAILURE );
      return EventTypes::Physics; // can never reach this line
    }
  } // namespace

  /** @class ODINEmulator ODINEmulator.cpp
   *
   * \brief Emulate ODIN
   *
   * The following parts of ODIN are emulated:
   * - eventNumber: incremented starting from 0 (not thread safe!)
   * - bunchCrossingType: either preserved (default) or set to a constant
   * - eventType: set according to the accept fractions.
   *   The deterministic prescaler machinery is used, with independent
   *   seeds for every combination of event type and bxtype.
   * All other fields are preserved.
   *
   * You must specify the location of the emulated ODIN with
   * OutputODINLocation. Optionally, you can configure
   * - the accept fractions for every combination of event type and
   bunch crossing type (default as for standard physics running),
   * - whether to override the bunch crossing type (no by default,
   use with care),
   * - whether to set filter passed based on the emulated event type
   (yes by default), and
   * - whether to overwrite the event number with a new monotonically
   *   increasing sequence. In multithreaded mode, the assignment of
   *   numbers is not deterministic.
   * - the ODIN input location.
   *
   */
  class ODINEmulator : public Algorithm::MultiTransformerFilter<std::tuple<ODIN>( const ODIN& )> {
  public:
    ODINEmulator( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformerFilter( name, pSvcLocator, KeyValue{"InputODINLocation", ODINLocation::Default},
                                  KeyValue{"OutputODINLocation", ""} ){};

    StatusCode             initialize() override;
    StatusCode             finalize() override;
    std::tuple<bool, ODIN> operator()( const ODIN& ) const override;

  private:
    mutable Gaudi::Accumulators::Counter<>          m_eventNumber{this, "Next event number."};
    std::map<unsigned int, std::array<uint32_t, 4>> m_initial;
    std::map<unsigned int, std::array<uint32_t, 4>> m_acc;
    mutable std::map<unsigned int, std::array<Gaudi::Accumulators::BinomialCounter<uint32_t>, 4>> m_counters;

    Gaudi::Property<accfrac_t> m_accFrac{
        this,
        "AcceptFractions",
        DEFAULT_ACCEPT_FRACTIONS,
        [this]( auto& ) {
          for ( auto& p : m_accFrac ) {
            for ( unsigned int i = 0; i < p.second.size(); ++i ) {
              auto accFrac                                        = p.second[i];
              auto acc                                            = ( accFrac <= 0 ? 0
                                        : accFrac >= 1 ? std::numeric_limits<uint32_t>::max()
                                                       : uint32_t( accFrac * std::numeric_limits<uint32_t>::max() ) );
              m_acc[static_cast<uint32_t>( parse( p.first ) )][i] = acc;
            }
          }
        },
        Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
        "Map {event_type: accept_fractions} with the latter is a list of the form [ee, be, eb, bb]."};
    Gaudi::Property<int>  m_overrideBXType{this, "OverrideBXType", -1,
                                          "If between 0 and 3, override the bunch crossing type for all events."};
    Gaudi::Property<bool> m_filter{this, "Filter", true, "Filter only selected events."};
    Gaudi::Property<bool> m_setEventNumber{this, "SetEventNumber", false,
                                           "Set event number using a new monitonically increasing sequence."};
  };

  DECLARE_COMPONENT_WITH_ID( ODINEmulator, "ODINEmulator" )

} // namespace LHCb

StatusCode LHCb::ODINEmulator::initialize() {
  return MultiTransformerFilter::initialize().andThen( [&] {
    for ( const auto& p : DEFAULT_ACCEPT_FRACTIONS ) {
      auto mask = static_cast<uint32_t>( parse( p.first ) );
      // create new empty entry in m_counters for mask
      (void)m_counters[mask];
      for ( auto&& [i, initial] : LHCb::range::enumerate( m_initial[mask], 0u ) ) {
        initial = LHCb::DeterministicMixer{name()}( mask )( i ).state;
      }
    }

    if ( msgLevel( MSG::DEBUG ) ) debug() << " generated initial value " << m_initial << endmsg;
  } );
}

std::tuple<bool, LHCb::ODIN> LHCb::ODINEmulator::operator()( const ODIN& odin ) const {
  ODIN emulated{odin};
  if ( m_setEventNumber ) {
    emulated.setEventNumber( m_eventNumber.value() );
    ++m_eventNumber;
  }
  if ( m_overrideBXType >= 0 && m_overrideBXType < 4 ) {
    emulated.setBunchCrossingType( static_cast<ODIN::BXTypes>( m_overrideBXType.value() ) );
  }
  auto bx = static_cast<std::uint8_t>( emulated.bunchCrossingType() );

  unsigned int eventType = std::accumulate(
      begin( m_initial ), end( m_initial ), 0u,
      [&]( unsigned int left, std::pair<unsigned int, std::array<uint32_t, 4>> p ) -> unsigned int {
        bool acc = true;
        auto x   = LHCb::DeterministicMixer{p.second[bx]}( odin.gpsTime(), odin.runNumber(), odin.eventNumber() ).state;
        auto accThld = m_acc.at( p.first )[bx];
        acc          = acc && ( accThld == std::numeric_limits<uint32_t>::max() || ( accThld > 0 && x < accThld ) );
        m_counters.at( p.first )[bx] += acc;
        return acc ? ( left | p.first ) : left;
      } );
  emulated.setEventType( eventType );
  return {eventType > 0 || !m_filter, emulated};
}

StatusCode LHCb::ODINEmulator::finalize() {
  for ( const auto& p : m_counters ) {
    for ( unsigned int i = 0; i < 4; ++i ) {
      info() << "#accept(" << p.first << "," << i << "): " << p.second[i] << endmsg;
    }
  }
  return MultiTransformerFilter::finalize();
}
