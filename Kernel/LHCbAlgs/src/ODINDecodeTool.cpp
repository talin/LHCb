/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Event/ODIN.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "GaudiKernel/SerializeSTL.h"
#include "ODINCodecBaseTool.h"
#include <sstream>

/** @class ODINDecodeTool ODINDecodeTool.h
 *
 *  Tool to decode the ODIN raw bank, fill a ODIN object with its data and
 *  register it to the transient store.
 *
 *  @see ODINCodecBaseTool for the properties.
 *
 *  @author Marco Clemencic
 *  @date   2009-02-02
 */
class ODINDecodeTool final : public ODINCodecBaseTool {

public:
  /// Standard constructor
  ODINDecodeTool( const std::string& type, const std::string& name, const IInterface* parent );

  /// Initialize the tool
  inline StatusCode initialize() override;

  /// Do the conversion
  void execute() override;

private:
  /// Location in the transient store of the ODIN object.
  /// FIXME: this is not stricly true, as we also read from this location...
  DataObjectWriteHandle<LHCb::ODIN> m_odinLocation{this, "ODINLocation", LHCb::ODINLocation::Default,
                                                   "Location of the ODIN object in the transient store"};
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( ODINDecodeTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ODINDecodeTool::ODINDecodeTool( const std::string& type, const std::string& name, const IInterface* parent )
    : ODINCodecBaseTool( type, name, parent ) {
  // new for decoders, initialize search path, and then call the base method
  m_rawEventLocations = {LHCb::RawEventLocation::Trigger, LHCb::RawEventLocation::Default};
  initRawEventSearch();
}
//=============================================================================
// Initialize
//=============================================================================
StatusCode ODINDecodeTool::initialize() {
  StatusCode sc = ODINCodecBaseTool::initialize(); // always first
  if ( sc.isFailure() ) return sc;                 // error message already printed

  if ( m_odinLocation.objKey() != LHCb::ODINLocation::Default ) {
    info() << "Using '" << m_odinLocation.objKey() << "' as location of the ODIN object" << endmsg;
  }

  if ( m_rawEventLocations.empty() || ( m_rawEventLocations[0] != LHCb::RawEventLocation::Default &&
                                        m_rawEventLocations[0] != LHCb::RawEventLocation::Trigger ) ) {
    info() << "Using '" << m_rawEventLocations << "' as search path for the RawEvent object" << endmsg;
  }

  return sc;
}
//=============================================================================
// Main function
//=============================================================================
void ODINDecodeTool::execute() {
  // load the odin
  LHCb::ODIN* odin = m_odinLocation.getIfExists();

  // Check if there is already an ODIN object
  if ( odin ) {
    if ( m_force ) {
      // Use the already registered object
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Modify existing ODIN object" << endmsg;
    } else {
      // ODIN already there and we are not supposed to touch it
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Keep existing ODIN object" << endmsg;
      return;
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Getting RawEvent" << endmsg;
  LHCb::RawEvent* rawEvent = findFirstRawEvent();
  if ( !rawEvent ) {
    using namespace GaudiUtils;
    // Throw a meaningful exception it the bank is not found;
    std::ostringstream out;
    out << "Cannot find RawEvent in " << m_rawEventLocations;
    Exception( out.str(), StatusCode::FAILURE );
  }

  // Check if have an ODIN bank...
  const auto& odinBanks = rawEvent->banks( LHCb::RawBank::ODIN );
  if ( !odinBanks.empty() ) { // ... good, we can decode it
    odin = this->i_decode( *odinBanks.begin(), odin );
    // synchronize DataObject::version with ODIN internal version
    odin->setVersion( LHCb::ODIN::BANK_VERSION );
    if ( odin && ( !odin->registry() ) ) { // register ODIN object if valid and not yet registered
      m_odinLocation.put( std::unique_ptr<LHCb::ODIN>( odin ) );
    }
  } else {
    Warning( "Cannot find ODIN bank in RawEvent" ).ignore();
  }
}

//=============================================================================
