/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ODINCodecBaseTool.h"

//=============================================================================
// IMPLEMENTATION of ODINCodecBaseTool
//=============================================================================

#include "ODINCodec.h"
#include <DAQKernel/DecoderToolBase.h>
#include <Event/ODIN.h>
#include <Event/RawBank.h>
#include <Event/RawEvent.h>
#include <algorithm>
#include <cstdint>
#include <memory>
#include <sstream>

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ODINCodecBaseTool::ODINCodecBaseTool( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {
  declareProperty( "Force", m_force = false, "If already present, override the destination object." );
  declareProperty( "IgnoreUnknownBankVersion", m_ignoreBankVersion = false,
                   "Do not stop in case of unknown bank version number, assuming"
                   " it is binary compatible with the latest known version." );
}
//=============================================================================
// Decode
//=============================================================================
LHCb::ODIN* ODINCodecBaseTool::i_decode( const LHCb::RawBank* bank, LHCb::ODIN* odin ) {
  // Check the passed pointers
  Assert( bank, "Called without a RawBank object (pointer NULL)" );

  if ( !odin ) {
    return new LHCb::ODIN{LHCb::ODINCodec::decode( *bank, m_ignoreBankVersion )};
  } else {
    *odin = LHCb::ODINCodec::decode( *bank, m_ignoreBankVersion );
    return odin;
  }
}
//=============================================================================
// Encode
//=============================================================================
LHCb::RawBank* ODINCodecBaseTool::i_encode( const LHCb::ODIN* odin, const int bank_version ) {
  // Check the passed pointer
  Assert( odin, "Called without an ODIN object (pointer NULL)" );

  // encode the ODIN instance
  std::array<std::uint32_t, LHCb::ODIN::BANK_SIZE / sizeof( std::uint32_t )> data{};
  LHCb::ODINCodec::encode( *odin, bank_version, data );
  // Create the new bank
  // Note that we cannot delete it, so better that there is no failure after
  // this line.
  // ODIN source ID must be in the range 0x0001 - 0x000A included (see EDMS 2100937)
  return LHCb::RawEvent::createBank( 0x0001, LHCb::RawBank::ODIN, bank_version, LHCb::ODIN::BANK_SIZE, data.data() );
}
//=============================================================================
