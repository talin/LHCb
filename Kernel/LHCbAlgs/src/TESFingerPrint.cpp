/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Gaudi/Algorithm.h"
#include "GaudiKernel/IDataSelector.h"
#include "GaudiKernel/IDataStoreLeaves.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/ObjectContainerBase.h"

#include <fstream>
#include <map>
#include <mutex>

//-----------------------------------------------------------------------------
// Implementation file for class : TESFingerPrint
//
// 2011-05-11 : Illya Shapoval
//-----------------------------------------------------------------------------

/**
 *  @author Illya Shapoval
 *  @date   2011-05-11
 */
struct TESFingerPrint final : Gaudi::Algorithm {
  using Algorithm::Algorithm;

  StatusCode execute( const EventContext& ) const override; ///< Algorithm execution
  StatusCode finalize() override;                           ///< Algorithm finalization

  /// Pointer to the (public) tool used to retrieve the objects in a file.
  PublicToolHandle<IDataStoreLeaves> m_leavesTool{this, "DataStoreLeavesTool", "DataSvcFileEntriesTool"};

  /// Counter map of nodes occurrences
  mutable std::map<std::string, long> m_stat_map;
  /// Counter map of containers and their contents occurrences
  mutable std::map<std::string, std::map<int, long>> m_cont_stat_map;
  /// mutex protecting access to m_stat_map and m_cont_stat_map
  mutable std::mutex m_stat_map_lock;

  Gaudi::Property<std::string> m_heur_level{this, "HeuristicsLevel", "Low",
                                            "The level of TES heuristic analysis to be performed to obtain"
                                            " its finger print."};
  Gaudi::Property<std::string> m_output_file_name{this, "OutputFileName", "tes_finger_print.dat",
                                                  "The name of the output file to store the TES finger print."};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TESFingerPrint )

StatusCode TESFingerPrint::execute( const EventContext& ) const {
  // serialize calls to this algo, as the code is not at all thread safe
  std::lock_guard lock( m_stat_map_lock );
  // Get the objects in the same file as the root node
  /// Collection of objects being selected
  IDataSelector objects;
  try {
    const auto& leaves = m_leavesTool->leaves();
    objects.assign( leaves.begin(), leaves.end() );
  } catch ( GaudiException& e ) {
    error() << e.message() << endmsg;
    return StatusCode::FAILURE;
  }
  // Collect TES statistics
  for ( const auto* o : objects ) {
    const auto& entry = o->registry()->identifier();
    if ( auto [iter, inserted] = m_stat_map.try_emplace( entry, 1 ); !inserted ) ++iter->second;
    if ( m_heur_level == "Medium" ) {
      const auto* pCont = dynamic_cast<const ObjectContainerBase*>( o );
      if ( pCont ) {
        const auto cont_size = pCont->numberOfObjects();
        if ( auto [iter, inserted] = m_cont_stat_map[entry].try_emplace( cont_size, 1 ); !inserted ) ++iter->second;
      }
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode TESFingerPrint::finalize() {
  std::ofstream tes_finger_print{m_output_file_name.value()};
  using namespace GaudiUtils;
  // Write low level TES statistics
  tes_finger_print << m_stat_map << "\n";
  if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "==> TES DataObject statistics: " << m_stat_map << endmsg; }
  // Write medium level TES statistics
  if ( m_heur_level == "Medium" ) {
    tes_finger_print << m_cont_stat_map << "\n";
    if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "==> TES Container sizes: " << m_cont_stat_map << endmsg; }
  }
  tes_finger_print.close();
  return Algorithm::finalize(); // must be called after all other actions
}
