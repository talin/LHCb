/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GaudiKernel/ICounterSummarySvc.h"
#include "GaudiKernel/StatEntity.h"
#include "LHCbAlgs/Consumer.h"

/**
 * Super simple algo with only one old style counter
 * Used only for testing XMLSummarySvc, hence the manual registration
 * Should be dropped when XMLSummarySvc is replaced
 */
class EventCountAlg final : public LHCb::Algorithm::Consumer<void()> {

public:
  using Consumer::Consumer;

  StatusCode finalize() override {
    SmartIF<ICounterSummarySvc> counterSummarySvc;
    counterSummarySvc = this->svcLoc()->service( "CounterSummarySvc", false );
    if ( !counterSummarySvc ) {
      fatal() << "could not locate CounterSummarySvc" << endmsg;
    } else {
      counterSummarySvc->addCounter( this->name(), "efficiency", m_counter, Gaudi::CounterSummary::SaveStatEntity );
    }
    return Consumer::finalize();
  }

  void operator()() const override { m_counter += 1; }

private:
  mutable StatEntity m_counter{0, 0, 0, 0, 0};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( EventCountAlg )
