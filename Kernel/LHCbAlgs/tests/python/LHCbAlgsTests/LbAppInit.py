###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def config():
    from PyConf.application import configure, configure_input, ApplicationOptions
    from PyConf.control_flow import CompositeNode
    from PyConf.Algorithms import LHCbTests__LbAppInitTester
    from Configurables import ApplicationMgr
    from DDDB.CheckDD4Hep import UseDD4Hep

    options = ApplicationOptions(_enabled=False)
    # No data from the input is used, but something should be there for the configuration
    options.input_files = ["dummy_input_file_name.dst"]
    options.input_type = 'ROOT'
    options.data_type = 'Upgrade'
    if not UseDD4Hep:
        options.dddb_tag = 'upgrade/dddb-20220705'
        options.conddb_tag = 'upgrade/sim-20220705-vc-mu100'
    else:
        options.geometry_version = "trunk"
        options.conditions_version = "master"
    options.simulation = True
    options.evt_max = 1
    # options.output_level = 2

    config = configure_input(options)

    app = ApplicationMgr()
    app.EvtSel = "NONE"  # ignore input configuration

    cf = CompositeNode("test", [
        LHCbTests__LbAppInitTester(name="LbAppInitTester"),
    ])
    app.OutStream.clear()
    config.update(configure(options, cf))


def check(stdout, causes, result):
    import re
    from DDDB.CheckDD4Hep import UseDD4Hep

    if not UseDD4Hep:
        stdout = re.sub(r"  /[^ ]*(DDDB|SIMCOND)", r"\1", stdout)
        expected = """LbAppInitTester                        INFO CondDB tags:
DDDB.git -> upgrade/dddb-20220705[c4e7d0e8]
SIMCOND.git -> upgrade/sim-20220705-vc-mu100[5d1f40ae]"""
    else:
        stdout = re.sub(r"git:[^ ]*lhcb-conditions-database\.git",
                        "git:/.../lhcb-conditions-database.git", stdout)
        expected = """LbAppInitTester                        INFO CondDB tags:
  GeometryLocation -> ${DETECTOR_PROJECT_ROOT}/compact
  GeometryVersion -> trunk
  GeometryMain -> LHCb.xml
  ConditionLocation -> git:/.../lhcb-conditions-database.git
  ConditionVersion -> master"""

    result["expected CondDB tags"] = result.Quote(expected)
    if expected not in '\n'.join(stdout.splitlines()):
        causes.append("stdout")
