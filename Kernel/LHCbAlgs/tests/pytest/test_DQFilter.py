###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiTests import run_gaudi

JSON_DUMP_FILENAME = f"test_DQFilter.json"


def config():
    from GaudiConfig2 import Configurables as C

    odin_path = '/Event/DummyODIN'

    algs = [
        C.LHCb.Tests.FakeRunNumberProducer(
            'FakeRunNumber', ODIN=odin_path, Start=1, Step=1),
        C.DQFilter(
            ODINLocation=odin_path,
            Flags={
                "a": "1,5",
                "b": "3,5-7"
            },
            OutputLevel=1),
    ]
    counterSink = C.Gaudi.Monitoring.JSONSink(
        FileName=JSON_DUMP_FILENAME,
        TypesToSave=["count.*"],
        ComponentsToSave=["DQFilter"],
        NamesToSave=["accepted"],
    )
    app = C.ApplicationMgr(
        EvtMax=10, EvtSel="NONE", TopAlg=algs, ExtSvc=[counterSink])
    return [app, counterSink] + algs


def test():
    import json
    import re

    result = run_gaudi(f"{__file__}:config", check=True, capture_output=True)
    out = result.stdout.decode(errors="replace").splitlines()
    sequence_of_good_runs = ["good" in l for l in out if "Filter event:" in l]
    assert sequence_of_good_runs == [
        False, True, False, True, False, False, False, True, True, True
    ]
    reasons = dict(
        m.groups()
        for m in (re.match(r"^.*run (\d+) ignored because of \[([^\]]+)\]", l)
                  for l in out) if m)
    assert reasons == {"1": "a", "3": "b", "5": "a, b", "6": "b", "7": "b"}

    counters = json.load(open(JSON_DUMP_FILENAME))
    counters = [  # filter for counter of interest
        c for c in counters
        if c["component"] == "DQFilter" and c["name"] == "accepted"
    ]
    assert len(counters) == 1
    counter = counters[0]["entity"]
    assert counter["nFalseEntries"] == 5
    assert counter["nTrueEntries"] == 5
