/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/Chrono.h"

#ifdef __x86_64__
#  include <thread>

namespace LHCb {
  namespace chrono {
    const uint64_t fast_clock::tsc_frequency = []() {
      _mm_lfence();
      uint64_t const start = __rdtsc();
      _mm_lfence();
      std::this_thread::sleep_for( std::chrono::milliseconds( 1000 ) );
      _mm_lfence();
      return __rdtsc() - start;
    }();
    const double fast_clock::tsc_ratio = static_cast<double>( fast_clock::period::den ) / fast_clock::tsc_frequency;
  } // namespace chrono
} // namespace LHCb
#endif
