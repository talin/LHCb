/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <functional>
#include <tuple>
#include <variant>

namespace LHCb {
  /** Utilities for defining operations that optionally operate on variant
   *  types.
   */
  template <typename T>
  struct is_variant : std::false_type {
    using tuple_type = std::tuple<T>;
  };

  template <typename... Ts>
  struct is_variant<std::variant<Ts...>> : std::true_type {
    using tuple_type = std::tuple<Ts...>;
  };

  template <typename T>
  inline constexpr bool is_variant_v = is_variant<T>::value;

  /** Alias that forwards to boost::variant2::variant or std::variant depending
   *  on compile-time checks. See https://gitlab.cern.ch/lhcb/Rec/-/issues/124
   */
  template <typename... Ts>
  using variant = std::variant<Ts...>;

  /** Check that the given list of types is not empty and contains only variant
   *  types.
   */
  template <typename... Variants>
  inline constexpr bool are_all_variants_v = ( sizeof...( Variants ) > 0 ) && ( is_variant_v<Variants> && ... );

  /** Use the given callable as a visitor for variant arguments, or simply
   *  invoke it if the arguments are not variants.
   */
  template <typename F, typename... Args>
  [[gnu::always_inline]] __attribute__( ( flatten ) ) inline decltype( auto ) invoke_or_visit( F&& f, Args&&... args ) {
    if constexpr ( are_all_variants_v<std::decay_t<Args>...> ) {
      // std::visit should both be found by ADL
      return visit( std::forward<F>( f ), std::forward<Args>( args )... );
    } else {
      return std::invoke( std::forward<F>( f ), std::forward<Args>( args )... );
    }
  }

  template <typename... Ts>
  using invoke_or_visit_result_t = decltype( invoke_or_visit( std::declval<Ts>()... ) );
} // namespace LHCb
