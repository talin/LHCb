/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Detector/Calo/CaloCellID.h"
#include "Detector/FT/FTChannelID.h"
#include "Detector/Muon/TileID.h"
#include "Detector/UT/ChannelID.h"
#include "Detector/VP/VPChannelID.h"
#include "Kernel/RichSmartID.h"
#include <ostream>

namespace LHCb {

  /** @class LHCbID LHCbID.h
   *
   * LHCb wide channel identifier
   *
   * @author Marco Cattaneo
   *
   */
  inline namespace v2 {
    class LHCbID final {
    public:
      /// types of sub-detector channel ID
      enum class channelIDtype {
        UNDEFINED                    = 0,
        Plume                        = 1,
        VP                           = 2,
        UT                           = 3,
        reserved_magnet_sidestations = 4,
        FT                           = 5,
        reserved_mighty_tracker      = 6,
        reserved_torch               = 7,
        Rich                         = 8,
        Calo                         = 9,
        Muon                         = 10
      };

      /// Default Constructor
      constexpr LHCbID() = default;

      /// Constructor from unsigned int
      explicit constexpr LHCbID( unsigned int theID ) : m_lhcbID( theID ) {}

      /// Constructor from type and unsigned int id
      constexpr LHCbID( channelIDtype t, unsigned int id )
          : m_lhcbID{( ( static_cast<unsigned int>( t ) << detectorTypeBits ) & detectorTypeMask ) | ( id & IDMask )} {
        assert( ( id & IDMask ) == id );
      }

      /// allow SIMD implementations to work with the bit representation....
      template <typename int_v>
      static constexpr auto make( channelIDtype t, int_v v ) {
        return ( ( static_cast<unsigned int>( t ) << detectorTypeBits ) & detectorTypeMask ) + v;
      }

      /// Constructor from VPChannelID, VP corresponding to pixel solution for upgrade
      constexpr LHCbID( Detector::VPChannelID chanID ) : LHCbID{channelIDtype::VP, chanID} {}

      /// Constructor from UTChannelID
      constexpr LHCbID( Detector::UT::ChannelID chanID ) : LHCbID{channelIDtype::UT, chanID} {}

      /// Constructor from RichSmartID
      constexpr LHCbID( const RichSmartID& chanID );

      /// Constructor from Calo::CellID
      constexpr LHCbID( Detector::Calo::CellID chanID ) : LHCbID{channelIDtype::Calo, chanID.all()} {}

      /// Constructor from MuonTileID
      constexpr LHCbID( const Detector::Muon::TileID& chanID );

      /// Constructor from FTChannelID
      constexpr LHCbID( Detector::FTChannelID chanID ) : LHCbID{channelIDtype::FT, chanID} {}

      /// comparison equality
      constexpr friend bool operator==( LHCbID lhs, LHCbID rhs ) { return lhs.lhcbID() == rhs.lhcbID(); }

      /// comparison ordering
      constexpr friend bool operator<( LHCbID lhs, LHCbID rhs ) { return lhs.lhcbID() < rhs.lhcbID(); }

      /// return true if this is a VP identifier
      constexpr bool isVP() const { return channelIDtype::VP == detectorType(); }

      /// return the VPChannelID
      constexpr Detector::VPChannelID vpID() const;

      /// return true if this is a UT Silicon Tracker identifier
      constexpr bool isUT() const { return channelIDtype::UT == detectorType(); }

      /// return the ChannelID
      constexpr Detector::UT::ChannelID utID() const;

      /// return true if this is a Rich identifier
      constexpr bool isRich() const { return channelIDtype::Rich == detectorType(); }

      /// return the richSmartID
      constexpr RichSmartID richID() const;

      /// return true if this is a Calo identifier
      constexpr bool isCalo() const { return channelIDtype::Calo == detectorType(); }

      /// return the CaloCellID
      constexpr Detector::Calo::CellID caloID() const;

      /// return true if this is a Muon identifier
      constexpr bool isMuon() const { return channelIDtype::Muon == detectorType(); }

      /// return the TileID
      constexpr Detector::Muon::TileID muonID() const;

      /// return true if this is a Fibre Tracker identifier
      constexpr bool isFT() const { return channelIDtype::FT == detectorType(); }

      /// return the FTChannelID
      constexpr Detector::FTChannelID ftID() const;

      /// Check the LHCbID sub-detector channel ID type identifier
      constexpr bool checkDetectorType( LHCbID::channelIDtype type ) const { return type == detectorType(); }

      /// General ID: returns detector ID = internal unsigned int
      constexpr unsigned int channelID() const;

      /// Print this LHCbID in a human readable way
      std::ostream& fillStream( std::ostream& s ) const;

      /// Retrieve const  the internal representation
      constexpr unsigned int lhcbID() const { return m_lhcbID; }

      /// Update the ID bits (to recreate the channelID)
      constexpr LHCbID& setID( unsigned int value ) {
        m_lhcbID &= ~IDMask;
        m_lhcbID |= value & IDMask;
        return *this;
      }

      /// Retrieve the LHCb detector type bits
      constexpr channelIDtype detectorType() const {
        return channelIDtype( ( m_lhcbID & detectorTypeMask ) >> detectorTypeBits );
      }

      /// Update the LHCb detector type bits
      constexpr LHCbID& setDetectorType( channelIDtype value ) {
        unsigned int val = static_cast<unsigned int>( value );
        m_lhcbID &= ~detectorTypeMask;
        m_lhcbID |= ( val << detectorTypeBits ) & detectorTypeMask;
        return *this;
      }

      friend std::ostream& operator<<( std::ostream& str, const LHCbID& obj ) { return obj.fillStream( str ); }

    private:
      /// Offsets of bitfield lhcbID
      static constexpr auto detectorTypeBits = 28;

      /// Checks if a given bit is set
      constexpr bool testBit( unsigned int pos ) const { return ( 0 != ( lhcbID() & ( 1 << pos ) ) ); }

      /// Sets the given bit to the given value
      constexpr LHCbID& setBit( unsigned int pos, unsigned int value ) {
        m_lhcbID |= value << pos;
        return *this;
      }

      /// Bitmasks for bitfield lhcbID
      enum lhcbIDMasks { IDMask = 0xfffffffL, detectorTypeMask = 0xf0000000L };

      unsigned int m_lhcbID = 0; ///< the internal representation

    }; // class LHCbID

    inline std::ostream& operator<<( std::ostream& s, LHCbID::channelIDtype e ) {
      switch ( e ) {
      case LHCbID::channelIDtype::Rich:
        return s << "Rich";
      case LHCbID::channelIDtype::Calo:
        return s << "Calo";
      case LHCbID::channelIDtype::Muon:
        return s << "Muon";
      case LHCbID::channelIDtype::VP:
        return s << "VP";
      case LHCbID::channelIDtype::FT:
        return s << "FT";
      case LHCbID::channelIDtype::UT:
        return s << "UT";
      default:
        return s << "ERROR wrong value " << int( e ) << " for enum LHCbID::channelIDtype";
      }
    }

    // -----------------------------------------------------------------------------
    // end of class
    // -----------------------------------------------------------------------------

    // Including forward declarations

    constexpr LHCbID::LHCbID( const RichSmartID& chanID ) : LHCbID{channelIDtype::Rich, chanID.dataBitsOnly()} {
      // Save the MaPMT/HPD flag in bit 27
      setBit( 27, chanID.idType() );
      // Set the validity bits
      setBit( 26, chanID.pixelDataAreValid() );
    }

    constexpr LHCbID::LHCbID( const Detector::Muon::TileID& chanID ) {
      // store lower part as it is // 18 bits
      unsigned int lhcbid_muon = chanID.key();
      unsigned int compactLay =
          ( ( chanID & Detector::Muon::Base::MaskLayoutX ) >> Detector::Muon::Base::ShiftLayoutX ) +
          ( ( chanID & Detector::Muon::Base::MaskLayoutY ) >> Detector::Muon::Base::ShiftLayoutY ) *
              Detector::Muon::Base::max_compacted_xGrid;

      m_lhcbID =
          ( ( static_cast<unsigned int>( channelIDtype::Muon ) << detectorTypeBits ) & detectorTypeMask ) |
          ( ( compactLay << Detector::Muon::Base::ShiftCompactedLayout ) & Detector::Muon::Base::MaskCompactedLayout ) |
          ( lhcbid_muon & Detector::Muon::Base::MaskKey ); // 18 bits
    }

    constexpr Detector::VPChannelID LHCbID::vpID() const {
      return Detector::VPChannelID( isVP() ? m_lhcbID & IDMask : 0xF0000000 );
    }

    constexpr Detector::UT::ChannelID LHCbID::utID() const {
      return ( isUT() ? Detector::UT::ChannelID( m_lhcbID & IDMask ) : Detector::UT::ChannelID( 0xF0000000 ) );
    }

    constexpr RichSmartID LHCbID::richID() const {
      // Create the RichSMartID data bits
      RichSmartID::KeyType data( isRich() ? ( m_lhcbID & IDMask ) : 0 );
      // Create a temporary RichSmartID
      RichSmartID tmpid( data );
      // Retrieve the MaPMT/HPD flag
      if ( isRich() ) { tmpid.setIDType( static_cast<RichSmartID::IDType>( testBit( 27 ) ) ); }
      // Object to return, with RICH and panel fields set
      RichSmartID id( tmpid.rich(), tmpid.panel(), tmpid.pdNumInCol(), tmpid.pdCol(), tmpid.idType() );
      // Set pixels fields
      if ( testBit( 26 ) ) {
        id.setPixelRow( tmpid.pixelRow() );
        id.setPixelCol( tmpid.pixelCol() );
        if ( tmpid.idType() == RichSmartID::HPDID ) { id.setPixelSubRow( tmpid.pixelSubRow() ); }
      }
      // return
      return id;
    }

    constexpr Detector::Calo::CellID LHCbID::caloID() const {
      return isCalo() ? Detector::Calo::CellID( m_lhcbID & IDMask ) : Detector::Calo::CellID( 0xF0000000 );
    }

    constexpr Detector::Muon::TileID LHCbID::muonID() const {
      // const unsigned int muonIDMask = 18;
      if ( !isMuon() ) return Detector::Muon::TileID( 0xF0000000 );
      Detector::Muon::TileID tmpid( m_lhcbID & 0x3FFFF );
      unsigned int           compacted_layout =
          ( m_lhcbID & Detector::Muon::Base::MaskCompactedLayout ) >> Detector::Muon::Base::ShiftCompactedLayout;
      Detector::Muon::TileID id(
          tmpid.key(), Detector::Muon::Layout( compacted_layout % Detector::Muon::Base::max_compacted_xGrid,
                                               compacted_layout / Detector::Muon::Base::max_compacted_xGrid ) );

      return isMuon() ? Detector::Muon::TileID( id ) : Detector::Muon::TileID( 0xF0000000 );
    }

    constexpr Detector::FTChannelID LHCbID::ftID() const {
      return isFT() ? Detector::FTChannelID( m_lhcbID & IDMask ) : Detector::FTChannelID( 0xF0000000 );
    }

    constexpr unsigned int LHCbID::channelID() const {

      switch ( detectorType() ) {
      case channelIDtype::VP:
        return vpID().channelID();
      case channelIDtype::UT:
        return utID().channelID();
      case channelIDtype::Rich:
        return richID().key();
      case channelIDtype::Calo: // C++17 [[ fall-through ]]
      case channelIDtype::Muon:
        return m_lhcbID & IDMask;
      case channelIDtype::FT:
        return ftID().channelID();
      default:
        return 0;
      }
    } // namespace v2
  }   // namespace v2
} // namespace LHCb
