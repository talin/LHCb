/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <type_traits>

// traits which indicate whether client code should use 'threeMomCovMatrix', `momPosCovMatrix` and `posCovMatrix`
// or whether to request a `trackState`
// possible values: yes (for track-like objects ) no (for neutrals, composites), maybe (particle, check at runtime, as
// calls may return invalid results -- not yet implemented)
//
namespace LHCb::Event::CanBeExtrapolatedDownstream {

  struct yes_t : std::true_type {};
  struct no_t : std::false_type {};
  struct maybe_t : no_t {
  }; // basically, if you need to know at compile time, the answer is 'no' as there is no guarantee -- but it could
     // still eg. be a Particle v1 representing a track, which in turn could be extrapolated downstream..

  inline constexpr auto yes   = yes_t{};
  inline constexpr auto no    = no_t{};
  inline constexpr auto maybe = maybe_t{};

} // namespace LHCb::Event::CanBeExtrapolatedDownstream

namespace LHCb::Event::IsBasicParticle {
  struct yes_t : std::true_type {};
  struct no_t : std::false_type {};

  inline constexpr auto yes = yes_t{};
  inline constexpr auto no  = no_t{};
} // namespace LHCb::Event::IsBasicParticle

namespace LHCb::Event::HasTrack {
  struct yes_t : std::true_type {};
  struct no_t : std::false_type {};

  inline constexpr auto yes = yes_t{};
  inline constexpr auto no  = no_t{};
} // namespace LHCb::Event::HasTrack
