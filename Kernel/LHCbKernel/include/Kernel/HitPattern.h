/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Kernel/LHCbID.h"
#include "Kernel/STLExtensions.h"
#include <GaudiKernel/GaudiException.h>
#include <bitset>
#include <ostream>
#include <string>
#include <vector>

// Forward declarations

namespace LHCb {

  // Forward declarations

  /** @class HitPattern HitPattern.h
   *
   * pattern of hits used on tracks
   *
   * @author Stephanie Hansmann-Menzemer and Wouter Hulsbergen (2009)
   *
   */

  template <std::size_t N>
  size_t countNonZero( const std::array<int8_t, N>& a ) {
    return std::count_if( std::begin( a ), std::end( a ), []( auto x ) {
      assert( !( x < 0 ) );
      return x != 0;
    } );
  }

  class HitPattern {
  public:
    // pattern_type is just std::array, but with one function added to make it look more like original bitset
    template <std::size_t N>
    struct pattern_type : public std::array<int8_t, N> {
      bool test( std::size_t pos ) const { return this->operator[]( pos ) != 0; }
    };

    // number of detector layers/regions
    enum Number { NumVelo = 26, NumUT = 4, NumFT = 12, NumMuon = 5 };
    // VP layer pattern
    using VPPattern = pattern_type<Number::NumVelo>;
    // UT layer pattern
    using UTPattern = pattern_type<Number::NumUT>;
    // FT-layer hit pattern
    using FTPattern = pattern_type<Number::NumFT>;
    // Muon layer pattern
    using MuonPattern = pattern_type<NumMuon>;

    /// Constructor from span of LHCbIDs
    HitPattern( span<const LHCbID> ids );

    /// HitPattern from arbitrary container
    template <typename LHCbIDRange>
    HitPattern( LHCbIDRange&& ids ) {
      for ( auto id : ids ) add( id );
    }

    /// Default Constructor
    HitPattern() = default;

    /// Add one hit
    void add( const LHCbID id ) {
      this->add( id, []( auto& n ) { ++n; } );
    }

    /// Remove one hit
    void remove( const LHCbID id ) {
      this->add( id, []( auto& n ) { --n; } );
    }

    /// Print this HitPattern
    std::ostream& fillStream( std::ostream& s ) const;

    /// velo layer pattern A-side
    auto veloA() const { return m_veloA; }

    /// velo layer pattern C-side
    auto veloC() const { return m_veloC; }

    /// combined A/C velo layer pattern
    VPPattern velo() const;

    /// ut layer pattern
    auto ut() const { return m_ut; }

    /// ft layer pattern
    auto ft() const { return m_ft; }

    /// ft layer pattern
    auto muon() const { return m_muon; }

    /// number of velo A-side layers
    auto numVeloA() const { return countNonZero( m_veloA ); }

    /// number of velo C-side layers
    auto numVeloC() const { return countNonZero( m_veloC ); }

    /// number Velo layers
    auto numVelo() const { return numVeloA() + numVeloC(); }

    /// number of FT layers
    auto numFT() const { return countNonZero( m_ft ); }

    /// number of UT layers
    auto numUT() const { return countNonZero( m_ut ); }

    /// number of Muon layers
    auto numMuon() const { return countNonZero( m_muon ); }

    /// minimum of number of velo layers on A or C side
    auto numVeloStationsOverlap() const { return std::min( numVeloA(), numVeloC() ); }

    /// number of holes in velo (layer) pattern
    size_t numVeloHoles() const;

    /// number of holes in T (layer) pattern
    size_t numFTHoles() const;

    /// number Velo layers (kept for backward compatibility)
    auto numVeloStations() const { return numVelo(); }

    /// number of T-station layers (kept for backward compatibility)
    auto numTLayers() const { return numFT(); }

    /// number of T-station holes (kept for backward compatibility)
    auto numTHoles() const { return numFTHoles(); }

    friend std::ostream& operator<<( std::ostream& str, const HitPattern& obj ) { return obj.fillStream( str ); }

    bool operator==( const HitPattern& rhs ) const {
      return m_veloA == rhs.m_veloA && m_veloC == rhs.m_veloC && m_ft == rhs.m_ft && m_ut == rhs.m_ut &&
             m_muon == rhs.m_muon;
    }

  private:
    template <typename Advance>
    void add( const LHCbID id, Advance&& advance );

  private:
    VPPattern   m_veloA{}; ///< VP hit pattern on detector A side
    VPPattern   m_veloC{}; ///< VP hit pattern on detector C side
    FTPattern   m_ft{};    ///< FT hit pattern
    UTPattern   m_ut{};    ///< ut bit pattern
    MuonPattern m_muon{};  ///< muon bit pattern

  }; // class HitPattern

  inline std::ostream& operator<<( std::ostream& s, LHCb::HitPattern::Number e ) {
    switch ( e ) {
    case LHCb::HitPattern::NumVelo:
      return s << "NumVelo";
    case LHCb::HitPattern::NumUT:
      return s << "NumUT";
    case LHCb::HitPattern::NumFT:
      return s << "NumFT";
    case LHCb::HitPattern::NumMuon:
      return s << "NumMuon";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::HitPattern::Number";
    }
  }

  template <typename Advance>
  void HitPattern::add( const LHCbID id, Advance&& advance ) {
    switch ( id.detectorType() ) {
    case LHCbID::channelIDtype::VP: {
      const auto vpid    = id.vpID();
      const auto station = vpid.station();
      if ( vpid.sidepos() == 0 ) {
        advance( m_veloA[station] );
      } else {
        advance( m_veloC[station] );
      }
    } break;
    case LHCbID::channelIDtype::FT:
      advance( m_ft[id.ftID().globalLayerIdx()] );
      break;
    case LHCbID::channelIDtype::Muon:
      advance( m_muon[id.muonID().station()] );
      break;
    case LHCbID::channelIDtype::UT: {
      // FIXME: would like to use utid.uniqueLayer(), but that currently returns {5,6,9,10}.
      const auto   utid        = id.utID();
      unsigned int uniquelayer = utid.layer();
      advance( m_ut[uniquelayer] );
    } break;
    default:
      throw GaudiException{"Unsupported detectortype: " + std::to_string( int( id.detectorType() ) ), "HitPattern::add",
                           StatusCode::FAILURE};
    }
  }

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------
