/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE example
#include <boost/test/unit_test.hpp>

#include <Kernel/CountIterator.h>
#include <vector>

BOOST_AUTO_TEST_CASE( test_count_iterator ) {
  count_iterator<int> i;
  BOOST_CHECK( i.count() == 0 );

  std::vector<int> data = {0, 1, 2, 3, 4, 5, 6};
  {
    auto result = std::copy( begin( data ), end( data ), count_iterator<int>() );
    BOOST_CHECK( result.count() == 7 );
  }
  {
    auto result = std::copy_if( begin( data ), end( data ), count_iterator<int>(), []( auto i ) { return i % 2; } );
    BOOST_CHECK( result.count() == 3 );
  }
}
