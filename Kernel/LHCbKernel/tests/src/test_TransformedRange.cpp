/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE example
#include <boost/test/unit_test.hpp>

#include <Kernel/TransformedRange.h>
#include <numeric>
#include <vector>

BOOST_AUTO_TEST_CASE( test_transformed_range ) {

  std::vector<int> data = {0, 1, 2, 3, 4, 5, 6};

  LHCb::TransformedRange data_square_even{data, []( const auto& i ) { return i * i; },
                                          []( const auto& i ) { return i % 2 == 0; }};
  {
    int sum = 0;
    for ( const auto& i : data_square_even ) sum += i;
    BOOST_CHECK( sum == 56 );
  }
}
