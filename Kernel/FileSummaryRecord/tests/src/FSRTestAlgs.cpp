/*****************************************************************************\
* (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Event/LumiEventCounter.h>
#include <Event/ODIN.h>
#include <Gaudi/Accumulators.h>
#include <LHCbAlgs/Consumer.h>

namespace LHCb ::Tests {
  /// Simple algorithm that counts how many times it is invoked
  struct EventCountAlg final : public LHCb::Algorithm::Consumer<void()> {
    using Consumer::Consumer;
    void operator()() const override { ++m_events; }

    mutable Gaudi::Accumulators::Counter<> m_events{this, "count"};
  };
  DECLARE_COMPONENT( EventCountAlg )

  /// Simple algorithm that counts how many times it is invoked per run
  struct RunEventCountAlg final : public LHCb::Algorithm::Consumer<void( const LHCb::ODIN& )> {
    RunEventCountAlg( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, KeyValue{"ODIN", ODINLocation::Default} ) {}
    void operator()( const LHCb::ODIN& odin ) const override { m_lumiCount.inc( odin.runNumber() ); }

    mutable LHCb::LumiEventCounter m_lumiCount{this, "eventsByRun"};
  };
  DECLARE_COMPONENT( RunEventCountAlg )
} // namespace LHCb::Tests
