###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import json
from traceback import format_exc
from unittest import TestCase
from pprint import pformat

FILENAMEFSR = f"{__name__}.fsr.json"
FILENAMES = [f"{__name__}.{i}.root" for i in range(2)]
FILENAMEJSON = f"{__name__}.json"


def checkDiff(a, b):
    try:
        TestCase().assertEqual(a, b)
    except AssertionError as err:
        return str(err)


def config():
    from PyConf.application import configure, configure_input, ApplicationOptions, root_writer
    from PyConf.control_flow import CompositeNode
    from PyConf.components import setup_component
    from PyConf.Algorithms import LHCb__Tests__EventCountAlg, Gaudi__Examples__IntDataProducer
    from Configurables import ApplicationMgr

    options = ApplicationOptions(_enabled=False)
    # No data from the input is used, but something should be there for the configuration
    options.input_files = ["dummy_input_file_name.dst"]
    options.input_type = 'ROOT'
    options.output_file = FILENAMES[0]
    options.output_type = 'ROOT'
    options.data_type = 'Upgrade'
    options.dddb_tag = 'upgrade/dddb-20220705'
    options.conddb_tag = 'upgrade/sim-20220705-vc-mu100'
    options.geometry_version = 'trunk'
    options.conditions_version = 'master'
    options.simulation = True
    options.evt_max = 5
    options.monitoring_file = FILENAMEJSON
    # options.output_level = 2

    config = configure_input(options)

    app = ApplicationMgr()
    app.EvtSel = "NONE"  # ignore input configuration
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                instance_name="FileSummaryRecord",
                AcceptRegex=r"^(EvtCounter\.count|FilesTracker\..*)$",
                OutputFile=FILENAMEFSR,
            )))
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__OpenFilesTracker",
                instance_name="FilesTracker",
            )))

    producer = Gaudi__Examples__IntDataProducer()

    cf = CompositeNode("test", [
        LHCb__Tests__EventCountAlg(name="EvtCounter"),
        producer,
    ] + [root_writer(f, [producer.OutputLocation]) for f in FILENAMES])
    app.OutStream.clear()
    config.update(configure(options, cf))

    # make sure the histogram file is not already there
    for name in FILENAMES + [FILENAMEFSR, FILENAMEJSON]:
        if os.path.exists(name):
            os.remove(name)


def check(causes, result):
    result["root_output_files"] = str(FILENAMES)

    missing_files = [
        name for name in FILENAMES + [FILENAMEFSR, FILENAMEJSON]
        if not os.path.exists(name)
    ]
    if missing_files:
        causes.append("missing output file(s)")
        result["missing_files"] = ", ".join(missing_files)
        return False

    expected = {
        "EvtCounter.count": {
            "empty": False,
            "nEntries": 5,
            "type": "counter:Counter:m"
        },
        "FilesTracker.FileEvents": [],
        "FilesTracker.OutputFileStats": {
            "FSRTests.write_2_streams.0.root": {
                "status": "full",
                "writes": 5
            },
            "FSRTests.write_2_streams.1.root": {
                "status": "full",
                "writes": 5
            }
        },
    }

    try:
        import ROOT

        fsr_dump = json.load(open(FILENAMEFSR))

        fsr_root = {}
        for name in FILENAMES:
            f = ROOT.TFile.Open(name)
            fsr_root[name] = json.loads(str(f.FileSummaryRecord))

    except Exception as err:
        causes.append("failure in root file")
        result["python_exception"] = result.Quote(format_exc())
        return False

    result["FSR"] = result.Quote(pformat(fsr_dump))

    checking = "no check yet"
    try:
        guids = {
            e["name"]: e["guid"]
            for e in fsr_dump.get("output_files", [])
        }
        assert guids, "missing or invalid GUIDs in FSR dump"

        # this part is implicitly tested when comparing the content of ROOT FSRs
        del fsr_dump["output_files"]

        tester = TestCase()
        tester.maxDiff = None
        checking = "JSON dump"
        tester.assertEqual(expected, fsr_dump)
        checking = "ROOT file(s)"
        for name in FILENAMES:
            expected["guid"] = guids[name]
            tester.assertEqual(expected, fsr_root[name])

    except AssertionError as err:
        causes.append("FSR content")
        result[f"FSR problem ({checking})"] = result.Quote(str(err))
        return False

    return True
