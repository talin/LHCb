###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from multiprocessing.sharedctypes import Value
import os
import json
from traceback import format_exc
from unittest import TestCase
from pprint import pformat

FILENAMEFSR = f"{__name__}.fsr.json"
FILENAMEOPTS = f"{__name__}.opts.json"
FILENAME = f"{__name__}.root"
FILENAMEJSON = f"{__name__}.json"


def checkDiff(a, b):
    try:
        TestCase().assertEqual(a, b)
    except AssertionError as err:
        return str(err)


def config():
    from PyConf.application import configure, configure_input, ApplicationOptions, root_writer
    from PyConf.control_flow import CompositeNode
    from PyConf.components import setup_component
    from PyConf.Algorithms import LHCb__Tests__EventCountAlg, Gaudi__Examples__IntDataProducer
    from Configurables import ApplicationMgr

    options = ApplicationOptions(_enabled=False)
    # No data from the input is used, but something should be there for the configuration
    options.input_files = ["dummy_input_file_name.dst"]
    options.input_type = 'ROOT'
    options.output_file = FILENAME
    options.output_type = 'ROOT'
    options.data_type = 'Upgrade'
    options.dddb_tag = 'upgrade/dddb-20220705'
    options.conddb_tag = 'upgrade/sim-20220705-vc-mu100'
    options.geometry_version = 'trunk'
    options.conditions_version = 'master'
    options.simulation = True
    options.evt_max = 5
    options.monitoring_file = FILENAMEJSON
    # options.output_level = 2

    config = configure_input(options)

    app = ApplicationMgr()
    app.EvtSel = "NONE"  # ignore input configuration
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                instance_name="FileSummaryRecord",
                AcceptRegex=r"^EvtCounter\.count$",
                OutputFile=FILENAMEFSR,
                IncludeJobOptions=True,
            )))

    producer = Gaudi__Examples__IntDataProducer(
        name='Gaudi__Examples__IntDataProducer', Value=1)

    cf = CompositeNode("test", [
        LHCb__Tests__EventCountAlg(name="EvtCounter"),
        producer,
        root_writer(options.output_file, [producer.OutputLocation]),
    ])
    app.OutStream.clear()
    config.update(configure(options, cf))

    # make sure the histogram file is not already there
    for name in [FILENAME, FILENAMEFSR, FILENAMEJSON, FILENAMEOPTS]:
        if os.path.exists(name):
            os.remove(name)


def check(causes, result):
    result["root_output_file"] = FILENAME

    missing_files = [
        name for name in [FILENAME, FILENAMEFSR, FILENAMEJSON, FILENAMEOPTS]
        if not os.path.exists(name)
    ]
    if missing_files:
        causes.append("missing output file(s)")
        result["missing_files"] = ", ".join(missing_files)
        return False

    expected = {
        "EvtCounter.count": {
            "empty": False,
            "nEntries": 5,
            "type": "counter:Counter:m"
        },
    }
    expected_options = {
        "FileSummaryRecord.AcceptRegex":
        "\"^EvtCounter\\\\.count$\"",
        "FileSummaryRecord.IncludeJobOptions":
        "True",
        "FileSummaryRecord.OutputFile":
        f"'{FILENAMEFSR}'",
        "Gaudi__Examples__IntDataProducer.OutputLocation":
        "/Event/Gaudi__Examples__IntDataProducer/OutputLocation",
        "Gaudi__Examples__IntDataProducer.Value":
        "1",
    }

    try:
        import ROOT

        fsr_dump = json.load(open(FILENAMEFSR))

        f = ROOT.TFile.Open(FILENAME)
        fsr_root = json.loads(str(f.FileSummaryRecord))

    except Exception as err:
        causes.append("failure in root file")
        result["python_exception"] = result.Quote(format_exc())
        return False

    result["FSR"] = result.Quote(pformat(fsr_dump))

    checking = "no check yet"
    try:
        guid = fsr_dump.get("guid")
        assert guid, "missing or invalid GUID in FSR dump"

        expected["guid"] = guid  # GUID is random

        jobOptions = fsr_dump.get("jobOptions")
        assert jobOptions, "missing or invalid job options in FSR dump"

        # we do not want to check all details of the options
        expected["jobOptions"] = jobOptions

        tester = TestCase()
        # tester.maxDiff = None
        checking = "JSON dump"
        tester.assertEqual(expected, fsr_dump)
        checking = "ROOT file"
        tester.assertEqual(expected, fsr_root)

        # check that we find in the options what we explicitly set
        found_options = jobOptions["options"]
        found_options = {
            k: found_options[k]
            for k in expected_options if k in found_options
        }
        tester.assertEqual(expected_options, found_options)

    except AssertionError as err:
        causes.append("FSR content")
        result[f"FSR problem ({checking})"] = result.Quote(str(err))
        return False

    return True
