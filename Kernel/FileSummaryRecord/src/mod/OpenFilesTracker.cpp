/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Monitorable.h"
#include <GaudiKernel/FileIncident.h>
#include <GaudiKernel/IIncidentListener.h>
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/Incident.h>
#include <GaudiKernel/Service.h>
#include <GaudiKernel/SmartIF.h>
#include <GaudiUtils/IIODataManager.h>
#include <nlohmann/json.hpp>
#include <unordered_map>
#include <utility>

#if defined( __clang__ )
#  pragma clang diagnostic push
#  pragma clang diagnostic ignored "-Wunused-function"
#endif

namespace {
  struct FileEvent {
    FileEvent( const Incident& inc, std::string id ) : type{inc.type()}, filename{inc.source()}, id{std::move( id )} {}
    std::string type;
    std::string filename;
    std::string id;
  };
  NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE( FileEvent, type, filename, id )

  /// Helper to record incidents related to output files.
  struct OutputStats {
    enum class Status { opened, part, full, fail };
    Status        status{Status::opened};
    std::uint64_t writes{0};

    /// New entry (IncidentType::BeginOutputFile)
    OutputStats() = default;
    /// Data written to file (IncidentType::WroteToOutputFile)
    void dataWrite() {
      status = Status::part;
      ++writes;
    }
    /// Data file closed (IncidentType::EndOutputFile)
    void close() { status = Status::full; }
    /// Problems when writing the file (IncidentType::FailOutputFile)
    void fail() { status = Status::fail; }
  };
  NLOHMANN_JSON_SERIALIZE_ENUM( OutputStats::Status, {
                                                         {OutputStats::Status::opened, "opened"},
                                                         {OutputStats::Status::part, "part"},
                                                         {OutputStats::Status::full, "full"},
                                                         {OutputStats::Status::fail, "fail"},
                                                     } )

  void to_json( nlohmann::json& j, const OutputStats& stats ) {
    j["status"] = stats.status;
    if ( stats.writes ) j["writes"] = stats.writes;
  }
} // namespace

namespace LHCb::FSR {
  struct OpenFilesTracker : extends<Service, IIncidentListener> {
    OpenFilesTracker( std::string name, ISvcLocator* svcloc ) : extends( std::move( name ), svcloc ) {}

    StatusCode initialize() override {
      return Service::initialize().andThen( [&] {
        // declare ourself as a incident listener (to know when files are opened/closed)
        if ( auto incSvc = service<IIncidentSvc>( "IncidentSvc" ) ) {
          incSvc->addListener( this, IncidentType::BeginInputFile );
          incSvc->addListener( this, IncidentType::BeginOutputFile );
          incSvc->addListener( this, IncidentType::WroteToOutputFile );
          incSvc->addListener( this, IncidentType::FailOutputFile );
          incSvc->addListener( this, IncidentType::FailInputFile );
          incSvc->addListener( this, IncidentType::EndOutputFile );
          incSvc->addListener( this, IncidentType::EndInputFile );
        }

        m_ioDataMgr = service( "IODataManager" );
      } );
    }
    StatusCode finalize() override {
      if ( auto incSvc = service<IIncidentSvc>( "IncidentSvc" ) ) { incSvc->removeListener( this ); }
      return Service::finalize();
    }

    void handle( const Incident& inc ) override {
      if ( inc.type() == IncidentType::BeginOutputFile ) {
        m_outputFileStats.value[inc.source()] = {};
      } else if ( inc.type() == IncidentType::WroteToOutputFile ) {
        m_outputFileStats.value[inc.source()].dataWrite();
      } else if ( inc.type() == IncidentType::EndOutputFile ) {
        m_outputFileStats.value[inc.source()].close();
      } else if ( inc.type() == IncidentType::FailOutputFile ) {
        m_outputFileStats.value[inc.source()].fail();
      } else {
        // we track file incidents only if we have access to IIODataManger
        auto conn = m_ioDataMgr ? m_ioDataMgr->connection( inc.source() ) : nullptr;
        m_fileEvents.value.emplace_back( inc, conn ? conn->fid() : std::string{"unknown"} );
      }
    }

    Monitorable<std::list<FileEvent>>                         m_fileEvents{this, "FileEvents"};
    Monitorable<std::unordered_map<std::string, OutputStats>> m_outputFileStats{this, "OutputFileStats"};

    SmartIF<Gaudi::IIODataManager> m_ioDataMgr;
  };
  DECLARE_COMPONENT( OpenFilesTracker )
} // namespace LHCb::FSR

#if defined( __clang__ )
#  pragma clang diagnostic pop
#endif
