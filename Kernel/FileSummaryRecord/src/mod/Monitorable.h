/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Gaudi/MonitoringHub.h>
#include <nlohmann/json.hpp>
#include <utility>

namespace LHCb ::FSR {
  /// Helper to allow monitoring of simple data
  template <typename T>
  struct Monitorable final {
    template <typename OWNER>
    Monitorable( OWNER* o, std::string name ) : m_monitoringHub{&o->serviceLocator()->monitoringHub()} {
      m_monitoringHub->registerEntity( o->name(), std::move( name ), typeid( T ).name(), *this );
    }
    ~Monitorable() {
      if ( m_monitoringHub ) m_monitoringHub->removeEntity( *this );
    }

    Monitorable() = default;
    template <typename U>
    Monitorable( U&& v ) : value{std::forward<U>( v )} {}
    template <typename U>
    Monitorable& operator=( U&& v ) {
      value = std::forward<U>( v );
      return *this;
    }

    operator T&() { return value; }
    operator const T&() const { return value; }

    nlohmann::json toJSON() const { return value; }
    void           reset() { value = {}; }

    T                       value{};
    Gaudi::Monitoring::Hub* m_monitoringHub{nullptr};
  };
} // namespace LHCb::FSR
