###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Kernel/LHCbMath
---------------
#]=======================================================================]

gaudi_add_library(LHCbMathLib
    SOURCES
        src/BSpline.cpp
        src/Bernstein1D.cpp
        src/Bernstein2D.cpp
        src/Bernstein3D.cpp
        src/BernsteinPoly.cpp
        src/Binomial.cpp
        src/Blind.cpp
        src/Chi2Fit.cpp
        src/Choose.cpp
        src/Combine.cpp
        src/EigenSystem.cpp
        src/Faddeeva.cpp
        src/Functions.cpp
        src/GSL_sentry.cpp
        src/GSL_utils.cpp
        src/Hesse.cpp
        src/HistoInterpolationNew.cpp
        src/Interpolation.cpp
        src/LHCbMath.cpp
        src/Lomont.cpp
        src/LorentzVectorWithError.cpp
        src/MD5.cpp
        src/MoreFunctions.cpp
        src/ParticleParams.cpp
        src/Point3DWithError.cpp
        src/Polynomials.cpp
        src/SIMDWrapper.cpp
        src/Similarity.cpp
        src/Spline.cpp
        src/StateVertexUtils.cpp
        src/ValueWithError.cpp
        src/Vector3DWithError.cpp
        src/WStatEntity.cpp
        src/nSphere.cpp
    LINK
        PUBLIC
            Boost::headers
            cppgsl::cppgsl
            Eigen3::Eigen
            Gaudi::GaudiKernel
            GSL::gsl
            LHCb::GaudiGSLLib
            ROOT::MathCore
            Vc::Vc
            VDT::vdt
        PRIVATE
            ROOT::Hist
)

if(CMAKE_SYSTEM_PROCESSOR STREQUAL x86_64)
    # FIXME: with lcg-toolchains, x86_64 means "-march=x86_64" while with LbDevTools
    # meant "-march=x86_64 -msse4.2", and TestSimilarityAccuracy is very sensitive
    # to the vectorization
    set_property(SOURCE src/Similarity.cpp APPEND PROPERTY COMPILE_OPTIONS "-msse4.2")
endif()

gaudi_add_dictionary(LHCbMathDict
    HEADERFILES dict/LHCbMathDict.h
    SELECTION dict/LHCbMathDict.xml
    LINK LHCb::LHCbMathLib
)

gaudi_install(PYTHON)

# flags to turn off AVX512.
set(NO_AVX512_FLAGS     "-mno-avx512bw -mno-avx512cd -mno-avx512dq -mno-avx512er -mno-avx512f -mno-avx512ifma -mno-avx512pf -mno-avx512vbmi -mno-avx512vl -mno-avx512vpopcntdq ")
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU" AND CMAKE_CXX_COMPILER_VERSION VERSION_GREATER "7.99")
    set(NO_AVX512_FLAGS "${NO_AVX512_FLAGS} -mno-avx512vbmi2 -mno-avx512vnni ")
endif() 

# Determine compiler flags for each build target
set(SSE4_BUILD_FLAGS    " -msse4.2 -mno-avx -mno-avx2 -mno-fma ${NO_AVX512_FLAGS}" )
set(AVX_BUILD_FLAGS     " -mavx -mno-avx2 -mno-fma ${NO_AVX512_FLAGS}" )
set(AVX2_BUILD_FLAGS    " -mavx2 -mno-fma ${NO_AVX512_FLAGS}")
set(AVX2FMA_BUILD_FLAGS " -mavx2 -mfma ${NO_AVX512_FLAGS}")
# only use 'basic' avx512 options here..
set(AVX512_BUILD_FLAGS  " -mavx512f -mavx512cd -mavx512dq")
exec_program(${CMAKE_CXX_COMPILER} ARGS -print-prog-name=as OUTPUT_VARIABLE _as)
if(NOT _as)
  message(FATAL_ERROR "Could not find the 'as' assembler")
else()
  exec_program(${_as} ARGS --version OUTPUT_VARIABLE _as_version)
  string(REGEX REPLACE "\\([^\\)]*\\)" "" _as_version "${_as_version}")
  string(REGEX MATCH "[1-9]\\.[0-9]+(\\.[0-9]+)?" _as_version "${_as_version}")
  if(_as_version VERSION_LESS "2.21.0")
     message(WARNING "binutils is too old to support AVX2+FMA... Falling back to AVX only.")
     set(AVX2_BUILD_FLAGS    " -mavx -mno-avx2 -mno-fma ${NO_AVX512_FLAGS}" )
     set(AVX2FMA_BUILD_FLAGS " -mavx -mno-avx2 -mno-fma ${NO_AVX512_FLAGS}" )
     set(AVX512_BUILD_FLAGS  " -mavx -mno-avx2 -mno-fma ${NO_AVX512_FLAGS}" )
  endif()
endif()

if(BUILD_TESTING)
    gaudi_add_tests(QMTest)
    gaudi_add_executable(TestSimilarity
        SOURCES tests/Similarity.cpp
        LINK
            LHCb::LHCbMathLib
            ROOT::MathCore
    )
    gaudi_add_executable(TestSimilarityAccuracy
        SOURCES tests/SimilarityAccuracy.cpp
        LINK LHCb::LHCbMathLib)
    gaudi_add_executable(TestPow
        SOURCES tests/pow.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestBit
        SOURCES tests/bit.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestDigit
        SOURCES tests/digit.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestKine
        SOURCES tests/kinematics.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestTruncate
        SOURCES tests/truncate.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestLomont
        SOURCES tests/TestLomontCompare.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestErrors
        SOURCES tests/withErrors.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestRound
        SOURCES tests/TestRound.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestLomont2
        SOURCES tests/TestLomontCPU.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestEigen
        SOURCES tests/TestEigen.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestBlind
        SOURCES tests/blind.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestAbs
        SOURCES tests/testAbs.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestChi2
        SOURCES tests/testChi2.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestHesse
        SOURCES tests/TestHesse.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestChi2Fit
        SOURCES tests/TestChi2Fit.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestLABug
        SOURCES tests/TestLABug.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestBloomFilter
        SOURCES tests/TestBloomFilter.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestEigenGeometry
        SOURCES tests/TestEigenGeometry.cpp
        LINK LHCb::LHCbMathLib
    )
    gaudi_add_executable(TestXYZTypes
        SOURCES tests/TestXYZTypes.cpp
        LINK
            LHCb::LHCbMathLib
            ROOT::GenVector
    )
    gaudi_add_executable(TestPolynomials
        SOURCES tests/poly.cpp
        LINK LHCb::LHCbMathLib
    )
    set(INSTRSET_SSE4    6)
    set(INSTRSET_AVX     7)
    set(INSTRSET_AVX2    8)
    set(INSTRSET_AVX2FMA 8) # AVX2 CPUs can always do FMA
    set(INSTRSET_AVX512  9) # at the moment we only use basic AVX512
    if(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
        foreach(instr_set IN ITEMS SSE4 AVX AVX2 AVX2FMA AVX512)
            gaudi_add_executable(TestVDTMath${instr_set}
                SOURCES tests/TestVDTMath.cpp
                LINK LHCb::LHCbMathLib
            )
            set_property(TARGET TestVDTMath${instr_set} APPEND_STRING PROPERTY COMPILE_FLAGS ${${instr_set}_BUILD_FLAGS})
            set_property(TARGET TestVDTMath${instr_set} APPEND PROPERTY COMPILE_DEFINITIONS INSTRSET=${INSTRSET_${instr_set}})
            gaudi_add_executable(TestMathSpeed${instr_set}
                SOURCES tests/MathSpeedTests/main.cpp
                LINK LHCb::LHCbMathLib rt
            )
            set_property(TARGET TestMathSpeed${instr_set} APPEND_STRING PROPERTY COMPILE_FLAGS " -Wno-ignored-attributes ${${instr_set}_BUILD_FLAGS}" )
        endforeach()
    endif()
endif()

foreach(test IN ITEMS test_md5 test_deterministic_mixer test_matvec test_simdwrapper)
    gaudi_add_executable(${test}
        SOURCES tests/${test}.cpp
        LINK
            LHCb::LHCbMathLib
            Boost::unit_test_framework
        TEST
    )
endforeach()

gaudi_add_executable(test_splinegridinterpolation
    SOURCES
        tests/test_SplineGridInterpolation.cpp
    LINK
        LHCb::LHCbMathLib
        Boost::unit_test_framework
    TEST
)
