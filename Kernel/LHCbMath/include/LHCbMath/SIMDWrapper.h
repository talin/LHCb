/***************************************************************************** \
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/STLExtensions.h"
#include "LHCbMath/TypeMapping.h"
#include "LHCbMath/bit_cast.h"

#ifdef __x86_64__
#  include <immintrin.h>
#endif
#ifdef __aarch64__
#  include <arm_neon.h>
#endif
#include <limits>

#include <algorithm>
#include <array>
#include <bitset>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <string>

template <typename T>
inline auto approx_log2( T x ) {
  auto vx = castToInt( x );
  auto mx = castToFloat( ( vx & 0x007FFFFF ) | 0x3F000000 );
  auto y  = T( vx ) * 1.1920928955078125e-7f;
  return y - 124.22551499f - 1.498030302f * mx - 1.72587999f / ( 0.3520887068f + mx );
}

// TODO: implement for double...
inline auto approx_log2( double x ) { return std::log2( x ); }

template <typename T>
inline auto approx_log( T x ) {
  return approx_log2( x ) * 0.69314718f;
}

template <typename F>
inline auto approx_exp( F x ) {
  // Implementation from vdt's fast_exp and
  // https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/Kernel/LHCbMath/include/LHCbMath/FastMaths.h

  using I     = typename LHCb::type_map<F>::int_t;
  F initial_x = x;
  I n         = I( x * 1.44269504088896341f + 0.5f - copysign( F( 1.f ), x ) );
  F z         = F( n );

  x -= z * ( 0.693359375f - 2.12194440e-4f );

  z = 1.9875691500E-4f * x + 1.3981999507E-3f;
  z = z * x + 8.3334519073E-3f;
  z = z * x + 4.1665795894E-2f;
  z = z * x + 1.6666665459E-1f;
  z = z * x + 5.0000001201E-1f;
  z = z * x * x + x + 1.f;

  // multiply by power of 2
  z *= F( castToFloat( ( n + 0x7f ) << 23 ) );

  z = select( initial_x > 88.72283905206835f, F( std::numeric_limits<float>::infinity() ), z );
  z = select( initial_x < -88.f, F( 0.f ), z );

  return z;
}

namespace SIMDWrapper {

  namespace details {
    template <typename T>
    constexpr T pow( T x, int n ) {
      if ( n < 0 ) return pow( T( 1 ) / x, -n );
      if ( n == 0 ) return T( 1 );
      if ( n == 1 ) return x;
      auto y = pow( x * x, n / 2 );
      return n % 2 == 1 ? x * y : y;
    }
  } // namespace details

  template <typename T>
  inline auto log( T x ) {
    return approx_log( x );
  }
  template <typename T>
  inline auto exp( T x ) {
    return approx_exp( x );
  }

  namespace simd_traits {
    template <typename T>
    struct traits {
      constexpr static bool is_specialized   = false;
      constexpr static bool is_simd_integral = false;
    };

  } // namespace simd_traits

  // https://github.com/WojciechMula/sse-popcount/blob/master/popcnt-avx2-harley-seal.cpp
  // https://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel
  template <typename T, typename = std::enable_if_t<simd_traits::traits<T>::is_simd_integral>>
  auto popcount_v( T a ) {
    const auto m1   = T{0x55555555}; // 101010101010...
    const auto m2   = T{0x33333333}; // 110011001100...
    const auto m4   = T{0x0F0F0F0F}; // 111100001111...
    const auto e1   = T{0x01010101}; // 100000001000...
    const auto tmp1 = a - ( ( a >> 1 ) & m1 );
    const auto tmp2 = ( tmp1 & m2 ) + ( ( tmp1 >> 2 ) & m2 );
    const auto tmp3 = ( tmp2 + ( tmp2 >> 4 ) ) & m4;
    return ( tmp3 * e1 ) >> 24;
  }

  // TODO reorganise so that we can use Kernel/meta_enum.h here
  // (it's in LHCbKernel, which depends on LHCbMath where this file lives)
  enum InstructionSet { Unknown = 0, Best, Scalar, SSE, AVX2, AVX256, AVX512, Neon };

  inline std::string instructionSetName( InstructionSet set ) {
    switch ( set ) {
    case Best:
      return "Best";
    case Scalar:
      return "Scalar";
    case SSE:
      return "SSE";
    case AVX2:
      return "AVX2";
    case AVX256:
      return "AVX256";
    case AVX512:
      return "AVX512";
    case Neon:
      return "Neon";
    default:
      return "Unknown";
    }
  }

  template <typename bare, std::size_t size, typename T>
  inline std::ostream& print_vector( std::ostream& os, T x, std::string_view name ) {
    std::array<bare, size> tmp;
    x.store( tmp );
    os << name << "{";
    for ( std::size_t i = 0; i < size - 1; ++i ) { os << tmp[i] << ", "; }
    return os << tmp[size - 1] << "}";
  }

  template <std::size_t size, typename T>
  inline std::ostream& print_bitset( std::ostream& os, T x, std::string_view name ) {
    auto s = std::bitset<size>{x}.to_string();
    std::reverse( s.begin(), s.end() );
    return os << name << "{" << s << "}";
  }

  // Define some helper functions that will always return which instruction set
  // was used for the types in a given namespace when the stack was built.
  namespace scalar {
    using SIMDWrapper::exp;
    using SIMDWrapper::log;
  } // namespace scalar
  namespace sse {
    using SIMDWrapper::exp;
    using SIMDWrapper::log;
  } // namespace sse
  namespace avx2 {
    using SIMDWrapper::exp;
    using SIMDWrapper::log;
  } // namespace avx2
  namespace avx256 {
    using SIMDWrapper::exp;
    using SIMDWrapper::log;
  } // namespace avx256
  namespace avx512 {
    using SIMDWrapper::exp;
    using SIMDWrapper::log;
  } // namespace avx512
  namespace neon {
    using SIMDWrapper::exp;
    using SIMDWrapper::log;
  } // namespace neon
  namespace best {
    using SIMDWrapper::exp;
    using SIMDWrapper::log;
  } // namespace best

  namespace detail {
    template <typename pod_t, typename vec_t>
    struct numeric_limits {
      static constexpr bool is_specialized = true;
      // Would prefer these to be constexpr, but that needs constexpr intrinsics
      static vec_t lowest() noexcept { return {std::numeric_limits<pod_t>::lowest()}; }
      static vec_t min() noexcept { return {std::numeric_limits<pod_t>::min()}; }
      static vec_t max() noexcept { return {std::numeric_limits<pod_t>::max()}; }
      // TODO add the rest of the methods...
    };
  } // namespace detail

#define SIMDWRAPPER_SPECIALISE_NUMERIC_LIMITS( pod_t, vec_t )                                                          \
  template <>                                                                                                          \
  struct std::numeric_limits<vec_t> : SIMDWrapper::detail::numeric_limits<pod_t, vec_t> {};                            \
  template <>                                                                                                          \
  struct std::numeric_limits<const vec_t> : SIMDWrapper::detail::numeric_limits<pod_t, vec_t> {};                      \
  template <>                                                                                                          \
  struct std::numeric_limits<volatile vec_t> : SIMDWrapper::detail::numeric_limits<pod_t, vec_t> {};                   \
  template <>                                                                                                          \
  struct std::numeric_limits<const volatile vec_t> : SIMDWrapper::detail::numeric_limits<pod_t, vec_t> {}

#define SIMDWRAPPER_SPECIALISE_HELPER_TYPES( types )                                                                   \
  SIMDWRAPPER_SPECIALISE_NUMERIC_LIMITS( int, types::int_v );                                                          \
  SIMDWRAPPER_SPECIALISE_NUMERIC_LIMITS( float, types::float_v );                                                      \
  template <>                                                                                                          \
  struct LHCb::type_map<types::int_v> {                                                                                \
    using float_t  = types::float_v;                                                                                   \
    using mask_t   = types::mask_v;                                                                                    \
    using scalar_t = int;                                                                                              \
  };                                                                                                                   \
  template <>                                                                                                          \
  struct LHCb::type_map<types::float_v> {                                                                              \
    using int_t    = types::int_v;                                                                                     \
    using mask_t   = types::mask_v;                                                                                    \
    using scalar_t = float;                                                                                            \
  }

  template <typename T>
  auto to_array( T vec ) {
    std::array<typename LHCb::type_map<T>::scalar_t, T::size()> tmp;
    vec.store( tmp.data() ); // TODO: vec.store( tmp ) with lhcb/LHCb!2958
    return tmp;
  }

  namespace scalar {
    using details::pow;

    constexpr InstructionSet instructionSet() { return Scalar; }

    class int_v;

    class float_v;

    class mask_v {
    public:
      constexpr mask_v() {} // Constructor must be empty
      constexpr explicit mask_v( bool v ) : data{v} {}
      constexpr mask_v( int m ) : data{m} {}

      constexpr mask_v& operator=( int m ) {
        data = m;
        return *this;
      }

      constexpr                    operator int_v() const;
      constexpr bool               cast() const { return data; }
      constexpr explicit           operator bool() const { return cast(); }
      constexpr static std::size_t size() { return 1; }

      // matching the other mask_v types, & | and ^ perform logic rather than
      // bitwise operations
      constexpr friend mask_v operator&( mask_v lhs, mask_v rhs ) { return lhs.data && rhs.data; }
      constexpr friend mask_v operator&( bool lhs, mask_v rhs ) { return mask_v{lhs} & rhs; }
      constexpr friend mask_v operator&( mask_v lhs, bool rhs ) { return lhs & mask_v{rhs}; }
      constexpr friend mask_v operator|( mask_v lhs, mask_v rhs ) { return lhs.data || rhs.data; }
      constexpr friend mask_v operator|( bool lhs, mask_v rhs ) { return mask_v{lhs} | rhs; }
      constexpr friend mask_v operator|( mask_v lhs, bool rhs ) { return lhs | mask_v{rhs}; }
      // ugly looking XOR (cast to bool and make sure they are different; i.e. one
      // true and one false)
      constexpr friend mask_v operator^( mask_v lhs, mask_v rhs ) { return ( !!lhs.data ) != ( !!rhs.data ); }

      constexpr friend mask_v operator&&( mask_v lhs, mask_v rhs ) { return lhs & rhs; }
      constexpr friend bool   operator&&( bool lhs, mask_v rhs ) { return ( lhs & rhs ).cast(); }
      constexpr friend bool   operator&&( mask_v lhs, bool rhs ) { return ( lhs & rhs ).cast(); }
      constexpr friend mask_v operator||( mask_v lhs, mask_v rhs ) { return lhs | rhs; }
      constexpr friend bool   operator||( bool lhs, mask_v rhs ) { return ( lhs | rhs ).cast(); }
      constexpr friend bool   operator||( mask_v lhs, bool rhs ) { return ( lhs | rhs ).cast(); }
      constexpr friend mask_v operator!( mask_v x ) { return !x.data; }

      constexpr friend bool all( mask_v mask ) { return mask.data == 1; }
      constexpr friend bool none( mask_v mask ) { return mask.data == 0; }
      constexpr friend bool any( mask_v mask ) { return mask.data != 0; }
      constexpr friend bool testbit( mask_v mask, int ) { return mask.data == 1; }
      constexpr friend int  ffs( mask_v mask ) { return mask.data; }
      constexpr friend int  popcount( mask_v mask ) { return mask.cast(); }
      constexpr friend int  select( mask_v mask, int a, int b ) { return mask.cast() ? a : b; }

      friend std::ostream& operator<<( std::ostream& os, mask_v mask ) { return os << "scalar{" << mask.data << "}"; }

    private:
      int data{};
    };

    template <typename T, typename = std::enable_if_t<std::is_arithmetic_v<T>>>
    constexpr T hmax( T data, mask_v mask ) {
      return mask.cast() ? data : std::numeric_limits<T>::lowest();
    }
    template <typename T, typename = std::enable_if_t<std::is_arithmetic_v<T>>>
    constexpr T hmin( T data, mask_v mask ) {
      return mask.cast() ? data : std::numeric_limits<T>::max();
    }

    struct types {
      constexpr static std::size_t size = 1;
      using int_v                       = scalar::int_v;
      using float_v                     = scalar::float_v;
      using mask_v                      = scalar::mask_v;
      static mask_v                   mask_true() { return mask_v{true}; }
      static mask_v                   mask_false() { return mask_v{false}; }
      static int_v                    indices();
      static int_v                    indices( int start );
      static mask_v                   loop_mask( int offset, int size ) { return size > offset; }
      static constexpr InstructionSet instructionSet() { return Scalar; }
    };

    class float_v {
    public:
      constexpr static std::size_t size() { return scalar::types::size; }
      using span       = LHCb::span<float, scalar::types::size>;
      using const_span = LHCb::span<const float, scalar::types::size>;

      constexpr float_v() {} // Constructor must be empty
      template <typename T, std::enable_if_t<std::is_integral_v<T>, int> = 0>
      constexpr float_v( T f ) : data( f ) {}
      constexpr float_v( float f ) : data{f} {}
      constexpr float_v( const float* f ) : float_v{*f} {}
      constexpr float_v( const_span f ) : float_v{f[0]} {}

      constexpr float_v& operator=( float f ) {
        data = f;
        return *this;
      }

      constexpr explicit operator int_v() const;

      constexpr float    cast() const { return data; }
      constexpr explicit operator float() const { return cast(); }

      constexpr const float_v& store( float* ptr ) const { return store( span{ptr, size()} ); }
      constexpr const float_v& store( span s ) const {
        *s.data() = data;
        return *this;
      }

      constexpr const float_v& compressstore( mask_v mask, float* ptr ) const {
        return compressstore( mask, span{ptr, size()} );
      }
      constexpr const float_v& compressstore( mask_v mask, span s ) const {
        if ( mask.cast() ) { *s.data() = data; }
        return *this;
      }

      constexpr float hmax() const { return data; }
      constexpr float hmin() const { return data; }
      constexpr float hadd() const { return data; }
      constexpr float hmax( mask_v mask ) const { return mask.cast() ? data : std::numeric_limits<float>::lowest(); }
      constexpr float hmin( mask_v mask ) const { return mask.cast() ? data : std::numeric_limits<float>::max(); }
      constexpr float hadd( mask_v mask ) const { return mask.cast() ? data : 0; }

      constexpr float_v& operator+=( float_v rhs ) {
        this->data += rhs.data;
        return *this;
      }
      constexpr float_v& operator-=( float_v rhs ) {
        this->data -= rhs.data;
        return *this;
      }
      constexpr float_v& operator*=( float_v rhs ) {
        this->data *= rhs.data;
        return *this;
      }
      constexpr float_v& operator/=( float_v rhs ) {
        this->data /= rhs.data;
        return *this;
      }

      constexpr friend float_v operator+( float_v lhs, float_v rhs ) { return lhs.data + rhs.data; }
      constexpr friend float_v operator-( float_v lhs, float_v rhs ) { return lhs.data - rhs.data; }
      constexpr friend float_v operator*( float_v lhs, float_v rhs ) { return lhs.data * rhs.data; }
      constexpr friend float_v operator/( float_v lhs, float_v rhs ) { return lhs.data / rhs.data; }
      constexpr friend float_v operator-( float_v x ) { return -1.f * x; }

      constexpr friend float_v abs( float_v v ) { return std::abs( v.data ); }
      constexpr friend float_v copysign( float_v x, float_v y ) { return std::copysign( x.data, y.data ); }

      friend float_v sqrt( float_v v ) { return std::sqrt( v.data ); }
      [[deprecated( "Be aware, the SIMD version yields different results for AMD and Intel" )]] friend float_v
      rsqrt( float_v v ) {
        return 1.f / std::sqrt( v.data );
      }
      [[deprecated( "Be aware, the SIMD version yields different results for AMD and Intel" )]] friend float_v
      rcp( float_v v ) {
        return 1.f / v.data;
      }

      friend float_v min( float_v lhs, float_v rhs ) { return std::min( lhs.data, rhs.data ); }
      friend float_v max( float_v lhs, float_v rhs ) { return std::max( lhs.data, rhs.data ); }

      constexpr friend float_v signselect( float_v s, float_v a, float_v b ) { return ( s.data > 0 ) ? a : b; }
      constexpr friend float_v select( mask_v mask, float_v a, float_v b ) { return mask.cast() ? a : b; }

      constexpr friend mask_v operator<( float_v lhs, float_v rhs ) { return lhs.data < rhs.data; }
      constexpr friend mask_v operator>( float_v lhs, float_v rhs ) { return lhs.data > rhs.data; }
      constexpr friend mask_v operator<=( float_v lhs, float_v rhs ) { return !( lhs > rhs ); }
      constexpr friend mask_v operator>=( float_v lhs, float_v rhs ) { return !( lhs < rhs ); }
      constexpr friend mask_v operator==( float_v lhs, float_v rhs ) { return lhs.data == rhs.data; }
      friend std::ostream&    operator<<( std::ostream& os, float_v x ) { return os << x.cast(); }

    private:
      float data{};
    };

    class int_v {
    public:
      constexpr static std::size_t size() { return scalar::types::size; }
      using span       = LHCb::span<int, scalar::types::size>;
      using const_span = LHCb::span<const int, scalar::types::size>;

      constexpr int_v() {} // Constructor must be empty
      constexpr int_v( int f ) : data{f} {}
      constexpr int_v( const int* f ) : int_v{*f} {}
      constexpr int_v( const_span f ) : int_v{f.front()} {}

      constexpr int_v& operator=( int f ) {
        data = f;
        return *this;
      }

      constexpr          operator float_v() const { return float_v{float( data )}; }
      constexpr int      cast() const { return data; } // don't cast directly to avoid conflict
      constexpr explicit operator int() const { return cast(); }

      constexpr const int_v& store( int* ptr ) const { return store( span{ptr, size()} ); }
      constexpr const int_v& store( span s ) const {
        *s.data() = data;
        return *this;
      }

      constexpr const int_v& compressstore( mask_v mask, int* ptr ) const {
        return compressstore( mask, span{ptr, size()} );
      }
      constexpr const int_v& compressstore( mask_v mask, span s ) const {
        if ( mask.cast() ) { *s.data() = data; }
        return *this;
      }

      constexpr int hmax() const { return data; }
      constexpr int hmin() const { return data; }
      constexpr int hadd() const { return data; }
      constexpr int hmax( mask_v mask ) const { return mask.cast() ? data : std::numeric_limits<int>::min(); }
      constexpr int hmin( mask_v mask ) const { return mask.cast() ? data : std::numeric_limits<int>::max(); }
      constexpr int hadd( mask_v mask ) const { return mask.cast() ? data : 0; }

      constexpr int_v& operator+=( int_v rhs ) {
        this->data += rhs.data;
        return *this;
      }
      constexpr int_v& operator-=( int_v rhs ) {
        this->data -= rhs.data;
        return *this;
      }
      constexpr int_v& operator*=( int_v rhs ) {
        this->data *= rhs.data;
        return *this;
      }

      constexpr friend int_v operator+( int_v lhs, int_v rhs ) { return lhs.data + rhs.data; }
      constexpr friend int_v operator-( int_v lhs, int_v rhs ) { return lhs.data - rhs.data; }
      constexpr friend int_v operator*( int_v lhs, int_v rhs ) { return lhs.data * rhs.data; }
      constexpr friend int_v operator-( int_v x ) { return -1 * x; }

      constexpr friend int_v operator&( int_v lhs, int_v rhs ) { return lhs.data & rhs.data; }
      constexpr friend int_v operator|( int_v lhs, int_v rhs ) { return lhs.data | rhs.data; }
      constexpr friend int_v operator^( int_v lhs, int_v rhs ) { return lhs.data ^ rhs.data; }
      constexpr friend int_v operator~( int_v x ) { return ~x.data; }

      constexpr friend int_v operator<<( int_v lhs, int_v rhs ) { return lhs.data << rhs.data; }
      constexpr friend int_v operator>>( int_v lhs, int_v rhs ) { return lhs.data >> rhs.data; }

      constexpr friend int_v min( int_v lhs, int_v rhs ) { return std::min( lhs.data, rhs.data ); }
      constexpr friend int_v max( int_v lhs, int_v rhs ) { return std::max( lhs.data, rhs.data ); }
      friend int_v           abs( int_v v ) { return std::abs( v.data ); } // std::abs(int) is not declared as constexpr

      constexpr friend int_v signselect( float_v s, int_v a, int_v b ) { return ( s > float_v{0.f} ).cast() ? a : b; }
      constexpr friend int_v select( mask_v mask, int_v a, int_v b ) { return mask.cast() ? a : b; }

      constexpr friend mask_v operator<( int_v lhs, int_v rhs ) { return lhs.data < rhs.data; }
      constexpr friend mask_v operator>( int_v lhs, int_v rhs ) { return lhs.data > rhs.data; }
      constexpr friend mask_v operator<=( int_v lhs, int_v rhs ) { return lhs.data <= rhs.data; }
      constexpr friend mask_v operator>=( int_v lhs, int_v rhs ) { return lhs.data >= rhs.data; }
      constexpr friend mask_v operator==( int_v lhs, int_v rhs ) { return lhs.data == rhs.data; }
      constexpr friend mask_v operator!=( int_v lhs, int_v rhs ) { return lhs.data != rhs.data; }

      friend std::ostream& operator<<( std::ostream& os, int_v x ) { return os << x.cast(); }

    private:
      int data{};
    };

    constexpr mask_v::operator int_v() const { return data; }

    constexpr float_v::operator int_v() const { return int_v{int( data )}; }

    constexpr int_v castToInt( float_v x ) { return bit_cast<int>( x.cast() ); }

    constexpr float_v castToFloat( int_v x ) { return bit_cast<float>( x.cast() ); }

    constexpr int_v gather( const int* base, int_v idx ) { return base[idx.cast()]; }

    constexpr float_v gather( const float* base, int_v idx ) { return base[idx.cast()]; }

    constexpr int_v maskgather( const int* base, int_v idx, mask_v mask, int_v source ) {
      if ( mask.cast() ) return int_v{base[idx.cast()]};
      return source;
    }

    constexpr float_v maskgather( const float* base, int_v idx, mask_v mask, float_v source ) {
      if ( mask.cast() ) return float_v{base[idx.cast()]};
      return source;
    }

    inline void scatter( int* base, int_v idx, int_v value ) { base[idx.cast()] = value.cast(); }

    inline void scatter( float* base, int_v idx, float_v value ) { base[idx.cast()] = value.cast(); }

    inline void maskscatter( int* base, int_v idx, mask_v mask, int_v value ) {
      if ( mask.cast() ) base[idx.cast()] = value.cast();
    }

    inline void maskscatter( float* base, int_v idx, mask_v mask, float_v value ) {
      if ( mask.cast() ) base[idx.cast()] = value.cast();
    }

    inline int_v types::indices() { return 0; }

    inline int_v types::indices( int start ) { return start; }

    constexpr int_v operator+( mask_v lhs, mask_v rhs ) { return int_v{lhs} + int_v{rhs}; }
    // TODO: c++20 std::popcount
    constexpr int_v popcount_v( int_v a ) { return __builtin_popcount( a.cast() ); }
  } // namespace scalar

#ifndef __aarch64__
  namespace neon {
    constexpr InstructionSet instructionSet() { return scalar::instructionSet(); }
    using float_v = scalar::float_v;
    using mask_v  = scalar::mask_v;
    using int_v   = scalar::int_v;
    using types   = scalar::types;
  } // namespace neon
#else
  namespace { // Helpers
    uint32_t movemask_u32( uint32x4_t mask ) {
      const uint32x4_t movemask = {1, 2, 4, 8};
      return vaddvq_u32( vandq_u32( mask, movemask ) );
    }
  } // namespace

  namespace neon {
    constexpr InstructionSet instructionSet() { return Neon; }
    // Permutation for sse compress, from https://github.com/lemire/simdprune
    alignas( 16 ) constexpr uint8_t compress_mask128_epi32[] = {
        0x0,  0x1,  0x2,  0x3,  0x4,  0x5,  0x6,  0x7,  0x8,  0x9, 0xa, 0xb, 0xc,  0xd,  0xe,  0xf,  0x4,  0x5,  0x6,
        0x7,  0x8,  0x9,  0xa,  0xb,  0xc,  0xd,  0xe,  0xf,  0xc, 0xd, 0xe, 0xf,  0x0,  0x1,  0x2,  0x3,  0x8,  0x9,
        0xa,  0xb,  0xc,  0xd,  0xe,  0xf,  0xc,  0xd,  0xe,  0xf, 0x8, 0x9, 0xa,  0xb,  0xc,  0xd,  0xe,  0xf,  0xc,
        0xd,  0xe,  0xf,  0xc,  0xd,  0xe,  0xf,  0x0,  0x1,  0x2, 0x3, 0x4, 0x5,  0x6,  0x7,  0xc,  0xd,  0xe,  0xf,
        0xc,  0xd,  0xe,  0xf,  0x4,  0x5,  0x6,  0x7,  0xc,  0xd, 0xe, 0xf, 0xc,  0xd,  0xe,  0xf,  0xc,  0xd,  0xe,
        0xf,  0x0,  0x1,  0x2,  0x3,  0xc,  0xd,  0xe,  0xf,  0xc, 0xd, 0xe, 0xf,  0xc,  0xd,  0xe,  0xf,  0xc,  0xd,
        0xe,  0xf,  0xc,  0xd,  0xe,  0xf,  0xc,  0xd,  0xe,  0xf, 0xc, 0xd, 0xe,  0xf,  0x0,  0x1,  0x2,  0x3,  0x4,
        0x5,  0x6,  0x7,  0x8,  0x9,  0xa,  0xb,  0x8,  0x9,  0xa, 0xb, 0x4, 0x5,  0x6,  0x7,  0x8,  0x9,  0xa,  0xb,
        0x8,  0x9,  0xa,  0xb,  0x8,  0x9,  0xa,  0xb,  0x0,  0x1, 0x2, 0x3, 0x8,  0x9,  0xa,  0xb,  0x8,  0x9,  0xa,
        0xb,  0x8,  0x9,  0xa,  0xb,  0x8,  0x9,  0xa,  0xb,  0x8, 0x9, 0xa, 0xb,  0x8,  0x9,  0xa,  0xb,  0x8,  0x9,
        0xa,  0xb,  0x0,  0x1,  0x2,  0x3,  0x4,  0x5,  0x6,  0x7, 0x4, 0x5, 0x6,  0x7,  0x4,  0x5,  0x6,  0x7,  0x4,
        0x5,  0x6,  0x7,  0x4,  0x5,  0x6,  0x7,  0x4,  0x5,  0x6, 0x7, 0x4, 0x5,  0x6,  0x7,  0x0,  0x1,  0x2,  0x3,
        0x0,  0x1,  0x2,  0x3,  0x0,  0x1,  0x2,  0x3,  0x0,  0x1, 0x2, 0x3, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    };

    class int_v;

    class float_v;

    class mask_v {
    public:
      constexpr static std::size_t size() { return 4; }

      mask_v() {} // Constructor must be empty
      mask_v( uint32x4_t m ) : data{m} {}
      explicit mask_v( bool v ) : data{vdupq_n_u32( v ? -1 : 0 )} {}

      mask_v& operator=( uint32x4_t m ) {
        data = m;
        return *this;
      }

      const mask_v& store( float* ptr ) const { return store( LHCb::span<float, 4>{ptr, size()} ); } // Non-standard
      const mask_v& store( LHCb::span<float, 4> s ) const {
        vst1q_u32( reinterpret_cast<uint32_t*>( s.data() ), data );
        return *this;
      } // Non-standard

      operator int_v() const;

      friend mask_v operator&( mask_v lhs, mask_v rhs ) { return vandq_u32( lhs.native(), rhs.native() ); }
      friend mask_v operator|( mask_v lhs, mask_v rhs ) { return vorrq_u32( lhs.native(), rhs.native() ); }
      friend mask_v operator^( mask_v lhs, mask_v rhs ) { return veorq_u32( lhs.native(), rhs.native() ); }

      friend mask_v operator&&( mask_v lhs, mask_v rhs ) { return vandq_u32( lhs.native(), rhs.native() ); }
      friend mask_v operator||( mask_v lhs, mask_v rhs ) { return vorrq_u32( lhs.native(), rhs.native() ); }
      friend mask_v operator!( mask_v x ) { return veorq_u32( x.native(), vdupq_n_u32( -1 ) ); }

      friend bool all( mask_v mask ) { return movemask_u32( mask.native() ) == 0xF; }
      friend bool none( mask_v mask ) { return movemask_u32( mask.native() ) == 0x0; }
      friend bool any( mask_v mask ) { return movemask_u32( mask.native() ) != 0x0; }
      friend bool testbit( mask_v mask, int bit ) { return ( movemask_u32( mask.native() ) & ( 1 << bit ) ) != 0; }
      // TODO: c++20 countl_zero
      friend int ffs( mask_v mask ) { return __builtin_ffs( movemask_u32( mask.native() ) ); }
      // TODO: c++20 std::popcount
      friend int popcount( mask_v mask ) { return __builtin_popcount( movemask_u32( mask.native() ) ); }

      friend std::ostream& operator<<( std::ostream& os, mask_v x ) {
        return print_bitset<4>( os, static_cast<unsigned long long>( movemask_u32( x.native() ) ), "neon" );
      }

      uint32x4_t native() const { return data; }

    private:
      uint32x4_t data{};
    };

    struct types {
      constexpr static std::size_t size = 4;
      using int_v                       = neon::int_v;
      using float_v                     = neon::float_v;
      using mask_v                      = neon::mask_v;
      static mask_v mask_true() { return mask_v{true}; }
      static mask_v mask_false() { return mask_v{false}; }
      static int_v  indices();
      static int_v  indices( int start );
      static mask_v loop_mask( int i, int n ) { return vcltq_s32( int32x4_t{0, 1, 2, 3}, vdupq_n_s32( n - i ) ); }
      static constexpr InstructionSet instructionSet() { return Neon; }
    };

    class float_v {
    public:
      constexpr static std::size_t size() { return neon::types::size; }
      using span       = LHCb::span<float, neon::types::size>;
      using const_span = LHCb::span<const float, neon::types::size>;

      float_v() {} // Constructor must be empty
      float_v( scalar::float_v f ) : data{vdupq_n_f32( f.cast() )} {}
      float_v( float f ) : data{vdupq_n_f32( f )} {}
      template <typename T, std::enable_if_t<std::is_integral_v<T>, int> = 0>
      float_v( T f ) : float_v{float( f )} {}
      float_v( const float* f ) : float_v{vld1q_f32( f )} {}
      float_v( const_span f ) : float_v{vld1q_f32( f.data() )} {}
      float_v( float32x4_t f ) : data{f} {}

      float_v& operator=( float32x4_t f ) {
        data = f;
        return *this;
      }

               operator float32x4_t() const { return data; }
      explicit operator int_v() const;

      const float_v& store( float* ptr ) const { return store( span{ptr, size()} ); }
      const float_v& store( span s ) const {
        vst1q_f32( s.data(), data );
        return *this;
      }

      const float_v& compressstore( mask_v mask, float* ptr ) const { return compressstore( mask, span{ptr, size()} ); }

      const float_v& compressstore( mask_v mask, span s ) const {
        uint8x16_t perm   = vld1q_u8( compress_mask128_epi32 + 16 * ( movemask_u32( mask.native() ) ^ 0xF ) );
        uint8x16_t reshuf = vqtbl1q_u8( vreinterpretq_u8_f32( data ), perm );
        vst1q_f32( s.data(), vreinterpretq_f32_u8( reshuf ) );
        return *this;
      }

      float hmax() const { return vmaxvq_f32( data ); }
      float hmin() const { return vminvq_f32( data ); }
      float hadd() const { return vaddvq_f32( data ); }
      float hmax( mask_v mask ) const { return select( mask, *this, std::numeric_limits<float>::lowest() ).hmax(); }
      float hmin( mask_v mask ) const { return select( mask, *this, std::numeric_limits<float>::max() ).hmin(); }
      float hadd( mask_v mask ) const { return select( mask, *this, 0.f ).hadd(); }

      float_v& operator+=( float_v rhs ) {
        *this = *this + rhs;
        return *this;
      }
      float_v& operator-=( float_v rhs ) {
        *this = *this - rhs;
        return *this;
      }
      float_v& operator*=( float_v rhs ) {
        *this = *this * rhs;
        return *this;
      }
      float_v& operator/=( float_v rhs ) {
        *this = *this / rhs;
        return *this;
      }

      friend float_v operator+( float_v lhs, float_v rhs ) { return vaddq_f32( lhs, rhs ); }
      friend float_v operator-( float_v lhs, float_v rhs ) { return vsubq_f32( lhs, rhs ); }
      friend float_v operator*( float_v lhs, float_v rhs ) { return vmulq_f32( lhs, rhs ); }
      friend float_v operator/( float_v lhs, float_v rhs ) {
        float32x4_t reciprocal = vrecpeq_f32( rhs );
        reciprocal             = vmulq_f32( vrecpsq_f32( rhs, reciprocal ), reciprocal );
        reciprocal             = vmulq_f32( vrecpsq_f32( rhs, reciprocal ), reciprocal );
        return vmulq_f32( lhs, reciprocal );
      }
      friend float_v operator-( float_v x ) { return -1.f * x; }

      friend float_v operator&( float_v lhs, float_v rhs ) {
        return vreinterpretq_f32_u32( vandq_u32( vreinterpretq_u32_f32( lhs ), vreinterpretq_u32_f32( rhs ) ) );
      }
      friend float_v operator|( float_v lhs, float_v rhs ) {
        return vreinterpretq_f32_u32( vorrq_u32( vreinterpretq_u32_f32( lhs ), vreinterpretq_u32_f32( rhs ) ) );
      }
      friend float_v operator^( float_v lhs, float_v rhs ) {
        return vreinterpretq_f32_u32( veorq_u32( vreinterpretq_u32_f32( lhs ), vreinterpretq_u32_f32( rhs ) ) );
      }

      friend float_v operator&&( float_v lhs, float_v rhs ) {
        return vreinterpretq_f32_u32( vandq_u32( vreinterpretq_u32_f32( lhs ), vreinterpretq_u32_f32( rhs ) ) );
      }
      friend float_v operator||( float_v lhs, float_v rhs ) {
        return vreinterpretq_f32_u32( vorrq_u32( vreinterpretq_u32_f32( lhs ), vreinterpretq_u32_f32( rhs ) ) );
      }
      friend float_v operator!( float_v x ) { return x ^ vreinterpretq_f32_u32( vdupq_n_u32( -1 ) ); }

      friend float_v min( float_v lhs, float_v rhs ) { return vminq_f32( lhs, rhs ); }
      friend float_v max( float_v lhs, float_v rhs ) { return vmaxq_f32( lhs, rhs ); }
      friend float_v abs( float_v v ) { return vabsq_f32( v ); }
      friend float_v copysign( float_v x, float_v y ) {
        return x ^ ( y & vreinterpretq_f32_u32( vdupq_n_u32( 0x80000000 ) ) );
      }

      friend float_v signselect( float_v s, float_v a, float_v b ) {
        return vbslq_f32( ( s < float_v{0.f} ).native(), b, a );
      }
      friend float_v select( mask_v mask, float_v a, float_v b ) { // vbslq_u32
        return vbslq_f32( mask.native(), a, b );
      }

      friend float_v sqrt( float_v v ) {
        float32x4_t rsqrt = vrsqrteq_f32( v );
        rsqrt             = vmulq_f32( vrsqrtsq_f32( vmulq_f32( v, rsqrt ), rsqrt ), rsqrt );
        rsqrt             = vmulq_f32( vrsqrtsq_f32( vmulq_f32( v, rsqrt ), rsqrt ), rsqrt );
        return vmulq_f32( v, rsqrt );
      }
      [[deprecated( "Be aware, yields different results for AMD and Intel" )]] friend float_v rsqrt( float_v v ) {
        return vrsqrteq_f32( v );
      }
      [[deprecated( "Be aware, yields different results for AMD and Intel" )]] friend float_v rcp( float_v v ) {
        return vrecpeq_f32( v );
      }

      friend mask_v        operator<( float_v lhs, float_v rhs ) { return vcltq_f32( lhs, rhs ); }
      friend mask_v        operator>( float_v lhs, float_v rhs ) { return vcgtq_f32( lhs, rhs ); }
      friend mask_v        operator==( float_v lhs, float_v rhs ) { return vceqq_f32( lhs, rhs ); }
      friend mask_v        operator<=( float_v lhs, float_v rhs ) { return !( lhs > rhs ); }
      friend mask_v        operator>=( float_v lhs, float_v rhs ) { return !( lhs < rhs ); }
      friend std::ostream& operator<<( std::ostream& os, float_v x ) { return print_vector<float, 4>( os, x, "neon" ); }

    private:
      float32x4_t data{};
    };

    class int_v {
    public:
      constexpr static std::size_t size() { return neon::types::size; }
      using span       = LHCb::span<int, neon::types::size>;
      using const_span = LHCb::span<const int, neon::types::size>;

      int_v() {} // Constructor must be empty
      int_v( int f ) : data{vdupq_n_s32( f )} {}
      int_v( const int* f ) : int_v{vld1q_s32( f )} {}
      int_v( const_span f ) : int_v{vld1q_s32( f.data() )} {}
      int_v( int32x4_t f ) : data{f} {}

      int_v& operator=( int32x4_t f ) {
        data = f;
        return *this;
      }

      operator int32x4_t() const { return data; }
      operator float_v() const { return float_v{vcvtq_f32_s32( data )}; }

      const int_v& store( int* ptr ) const { return store( span{ptr, size()} ); }
      const int_v& store( span s ) const {
        vst1q_s32( s.data(), data );
        return *this;
      }

      const int_v& compressstore( mask_v mask, int* ptr ) const { return compressstore( mask, span{ptr, size()} ); }

      const int_v& compressstore( mask_v mask, span s ) const {
        uint8x16_t perm   = vld1q_u8( compress_mask128_epi32 + 16 * ( movemask_u32( mask.native() ) ^ 0xF ) );
        uint8x16_t reshuf = vqtbl1q_u8( vreinterpretq_u8_s32( data ), perm );
        vst1q_s32( s.data(), vreinterpretq_s32_u8( reshuf ) );
        return *this;
      }

      int hmax() const { return vmaxvq_s32( data ); }
      int hmin() const { return vminvq_s32( data ); }
      int hadd() const { return vaddvq_s32( data ); }
      int hmax( mask_v mask ) const { return select( mask, *this, std::numeric_limits<int>::min() ).hmax(); }
      int hmin( mask_v mask ) const { return select( mask, *this, std::numeric_limits<int>::max() ).hmin(); }
      int hadd( mask_v mask ) const { return select( mask, *this, 0 ).hadd(); }

      int_v& operator+=( int_v rhs ) {
        *this = *this + rhs;
        return *this;
      }
      int_v& operator-=( int_v rhs ) {
        *this = *this - rhs;
        return *this;
      }
      int_v& operator*=( int_v rhs ) {
        *this = *this * rhs;
        return *this;
      }

      friend int_v operator+( int_v lhs, int_v rhs ) { return vaddq_s32( lhs, rhs ); }
      friend int_v operator-( int_v lhs, int_v rhs ) { return vsubq_s32( lhs, rhs ); }
      friend int_v operator*( int_v lhs, int_v rhs ) { return vmulq_s32( lhs, rhs ); }

      friend int_v operator-( int_v x ) { return -1 * x; }

      friend int_v operator&( int_v lhs, int_v rhs ) { return vandq_s32( lhs, rhs ); }
      friend int_v operator|( int_v lhs, int_v rhs ) { return vorrq_s32( lhs, rhs ); }
      friend int_v operator^( int_v lhs, int_v rhs ) { return veorq_s32( lhs, rhs ); }
      friend int_v operator~( int_v x ) { return vmvnq_s32( x ); }

      friend int_v operator<<( int_v lhs, int rhs ) { return vshlq_s32( lhs, vdupq_n_s32( rhs ) ); }
      friend int_v operator>>( int_v lhs, int rhs ) { return vshlq_s32( lhs, vdupq_n_s32( -rhs ) ); }

      friend int_v operator<<( int_v lhs, int_v rhs ) { return vshlq_s32( lhs, rhs ); }
      friend int_v operator>>( int_v lhs, int_v rhs ) { return vshlq_s32( lhs, -rhs ); }

      friend int_v min( int_v lhs, int_v rhs ) { return vminq_s32( lhs, rhs ); }
      friend int_v max( int_v lhs, int_v rhs ) { return vmaxq_s32( lhs, rhs ); }
      friend int_v abs( int_v v ) { return vabsq_s32( v ); }

      friend int_v signselect( float_v s, int_v a, int_v b ) {
        return vbslq_s32( ( s < float_v{0.f} ).native(), b, a );
      }
      friend int_v select( mask_v mask, int_v a, int_v b ) { return vbslq_s32( mask.native(), a, b ); }

      friend mask_v        operator<( int_v lhs, int_v rhs ) { return vcltq_s32( lhs, rhs ); }
      friend mask_v        operator>( int_v lhs, int_v rhs ) { return vcgtq_s32( lhs, rhs ); }
      friend mask_v        operator<=( int_v lhs, int_v rhs ) { return !( lhs > rhs ); }
      friend mask_v        operator>=( int_v lhs, int_v rhs ) { return !( lhs < rhs ); }
      friend mask_v        operator==( int_v lhs, int_v rhs ) { return vceqq_s32( lhs, rhs ); }
      friend std::ostream& operator<<( std::ostream& os, int_v x ) { return print_vector<int, 4>( os, x, "neon" ); }

    private:
      int32x4_t data{};
    };
  } // namespace neon

  namespace simd_traits {
    template <>
    struct traits<neon::int_v> {
      constexpr static bool is_specialized   = true;
      constexpr static bool is_simd_integral = true;
    };
  } // namespace simd_traits

  namespace neon {
    using details::pow;

    inline float_v::operator int_v() const { return int_v{vcvtq_s32_f32( data )}; }
    inline mask_v:: operator int_v() const { return select( *this, int_v{1}, int_v{0} ); }

    inline int_v castToInt( float_v x ) { return int_v{vreinterpretq_s32_f32( x )}; }

    inline float_v castToFloat( int_v x ) { return float_v{vreinterpretq_f32_s32( x )}; }

    inline int_v gather( const int* base, int_v idx ) {
      std::array<int, 4> idx_v;
      std::array<int, 4> val_v;
      idx.store( idx_v );
      for ( int i = 0; i < 4; i++ ) val_v[i] = base[idx_v[i]];
      return int_v{val_v};
    }

    inline float_v gather( const float* base, int_v idx ) {
      std::array<int, 4>   idx_v;
      std::array<float, 4> val_v;
      idx.store( idx_v );
      for ( int i = 0; i < 4; i++ ) val_v[i] = base[idx_v[i]];
      return float_v{val_v};
    }

    inline int_v maskgather( const int* base, int_v idx, mask_v mask, int_v source ) {
      std::array<int, 4> idx_v;
      std::array<int, 4> val_v;
      int                m_v = movemask_u32( mask.native() );
      idx.store( idx_v );
      for ( int i = 0; i < 4; i++ ) {
        if ( m_v & 1 ) val_v[i] = base[idx_v[i]];
        m_v >>= 1;
      }
      return select( mask, int_v{val_v}, source );
    }

    inline float_v maskgather( const float* base, int_v idx, mask_v mask, float_v source ) {
      std::array<int, 4>   idx_v;
      std::array<float, 4> val_v;
      int                  m_v = movemask_u32( mask.native() );
      idx.store( idx_v );
      for ( int i = 0; i < 4; i++ ) {
        if ( m_v & 1 ) val_v[i] = base[idx_v[i]];
        m_v >>= 1;
      }
      return select( mask, float_v{val_v}, source );
    }

    inline void scatter( int* base, int_v idx, int_v value ) {
      std::array<int, 4> idx_v;
      std::array<int, 4> val_v;
      idx.store( idx_v );
      value.store( val_v );
      for ( int i = 0; i < 4; i++ ) base[idx_v[i]] = val_v[i];
    }

    inline void scatter( float* base, int_v idx, float_v value ) {
      std::array<int, 4>   idx_v;
      std::array<float, 4> val_v;
      idx.store( idx_v );
      value.store( val_v );
      for ( int i = 0; i < 4; i++ ) base[idx_v[i]] = val_v[i];
    }

    inline void maskscatter( int* base, int_v idx, mask_v mask, int_v value ) {
      std::array<int, 4> idx_v;
      std::array<int, 4> val_v;
      int                m_v = movemask_u32( mask.native() );
      idx.store( idx_v );
      value.store( val_v );
      for ( int i = 0; i < 4; i++ ) {
        if ( m_v & 1 ) val_v[i] = base[idx_v[i]];
        m_v >>= 1;
      }
    }

    inline void maskscatter( float* base, int_v idx, mask_v mask, float_v value ) {
      std::array<int, 4>   idx_v;
      std::array<float, 4> val_v;
      int                  m_v = movemask_u32( mask.native() );
      idx.store( idx_v );
      value.store( val_v );
      for ( int i = 0; i < 4; i++ ) {
        if ( m_v & 1 ) val_v[i] = base[idx_v[i]];
        m_v >>= 1;
      }
    }

    inline int_v types::indices() { return int32x4_t{0, 1, 2, 3}; }
    inline int_v types::indices( int start ) { return indices() + start; }
    inline int_v operator+( mask_v lhs, mask_v rhs ) { return int_v{lhs} + int_v{rhs}; }
    inline int_v popcount_v( int_v a ) { return SIMDWrapper::popcount_v( a ); }
  } // namespace neon

} // namespace SIMDWrapper

SIMDWRAPPER_SPECIALISE_HELPER_TYPES( SIMDWrapper::neon );

namespace SIMDWrapper {
#endif

#ifndef __SSE4_2__
  namespace sse {
    constexpr InstructionSet instructionSet() { return neon::instructionSet(); }
    using float_v = neon::float_v;
    using mask_v  = neon::mask_v;
    using int_v   = neon::int_v;
    using types   = neon::types;
  } // namespace sse
#else
  namespace sse {
    constexpr InstructionSet instructionSet() { return SSE; }
    // Permutation for sse compress, from https://github.com/lemire/simdprune
    alignas( 16 ) constexpr uint8_t compress_mask128_epi32[] = {
        0x0,  0x1,  0x2,  0x3,  0x4,  0x5,  0x6,  0x7,  0x8,  0x9, 0xa, 0xb, 0xc,  0xd,  0xe,  0xf,  0x4,  0x5,  0x6,
        0x7,  0x8,  0x9,  0xa,  0xb,  0xc,  0xd,  0xe,  0xf,  0xc, 0xd, 0xe, 0xf,  0x0,  0x1,  0x2,  0x3,  0x8,  0x9,
        0xa,  0xb,  0xc,  0xd,  0xe,  0xf,  0xc,  0xd,  0xe,  0xf, 0x8, 0x9, 0xa,  0xb,  0xc,  0xd,  0xe,  0xf,  0xc,
        0xd,  0xe,  0xf,  0xc,  0xd,  0xe,  0xf,  0x0,  0x1,  0x2, 0x3, 0x4, 0x5,  0x6,  0x7,  0xc,  0xd,  0xe,  0xf,
        0xc,  0xd,  0xe,  0xf,  0x4,  0x5,  0x6,  0x7,  0xc,  0xd, 0xe, 0xf, 0xc,  0xd,  0xe,  0xf,  0xc,  0xd,  0xe,
        0xf,  0x0,  0x1,  0x2,  0x3,  0xc,  0xd,  0xe,  0xf,  0xc, 0xd, 0xe, 0xf,  0xc,  0xd,  0xe,  0xf,  0xc,  0xd,
        0xe,  0xf,  0xc,  0xd,  0xe,  0xf,  0xc,  0xd,  0xe,  0xf, 0xc, 0xd, 0xe,  0xf,  0x0,  0x1,  0x2,  0x3,  0x4,
        0x5,  0x6,  0x7,  0x8,  0x9,  0xa,  0xb,  0x8,  0x9,  0xa, 0xb, 0x4, 0x5,  0x6,  0x7,  0x8,  0x9,  0xa,  0xb,
        0x8,  0x9,  0xa,  0xb,  0x8,  0x9,  0xa,  0xb,  0x0,  0x1, 0x2, 0x3, 0x8,  0x9,  0xa,  0xb,  0x8,  0x9,  0xa,
        0xb,  0x8,  0x9,  0xa,  0xb,  0x8,  0x9,  0xa,  0xb,  0x8, 0x9, 0xa, 0xb,  0x8,  0x9,  0xa,  0xb,  0x8,  0x9,
        0xa,  0xb,  0x0,  0x1,  0x2,  0x3,  0x4,  0x5,  0x6,  0x7, 0x4, 0x5, 0x6,  0x7,  0x4,  0x5,  0x6,  0x7,  0x4,
        0x5,  0x6,  0x7,  0x4,  0x5,  0x6,  0x7,  0x4,  0x5,  0x6, 0x7, 0x4, 0x5,  0x6,  0x7,  0x0,  0x1,  0x2,  0x3,
        0x0,  0x1,  0x2,  0x3,  0x0,  0x1,  0x2,  0x3,  0x0,  0x1, 0x2, 0x3, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    };

    class int_v;

    class float_v;

    class mask_v {
    public:
      constexpr static std::size_t size() { return 4; }

      mask_v() {} // Constructor must be empty
      mask_v( __m128 m ) : data{m} {}
      explicit mask_v( bool v ) : data{_mm_castsi128_ps( _mm_set1_epi32( v ? -1 : 0 ) )} {}

      mask_v& operator=( __m128 m ) {
        data = m;
        return *this;
      }

      const mask_v& store( float* ptr ) const {
        return store( LHCb::span<float, size()>{ptr, size()} );
      } // Non-standard
      const mask_v& store( LHCb::span<float, 4> s ) const {
        _mm_storeu_ps( s.data(), data );
        return *this;
      } // Non-standard

      operator int_v() const;

      friend mask_v operator&( mask_v lhs, mask_v rhs ) { return _mm_and_ps( lhs.native(), rhs.native() ); }
      friend mask_v operator|( mask_v lhs, mask_v rhs ) { return _mm_or_ps( lhs.native(), rhs.native() ); }
      friend mask_v operator^( mask_v lhs, mask_v rhs ) { return _mm_xor_ps( lhs.native(), rhs.native() ); }

      friend mask_v operator&&( mask_v lhs, mask_v rhs ) { return _mm_and_ps( lhs.native(), rhs.native() ); }
      friend mask_v operator||( mask_v lhs, mask_v rhs ) { return _mm_or_ps( lhs.native(), rhs.native() ); }
      friend mask_v operator!( mask_v x ) { return _mm_xor_ps( x.native(), _mm_castsi128_ps( _mm_set1_epi32( -1 ) ) ); }

      friend bool all( mask_v mask ) { return _mm_movemask_ps( mask.native() ) == 0xF; }
      friend bool none( mask_v mask ) { return _mm_movemask_ps( mask.native() ) == 0x0; }
      friend bool any( mask_v mask ) { return _mm_movemask_ps( mask.native() ) != 0x0; }
      friend bool testbit( mask_v mask, int bit ) { return ( _mm_movemask_ps( mask.native() ) & ( 1 << bit ) ) != 0; }
      // TODO: c++20 countl_zero
      friend int ffs( mask_v mask ) { return __builtin_ffs( _mm_movemask_ps( mask.native() ) ); }
      friend int popcount( mask_v mask ) { return _mm_popcnt_u32( _mm_movemask_ps( mask.native() ) ); }

      friend std::ostream& operator<<( std::ostream& os, mask_v x ) {
        return print_bitset<4>( os, static_cast<unsigned long long>( _mm_movemask_ps( x.native() ) ), "sse" );
      }

      __m128 native() const { return data; }

    private:
      __m128 data{};
    };

    struct types {
      constexpr static std::size_t size = 4;
      using int_v = sse::int_v;
      using float_v = sse::float_v;
      using mask_v = sse::mask_v;
      static mask_v mask_true() { return mask_v{true}; }
      static mask_v mask_false() { return mask_v{false}; }
      static int_v indices();
      static int_v indices( int start );
      static mask_v loop_mask( int i, int n ) {
        return _mm_cmplt_ps( _mm_setr_ps( 0, 1, 2, 3 ), _mm_set1_ps( n - i ) );
      }
      static constexpr InstructionSet instructionSet() { return SSE; }
    };

    class float_v {
    public:
      constexpr static std::size_t size() { return sse::types::size; }
      using span = LHCb::span<float, sse::types::size>;
      using const_span = LHCb::span<const float, sse::types::size>;

      float_v() {} // Constructor must be empty
      float_v( scalar::float_v f ) : float_v{_mm_set1_ps( f.cast() )} {}
      float_v( float f ) : float_v{_mm_set1_ps( f )} {}
      template <typename T, std::enable_if_t<std::is_integral_v<T>, int> = 0>
      float_v( T f ) : float_v{float( f )} {}
      float_v( const float* f ) : float_v{_mm_loadu_ps( f )} {}
      float_v( const_span f ) : float_v{_mm_loadu_ps( f.data() )} {}
      float_v( __m128 f ) : data{f} {}

      float_v& operator=( __m128 f ) {
        data = f;
        return *this;
      }

      constexpr operator __m128() const { return data; }
      explicit operator int_v() const;

      const float_v& store( float* ptr ) const { return store( span{ptr, size()} ); }
      const float_v& store( span s ) const {
        _mm_storeu_ps( s.data(), data );
        return *this;
      }

      const float_v& compressstore( mask_v mask, float* ptr ) const { return compressstore( mask, span{ptr, size()} ); }
      const float_v& compressstore( mask_v mask, span s ) const {
        __m128i perm =
            _mm_load_si128( (const __m128i*)compress_mask128_epi32 + ( _mm_movemask_ps( mask.native() ) ^ 0xF ) );
        _mm_storeu_ps( s.data(), _mm_castsi128_ps( _mm_shuffle_epi8( _mm_castps_si128( data ), perm ) ) );
        return *this;
      }

      float hmax() const {
        __m128 r = data;
        r = _mm_max_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_max_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_cvtss_f32( r );
      }
      float hmin() const {
        __m128 r = data;
        r = _mm_min_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_min_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_cvtss_f32( r );
      }
      float hadd() const {
        __m128 r = data;
        r = _mm_add_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_add_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_cvtss_f32( r );
      }
      float hmax( mask_v mask ) const { return select( mask, *this, std::numeric_limits<float>::lowest() ).hmax(); }
      float hmin( mask_v mask ) const { return select( mask, *this, std::numeric_limits<float>::max() ).hmin(); }
      float hadd( mask_v mask ) const { return select( mask, *this, 0.f ).hadd(); }

      float_v& operator+=( float_v rhs ) {
        *this = *this + rhs;
        return *this;
      }
      float_v& operator-=( float_v rhs ) {
        *this = *this - rhs;
        return *this;
      }
      float_v& operator*=( float_v rhs ) {
        *this = *this * rhs;
        return *this;
      }
      float_v& operator/=( float_v rhs ) {
        *this = *this / rhs;
        return *this;
      }

      friend float_v operator+( float_v lhs, float_v rhs ) { return _mm_add_ps( lhs, rhs ); }
      friend float_v operator-( float_v lhs, float_v rhs ) { return _mm_sub_ps( lhs, rhs ); }
      friend float_v operator*( float_v lhs, float_v rhs ) { return _mm_mul_ps( lhs, rhs ); }
      friend float_v operator/( float_v lhs, float_v rhs ) { return _mm_div_ps( lhs, rhs ); }
      friend float_v operator-( float_v x ) { return -1.f * x; }

      friend float_v operator&( float_v lhs, float_v rhs ) { return _mm_and_ps( lhs, rhs ); }
      friend float_v operator|( float_v lhs, float_v rhs ) { return _mm_or_ps( lhs, rhs ); }
      friend float_v operator^( float_v lhs, float_v rhs ) { return _mm_xor_ps( lhs, rhs ); }

      friend float_v operator&&( float_v lhs, float_v rhs ) { return _mm_and_ps( lhs, rhs ); }
      friend float_v operator||( float_v lhs, float_v rhs ) { return _mm_or_ps( lhs, rhs ); }
      friend float_v operator!( float_v x ) { return x ^ _mm_castsi128_ps( _mm_set1_epi32( -1 ) ); }

      friend float_v min( float_v lhs, float_v rhs ) { return _mm_min_ps( lhs, rhs ); }
      friend float_v max( float_v lhs, float_v rhs ) { return _mm_max_ps( lhs, rhs ); }
      friend float_v abs( float_v v ) { return v & _mm_castsi128_ps( _mm_set1_epi32( 0x7FFFFFFF ) ); }
      friend float_v copysign( float_v x, float_v y ) {
        return x ^ ( y & _mm_castsi128_ps( _mm_set1_epi32( 0x80000000 ) ) );
      }

      friend float_v signselect( float_v s, float_v a, float_v b ) {
        return _mm_blendv_ps( a, b, ( s < float_v{0.f} ).native() ); //_mm_mask_mov_ps( a, s < float_v{ 0.f }, b );
      }
      friend float_v select( mask_v mask, float_v a, float_v b ) {
        return _mm_blendv_ps( b, a, mask.native() ); //_mm_mask_mov_ps( b, mask, a );
      }

      friend float_v sqrt( float_v v ) { return _mm_sqrt_ps( v ); }
      [[deprecated( "Be aware, yields different results for AMD and Intel" )]] friend float_v rsqrt( float_v v ) {
        return _mm_rsqrt_ps( v );
      }
      [[deprecated( "Be aware, yields different results for AMD and Intel" )]] friend float_v rcp( float_v v ) {
        return _mm_rcp_ps( v );
      }

      friend mask_v operator<( float_v lhs, float_v rhs ) { return _mm_cmplt_ps( lhs, rhs ); }
      friend mask_v operator>( float_v lhs, float_v rhs ) { return _mm_cmpgt_ps( lhs, rhs ); }
      friend mask_v operator<=( float_v lhs, float_v rhs ) { return !( lhs > rhs ); }
      friend mask_v operator>=( float_v lhs, float_v rhs ) { return !( lhs < rhs ); }
      friend mask_v operator==( float_v lhs, float_v rhs ) { return _mm_cmpeq_ps( lhs, rhs ); }
      friend std::ostream& operator<<( std::ostream& os, float_v x ) { return print_vector<float, 4>( os, x, "sse" ); }

    private:
      __m128 data{};
    };

    class int_v {
    public:
      constexpr static std::size_t size() { return sse::types::size; }
      using span = LHCb::span<int, sse::types::size>;
      using const_span = LHCb::span<const int, sse::types::size>;

      int_v() {} // Constructor must be empty
      int_v( int f ) : int_v{_mm_set1_epi32( f )} {}
      int_v( const int* f ) : int_v{_mm_loadu_si128( (__m128i*)f )} {}
      int_v( const_span f ) : int_v{_mm_loadu_si128( (__m128i*)f.data() )} {}
      int_v( __m128i f ) : data{f} {}

      int_v& operator=( __m128i f ) {
        data = f;
        return *this;
      }

      constexpr operator __m128i() const { return data; }
      operator float_v() const { return float_v{_mm_cvtepi32_ps( data )}; }

      const int_v& store( int* ptr ) const { return store( span{ptr, size()} ); }
      const int_v& store( span s ) const {
        _mm_storeu_si128( (__m128i*)s.data(), data );
        return *this;
      }

      const int_v& compressstore( mask_v mask, int* ptr ) const { return compressstore( mask, span{ptr, size()} ); }

      const int_v& compressstore( mask_v mask, span s ) const {
        __m128i perm =
            _mm_load_si128( (const __m128i*)compress_mask128_epi32 + ( _mm_movemask_ps( mask.native() ) ^ 0xF ) );
        _mm_storeu_si128( (__m128i*)s.data(), _mm_shuffle_epi8( data, perm ) );
        return *this;
      }

      int hmax() const {
        __m128i r = data;
        r = _mm_max_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_max_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_extract_epi32( r, 0 );
      }
      int hmin() const {
        __m128i r = data;
        r = _mm_min_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_min_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_extract_epi32( r, 0 );
      }
      int hadd() const {
        __m128i r = data;
        r = _mm_add_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_add_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_extract_epi32( r, 0 );
      }
      int hmax( mask_v mask ) const { return select( mask, *this, std::numeric_limits<int>::min() ).hmax(); }
      int hmin( mask_v mask ) const { return select( mask, *this, std::numeric_limits<int>::max() ).hmin(); }
      int hadd( mask_v mask ) const { return select( mask, *this, 0 ).hadd(); }

      int_v& operator+=( int_v rhs ) {
        *this = *this + rhs;
        return *this;
      }
      int_v& operator-=( int_v rhs ) {
        *this = *this - rhs;
        return *this;
      }
      int_v& operator*=( int_v rhs ) {
        *this = *this * rhs;
        return *this;
      }

      friend int_v operator+( int_v lhs, int_v rhs ) { return _mm_add_epi32( lhs, rhs ); }
      friend int_v operator-( int_v lhs, int_v rhs ) { return _mm_sub_epi32( lhs, rhs ); }
      friend int_v operator*( int_v lhs, int_v rhs ) { return _mm_mullo_epi32( lhs, rhs ); }
      friend int_v operator-( int_v x ) { return -1 * x; }

      friend int_v operator&( int_v lhs, int_v rhs ) { return _mm_and_si128( lhs, rhs ); }
      friend int_v operator|( int_v lhs, int_v rhs ) { return _mm_or_si128( lhs, rhs ); }
      friend int_v operator^( int_v lhs, int_v rhs ) { return _mm_xor_si128( lhs, rhs ); }
      friend int_v operator~( int_v x ) { return x ^ _mm_set1_epi32( -1 ); }

      friend int_v operator<<( int_v lhs, int rhs ) { return _mm_slli_epi32( lhs, rhs ); }
      friend int_v operator>>( int_v lhs, int rhs ) { return _mm_srli_epi32( lhs, rhs ); }

      friend int_v operator<<( int_v lhs, int_v rhs ) { return _mm_sll_epi32( lhs, rhs ); }
      friend int_v operator>>( int_v lhs, int_v rhs ) { return _mm_srl_epi32( lhs, rhs ); }

      friend int_v min( int_v lhs, int_v rhs ) { return _mm_min_epi32( lhs, rhs ); }
      friend int_v max( int_v lhs, int_v rhs ) { return _mm_max_epi32( lhs, rhs ); }
      friend int_v abs( int_v v ) { return _mm_abs_epi32( v ); }

      friend int_v signselect( float_v s, int_v a, int_v b ) {
        return _mm_castps_si128(
            _mm_blendv_ps( _mm_castsi128_ps( a ), _mm_castsi128_ps( b ), ( s < float_v{0.f} ).native() ) );
      }
      friend int_v select( mask_v mask, int_v a, int_v b ) {
        return _mm_castps_si128( _mm_blendv_ps( _mm_castsi128_ps( b ), _mm_castsi128_ps( a ), mask.native() ) );
      }

      friend mask_v operator<( int_v lhs, int_v rhs ) { return _mm_castsi128_ps( _mm_cmplt_epi32( lhs, rhs ) ); }
      friend mask_v operator>( int_v lhs, int_v rhs ) { return _mm_castsi128_ps( _mm_cmpgt_epi32( lhs, rhs ) ); }
      friend mask_v operator<=( int_v lhs, int_v rhs ) { return !( lhs > rhs ); }
      friend mask_v operator>=( int_v lhs, int_v rhs ) { return !( lhs < rhs ); }
      friend mask_v operator==( int_v lhs, int_v rhs ) { return _mm_castsi128_ps( _mm_cmpeq_epi32( lhs, rhs ) ); }
      friend std::ostream& operator<<( std::ostream& os, int_v x ) { return print_vector<int, 4>( os, x, "sse" ); }

    private:
      __m128i data{};
    };
  } // namespace sse

  namespace simd_traits {
    template <>
    struct traits<sse::int_v> {
      constexpr static bool is_specialized = true;
      constexpr static bool is_simd_integral = true;
    };
  } // namespace simd_traits

  namespace sse {
    using details::pow;
    inline float_v::operator int_v() const { return int_v{_mm_cvttps_epi32( data )}; }
    inline mask_v::operator int_v() const { return _mm_srli_epi32( _mm_castps_si128( data ), 31 ); }

    inline int_v castToInt( float_v x ) { return int_v{_mm_castps_si128( x )}; }

    inline float_v castToFloat( int_v x ) { return float_v{_mm_castsi128_ps( x )}; }

    inline int_v gather( const int* base, int_v idx ) {
      std::array<int, 4> idx_v;
      std::array<int, 4> val_v;
      idx.store( idx_v );
      for ( int i = 0; i < 4; i++ ) val_v[i] = base[idx_v[i]];
      return int_v{val_v};
    }

    inline float_v gather( const float* base, int_v idx ) {
      std::array<int, 4> idx_v;
      std::array<float, 4> val_v;
      idx.store( idx_v );
      for ( int i = 0; i < 4; i++ ) val_v[i] = base[idx_v[i]];
      return float_v{val_v};
    }

    inline int_v maskgather( const int* base, int_v idx, mask_v mask, int_v source ) {
      std::array<int, 4> idx_v;
      std::array<int, 4> val_v;
      int m_v = _mm_movemask_ps( mask.native() );
      idx.store( idx_v );
      for ( int i = 0; i < 4; i++ ) {
        if ( m_v & 1 ) val_v[i] = base[idx_v[i]];
        m_v >>= 1;
      }
      return select( mask, int_v{val_v}, source );
    }

    inline float_v maskgather( const float* base, int_v idx, mask_v mask, float_v source ) {
      std::array<int, 4> idx_v;
      std::array<float, 4> val_v;
      int m_v = _mm_movemask_ps( mask.native() );
      idx.store( idx_v );
      for ( int i = 0; i < 4; i++ ) {
        if ( m_v & 1 ) val_v[i] = base[idx_v[i]];
        m_v >>= 1;
      }
      return select( mask, float_v{val_v}, source );
    }

    inline void scatter( int* base, int_v idx, int_v value ) {
      std::array<int, 4> idx_v;
      std::array<int, 4> val_v;
      idx.store( idx_v );
      value.store( val_v );
      for ( int i = 0; i < 4; i++ ) base[idx_v[i]] = val_v[i];
    }

    inline void scatter( float* base, int_v idx, float_v value ) {
      std::array<int, 4> idx_v;
      std::array<float, 4> val_v;
      idx.store( idx_v );
      value.store( val_v );
      for ( int i = 0; i < 4; i++ ) base[idx_v[i]] = val_v[i];
    }

    inline void maskscatter( int* base, int_v idx, mask_v mask, int_v value ) {
      std::array<int, 4> idx_v;
      std::array<int, 4> val_v;
      int m_v = _mm_movemask_ps( mask.native() );
      idx.store( idx_v );
      value.store( val_v );
      for ( int i = 0; i < 4; i++ ) {
        if ( m_v & 1 ) val_v[i] = base[idx_v[i]];
        m_v >>= 1;
      }
    }

    inline void maskscatter( float* base, int_v idx, mask_v mask, float_v value ) {
      std::array<int, 4> idx_v;
      std::array<float, 4> val_v;
      int m_v = _mm_movemask_ps( mask.native() );
      idx.store( idx_v );
      value.store( val_v );
      for ( int i = 0; i < 4; i++ ) {
        if ( m_v & 1 ) val_v[i] = base[idx_v[i]];
        m_v >>= 1;
      }
    }

    inline int_v types::indices() { return _mm_setr_epi32( 0, 1, 2, 3 ); }
    inline int_v types::indices( int start ) { return indices() + start; }
    inline int_v operator+( mask_v lhs, mask_v rhs ) { return int_v{lhs} + int_v{rhs}; }
    inline int_v popcount_v( int_v a ) { return SIMDWrapper::popcount_v( a ); }
  } // namespace sse
} // namespace SIMDWrapper

SIMDWRAPPER_SPECIALISE_HELPER_TYPES( SIMDWrapper::sse );

namespace SIMDWrapper {
#endif
} // namespace SIMDWrapper

SIMDWRAPPER_SPECIALISE_HELPER_TYPES( SIMDWrapper::scalar );

namespace SIMDWrapper {
#ifndef __AVX2__
  namespace avx2 {
    constexpr InstructionSet instructionSet() { return sse::instructionSet(); }
    using float_v = sse::float_v;
    using mask_v  = sse::mask_v;
    using int_v   = sse::int_v;
    using types   = sse::types;
  } // namespace avx2
#else
  namespace avx2 {
    constexpr InstructionSet instructionSet() { return AVX2; }
    // Permutation for avx2 compress, from https://github.com/lemire/simdprune
    alignas( 32 ) constexpr uint32_t compress_mask256_epi32[] = {
        0, 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 7, 0, 2, 3, 4, 5, 6, 7, 7, 2, 3, 4, 5, 6, 7, 7, 7, 0, 1, 3, 4, 5,
        6, 7, 7, 1, 3, 4, 5, 6, 7, 7, 7, 0, 3, 4, 5, 6, 7, 7, 7, 3, 4, 5, 6, 7, 7, 7, 7, 0, 1, 2, 4, 5, 6, 7, 7, 1, 2,
        4, 5, 6, 7, 7, 7, 0, 2, 4, 5, 6, 7, 7, 7, 2, 4, 5, 6, 7, 7, 7, 7, 0, 1, 4, 5, 6, 7, 7, 7, 1, 4, 5, 6, 7, 7, 7,
        7, 0, 4, 5, 6, 7, 7, 7, 7, 4, 5, 6, 7, 7, 7, 7, 7, 0, 1, 2, 3, 5, 6, 7, 7, 1, 2, 3, 5, 6, 7, 7, 7, 0, 2, 3, 5,
        6, 7, 7, 7, 2, 3, 5, 6, 7, 7, 7, 7, 0, 1, 3, 5, 6, 7, 7, 7, 1, 3, 5, 6, 7, 7, 7, 7, 0, 3, 5, 6, 7, 7, 7, 7, 3,
        5, 6, 7, 7, 7, 7, 7, 0, 1, 2, 5, 6, 7, 7, 7, 1, 2, 5, 6, 7, 7, 7, 7, 0, 2, 5, 6, 7, 7, 7, 7, 2, 5, 6, 7, 7, 7,
        7, 7, 0, 1, 5, 6, 7, 7, 7, 7, 1, 5, 6, 7, 7, 7, 7, 7, 0, 5, 6, 7, 7, 7, 7, 7, 5, 6, 7, 7, 7, 7, 7, 7, 0, 1, 2,
        3, 4, 6, 7, 7, 1, 2, 3, 4, 6, 7, 7, 7, 0, 2, 3, 4, 6, 7, 7, 7, 2, 3, 4, 6, 7, 7, 7, 7, 0, 1, 3, 4, 6, 7, 7, 7,
        1, 3, 4, 6, 7, 7, 7, 7, 0, 3, 4, 6, 7, 7, 7, 7, 3, 4, 6, 7, 7, 7, 7, 7, 0, 1, 2, 4, 6, 7, 7, 7, 1, 2, 4, 6, 7,
        7, 7, 7, 0, 2, 4, 6, 7, 7, 7, 7, 2, 4, 6, 7, 7, 7, 7, 7, 0, 1, 4, 6, 7, 7, 7, 7, 1, 4, 6, 7, 7, 7, 7, 7, 0, 4,
        6, 7, 7, 7, 7, 7, 4, 6, 7, 7, 7, 7, 7, 7, 0, 1, 2, 3, 6, 7, 7, 7, 1, 2, 3, 6, 7, 7, 7, 7, 0, 2, 3, 6, 7, 7, 7,
        7, 2, 3, 6, 7, 7, 7, 7, 7, 0, 1, 3, 6, 7, 7, 7, 7, 1, 3, 6, 7, 7, 7, 7, 7, 0, 3, 6, 7, 7, 7, 7, 7, 3, 6, 7, 7,
        7, 7, 7, 7, 0, 1, 2, 6, 7, 7, 7, 7, 1, 2, 6, 7, 7, 7, 7, 7, 0, 2, 6, 7, 7, 7, 7, 7, 2, 6, 7, 7, 7, 7, 7, 7, 0,
        1, 6, 7, 7, 7, 7, 7, 1, 6, 7, 7, 7, 7, 7, 7, 0, 6, 7, 7, 7, 7, 7, 7, 6, 7, 7, 7, 7, 7, 7, 7, 0, 1, 2, 3, 4, 5,
        7, 7, 1, 2, 3, 4, 5, 7, 7, 7, 0, 2, 3, 4, 5, 7, 7, 7, 2, 3, 4, 5, 7, 7, 7, 7, 0, 1, 3, 4, 5, 7, 7, 7, 1, 3, 4,
        5, 7, 7, 7, 7, 0, 3, 4, 5, 7, 7, 7, 7, 3, 4, 5, 7, 7, 7, 7, 7, 0, 1, 2, 4, 5, 7, 7, 7, 1, 2, 4, 5, 7, 7, 7, 7,
        0, 2, 4, 5, 7, 7, 7, 7, 2, 4, 5, 7, 7, 7, 7, 7, 0, 1, 4, 5, 7, 7, 7, 7, 1, 4, 5, 7, 7, 7, 7, 7, 0, 4, 5, 7, 7,
        7, 7, 7, 4, 5, 7, 7, 7, 7, 7, 7, 0, 1, 2, 3, 5, 7, 7, 7, 1, 2, 3, 5, 7, 7, 7, 7, 0, 2, 3, 5, 7, 7, 7, 7, 2, 3,
        5, 7, 7, 7, 7, 7, 0, 1, 3, 5, 7, 7, 7, 7, 1, 3, 5, 7, 7, 7, 7, 7, 0, 3, 5, 7, 7, 7, 7, 7, 3, 5, 7, 7, 7, 7, 7,
        7, 0, 1, 2, 5, 7, 7, 7, 7, 1, 2, 5, 7, 7, 7, 7, 7, 0, 2, 5, 7, 7, 7, 7, 7, 2, 5, 7, 7, 7, 7, 7, 7, 0, 1, 5, 7,
        7, 7, 7, 7, 1, 5, 7, 7, 7, 7, 7, 7, 0, 5, 7, 7, 7, 7, 7, 7, 5, 7, 7, 7, 7, 7, 7, 7, 0, 1, 2, 3, 4, 7, 7, 7, 1,
        2, 3, 4, 7, 7, 7, 7, 0, 2, 3, 4, 7, 7, 7, 7, 2, 3, 4, 7, 7, 7, 7, 7, 0, 1, 3, 4, 7, 7, 7, 7, 1, 3, 4, 7, 7, 7,
        7, 7, 0, 3, 4, 7, 7, 7, 7, 7, 3, 4, 7, 7, 7, 7, 7, 7, 0, 1, 2, 4, 7, 7, 7, 7, 1, 2, 4, 7, 7, 7, 7, 7, 0, 2, 4,
        7, 7, 7, 7, 7, 2, 4, 7, 7, 7, 7, 7, 7, 0, 1, 4, 7, 7, 7, 7, 7, 1, 4, 7, 7, 7, 7, 7, 7, 0, 4, 7, 7, 7, 7, 7, 7,
        4, 7, 7, 7, 7, 7, 7, 7, 0, 1, 2, 3, 7, 7, 7, 7, 1, 2, 3, 7, 7, 7, 7, 7, 0, 2, 3, 7, 7, 7, 7, 7, 2, 3, 7, 7, 7,
        7, 7, 7, 0, 1, 3, 7, 7, 7, 7, 7, 1, 3, 7, 7, 7, 7, 7, 7, 0, 3, 7, 7, 7, 7, 7, 7, 3, 7, 7, 7, 7, 7, 7, 7, 0, 1,
        2, 7, 7, 7, 7, 7, 1, 2, 7, 7, 7, 7, 7, 7, 0, 2, 7, 7, 7, 7, 7, 7, 2, 7, 7, 7, 7, 7, 7, 7, 0, 1, 7, 7, 7, 7, 7,
        7, 1, 7, 7, 7, 7, 7, 7, 7, 0, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 0, 1, 2, 3, 4, 5, 6, 6, 1, 2, 3, 4,
        5, 6, 6, 6, 0, 2, 3, 4, 5, 6, 6, 6, 2, 3, 4, 5, 6, 6, 6, 6, 0, 1, 3, 4, 5, 6, 6, 6, 1, 3, 4, 5, 6, 6, 6, 6, 0,
        3, 4, 5, 6, 6, 6, 6, 3, 4, 5, 6, 6, 6, 6, 6, 0, 1, 2, 4, 5, 6, 6, 6, 1, 2, 4, 5, 6, 6, 6, 6, 0, 2, 4, 5, 6, 6,
        6, 6, 2, 4, 5, 6, 6, 6, 6, 6, 0, 1, 4, 5, 6, 6, 6, 6, 1, 4, 5, 6, 6, 6, 6, 6, 0, 4, 5, 6, 6, 6, 6, 6, 4, 5, 6,
        6, 6, 6, 6, 6, 0, 1, 2, 3, 5, 6, 6, 6, 1, 2, 3, 5, 6, 6, 6, 6, 0, 2, 3, 5, 6, 6, 6, 6, 2, 3, 5, 6, 6, 6, 6, 6,
        0, 1, 3, 5, 6, 6, 6, 6, 1, 3, 5, 6, 6, 6, 6, 6, 0, 3, 5, 6, 6, 6, 6, 6, 3, 5, 6, 6, 6, 6, 6, 6, 0, 1, 2, 5, 6,
        6, 6, 6, 1, 2, 5, 6, 6, 6, 6, 6, 0, 2, 5, 6, 6, 6, 6, 6, 2, 5, 6, 6, 6, 6, 6, 6, 0, 1, 5, 6, 6, 6, 6, 6, 1, 5,
        6, 6, 6, 6, 6, 6, 0, 5, 6, 6, 6, 6, 6, 6, 5, 6, 6, 6, 6, 6, 6, 6, 0, 1, 2, 3, 4, 6, 6, 6, 1, 2, 3, 4, 6, 6, 6,
        6, 0, 2, 3, 4, 6, 6, 6, 6, 2, 3, 4, 6, 6, 6, 6, 6, 0, 1, 3, 4, 6, 6, 6, 6, 1, 3, 4, 6, 6, 6, 6, 6, 0, 3, 4, 6,
        6, 6, 6, 6, 3, 4, 6, 6, 6, 6, 6, 6, 0, 1, 2, 4, 6, 6, 6, 6, 1, 2, 4, 6, 6, 6, 6, 6, 0, 2, 4, 6, 6, 6, 6, 6, 2,
        4, 6, 6, 6, 6, 6, 6, 0, 1, 4, 6, 6, 6, 6, 6, 1, 4, 6, 6, 6, 6, 6, 6, 0, 4, 6, 6, 6, 6, 6, 6, 4, 6, 6, 6, 6, 6,
        6, 6, 0, 1, 2, 3, 6, 6, 6, 6, 1, 2, 3, 6, 6, 6, 6, 6, 0, 2, 3, 6, 6, 6, 6, 6, 2, 3, 6, 6, 6, 6, 6, 6, 0, 1, 3,
        6, 6, 6, 6, 6, 1, 3, 6, 6, 6, 6, 6, 6, 0, 3, 6, 6, 6, 6, 6, 6, 3, 6, 6, 6, 6, 6, 6, 6, 0, 1, 2, 6, 6, 6, 6, 6,
        1, 2, 6, 6, 6, 6, 6, 6, 0, 2, 6, 6, 6, 6, 6, 6, 2, 6, 6, 6, 6, 6, 6, 6, 0, 1, 6, 6, 6, 6, 6, 6, 1, 6, 6, 6, 6,
        6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 1, 2, 3, 4, 5, 5, 5, 1, 2, 3, 4, 5, 5, 5, 5, 0, 2,
        3, 4, 5, 5, 5, 5, 2, 3, 4, 5, 5, 5, 5, 5, 0, 1, 3, 4, 5, 5, 5, 5, 1, 3, 4, 5, 5, 5, 5, 5, 0, 3, 4, 5, 5, 5, 5,
        5, 3, 4, 5, 5, 5, 5, 5, 5, 0, 1, 2, 4, 5, 5, 5, 5, 1, 2, 4, 5, 5, 5, 5, 5, 0, 2, 4, 5, 5, 5, 5, 5, 2, 4, 5, 5,
        5, 5, 5, 5, 0, 1, 4, 5, 5, 5, 5, 5, 1, 4, 5, 5, 5, 5, 5, 5, 0, 4, 5, 5, 5, 5, 5, 5, 4, 5, 5, 5, 5, 5, 5, 5, 0,
        1, 2, 3, 5, 5, 5, 5, 1, 2, 3, 5, 5, 5, 5, 5, 0, 2, 3, 5, 5, 5, 5, 5, 2, 3, 5, 5, 5, 5, 5, 5, 0, 1, 3, 5, 5, 5,
        5, 5, 1, 3, 5, 5, 5, 5, 5, 5, 0, 3, 5, 5, 5, 5, 5, 5, 3, 5, 5, 5, 5, 5, 5, 5, 0, 1, 2, 5, 5, 5, 5, 5, 1, 2, 5,
        5, 5, 5, 5, 5, 0, 2, 5, 5, 5, 5, 5, 5, 2, 5, 5, 5, 5, 5, 5, 5, 0, 1, 5, 5, 5, 5, 5, 5, 1, 5, 5, 5, 5, 5, 5, 5,
        0, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 0, 1, 2, 3, 4, 4, 4, 4, 1, 2, 3, 4, 4, 4, 4, 4, 0, 2, 3, 4, 4,
        4, 4, 4, 2, 3, 4, 4, 4, 4, 4, 4, 0, 1, 3, 4, 4, 4, 4, 4, 1, 3, 4, 4, 4, 4, 4, 4, 0, 3, 4, 4, 4, 4, 4, 4, 3, 4,
        4, 4, 4, 4, 4, 4, 0, 1, 2, 4, 4, 4, 4, 4, 1, 2, 4, 4, 4, 4, 4, 4, 0, 2, 4, 4, 4, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4,
        4, 0, 1, 4, 4, 4, 4, 4, 4, 1, 4, 4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 0, 1, 2, 3,
        3, 3, 3, 3, 1, 2, 3, 3, 3, 3, 3, 3, 0, 2, 3, 3, 3, 3, 3, 3, 2, 3, 3, 3, 3, 3, 3, 3, 0, 1, 3, 3, 3, 3, 3, 3, 1,
        3, 3, 3, 3, 3, 3, 3, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2,
        2, 2, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    class int_v;

    class float_v;

    class mask_v {
    public:
      constexpr static std::size_t size() { return 8; }

      mask_v() {} // Constructor must be empty
      mask_v( __m256 f ) : data{f} {}
      explicit mask_v( bool v ) : data{_mm256_set1_ps( v ? -1.f : 0.f )} {}

      mask_v& operator=( __m256 f ) {
        data = f;
        return *this;
      }

      operator int_v() const;

      const mask_v& store( float* ptr ) const { return store( LHCb::span<float, 8>{ptr, size()} ); } // non standard
      const mask_v& store( LHCb::span<float, 8> s ) const {
        _mm256_storeu_ps( s.data(), data );
        return *this;
      } // non standard

      friend mask_v operator&( mask_v lhs, mask_v rhs ) { return _mm256_and_ps( lhs.native(), rhs.native() ); }
      friend mask_v operator|( mask_v lhs, mask_v rhs ) { return _mm256_or_ps( lhs.native(), rhs.native() ); }
      friend mask_v operator^( mask_v lhs, mask_v rhs ) { return _mm256_xor_ps( lhs.native(), rhs.native() ); }

      friend mask_v operator&&( mask_v lhs, mask_v rhs ) { return _mm256_and_ps( lhs.native(), rhs.native() ); }
      friend mask_v operator||( mask_v lhs, mask_v rhs ) { return _mm256_or_ps( lhs.native(), rhs.native() ); }
      friend mask_v operator!( mask_v x ) {
        return _mm256_xor_ps( x.native(), _mm256_castsi256_ps( _mm256_set1_epi32( -1 ) ) );
      }

      friend std::ostream& operator<<( std::ostream& os, mask_v x ) {
        return print_bitset<8>( os, static_cast<unsigned long long>( _mm256_movemask_ps( x.native() ) ), "avx2" );
      }

      friend bool all( mask_v mask ) { return _mm256_movemask_ps( mask.native() ) == 0xFF; }
      friend bool none( mask_v mask ) { return _mm256_movemask_ps( mask.native() ) == 0x00; }
      friend bool any( mask_v mask ) { return _mm256_movemask_ps( mask.native() ) != 0x00; }
      friend bool testbit( mask_v mask, const int bit ) {
        return ( _mm256_movemask_ps( mask.native() ) & ( 1 << bit ) ) != 0;
      }
      // TODO: c++20 countl_zero
      friend int ffs( mask_v mask ) { return __builtin_ffs( _mm256_movemask_ps( mask.native() ) ); }
      friend int popcount( mask_v mask ) { return _mm_popcnt_u32( _mm256_movemask_ps( mask.native() ) ); }

      __m256 native() const { return data; }

    private:
      __m256 data;
    };

    struct types {
      constexpr static std::size_t size = 8;
      using int_v = avx2::int_v;
      using float_v = avx2::float_v;
      using mask_v = avx2::mask_v;
      static mask_v mask_true() { return mask_v{true}; }
      static mask_v mask_false() { return mask_v{false}; }
      static int_v indices();
      static int_v indices( int start );
      static mask_v loop_mask( int i, int n );
      static constexpr InstructionSet instructionSet() { return AVX2; }
    };

    class float_v {
    public:
      constexpr static std::size_t size() { return avx2::types::size; }
      using span = LHCb::span<float, avx2::types::size>;
      using const_span = LHCb::span<const float, avx2::types::size>;

      float_v() {} // Constructor must be empty
      float_v( scalar::float_v f ) : float_v{_mm256_set1_ps( f.cast() )} {}
      template <typename T, std::enable_if_t<std::is_integral_v<T>, int> = 0>
      float_v( T f ) : float_v( _mm256_set1_ps( float( f ) ) ) {}
      float_v( float f ) : float_v{_mm256_set1_ps( f )} {}
      float_v( const float* f ) : float_v{_mm256_loadu_ps( f )} {}
      float_v( const_span f ) : float_v{_mm256_loadu_ps( f.data() )} {}
      float_v( __m256 f ) : data{f} {}

      float_v& operator=( __m256 f ) {
        data = f;
        return *this;
      }

      operator __m256() const { return data; }

      explicit operator int_v() const;

      const float_v& store( float* ptr ) const { return store( span{ptr, size()} ); }
      const float_v& store( span s ) const {
        _mm256_storeu_ps( s.data(), data );
        return *this;
      }

      const float_v& compressstore( mask_v mask, float* ptr ) const { return compressstore( mask, span{ptr, size()} ); }
      const float_v& compressstore( mask_v mask, span s ) const {
        __m256i perm = _mm256_load_si256( (const __m256i*)compress_mask256_epi32 +
                                          ( _mm256_movemask_ps( mask.native() ) ^ 0xFF ) );
        _mm256_storeu_ps( s.data(), _mm256_permutevar8x32_ps( data, perm ) );
        return *this;
      }

      float_v& operator+=( float_v rhs ) {
        *this = *this + rhs;
        return *this;
      }
      float_v& operator-=( float_v rhs ) {
        *this = *this - rhs;
        return *this;
      }
      float_v& operator*=( float_v rhs ) {
        *this = *this * rhs;
        return *this;
      }
      float_v& operator/=( float_v rhs ) {
        *this = *this / rhs;
        return *this;
      }

      friend float_v operator+( float_v lhs, float_v rhs ) { return _mm256_add_ps( lhs, rhs ); }
      friend float_v operator-( float_v lhs, float_v rhs ) { return _mm256_sub_ps( lhs, rhs ); }
      friend float_v operator*( float_v lhs, float_v rhs ) { return _mm256_mul_ps( lhs, rhs ); }
      friend float_v operator/( float_v lhs, float_v rhs ) { return _mm256_div_ps( lhs, rhs ); }
      friend float_v operator-( float_v x ) { return -1.f * x; }

      friend float_v operator&( float_v lhs, float_v rhs ) { return _mm256_and_ps( lhs, rhs ); }
      friend float_v operator|( float_v lhs, float_v rhs ) { return _mm256_or_ps( lhs, rhs ); }
      friend float_v operator^( float_v lhs, float_v rhs ) { return _mm256_xor_ps( lhs, rhs ); }

      friend float_v operator&&( float_v lhs, float_v rhs ) { return _mm256_and_ps( lhs, rhs ); }
      friend float_v operator||( float_v lhs, float_v rhs ) { return _mm256_or_ps( lhs, rhs ); }
      friend float_v operator!( float_v x ) { return x ^ _mm256_castsi256_ps( _mm256_set1_epi32( -1 ) ); }

      friend std::ostream& operator<<( std::ostream& os, float_v x ) {
        return print_vector<float, size()>( os, x, "avx2" );
      }

      friend float_v min( float_v lhs, float_v rhs ) { return _mm256_min_ps( lhs, rhs ); }
      friend float_v max( float_v lhs, float_v rhs ) { return _mm256_max_ps( lhs, rhs ); }
      friend float_v abs( float_v v ) { return v & _mm256_castsi256_ps( _mm256_set1_epi32( 0x7FFFFFFF ) ); }
      friend float_v copysign( float_v x, float_v y ) {
        return x ^ ( y & _mm256_castsi256_ps( _mm256_set1_epi32( 0x80000000 ) ) );
      }

      friend float_v signselect( float_v s, float_v a, float_v b ) { return _mm256_blendv_ps( a, b, s ); }
      friend float_v select( mask_v mask, float_v a, float_v b ) { return _mm256_blendv_ps( b, a, mask.native() ); }

      friend float_v sqrt( float_v v ) { return _mm256_sqrt_ps( v ); }
      [[deprecated( "Be aware, yields different results for AMD and Intel" )]] friend float_v rsqrt( float_v v ) {
        return _mm256_rsqrt_ps( v );
      }
      [[deprecated( "Be aware, yields different results for AMD and Intel" )]] friend float_v rcp( float_v v ) {
        return _mm256_rcp_ps( v );
      }

      friend mask_v operator<( float_v lhs, float_v rhs ) { return _mm256_cmp_ps( lhs, rhs, _CMP_LT_OS ); }
      friend mask_v operator>( float_v lhs, float_v rhs ) { return _mm256_cmp_ps( lhs, rhs, _CMP_GT_OS ); }
      friend mask_v operator<=( float_v lhs, float_v rhs ) { return !( lhs > rhs ); }
      friend mask_v operator>=( float_v lhs, float_v rhs ) { return !( lhs < rhs ); }
      friend mask_v operator==( float_v lhs, float_v rhs ) { return _mm256_cmp_ps( lhs, rhs, _CMP_EQ_OS ); }

      float hmax() const {
        __m128 r = _mm_max_ps( _mm256_extractf128_ps( data, 0 ), _mm256_extractf128_ps( data, 1 ) );
        r = _mm_max_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_max_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_cvtss_f32( r );
      }
      float hmin() const {
        __m128 r = _mm_min_ps( _mm256_extractf128_ps( data, 0 ), _mm256_extractf128_ps( data, 1 ) );
        r = _mm_min_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_min_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_cvtss_f32( r );
      }
      float hadd() const {
        __m128 r = _mm_add_ps( _mm256_extractf128_ps( data, 0 ), _mm256_extractf128_ps( data, 1 ) );
        r = _mm_add_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_add_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_cvtss_f32( r );
      }
      float hmax( mask_v mask ) const { return select( mask, *this, std::numeric_limits<float>::lowest() ).hmax(); }
      float hmin( mask_v mask ) const { return select( mask, *this, std::numeric_limits<float>::max() ).hmin(); }
      float hadd( mask_v mask ) const { return select( mask, *this, 0.f ).hadd(); }

    private:
      __m256 data{};
    };

    class int_v {
    public:
      constexpr static std::size_t size() { return avx2::types::size; }
      using span = LHCb::span<int, avx2::types::size>;
      using const_span = LHCb::span<const int, avx2::types::size>;

      int_v() {} // Constructor must be empty
      int_v( int f ) : int_v{_mm256_set1_epi32( f )} {}
      int_v( const int* f ) : int_v{_mm256_loadu_si256( (__m256i*)f )} {}
      int_v( const_span f ) : int_v{_mm256_loadu_si256( (__m256i*)f.data() )} {}
      int_v( __m256i f ) : data{f} {}

      int_v& operator=( __m256i f ) {
        data = f;
        return *this;
      }

      operator __m256i() const { return data; }
      operator float_v() const { return float_v{_mm256_cvtepi32_ps( data )}; }

      const int_v& store( int* ptr ) const { return store( span{ptr, size()} ); }
      const int_v& store( span s ) const {
        _mm256_storeu_si256( (__m256i*)s.data(), data );
        return *this;
      }

      const int_v& compressstore( mask_v mask, int* ptr ) const { return compressstore( mask, span{ptr, size()} ); }
      const int_v& compressstore( mask_v mask, span s ) const {
        __m256i perm = _mm256_load_si256( (const __m256i*)compress_mask256_epi32 +
                                          ( _mm256_movemask_ps( mask.native() ) ^ 0xFF ) );
        _mm256_storeu_si256( (__m256i*)s.data(), _mm256_permutevar8x32_epi32( data, perm ) );
        return *this;
      }

      int hmax() const {
        __m128i r = _mm_max_epi32( _mm256_extractf128_si256( data, 0 ), _mm256_extractf128_si256( data, 1 ) );
        r = _mm_max_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_max_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_extract_epi32( r, 0 );
      }
      int hmin() const {
        __m128i r = _mm_min_epi32( _mm256_extractf128_si256( data, 0 ), _mm256_extractf128_si256( data, 1 ) );
        r = _mm_min_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_min_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_extract_epi32( r, 0 );
      }
      int hadd() const {
        __m128i r = _mm_add_epi32( _mm256_extractf128_si256( data, 0 ), _mm256_extractf128_si256( data, 1 ) );
        r = _mm_add_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_add_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_extract_epi32( r, 0 );
      }
      int hmax( mask_v mask ) const { return select( mask, *this, std::numeric_limits<int>::min() ).hmax(); }
      int hmin( mask_v mask ) const { return select( mask, *this, std::numeric_limits<int>::max() ).hmin(); }
      int hadd( mask_v mask ) const { return select( mask, *this, 0 ).hadd(); }

      int_v& operator+=( int_v rhs ) {
        *this = *this + rhs;
        return *this;
      }
      int_v& operator-=( int_v rhs ) {
        *this = *this - rhs;
        return *this;
      }
      int_v& operator*=( int_v rhs ) {
        *this = *this * rhs;
        return *this;
      }

      friend int_v operator+( int_v lhs, int_v rhs ) { return _mm256_add_epi32( lhs, rhs ); }
      friend int_v operator-( int_v lhs, int_v rhs ) { return _mm256_sub_epi32( lhs, rhs ); }
      friend int_v operator*( int_v lhs, int_v rhs ) { return _mm256_mullo_epi32( lhs, rhs ); }
      friend int_v operator-( int_v x ) { return -1 * x; }

      friend int_v operator&( int_v lhs, int_v rhs ) { return _mm256_and_si256( lhs, rhs ); }
      friend int_v operator|( int_v lhs, int_v rhs ) { return _mm256_or_si256( lhs, rhs ); }
      friend int_v operator^( int_v lhs, int_v rhs ) { return _mm256_xor_si256( lhs, rhs ); }
      friend int_v operator~( int_v x ) { return x ^ _mm256_set1_epi32( -1 ); }

      friend int_v operator<<( int_v lhs, int rhs ) { return _mm256_slli_epi32( lhs, rhs ); }
      friend int_v operator>>( int_v lhs, int rhs ) { return _mm256_srli_epi32( lhs, rhs ); }
      friend int_v operator<<( int_v lhs, int_v rhs ) { return _mm256_sllv_epi32( lhs, rhs ); }
      friend int_v operator>>( int_v lhs, int_v rhs ) { return _mm256_srlv_epi32( lhs, rhs ); }

      friend std::ostream& operator<<( std::ostream& os, int_v x ) {
        return print_vector<int, size()>( os, x, "avx2" );
      }

      friend int_v min( int_v lhs, int_v rhs ) { return _mm256_min_epi32( lhs, rhs ); }
      friend int_v max( int_v lhs, int_v rhs ) { return _mm256_max_epi32( lhs, rhs ); }
      friend int_v abs( int_v x ) { return _mm256_abs_epi32( x ); }

      friend int_v signselect( float_v s, int_v a, int_v b ) {
        return _mm256_castps_si256( _mm256_blendv_ps( _mm256_castsi256_ps( a ), _mm256_castsi256_ps( b ), s ) );
      }
      friend int_v select( mask_v mask, int_v a, int_v b ) {
        return _mm256_castps_si256(
            _mm256_blendv_ps( _mm256_castsi256_ps( b ), _mm256_castsi256_ps( a ), mask.native() ) );
      }

      friend mask_v operator<( int_v lhs, int_v rhs ) { return _mm256_castsi256_ps( _mm256_cmpgt_epi32( rhs, lhs ) ); }
      friend mask_v operator>( int_v lhs, int_v rhs ) { return _mm256_castsi256_ps( _mm256_cmpgt_epi32( lhs, rhs ) ); }
      friend mask_v operator<=( int_v lhs, int_v rhs ) { return !( lhs > rhs ); }
      friend mask_v operator>=( int_v lhs, int_v rhs ) { return !( lhs < rhs ); }
      friend mask_v operator==( int_v lhs, int_v rhs ) { return _mm256_castsi256_ps( _mm256_cmpeq_epi32( lhs, rhs ) ); }

    private:
      __m256i data{};
    };
  } // namespace avx2
  namespace simd_traits {
    template <>
    struct traits<avx2::int_v> {
      constexpr static bool is_specialized = true;
      constexpr static bool is_simd_integral = true;
    };
  } // namespace simd_traits

  namespace avx2 {
    using details::pow;

    inline mask_v::operator int_v() const { return _mm256_srli_epi32( _mm256_castps_si256( data ), 31 ); }

    inline float_v::operator int_v() const { return int_v{_mm256_cvttps_epi32( data )}; }

    inline int_v castToInt( mask_v x ) { return int_v{_mm256_castps_si256( x.native() )}; }

    inline int_v castToInt( float_v x ) { return int_v{_mm256_castps_si256( x )}; }

    inline float_v castToFloat( int_v x ) { return float_v{_mm256_castsi256_ps( x )}; }

    inline int_v gather( const int* base, int_v idx ) { return _mm256_i32gather_epi32( base, idx, sizeof( int ) ); }

    inline float_v gather( const float* base, int_v idx ) { return _mm256_i32gather_ps( base, idx, sizeof( float ) ); }

    inline int_v maskgather( const int* base, int_v idx, mask_v mask, int_v source ) {
      return _mm256_mask_i32gather_epi32( source, base, idx, castToInt( mask ), sizeof( int ) );
    }

    inline float_v maskgather( const float* base, int_v idx, mask_v mask, float_v source ) {
      // explicit cast to __m256 for the mask: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=94046
      return _mm256_mask_i32gather_ps( source, base, idx, mask.native(), sizeof( float ) );
    }

    inline void scatter( int* base, int_v idx, int_v value ) {
      std::array<int, 8> idx_v;
      std::array<int, 8> val_v;
      idx.store( idx_v );
      value.store( val_v );
      for ( int i = 0; i < 8; i++ ) base[idx_v[i]] = val_v[i];
    }

    inline void scatter( float* base, int_v idx, float_v value ) {
      std::array<int, 8> idx_v;
      std::array<float, 8> val_v;
      idx.store( idx_v );
      value.store( val_v );
      for ( int i = 0; i < 8; i++ ) base[idx_v[i]] = val_v[i];
    }

    inline void maskscatter( int* base, int_v idx, mask_v mask, int_v value ) {
      std::array<int, 8> idx_v;
      std::array<int, 8> val_v;
      int m_v = _mm256_movemask_ps( mask.native() );
      idx.store( idx_v );
      value.store( val_v );
      for ( int i = 0; i < 8; i++ ) {
        if ( m_v & 1 ) val_v[i] = base[idx_v[i]];
        m_v >>= 1;
      }
    }

    inline void maskscatter( float* base, int_v idx, mask_v mask, float_v value ) {
      std::array<int, 8> idx_v;
      std::array<float, 8> val_v;
      int m_v = _mm256_movemask_ps( mask.native() );
      idx.store( idx_v );
      value.store( val_v );
      for ( int i = 0; i < 8; i++ ) {
        if ( m_v & 1 ) val_v[i] = base[idx_v[i]];
        m_v >>= 1;
      }
    }

    inline int_v types::indices() { return _mm256_setr_epi32( 0, 1, 2, 3, 4, 5, 6, 7 ); }

    inline int_v types::indices( int start ) { return indices() + start; }

    inline mask_v types::loop_mask( int i, int n ) {
      return _mm256_cmp_ps( _mm256_setr_ps( 0, 1, 2, 3, 4, 5, 6, 7 ), _mm256_set1_ps( n - i ), _CMP_LT_OS );
    }

    inline int_v operator+( mask_v lhs, mask_v rhs ) { return int_v{lhs} + int_v{rhs}; }
    inline int_v popcount_v( int_v a ) { return SIMDWrapper::popcount_v( a ); }
  } // namespace avx2

} // namespace SIMDWrapper

SIMDWRAPPER_SPECIALISE_HELPER_TYPES( SIMDWrapper::avx2 );

namespace SIMDWrapper {
#endif

#ifndef __AVX512F__
  namespace avx512 {
    constexpr InstructionSet instructionSet() { return avx2::instructionSet(); }
    using float_v = avx2::float_v;
    using mask_v  = avx2::mask_v;
    using int_v   = avx2::int_v;
    using types   = avx2::types;
  } // namespace avx512

  namespace avx256 {
    constexpr InstructionSet instructionSet() { return avx2::instructionSet(); }
    using float_v = avx2::float_v;
    using mask_v  = avx2::mask_v;
    using int_v   = avx2::int_v;
    using types   = avx2::types;
  } // namespace avx256
#else
  namespace avx256 {
    constexpr InstructionSet instructionSet() { return AVX256; }
    class int_v;

    class mask_v {
    public:
      mask_v() {} // Constructor must be empty
      mask_v( __mmask8 m ) : data{m} {}
      explicit mask_v( bool v ) : data{v ? __mmask8{0xFF} : __mmask8{0x00}} {}

      mask_v& operator=( __mmask8 m ) {
        data = m;
        return *this;
      }

      constexpr static std::size_t size() { return 8; }

      operator int_v() const;

      friend mask_v operator|( mask_v lhs, mask_v rhs ) { return lhs.native() | rhs.native(); }

      friend mask_v operator&&( mask_v lhs, mask_v rhs ) { return lhs.native() & rhs.native(); }
      friend mask_v operator||( mask_v lhs, mask_v rhs ) { return lhs.native() | rhs.native(); }
      friend mask_v operator!( mask_v x ) { return ~x.native(); }

      friend bool all( mask_v mask ) { return mask.native() == 0xFF; }
      friend bool none( mask_v mask ) { return mask.native() == 0x00; }
      friend bool any( mask_v mask ) { return mask.native() != 0x00; }
      friend bool testbit( mask_v mask, int bit ) { return ( mask.native() & ( 1 << bit ) ) != 0; }
      // TODO: c++20 countl_zero
      friend int ffs( mask_v mask ) { return __builtin_ffs( mask.native() ); }
      friend int popcount( mask_v mask ) { return _mm_popcnt_u32( mask.native() ); }

      friend std::ostream& operator<<( std::ostream& os, mask_v x ) {
        return print_bitset<8>( os, x.native(), "avx256" );
      }

      __mmask8 native() const { return data; }

    private:
      __mmask8 data{};
    };

    class float_v;

    struct types {
      constexpr static std::size_t size = 8;
      using int_v = avx256::int_v;
      using float_v = avx256::float_v;
      using mask_v = avx256::mask_v;
      static mask_v mask_true() { return mask_v{true}; }
      static mask_v mask_false() { return mask_v{false}; }
      static int_v indices();
      static int_v indices( int start );
      static mask_v loop_mask( int i, int n ) { return ( ( i + 8 ) > n ) ? ~( 0xFF << ( n - i ) ) : 0xFF; }
      static constexpr InstructionSet instructionSet() { return AVX256; }
    };

    class float_v {
    public:
      constexpr static std::size_t size() { return avx256::types::size; }
      using span = LHCb::span<float, avx256::types::size>;
      using const_span = LHCb::span<const float, avx256::types::size>;

      float_v() {} // Constructor must be empty
      float_v( scalar::float_v f ) : float_v{_mm256_set1_ps( f.cast() )} {}
      template <typename T, std::enable_if_t<std::is_integral_v<T>, int> = 0>
      float_v( T f ) : float_v{_mm256_set1_ps( float( f ) )} {}
      float_v( float f ) : float_v{_mm256_set1_ps( f )} {}
      float_v( const float* f ) : float_v{_mm256_loadu_ps( f )} {}
      float_v( const_span f ) : float_v{_mm256_loadu_ps( f.data() )} {}
      float_v( __m256 f ) : data{f} {}

      float_v& operator=( __m256 f ) {
        data = f;
        return *this;
      }

      operator __m256() const { return data; }
      explicit operator int_v() const;

      const float_v& store( float* ptr ) const { return store( span{ptr, size()} ); }
      const float_v& store( span s ) const {
        _mm256_storeu_ps( s.data(), data );
        return *this;
      }

      const float_v& compressstore( mask_v mask, float* ptr ) const { return compressstore( mask, span{ptr, size()} ); }
      const float_v& compressstore( mask_v mask, span s ) const {
        _mm256_mask_compressstoreu_ps( s.data(), mask.native(), data );
        return *this;
      }

      float hmax() const {
        __m128 r = _mm_max_ps( _mm256_extractf128_ps( data, 0 ), _mm256_extractf128_ps( data, 1 ) );
        r = _mm_max_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_max_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_cvtss_f32( r );
      }
      float hmin() const {
        __m128 r = _mm_min_ps( _mm256_extractf128_ps( data, 0 ), _mm256_extractf128_ps( data, 1 ) );
        r = _mm_min_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_min_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_cvtss_f32( r );
      }
      float hadd() const {
        __m128 r = _mm_add_ps( _mm256_extractf128_ps( data, 0 ), _mm256_extractf128_ps( data, 1 ) );
        r = _mm_add_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_add_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_cvtss_f32( r );
      }
      float hmax( mask_v mask ) const {
        return select( mask.native(), *this, std::numeric_limits<float>::lowest() ).hmax();
      }
      float hmin( mask_v mask ) const {
        return select( mask.native(), *this, std::numeric_limits<float>::max() ).hmin();
      }
      float hadd( mask_v mask ) const { return select( mask.native(), *this, 0.f ).hadd(); }

      float_v& operator+=( float_v rhs ) {
        *this = *this + rhs;
        return *this;
      }
      float_v& operator-=( float_v rhs ) {
        *this = *this - rhs;
        return *this;
      }
      float_v& operator*=( float_v rhs ) {
        *this = *this * rhs;
        return *this;
      }
      float_v& operator/=( float_v rhs ) {
        *this = *this / rhs;
        return *this;
      }

      friend float_v operator+( float_v lhs, float_v rhs ) { return _mm256_add_ps( lhs, rhs ); }
      friend float_v operator-( float_v lhs, float_v rhs ) { return _mm256_sub_ps( lhs, rhs ); }
      friend float_v operator*( float_v lhs, float_v rhs ) { return _mm256_mul_ps( lhs, rhs ); }
      friend float_v operator/( float_v lhs, float_v rhs ) { return _mm256_div_ps( lhs, rhs ); }
      friend float_v operator-( float_v x ) { return -1.f * x; }

      friend float_v operator&( float_v lhs, float_v rhs ) { return _mm256_and_ps( lhs, rhs ); }
      friend float_v operator|( float_v lhs, float_v rhs ) { return _mm256_or_ps( lhs, rhs ); }
      friend float_v operator^( float_v lhs, float_v rhs ) { return _mm256_xor_ps( lhs, rhs ); }

      friend float_v operator&&( float_v lhs, float_v rhs ) { return _mm256_and_ps( lhs, rhs ); }
      friend float_v operator||( float_v lhs, float_v rhs ) { return _mm256_or_ps( lhs, rhs ); }
      friend float_v operator!( float_v x ) { return x ^ _mm256_castsi256_ps( _mm256_set1_epi32( -1 ) ); }

      friend float_v min( float_v lhs, float_v rhs ) { return _mm256_min_ps( lhs, rhs ); }
      friend float_v max( float_v lhs, float_v rhs ) { return _mm256_max_ps( lhs, rhs ); }
      friend float_v abs( float_v v ) { return v & _mm256_castsi256_ps( _mm256_set1_epi32( 0x7FFFFFFF ) ); }
      friend float_v copysign( float_v x, float_v y ) {
        return x ^ ( y & _mm256_castsi256_ps( _mm256_set1_epi32( 0x80000000 ) ) );
      }

      friend float_v signselect( float_v s, float_v a, float_v b ) {
        return _mm256_mask_mov_ps( a, ( s < float_v{0.f} ).native(), b );
      }
      friend float_v select( mask_v mask, float_v a, float_v b ) { return _mm256_mask_mov_ps( b, mask.native(), a ); }

      friend float_v sqrt( float_v v ) { return _mm256_sqrt_ps( v ); }
      [[deprecated( "Be aware, yields different results for AMD and Intel" )]] friend float_v rsqrt( float_v v ) {
        return _mm256_rsqrt_ps( v );
      }
      [[deprecated( "Be aware, yields different results for AMD and Intel" )]] friend float_v rcp( float_v v ) {
        return _mm256_rcp_ps( v );
      }

      friend mask_v operator<( float_v lhs, float_v rhs ) { return _mm256_cmp_ps_mask( lhs, rhs, _CMP_LT_OS ); }
      friend mask_v operator>( float_v lhs, float_v rhs ) { return _mm256_cmp_ps_mask( lhs, rhs, _CMP_GT_OS ); }
      friend mask_v operator<=( float_v lhs, float_v rhs ) { return !( lhs > rhs ); }
      friend mask_v operator>=( float_v lhs, float_v rhs ) { return !( lhs < rhs ); }
      friend mask_v operator==( float_v lhs, float_v rhs ) { return _mm256_cmp_ps_mask( lhs, rhs, _CMP_EQ_OS ); }
      friend std::ostream& operator<<( std::ostream& os, float_v x ) {
        return print_vector<float, size()>( os, x, "avx256" );
      }

    private:
      __m256 data{};
    };

    class int_v {
    public:
      constexpr static std::size_t size() { return avx256::types::size; }
      using span = LHCb::span<int, avx256::types::size>;
      using const_span = LHCb::span<const int, avx256::types::size>;

      int_v() {} // Constructor must be empty
      int_v( int f ) : int_v{_mm256_set1_epi32( f )} {}
      int_v( const int* f ) : int_v{_mm256_loadu_si256( (__m256i*)f )} {}
      int_v( const_span f ) : int_v{_mm256_loadu_si256( (__m256i*)f.data() )} {}
      constexpr int_v( __m256i f ) : data{f} {}

      int_v& operator=( __m256i f ) {
        data = f;
        return *this;
      }

      operator __m256i() const { return data; }
      operator float_v() const { return float_v{_mm256_cvtepi32_ps( data )}; }

      const int_v& store( int* ptr ) const { return store( span{ptr, size()} ); }
      const int_v& store( span s ) const {
        _mm256_storeu_si256( (__m256i*)s.data(), data );
        return *this;
      }

      const int_v& compressstore( mask_v mask, int* ptr ) const { return compressstore( mask, span{ptr, size()} ); }
      const int_v& compressstore( mask_v mask, span s ) const {
        _mm256_mask_compressstoreu_epi32( s.data(), mask.native(), data );
        return *this;
      }

      int hmax() const {
        __m128i r = _mm_max_epi32( _mm256_extractf128_si256( data, 0 ), _mm256_extractf128_si256( data, 1 ) );
        r = _mm_max_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_max_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_extract_epi32( r, 0 );
      }
      int hmin() const {
        __m128i r = _mm_min_epi32( _mm256_extractf128_si256( data, 0 ), _mm256_extractf128_si256( data, 1 ) );
        r = _mm_min_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_min_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_extract_epi32( r, 0 );
      }
      int hadd() const {
        __m128i r = _mm_add_epi32( _mm256_extractf128_si256( data, 0 ), _mm256_extractf128_si256( data, 1 ) );
        r = _mm_add_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
        r = _mm_add_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
        return _mm_extract_epi32( r, 0 );
      }
      int hmax( mask_v mask ) const { return select( mask.native(), *this, std::numeric_limits<int>::min() ).hmax(); }
      int hmin( mask_v mask ) const { return select( mask.native(), *this, std::numeric_limits<int>::max() ).hmin(); }
      int hadd( mask_v mask ) const { return select( mask.native(), *this, 0 ).hadd(); }

      int_v& operator+=( int_v rhs ) {
        *this = *this + rhs;
        return *this;
      }
      int_v& operator-=( int_v rhs ) {
        *this = *this - rhs;
        return *this;
      }
      int_v& operator*=( int_v rhs ) {
        *this = *this * rhs;
        return *this;
      }

      friend int_v operator+( int_v lhs, int_v rhs ) { return _mm256_add_epi32( lhs, rhs ); }
      friend int_v operator-( int_v lhs, int_v rhs ) { return _mm256_sub_epi32( lhs, rhs ); }
      friend int_v operator*( int_v lhs, int_v rhs ) { return _mm256_mullo_epi32( lhs, rhs ); }
      friend int_v operator-( int_v x ) { return -1 * x; }

      friend int_v operator&( int_v lhs, int_v rhs ) { return _mm256_and_si256( lhs, rhs ); }
      friend int_v operator|( int_v lhs, int_v rhs ) { return _mm256_or_si256( lhs, rhs ); }
      friend int_v operator^( int_v lhs, int_v rhs ) { return _mm256_xor_si256( lhs, rhs ); }
      friend int_v operator~( int_v x ) { return x ^ _mm256_set1_epi32( -1 ); }

      friend int_v operator<<( int_v lhs, int rhs ) { return _mm256_slli_epi32( lhs, rhs ); }
      friend int_v operator>>( int_v lhs, int rhs ) { return _mm256_srli_epi32( lhs, rhs ); }
      friend int_v operator<<( int_v lhs, int_v rhs ) { return _mm256_sllv_epi32( lhs, rhs ); }
      friend int_v operator>>( int_v lhs, int_v rhs ) { return _mm256_srlv_epi32( lhs, rhs ); }

      friend int_v min( int_v lhs, int_v rhs ) { return _mm256_min_epi32( lhs, rhs ); }
      friend int_v max( int_v lhs, int_v rhs ) { return _mm256_max_epi32( lhs, rhs ); }
      friend int_v abs( int_v x ) { return _mm256_abs_epi32( x ); }

      friend int_v signselect( float_v s, int_v a, int_v b ) {
        return _mm256_mask_mov_epi32( a, ( s < float_v{0.f} ).native(), b );
      }
      friend int_v select( mask_v mask, int_v a, int_v b ) { return _mm256_mask_mov_epi32( b, mask.native(), a ); }

      friend mask_v operator<( int_v lhs, int_v rhs ) { return _mm256_cmplt_epi32_mask( lhs, rhs ); }
      friend mask_v operator>( int_v lhs, int_v rhs ) { return _mm256_cmpgt_epi32_mask( lhs, rhs ); }
      friend mask_v operator<=( int_v lhs, int_v rhs ) { return !( lhs > rhs ); }
      friend mask_v operator>=( int_v lhs, int_v rhs ) { return !( lhs < rhs ); }
      friend mask_v operator==( int_v lhs, int_v rhs ) { return _mm256_cmpeq_epi32_mask( lhs, rhs ); }
      friend std::ostream& operator<<( std::ostream& os, int_v x ) {
        return print_vector<int, size()>( os, x, "avx256" );
      }

    private:
      __m256i data{};
    };
  } // namespace avx256

  namespace simd_traits {
    template <>
    struct traits<avx256::int_v> {
      constexpr static bool is_specialized = true;
      constexpr static bool is_simd_integral = true;
    };
  } // namespace simd_traits

  namespace avx256 {
    using details::pow;

    inline mask_v::operator int_v() const { return select( *this, int_v{1}, int_v{0} ); }

    inline float_v::operator int_v() const { return int_v{_mm256_cvttps_epi32( data )}; }

    inline int_v castToInt( float_v x ) { return int_v{_mm256_castps_si256( x )}; }

    inline float_v castToFloat( int_v x ) { return float_v{_mm256_castsi256_ps( x )}; }

    inline int_v gather( const int* base, int_v idx ) { return _mm256_i32gather_epi32( base, idx, sizeof( int ) ); }

    inline float_v gather( const float* base, int_v idx ) { return _mm256_i32gather_ps( base, idx, sizeof( float ) ); }

    inline int_v maskgather( const int* base, int_v idx, mask_v mask, int_v source ) {
      return _mm256_mmask_i32gather_epi32( source, mask.native(), idx, base, sizeof( int ) );
    }

    inline float_v maskgather( const float* base, int_v idx, mask_v mask, float_v source ) {
      return _mm256_mmask_i32gather_ps( source, mask.native(), idx, base, sizeof( float ) );
    }

    inline void scatter( int* base, int_v idx, int_v value ) {
      _mm256_i32scatter_epi32( base, idx, value, sizeof( int ) );
    }

    inline void scatter( float* base, int_v idx, float_v value ) {
      _mm256_i32scatter_ps( base, idx, value, sizeof( float ) );
    }

    inline void maskscatter( int* base, int_v idx, mask_v mask, int_v value ) {
      _mm256_mask_i32scatter_epi32( base, mask.native(), idx, value, sizeof( int ) );
    }

    inline void maskscatter( float* base, int_v idx, mask_v mask, float_v value ) {
      _mm256_mask_i32scatter_ps( base, mask.native(), idx, value, sizeof( float ) );
    }

    inline int_v types::indices() { return _mm256_setr_epi32( 0, 1, 2, 3, 4, 5, 6, 7 ); }

    inline int_v types::indices( int start ) { return indices() + start; }

    inline int_v operator+( mask_v lhs, mask_v rhs ) { return int_v{lhs} + int_v{rhs}; }
    inline int_v popcount_v( int_v a ) { return SIMDWrapper::popcount_v( a ); }
  } // namespace avx256

  namespace avx512 {
    constexpr InstructionSet instructionSet() { return AVX512; }

    class int_v;

    class mask_v {
    public:
      mask_v() {} // Constructor must be empty
      mask_v( __mmask16 m ) : data{m} {}
      explicit mask_v( bool v ) : mask_v{v ? __mmask16{0xFFFF} : __mmask16{0x0000}} {}

      mask_v& operator=( __mmask16 m ) {
        data = m;
        return *this;
      }

      constexpr static std::size_t size() { return 16; }

      operator int_v() const;

      friend mask_v operator&&( mask_v lhs, mask_v rhs ) { return lhs.native() & rhs.native(); }
      friend mask_v operator||( mask_v lhs, mask_v rhs ) { return lhs.native() | rhs.native(); }
      friend mask_v operator!( mask_v x ) { return ~x.native(); }

      friend bool all( mask_v mask ) { return mask.native() == 0xFFFF; }
      friend bool none( mask_v mask ) { return mask.native() == 0x0000; }
      friend bool any( mask_v mask ) { return mask.native() != 0x0000; }
      friend bool testbit( mask_v mask, int bit ) { return ( mask.native() & ( 1 << bit ) ) != 0; }
      // TODO: c++20 countl_zero
      friend int ffs( mask_v mask ) { return __builtin_ffs( mask.native() ); }
      friend int popcount( mask_v mask ) { return _mm_popcnt_u32( mask.native() ); }

      friend std::ostream& operator<<( std::ostream& os, mask_v x ) {
        return print_bitset<16>( os, x.native(), "avx512" );
      }

      __mmask16 native() const { return data; }

    private:
      __mmask16 data{};
    };

    class float_v;

    struct types {
      constexpr static std::size_t size = 16;
      using int_v = avx512::int_v;
      using float_v = avx512::float_v;
      using mask_v = avx512::mask_v;
      static mask_v mask_true() { return mask_v{true}; }
      static mask_v mask_false() { return mask_v{false}; }
      static int_v indices();
      static int_v indices( int start );
      static mask_v loop_mask( int i, int n ) { return ( ( i + 16 ) > n ) ? ~( 0xFFFF << ( n - i ) ) : 0xFFFF; }
      static constexpr InstructionSet instructionSet() { return AVX512; }
    };

    class float_v {
    public:
      constexpr static std::size_t size() { return avx512::types::size; }
      using span = LHCb::span<float, avx512::types::size>;
      using const_span = LHCb::span<const float, avx512::types::size>;

      float_v() {} // Constructor must be empty
      float_v( scalar::float_v f ) : float_v{_mm512_set1_ps( f.cast() )} {}
      template <typename T, std::enable_if_t<std::is_integral_v<T>, int> = 0>
      float_v( T f ) : float_v( _mm512_set1_ps( float( f ) ) ) {}
      float_v( float f ) : float_v{_mm512_set1_ps( f )} {}
      float_v( const float* f ) : float_v{_mm512_loadu_ps( f )} {}
      float_v( const_span f ) : float_v{_mm512_loadu_ps( f.data() )} {}
      float_v( __m512 f ) : data{f} {}

      float_v& operator=( __m512 f ) {
        data = f;
        return *this;
      }

      operator __m512() const { return data; }
      explicit operator int_v() const;

      const float_v& store( float* ptr ) const { return store( span{ptr, size()} ); }
      const float_v& store( span s ) const {
        _mm512_storeu_ps( s.data(), data );
        return *this;
      }

      const float_v& compressstore( mask_v mask, float* ptr ) const { return compressstore( mask, span{ptr, size()} ); }
      const float_v& compressstore( mask_v mask, span s ) const {
        _mm512_mask_compressstoreu_ps( s.data(), mask.native(), data );
        return *this;
      }

      float hmax() const { return _mm512_reduce_max_ps( data ); }
      float hmin() const { return _mm512_reduce_min_ps( data ); }
      float hadd() const { return _mm512_reduce_add_ps( data ); }
      float hmax( mask_v mask ) const { return _mm512_mask_reduce_max_ps( mask.native(), data ); }
      float hmin( mask_v mask ) const { return _mm512_mask_reduce_min_ps( mask.native(), data ); }
      float hadd( mask_v mask ) const { return _mm512_mask_reduce_add_ps( mask.native(), data ); }

      float_v& operator+=( float_v rhs ) {
        *this = *this + rhs;
        return *this;
      }
      float_v& operator-=( float_v rhs ) {
        *this = *this - rhs;
        return *this;
      }
      float_v& operator*=( float_v rhs ) {
        *this = *this * rhs;
        return *this;
      }
      float_v& operator/=( float_v rhs ) {
        *this = *this / rhs;
        return *this;
      }

      friend float_v operator+( float_v lhs, float_v rhs ) { return _mm512_add_ps( lhs, rhs ); }
      friend float_v operator-( float_v lhs, float_v rhs ) { return _mm512_sub_ps( lhs, rhs ); }
      friend float_v operator*( float_v lhs, float_v rhs ) { return _mm512_mul_ps( lhs, rhs ); }
      friend float_v operator/( float_v lhs, float_v rhs ) { return _mm512_div_ps( lhs, rhs ); }
      friend float_v operator-( float_v x ) { return -1.f * x; }

      friend float_v operator&( float_v lhs, float_v rhs ) { return _mm512_and_ps( lhs, rhs ); }
      friend float_v operator|( float_v lhs, float_v rhs ) { return _mm512_or_ps( lhs, rhs ); }
      friend float_v operator^( float_v lhs, float_v rhs ) { return _mm512_xor_ps( lhs, rhs ); }

      friend float_v operator&&( float_v lhs, float_v rhs ) { return _mm512_and_ps( lhs, rhs ); }
      friend float_v operator||( float_v lhs, float_v rhs ) { return _mm512_or_ps( lhs, rhs ); }
      friend float_v operator!( float_v x ) { return x ^ _mm512_castsi512_ps( _mm512_set1_epi32( -1 ) ); }

      friend float_v min( float_v lhs, float_v rhs ) { return _mm512_min_ps( lhs, rhs ); }
      friend float_v max( float_v lhs, float_v rhs ) { return _mm512_max_ps( lhs, rhs ); }
      friend float_v abs( float_v v ) { return _mm512_abs_ps( v ); }
      friend float_v copysign( float_v x, float_v y ) {
        return x ^ ( y & _mm512_castsi512_ps( _mm512_set1_epi32( 0x80000000 ) ) );
      }

      friend float_v signselect( float_v s, float_v a, float_v b ) {
        return _mm512_mask_mov_ps( a, ( s < float_v{0.f} ).native(), b );
      }
      friend float_v select( mask_v mask, float_v a, float_v b ) { return _mm512_mask_mov_ps( b, mask.native(), a ); }

      friend float_v sqrt( float_v v ) { return _mm512_sqrt_ps( v ); }
      [[deprecated( "Be aware, yields different results for AMD and Intel" )]] friend float_v rsqrt( float_v v ) {
        return _mm512_rsqrt14_ps( v );
      }
      [[deprecated( "Be aware, yields different results for AMD and Intel" )]] friend float_v rcp( float_v v ) {
        return _mm512_rcp14_ps( v );
      }

      friend mask_v operator<( float_v lhs, float_v rhs ) { return _mm512_cmp_ps_mask( lhs, rhs, _CMP_LT_OS ); }
      friend mask_v operator>( float_v lhs, float_v rhs ) { return _mm512_cmp_ps_mask( lhs, rhs, _CMP_GT_OS ); }
      friend mask_v operator<=( float_v lhs, float_v rhs ) { return !( lhs > rhs ); }
      friend mask_v operator>=( float_v lhs, float_v rhs ) { return !( lhs < rhs ); }
      friend mask_v operator==( float_v lhs, float_v rhs ) { return _mm512_cmp_ps_mask( lhs, rhs, _CMP_EQ_OS ); }
      friend std::ostream& operator<<( std::ostream& os, float_v x ) {
        return print_vector<float, size()>( os, x, "avx512" );
      }

    private:
      __m512 data{};
    };

    class int_v {
    public:
      constexpr static std::size_t size() { return avx512::types::size; }
      using span = LHCb::span<int, avx512::types::size>;
      using const_span = LHCb::span<const int, avx512::types::size>;

      int_v() {} // Constructor must be empty
      int_v( int f ) : int_v{_mm512_set1_epi32( f )} {}
      int_v( const int* f ) : int_v{_mm512_loadu_si512( f )} {}
      int_v( const_span f ) : int_v{_mm512_loadu_si512( f.data() )} {}
      constexpr int_v( __m512i f ) : data{f} {}

      int_v& operator=( __m512i f ) {
        data = f;
        return *this;
      }

      operator __m512i() const { return data; }
      operator float_v() const { return float_v{_mm512_cvtepi32_ps( data )}; }

      const int_v& store( int* ptr ) const { return store( span{ptr, size()} ); }
      const int_v& store( span s ) const {
        _mm512_storeu_si512( s.data(), data );
        return *this;
      }

      const int_v& compressstore( mask_v mask, int* ptr ) const { return compressstore( mask, span{ptr, size()} ); }
      const int_v& compressstore( mask_v mask, span s ) const {
        _mm512_mask_compressstoreu_epi32( s.data(), mask.native(), data );
        return *this;
      }

      int hmax() const { return _mm512_reduce_max_epi32( data ); }
      int hmin() const { return _mm512_reduce_min_epi32( data ); }
      int hadd() const { return _mm512_reduce_add_epi32( data ); }
      int hmax( mask_v mask ) const { return _mm512_mask_reduce_max_epi32( mask.native(), data ); }
      int hmin( mask_v mask ) const { return _mm512_mask_reduce_min_epi32( mask.native(), data ); }
      int hadd( mask_v mask ) const { return _mm512_mask_reduce_add_epi32( mask.native(), data ); }

      int_v& operator+=( int_v rhs ) {
        *this = *this + rhs;
        return *this;
      }
      int_v& operator-=( int_v rhs ) {
        *this = *this - rhs;
        return *this;
      }
      int_v& operator*=( int_v rhs ) {
        *this = *this * rhs;
        return *this;
      }

      friend int_v operator+( int_v lhs, int_v rhs ) { return _mm512_add_epi32( lhs, rhs ); }
      friend int_v operator-( int_v lhs, int_v rhs ) { return _mm512_sub_epi32( lhs, rhs ); }
      friend int_v operator*( int_v lhs, int_v rhs ) { return _mm512_mullo_epi32( lhs, rhs ); }
      friend int_v operator-( int_v x ) { return -1 * x; }

      friend int_v operator&( int_v lhs, int_v rhs ) { return _mm512_and_si512( lhs, rhs ); }
      friend int_v operator|( int_v lhs, int_v rhs ) { return _mm512_or_si512( lhs, rhs ); }
      friend int_v operator^( int_v lhs, int_v rhs ) { return _mm512_xor_si512( lhs, rhs ); }
      friend int_v operator~( int_v x ) { return x ^ _mm512_set1_epi32( -1 ); }

      friend int_v operator<<( int_v lhs, int rhs ) { return _mm512_slli_epi32( lhs, rhs ); }
      friend int_v operator>>( int_v lhs, int rhs ) { return _mm512_srli_epi32( lhs, rhs ); }
      friend int_v operator<<( int_v lhs, int_v rhs ) { return _mm512_sllv_epi32( lhs, rhs ); }
      friend int_v operator>>( int_v lhs, int_v rhs ) { return _mm512_srlv_epi32( lhs, rhs ); }

      friend int_v min( int_v lhs, int_v rhs ) { return _mm512_min_epi32( lhs, rhs ); }
      friend int_v max( int_v lhs, int_v rhs ) { return _mm512_max_epi32( lhs, rhs ); }
      friend int_v abs( int_v x ) { return _mm512_abs_epi32( x ); }

      friend int_v signselect( float_v s, int_v a, int_v b ) {
        return _mm512_mask_mov_epi32( a, ( s < float_v{0.f} ).native(), b );
      }
      friend int_v select( mask_v mask, int_v a, int_v b ) { return _mm512_mask_mov_epi32( b, mask.native(), a ); }

      friend mask_v operator<( int_v lhs, int_v rhs ) { return _mm512_cmplt_epi32_mask( lhs, rhs ); }
      friend mask_v operator>( int_v lhs, int_v rhs ) { return _mm512_cmpgt_epi32_mask( lhs, rhs ); }
      friend mask_v operator<=( int_v lhs, int_v rhs ) { return !( lhs > rhs ); }
      friend mask_v operator>=( int_v lhs, int_v rhs ) { return !( lhs < rhs ); }
      friend mask_v operator==( int_v lhs, int_v rhs ) { return _mm512_cmpeq_epi32_mask( lhs, rhs ); }
      friend std::ostream& operator<<( std::ostream& os, int_v x ) {
        return print_vector<int, size()>( os, x, "avx512" );
      }

    private:
      __m512i data{};
    };
  } // namespace avx512
  namespace simd_traits {
    template <>
    struct traits<avx512::int_v> {
      constexpr static bool is_specialized = true;
      constexpr static bool is_simd_integral = true;
    };
  } // namespace simd_traits
  namespace avx512 {
    using details::pow;

    inline mask_v::operator int_v() const { return select( *this, int_v{1}, int_v{0} ); }

    inline float_v::operator int_v() const { return int_v{_mm512_cvttps_epi32( data )}; }

    inline int_v castToInt( float_v x ) { return int_v{_mm512_castps_si512( x )}; }

    inline float_v castToFloat( int_v x ) { return float_v{_mm512_castsi512_ps( x )}; }

    inline int_v gather( const int* base, int_v idx ) { return _mm512_i32gather_epi32( idx, base, sizeof( int ) ); }

    inline float_v gather( const float* base, int_v idx ) { return _mm512_i32gather_ps( idx, base, sizeof( float ) ); }

    inline int_v maskgather( const int* base, int_v idx, mask_v mask, int_v source ) {
      return _mm512_mask_i32gather_epi32( source, mask.native(), idx, base, sizeof( int ) );
    }

    inline float_v maskgather( const float* base, int_v idx, mask_v mask, float_v source ) {
      return _mm512_mask_i32gather_ps( source, mask.native(), idx, base, sizeof( float ) );
    }

    inline void scatter( int* base, int_v idx, int_v value ) {
      _mm512_i32scatter_epi32( base, idx, value, sizeof( int ) );
    }

    inline void scatter( float* base, int_v idx, float_v value ) {
      _mm512_i32scatter_ps( base, idx, value, sizeof( float ) );
    }

    inline void maskscatter( int* base, int_v idx, mask_v mask, int_v value ) {
      _mm512_mask_i32scatter_epi32( base, mask.native(), idx, value, sizeof( int ) );
    }

    inline void maskscatter( float* base, int_v idx, mask_v mask, float_v value ) {
      _mm512_mask_i32scatter_ps( base, mask.native(), idx, value, sizeof( float ) );
    }

    inline int_v types::indices() { return _mm512_setr_epi32( 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ); }

    inline int_v types::indices( int start ) { return indices() + start; }

    inline int_v operator+( mask_v lhs, mask_v rhs ) { return int_v{lhs} + int_v{rhs}; }
    inline int_v popcount_v( int_v a ) { return SIMDWrapper::popcount_v( a ); }
  } // namespace avx512
}

SIMDWRAPPER_SPECIALISE_HELPER_TYPES( SIMDWrapper::avx256 );
SIMDWRAPPER_SPECIALISE_HELPER_TYPES( SIMDWrapper::avx512 );

namespace SIMDWrapper {
#endif

  namespace best {
    using float_v = avx256::float_v;
    using int_v   = avx256::int_v;
    using types   = avx256::types;
    constexpr InstructionSet instructionSet() { return avx256::instructionSet(); }
  } // namespace best

  template <InstructionSet>
  struct type_map {};

  template <>
  struct type_map<InstructionSet::Best> {
    using type = best::types;
    static constexpr InstructionSet instructionSet() { return best::instructionSet(); }
    static InstructionSet           stackInstructionSet();
  };

  template <>
  struct type_map<InstructionSet::Scalar> {
    using type = scalar::types;
    static constexpr InstructionSet instructionSet() { return scalar::instructionSet(); }
    static InstructionSet           stackInstructionSet();
  };

  template <>
  struct type_map<InstructionSet::SSE> {
    using type = sse::types;
    static constexpr InstructionSet instructionSet() { return sse::instructionSet(); }
    static InstructionSet           stackInstructionSet();
  };

  template <>
  struct type_map<InstructionSet::AVX2> {
    using type = avx2::types;
    static constexpr InstructionSet instructionSet() { return avx2::instructionSet(); }
    static InstructionSet           stackInstructionSet();
  };

  template <>
  struct type_map<InstructionSet::AVX256> {
    using type = avx256::types;
    static constexpr InstructionSet instructionSet() { return avx256::instructionSet(); }
    static InstructionSet           stackInstructionSet();
  };

  template <>
  struct type_map<InstructionSet::AVX512> {
    using type = avx512::types;
    static constexpr InstructionSet instructionSet() { return avx512::instructionSet(); }
    static InstructionSet           stackInstructionSet();
  };

  template <>
  struct type_map<InstructionSet::Neon> {
    using type = neon::types;
    static constexpr InstructionSet instructionSet() { return neon::instructionSet(); }
    static InstructionSet           stackInstructionSet();
  };

  template <InstructionSet simd>
  using type_map_t = typename type_map<simd>::type;

  // Map (scalar_type, simd_types) to vector types:
  template <typename, typename>
  struct vector_type_map {};

  template <typename simd_t>
  struct vector_type_map<int, simd_t> {
    using type = typename simd_t::int_v;
  };

  template <typename simd_t>
  struct vector_type_map<float, simd_t> {
    using type = typename simd_t::float_v;
  };

  template <typename simd_t>
  struct vector_type_map<bool, simd_t> {
    using type = typename simd_t::mask_v;
  };

  template <typename T, typename simd_t>
  using vector_type_map_t = typename vector_type_map<T, simd_t>::type;
} // namespace SIMDWrapper

// Helper to build SIMDWrapper accessors:
#define SOA_ACCESSOR( name, location )                                                                                 \
  template <typename T>                                                                                                \
  inline auto name( int key ) const {                                                                                  \
    return T( location + key );                                                                                        \
  }                                                                                                                    \
  template <typename T>                                                                                                \
  inline auto broadcast_##name( int key ) const {                                                                      \
    return T{*( location + key )};                                                                                     \
  }                                                                                                                    \
  template <typename T>                                                                                                \
  inline auto gather_##name( const T& key ) const {                                                                    \
    return gather( location, key );                                                                                    \
  }                                                                                                                    \
  template <typename T, typename KeyT, typename MaskT>                                                                 \
  inline auto maskgather_##name( const KeyT& key, const MaskT& mask, const T& src ) const {                            \
    return maskgather( location, key, mask, src );                                                                     \
  }                                                                                                                    \
  template <typename T>                                                                                                \
  inline auto store_##name( int key, const T& v ) {                                                                    \
    v.store( location + key );                                                                                         \
  }                                                                                                                    \
  template <typename T, typename MaskT>                                                                                \
  inline auto compressstore_##name( int key, const MaskT& mask, const T& v ) {                                         \
    v.compressstore( mask, location + key );                                                                           \
  }

// With variadic parameters:
#define SOA_ACCESSOR_VAR( name, location, ... )                                                                        \
  template <typename T>                                                                                                \
  inline auto name( int key, __VA_ARGS__ ) const {                                                                     \
    return T( location + key );                                                                                        \
  }                                                                                                                    \
  template <typename T>                                                                                                \
  inline auto gather_##name( const T& key, __VA_ARGS__ ) const {                                                       \
    return gather( location, key );                                                                                    \
  }                                                                                                                    \
  template <typename T, typename KeyT, typename MaskT>                                                                 \
  inline auto maskgather_##name( const KeyT& key, const MaskT& mask, const T& src, __VA_ARGS__ ) const {               \
    return maskgather( location, key, mask, src );                                                                     \
  }                                                                                                                    \
  template <typename T>                                                                                                \
  inline auto store_##name( int key, __VA_ARGS__, const T& v ) {                                                       \
    v.store( location + key );                                                                                         \
  }                                                                                                                    \
  template <typename T, typename MaskT>                                                                                \
  inline auto compressstore_##name( int key, __VA_ARGS__, MaskT mask, const T& v ) {                                   \
    v.compressstore( mask, location + key );                                                                           \
  }

// Align size to AVX512 boundary
constexpr int align_size( int n ) { return ( n / 16 + 1 ) * 16; }

// Maths :

template <typename T>
inline auto faster_atan2( T y, T x ) { // error < 0.07 rad, no 0/0 security
  const T c1    = M_PI / 4.0;
  const T c2    = 3.0 * M_PI / 4.0;
  T       abs_y = abs( y );

  T x_plus_y = x + abs_y;
  T x_sub_y  = x - abs_y;
  T y_sub_x  = abs_y - x;

  T nom = signselect( x, x_sub_y, x_plus_y );
  T den = signselect( x, x_plus_y, y_sub_x );

  T r     = nom / den;
  T angle = signselect( x, c1, c2 ) - c1 * r;

  return copysign( angle, y );
}

template <typename T>
inline auto vapprox_log( T x ) {
  return castToInt( x ) * 8.2629582881927490e-8f - 87.989971088f;
}

template <typename M, typename T>
inline void swap( M mask, T& a, T& b ) {
  T tmp = select( mask, b, a );
  b     = select( mask, a, b );
  a     = tmp;
}
