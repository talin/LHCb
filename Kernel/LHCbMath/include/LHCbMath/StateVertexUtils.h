/***************************************************************************** \
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbMath/MatVec.h"

namespace ROOT::Math {
  template <typename T>
  auto X( const T& t ) {
    return t.x();
  }
  template <typename T>
  auto Y( const T& t ) {
    return t.y();
  }
  template <typename T>
  auto Z( const T& t ) {
    return t.z();
  }
} // namespace ROOT::Math

namespace LHCb {

  namespace StateVertexUtils {

    enum ReturnStatus { Failure, Success };

    ///////////////////////////////////////////////////////////////////////////
    /// Return the doca between two track states
    ///////////////////////////////////////////////////////////////////////////
    template <typename StateVector>
    auto doca( const StateVector& stateA, const StateVector& stateB );

    ///////////////////////////////////////////////////////////////////////////
    /// Return the distance between a track state and a point
    ///////////////////////////////////////////////////////////////////////////
    template <typename StateVector, typename XYZPoint>
    auto doca( const StateVector& stateA, const XYZPoint& pos );

    /////////////////////////////////////////////////////////////////////////
    /// Compute the chi2 and decaylength of a 'particle' with respect
    /// to a vertex. Return 1 if successful.
    /// This should probably go into LHCb math.
    /////////////////////////////////////////////////////////////////////////
    ReturnStatus computeChiSquare( const Gaudi::XYZPoint& pos, const Gaudi::XYZVector& mom,
                                   const Gaudi::SymMatrix6x6& cov6, const Gaudi::XYZPoint& motherpos,
                                   const Gaudi::SymMatrix3x3& mothercov, double& chi2, double& decaylength,
                                   double& decaylengtherr );

    /////////////////////////////////////////////////////////////////////////
    /// Compute the chi2,  decaylength and decaylength uncertainty of a 'particle' with respect
    /// to a vertex.
    /////////////////////////////////////////////////////////////////////////

    template <typename Vec_t, typename Sym6x6_t, typename Sym3x3_t, typename Vec_t1>
    auto computeChiSquare( Vec_t const& pos, Vec_t const& mom, Sym6x6_t const& cov6, Vec_t1 const& motherpos,
                           Sym3x3_t const& mothercov ) {
      // pos:  decay vertex of particle
      // vec:  direction or momentum of particle (does not need to be normalized)
      // cov6: corresponding covariance matrix

      // This calculation is basically a 1-iteration beamspot fit. The
      // constraint is
      //
      //    r = x - lambda p/|p| - xbs
      //
      // where x and p are the position of the decay vertex of the
      // candidate and its momentum, lambda is the decaylength and xbs
      // the position of the beamspot. The covariance in the constraint
      // is
      //
      //    V = Vbs + Vxx - a * Vxp - a Vxp^T + a^2 * Vpp
      //
      // where a=lambda/|p|^2. It needs an initial estimate for the
      // flightlength, for which we simply take the projection of deltaX
      // on the direction. We now minimize  the chisquare contribution
      //
      //     chi^2 = r^T V^{-1} r
      //
      // for lambda.
      //
      // @todo add an implementation for ipchi2-of-composite that just returns
      //       it without calculating the decay length [error] too. OL tried
      //       this in https://gitlab.cern.ch/snippets/894, but without a
      //       better study of which implementation copes better when the
      //       matrix inversion fails (which it inevitably does sometimes)
      //       lets not switch to using it.

      // const auto dx    = pos - motherpos;
      const auto dx    = Vec_t{pos.X() - motherpos.X(), pos.Y() - motherpos.Y(), pos.Z() - motherpos.Z()};
      const auto p3mag = mom.mag();
      const auto dir   = mom * ( 1.f / p3mag ); // rsqrt
      const auto a     = dot( dir, dx ) / p3mag;

      LHCb::LinAlg::resize_t<Sym6x6_t, 3> W{};
      W.fill( [&]( auto i, auto j ) {
        return mothercov( i, j ) + cov6( i, j ) + a * a * cov6( i + 3, j + 3 ) -
               a * ( cov6( i + 3, j ) + cov6( j + 3, i ) );
      } );
      // TODO check if easier to just give 3x3 cov matrices instead of creating 6x6 cov matrix

      W = W.invChol();

      const auto halfdChi2dLam2 = similarity( dir, W );
      const auto decaylength    = dir.dot( W * dx ) / halfdChi2dLam2;
      const auto decaylengtherr = sqrt( 1 / halfdChi2dLam2 ); // TODO: put rsqrt here
      const auto res            = dx - dir * decaylength;
      const auto chi2           = similarity( res, W );

      return std::tuple{chi2, decaylength, decaylengtherr};
    }

    /////////////////////////////////////////////////////////////////////////
    /// Compute the point of the doca of two track states.  Return 1 if successful.
    /////////////////////////////////////////////////////////////////////////
    template <typename StateVector, typename XYZPoint>
    ReturnStatus poca( const StateVector& stateA, const StateVector& stateB, XYZPoint& vertex );

    ///////////////////////////////////////////////////////////////////////////
    /// Return the doca between two track states
    ///////////////////////////////////////////////////////////////////////////
    template <typename StateVector>
    auto doca( const StateVector& stateA, const StateVector& stateB ) {
      // first compute the cross product of the directions.
      const auto txA = stateA.tx();
      const auto tyA = stateA.ty();
      const auto txB = stateB.tx();
      const auto tyB = stateB.ty();
      auto       nx  = tyA - tyB;             //   y1 * z2 - y2 * z1
      auto       ny  = txB - txA;             // - x1 * z2 + x2 * z1
      auto       nz  = txA * tyB - tyA * txB; //   x1 * y2 - x2 * y1
      auto       n   = std::sqrt( nx * nx + ny * ny + nz * nz );
      // compute the doca
      auto dx    = stateA.x() - stateB.x();
      auto dy    = stateA.y() - stateB.y();
      auto dz    = stateA.z() - stateB.z();
      auto ndoca = dx * nx + dy * ny + dz * nz;
      return ndoca / n;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Return the distance between a track state and a point
    ///////////////////////////////////////////////////////////////////////////
    template <typename StateVector, typename XYZPoint>
    auto doca( const StateVector& state, const XYZPoint& pos ) {
      auto tx = state.tx();
      auto ty = state.ty();
      auto dz = pos.z() - state.z();
      auto dx = state.x() + dz * tx - pos.x();
      auto dy = state.y() + dz * ty - pos.y();
      return std::sqrt( ( dx * dx + dy * dy ) / ( 1.0 + tx * tx + ty * ty ) );
    }

    /////////////////////////////////////////////////////////////////////////
    /// Compute the point of the doca of two track states.  Return 1 if successful.
    /////////////////////////////////////////////////////////////////////////
    template <typename StateVector, typename XYZPoint>
    ReturnStatus poca( const StateVector& stateA, const StateVector& stateB, XYZPoint& vertex ) {
      // find the z-positions of the doca. then take the average of the two points.
      // see also Gaudi::Math::closestPointParams.
      const auto zA  = stateA.z();
      const auto xA  = stateA.x();
      const auto yA  = stateA.y();
      const auto txA = stateA.tx();
      const auto tyA = stateA.ty();
      const auto zB  = stateB.z();
      const auto xB  = stateB.x();
      const auto yB  = stateB.y();
      const auto txB = stateB.tx();
      const auto tyB = stateB.ty();

      // define d^2 = ( xA + muA*txA - xB - muB*txB)^2 +  ( yA + muA*tyA - xB - muB*tyB)^2 + (zA + muA - zB - muB)^2
      // compute (half) 2nd derivative to muA and muB in point muA=muB=0
      auto secondAA = txA * txA + tyA * tyA + 1;
      auto secondBB = txB * txB + tyB * tyB + 1;
      auto secondAB = -txA * txB - tyA * tyB - 1;
      // compute inverse matrix, but stop if determinant not positive
      auto         det = secondAA * secondBB - secondAB * secondAB;
      ReturnStatus rc  = Success;
      if ( std::abs( det ) > 0 ) {
        auto secondinvAA = secondBB / det;
        auto secondinvBB = secondAA / det;
        auto secondinvAB = -secondAB / det;
        // compute (half) first derivative
        auto firstA = txA * ( xA - xB ) + tyA * ( yA - yB ) + ( zA - zB );
        auto firstB = -txB * ( xA - xB ) - tyB * ( yA - yB ) - ( zA - zB );
        // compute muA and muB with delta-mu = - seconderivative^-1 * firstderivative
        auto muA = -( secondinvAA * firstA + secondinvAB * firstB );
        auto muB = -( secondinvBB * firstB + secondinvAB * firstA );
        // return the average point
        vertex.SetX( 0.5 * ( xA + muA * txA + xB + muB * txB ) );
        vertex.SetY( 0.5 * ( yA + muA * tyA + yB + muB * tyB ) );
        vertex.SetZ( 0.5 * ( zA + muA + zB + muB ) );
      } else {
        rc = Failure; // parallel tracks
      }
      return rc;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Return the chi2 of a track state with respect to a
    /// vertex. This is also known as the 'IPCHI2'.
    ///////////////////////////////////////////////////////////////////////////
    template <typename STATE, typename POS, typename POSCOV>
    auto vertexChi2( const STATE& state, const POS& vertexpos, const POSCOV& vertexcov ) {
      auto tx = state.tx();
      auto ty = state.ty();
      auto dz = Z( vertexpos ) - state.z();
      auto dx = state.x() + dz * tx - X( vertexpos );
      auto dy = state.y() + dz * ty - Y( vertexpos );

      // compute the covariance matrix. first only the trivial parts:
      const auto& statecov = state.covariance();
      auto        cov00    = vertexcov( 0, 0 ) + statecov( 0, 0 );
      auto        cov10    = vertexcov( 1, 0 ) + statecov( 1, 0 );
      auto        cov11    = vertexcov( 1, 1 ) + statecov( 1, 1 );

      // add the contribution from the extrapolation
      cov00 += dz * dz * statecov( 2, 2 ) + 2 * dz * statecov( 2, 0 );
      cov10 += dz * dz * statecov( 3, 2 ) + dz * ( statecov( 3, 0 ) + statecov( 2, 1 ) );
      cov11 += dz * dz * statecov( 3, 3 ) + 2 * dz * statecov( 3, 1 );

      // add the contribution from pv Z
      cov00 += tx * tx * vertexcov( 2, 2 ) - 2 * tx * vertexcov( 2, 0 );
      cov10 += tx * ty * vertexcov( 2, 2 ) - ty * vertexcov( 2, 0 ) - tx * vertexcov( 2, 1 );
      cov11 += ty * ty * vertexcov( 2, 2 ) - 2 * ty * vertexcov( 2, 1 );

      // invert the covariance matrix on the fly
      return ( dx * dx * cov11 - 2 * dx * dy * cov10 + dy * dy * cov00 ) / ( cov00 * cov11 - cov10 * cov10 );
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Return the chi2 of the vertex of two track states
    ///////////////////////////////////////////////////////////////////////////
    template <typename STATEA, typename STATEB>
    auto vertexChi2( const STATEA& stateA, const STATEB& stateB ) {
      // first compute the cross product of the directions. we'll need this in any case
      const auto txA = stateA.tx();
      const auto tyA = stateA.ty();
      const auto txB = stateB.tx();
      const auto tyB = stateB.ty();
      auto       nx  = tyA - tyB;             //   y1 * z2 - y2 * z1
      auto       ny  = txB - txA;             // - x1 * z2 + x2 * z1
      auto       nz  = txA * tyB - tyA * txB; //   x1 * y2 - x2 * y1
      // auto n2 = nx*nx + ny*ny + nz*nz ;

      // compute doca. we don't divide by the normalization to save time. we call it 'ndoca'
      auto dx    = stateA.x() - stateB.x();
      auto dy    = stateA.y() - stateB.y();
      auto dz    = stateA.z() - stateB.z();
      auto ndoca = dx * nx + dy * ny + dz * nz;

      // figure out what floating point type to use for the covariance matrix manipulations
      using float_t      = std::decay_t<decltype( stateA.covariance()( 0, 0 ) )>;
      using Vector4      = ROOT::Math::SVector<float_t, 4>;
      using SymMatrix4x4 = ROOT::Math::SMatrix<float_t, 4, 4, ROOT::Math::MatRepSym<float_t, 4>>;

      // the hard part: compute the jacobians :-)
      Vector4 jacA, jacB;
      jacA( 0 ) = nx;
      jacA( 1 ) = ny;
      jacA( 2 ) = -dy + dz * tyB;
      jacA( 3 ) = dx - dz * txB;
      jacB( 0 ) = -nx;
      jacB( 1 ) = -ny;
      jacB( 2 ) = dy - dz * tyA;
      jacB( 3 ) = -dx + dz * txA;

      // compute the variance on ndoca
      decltype( auto ) covA = stateA.covariance();
      decltype( auto ) covB = stateB.covariance();
      using ROOT::Math::Similarity;
      auto const varndoca = Similarity( jacA, covA.template Sub<SymMatrix4x4>( 0, 0 ) ) +
                            Similarity( jacB, covB.template Sub<SymMatrix4x4>( 0, 0 ) );

      // return the chi2
      return ndoca * ndoca / varndoca;
    }

  } // namespace StateVertexUtils
} // namespace LHCb
