/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "SIMDWrapper.h"
#include "Utils.h"
#include <cassert>

// just to support 'convert'...
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "GaudiKernel/Vector4DTypes.h"
// just to support 'similarity'...
#include "LHCbMath/Vec3.h"

namespace LHCb::LinAlg {

  // fwd declarations
  template <typename T, int N, int M>
  struct Mat;
  template <typename T, int N>
  struct MatSym;
  template <typename T, int N>
  struct Vec;

  template <typename T>
  constexpr static bool is_mat_v = false;

  template <typename T, int N, int M>
  constexpr static bool is_mat_v<Mat<T, N, M>> = true;

  template <typename T>
  constexpr static bool is_sym_mat_v = false;

  template <typename T, int N>
  constexpr static bool is_sym_mat_v<MatSym<T, N>> = true;

  template <typename T>
  constexpr static bool is_vec_v = false;

  template <typename T, int N>
  constexpr static bool is_vec_v<Vec<T, N>> = true;

  template <typename F>
  constexpr auto initialize_with_zeros() {
    static_assert( is_mat_v<F> or is_sym_mat_v<F> or is_vec_v<F> );
    F out{};
    out.m.fill( 0. );
    return out;
  }

  namespace details {

    template <typename T, int newN, int newM = newN>
    struct resize_impl {};

    template <typename T, int N, int newN>
    struct resize_impl<MatSym<T, N>, newN, newN> {
      using type = MatSym<T, newN>;
    };

    template <typename T, int N, int M, int newN, int newM>
    struct resize_impl<Mat<T, N, M>, newN, newM> {
      using type = Mat<T, newN, newM>;
    };

    template <typename T, int N, int newN>
    struct resize_impl<Vec<T, N>, newN> {
      using type = Vec<T, newN>;
    };

    template <typename T, typename X = void>
    struct has_rsqrt {
      constexpr static bool value = false;
    };

    template <typename T>
    struct has_rsqrt<T, std::void_t<decltype( rsqrt( std::declval<T>() ) )>> {
      constexpr static bool value = true;
    };

    /** Helper for implementing std::array equality without requiring that the
     *  result is a bool. Useful for checking equality of matrices and vectors
     *  with value types that are themselves vectors, e.g. in explicitly
     *  vectorised code.
     */
    template <typename T, std::size_t... Is>
    [[gnu::pure]] auto cmp_array( std::array<T, sizeof...( Is )> const& lhs, std::array<T, sizeof...( Is )> const& rhs,
                                  std::index_sequence<Is...> ) {
      return ( ( lhs[Is] == rhs[Is] ) && ... );
    }
  } // namespace details

  template <typename T, int newN, int newM = newN>
  using resize_t = typename details::resize_impl<T, newN, newM>::type;

  /** See https://gitlab.cern.ch/lhcb/LHCb/-/issues/144 for a strategy on removing the warning here.
   */
  template <typename T>
  [[deprecated( "Be aware, the SIMD version yields different results for AMD and Intel" )]] T rsqrt( T const& t ) {
    if constexpr ( details::has_rsqrt<T>::value ) {
      return rsqrt( t );
    } else {
      using std::sqrt;
      return T{1} / sqrt( t );
    }
  }

  // Generic Vector object
  /** @class Vec
   *  implementation of an N-dim Vector of type T
   */
  template <typename T, int N>
  struct Vec {
    static constexpr auto size = N;
    using value_type           = T;
    std::array<T, N> m{};

    Vec() {}

    template <typename... Ts, typename std::enable_if<sizeof...( Ts ) == N, bool>::type = true>
    Vec( Ts... vs ) : m{vs...} {}

    template <typename U>
    Vec<U, N> cast() const {
      Vec<U, N> C;
      Utils::unwind<0, N>( [&]( auto i ) { C.m[i] = m[i]; } );
      return C;
    }

    /** get a val from the vector
     *  @param i index
     */
    [[gnu::pure]] const T& operator()( int i ) const { return m[i]; }

    /** get a val from the vector
     *  @param i index
     */
    T& operator()( int i ) { return m[i]; }

    T& x() {
      static_assert( N > 0 );
      return m[0];
    }
    T& y() {
      static_assert( N > 1 );
      return m[1];
    }
    T& z() {
      static_assert( N > 2 );
      return m[2];
    }

    T x() const {
      static_assert( N > 0 );
      return m[0];
    }

    T y() const {
      static_assert( N > 1 );
      return m[1];
    }
    T z() const {
      static_assert( N > 2 );
      return m[2];
    }

    /// elementwise comparison ==
    [[gnu::pure]] friend auto operator==( Vec<T, N> const& lhs, Vec<T, N> const& rhs ) {
      return details::cmp_array( lhs.m, rhs.m, std::make_index_sequence<N>{} );
    }
    /// elementwise comparison !=
    [[gnu::pure]] friend auto operator!=( Vec<T, N> const& lhs, Vec<T, N> const& rhs ) { return not( lhs == rhs ); }

    /// elementwise addition
    [[gnu::pure]] friend Vec<T, N> operator+( const Vec<T, N>& A, const Vec<T, N>& B ) {
      Vec<T, N> C;
      Utils::unwind<0, N>( [&]( auto i ) { C.m[i] = A.m[i] + B.m[i]; } );
      return C;
    }
    [[gnu::pure]] friend Vec<T, N> operator+( const Vec<T, N>& A, const T& k ) {
      Vec<T, N> C;
      Utils::unwind<0, N>( [&]( auto i ) { C.m[i] = A.m[i] + k; } );
      return C;
    }
    [[gnu::pure]] friend Vec<T, N> operator+( const T& k, const Vec<T, N>& B ) {
      Vec<T, N> C;
      Utils::unwind<0, N>( [&]( auto i ) { C.m[i] = k + B.m[i]; } );
      return C;
    }
    /// elementwise substraction
    [[gnu::pure]] friend Vec<T, N> operator-( const Vec<T, N>& A, const Vec<T, N>& B ) {
      Vec<T, N> C;
      Utils::unwind<0, N>( [&]( auto i ) { C.m[i] = A.m[i] - B.m[i]; } );
      return C;
    }
    [[gnu::pure]] friend Vec<T, N> operator-( const Vec<T, N>& A, const T& k ) {
      Vec<T, N> C;
      Utils::unwind<0, N>( [&]( auto i ) { C.m[i] = A.m[i] - k; } );
      return C;
    }
    [[gnu::pure]] friend Vec<T, N> operator-( const T& k, const Vec<T, N>& B ) {
      Vec<T, N> C;
      Utils::unwind<0, N>( [&]( auto i ) { C.m[i] = k - B.m[i]; } );
      return C;
    }
    /// elementwise multiplication
    [[gnu::pure]] friend Vec<T, N> operator*( const Vec<T, N>& A, const Vec<T, N>& B ) {
      Vec<T, N> C;
      Utils::unwind<0, N>( [&]( auto i ) { C.m[i] = A.m[i] * B.m[i]; } );
      return C;
    }
    [[gnu::pure]] friend Vec<T, N> operator*( const Vec<T, N>& A, const T& k ) {
      Vec<T, N> C;
      Utils::unwind<0, N>( [&]( auto i ) { C.m[i] = A.m[i] * k; } );
      return C;
    }
    [[gnu::pure]] friend Vec<T, N> operator*( const T& k, const Vec<T, N>& B ) {
      Vec<T, N> C;
      Utils::unwind<0, N>( [&]( auto i ) { C.m[i] = k * B.m[i]; } );
      return C;
    }
    /// elementwise division
    [[gnu::pure]] friend Vec<T, N> operator/( const Vec<T, N>& A, const Vec<T, N>& B ) {
      Vec<T, N> C;
      Utils::unwind<0, N>( [&]( auto i ) { C.m[i] = A.m[i] / B.m[i]; } );
      return C;
    }
    [[gnu::pure]] friend Vec<T, N> operator/( const Vec<T, N>& A, const T& k ) {
      Vec<T, N> C;
      Utils::unwind<0, N>( [&]( auto i ) { C.m[i] = A.m[i] / k; } );
      return C;
    }
    [[gnu::pure]] friend Vec<T, N> operator/( const T& k, const Vec<T, N>& B ) {
      Vec<T, N> C;
      Utils::unwind<0, N>( [&]( auto i ) { C.m[i] = k / B.m[i]; } );
      return C;
    }
    /// dot product
    [[gnu::pure]] T dot( const Vec<T, N>& B ) const {
      return Utils::unwind_sum<0, N>( [&]( auto i ) { return m[i] * B.m[i]; } );
    }
    [[gnu::pure]] T mag2() const { return this->dot( *this ); }
    [[gnu::pure]] T mag() const {
      using std::sqrt;
      return sqrt( this->mag2() );
    }
    [[gnu::pure]] T         invmag() const { rsqrt( this->mag2() ); }
    [[gnu::pure]] Vec<T, N> cross( const Vec<T, N>& b ) const {
      return {y() * b.z() - z() * b.y(), z() * b.x() - x() * b.z(), x() * b.y() - y() * b.x()};
    }
    [[gnu::pure]] T perp2() const { return x() * x() + y() * y(); }
    [[gnu::pure]] T rho() const { return sqrt( perp2() ); }
    [[gnu::pure]] T theta() const { return faster_atan2( rho(), z() ); }
    [[gnu::pure]] T phi() const { return faster_atan2( y(), x() ); }
    [[gnu::pure]] T eta() const {
      auto zs = z() / rho();
      return approx_log<T>( zs + sqrt( zs * zs + 1 ) );
    }
    [[gnu::pure]] Vec<T, N> normalize() const {
      const T invMag = T{1} / sqrt( mag2() );
      return invMag * ( *this );
    }

    // Compatibility with ROOT::Math::XYZVector and friends
    T         X() const { return x(); }
    T         Y() const { return y(); }
    T         Z() const { return z(); }
    T         Rho() const { return rho(); }
    Vec<T, N> Cross( Vec<T, N> const& b ) const { return cross( b ); }

    /// transpose
    [[gnu::pure]] Mat<T, 1, N> transpose() const {
      Mat<T, 1, N> M;
      Utils::unwind<0, N>( [&]( auto i ) { M( 0, i ) = m[i]; } );
      return M;
    }
    /// operator<< for printout of the vector
    friend std::ostream& operator<<( std::ostream& o, const Vec<T, N>& v ) {
      o << "[";
      Utils::unwind<0, N - 1>( [&]( auto _n ) { o << v( _n ) << ", "; } );
      o << v( N - 1 ) << ']';
      return o;
    }

    /// if the dimension of the vector is 1, you can cast it to the single value it holds
    [[gnu::pure]] T cast_to_value() {
      static_assert( N == 1, "Vec with dimension > 1 cannot be cast to a value" );
      return m[0];
    }

    /// get a smaller vector of type SubType from the original vector, starting at index Start
    template <typename SubType, int Start>
    SubType sub() const {
      static_assert( is_vec_v<SubType>, "SubType needs to be of type Vec" );
      SubType out{};
      Utils::unwind<0, SubType::size>( [&]( auto i ) { out( i ) = ( *this )( i + Start ); } );
      return out;
    }
  };

  template <typename T, int N>
  auto dot( Vec<T, N> const& lhs, Vec<T, N> const& rhs ) {
    return lhs.dot( rhs );
  }

  template <typename... Ts>
  Vec( Ts... )->Vec<std::common_type_t<Ts...>, sizeof...( Ts )>;

  template <typename Out, typename In, int N>
  Vec<Out, N> gather( std::array<Vec<In, N>, Out::size()> const& in ) {
    return std::apply(
        []( const auto&... i ) {
          return Vec<Out, N>{Out{std::array{i.x().cast()...}}, Out{std::array{i.y().cast()...}},
                             Out{std::array{i.z().cast()...}}};
        },
        in );
  }

  // Generic Matrix object
  /** @class Matrix
   *  0, 1, 2
   *  3, 4, 5
   *  6, 7, 8
   *  implementation of an N-dim Vector of type T
   */
  template <typename T, int N /* row */, int M /* column */ = N>
  struct Mat {
    static constexpr auto n_rows = N;
    static constexpr auto n_cols = M;
    using value_type             = T;

    std::array<T, N * M> m{};

    /// elementwise comparison ==
    [[gnu::pure]] friend auto operator==( Mat<T, N, M> const& lhs, Mat<T, N, M> const& rhs ) {
      return details::cmp_array( lhs.m, rhs.m, std::make_index_sequence<N * M>{} );
    }
    /// elementwise comparison !=
    [[gnu::pure]] friend auto operator!=( Mat<T, N, M> const& lhs, Mat<T, N, M> const& rhs ) {
      return not( lhs == rhs );
    }

    /// get element
    /// @param i row
    /// @param j col
    [[gnu::pure]] const T& operator()( int i, int j ) const {
      assert( i >= 0 && j >= 0 && i < N && j < M );
      return m[i * M + j];
    }
    /// get element reference (use for assignment)
    /// @param i row
    /// @param j col
    T& operator()( int i, int j ) {
      assert( i >= 0 && j >= 0 && i < N && j < M );
      return m[i * M + j];
    }

    /// placing sub matrix into the matrix (not in place)
    template <int N_init, int M_init, typename Mat_t>
    [[gnu::pure]] Mat<T, N, M> place_at( Mat_t const& to_place ) {
      static_assert( is_sym_mat_v<Mat_t> or is_mat_v<Mat_t> );
      static_assert( Mat_t::n_rows + N_init <= n_rows );
      static_assert( Mat_t::n_cols + M_init <= n_cols );
      auto C = *this;
      LHCb::Utils::unwind<0, Mat_t::n_rows>( [&]( auto i ) {
        LHCb::Utils::unwind<0, Mat_t::n_cols>( [&]( auto j ) { C( i + N_init, j + M_init ) = to_place( i, j ); } );
      } );
      return C;
    }

    /// elementwise addition
    [[gnu::pure]] friend Mat<T, N, M> operator+( const Mat<T, N, M>& A, const Mat<T, N, M>& B ) {
      Mat<T, N, M> C;
      Utils::unwind<0, N * M>( [&]( auto i ) { C.m[i] = A.m[i] + B.m[i]; } );
      return C;
    }

    /// elementwise substraction
    [[gnu::pure]] friend Mat<T, N, M> operator-( const Mat<T, N, M>& A, const Mat<T, N, M>& B ) {
      Mat<T, N, M> C;
      Utils::unwind<0, N * M>( [&]( auto i ) { C.m[i] = A.m[i] - B.m[i]; } );
      return C;
    }

    /// Mat * Vec
    [[gnu::pure]] friend Vec<T, N> operator*( const Mat<T, N, M>& m, const Vec<T, M>& v ) {
      Vec<T, N> X;
      Utils::unwind<0, N>(
          [&]( auto i ) { X( i ) = Utils::unwind_sum<0, M>( [&]( auto j ) -> T { return m( i, j ) * v( j ); } ); } );
      return X;
    }

    /// elementwise multiplication with a value
    [[gnu::pure]] friend Mat<T, N, M> operator*( const Mat<T, N, M>& A, const T& s ) {
      Mat<T, N, M> X;
      Utils::unwind<0, N * M>( [&]( auto i ) { X.m[i] = A.m[i] * s; } );
      return X;
    }

    /// elementwise division with a value
    [[gnu::pure]] friend Mat<T, N, M> operator/( const Mat<T, N, M>& A, const T& s ) {
      Mat<T, N, M> X;
      Utils::unwind<0, N * M>( [&]( auto i ) { X.m[i] = A.m[i] / s; } );
      return X;
    }

    /// operator<< for printing
    friend std::ostream& operator<<( std::ostream& o, const Mat<T, N, M>& A ) {
      o << "[";
      Utils::unwind<0, N - 1>( [&]( auto _n ) {
        Utils::unwind<0, M>( [&]( auto _m ) { o << A( _n, _m ) << ", "; } );
        o << '\n';
      } );
      Utils::unwind<0, M - 1>( [&]( auto _m ) { o << A( N - 1, _m ) << ", "; } );
      o << A( N - 1, M - 1 ) << ']';
      return o;
    }

    /// transposition of Matrix
    Mat<T, M, N> transpose() const {
      Mat<T, M, N> C;
      Utils::unwind<0, M>( [&]( auto i ) { Utils::unwind<0, N>( [&]( auto j ) { C( i, j ) = ( *this )( j, i ); } ); } );
      return C;
    }

    /// cast to symmetrical matrix, using the lower triangle
    [[gnu::pure]] MatSym<T, N> cast_to_sym() const {
      static_assert( N == M, "only square matrices can be cast to MatSym" );
      MatSym<T, N> out{};
      Utils::unwind<0, N>(
          [&]( auto i ) { Utils::unwind<0, i + 1>( [&]( auto j ) { out( i, j ) = ( *this )( i, j ); } ); } );
      return out;
    }

    /// get the submatrix of type SubType, starting at row start_row and col start_col
    template <typename SubType, int start_row, int start_col>
    SubType sub() const {
      static_assert( is_mat_v<SubType> or is_sym_mat_v<SubType>, "SubType needs to be of type Mat or MatSym" );
      SubType out{};
      if constexpr ( is_mat_v<SubType> ) {
        Utils::unwind<0, SubType::n_rows>( [&]( auto i ) {
          Utils::unwind<0, SubType::n_cols>(
              [&]( auto j ) { out( i, j ) = ( *this )( i + start_row, j + start_col ); } );
        } );
      } else {
        Utils::unwind<0, SubType::n_rows>( [&]( auto i ) {
          Utils::unwind<0, i + 1>( [&]( auto j ) { out( i, j ) = ( *this )( i + start_row, j + start_col ); } );
        } );
      }
      return out;
    }
  };

  template <typename First, typename... Nexts, std::enable_if_t<( std::is_same_v<First, Nexts> && ... ), int> = 0>
  Mat( First, Nexts... )->Mat<First, sizeof...( Nexts ) + 1>;

  /// Vec * Mat
  template <typename T, int N>
  [[gnu::pure]] Mat<T, N> operator*( const Vec<T, N>& v, const Mat<T, N, 1>& m ) {
    Mat<T, N> C;
    Utils::unwind<0, N>( [&]( auto i ) { Utils::unwind<0, N>( [&]( auto j ) { C( i, j ) = v( i ) * m( j, 0 ); } ); } );
    return C;
  }

  /// Matrix multiplication
  template <typename T, int N, int NM, int M>
  [[gnu::pure]] Mat<T, N, M> operator*( const Mat<T, N, NM>& A, const Mat<T, NM, M>& B ) {
    Mat<T, N, M> C;
    Utils::unwind<0, N>( [&]( auto i ) {
      Utils::unwind<0, M>( [&]( auto j ) {
        C( i, j ) = Utils::unwind_sum<0, NM>( [&]( auto k ) -> T { return A( i, k ) * B( k, j ); } );
      } );
    } );
    return C;
  }

  /// @class MatSym
  /// Generic Symetric Matrix object
  ///  0
  ///  1  2
  ///  3  4  5
  ///  6  7  8  9
  template <typename T, int N>
  struct MatSym {
    static constexpr auto n_rows = N;
    static constexpr auto n_cols = N;
    using value_type             = T;
    std::array<T, N*( N + 1 ) / 2> m{};

    /// given a value f, fill all elements m(i,j) with f
    /// given a callable f, fill all elements m(i,j) with the result of f(i,j)
    template <typename F>
    [[gnu::always_inline]] constexpr decltype( auto ) fill( F f ) {
      if constexpr ( std::is_assignable_v<T, F> ) {
        m.fill( f );
      } else {
        Utils::unwind<0, N>(
            [&]( auto i ) { Utils::unwind<0, i + 1>( [&]( auto j ) { m[i * ( i + 1 ) / 2 + j] = f( i, j ); } ); } );
      }
      return *this;
    }

    /// elementwise comparison ==
    [[gnu::pure]] friend auto operator==( MatSym<T, N> const& lhs, MatSym<T, N> const& rhs ) {
      return details::cmp_array( lhs.m, rhs.m, std::make_index_sequence<N*( N + 1 ) / 2>{} );
    }
    /// elementwise comparison !=
    [[gnu::pure]] friend auto operator!=( MatSym<T, N> const& lhs, MatSym<T, N> const& rhs ) {
      return not( lhs == rhs );
    }

    /// get element
    /// @param i row
    /// @param j col
    [[gnu::pure]] const T& operator()( int i, int j ) const {
      if ( i < j ) std::swap( i, j );
      assert( i >= j && i >= 0 && j >= 0 && i < N && j < N );
      return m[i * ( i + 1 ) / 2 + j];
    }
    /// get element reference (use for assignment)
    /// @param i row
    /// @param j col
    T& operator()( int i, int j ) {
      if ( i < j ) std::swap( i, j );
      assert( i >= j && i >= 0 && j >= 0 && i < N && j < N );
      return m[i * ( i + 1 ) / 2 + j];
    }

    /// placing sub matrix into the matrix (not in place)
    template <int N_init, int M_init, typename Mat_t>
    [[gnu::pure]] auto place_at( Mat_t const& to_place ) {
      static_assert( is_sym_mat_v<Mat_t> or is_mat_v<Mat_t> );
      static_assert( Mat_t::n_rows + N_init <= n_rows );
      static_assert( Mat_t::n_cols + M_init <= n_cols );
      auto C = *this;
      if constexpr ( N_init == M_init and is_sym_mat_v<Mat_t> ) {
        LHCb::Utils::unwind<0, Mat_t::n_rows>( [&]( auto i ) {
          LHCb::Utils::unwind<0, i + 1>( [&]( auto j ) { C( i + N_init, j + M_init ) = to_place( i, j ); } );
        } );
      } else {
        LHCb::Utils::unwind<0, Mat_t::n_rows>( [&]( auto i ) {
          LHCb::Utils::unwind<0, Mat_t::n_cols>( [&]( auto j ) { C( i + N_init, j + M_init ) = to_place( i, j ); } );
        } );
      }
      return C;
    }

    /// Cholesky factorization and substitution based on:
    /// https://hal.archives-ouvertes.fr/hal-01550129/document
    [[gnu::pure]] MatSym<T, N> invChol() const {
      const MatSym<T, N>& A = *this;
      MatSym<T, N>        L;
      auto                inv = initialize_with_zeros<Mat<T, N>>();

      // Cholesky factorization
      Utils::unwind<0, N>( [&]( auto j ) {
        T s = A( j, j );
        Utils::unwind<0, j>( [&]( auto k ) { s = s - L( j, k ) * L( j, k ); } );
        const T rsqrt_s = T{1} / sqrt( s );
        L( j, j )       = rsqrt_s; // store directly the reciprocal
        Utils::unwind<j + 1, N>(
            []( auto i, auto j, auto& L, const auto& A, auto rsqrt_s ) {
              T s = A( i, j );
              Utils::unwind<0, j>(
                  []( auto k, const auto& L, auto& s, auto i, auto j ) { s = s - L( i, k ) * L( j, k ); }, L, s, i, j );
              L( i, j ) = s * rsqrt_s;
            },
            j, L, A, rsqrt_s );
      } );

      // Forward substitution
      Utils::unwind<0, N>( [&]( auto k ) {
        inv( k, k ) = L( k, k );
        Utils::unwind<k + 1, N>( [&]( auto i ) {
          auto s      = Utils::unwind_sum<0, i>( [&]( auto j ) { return -L( i, j ) * inv( j, k ); } );
          inv( i, k ) = s * L( i, i ); // multiply by the reciprocal
        } );
      } );

      // Backward substitution
      Utils::unwind<0, N>( [&]( auto k ) {
        Utils::unwind<N - 1, k - 1, -1>( [&]( auto i ) {
          auto s = inv( i, k );
          Utils::unwind<i + 1, N>( [&]( auto j ) { s = s - L( j, i ) * inv( j, k ); } );
          inv( i, k ) = s * L( i, i ); // multiply by the reciprocal
        } );
      } );
      return inv.cast_to_sym();
    }

    /// elementwise addition
    [[gnu::pure]] friend MatSym<T, N> operator+( const MatSym<T, N>& A, const MatSym<T, N>& B ) {
      MatSym<T, N> C;
      Utils::unwind<0, N*( N + 1 ) / 2>( [&]( auto i ) { C.m[i] = A.m[i] + B.m[i]; } );
      return C;
    }
    /// elementwise substraction
    [[gnu::pure]] friend MatSym<T, N> operator-( const MatSym<T, N>& A, const MatSym<T, N>& B ) {
      MatSym<T, N> C;
      Utils::unwind<0, N*( N + 1 ) / 2>( [&]( auto i ) { C.m[i] = A.m[i] - B.m[i]; } );
      return C;
    }
    /// matrix multiplication
    [[gnu::pure]] friend Mat<T, N> operator*( const MatSym<T, N>& A, const MatSym<T, N>& B ) {
      Mat<T, N> C;
      Utils::unwind<0, N>( [&]( auto i ) {
        Utils::unwind<0, N>( [&]( auto j ) {
          C( i, j ) = Utils::unwind_sum<0, N>( [&]( auto k ) -> T { return A( i, k ) * B( k, j ); } );
        } );
      } );
      return C;
    }
    /// matrix multiplication MatSym * Mat
    template <int M>
    [[gnu::pure]] friend Mat<T, N, M> operator*( const MatSym<T, N>& A, const Mat<T, N, M>& B ) {
      return A.cast_to_mat() * B;
    }
    /// matrix multiplication Mat * MatSym
    template <int M>
    [[gnu::pure]] friend Mat<T, N, M> operator*( const Mat<T, N, M>& A, const MatSym<T, M>& B ) {
      return A * B.cast_to_mat();
    }
    /// matrix addition MatSym + Mat
    [[gnu::pure]] friend Mat<T, N> operator+( const MatSym<T, N>& A, const Mat<T, N>& B ) {
      return A.cast_to_mat() + B;
    }
    /// matrix addition Mat + MatSym
    [[gnu::pure]] friend Mat<T, N> operator+( const Mat<T, N>& A, const MatSym<T, N>& B ) {
      return A + B.cast_to_mat();
    }
    /// matrix subtraction MatSym - Mat
    [[gnu::pure]] friend Mat<T, N> operator-( const MatSym<T, N>& A, const Mat<T, N>& B ) {
      return A.cast_to_mat() - B;
    }
    /// matrix subtraction Mat - MatSym
    [[gnu::pure]] friend Mat<T, N> operator-( const Mat<T, N>& A, const MatSym<T, N>& B ) {
      return A - B.cast_to_mat();
    }
    /// Matrix * Vector
    [[gnu::pure]] friend Vec<T, N> operator*( const MatSym<T, N>& m, const Vec<T, N>& v ) {
      Vec<T, N> X;
      Utils::unwind<0, N>(
          [&]( auto i ) { X( i ) = Utils::unwind_sum<0, N>( [&]( auto j ) -> T { return m( i, j ) * v( j ); } ); } );
      return X;
    }

    /// elementwise multiplication with a value
    [[gnu::pure]] friend MatSym<T, N> operator*( const MatSym<T, N>& A, const T& s ) {
      MatSym<T, N> X;
      Utils::unwind<0, N*( N + 1 ) / 2>( [&]( auto i ) { X.m[i] = A.m[i] * s; } );
      return X;
    }

    /// operator<< for printing
    friend std::ostream& operator<<( std::ostream& o, const MatSym<T, N>& A ) {
      o << "[";
      Utils::unwind<0, N - 1>( [&]( auto _n ) {
        Utils::unwind<0, N>( [&]( auto _m ) { o << A( _n, _m ) << ", "; } );
        o << '\n';
      } );
      Utils::unwind<0, N - 1>( [&]( auto _m ) { o << A( N - 1, _m ) << ", "; } );
      o << A( N - 1, N - 1 ) << ']';
      return o;
    }

    /// conversion to non symmetrical matrix
    Mat<T, N> cast_to_mat() const {
      Mat<T, N> out{};
      Utils::unwind<0, N>(
          [&]( auto i ) { Utils::unwind<0, N>( [&]( auto j ) { out( i, j ) = ( *this )( i, j ); } ); } );
      return out;
    }

    /// get the submatrix of type SubType, starting at row start_row and col start_col
    template <typename SubType, int start_row, int start_col>
    SubType sub() const {
      static_assert( is_sym_mat_v<SubType> or is_mat_v<SubType>, "SubType needs to be of type Mat or MatSym" );
      SubType out{};
      if constexpr ( is_mat_v<SubType> ) {
        Utils::unwind<0, SubType::n_rows>( [&]( auto i ) {
          Utils::unwind<0, SubType::n_cols>(
              [&]( auto j ) { out( i, j ) = ( *this )( i + start_row, j + start_col ); } );
        } );
      } else if constexpr ( is_sym_mat_v<SubType> ) {
        Utils::unwind<0, SubType::n_rows>( [&]( auto i ) {
          Utils::unwind<0, i + 1>( [&]( auto j ) { out( i, j ) = ( *this )( i + start_row, j + start_col ); } );
        } );
      }
      return out;
    }
  };

  template <typename First, typename... Nexts, std::enable_if_t<( std::is_same_v<First, Nexts> && ... ), int> = 0>
  MatSym( First, Nexts... )->MatSym<First, sizeof...( Nexts ) + 1>;

  /// Similarity of MatSym S with Mat M, defined as (M * S * transpose(M)) -> MatSym
  template <typename T, int U, int V>
  [[gnu::pure]] MatSym<T, V> similarity( const Mat<T, V, U>& M, const MatSym<T, U>& S ) {
    return ( M * S.cast_to_mat() * M.transpose() ).cast_to_sym();
  }

  /// Similarity of MatSym S with Vec V, defined as (transpose(V) * M * V) -> scalar
  template <typename T, int N>
  [[gnu::pure]] T similarity( const Vec<T, N>& V, const MatSym<T, N>& S ) {
    return ( V.transpose() * S.cast_to_mat() * V ).cast_to_value();
  }

  /// Similarity of Mat M with Vec V, defined as (transpose(V) * M * V) -> scalar
  template <typename T, int N>
  [[gnu::pure]] T similarity( const Vec<T, N>& V, const Mat<T, N>& M ) {
    return ( V.transpose() * M * V ).cast_to_value();
  }

  template <typename Out, typename In, int N>
  MatSym<Out, N> gather( std::array<MatSym<In, N>, Out::size()> const& in ) {
    using Utils::unwind;
    LinAlg::MatSym<Out, N> out;
    std::apply(
        [&]( const auto&... i ) {
          unwind<0, N>( [&]( auto j ) {
            unwind<0, j + 1>( [&]( auto k ) { out( j, k ) = Out{std::array{i( j, k ).cast()...}}; } );
          } );
        },
        in );
    return out;
  }

  template <typename Out, auto N, typename In>
  auto convert( const ROOT::Math::SMatrix<In, N, N, ROOT::Math::MatRepSym<In, N>>& in ) {
    using Utils::unwind;
    LinAlg::MatSym<Out, N> out;
    unwind<0, N>( [&]( auto i ) { unwind<0, i + 1>( [&]( auto j ) { out( i, j ) = in( i, j ); } ); } );
    return out;
  }

  template <auto N, typename In>
  auto convert( const ROOT::Math::SMatrix<In, N, N, ROOT::Math::MatRepSym<In, N>>& in ) {
    return convert<In>( in );
  }

  template <typename Out, auto N, auto M, typename In>
  auto convert( const ROOT::Math::SMatrix<In, N, M>& in ) {
    using Utils::unwind;
    LinAlg::Mat<Out, N, M> out;
    unwind<0, N>( [&]( auto i ) { unwind<0, M>( [&]( auto j ) { out( i, j ) = in( i, j ); } ); } );
    return out;
  }

  template <auto N, auto M, typename In>
  auto convert( const ROOT::Math::SMatrix<In, N, M>& in ) {
    return convert<In>( in );
  }

  template <typename Float>
  constexpr auto convert( const Vec3<Float>& v ) {
    return Vec{v.X(), v.Y(), v.Z()};
  }

  template <typename In>
  auto convert( LHCb::LinAlg::Vec<In, 4> const& in ) {
    return Gaudi::LorentzVector( in( 0 ).cast(), in( 1 ).cast(), in( 2 ).cast(), in( 3 ).cast() );
  }

  template <typename In>
  auto convert( LHCb::LinAlg::MatSym<In, 4> const& in ) {
    using LHCb::Utils::unwind;
    Gaudi::SymMatrix4x4 out;
    unwind<0, 4>( [&]( auto i ) { unwind<0, i + 1>( [&]( auto j ) { out( i, j ) = in( i, j ).cast(); } ); } );
    return out;
  }

  template <typename In>
  auto convert( LHCb::LinAlg::Mat<In, 4, 3> const& in ) {
    using LHCb::Utils::unwind;
    Gaudi::Matrix4x3 out;
    unwind<0, 4>( [&]( auto i ) { unwind<0, 3>( [&]( auto j ) { out( i, j ) = in( i, j ).cast(); } ); } );
    return out;
  }

  template <typename Float>
  constexpr auto X( Vec<Float, 3> const& v ) {
    return v( 0 );
  }
  template <typename Float>
  constexpr auto Y( Vec<Float, 3> const& v ) {
    return v( 1 );
  }
  template <typename Float>
  constexpr auto Z( Vec<Float, 3> const& v ) {
    return v( 2 );
  }
  template <typename Float>
  constexpr auto X( Vec<Float, 4> const& v ) {
    return v( 0 );
  }
  template <typename Float>
  constexpr auto Y( Vec<Float, 4> const& v ) {
    return v( 1 );
  }
  template <typename Float>
  constexpr auto Z( Vec<Float, 4> const& v ) {
    return v( 2 );
  }
  template <typename Float>
  constexpr auto T( Vec<Float, 4> const& v ) {
    return v( 3 );
  }
  template <typename Float>
  constexpr auto E( Vec<Float, 4> const& v ) {
    return v( 3 );
  }
  template <typename Float>
  constexpr auto mass2( Vec<Float, 4> const& v ) {
    // TODO: switch to the stable version mentioned in https://mattermost.web.cern.ch/lhcb/pl/yac6rcp1z3g3b8e4rwowqn384y
    return E( v ) * E( v ) - ( X( v ) * X( v ) + Y( v ) * Y( v ) + Z( v ) * Z( v ) );
  }

} // namespace LHCb::LinAlg
