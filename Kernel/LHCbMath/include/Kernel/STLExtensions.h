/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// This header contains methods that are likely to become parts of the STL
// at some point in the future, but currently are not available.

#include <array>
#include <cassert>
#include <cstddef>
#include <functional>
#include <iterator>
#include <memory>
#include <optional>
#include <tuple>
#include <type_traits>
#include <utility>

#if __cplusplus > 201703L && __has_include( <source_location> )
#  include <source_location>
namespace LHCb::cxx {
  using std::source_location;
}
#elif __cplusplus >= 201402L
#  include <experimental/source_location>
namespace LHCb::cxx {
  using std::experimental::source_location;
}
#endif

// add a macro state assumptions
#if defined( ASSUME )
#  undef ASSUME
#endif
#ifdef NDEBUG
#  define ASSUME( COND ) static_cast<void>( ( COND ) ? void( 0 ) : __builtin_unreachable() )
#else
#  define ASSUME( COND ) assert( COND )
#endif

#include <cstddef>
// cstddef must be defined _before_ gsl/span to make gsl::span use std::byte
// instead of gsl::byte...

#ifdef NDEBUG
#  pragma GCC diagnostic push
#  pragma GCC diagnostic ignored "-Wunused-parameter"
#  define GSL_UNENFORCED_ON_CONTRACT_VIOLATION
#endif
// drop bounds checking in non-debug mode in a way which (also) works with MS-GSL3
#include "gsl/gsl_assert"
#ifdef NDEBUG
#  undef GSL_CONTRACT_CHECK
#  define GSL_CONTRACT_CHECK( type, cond ) GSL_ASSUME( cond )
#endif
#include "gsl/span"
#include "gsl/span_ext"
#ifdef NDEBUG
#  pragma GCC diagnostic pop
#endif

namespace LHCb {

  namespace cxx {

    // FIXME: C++20: replace with std::bind_front
    template <typename F, typename... BoundArgs>
    auto bind_front( F&& f, BoundArgs&&... boundArgs ) {
      return [f         = std::forward<F>( f ),
              boundArgs = std::tuple{std::forward<BoundArgs>( boundArgs )...}]( auto&&... args ) -> decltype( auto ) {
        return std::apply(
            f, std::tuple_cat( boundArgs, std::forward_as_tuple( std::forward<decltype( args )>( args )... ) ) );
      };
    }

  } // namespace cxx

  // FIXME/TODO: C++20: replace with std::span
  using gsl::make_span;
  using gsl::span;

  namespace details_se {
    template <typename, typename = void>
    struct is_span_iterator : std::false_type {};

    template <typename Iterator>
    struct is_span_iterator<
        Iterator, std::enable_if_t<std::is_same_v<
                      typename gsl::span<std::remove_pointer_t<typename Iterator::pointer>>::iterator, Iterator>>>
        : std::true_type {};

    template <typename Iterator>
    constexpr bool is_span_iterator_v = is_span_iterator<Iterator>::value;

    // see https://en.cppreference.com/w/cpp/named_req/ContiguousIterator
    // 1) array::iterator is typically a pointer, so no need to support it here
    // 2) string_view::iterator: why would ever want to turn that into a span?
    // 3) valarray: if you really need it, then feel free to add it
    //
    // or it is a pair of iterators into a span...
    template <typename Iterator>
    constexpr bool isContiguous() {
      using Value = typename std::iterator_traits<Iterator>::value_type;
      return ((std::is_same_v<typename std::vector<Value>::iterator, Iterator> ||
               std::is_same_v<typename std::vector<Value>::const_iterator, Iterator>)&&!std::is_same_v<bool, Value>) ||
             is_span_iterator_v<Iterator>;
    }
  } // namespace details_se

  template <typename Iterator, typename = std::enable_if_t<details_se::isContiguous<Iterator>()>>
  auto make_span( Iterator firstElem, Iterator lastElem ) {
    // avoid forming a reference to *firstElem, as [firstElem,lastElem) may be an empty range,
    // in which case *firstElem would be an invalid expression, and eg. std::addressof(*firstElem)
    // would imply UB.
    // C++20 FIXME: use std::to_address(firstElem)
    return make_span( firstElem.operator->(), std::distance( firstElem, lastElem ) );
  }

  namespace range {

    namespace details {
      template <typename A, typename B>
      using disable_if_same_or_derived = std::enable_if_t<!std::is_base_of_v<A, std::remove_reference_t<B>>>;
    }

    template <typename T>
    class single final {
      T m_data{};

    public:
      using value_type      = T;
      using pointer         = T*;
      using const_pointer   = std::add_const_t<T>*;
      using reference       = T&;
      using const_reference = std::add_const_t<T>&;

      template <typename U = T, typename = details::disable_if_same_or_derived<single, U>,
                typename = std::enable_if_t<std::is_constructible_v<T, U&&>>>
      constexpr single( U&& u ) : m_data( std::forward<U>( u ) ) {}

      template <typename... Args, typename = std::enable_if_t<std::is_constructible_v<T, Args&&...>>>
      constexpr explicit single( std::in_place_t, Args&&... args ) : m_data( std::forward<Args>( args )... ) {}

      constexpr pointer begin() noexcept { return std::addressof( m_data ); }
      constexpr pointer end() noexcept { return std::addressof( m_data ) + 1; }
      constexpr pointer data() noexcept { return begin(); }

      constexpr const_pointer begin() const noexcept { return std::addressof( m_data ); }
      constexpr const_pointer end() const noexcept { return std::addressof( m_data ) + 1; }
      constexpr const_pointer data() const noexcept { return begin(); }

      constexpr const_pointer cbegin() const noexcept { return begin(); }
      constexpr const_pointer cend() const noexcept { return end(); }

      [[nodiscard]] constexpr std::size_t size() const noexcept { return 1; }
      [[nodiscard]] constexpr bool        empty() const noexcept { return false; }

      constexpr const_reference value() const& noexcept { return m_data; }
      constexpr reference       value() & noexcept { return m_data; }
      constexpr T&&             value() && noexcept { return std::move( m_data ); }

      constexpr const_reference front() const noexcept { return m_data; }
      constexpr const_reference back() const noexcept { return m_data; }
      constexpr reference       front() noexcept { return m_data; }
      constexpr reference       back() noexcept { return m_data; }
    };

    template <typename T>
    single( T )->single<T>;

    template <typename ValueType>
    class chunk {
    public:
      using size_type = typename span<ValueType>::size_type;

    private:
      const span<ValueType> m_data;
      const size_type       m_chunkSize;
      struct Sentinel {};
      class Iterator {
        span<ValueType> m_remainder;
        const size_type m_chunkSize;

        friend class chunk;
        constexpr Iterator( span<ValueType> data, size_type chunkSize )
            : m_remainder{std::move( data )}, m_chunkSize{chunkSize} {
          assert( m_chunkSize > 0 );
        }

      public:
        constexpr span<ValueType> operator*() const {
          return m_remainder.first( std::min( m_remainder.size(), m_chunkSize ) );
        }
        constexpr bool      operator!=( Sentinel ) const { return !m_remainder.empty(); }
        constexpr Iterator& operator++() {
          m_remainder = m_remainder.subspan( std::min( m_remainder.size(), m_chunkSize ) );
          return *this;
        }
      };

    public:
      constexpr chunk( span<ValueType> in, size_type chunkSize ) : m_data{std::move( in )}, m_chunkSize{chunkSize} {}
      constexpr Iterator begin() const { return {m_data, m_chunkSize}; };
      constexpr Sentinel end() const { return {}; }
      constexpr size_t   size() const { return ( m_data.size() + m_chunkSize - 1 ) / m_chunkSize; }
    };

    template <typename Container, typename Int>
    chunk( const Container&, Int )->chunk<std::add_const_t<typename Container::value_type>>;
    template <typename Container, typename Int>
    chunk( Container&, Int )->chunk<typename Container::value_type>;

    template <typename Iterable, typename Count = size_t>
    constexpr auto enumerate( Iterable&& iterable, Count init = {} ) {
      struct Range {
        Iterable iterable;
        Count    initial;
        auto     end() const {
          using std::end;
          return end( iterable );
        }
        auto begin() const {
          using std::begin;
          struct Iterator {
            decltype( begin( std::declval<Iterable>() ) ) iterator;
            Count                                         count;
            bool operator!=( decltype( std::declval<Range>().end() ) sentinel ) const { return iterator != sentinel; }
            Iterator& operator++() {
              ++count;
              ++iterator;
              return *this;
            }
            auto operator*() const {
              using ValueType = decltype( *iterator );
              if constexpr ( std::is_lvalue_reference_v<ValueType> ) {
                return std::tie( count, *iterator );
              } else if constexpr ( std::is_rvalue_reference_v<ValueType> ) {
                return std::forward_as_tuple( count, *iterator );
              } else {
                return std::tuple( count, *iterator );
              }
            }
          };
          return Iterator{begin( iterable ), initial};
        }
      };
      return Range{std::forward<Iterable>( iterable ), init};
    }

    template <auto initial_count, typename Iterable>
    constexpr auto enumerate( Iterable&& iterable ) {
      return enumerate( std::forward<Iterable>( iterable ), initial_count );
    }

    template <typename Iterable>
    constexpr auto drop( Iterable&& iterable, size_t n ) {
      struct Range {
        Iterable iterable;
        size_t   n;
        auto     end() const {
          using std::end;
          return end( iterable );
        }
        auto begin() const {
          using std::begin;
          using std::size;
          return n < size( iterable ) ? std::next( begin( iterable ), n ) : end();
        }
      };
      return Range{std::forward<Iterable>( iterable ), n};
    }

    template <typename Iterable, typename difference_t>
    constexpr auto drop_exactly( Iterable&& iterable, difference_t n ) {
      struct Range {
        Iterable     iterable;
        difference_t n;
        auto         end() const {
          using std::end;
          return end( iterable );
        }
        auto begin() const {
          using std::begin;
          return std::next( begin( iterable ), n );
        }
      };
      return Range{std::forward<Iterable>( iterable ), n};
    }

  } // namespace range

  namespace details_se {
    /** Helper for expanding a pack of values and discarding them.
     */
    template <auto, typename T>
    using second_arg_t = T;

    template <typename T, std::size_t... Is, typename... Args>
    std::array<T, 1 + sizeof...( Is )> make_object_array( std::index_sequence<0, Is...>, Args&&... args ) {
      return {second_arg_t<Is, T>{static_cast<std::remove_reference_t<Args>&>( args )...}...,
              T{std::forward<Args>( args )...}};
    }
  } // namespace details_se

  /** Construct std::array<T, N>, explicitly forwarding the given arguments to the
   *  constructor of each T. For the first N-1 elements the arguments are forwarded
   *  as possibly-const lvalue references, and for the Nth element they are perfectly
   *  fowarded.
   */
  template <typename T, std::size_t N, typename... Args>
  std::array<T, N> make_object_array( Args&&... args ) {
    return details_se::make_object_array<T>( std::make_index_sequence<N>{}, std::forward<Args>( args )... );
  }

  /** @brief Get the index/position in a tuple of a specified type
   *
   *  index_of_v<B, std::tuple<A, B, C>> yields 1 etc.
   *
   *  Implementation based on
   *  https://stackoverflow.com/questions/18063451/get-index-of-a-tuple-elements-type
   */
  template <typename T, typename Tuple>
  struct index_of;

  template <typename... Ts>
  inline constexpr std::size_t index_of_v = index_of<Ts...>::value;

  template <typename T, typename... Ts>
  struct index_of<T, std::tuple<T, Ts...>> {
    static constexpr std::size_t value{0};
  };

  template <typename T, typename U, typename... Ts>
  struct index_of<T, std::tuple<U, Ts...>> {
    static constexpr std::size_t value{1 + index_of_v<T, std::tuple<Ts...>>};
  };

  template <typename T>
  auto get_pointer( std::optional<T>&& ) = delete; // make sure that calling get_pointer will not return a dangling
                                                   // pointer

  template <typename T>
  T* get_pointer( std::optional<T>& opt ) {
    return opt ? opt.operator->() : nullptr;
  }

  template <typename T>
  const T* get_pointer( std::optional<T> const& opt ) {
    return opt ? opt.operator->() : nullptr;
  }

} // namespace LHCb
