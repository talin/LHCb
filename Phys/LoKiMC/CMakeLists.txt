###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/LoKiMC
-----------
#]=======================================================================]

gaudi_add_library(LoKiMCLib
    SOURCES
        src/BuildMCTrees.cpp
        src/IMCDecay.cpp
        src/IMCHybridFactory.cpp
        src/IMCHybridTool.cpp
        src/LoKiMC.cpp
        src/MCAlgsDicts.cpp
        src/MCChild.cpp
        src/MCChildSelector.cpp
        src/MCDecayChain.cpp
        src/MCDecayVertex.cpp
        src/MCDecays.cpp
        src/MCDump.cpp
        src/MCExtractDicts.cpp
        src/MCFinder.cpp
        src/MCFinderDicts.cpp
        src/MCFinderObj.cpp
        src/MCHybridEngine.cpp
        src/MCHybridEngineActor.cpp
        src/MCHybridLock.cpp
        src/MCKinematics.cpp
        src/MCMoniDicts.cpp
        src/MCParticles.cpp
        src/MCParticles1.cpp
        src/MCParticles2.cpp
        src/MCParticles3.cpp
        src/MCSections.cpp
        src/MCSources.cpp
        src/MCStreamers.cpp
        src/MCTreeFactory.cpp
        src/MCTrees.cpp
        src/MCVertices.cpp
        src/PIDOperators.cpp
        src/PrintMCDecay.cpp
    LINK
        PUBLIC
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::LHCbKernel
            LHCb::LoKiCoreLib
            LHCb::MCEvent
            LHCb::MCInterfaces
            LHCb::PartPropLib
        PRIVATE
            Boost::headers
)

gaudi_add_module(LoKiMC
    SOURCES
        src/Components/DumpMC.cpp
        src/Components/HybridMCParticleSelector.cpp
        src/Components/MCDecay.cpp
        src/Components/MCFilter.cpp
        src/Components/MCHybridTool.cpp
    LINK
        Gaudi::GaudiKernel
        LHCb::LHCbAlgsLib
        LHCb::LoKiCoreLib
        LHCb::LoKiMCLib
        LHCb::MCEvent
        LHCb::MCInterfaces
)

gaudi_add_dictionary(LoKiMCDict
    HEADERFILES dict/LoKiMCDict.h
    SELECTION dict/LoKiMC.xml
    LINK LHCb::LoKiMCLib
    OPTIONS ${LHCB_DICT_GEN_DEFAULT_OPTS}
)

if(BUILD_TESTING)
    gaudi_add_executable(MCDecayGrammarTest
        SOURCES
            src/tests/MCDecayGrammarTest.cpp
        LINK
            Gaudi::GaudiKernel
            LHCb::LoKiCoreLib
            LHCb::LoKiMCLib
            LHCb::PartPropLib
    )
endif()

gaudi_install(PYTHON)

gaudi_add_tests(QMTest)
