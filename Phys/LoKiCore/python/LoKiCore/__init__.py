###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import ROOT

# Prevent ROOT printing "errors" on stderr such as:
#     Error in <TGClient::TGClient>: can't open display "", switching to batch mode...
#      In case you run from a remote ssh session, reconnect with ssh -Y
ROOT.gROOT.SetBatch(True)
