/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/ParsersFactory.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RawBank
//
// 2008-02-13 : Marco Cattaneo
//-----------------------------------------------------------------------------

namespace LHCb {
  std::string toString( RawBank::BankType e ) {
    switch ( e ) {
      //
      // Important note:
      // Keep the order exactly as in the RawEvent header file!
      //
    case RawBank::L0Calo:
      return "L0Calo";
    case RawBank::L0DU:
      return "L0DU";
    case RawBank::PrsE:
      return "PrsE";
    case RawBank::EcalE:
      return "EcalE";
    case RawBank::HcalE:
      return "HcalE";
    case RawBank::PrsTrig:
      return "PrsTrig";
    case RawBank::EcalTrig:
      return "EcalTrig";
    case RawBank::HcalTrig:
      return "HcalTrig";
    case RawBank::Velo:
      return "Velo";
    case RawBank::Rich:
      return "Rich";
    case RawBank::TT:
      return "TT";
    case RawBank::IT:
      return "IT";
    case RawBank::OT:
      return "OT";
    case RawBank::Muon:
      return "Muon";
    case RawBank::L0PU:
      return "L0PU";
    case RawBank::DAQ:
      return "DAQ";
    case RawBank::ODIN:
      return "ODIN";
    case RawBank::HltDecReports:
      return "HltDecReports";
    case RawBank::VeloFull:
      return "VeloFull";
    case RawBank::TTFull:
      return "TTFull";
    case RawBank::ITFull:
      return "ITFull";
    case RawBank::EcalPacked:
      return "EcalPacked";
    case RawBank::HcalPacked:
      return "HcalPacked";
    case RawBank::PrsPacked:
      return "PrsPacked";
    case RawBank::L0Muon:
      return "L0Muon";
    case RawBank::ITError:
      return "ITError";
    case RawBank::TTError:
      return "TTError";
    case RawBank::ITPedestal:
      return "ITPedestal";
    case RawBank::TTPedestal:
      return "TTPedestal";
    case RawBank::VeloError:
      return "VeloError";
    case RawBank::VeloPedestal:
      return "VeloPedestal";
    case RawBank::VeloProcFull:
      return "VeloProcFull";
    case RawBank::OTRaw:
      return "OTRaw";
    case RawBank::OTError:
      return "OTError";
    case RawBank::EcalPackedError:
      return "EcalPackedError";
    case RawBank::HcalPackedError:
      return "HcalPackedError";
    case RawBank::PrsPackedError:
      return "PrsPackedError";
    case RawBank::L0CaloFull:
      return "L0CaloFull";
    case RawBank::L0CaloError:
      return "L0CaloError";
    case RawBank::L0MuonCtrlAll:
      return "L0MuonCtrlAll";
    case RawBank::L0MuonProcCand:
      return "L0MuonProcCand";
    case RawBank::L0MuonProcData:
      return "L0MuonProcData";
    case RawBank::L0MuonRaw:
      return "L0MuonRaw";
    case RawBank::L0MuonError:
      return "L0MuonError";
    case RawBank::GaudiSerialize:
      return "GaudiSerialize";
    case RawBank::GaudiHeader:
      return "GaudiHeader";
    case RawBank::TTProcFull:
      return "TTProcFull";
    case RawBank::ITProcFull:
      return "ITProcFull";
    case RawBank::TAEHeader:
      return "TAEHeader";
    case RawBank::MuonFull:
      return "MuonFull";
    case RawBank::MuonError:
      return "MuonError";
    case RawBank::TestDet:
      return "TestDet";
    case RawBank::L0DUError:
      return "L0DUError";
    case RawBank::HltRoutingBits:
      return "HltRoutingBits";
    case RawBank::HltSelReports:
      return "HltSelReports";
    case RawBank::HltVertexReports:
      return "HltVertexReports";
    case RawBank::HltLumiSummary:
      return "HltLumiSummary";
    case RawBank::L0PUFull:
      return "L0PUFull";
    case RawBank::L0PUError:
      return "L0PUError";
    case RawBank::DstBank:
      return "DstBank";
    case RawBank::DstData:
      return "DstData";
    case RawBank::DstAddress:
      return "DstAddress";
    case RawBank::FileID:
      return "FileID";
    case RawBank::VP:
      return "VP";
    case RawBank::FTCluster:
      return "FTCluster";
    case RawBank::VL:
      return "VL";
    case RawBank::UT:
      return "UT";
    case RawBank::UTFull:
      return "UTFull";
    case RawBank::UTError:
      return "UTError";
    case RawBank::UTPedestal:
      return "UTPedestal";
    case RawBank::HC:
      return "HC";
    case RawBank::HltTrackReports:
      return "HltTrackReports";
    case RawBank::HCError:
      return "HCError";
    case RawBank::VPRetinaCluster:
      return "VPRetinaCluster";
    case RawBank::FTGeneric:
      return "FTGeneric";
    case RawBank::FTCalibration:
      return "FTCalibration";
    case RawBank::FTNZS:
      return "FTNZS";
    case RawBank::DaqErrorFragmentThrottled:
      return "DaqErrorFragmentThrottled";
    case RawBank::DaqErrorBXIDCorrupted:
      return "DaqErrorBXIDCorrupted";
    case RawBank::DaqErrorSyncBXIDCorrupted:
      return "DaqErrorSyncBXIDCorrupted";
    case RawBank::DaqErrorFragmentMissing:
      return "DaqErrorFragmentMissing";
    case RawBank::DaqErrorFragmentTruncated:
      return "DaqErrorFragmentTruncated";
    case RawBank::Calo:
      return "Calo";
    case RawBank::CaloError:
      return "CaloError";
    case RawBank::MuonSpecial:
      return "MuonSpecial";
    case RawBank::RichCommissioning:
      return "RichCommissioning";
    case RawBank::RichError:
      return "RichError";
    case RawBank::FTSpecial:
      return "FTSpecial";
    case RawBank::CaloSpecial:
      return "CaloSpecial";
    case RawBank::Plume:
      return "Plume";
    case RawBank::PlumeSpecial:
      return "PlumeSpecial";
    case RawBank::PlumeError:
      return "PlumeError";
    case RawBank::VeloThresholdScan:
      return "VeloThresholdScan";
    case RawBank::FTError:
      return "FTError";
    case RawBank::DaqErrorIdleBXIDCorrupted:
      return "DaqErrorIdleBXIDCorrupted";
    case RawBank::DaqErrorFragmentMalformed:
      return "DaqErrorFragmentMalformed";
    case RawBank::DaqErrorEVIDJumped:
      return "DaqErrorEVIDJumped";
    case RawBank::VeloSPPandCluster:
      return "VeloSPPandCluster";
    case RawBank::UTNZS:
      return "UTNZS";
    case RawBank::UTSpecial:
      return "UTSpecial";
    default:
      return "Undefined_name";
    };
  }
  StatusCode parse( RawBank::BankType& bt, const std::string& in ) {
    auto s = std::string_view{in};
    if ( !s.empty() && s.front() == s.back() && ( s.front() == '\'' || s.front() == '\"' ) ) {
      s.remove_prefix( 1 );
      s.remove_suffix( 1 );
    }
    constexpr auto tps = RawBank::types();
    auto           i   = std::find_if( tps.begin(), tps.end(), [s]( auto tp ) { return s == toString( tp ); } );
    if ( i == tps.end() ) return StatusCode::FAILURE;
    bt = *i;
    return StatusCode::SUCCESS;
  }
} // namespace LHCb

namespace {
  template <typename Range, typename Inserter>
  StatusCode convert( const Range& from, Inserter to ) {
    try {
      std::transform( begin( from ), end( from ), to, []( const std::string& str ) {
        LHCb::RawBank::BankType t{};
        parse( t, str ).orThrow( "Bad Parse", "" );
        return t;
      } );
      return StatusCode::SUCCESS;
    } catch ( const GaudiException& e ) { return e.code(); }
  }
} // namespace

namespace Gaudi::Parsers {
  StatusCode parse( std::vector<LHCb::RawBank::BankType>& v, const std::string& in ) {
    v.clear();
    using Gaudi::Parsers::parse;
    std::vector<std::string> vs;
    return parse( vs, in ).andThen( [&]() {
      v.reserve( vs.size() );
      return convert( vs, std::back_inserter( v ) );
    } );
  }
  StatusCode parse( std::set<LHCb::RawBank::BankType>& s, const std::string& in ) {
    s.clear();
    using Gaudi::Parsers::parse;
    std::set<std::string> ss;
    return parse( ss, in ).andThen( [&] { return convert( ss, std::inserter( s, begin( s ) ) ); } );
  }

} // namespace Gaudi::Parsers
