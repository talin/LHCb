/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE test_ODIN

#include <Event/ODIN.h>

#include <boost/test/unit_test.hpp>

std::array<std::uint32_t, 10> random_bits = {0xb353bf00, 0xff2631ec, 0xee0d27e6, 0x4178e913, 0x6d4e0fca,
                                             0x6027c5c2, 0x2cdcab4e, 0x937df4d1, 0xf0e968d5, 0x9c6420bc};

BOOST_AUTO_TEST_CASE( test_initialization ) {
  LHCb::ODIN odin{random_bits};
  BOOST_CHECK( odin.runNumber() == random_bits[0] );
}

BOOST_AUTO_TEST_CASE( lossless_codec ) {
  LHCb::ODIN odin{random_bits};

  std::array<std::uint32_t, 10> output_buffer{0};
  odin.to_version<LHCb::ODIN::BANK_VERSION>( output_buffer );

  BOOST_CHECK( output_buffer == random_bits );

  auto new_odin = LHCb::ODIN::from_version<LHCb::ODIN::BANK_VERSION>( output_buffer );

  BOOST_CHECK( odin.data == new_odin.data );
}

BOOST_AUTO_TEST_CASE( version_7_stability ) {
  // ODIN -> raw 7 -> ODIN -> raw 7
  LHCb::ODIN odin{random_bits};

  std::array<std::uint32_t, 10> raw7_1{0};
  odin.to_version<7>( raw7_1 );

  std::array<std::uint32_t, 10> raw7_2{0};
  LHCb::ODIN::from_version<7>( raw7_1 ).to_version<7>( raw7_2 );

  BOOST_CHECK( raw7_1 == raw7_2 );
}

BOOST_AUTO_TEST_CASE( version_6_stability ) {
  // ODIN -> raw 6 -> ODIN -> raw 6
  LHCb::ODIN odin{random_bits};
  // We can convert to version 6 only if trigger and calibration types fit
  odin.setTriggerType( odin.triggerType() & 0x7 );         // make sure the trigger type fits 3 bits
  odin.setCalibrationType( odin.calibrationType() & 0x3 ); // make sure the calibration type fits 2 bits

  std::array<std::uint32_t, 10> raw6_1{0};
  odin.to_version<6>( raw6_1 );

  std::array<std::uint32_t, 10> raw6_2{0};
  LHCb::ODIN::from_version<6>( raw6_1 ).to_version<6>( raw6_2 );

  BOOST_CHECK( raw6_1 == raw6_2 );
}

BOOST_AUTO_TEST_CASE( zero_unused_bits_in_v6 ) {
  // zeroing unused bits in conversion
  LHCb::ODIN odin{random_bits};
  // We can convert to version 6 only if trigger and calibration types fit
  odin.setTriggerType( odin.triggerType() & 0x7 );         // make sure the trigger type fits 3 bits
  odin.setCalibrationType( odin.calibrationType() & 0x3 ); // make sure the calibration type fits 2 bits

  std::array<std::uint32_t, 10> raw6_1{0};
  odin.to_version<6>( raw6_1 );

  std::array<std::uint32_t, 10> raw6_2{0xff};
  odin.to_version<6>( raw6_2 );

  BOOST_CHECK( raw6_1 == raw6_2 );
}
