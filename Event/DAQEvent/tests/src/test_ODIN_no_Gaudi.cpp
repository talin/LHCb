/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE test_ODIN

#define ODIN_WITHOUT_GAUDI 1
#include <Event/ODIN.h>

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE( basic_tests ) {
  LHCb::ODIN odin{};
  BOOST_CHECK( odin.runNumber() == 0 );
}

constexpr auto constexpr_example( LHCb::ODIN::EventTypes event_type ) {
  LHCb::ODIN odin{};
  odin.setEventType( event_type );
  return odin.eventType();
}

BOOST_AUTO_TEST_CASE( check_constexpr ) {
  bool passed = false;
  if constexpr ( constexpr_example( LHCb::ODIN::EventTypes::NoBias ) == 4 ) { passed = true; }
  BOOST_CHECK( passed );
}
