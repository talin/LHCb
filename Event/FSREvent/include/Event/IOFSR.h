/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/SerializeSTL.h"
#include "GaudiKernel/VectorMap.h"
#include <algorithm>
#include <map>
#include <ostream>
#include <string>
#include <vector>

namespace LHCb {

  // Class ID definition
  static const CLID CLID_IOFSR = 13506;

  // Namespace for locations in TDS
  namespace IOFSRLocation {
    inline const std::string Default = "/FileRecords/IOFSR";
  }

  /** @class IOFSR IOFSR.h
   *
   * Class to store IO information about a job, upgraded EventAccountFSR, needs a
   * special FSRWriter to add file-by-file
   *
   * @author Rob lambert
   *
   */

  class IOFSR final : public DataObject {
  public:
    /// typedef for std::vector of IOFSR
    typedef std::vector<IOFSR*>       Vector;
    typedef std::vector<const IOFSR*> ConstVector;

    /// File Input Events, {GUID:unsigned long long}
    typedef std::map<std::string, unsigned long long> FileEventMap;
    /// File Provenance, {GUID:<GUIDs>}
    typedef std::map<std::string, std::vector<std::string>> ProvenanceMap;

    /// FSR status, is the accounting OK
    enum StatusFlag {
      Unknown      = -6, // other enum possibilities
      FILEDOUBLED  = -5, // Some files have been multiply processed, in more than one job
      EVENTDOUBLED = -4, // Some events have been multiply processed, seen events too large
      EVENTSKIPPED = -3, // Some events have been skipped, seen events too small
      UNRELIABLE   = -2, // The counting in this FSR is known to be suspect
      ERROR        = -1, // Missing or corrupt FSRs
      UNCHECKED    = 0,  // The counting needs to be checked
      VERIFIED     = 1   // The counting in all lower FSRs has been verified OK
    };

    /// Default Constructor
    IOFSR() = default;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::IOFSR::classID(); }
    static const CLID& classID() { return CLID_IOFSR; }

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override;

    /// conversion of string to enum for type StatusFlag
    static LHCb::IOFSR::StatusFlag StatusFlagToType( const std::string& aName );

    /// conversion to string for enum type StatusFlag
    static const std::string& StatusFlagToString( int aEnum );

    /// Retrieve const  Number of events this output file should contain
    [[nodiscard]] unsigned long long eventsOutput() const { return m_eventsOutput; }

    /// Update  Number of events this output file should contain
    IOFSR& setEventsOutput( unsigned long long value ) {
      m_eventsOutput = value;
      return *this;
    }

    /// Retrieve const  Number of events read over to produce this file
    [[nodiscard]] unsigned long long eventsSeen() const { return m_eventsSeen; }

    /// Update  Number of events read over to produce this file
    IOFSR& setEventsSeen( unsigned long long value ) {
      m_eventsSeen = value;
      return *this;
    }

    /// Retrieve const  list of parent files at this level
    [[nodiscard]] const std::vector<std::string>& parents() const { return m_parents; }

    /// Update  list of parent files at this level
    IOFSR& setParents( const std::vector<std::string>& value ) {
      m_parents = value;
      return *this;
    }

    /// Retrieve const  maps of ancestor files below this level
    [[nodiscard]] const ProvenanceMap& provenance() const { return m_provenance; }

    /// Update  maps of ancestor files below this level
    IOFSR& setProvenance( const ProvenanceMap& value ) {
      m_provenance = value;
      return *this;
    }

    /// Retrieve const  Map of input files and events written to those files
    [[nodiscard]] const FileEventMap& eventsWrittenTo() const { return m_eventsWrittenTo; }

    /// Update  Map of input files and events written to those files
    IOFSR& setEventsWrittenTo( const FileEventMap& value ) {
      m_eventsWrittenTo = value;
      return *this;
    }

    /// Retrieve const  Map of input files and events read in creating those files
    [[nodiscard]] const FileEventMap& eventsSeenBy() const { return m_eventsSeenBy; }

    /// Update  Map of input files and events read in creating those files
    IOFSR& setEventsSeenBy( const FileEventMap& value ) {
      m_eventsSeenBy = value;
      return *this;
    }

    /// Retrieve const  Map of input files and events read from those files
    [[nodiscard]] const FileEventMap& eventsReadFrom() const { return m_eventsReadFrom; }

    /// Update  Map of input files and events read from those files
    IOFSR& setEventsReadFrom( const FileEventMap& value ) {
      m_eventsReadFrom = value;
      return *this;
    }

    /// Retrieve const  May be filled with this file's GUID if it is known, almost never known
    [[nodiscard]] const std::string& fid() const { return m_FID; }

    /// Update  May be filled with this file's GUID if it is known, almost never known
    IOFSR& setFID( const std::string& value ) {
      m_FID = value;
      return *this;
    }

    /// Retrieve const  Is the FSR reliable? This time, OK wrt streaming. Fill during merging.
    [[nodiscard]] const LHCb::IOFSR::StatusFlag& statusFlag() const { return m_statusFlag; }

    /// Update  Is the FSR reliable? This time, OK wrt streaming. Fill during merging.
    IOFSR& setStatusFlag( const LHCb::IOFSR::StatusFlag& value ) {
      m_statusFlag = value;
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const IOFSR& obj ) { return obj.fillStream( str ); }

  private:
    unsigned long long       m_eventsOutput = 0; ///< Number of events this output file should contain
    unsigned long long       m_eventsSeen   = 0; ///< Number of events read over to produce this file
    std::vector<std::string> m_parents;          ///< list of parent files at this level
    ProvenanceMap            m_provenance;       ///< maps of ancestor files below this level
    FileEventMap             m_eventsWrittenTo;  ///< Map of input files and events written to those files
    FileEventMap             m_eventsSeenBy;     ///< Map of input files and events read in creating those files
    FileEventMap             m_eventsReadFrom;   ///< Map of input files and events read from those files
    std::string              m_FID; ///< May be filled with this file's GUID if it is known, almost never known
    LHCb::IOFSR::StatusFlag  m_statusFlag =
        LHCb::IOFSR::StatusFlag::UNCHECKED; ///< Is the FSR reliable? This time, OK wrt streaming. Fill during merging.

    static const GaudiUtils::VectorMap<std::string, StatusFlag>& s_StatusFlagTypMap();

  }; // class IOFSR

  inline std::ostream& operator<<( std::ostream& s, LHCb::IOFSR::StatusFlag e ) {
    switch ( e ) {
    case LHCb::IOFSR::Unknown:
      return s << "Unknown";
    case LHCb::IOFSR::FILEDOUBLED:
      return s << "FILEDOUBLED";
    case LHCb::IOFSR::EVENTDOUBLED:
      return s << "EVENTDOUBLED";
    case LHCb::IOFSR::EVENTSKIPPED:
      return s << "EVENTSKIPPED";
    case LHCb::IOFSR::UNRELIABLE:
      return s << "UNRELIABLE";
    case LHCb::IOFSR::ERROR:
      return s << "ERROR";
    case LHCb::IOFSR::UNCHECKED:
      return s << "UNCHECKED";
    case LHCb::IOFSR::VERIFIED:
      return s << "VERIFIED";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::IOFSR::StatusFlag";
    }
  }

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

inline std::ostream& LHCb::IOFSR::fillStream( std::ostream& s ) const {
  using GaudiUtils::operator<<;
  return s << "{ "
           << "eventsOutput :	" << m_eventsOutput << "\neventsSeen :	" << m_eventsSeen
           << "\nparents :	" << m_parents << "\nprovenance :	" << m_provenance
           << "\neventsWrittenTo :	" << m_eventsWrittenTo << "\neventsSeenBy :	" << m_eventsSeenBy
           << "\neventsReadFrom :	" << m_eventsReadFrom << "\nFID :	" << m_FID
           << "\nstatusFlag :	" << m_statusFlag << "\n }";
}

inline const GaudiUtils::VectorMap<std::string, LHCb::IOFSR::StatusFlag>& LHCb::IOFSR::s_StatusFlagTypMap() {
  static const GaudiUtils::VectorMap<std::string, StatusFlag> m = {
      {"Unknown", Unknown},           {"FILEDOUBLED", FILEDOUBLED}, {"EVENTDOUBLED", EVENTDOUBLED},
      {"EVENTSKIPPED", EVENTSKIPPED}, {"UNRELIABLE", UNRELIABLE},   {"ERROR", ERROR},
      {"UNCHECKED", UNCHECKED},       {"VERIFIED", VERIFIED}};
  return m;
}

inline LHCb::IOFSR::StatusFlag LHCb::IOFSR::StatusFlagToType( const std::string& aName ) {
  auto iter = s_StatusFlagTypMap().find( aName );
  return iter != s_StatusFlagTypMap().end() ? iter->second : Unknown;
}

inline const std::string& LHCb::IOFSR::StatusFlagToString( int aEnum ) {
  static const std::string s_Unknown = "Unknown";
  auto                     iter      = std::find_if( s_StatusFlagTypMap().begin(), s_StatusFlagTypMap().end(),
                            [&]( const std::pair<const std::string, StatusFlag>& i ) { return i.second == aEnum; } );
  return iter != s_StatusFlagTypMap().end() ? iter->first : s_Unknown;
}
