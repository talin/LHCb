/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Detector/UT/ChannelID.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include <cassert>
#include <ostream>

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_UTDigit = 9003;

  // Namespace for locations in TDS
  namespace UTDigitLocation {
    inline const std::string UTDigits      = "Raw/UT/Digits";
    inline const std::string UTTightDigits = "Raw/UT/TightDigits"; // Digit after ADC value cuts
  }                                                                // namespace UTDigitLocation

  /** @class UTDigit UTDigit.h
   *
   * Upstream tracker digitization class
   *
   * @author Xuhao Yuan (based on code by Andy Beiter and Matthew Needham)
   *
   */

  class UTDigit final : public KeyedObject<LHCb::Detector::UT::ChannelID> {
  public:
    /// typedef for KeyedContainer of UTDigit
    typedef KeyedContainer<UTDigit, Containers::HashMap> Container;

    /// special constructor
    UTDigit( float charge ) : m_depositedCharge( charge ) {}

    /// channelID constructor
    UTDigit( LHCb::Detector::UT::ChannelID utchannelID, float charge )
        : KeyedObject<LHCb::Detector::UT::ChannelID>{utchannelID}, m_depositedCharge( charge ) {
      assert( utchannelID.isUT() && "Non-UT channelID" );
    }

    /// constructor overloading
    UTDigit( float charge, LHCb::Detector::UT::ChannelID utchannelID ) : UTDigit( utchannelID, charge ) {}

    /// channelID and DAQID constructor
    UTDigit( LHCb::Detector::UT::ChannelID utchannelID, float charge, unsigned int utdaqID )
        : KeyedObject<LHCb::Detector::UT::ChannelID>{utchannelID}, m_depositedCharge( charge ), m_daqID( utdaqID ) {
      assert( utchannelID.isUT() && "Non-UT channelID" );
    }

    /// copy constructor
    UTDigit( const LHCb::UTDigit& digit )
        : KeyedObject<LHCb::Detector::UT::ChannelID>(), m_depositedCharge( digit.m_depositedCharge ) {}

    /// Default Constructor
    UTDigit() = default;

    // Retrieve pointer to class definition structure
    [[nodiscard]] const CLID& clID() const override;
    static const CLID&        classID();

    /// channel = key
    [[nodiscard]] Detector::UT::ChannelID channelID() const { return key(); }

    /// Print the digit in a human readable way
    std::ostream& fillStream( std::ostream& s ) const override;

    /// get DAQID
    [[nodiscard]] constexpr unsigned int getdaqID() const { return m_daqID; }

    /// short cut for station
    [[nodiscard]] unsigned int station() const { return channelID().station(); }

    /// shortcut for layer
    [[nodiscard]] unsigned int layer() const { return channelID().layer(); }

    /// short cut for detRegion
    [[nodiscard]] unsigned int detRegion() const { return channelID().detRegion(); }

    /// short cut for sector
    [[nodiscard]] unsigned int sector() const { return channelID().sector(); }

    /// short cut for strip
    [[nodiscard]] unsigned int strip() const { return channelID().strip(); };

    /// Print the unique sector name
    [[nodiscard]] std::string sectorName() const;

    /// Print the unique layer name
    [[nodiscard]] std::string layerName() const;

    /// Print the unique det region name
    [[nodiscard]] std::string detRegionName() const;

    /// Print the station name
    [[nodiscard]] std::string stationName() const;

    /// Retrieve const  charge deposited on strip
    [[nodiscard]] constexpr float depositedCharge() const { return m_depositedCharge; }

    /// Update  charge deposited on strip
    constexpr UTDigit& setDepositedCharge( float value ) {
      m_depositedCharge = value;
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const UTDigit& obj ) { return obj.fillStream( str ); }

  private:
    float        m_depositedCharge{0.0}; ///< charge deposited on strip
    unsigned int m_daqID{0};

  }; // class UTDigit

  /// Definition of Keyed Container for UTDigit
  typedef KeyedContainer<UTDigit, Containers::HashMap> UTDigits;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline const CLID& LHCb::UTDigit::clID() const { return LHCb::UTDigit::classID(); }

inline const CLID& LHCb::UTDigit::classID() { return CLID_UTDigit; }

inline std::ostream& LHCb::UTDigit::fillStream( std::ostream& s ) const {
  return s << "{ UTDigit with key: " << (int)key() << " channel: " << sectorName() << " strip " << strip()
           << " depositedCharge :" << (float)m_depositedCharge << std::endl
           << " }";
}
