/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/Calo/CaloCellCode.h"
#include "Detector/Calo/CaloCellID.h"
#include <algorithm>
#include <array>
#include <cassert>
#include <memory>
#include <numeric>
#include <optional>

/**
 * Calorimeter digits
 *
 * Please note that v2::Digits plays a similar role to the v2::Track, namely as a starting
 * point for further optimization, and not as the ultimate goal.
 *
 */

namespace LHCb::Event::Calo {
  namespace details {
    template <size_t N>
    class bitmap {
      constexpr static auto                             n = 8 * sizeof( unsigned long long );
      std::array<unsigned long long, 1 + ( N - 1 ) / n> m_data{};
      static_assert( 8 * sizeof( m_data ) >= N );

    public:
      constexpr static auto size() { return N; }

      bool empty() const {
        return std::all_of( m_data.begin(), m_data.end(), []( auto i ) { return i == 0; } );
      }
      int count() const {
        return std::accumulate( m_data.begin(), m_data.end(), 0, []( int c, unsigned long long ull ) {
          return c + __builtin_popcountll( ull ); // FIXME: C++20: std::popcount
        } );
      }
      bool operator[]( unsigned i ) const {
        assert( i < N );
        return m_data[i / n] & ( 0x1ull << ( i % n ) );
      }
      void set( unsigned i ) {
        assert( i < N );
        m_data[i / n] |= 0x1ull << ( i % n );
      }
      unsigned first() const {
        for ( unsigned j = 0; j != m_data.size(); ++j ) {
          if ( m_data[j] != 0 )
            return j * n + __builtin_ctzll( m_data[j] ); // FIXME: C++20 std::countr_zero(m_data[j]);
        }
        return N;
      }
      unsigned next( unsigned i ) const {
        auto v = i / n;
        auto w = i % n;
        auto d = m_data[v] & ( ~0ull << w );
        if ( d != 0 ) return v * n + __builtin_ctzll( d );
        for ( unsigned j = v + 1; j < m_data.size(); ++j ) {
          if ( m_data[j] != 0 )
            return j * n + __builtin_ctzll( m_data[j] ); // FIXME: C++20 std::countr_zero(m_data[j]);
        }
        return N;
      }
    };
  } // namespace details

  inline namespace v2 {

    class Digit final {
      LHCb::Detector::Calo::CellID m_id;
      float                        m_energy;
      int                          m_adc;

    public:
      constexpr Digit( LHCb::Detector::Calo::CellID id, float energy, int adc ) noexcept
          : m_id{id}, m_energy{energy}, m_adc{adc} {}
      [[nodiscard]] constexpr LHCb::Detector::Calo::CellID cellID() const noexcept { return m_id; }
      [[nodiscard]] constexpr float                        energy() const noexcept { return m_energy; }
      [[nodiscard]] constexpr int                          adc() const noexcept { return m_adc; }
      [[nodiscard]] friend bool                            operator==( const Digit& lhs, const Digit& rhs ) noexcept {
        return lhs.m_id == rhs.m_id && lhs.m_energy == rhs.m_energy && lhs.m_adc == rhs.m_adc;
      }
    };

    class Digits final {

      struct Storage {
        details::bitmap<LHCb::Detector::Calo::Index::max()>     m_valid{};
        std::array<float, LHCb::Detector::Calo::Index::max()>   m_energy{};
        std::array<int16_t, LHCb::Detector::Calo::Index::max()> m_adc{};
      };
      std::unique_ptr<Storage> m_data = std::make_unique<Storage>();

      friend class Iterator;

    public:
      std::optional<const Digit> find( LHCb::Detector::Calo::CellID id ) const {
        LHCb::Detector::Calo::Index idx{id};
        if ( !idx || !m_data->m_valid[idx] ) { // FIXME: remove the check whether id is a valid CeloCellID...
          return std::nullopt;
        }
        return Digit{id, m_data->m_energy[idx], m_data->m_adc[idx]};
      }

      auto operator()( LHCb::Detector::Calo::CellID id ) const { return find( id ); }

      std::optional<const Digit> try_emplace( LHCb::Detector::Calo::CellID id, float energy, int adc ) {
        LHCb::Detector::Calo::Index idx{id};
        if ( m_data->m_valid[idx] ) return std::nullopt;
        m_data->m_energy[idx] = energy;
        m_data->m_adc[idx]    = adc;
        m_data->m_valid.set( idx );
        return Digit{id, energy, adc};
      }

      [[nodiscard]] bool empty() const { return m_data->m_valid.empty(); }
      [[nodiscard]] int  size() const { return m_data->m_valid.count(); }

      class Iterator {
        const Digits::Storage* m_data   = nullptr;
        int                    m_offset = 0;

        void next_valid() { m_offset = m_data->m_valid.next( m_offset ); }

        friend class Digits;
        Iterator( const Digits::Storage* storage, int offset = 0 ) : m_data( storage ), m_offset( offset ) {
          next_valid();
        }

      public:
        Iterator& operator++() {
          ++m_offset;
          next_valid();
          return *this;
        }
        Iterator operator++( int ) {
          Iterator other{*this};
          ++*this;
          return other;
        }
        friend bool operator!=( Iterator lhs, Iterator rhs ) {
          assert( lhs.m_data == rhs.m_data );
          return lhs.m_offset != rhs.m_offset;
        }
        friend bool operator==( Iterator lhs, Iterator rhs ) {
          assert( lhs.m_data == rhs.m_data );
          return lhs.m_offset == rhs.m_offset;
        }
        [[nodiscard]] const Digit operator*() const {
          assert( m_data->m_valid[m_offset] );
          return {LHCb::Detector::Calo::DenseIndex::details::toCellID( m_offset ), m_data->m_energy[m_offset],
                  m_data->m_adc[m_offset]};
        }
      };

      [[nodiscard]] Iterator begin() const { return {m_data.get(), 0}; }
      [[nodiscard]] Iterator end() const { return {m_data.get(), LHCb::Detector::Calo::Index::max()}; }
    };

  } // namespace v2
} // namespace LHCb::Event::Calo
