/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <cstdint>
#include <string>
#include <vector>

namespace LHCb {

  namespace FTCounterLocation {
    inline const std::string Default = "Raw/FT/FTCounters";
  }

  /** @struct FTCounter FTCounter.h
   *
   * This class represents the calibration counter for one single SiPM channel
   *
   */
  struct FTCounter {
    using FTCounters = std::vector<FTCounter>;

    std::uint32_t runNumber;
    std::uint16_t stepNumber;
    std::uint16_t sourceID;
    std::uint8_t  link;
    std::uint8_t  channel;
    std::uint8_t  integrator;
    std::uint16_t countsAdc0;
    std::uint16_t countsAdc1;
    std::uint16_t countsAdc2;
    std::uint16_t countsAdc3;
  };

} // namespace LHCb
