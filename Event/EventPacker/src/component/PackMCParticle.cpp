/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/MCParticle.h"
#include "Event/PackedMCParticle.h"
#include "Event/StandardPacker.h"

#include "LHCbAlgs/Consumer.h"

#include <algorithm>

namespace LHCb {

  /**
   *  Pack the MCParticles
   *
   *  Note that the inheritance from Consumer and the void input are misleading.
   *  The algorithm is reading from and writing to TES, just via Handles so that
   *  it can ignore failures on the output and thus behave as the previous, non
   *  functional algo and also succeed in case input is missing, again for backward
   *  compatibility.
   *  FIXME we should not ignore output failures obviously, but here it's an
   *  "expected" one due to the entanglement of MCVertex and MCParticle packers
   *  (see comment below).
   *
   *  @author Olivier Callot
   *  @date   2005-03-18
   */
  struct PackMCParticle : Algorithm::Consumer<void()> {
    using Consumer::Consumer;

    DataObjectReadHandle<MCParticles>        m_parts{this, "InputName", MCParticleLocation::Default};
    DataObjectWriteHandle<PackedMCParticles> m_pparts{this, "OutputName", PackedMCParticleLocation::Default};

    void operator()() const override;
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::PackMCParticle, "PackMCParticle" )

void LHCb::PackMCParticle::operator()() const {

  if ( !m_parts.exist() ) return;
  auto const* parts = m_parts.get();

  if ( msgLevel( MSG::DEBUG ) )
    debug() << m_parts.fullKey() << " contains " << parts->size() << " MCParticles to convert." << endmsg;

  // NOTE: in case of failure, the failure is considered as "normal" and interpreted
  // as the fact that the MCParticles were already packed as a consequence of the
  // packing of MCVertices and the action of DataOnDemand. Thus the failure is
  // ignored. FIXME !
  try {
    auto* out = m_pparts.put( std::make_unique<PackedMCParticles>() );
    out->mcParts().reserve( parts->size() );
    for ( const MCParticle* part : *parts ) {
      auto& newPart        = out->mcParts().emplace_back( PackedMCParticle() );
      newPart.key          = part->key();
      newPart.px           = StandardPacker::energy( part->momentum().px() );
      newPart.py           = StandardPacker::energy( part->momentum().py() );
      newPart.pz           = StandardPacker::energy( part->momentum().pz() );
      newPart.mass         = (float)part->virtualMass();
      newPart.PID          = part->particleID().pid();
      newPart.flags        = part->flags();
      newPart.originVertex = StandardPacker::reference64( out, part->originVertex() );
      for ( const auto& V : part->endVertices() ) {
        newPart.endVertices.push_back( StandardPacker::reference64( out, V ) );
      }
    }

  } catch ( GaudiException& ) { return; }

  // Clear the registry address of the unpacked container, to prevent reloading
  parts->registry()->setAddress( nullptr );
}
