/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "BufferSOAPackerBaseAlg.h"
#include "Event/CaloAdc.h"
#include "Event/CaloCluster.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloHypos_v2.h"
#include "Event/FlavourTag.h"
#include "Event/HltDecReports.h"
#include "Event/MCParticle.h"
#include "Event/PackedCaloAdc.h"
#include "Event/PackedCaloCluster.h"
#include "Event/PackedCaloDigit.h"
#include "Event/PackedCaloHypo.h"
#include "Event/PackedData.h"
#include "Event/PackedDataBuffer.h"
#include "Event/PackedDataChecksum.h"
#include "Event/PackedEventChecks.h"
#include "Event/PackedFlavourTag.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedProtoParticle.h"
#include "Event/PackedRecSummary.h"
#include "Event/PackedRecVertex.h"
#include "Event/PackedRelations.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedTrack.h"
#include "Event/PackedTwoProngVertex.h"
#include "Event/PackedVertex.h"
#include "Event/PackedWeightsVector.h"
#include "Event/Particle.h"
#include "Event/RecSummary.h"
#include "Event/RelatedInfoMap.h"
#include "Event/Track_v3.h"
#include "Event/TwoProngVertex.h"
#include "Event/WeightsVector.h"
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/DataObjID.h"
#include "Kernel/IIndexedANNSvc.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/TaggedBool.h"
#include "RegistryWrapper.h"
#include "RelationPackers.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"
#include <GaudiKernel/detected.h>
#include <LHCbAlgs/MergingTransformer.h>
#include <algorithm>
#include <numeric>
#include <type_traits>

namespace Gaudi::Parsers {
  StatusCode parse( std::map<std::string, DataObjIDColl>& result, const std::string& s ) {
    std::map<std::string, std::vector<std::string>> t;
    return parse( t, s ).andThen( [&] {
      result.clear();
      for ( const auto& [k, v] : t ) {
        DataObjIDColl col;
        std::transform( v.begin(), v.end(), std::inserter( col, col.end() ),
                        []( const auto& i ) { return DataObjID{i}; } );
        result.emplace( k, col );
      }
    } );
  }
} // namespace Gaudi::Parsers

namespace LHCb {
  template <typename F, typename Value, typename... C>
  void enumerate( F f, Value i, C&&... c ) {
    ( f( i++, std::forward<C>( c ) ), ... );
  }

  namespace {
    template <typename C>
    auto size( C const& c ) -> decltype( c.size() ) {
      return c.size();
    }

    template <typename From, typename To>
    auto size( LHCb::Relation1D<From, To> const& r ) -> decltype( r.relations().size() ) {
      return r.relations().size();
    }

    template <typename From, typename To, typename Weight>
    auto size( LHCb::RelationWeighted1D<From, To, Weight> const& r ) -> decltype( r.relations().size() ) {
      return r.relations().size();
    }
    template <typename Target, typename... Items>
    struct packer_type;

    template <typename Target, typename Head, typename... Tails>
    struct packer_type<Target, Head, Tails...> {
      template <typename T>
      using DataVector_for_t =
          typename T::DataVector; // allow T to mimic another class by pretending it is T::DataVector
      using TargetDataVector =
          Gaudi::cpp17::detected_or_t<Target, DataVector_for_t, Target>; // either Target, or Target::DataVector if that
                                                                         // is defined... this allow Target to 'pretend'
                                                                         // it is a DataVector the packer will be happy
                                                                         // with
      using type = std::conditional_t<std::is_same_v<TargetDataVector, typename Head::DataVector>, Head,
                                      typename packer_type<Target, Tails...>::type>;
    };
    template <typename Target>
    struct packer_type<Target> {
      using type = void;
    };

    template <typename Target, typename... Packers>
    using select_packer_for_t = typename packer_type<Target, Packers...>::type;

    template <typename Target>
    using packer_for_t =
        select_packer_for_t<Target, LHCb::RecVertexPacker, LHCb::VertexPacker, LHCb::TwoProngVertexPacker,
                            LHCb::RichPIDPacker, LHCb::MuonPIDPacker, LHCb::ProtoParticlePacker, LHCb::ParticlePacker,
                            LHCb::TrackPacker, LHCb::FlavourTagPacker, LHCb::CaloHypoPacker, LHCb::CaloClusterPacker,
                            LHCb::CaloDigitPacker, LHCb::CaloAdcPacker, LHCb::WeightsVectorPacker,
                            LHCb::RecSummaryPacker, LHCb::Packers::ParticleRelation<LHCb::VertexBase>,
                            LHCb::Packers::ParticleRelation<LHCb::MCParticle>, LHCb::Packers::PP2MCPRelation,
                            LHCb::Packers::P2IntRelation, LHCb::Packers::P2InfoRelation>;

    template <typename T>
    using VOC = Gaudi::Functional::vector_of_const_<T>;

    struct Counters {
      Gaudi::Accumulators::SummingCounter<unsigned int> nbPackedData;
      Gaudi::Accumulators::SummingCounter<unsigned int> nbBufferData;
      Gaudi::Accumulators::SummingCounter<unsigned int> nContainersInput;
      Gaudi::Accumulators::SummingCounter<unsigned int> nContainersPacked;
      Gaudi::Accumulators::MsgCounter<MSG::ERROR>       unregisterError;

      Counters( Gaudi::Algorithm const* parent, std::string_view name )
          : nbPackedData{parent, fmt::format( "{} # of packed entries", name )}
          , nbBufferData{parent, fmt::format( "{} buffer size", name )}
          , nContainersInput{parent, fmt::format( "{} # containers input", name )}
          , nContainersPacked{parent, fmt::format( "{} # containers packed", name )}
          , unregisterError{parent, "Problem unregistering data during packing check", 10} {}
    };

    template <typename... Containers>
    class CountersFor {
      std::array<Counters, sizeof...( Containers )> m_counters;

    public:
      CountersFor( Gaudi::Algorithm const* parent )
          : m_counters{Counters{parent, packer_for_t<Containers>::propertyName()}...} {}

      template <typename C>
      constexpr Counters& get() {
        return m_counters[index_of_v<C, std::tuple<Containers...>>];
      }
    };

    /// Copy data object version
    void saveVersion( Gaudi::Algorithm const& parent, int i_ver, std::string_view identifier, DataObject& out ) {
      const int o_ver = out.version();
      // sanity check
      if ( o_ver != 0 && o_ver != i_ver ) {
        parent.warning() << identifier << "  input version " << i_ver << " != current packed version " << o_ver
                         << endmsg;
      }
      out.setVersion( i_ver );
    }

    const Gaudi::StringKey PackedObjectLocations{"PackedObjectLocations"};

    using Buffer = Hlt::PackedData::PackedDataOutBuffer;

    using P2VRelations    = Relation1D<Particle, VertexBase>;
    using P2MCPRelations  = Relation1D<Particle, MCParticle>;
    using P2IntRelations  = Relation1D<Particle, int>;
    using P2InfoRelations = Relation1D<Particle, RelatedInfoMap>;
    using PP2MCPRelations = RelationWeighted1D<ProtoParticle, MCParticle, double>;

    std::string_view remove_prefix( std::string_view s, std::string_view prefix ) {
      if ( s.substr( 0, prefix.size() ) == prefix ) s.remove_prefix( prefix.size() );
      return s;
    }

    std::string_view remove_suffix( std::string_view s, std::string_view suffix ) {
      if ( s.substr( s.size() - suffix.size() ) == suffix ) s.remove_suffix( suffix.size() );
      return s;
    }

    class Encoder {
      DataObjIDColl const*                          m_requested;
      IIndexedANNSvc::inv_map_t const*              m_map;
      std::unordered_map<std::string, unsigned int> m_deps;
      unsigned                                      m_key = 0;
      std::string                                   m_prefix;
      bool                                          m_anonymize = false;
      unsigned int                                  id( std::string const& l ) {
        auto id = m_map->find( l ); // TODO: support transparent lookup
        if ( id == m_map->end() ) {
          throw GaudiException(
              fmt::format( "could not locate packedobjectlocation {} (configured prefix={}) in table for key 0x{:08x}",
                           l, m_prefix, m_key ),
              __PRETTY_FUNCTION__, StatusCode::FAILURE );
        }
        return id->second;
      }

    public:
      struct Anonymize_tag;
      using Anonymize = LHCb::tagged_bool<Anonymize_tag>;

      Encoder( DataObjIDColl const& requested, IIndexedANNSvc const& annsvc, unsigned key, std::string_view stream,
               Anonymize anonymize )
          : m_requested{&requested}
          , m_map{&annsvc.s2i( key, PackedObjectLocations )}
          , m_key{key}
          , m_prefix{fmt::format( "/Event/{}/", remove_suffix( remove_prefix( stream, "/Event/" ), "/" ) )}
          , m_anonymize{anonymize} {}
      unsigned         key() const { return m_key; }
      std::string_view stream() const { return remove_suffix( remove_prefix( m_prefix, "/Event/" ), "/" ); }
      auto             packedLocation( std::string_view loc ) const {
        if ( loc.empty() )
          throw GaudiException( "empty PackedObjectLocation", __PRETTY_FUNCTION__, StatusCode::FAILURE );
        if ( loc.front() != '/' ) return std::string{m_prefix}.append( loc );
        if ( loc.substr( 0, m_prefix.size() ) == m_prefix ) return std::string{loc};
        return std::string{m_prefix}.append( remove_prefix( loc, "/Event/" ) );
      }
      auto operator()( std::string_view loc ) {
        auto packed = packedLocation( loc );
        if ( m_anonymize && m_requested->find( DataObjID{std::string{loc}} ) == m_requested->end() ) {
          // implicit dependency -- generate location ID for this location if not already present (for this event!)
          auto [dep, _] = m_deps.emplace( packed, ( -1u ) - m_deps.size() );
          return dep->second;
        } else {
          return id( packed );
        }
      }
    };

    template <typename PackedDataVector>
    auto create_buffer( Gaudi::Algorithm const& parent, std::string_view location, Encoder& encoder,
                        PackedDataVector const& pdata ) {

      // reserve some space for data, this should be tuned
      LHCb::Hlt::PackedData::PackedDataOutBuffer buffer( encoder.key() );
      buffer.reserve( pdata.data().size() );

      buffer.save<uint32_t>( pdata.clID() );
      auto locationID = encoder( location );
      buffer.save<int32_t>( locationID );
      auto*        linkMgr = pdata.linkMgr();
      unsigned int nlinks  = linkMgr->size();

      if ( parent.msgLevel( MSG::DEBUG ) ) {
        parent.debug() << "packed version " << (unsigned int)pdata.version() << endmsg;
        parent.debug() << "packed data type " << System::typeinfoName( typeid( pdata ) ) << endmsg;
        parent.debug() << "packed data entries " << pdata.data().size() << endmsg;
        parent.debug() << "classID " << pdata.clID() << " locationID " << locationID << " nlinks " << nlinks << endmsg;
      }

      buffer.saveSize( nlinks );
      for ( auto const& link : *linkMgr ) buffer.save<int32_t>( encoder( link.path() ) );

      // Reserve bytes for the size of the object
      auto posObjectSize = buffer.saveSize( 0 ).first;

      // Save the object actual object and see how many bytes were written
      auto objectSize = buffer.save( pdata ).second;

      // Save the object's size in the correct position
      buffer.saveAt<uint32_t>( objectSize, posObjectSize );

      return buffer;
    }

    class ShoppingList {

      template <typename C>
      class Item {
        std::set<C const*> m_set;
        using Packer = packer_for_t<C>;
        static_assert( !std::is_void_v<Packer> );
        Packer    m_packer;
        Counters* m_counters = nullptr;

        auto create_packeddata( C const& d ) const {
          auto pdata = DataPacking::Buffer::RegistryWrapper<typename Packer::PackedDataVector>(
              fmt::format( "{}Packed", identifier( d ) ) );
          saveVersion( m_packer.parent(), d.version(), identifier( d ), *pdata );
          m_packer.pack( d, *pdata );
          return pdata;
        }

        void check_packeddata( C const& d, typename Packer::PackedDataVector const& pdata ) const {
          using DataVector = typename Packer::DataVector;
          auto* unpacked   = new DataVector();
          m_packer.parent()
              .evtSvc()
              ->registerObject( fmt::format( "{}_PackingCheck", identifier( d ) ), unpacked )
              .ignore();
          unpacked->setVersion( pdata.version() );
          m_packer.unpack( pdata, *unpacked );
          // checker
          const DataPacking::DataChecks ch( m_packer.parent() );
          StatusCode                    sc = ch.check( m_packer, d, *unpacked );

          if ( sc == StatusCode::FAILURE )
            m_packer.parent().warning() << "Packing check failed for " << identifier( d ) << endmsg;
          else
            m_packer.parent().info() << "Packing check successful for " << identifier( d ) << endmsg;

          m_packer.parent()
              .evtSvc()
              ->unregisterObject( unpacked )
              .andThen( [&unpacked] { delete unpacked; } )
              .orElse( [&] { /* ++unregisterError;*/ } )
              .ignore();
        }

      public:
        Item( Gaudi::Algorithm const& parent, Counters& counters ) : m_packer{&parent}, m_counters{&counters} {}

        Packer& packer() { return m_packer; }

        bool emplace( C const* c ) { return m_set.emplace( c ).second; }

        void append_to( Buffer& buffer, Encoder& encoder, bool check,
                        LHCb::Hlt::PackedData::PackedDataChecksum* checksum ) const {
          for ( auto i : m_set ) {
            if ( m_packer.parent().msgLevel( MSG::DEBUG ) ) {
              m_packer.parent().debug() << "Location " << identifier( *i ) << " (" << encoder( identifier( *i ) ) << ")"
                                        << " size " << size( *i ) << " version " << static_cast<int>( i->version() )
                                        << " type " << System::typeinfoName( typeid( C ) ) << endmsg;
            }
            const auto pdata = create_packeddata( *i );
            m_counters->nbPackedData += pdata->data().size();
            if ( checksum )
              checksum->processObject( *pdata, encoder.packedLocation( ( identifier( *i ) ) ) + "Packed" );
            if ( check ) check_packeddata( *i, *pdata );

            const auto buf = create_buffer( m_packer.parent(), identifier( *i ), encoder, *pdata );
            m_counters->nbBufferData += buf.size();
            buffer.addBuffer( buf );
          }
          m_counters->nContainersPacked += m_set.size();
        }
      };

      template <typename ValueType, typename Mapping>
      class Item<KeyedContainer<ValueType, Mapping>> {
        using C = KeyedContainer<ValueType, Mapping>;
        using K = typename C::key_type;
        struct Less {
          using is_transparent = void;
          bool operator()( ValueType const* lhs, ValueType const* rhs ) const {
            return std::pair{lhs->parent(), lhs->key()} < std::pair{rhs->parent(), rhs->key()};
          }
          bool operator()( ObjectContainerBase const* c, ValueType const* rhs ) const { return c < rhs->parent(); }
          bool operator()( ValueType const* lhs, ObjectContainerBase const* c ) const { return lhs->parent() < c; }
        };

        using S = std::set<ValueType const*, Less>;
        S    m_set;
        bool m_locked = false;
        class ValueRange {
          using Iterator = typename S::const_iterator;
          Iterator m_current;
          Iterator m_end;

        public:
          ValueRange( Iterator current, Iterator end ) : m_current{current}, m_end{end} {
            assert( current == end || std::all_of( next( current ), end, [p = ( *current )->parent()]( const auto* i ) {
                      return i->parent() == p;
                    } ) );
          }

          auto        begin() const { return m_current; }
          auto        end() const { return m_end; }
          ValueRange& operator++() {
            ++m_current;
            return *this;
          }

          size_t      size() const { return std::distance( begin(), end() ); }
          const auto* container() const {
            assert( begin() != end() );
            return dynamic_cast<const C*>( ( *begin() )->parent() );
          }
          const std::string& identifier() const {
            assert( begin() != end() );
            using LHCb::identifier;
            return identifier( **begin() );
          }
          int version() const {
            assert( begin() != end() );
            return ( ( *begin() )->parent() )->version();
          }
        };

        class ContainerRange {
          struct Sentinel {};
          S const*   m_parent;
          ValueRange m_current;
          ValueRange next( ValueRange r ) const {
            return r.begin() != m_parent->end()
                       ? ValueRange{r.end(), r.end() != m_parent->end()
                                                 ? m_parent->upper_bound( ( *r.end() )->parent() )
                                                 : r.end()}
                       : r;
          }

        public:
          ContainerRange( S const& parent )
              : m_parent{&parent}, m_current{next( {m_parent->begin(), m_parent->begin()} )} {}

          auto     begin() const { return *this; }
          Sentinel end() const { return {}; }
          bool     operator!=( Sentinel ) const { return m_current.begin() != m_parent->end(); }

          ContainerRange& operator++() {
            m_current = next( m_current );
            return *this;
          }
          ValueRange const& operator*() const { return m_current; }

          auto size() const { return m_current.size(); }
        };
        using Packer = packer_for_t<C>;
        static_assert( !std::is_void_v<Packer> );
        Packer    m_packer;
        Counters* m_counters = nullptr;

        auto create_packeddata( ValueRange range ) const {
          auto pdata = DataPacking::Buffer::RegistryWrapper<typename Packer::PackedDataVector>(
              fmt::format( "{}Packed", identifier( range ) ) );
          saveVersion( m_packer.parent(), range.version(), identifier( range ), *pdata );
          pdata->data().reserve( range.size() );
          m_packer.pack( range, *pdata );
          return pdata;
        }

        void check_packeddata( ValueRange range, typename Packer::PackedDataVector const& pdata ) const {
          using DataVector = typename Packer::DataVector;
          auto* unpacked   = new DataVector();
          m_packer.parent()
              .evtSvc()
              ->registerObject( fmt::format( "{}_PackingCheck", identifier( range ) ), unpacked )
              .ignore();
          unpacked->setVersion( pdata.version() );
          m_packer.unpack( pdata, *unpacked );
          // checker
          const DataPacking::DataChecks ch( m_packer.parent() );
          StatusCode                    sc = ch.check( m_packer, range, *unpacked );

          if ( sc == StatusCode::FAILURE )
            m_packer.parent().warning() << "Packing check failed for " << identifier( range ) << endmsg;
          else
            m_packer.parent().info() << "Packing check successful for " << identifier( range ) << endmsg;

          m_packer.parent()
              .evtSvc()
              ->unregisterObject( unpacked )
              .andThen( [&unpacked] { delete unpacked; } )
              .orElse( [&] { /* ++unregisterError;*/ } )
              .ignore();
        }

      public:
        Item( Gaudi::Algorithm const& parent, Counters& counters ) : m_packer{&parent}, m_counters{&counters} {}

        Packer& packer() { return m_packer; }

        bool emplace( ValueType const* v ) {
          if ( m_locked )
            throw GaudiException( "attempt to add to locked item", __PRETTY_FUNCTION__, StatusCode::FAILURE );
          return m_set.emplace( v ).second;
        }

        bool contains( ValueType const* v ) const { return m_set.find( v ) != m_set.end(); }

        void lock() { m_locked = true; }

        void append_to( Buffer& buffer, Encoder& encoder, bool check,
                        LHCb::Hlt::PackedData::PackedDataChecksum* checksum ) const {
          int nContainers = 0;
          for ( const auto& chunk : ContainerRange{m_set} ) {
            assert( chunk.size() != 0 );
            ++nContainers;

            m_counters->nbPackedData += chunk.size();

            if ( m_packer.parent().msgLevel( MSG::DEBUG ) ) {
              m_packer.parent().debug() << "Location " << identifier( chunk ) << " (" << encoder( identifier( chunk ) )
                                        << ")"
                                        << " size " << chunk.size() << " version "
                                        << static_cast<int>( chunk.version() ) << " type "
                                        << System::typeinfoName( typeid( C ) ) << endmsg;
            }

            const auto pdata = create_packeddata( chunk );
            if ( checksum )
              checksum->processObject( *pdata, encoder.packedLocation( ( identifier( chunk ) ) ) + "Packed" );
            if ( check ) check_packeddata( chunk, *pdata );

            const auto buf = create_buffer( m_packer.parent(), identifier( chunk ), encoder, *pdata );

            if ( m_packer.parent().msgLevel( MSG::DEBUG ) ) {
              m_packer.parent().debug() << "buffer size " << buffer.size() << endmsg;
            }

            // Count packed output
            m_counters->nbBufferData += buf.size();

            buffer.addBuffer( buf );
          }
          m_counters->nContainersPacked += nContainers;
        }
      };

      template <typename From, typename To, typename Weight>
      class Item<RelationWeighted1D<From, To, Weight>> {
        using container_type = RelationWeighted1D<From, To, Weight>;

      public:
        class Entry {
        public:
          using container_type = RelationWeighted1D<From, To, Weight>;
          using entry_type     = typename container_type::Entry;
          using DataVector     = RelationWeighted1D<From, To, Weight>; // this is the thing we want to mimic

          Entry( container_type const* p, entry_type const* entry ) : m_parent{p}, m_entry{entry} {}

          auto             parent() const { return m_parent; }
          decltype( auto ) from() const { return m_entry->from(); }
          decltype( auto ) to() const { return m_entry->to(); }
          decltype( auto ) weight() const { return m_entry->weight(); }

        private:
          container_type const* m_parent;
          entry_type const*     m_entry;
        };

      private:
        struct Less {
          using is_transparent = void;
          bool operator()( Entry const& lhs, Entry const& rhs ) const {
            if ( lhs.parent() < rhs.parent() ) return true;
            if ( rhs.parent() < lhs.parent() ) return false;
            using LessF = typename Entry::entry_type::LessF;
            return LessF{}( lhs.from(), rhs.from() );
          }
          bool operator()( container_type const* c, Entry const& rhs ) const { return c < rhs.parent(); }
          bool operator()( Entry const& lhs, container_type const* c ) const { return lhs.parent() < c; }
        };
        using S = std::set<Entry, Less>;
        S m_set;

        class ValueRange {
          using Iterator = typename S::const_iterator;
          Iterator m_current;
          Iterator m_end;

        public:
          ValueRange( Iterator current, Iterator end ) : m_current{current}, m_end{end} {
            assert( current == end || std::all_of( next( current ), end, [p = current->parent()]( const auto& i ) {
                      return i.parent() == p;
                    } ) );
          }

          ValueRange const& relations() const { return *this; } // mimic the original RelationWeighted class...

          auto        begin() const { return m_current; }
          auto        end() const { return m_end; }
          ValueRange& operator++() {
            ++m_current;
            return *this;
          }

          size_t      size() const { return std::distance( begin(), end() ); }
          const auto* container() const {
            assert( begin() != end() );
            return begin()->parent();
          }
          const std::string& identifier() const {
            assert( begin() != end() );
            using LHCb::identifier;
            return identifier( *( begin()->parent() ) );
          }
          int version() const {
            assert( begin() != end() );
            return begin()->parent()->version();
          }
        };
        class ContainerRange {
          struct Sentinel {};
          S const*   m_parent;
          ValueRange m_current;
          ValueRange next( ValueRange r ) const {
            return r.begin() != m_parent->end()
                       ? ValueRange{r.end(),
                                    r.end() != m_parent->end() ? m_parent->upper_bound( r.end()->parent() ) : r.end()}
                       : r;
          }

        public:
          ContainerRange( S const& parent )
              : m_parent{&parent}, m_current{next( {m_parent->begin(), m_parent->begin()} )} {}

          auto     begin() const { return *this; }
          Sentinel end() const { return {}; }
          bool     operator!=( Sentinel ) const { return m_current.begin() != m_parent->end(); }

          ContainerRange& operator++() {
            m_current = next( m_current );
            return *this;
          }
          ValueRange const& operator*() const { return m_current; }

          auto size() const { return m_current.size(); }
        };

        using Packer = packer_for_t<RelationWeighted1D<From, To, Weight>>;
        static_assert( !std::is_void_v<Packer> );
        Packer    m_packer;
        Counters* m_counters = nullptr;

        auto create_packeddata( ValueRange range ) const {
          auto pdata = DataPacking::Buffer::RegistryWrapper<typename Packer::PackedDataVector>(
              fmt::format( "{}Packed", identifier( range ) ) );
          saveVersion( m_packer.parent(), range.version(), identifier( range ), *pdata );
          pdata->data().reserve( range.size() );
          m_packer.pack( range, *pdata );
          return pdata;
        }

        void check_packeddata( ValueRange range, typename Packer::PackedDataVector const& pdata ) const {
          using DataVector = typename Packer::DataVector;
          auto* unpacked   = new DataVector();
          m_packer.parent()
              .evtSvc()
              ->registerObject( fmt::format( "{}_PackingCheck", identifier( range ) ), unpacked )
              .ignore();
          unpacked->setVersion( pdata.version() );
          m_packer.unpack( pdata, *unpacked );

          // checker
          const DataPacking::DataChecks ch( m_packer.parent() );
          StatusCode                    sc = ch.check( m_packer, range, *unpacked );

          if ( sc == StatusCode::FAILURE )
            m_packer.parent().warning() << "Packing check failed for " << identifier( range ) << endmsg;
          else
            m_packer.parent().info() << "Packing check successful for " << identifier( range ) << endmsg;

          m_packer.parent()
              .evtSvc()
              ->unregisterObject( unpacked )
              .andThen( [&unpacked] { delete unpacked; } )
              .orElse( [&] { /* ++unregisterError;*/ } )
              .ignore();
        }

      public:
        Item( Gaudi::Algorithm const& parent, Counters& counters ) : m_packer{&parent}, m_counters{&counters} {}

        Packer& packer() { return m_packer; }

        bool emplace( Entry const* v ) { return m_set.emplace( *v ).second; }

        void append_to( Buffer& buffer, Encoder& encoder, bool check,
                        LHCb::Hlt::PackedData::PackedDataChecksum* checksum ) const {
          int nContainers = 0;
          for ( const auto& chunk : ContainerRange{m_set} ) {
            assert( chunk.size() != 0 );
            ++nContainers;

            m_counters->nbPackedData += chunk.size();

            if ( m_packer.parent().msgLevel( MSG::DEBUG ) ) {
              m_packer.parent().debug() << "Location " << identifier( chunk ) << " (" << encoder( identifier( chunk ) )
                                        << ")"
                                        << " size " << chunk.size() << " version "
                                        << static_cast<int>( chunk.version() ) << endmsg;
            }

            const auto pdata = create_packeddata( chunk );
            if ( checksum )
              checksum->processObject( *pdata, encoder.packedLocation( ( identifier( chunk ) ) ) + "Packed" );
            if ( check ) check_packeddata( chunk, *pdata );

            const auto buf = create_buffer( m_packer.parent(), identifier( chunk ), encoder, *pdata );

            if ( m_packer.parent().msgLevel( MSG::DEBUG ) ) {
              m_packer.parent().debug() << "buffer size " << buffer.size() << endmsg;
            }

            // Count packed output
            m_counters->nbBufferData += buf.size();

            buffer.addBuffer( buf );
          }
          m_counters->nContainersPacked += nContainers;
        }
      };

      template <typename Target, typename... Items>
      struct container_type;

      template <typename Target, typename Head, typename... Tails>
      struct container_type<Target, Head, Tails...> {
        template <typename T>
        using contained_t = typename T::contained_type;
        template <typename T>
        using container_t = typename T::container_type;
        using type =
            std::conditional_t<std::is_same_v<Target, Gaudi::cpp17::detected_or_t<void, contained_t, Head>> ||
                                   std::is_same_v<Head, Gaudi::cpp17::detected_or_t<void, container_t, Target>>,
                               Head, typename container_type<Target, Tails...>::type>;
      };

      template <typename Target>
      struct container_type<Target> {
        using type = void;
      };

      template <typename Target, typename... Containers>
      using container_type_t = typename container_type<Target, Containers...>::type;

      template <typename... Container>
      class Maps {
        std::tuple<Item<Container>...> m_items;

      public:
        template <typename Counters>
        Maps( const Gaudi::Algorithm& parent, Counters& counters )
            : m_items{{parent, counters.template get<Container>()}...} {}

        template <typename C>
        const auto& get() const {
          static_assert( std::disjunction_v<std::is_same<C, Container>...> ||
                         std::disjunction_v<std::is_same<container_type_t<C, Container...>, Container>...> );
          if constexpr ( std::disjunction_v<std::is_same<C, Container>...> ) {
            return std::get<Item<C>>( m_items );
          } else {
            return std::get<Item<container_type_t<C, Container...>>>( m_items );
          }
        }

        template <typename C>
        auto& get() {
          static_assert( std::disjunction_v<std::is_same<C, Container>...> ||
                         std::disjunction_v<std::is_same<container_type_t<C, Container...>, Container>...> );
          if constexpr ( std::disjunction_v<std::is_same<C, Container>...> ) {
            return std::get<Item<C>>( m_items );
          } else {
            return std::get<Item<container_type_t<C, Container...>>>( m_items );
          }
        }

        template <typename Item>
        bool add( Item const& v ) {
          // v is either a container, or a contained element...
          // ... or an entry which wraps a contained element...
          using C = container_type_t<Item, Container...>;
          using T = std::conditional_t<std::is_void_v<C>, Item, C>;
          return this->template get<T>().emplace( &v );
        }

        template <typename Fun>
        void for_each( Fun f ) const {
          ( f( get<Container>() ), ... );
        }
      };

      template <typename V>
      void add( const SmartRef<V>& v ) {
        add( v.target() );
      }

      template <typename V>
      void add( const SmartRefVector<V>& v ) {
        for ( const auto& i : v ) add( i );
      }

      void add( const Particle& p ) {
        if ( m_lists.add( p ) ) {
          add( p.endVertex() );
          add( p.proto() );
          add( p.daughters() );
        }
      }

      void add( const Vertex& v ) {
        if ( m_lists.add( v ) ) add( v.outgoingParticles() );
      }

      void add( const VertexBase& v ) {
        if ( auto rv = dynamic_cast<const RecVertex*>( &v ) ) {
          add( *rv );
        } else {
          std::cerr << __PRETTY_FUNCTION__ << " do not know how to add this type  yet.... " << std::endl;
        }
      }

      void add( const ProtoParticle& p ) {
        if ( m_lists.add( p ) ) {
          add( p.track() );
          add( p.richPID() );
          add( p.muonPID() );
          add( p.calo() );
        }
      }

      void add( const Track& t ) {
        if ( m_lists.add( t ) && m_options.add_track_ancestors ) add( t.ancestors() );
      }

      void add( const RichPID& r ) {
        if ( m_lists.add( r ) ) add( r.track() );
      }

      void add( const MuonPID& m ) {
        if ( m_lists.add( m ) ) {
          add( m.idTrack() );
          add( m.muonTrack() );
        }
      }

      void add( const CaloHypo& h ) {
        if ( m_lists.add( h ) ) {
          if ( m_options.add_calo_digits ) add( h.digits() );
          if ( m_options.add_calo_clusters ) add( h.clusters() );
          add( h.hypos() );
        }
      }

      void add( const CaloCluster& c ) {
        if ( m_lists.add( c ) && m_options.add_calo_digits ) {
          for ( const auto& entry : c.entries() ) add( entry.digit() );
        }
      }

      // no references in PackedCaloDigit
      void add( const CaloDigit& h ) { m_lists.add( h ); }

      // no references in PackedCaloAdc
      void add( const CaloAdc& a ) { m_lists.add( a ); }

      void add( const FlavourTag& ft ) {
        if ( m_lists.add( ft ) ) {
          add( ft.taggedB() );
          // taggers are kept 'by value' inside FlavourTag, so no seperate 'add' for taggers type, i.e. Tagger
          // instead, go transitive here...
          if ( m_options.add_tagger_particles )
            for ( const auto& t : ft.taggers() ) add( t.taggerParts() );
        }
      }

      void add( const RecVertex& rv ) {
        if ( !rv.isPrimary() )
          throw GaudiException( fmt::format( "got RecVertex which is not primary from {}", identifier( rv ) ),
                                __PRETTY_FUNCTION__, StatusCode::FAILURE );
        if ( m_lists.add( rv ) ) {
          if ( m_options.add_pv_tracks ) add( rv.tracks() );
        }
      }

      void add( const TwoProngVertex& rv ) {
        if ( m_lists.add( rv ) ) add( rv.tracks() );
      }

      template <typename From, typename To>
      void add( const Relation1D<From, To>& r ) {
        // TODO: can go in two directions: either add anything that is in the table, or (once all keys are known) prune
        // all unused entries from the table. Since both may be needed, this should be configured externally
        // regardless, the 'to' side needs to go recursive (which may create loops?)
        // Probably best to prune the 'from' hand side (when explicitly requested!), and go recursive on the 'to' side
        // TODO: figure out what the packers need...
        if ( m_lists.add( r ) ) {
          if ( false /*  prune( r.registry() ) */ ) {
            for ( const auto& i : r.relations() ) {
              //     if ( !m_lists.contains( i.from() ) ) continue;
              // add( i );
              if constexpr ( std::is_base_of_v<DataObject, To> ) add( i.to() );
            }
          } else {
            for ( const auto& i : r.relations() ) {
              // add( i );
              add( i.from() );
              if constexpr ( std::is_base_of_v<DataObject, To> || std::is_base_of_v<ContainedObject, To> )
                add( i.to() );
            }
          }
        }
      }

      template <typename From, typename To, typename Weight>
      void add( const RelationWeighted1D<From, To, Weight>& r ) {
        auto* item = ( m_options.prune_relations /* ( r.registry() )*/ ? &m_lists.get<From>() : nullptr );
        // if we prune, have to make sure that, from now on, adding new `From` entries is considered an error
        if ( item ) item->lock();
        for ( const auto& i : r.relations() ) {
          if ( item ) {
            if ( !item->contains( i.from() ) ) continue;
          } else {
            add( i.from() );
          }
          if ( m_lists.add( typename Item<RelationWeighted1D<From, To, Weight>>::Entry{&r, &i} ) ) {
            if constexpr ( std::is_base_of_v<DataObject, To> || std::is_base_of_v<ContainedObject, To> ) add( i.to() );
          }
        }
      }

      template <typename Container>
      void add( VOC<Container*> const& c ) {
        for ( const auto& i : c ) add( i );
      }

      template <typename V>
      void add( const V* v ) {
        if ( v ) add( *v );
      }

      template <typename V, typename M>
      void add( const KeyedContainer<V, M>& kc ) {
        auto r = kc.registry();
        if ( r && is_explicitly_requested( identifier( *r ) ) ) {
          for ( const auto& i : kc ) add( i );
        } else {
          // std::cerr << "skipped " << identifier( kc ) << std::endl; TODO: add  a counter for each input location, and
          // count how often it is requested...
        }
      }

      void add( const RecSummary& rs ) {
        auto r = rs.registry();
        if ( r && is_explicitly_requested( identifier( *r ) ) ) m_lists.add( rs );
      }

      void add( const MCParticle& /*p*/ ) {
        // foe now, assume MCParticles are somehow packed elsewhere...
#if 0
      if ( m_lists.add( p ) ) {
        add( p.originVertex() );
        add( p.endVertices() );
      }
#endif
      }

      void add( const MCVertex& /*v*/ ) {
        // foe now, assume MCVertices are somehow packed elsewhere...
#if 0
      if ( m_lists.add( v ) ) {
        add( v.mother() );
        add( v.products() );
      }
#endif
      }

      void add( const WeightsVector& v ) { m_lists.add( v ); }

    public:
      struct Options {
        // TODO: make these 'per container' instead of global
        bool add_track_ancestors  = false;
        bool add_calo_digits      = false;
        bool add_calo_clusters    = false;
        bool add_tagger_particles = false;
        bool add_pv_tracks        = false;
        bool prune_relations      = true;
      };

      template <typename... Items>
      ShoppingList( Gaudi::Algorithm const& parent, DataObjIDColl containers, CountersFor<Items...>& counters,
                    Options options, VOC<Items*> const&... items )
          : m_requested{std::move( containers )}, m_options{std::move( options )}, m_lists{parent, counters} {
        m_lists.get<CaloHypos>()
            .packer()
            .packDigitRefs( m_options.add_calo_digits )
            .packClusterRefs( m_options.add_calo_clusters );
        m_lists.get<CaloClusters>().packer().packDigitRefs( m_options.add_calo_digits );
        m_lists.get<FlavourTags>().packer().packTagParticleRefs( m_options.add_tagger_particles );
        m_lists.get<RecVertices>().packer().packTrackRefs( m_options.add_pv_tracks );
        // do the actual work!
        ( add( items ), ... );
        // TODO: verify that anything in containers is actually added in the end....
      }

      bool is_explicitly_requested( DataObjID const& id ) { return m_requested.find( id ) != m_requested.end(); }

      template <typename Fun>
      void for_each( Fun f ) const {
        m_lists.for_each( std::forward<Fun>( f ) );
      }

    private:
      DataObjIDColl m_requested = {};
      Options       m_options   = {};
      // list of all supported types -- i.e. _containers_ for which packers exist.
      Maps<Particles, Vertices, RecVertices, TwoProngVertices, ProtoParticles, Tracks, RichPIDs, MuonPIDs, CaloHypos,
           CaloClusters, CaloDigits, CaloAdcs, FlavourTags, WeightsVectors, /* MCParticles, MCVertices,*/ RecSummary,
           P2VRelations, P2MCPRelations, P2IntRelations, P2InfoRelations, PP2MCPRelations>
          m_lists;
    };

    template <typename PACKER>
    using arg_for_packer_t = Gaudi::Functional::vector_of_const_<typename PACKER::DataVector*> const&;
    template <typename... PACKER>
    using baseclass_t = LHCb::Algorithm::MergingTransformer<Buffer( arg_for_packer_t<PACKER>... )>;

  } // namespace

  template <typename... PACKER>
  class SelectivePacker final : public baseclass_t<PACKER...> {
    DataObjectReadHandle<HltDecReports>                   m_decrep{this, "DecReports", HltDecReportsLocation::Default};
    Gaudi::Property<std::map<std::string, DataObjIDColl>> m_map{this, "LineToLocations", {}};
    Gaudi::Property<unsigned int>                         m_encodingKey{this, "EncodingKey", 0u};
    Gaudi::Property<bool>                                 m_enableCheck{this, "EnableCheck", false};
    Gaudi::Property<bool>                                 m_enableChecksum{this, "EnableChecksum", false};
    Gaudi::Property<bool>                                 m_anonymizeDependencies{this, "AnonymizeDependencies", false};
    Gaudi::Property<std::vector<std::string>>             m_addTrackAncestors{this, "AddTrackAncestors", {}};
    Gaudi::Property<std::vector<std::string>>             m_addCaloDigits{this, "AddCaloDigits", {}};
    Gaudi::Property<std::vector<std::string>>             m_addCaloClusters{this, "AddCaloClusters", {}};
    Gaudi::Property<std::vector<std::string>>             m_addTagParticles{this, "AddTagParticles", {}};
    Gaudi::Property<std::vector<std::string>>             m_addPVTracks{this, "AddPVTracks", {}};
    Gaudi::Property<std::string>                          m_stream{this, "OutputPrefix", {}};
    ServiceHandle<IIndexedANNSvc> m_annsvc{this, "ANNSvc", "HltANNSvc", "Service to retrieve PackedObjectLocations"};

    mutable CountersFor<typename PACKER::DataVector...> m_counters{this};

  public:
    SelectivePacker( std::string const& name, ISvcLocator* pSvcLocator )
        : baseclass_t<PACKER...>{name, pSvcLocator, {{PACKER::propertyName(), {}}...}, {"outputLocation", {}}} {}

    Buffer operator()( arg_for_packer_t<PACKER>... items ) const override {
      const auto& dec = m_decrep.get();
      // figure out the subset of containers to actually persist
      DataObjIDColl containers;
      if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "got positive decisions: ";
      for ( auto const& [k, dr] : *dec ) {
        if ( !dr.decision() ) continue;
        if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << k << " ";
        auto i = m_map.find( k );
        if ( i != m_map.end() ) containers.insert( i->second.begin(), i->second.end() );
      }
      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << endmsg;
        this->debug() << "explicitly requested containers for this event: "; // TODO: for each container, report which
                                                                             // lines asked for it...
        for ( const auto& i : containers ) this->debug() << i << " ";
        this->debug() << endmsg;
      }

      DataObjIDColl available;
      DataObjIDColl requested;
      enumerate(
          [&]( int i, const auto& voc ) {
            int nContainers = 0;
            for ( auto const& [j, c] : LHCb::range::enumerate( voc ) ) {
              if ( !c ) continue;
              ++nContainers;
              available.emplace( this->inputLocation( i, j ) );
              if ( auto in = containers.find( this->inputLocation( i, j ) ); in != containers.end() ) {
                requested.emplace( identifier( *c ) ); // some TES implementations do not have the common prefix in
                                                       // their registry identifier...
                containers.erase( in );
              }
            }
            // m_counters.template get< decltype( *voc.begin() ) >().nContainersInput += nContainers;
          },
          0, items... );

      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << "containers available for this event: "; // TODO: for each container, report which
                                                                  // lines asked for it...
        for ( const auto& i : available ) this->debug() << i << " ";
        this->debug() << endmsg;

        for ( auto const& i : containers ) {
          this->debug() << "requested container " << i << " not found -- requested by: ";
          // TODO: figure out which trigger lines asked for this...
          for ( auto const& [k, dr] : *dec ) {
            if ( !dr.decision() ) continue;
            auto j = m_map.find( k );
            if ( j != m_map.end() && j->second.find( i ) != j->second.end() ) this->debug() << k << " ";
          }
          this->debug() << endmsg;
        }
      }

      auto any_fired = [&]( std::vector<std::string> const& lines ) {
        // perhaps figure this out in loop above instead, by checking whether for any positive decision
        // their name is mentioned in one of the 'addXYZ' ?
        return std::any_of( lines.begin(), lines.end(), [&]( const auto& line ) -> bool {
          auto i = dec->find( line );
          if ( i == dec->end() ) {
            throw GaudiException( "requested line not present", __PRETTY_FUNCTION__, StatusCode::FAILURE );
          }
          return i->second.decision();
        } );
      };

// FIXME: C++20: stop ignoring "-Wpedantic" or "-Wc++20-extensions"
#pragma GCC diagnostic push
#if __GNUC__ >= 12
#  pragma GCC diagnostic ignored "-Wc++20-extensions"
#else
#  pragma GCC diagnostic ignored "-Wpedantic"
#endif
      ShoppingList map{*this, requested, m_counters,
                       ShoppingList::Options{.add_track_ancestors  = any_fired( m_addTrackAncestors ),
                                             .add_calo_digits      = any_fired( m_addCaloDigits ),
                                             .add_calo_clusters    = any_fired( m_addCaloClusters ),
                                             .add_tagger_particles = any_fired( m_addTagParticles ),
                                             .add_pv_tracks        = any_fired( m_addPVTracks )},
                       items...};
#pragma GCC diagnostic pop

      Buffer buffer( m_encodingKey );

      std::optional<LHCb::Hlt::PackedData::PackedDataChecksum> checksum;
      if ( m_enableChecksum ) checksum.emplace();

      auto encoder =
          Encoder{requested, *m_annsvc, m_encodingKey, m_stream, Encoder::Anonymize{m_anonymizeDependencies.value()}};
      map.for_each( [&, cksum = LHCb::get_pointer( checksum )]( const auto& c ) {
        c.append_to( buffer, encoder, m_enableCheck, cksum );
      } );
      if ( checksum )
        for ( const auto& x : checksum->checksums() )
          this->info() << "Packed data checksum for '" << x.first << "' = " << std::hex << x.second << endmsg;

      if ( this->msgLevel( MSG::DEBUG ) ) {

        auto m          = std::map<int, LHCb::Hlt::PackedData::ObjectHeader>{};
        auto readBuffer = LHCb::Hlt::PackedData::PackedDataInBuffer( buffer.key() );
        readBuffer.init( buffer.buffer() );
        while ( !readBuffer.eof() ) {
          LHCb::Hlt::PackedData::ObjectHeader header{readBuffer};
          readBuffer.skip( header.storedSize );
          m.emplace( header.locationID, std::move( header ) );
        }
        for ( const auto& [_, header] : m ) {
          this->debug() << "Found CLID=" << header.classID << " locationID=" << header.locationID << " size "
                        << header.storedSize << " with " << header.linkLocationIDs.size() << " links; internal: ";
          const auto&   links = header.linkLocationIDs;
          std::set<int> internal, external;
          std::partition_copy( links.begin(), links.end(), std::inserter( internal, internal.end() ),
                               std::inserter( external, external.end() ),
                               [&m]( const auto& i ) { return m.find( i ) != m.end(); } );
          for ( auto id : internal ) this->debug() << id << " ";
          if ( !external.empty() ) {
            this->debug() << " external: ";
            for ( auto id : external ) this->debug() << id << " ";
          }
          this->debug() << endmsg;
        }
      }

      return buffer;
    }
  };

  // relations get done last, as we may want to prune entries where the 'from' side is not packed
  using SelectivePackerInstance =
      SelectivePacker<LHCb::RecVertexPacker, LHCb::VertexPacker, LHCb::TwoProngVertexPacker, LHCb::RichPIDPacker,
                      LHCb::MuonPIDPacker, LHCb::ProtoParticlePacker, LHCb::ParticlePacker, LHCb::TrackPacker,
                      LHCb::FlavourTagPacker, LHCb::CaloHypoPacker, LHCb::CaloClusterPacker, LHCb::CaloDigitPacker,
                      LHCb::CaloAdcPacker, LHCb::WeightsVectorPacker, LHCb::RecSummaryPacker,
                      LHCb::Packers::ParticleRelation<LHCb::VertexBase>,
                      LHCb::Packers::ParticleRelation<LHCb::MCParticle>, LHCb::Packers::PP2MCPRelation,
                      LHCb::Packers::P2IntRelation, LHCb::Packers::P2InfoRelation>;

  DECLARE_COMPONENT_WITH_ID( SelectivePackerInstance, "LHCb__SelectivePacker" )
  // SOA packers
  DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::SOA::Pack<LHCb::Event::v3::Tracks>, "SOATrackPacker" )
  DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::SOA::Pack<LHCb::Event::Calo::v2::Clusters>, "SOACaloClusterPacker" )
  DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::SOA::Pack<LHCb::Event::Calo::v2::Hypotheses>, "SOACaloHypoPacker" )

} // namespace LHCb
