/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <GaudiKernel/StatusCode.h>

namespace LHCb::Packers {
  enum class ErrorCode : StatusCode::code_t {
    CONTAINER_NOT_FOUND = 10,
    KEY_NOT_FOUND,
    INCONSISTENT_SIZE,
    UNKNOWN_LOCATIONID,
    MISSING_DEPENDENCY,
    WRONG_CONTAINER_TYPE,
    NO_LOADER_FOR_CLID,
    NOT_ALL_DATA_USED,
    MISMATCHED_LINK_ID,
  };

  // TODO: make a 'map' of ErrorCode->Counter (including buffers?)

  struct ErrorCategory : StatusCode::Category {
    const char* name() const override;
    bool        isRecoverable( StatusCode::code_t ) const override;
    std::string message( StatusCode::code_t code ) const override;
  };
} // namespace LHCb::Packers

STATUSCODE_ENUM_DECL( LHCb::Packers::ErrorCode )
