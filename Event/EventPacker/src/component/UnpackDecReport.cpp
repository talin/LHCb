/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/HltDecReports.h"
#include "Event/PackedDecReport.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/LinkManager.h"
#include "LHCbAlgs/Transformer.h"

namespace LHCb {

  /**
   *  Unpacks DecReports
   *
   *  @author Olivier Callot
   *  @date   2012-01-23
   */
  struct UnpackDecReport : Algorithm::Transformer<HltDecReports( PackedDecReport const& )> {

    UnpackDecReport( std::string const& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, KeyValue{"InputName", PackedDecReportLocation::Default},
                       KeyValue{"OutputName", "Strip/Phys/DecReports"} ) {}

    HltDecReports operator()( PackedDecReport const& ) const override;
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::UnpackDecReport, "UnpackDecReport" )

LHCb::HltDecReports LHCb::UnpackDecReport::operator()( LHCb::PackedDecReport const& dst ) const {
  HltDecReports newReport;

  newReport.setConfiguredTCK( dst.configuredTCK() );
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Unpacked TCK = " << newReport.configuredTCK() << endmsg;

  newReport.setTaskID( 0 );
  for ( const auto& R : dst.reports() ) {
    HltDecReport tmp( R );
    auto*        myLink = dst.linkMgr()->link( tmp.intDecisionID() - 1 ); // Was stored with +1.
    if ( !myLink ) {
      info() << "No link table entry for " << tmp.intDecisionID() << endmsg;
    } else {
      const std::string& name = myLink->path();
      tmp.setIntDecisionID( 1 );
      auto sc = newReport.insert( name, tmp );
      if ( !sc ) { error() << "Problem saving " << name << endmsg; }
      if ( msgLevel( MSG::DEBUG ) )
        debug() << format( "restored report %8.8x link ID %3d", tmp.decReport(), myLink->ID() ) << " name " << name
                << endmsg;
    }
  }

  return newReport;
}
