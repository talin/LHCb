/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/FlavourTag.h"
#include "Event/MCParticle.h"
#include "Event/MuonPID.h"
#include "Event/PackedFlavourTag.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedProtoParticle.h"
#include "Event/PackedRecVertex.h"
#include "Event/PackedRelations.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedTrack.h"
#include "Event/PackedVertex.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Event/RecVertex.h"
#include "Event/RelatedInfoMap.h"
#include "Event/RichPID.h"
#include "Event/StandardPacker.h"
#include "Event/Track.h"
#include "Event/Vertex.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Kernel/Particle2LHCbIDs.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"
#include <memory>

//-----------------------------------------------------------------------------
// Implementation file for class : UnpackParticlesAndVertices
//
// 2012-01-23 : Olivier Callot
//-----------------------------------------------------------------------------

/** @class UnpackParticlesAndVertices UnpackParticlesAndVertices.h
 *
 *  Unpacks all Packed Particles and related objects
 *
 *  @author Olivier Callot
 *  @date   2012-01-23
 */
class UnpackParticlesAndVertices : public GaudiAlgorithm {

public:
  /// Standard constructor
  UnpackParticlesAndVertices( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  template <class FROM, class TO, class FROMCONT, class TOCONT>
  void unpackP2PRelations( const std::string& location );

  template <class FROM, class TO, class FROMCONT, class TOCONT, class WEIGHT>
  void unpackP2PWeightedRelations( const std::string& location );

  template <class FROM, class TO, class FROMCONT>
  void unpackP2IntRelations( const std::string& location );

private:
  std::string m_inputStream; ///< Input stream root

  std::string m_postFix;

  /// Track packer
  const LHCb::TrackPacker m_trackPacker{this};
  /// Muon PID packer
  const LHCb::MuonPIDPacker m_muonPacker{this};
  /// Rich PID packer
  const LHCb::RichPIDPacker m_richPacker{this};
  /// ProtoParticle packer
  const LHCb::ProtoParticlePacker m_protoPacker{this};
  /// Particle Packer
  const LHCb::ParticlePacker m_partPacker{this};
  /// Vertex Packer
  const LHCb::VertexPacker m_vertPacker{this};
  /// Flavour Tag Packer
  const LHCb::FlavourTagPacker m_ftPacker{this};
  /// Rec Vertex Packer
  const LHCb::RecVertexPacker m_rvPacker{this};
  /// Related Info Packer
  const LHCb::RelatedInfoRelationsPacker m_rInfoPacker{this};

  mutable Gaudi::Accumulators::StatCounter<> m_unpackedTracks{this, "# UnPacked Tracks"};
  mutable Gaudi::Accumulators::StatCounter<> m_unpackedMuonPIDs{this, "# UnPacked MuonPIDs"};
  mutable Gaudi::Accumulators::StatCounter<> m_unpackedRichPIDs{this, "# UnPacked RichPIDs"};
  mutable Gaudi::Accumulators::StatCounter<> m_unpackedProtoParticles{this, "# UnPacked ProtoParticles"};
  mutable Gaudi::Accumulators::StatCounter<> m_unpackedParticles{this, "# UnPacked Particles"};
  mutable Gaudi::Accumulators::StatCounter<> m_unpackedVertices{this, "# UnPacked Vertices"};
  mutable Gaudi::Accumulators::StatCounter<> m_unpackedFlavourTags{this, "# UnPacked FlavourTags"};
  mutable Gaudi::Accumulators::StatCounter<> m_unpackedRecVertices{this, "# UnPacked RecVertices"};
};

template <class FROM, class TO, class FROMCONT, class TOCONT>
void UnpackParticlesAndVertices::unpackP2PRelations( const std::string& location ) {
  typedef LHCb::Relation1D<FROM, TO> RELATION;

  unsigned int nbRelContainer( 0 ), nbRel( 0 );

  RELATION*                    rels  = nullptr;
  const LHCb::PackedRelations* prels = getIfExists<LHCb::PackedRelations>( location );
  if ( nullptr != prels ) {
    for ( const auto& prel : prels->data() ) {
      int                indx          = prel.container >> 32;
      const std::string& containerName = prels->linkMgr()->link( indx )->path() + m_postFix;
      rels                             = new RELATION();
      rels->setVersion( prels->version() );
      put( rels, containerName );
      ++nbRelContainer;
      FROMCONT*   srcContainer = nullptr;
      int         prevSrcLink  = -1;
      DataObject* dstContainer = nullptr;
      int         prevDstLink  = -1;
      for ( int kk = prel.start; prel.end > kk; ++kk ) {
        int srcLink( 0 ), srcKey( 0 );
        StandardPacker::indexAndKey64( prels->sources()[kk], srcLink, srcKey );
        if ( srcLink != prevSrcLink || !srcContainer ) {
          prevSrcLink                = srcLink;
          const std::string& srcName = prels->linkMgr()->link( srcLink )->path();
          // srcContainer = get<FROMCONT>( srcName );
          srcContainer = getIfExists<FROMCONT>( srcName );
          if ( !srcContainer ) {
            Error( "Unpack('" + location + "'): missing source '" + srcName + "', skip link" ).ignore();
            continue;
          }
        }
        FROM* from = srcContainer->object( srcKey );
        int   dstLink( 0 ), dstKey( 0 );
        StandardPacker::indexAndKey64( prels->dests()[kk], dstLink, dstKey );
        if ( dstLink != prevDstLink || !dstContainer ) {
          prevDstLink                = dstLink;
          const std::string& dstName = prels->linkMgr()->link( dstLink )->path();
          // dstContainer = get<DataObject>( dstName );
          dstContainer = getIfExists<DataObject>( dstName );
          if ( !dstContainer ) {
            Error( "Unpack('" + location + "'): missing destination '" + dstName + "', skip link" ).ignore();
            continue;
          }
        }
        TOCONT* _to = dynamic_cast<TOCONT*>( dstContainer );
        TO*     to  = ( _to ? _to->object( dstKey ) : nullptr );
        if ( !to )
          info() << "Unknown objec: Container type " << ( dstContainer->clID() >> 16 ) << "+"
                 << ( dstContainer->clID() & 0xFFFF ) << " key " << dstKey << endmsg;
        auto sc = rels->relate( from, to );
        if ( !sc ) { Error( "Problem forming relation" ).ignore(); }
        ++nbRel;
      }
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Retrieved " << nbRel << " relations in " << nbRelContainer << " containers"
            << " from " << location << endmsg;
  }
}

template <class FROM, class TO, class FROMCONT, class TOCONT, class WEIGHT>
void UnpackParticlesAndVertices::unpackP2PWeightedRelations( const std::string& location ) {
  typedef LHCb::RelationWeighted1D<FROM, TO, WEIGHT> RELATION;

  unsigned int nbRelContainer( 0 ), nbRel( 0 );

  RELATION*                            rels  = nullptr;
  const LHCb::PackedWeightedRelations* prels = getIfExists<LHCb::PackedWeightedRelations>( location );
  if ( nullptr != prels ) {
    for ( const auto& prel : prels->data() ) {
      int                indx          = prel.container >> 32;
      const std::string& containerName = prels->linkMgr()->link( indx )->path() + m_postFix;
      rels                             = new RELATION();
      rels->setVersion( prels->version() );
      put( rels, containerName );
      ++nbRelContainer;
      FROMCONT*   srcContainer = nullptr;
      int         prevSrcLink  = -1;
      DataObject* dstContainer = nullptr;
      int         prevDstLink  = -1;
      for ( int kk = prel.start; prel.end > kk; ++kk ) {
        int srcLink( 0 ), srcKey( 0 );
        StandardPacker::indexAndKey64( prels->sources()[kk], srcLink, srcKey );
        if ( srcLink != prevSrcLink || !srcContainer ) {
          prevSrcLink                = srcLink;
          const std::string& srcName = prels->linkMgr()->link( srcLink )->path();
          srcContainer               = getIfExists<FROMCONT>( srcName );
          if ( !srcContainer ) {
            Error( "Unpack('" + location + "'): missing source '" + srcName + "', skip link" ).ignore();
            continue;
          }
        }
        FROM* from = srcContainer->object( srcKey );
        int   dstLink( 0 ), dstKey( 0 );
        StandardPacker::indexAndKey64( prels->dests()[kk], dstLink, dstKey );
        if ( dstLink != prevDstLink || !dstContainer ) {
          prevDstLink                = dstLink;
          const std::string& dstName = prels->linkMgr()->link( dstLink )->path();
          dstContainer               = getIfExists<DataObject>( dstName );
          if ( !dstContainer ) {
            Error( "Unpack('" + location + "'): missing destination '" + dstName + "', skip link" ).ignore();
            continue;
          }
        }
        const WEIGHT wgt = prels->weights()[kk];
        TOCONT*      _to = dynamic_cast<TOCONT*>( dstContainer );
        TO*          to  = ( _to ? _to->object( dstKey ) : nullptr );
        if ( !to )
          info() << "Unknown objec: Container type " << ( dstContainer->clID() >> 16 ) << "+"
                 << ( dstContainer->clID() & 0xFFFF ) << " key " << dstKey << endmsg;
        auto sc = rels->relate( from, to, wgt );
        if ( !sc ) { Error( "Problem forming weighted relation" ).ignore(); }
        ++nbRel;
      }
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Retrieved " << nbRel << " relations in " << nbRelContainer << " containers"
            << " from " << location << endmsg;
  }
}

template <class FROM, class TO, class FROMCONT>
void UnpackParticlesAndVertices::unpackP2IntRelations( const std::string& location ) {
  typedef LHCb::Relation1D<FROM, TO> RELATION;

  unsigned int nbRelContainer( 0 ), nbRel( 0 );

  LHCb::PackedRelations* prels = getIfExists<LHCb::PackedRelations>( location );
  if ( prels ) {
    for ( const LHCb::PackedRelation& prel : prels->data() ) {
      const int          indx          = prel.container >> 32;
      const std::string& containerName = prels->linkMgr()->link( indx )->path() + m_postFix;
      RELATION*          rels          = new RELATION();
      rels->setVersion( prels->version() );
      put( rels, containerName );
      ++nbRelContainer;
      FROMCONT* srcContainer = nullptr;
      int       prevSrcLink  = -1;
      for ( int kk = prel.start; prel.end > kk; ++kk ) {
        int srcLink( 0 ), srcKey( 0 );
        StandardPacker::indexAndKey64( prels->sources()[kk], srcLink, srcKey );
        if ( srcLink != prevSrcLink ) {
          prevSrcLink                = srcLink;
          const std::string& srcName = prels->linkMgr()->link( srcLink )->path();
          srcContainer               = get<FROMCONT>( srcName );
        }
        FROM* from = srcContainer->object( srcKey );
        TO    to   = (TO)prels->dests()[kk];
        auto  sc   = rels->relate( from, to );
        if ( !sc ) { Error( "Problem forming relation" ).ignore(); }
        ++nbRel;
      }
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Retrieved " << nbRel << " relations in " << nbRelContainer << " containers"
            << " from " << location << endmsg;
  }
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UnpackParticlesAndVertices::UnpackParticlesAndVertices( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {
  declareProperty( "InputStream", m_inputStream = "/Event/" );
  declareProperty( "PostFix", m_postFix = "" );
  // setProperty( "OutputLevel", 1 );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode UnpackParticlesAndVertices::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  //=================================================================
  //== Process the Tracks
  //=================================================================
  {
    int          prevLink = -1;
    unsigned int nbPartContainer( 0 ), nbPart( 0 );
    auto*        ptracks = getIfExists<LHCb::PackedTracks>( m_inputStream + LHCb::PackedTrackLocation::InStream );
    if ( ptracks ) {
      m_trackPacker.resetWrappingCounts();
      LHCb::Tracks* tracks = nullptr;
      for ( const LHCb::PackedTrack& ptrack : ptracks->data() ) {
        int key( 0 ), linkID( 0 );
        StandardPacker::indexAndKey64( ptrack.key, linkID, key );
        if ( linkID != prevLink ) {
          prevLink                         = linkID;
          const std::string& containerName = ptracks->linkMgr()->link( linkID )->path() + m_postFix;
          // Check to see if container already exists. If it does, unpacking has already been run this
          // event so quit
          if ( exist<LHCb::Tracks>( containerName ) ) {
            if ( msgLevel( MSG::DEBUG ) ) debug() << " -> " << containerName << " exists" << endmsg;
            return StatusCode::SUCCESS;
          }
          tracks = new LHCb::Tracks();
          put( tracks, containerName );
          ++nbPartContainer;
        }

        // Make new object and insert into the output container
        LHCb::Track* track = new LHCb::Track();
        tracks->insert( track, key );
        ++nbPart;

        // Unpack the physics info
        m_trackPacker.unpack( ptrack, *track, *ptracks, *tracks );

      } // loop over packed tracks
    }
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Retrieved " << nbPart << " Tracks in " << nbPartContainer << " containers" << endmsg;
    }
    m_unpackedTracks += nbPart;
  }

  //=================================================================
  //== Process the MuonPIDs
  //=================================================================
  {
    int          prevLink = -1;
    unsigned int nbPartContainer( 0 ), nbPart( 0 );
    auto*        ppids = getIfExists<LHCb::PackedMuonPIDs>( m_inputStream + LHCb::PackedMuonPIDLocation::InStream );
    if ( ppids ) {
      LHCb::MuonPIDs* pids = nullptr;
      for ( const LHCb::PackedMuonPID& ppid : ppids->data() ) {
        int key( 0 ), linkID( 0 );
        StandardPacker::indexAndKey64( ppid.key, linkID, key );
        if ( linkID != prevLink ) {
          prevLink                         = linkID;
          const std::string& containerName = ppids->linkMgr()->link( linkID )->path() + m_postFix;
          // Check to see if container already exists. If it does, unpacking has already been run this
          // event so quit
          if ( exist<LHCb::MuonPIDs>( containerName ) ) {
            if ( msgLevel( MSG::DEBUG ) ) debug() << " -> " << containerName << " exists" << endmsg;
            return StatusCode::SUCCESS;
          }
          pids = new LHCb::MuonPIDs();
          pids->setVersion( ppids->version() );
          put( pids, containerName );
          ++nbPartContainer;
        }

        // Make new object and insert into the output container
        LHCb::MuonPID* pid = new LHCb::MuonPID();
        pids->insert( pid, key );
        ++nbPart;

        // Unpack the physics info
        m_muonPacker.unpack( ppid, *pid, *ppids, *pids );
      }
    }
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Retrieved " << nbPart << " MuonPIDs in " << nbPartContainer << " containers" << endmsg;
    }
    m_unpackedMuonPIDs += nbPart;
  }

  //=================================================================
  //== Process the RichPIDs
  //=================================================================
  {
    int          prevLink = -1;
    unsigned int nbPartContainer( 0 ), nbPart( 0 );
    auto*        ppids = getIfExists<LHCb::PackedRichPIDs>( m_inputStream + LHCb::PackedRichPIDLocation::InStream );
    if ( ppids ) {
      LHCb::RichPIDs* pids = nullptr;
      for ( const LHCb::PackedRichPID& ppid : ppids->data() ) {
        int key( 0 ), linkID( 0 );
        StandardPacker::indexAndKey64( ppid.key, linkID, key );
        if ( linkID != prevLink ) {
          prevLink                         = linkID;
          const std::string& containerName = ppids->linkMgr()->link( linkID )->path() + m_postFix;
          // Check to see if container already exists. If it does, unpacking has already been run this
          // event so quit
          if ( exist<LHCb::RichPIDs>( containerName ) ) {
            if ( msgLevel( MSG::DEBUG ) ) debug() << " -> " << containerName << " exists" << endmsg;
            return StatusCode::SUCCESS;
          }
          pids = new LHCb::RichPIDs();
          pids->setVersion( ppids->version() );
          put( pids, containerName );
          ++nbPartContainer;
        }

        // Make new object and insert into the output container
        LHCb::RichPID* pid = new LHCb::RichPID();
        pids->insert( pid, key );
        ++nbPart;

        // Unpack the physics info
        m_richPacker.unpack( ppid, *pid, *ppids, *pids );
      }
    }
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Retrieved " << nbPart << " RichPIDs in " << nbPartContainer << " containers" << endmsg;
    }
    m_unpackedRichPIDs += nbPart;
  }

  //=================================================================
  //== Process the ProtoParticles
  //=================================================================
  {
    int          prevLink = -1;
    unsigned int nbPartContainer( 0 ), nbPart( 0 );
    auto*        pprotos =
        getIfExists<LHCb::PackedProtoParticles>( m_inputStream + LHCb::PackedProtoParticleLocation::InStream );
    if ( pprotos ) {
      LHCb::ProtoParticles* protos = nullptr;
      LHCb::Packer::Carry   carry{};
      for ( const LHCb::PackedProtoParticle& pproto : pprotos->data() ) {
        int key( 0 ), linkID( 0 );
        StandardPacker::indexAndKey64( pproto.key, linkID, key );
        if ( linkID != prevLink ) {
          prevLink                         = linkID;
          const std::string& containerName = pprotos->linkMgr()->link( linkID )->path() + m_postFix;
          // Check to see if container already exists. If it does, unpacking has already been run this
          // event so quit
          if ( exist<LHCb::ProtoParticles>( containerName ) ) {
            if ( msgLevel( MSG::DEBUG ) ) debug() << " -> " << containerName << " exists" << endmsg;
            return StatusCode::SUCCESS;
          }
          protos = new LHCb::ProtoParticles();
          put( protos, containerName );
          ++nbPartContainer;
        }

        // Make new object and insert into the output container
        LHCb::ProtoParticle* proto = new LHCb::ProtoParticle();
        protos->insert( proto, key );
        ++nbPart;

        // Unpack the physics info
        m_protoPacker.unpack( pproto, *proto, *pprotos, *protos, carry );
      }
    }
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Retrieved " << nbPart << " ProtoParticles in " << nbPartContainer << " containers" << endmsg;
    }
    m_unpackedProtoParticles += nbPart;
  }

  //=================================================================
  //== Process the Particles
  //=================================================================
  {
    int          prevLink = -1;
    unsigned int nbPartContainer( 0 ), nbPart( 0 );
    auto*        pparts = getIfExists<LHCb::PackedParticles>( m_inputStream + LHCb::PackedParticleLocation::InStream );
    if ( pparts ) {
      LHCb::Particles*    parts = nullptr;
      LHCb::Packer::Carry carry{};
      for ( const LHCb::PackedParticle& ppart : pparts->data() ) {
        int key( 0 ), linkID( 0 );
        StandardPacker::indexAndKey64( ppart.key, linkID, key );
        if ( linkID != prevLink ) {
          prevLink                         = linkID;
          const std::string& containerName = pparts->linkMgr()->link( linkID )->path() + m_postFix;
          // Check to see if container alrady exists. If it does, unpacking has already been run this
          // event so quit
          if ( exist<LHCb::Particles>( containerName ) ) {
            if ( msgLevel( MSG::DEBUG ) ) debug() << " -> " << containerName << " exists" << endmsg;
            return StatusCode::SUCCESS;
          }
          parts = new LHCb::Particles();
          parts->setVersion( pparts->version() );
          put( parts, containerName );
          ++nbPartContainer;
        }

        // Make new object and insert into the output container
        LHCb::Particle* part = new LHCb::Particle();
        parts->insert( part, key );
        ++nbPart;

        // Unpack the physics info
        m_partPacker.unpack( ppart, *part, *pparts, *parts, carry );
      }
    }
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Retrieved " << nbPart << " Particles in " << nbPartContainer << " containers" << endmsg;
    }
    m_unpackedParticles += nbPart;
  }

  //=================================================================
  //== Process the vertices
  //=================================================================
  {
    unsigned int nbVertContainer( 0 ), nbVert( 0 );
    int          prevLink = -1;
    auto*        pverts   = getIfExists<LHCb::PackedVertices>( m_inputStream + LHCb::PackedVertexLocation::InStream );
    if ( pverts ) {
      LHCb::Vertices* verts = nullptr;
      for ( const LHCb::PackedVertex& pvert : pverts->data() ) {
        int key( 0 ), linkID( 0 );
        StandardPacker::indexAndKey64( pvert.key, linkID, key );
        if ( linkID != prevLink ) {
          prevLink                         = linkID;
          const std::string& containerName = pverts->linkMgr()->link( linkID )->path() + m_postFix;
          // Check to see if container already exists. If it does, unpacking has already been run this
          // event so quit
          if ( exist<LHCb::Vertices>( containerName ) ) {
            if ( msgLevel( MSG::DEBUG ) ) debug() << " -> " << containerName << " exists" << endmsg;
            return StatusCode::SUCCESS;
          }
          verts = new LHCb::Vertices();
          verts->setVersion( pverts->version() );
          put( verts, containerName );
          ++nbVertContainer;
        }
        //== Construct with verticle ID and key.
        LHCb::Vertex* vert = new LHCb::Vertex();
        verts->insert( vert, key );
        ++nbVert;

        // unpack the physics info
        m_vertPacker.unpack( pvert, *vert, *pverts, *verts );
      }
    }
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Retrieved " << nbVert << " vertices in " << nbVertContainer << " containers" << endmsg;
    }
    m_unpackedVertices += nbVert;
  }

  //=================================================================
  //== Process the Flavour Tags
  //=================================================================
  {
    unsigned int nbFTContainer( 0 ), nbFT( 0 );
    int          prevLink = -1;
    auto* pfts = getIfExists<LHCb::PackedFlavourTags>( m_inputStream + LHCb::PackedFlavourTagLocation::InStream );
    if ( pfts ) {
      LHCb::FlavourTags* fts = nullptr;
      for ( const LHCb::PackedFlavourTag& pft : pfts->data() ) {
        int key( 0 ), linkID( 0 );
        StandardPacker::indexAndKey64( pft.key, linkID, key );
        if ( linkID != prevLink ) {
          prevLink                         = linkID;
          const std::string& containerName = pfts->linkMgr()->link( linkID )->path() + m_postFix;
          // Check to see if container alrady exists. If it does, unpacking has already been run this
          // event so quit
          if ( exist<LHCb::FlavourTags>( containerName ) ) {
            if ( msgLevel( MSG::DEBUG ) ) debug() << " -> " << containerName << " exists" << endmsg;
            return StatusCode::SUCCESS;
          }
          fts = new LHCb::FlavourTags();
          fts->setVersion( pfts->version() );
          put( fts, containerName );
          ++nbFTContainer;
        }

        // Make new object and insert into the output container
        LHCb::FlavourTag* ft = new LHCb::FlavourTag();
        fts->insert( ft, key );
        ++nbFT;

        // Unpack the physics info
        m_ftPacker.unpack( pft, *ft, *pfts, *fts );
      }
    }
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Retrieved " << nbFT << " FlavourTags in " << nbFTContainer << " containers" << endmsg;
    }
    m_unpackedFlavourTags += nbFT;
  }

  //=================================================================
  //== Process the RecVertices
  //=================================================================
  {
    unsigned int nbRecVertContainer( 0 ), nbRecVert( 0 );
    int          prevLink = -1;
    auto* pRecVerts = getIfExists<LHCb::PackedRecVertices>( m_inputStream + LHCb::PackedRecVertexLocation::InStream );
    if ( pRecVerts ) {
      LHCb::RecVertices* recVerts = nullptr;
      for ( const LHCb::PackedRecVertex& pRecVert : pRecVerts->data() ) {
        const int key    = pRecVert.key;
        const int linkID = pRecVert.container;
        if ( linkID != prevLink ) {
          prevLink                         = linkID;
          const std::string& containerName = pRecVerts->linkMgr()->link( linkID )->path() + m_postFix;
          // Check to see if container already exists. If it does, unpacking has already been run this
          // event so quit
          if ( exist<LHCb::RecVertices>( containerName ) ) {
            if ( msgLevel( MSG::DEBUG ) ) debug() << " -> " << containerName << " exists" << endmsg;
            return StatusCode::SUCCESS;
          }
          recVerts = new LHCb::RecVertices();
          recVerts->setVersion( pRecVerts->version() );
          put( recVerts, containerName );
          ++nbRecVertContainer;
        }

        //== Construct with RecVertex ID and key.
        LHCb::RecVertex* recVert = new LHCb::RecVertex( key );
        recVerts->add( recVert );
        ++nbRecVert;

        // Physics Info
        m_rvPacker.unpack( pRecVert, *recVert, *pRecVerts, *recVerts );
      }
    }
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Retrieved " << nbRecVert << " RecVertices in " << nbRecVertContainer << " containers" << endmsg;
    }
    m_unpackedRecVertices += nbRecVert;
  }

  //=================================================================
  //== Process the P2V relations
  //=================================================================
  unpackP2PRelations<LHCb::Particle, LHCb::VertexBase, LHCb::Particles, const LHCb::RecVertices>(
      m_inputStream + LHCb::PackedRelationsLocation::InStream );

  //=================================================================
  //== Process the P2MCP relations
  //=================================================================
  unpackP2PRelations<LHCb::Particle, LHCb::MCParticle, LHCb::Particles, const LHCb::MCParticles>(
      m_inputStream + LHCb::PackedRelationsLocation::P2MCP );

  //=================================================================
  //== Process the PP2MCP relations
  //=================================================================
  unpackP2PWeightedRelations<LHCb::ProtoParticle, LHCb::MCParticle, LHCb::ProtoParticles, const LHCb::MCParticles,
                             double>( m_inputStream + LHCb::PackedWeightedRelationsLocation::PP2MCP );

  //=================================================================
  //== Process the P2Int relations
  //=================================================================
  unpackP2IntRelations<LHCb::Particle, int, LHCb::Particles>( m_inputStream + LHCb::PackedRelationsLocation::P2Int );

  //=================================================================
  //== Process the Related info relations
  //=================================================================
  {
    typedef LHCb::Particle             FROM;
    typedef LHCb::RelatedInfoMap       TO;
    typedef LHCb::Relation1D<FROM, TO> RELATION;

    // Count data objects recreated
    unsigned int nbRelContainer( 0 ), nbRel( 0 );

    // Location of the packed data
    const std::string location = m_inputStream + LHCb::PackedRelatedInfoLocation::InStream;

    // do we have any packed data
    LHCb::PackedRelatedInfoRelations* prels = getIfExists<LHCb::PackedRelatedInfoRelations>( location );
    if ( nullptr != prels ) {
      // Loop over the different TES containers that where saved
      for ( const auto& cont : prels->containers() ) {
        // Reconstruct container name for this entry
        const int          indx          = cont.reference >> 32;
        const std::string& containerName = prels->linkMgr()->link( indx )->path() + m_postFix;

        // Check to see if container already exists. If it does, unpacking has already been run this
        // event so quit
        if ( exist<RELATION>( containerName ) ) {
          if ( msgLevel( MSG::DEBUG ) ) debug() << " -> " << containerName << " exists" << endmsg;
          return StatusCode::SUCCESS;
        }

        // Create a new unpacked object at the TES location and save
        RELATION* rels = new RELATION();
        rels->setVersion( prels->version() );
        put( rels, containerName );

        // Loop over the relations saved at this container location
        for ( unsigned int kk = cont.first; cont.last > kk; ++kk ) {
          // The relation information
          const auto& rel = prels->relations()[kk];

          // unpack this one entry
          m_rInfoPacker.unpack( rel, *prels, *rels );

          // Count
          ++nbRel;
        }

        // Count containers
        ++nbRelContainer;
      }
    }

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Retrieved " << nbRel << " RelatedInfo relations in " << nbRelContainer << " containers"
              << " from " << location << endmsg;
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "... Execute finished." << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( UnpackParticlesAndVertices )

//=============================================================================
