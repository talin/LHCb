/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/RegistryEntry.h"
#include <string>
#include <type_traits>

namespace DataPacking::Buffer {

  template <typename Data>
  class RegistryWrapper {
    DataSvcHelpers::RegistryEntry m_reg;
    Data                          m_data;

  public:
    template <typename... Args, typename = std::enable_if_t<std::is_constructible_v<Data, Args...>>>
    RegistryWrapper( std::string location, Args&&... args )
        : m_reg{std::move( location ), nullptr}, m_data{std::forward<Args>( args )...} {
      m_data.setRegistry( &m_reg );
    }
    ~RegistryWrapper() { m_data.setRegistry( nullptr ); }

    RegistryWrapper( RegistryWrapper&& ) = default;
    RegistryWrapper& operator=( RegistryWrapper&& ) = default;

    Data*       operator->() { return &m_data; }
    Data const* operator->() const { return &m_data; }
    Data&       operator*() { return m_data; }
    Data const& operator*() const { return m_data; }
  };
} // namespace DataPacking::Buffer
