/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedTwoProngVertex.h"
#include "Event/StandardPacker.h"
#include "Event/TwoProngVertex.h"

#include "LHCbAlgs/Consumer.h"

namespace LHCb {

  /**
   *  Unpack the Two Prong vertex class
   *
   *  Note that the inheritance from Consumer is misleading. The algorithm is
   *  writing to TES, just via a Handle so that it can do it at the begining of
   *  the operator(), as cross pointers are used in the TES and requires this.
   *
   *  @author Olivier Callot
   *  @date   2009-01-21
   */
  struct UnpackTwoProngVertex : Algorithm::Consumer<void( PackedTwoProngVertices const& )> {

    UnpackTwoProngVertex( std::string const& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, KeyValue{"InputName", PackedTwoProngVertexLocation::Default} ) {}

    DataObjectWriteHandle<TwoProngVertices> m_vertices{this, "OutputName", TwoProngVertexLocation::Default};

    void operator()( PackedTwoProngVertices const& ) const override;
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::UnpackTwoProngVertex, "UnpackTwoProngVertex" )

void LHCb::UnpackTwoProngVertex::operator()( LHCb::PackedTwoProngVertices const& dst ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Size of PackedTwoProngVertices = " << dst.data().size() << endmsg;

  // NOTE: the output container _must be on the TES_ prior to passing it to the
  //      unpacker, as otherwise filling the references to other objects does
  //      not work
  auto* newTwoProngVertices = m_vertices.put( std::make_unique<TwoProngVertices>() );
  newTwoProngVertices->reserve( dst.data().size() );

  // packing version
  const auto pVer = dst.packingVersion();

  for ( const auto& src : dst.data() ) {

    auto* vert = new TwoProngVertex();
    newTwoProngVertices->insert( vert, src.key );

    vert->setTechnique( (RecVertex::RecVertexType)src.technique );
    vert->setChi2AndDoF( StandardPacker::fltPacked( src.chi2 ), src.nDoF );
    vert->setPosition( Gaudi::XYZPoint( StandardPacker::position( src.x ), StandardPacker::position( src.y ),
                                        StandardPacker::position( src.z ) ) );

    //== Store the Tracks
    int hintID{0};
    int key{0};
    for ( auto ref : Packer::subrange( dst.refs(), src.firstTrack, src.lastTrack ) ) {
      if ( ( 0 == pVer && StandardPacker::hintAndKey32( ref, &dst, newTwoProngVertices, hintID, key ) ) ||
           ( 0 != pVer && StandardPacker::hintAndKey64( ref, &dst, newTwoProngVertices, hintID, key ) ) ) {
        SmartRef<Track> ref( newTwoProngVertices, hintID, key );
        vert->addToTracks( ref );
      } else {
        error() << "Corrupt TwoProngVertex Track SmartRef detected." << endmsg;
      }
    }

    //== Handles the ExtraInfo
    for ( const auto& [k, v] : Packer::subrange( dst.extras(), src.firstInfo, src.lastInfo ) ) {
      vert->addInfo( k, StandardPacker::fltPacked( v ) );
    }

    //== Momentum of the two prongs
    const auto pA = StandardPacker::energy( src.pA );
    const auto pB = StandardPacker::energy( src.pB );
    vert->setMomA( ROOT::Math::SVector<double, 3>( StandardPacker::slope( src.txA ), StandardPacker::slope( src.tyA ),
                                                   1.0 / pA ) );
    vert->setMomB( ROOT::Math::SVector<double, 3>( StandardPacker::slope( src.txB ), StandardPacker::slope( src.tyB ),
                                                   1.0 / pB ) );

    // convariance Matrix
    const auto err0 = StandardPacker::position( src.cov00 );
    const auto err1 = StandardPacker::position( src.cov11 );
    const auto err2 = StandardPacker::position( src.cov22 );
    const auto err3 = StandardPacker::slope( src.cov33 );
    const auto err4 = StandardPacker::slope( src.cov44 );
    const auto err5 = StandardPacker::energy( src.cov55 ) / std::abs( pA ) * 1.e-5;
    const auto err6 = StandardPacker::slope( src.cov66 );
    const auto err7 = StandardPacker::slope( src.cov77 );
    const auto err8 = StandardPacker::energy( src.cov88 ) / std::abs( pB ) * 1.e-5;

    Gaudi::SymMatrix3x3 cov;
    cov( 0, 0 ) = err0 * err0;
    cov( 1, 0 ) = err1 * err0 * StandardPacker::fraction( src.cov10 );
    cov( 1, 1 ) = err1 * err1;
    cov( 2, 0 ) = err2 * err0 * StandardPacker::fraction( src.cov20 );
    cov( 2, 1 ) = err2 * err1 * StandardPacker::fraction( src.cov21 );
    cov( 2, 2 ) = err2 * err2;
    vert->setCovMatrix( cov );

    cov( 0, 0 )     = err3 * err3;
    cov( 1, 0 )     = err4 * err3 * StandardPacker::fraction( src.cov43 );
    cov( 1, 1 )     = err4 * err4;
    cov( 2, 0 )     = err5 * err3 * StandardPacker::fraction( src.cov53 );
    cov( 2, 1 )     = err5 * err4 * StandardPacker::fraction( src.cov54 );
    cov( 2, 2 )     = err5 * err5;
    vert->momcovA() = cov;

    cov( 0, 0 )     = err6 * err6;
    cov( 1, 0 )     = err7 * err6 * StandardPacker::fraction( src.cov76 );
    cov( 1, 1 )     = err7 * err7;
    cov( 2, 0 )     = err8 * err6 * StandardPacker::fraction( src.cov86 );
    cov( 2, 1 )     = err8 * err7 * StandardPacker::fraction( src.cov87 );
    cov( 2, 2 )     = err8 * err8;
    vert->momcovB() = cov;

    ROOT::Math::SMatrix<double, 3, 3> mat;
    mat( 0, 0 )        = err3 * err0 * StandardPacker::fraction( src.cov30 );
    mat( 0, 1 )        = err3 * err1 * StandardPacker::fraction( src.cov31 );
    mat( 0, 2 )        = err3 * err2 * StandardPacker::fraction( src.cov32 );
    mat( 1, 0 )        = err4 * err0 * StandardPacker::fraction( src.cov40 );
    mat( 1, 1 )        = err4 * err1 * StandardPacker::fraction( src.cov41 );
    mat( 1, 2 )        = err4 * err2 * StandardPacker::fraction( src.cov42 );
    mat( 2, 0 )        = err5 * err0 * StandardPacker::fraction( src.cov50 );
    mat( 2, 1 )        = err5 * err1 * StandardPacker::fraction( src.cov51 );
    mat( 2, 2 )        = err5 * err2 * StandardPacker::fraction( src.cov52 );
    vert->momposcovA() = mat;

    mat( 0, 0 )        = err6 * err0 * StandardPacker::fraction( src.cov60 );
    mat( 0, 1 )        = err6 * err1 * StandardPacker::fraction( src.cov61 );
    mat( 0, 2 )        = err6 * err2 * StandardPacker::fraction( src.cov62 );
    mat( 1, 0 )        = err7 * err0 * StandardPacker::fraction( src.cov70 );
    mat( 1, 1 )        = err7 * err1 * StandardPacker::fraction( src.cov71 );
    mat( 1, 2 )        = err7 * err2 * StandardPacker::fraction( src.cov72 );
    mat( 2, 0 )        = err8 * err0 * StandardPacker::fraction( src.cov80 );
    mat( 2, 1 )        = err8 * err1 * StandardPacker::fraction( src.cov81 );
    mat( 2, 2 )        = err8 * err2 * StandardPacker::fraction( src.cov82 );
    vert->momposcovB() = mat;

    mat( 0, 0 )       = err6 * err3 * StandardPacker::fraction( src.cov63 );
    mat( 0, 1 )       = err6 * err4 * StandardPacker::fraction( src.cov64 );
    mat( 0, 2 )       = err6 * err5 * StandardPacker::fraction( src.cov65 );
    mat( 1, 0 )       = err7 * err3 * StandardPacker::fraction( src.cov73 );
    mat( 1, 1 )       = err7 * err4 * StandardPacker::fraction( src.cov74 );
    mat( 1, 2 )       = err7 * err5 * StandardPacker::fraction( src.cov75 );
    mat( 2, 0 )       = err8 * err3 * StandardPacker::fraction( src.cov83 );
    mat( 2, 1 )       = err8 * err4 * StandardPacker::fraction( src.cov84 );
    mat( 2, 2 )       = err8 * err5 * StandardPacker::fraction( src.cov85 );
    vert->mommomcov() = mat;

    //== Unpack the ParticleID
    std::vector<ParticleID> pids;
    for ( auto i : Packer::subrange( dst.refs(), src.firstPid, src.lastPid ) ) pids.emplace_back( i );
    vert->setCompatiblePIDs( pids );
  }
}
