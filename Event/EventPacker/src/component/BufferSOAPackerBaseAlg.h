/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/IIndexedANNSvc.h"
#include "LHCbAlgs/MergingTransformer.h"
#include "RegistryWrapper.h"

#include "Event/PackedData.h"
#include "Event/PackedDataBuffer.h"
#include "Event/PackedDataChecksum.h"
#include "Event/RawEvent.h"
#include "RZip.h"
#include <optional>

/**
 *  Templated base algorithm for all SOA packing algorithms
 **/

namespace DataPacking::Buffer::SOA {

  template <class PACKER>
  class Pack final : public LHCb::Algorithm::MergingTransformer<LHCb::Hlt::PackedData::PackedDataOutBuffer(
                         Gaudi::Functional::vector_of_const_<PACKER*> const& )> {

    using Buffer = LHCb::Hlt::PackedData::PackedDataOutBuffer;

  public:
    // Standard constructor
    Pack( const std::string& name, ISvcLocator* pSvcLocator )
        : LHCb::Algorithm::MergingTransformer<LHCb::Hlt::PackedData::PackedDataOutBuffer(
              Gaudi::Functional::vector_of_const_<PACKER*> const& )>( name, pSvcLocator, {"InputName", {}},
                                                                      {"OutputName", ""} ) {}

    Buffer operator()( Gaudi::Functional::vector_of_const_<PACKER*> const& inputs ) const override {

      if ( m_encodingKey == 0u ) {
        throw GaudiException( "encoding key is zero", __PRETTY_FUNCTION__, StatusCode::FAILURE );
        ++m_zero_key;
      }

      static const Gaudi::StringKey PackedObjectLocations{"PackedObjectLocations"};
      Buffer                        bigBuffer( m_encodingKey );

      for ( auto const& [i, data] : LHCb::range::enumerate( inputs ) ) {

        if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "Inputs  " << i << " " << data->size() << endmsg;
        if ( data && data->size() > 0 ) {

          Buffer buffer( m_encodingKey );
          buffer.reserve( data->size() );

          const auto& map  = m_annsvc->s2i( m_encodingKey, PackedObjectLocations );
          auto        iloc = map.find( this->inputLocation( i ) );
          if ( iloc == map.end() ) {
            this->error() << "could not locate packedobjectlocation " << this->inputLocation( i )
                          << " in table with key " << m_encodingKey.value() << endmsg;
            throw GaudiException( "unknown location", __PRETTY_FUNCTION__, StatusCode::FAILURE );
          }

          auto locationID = iloc->second;
          buffer.save<uint32_t>( data->clID() );
          buffer.save<int32_t>( locationID );
          unsigned int nlinks = 0;
          buffer.saveSize( nlinks );

          auto oSize1 = buffer.saveSize( 0 );                     // get position from this one
          auto oSize2 = buffer.save( *data );                     // get size from this one
          buffer.saveAt<uint32_t>( oSize2.second, oSize1.first ); // save the correct size of the object

          if ( m_enableCheck ) {
            PACKER                                    unpacked;
            LHCb::Hlt::PackedData::PackedDataInBuffer test_buffer{m_encodingKey};
            test_buffer.init( buffer.buffer(), false );

            while ( !test_buffer.eof() ) {
              LHCb::Hlt::PackedData::ObjectHeader header{test_buffer};

              std::size_t nbytes = test_buffer.load( unpacked );

              if ( nbytes != header.storedSize )
                this->error() << "Unpacking check failed "
                              << "saved size does not match the unpacked object " << nbytes << "!=" << header.storedSize
                              << endmsg;

              if ( !data->checkEqual( unpacked ) ) {
                this->warning() << "Unpacking check failed " << endmsg;
                this->warning() << "Original "
                                << " classID " << data->clID() << " locationID " << locationID << " linkLocationIDs "
                                << nlinks << " storedSize " << oSize2.second << endmsg;

                this->warning() << "Unpacking check "
                                << " classID " << header.classID << " locationID " << header.locationID
                                << " linkLocationIDs " << header.linkLocationIDs << " storedSize " << header.storedSize
                                << endmsg;
              }
            }
          }

          if ( this->msgLevel( MSG::DEBUG ) ) {
            this->debug() << "Packed buffer size  " << buffer.buffer().size() << endmsg;
            this->debug() << "data type " << System::typeinfoName( typeid( data ) ) << endmsg;
            this->debug() << "data location " << this->outputLocation() << endmsg;
            this->debug() << "packed data size " << oSize2.second << endmsg;
            this->debug() << "classID " << data->clID() << " locationID " << locationID << " nlinks " << nlinks
                          << endmsg;
          }

          m_nbBufferSize += buffer.buffer().size();
          bigBuffer.addBuffer( buffer );
        }
      }
      return bigBuffer;
    }

  private:
    ServiceHandle<IIndexedANNSvc> m_annsvc{this, "ANNSvc", "HltANNSvc", "Service to retrieve PackedObjectLocations"};

    Gaudi::Property<unsigned int> m_encodingKey{this, "EncodingKey", 0u};
    Gaudi::Property<bool>         m_enableCheck{this, "EnableCheck", false};

    // This is only needed to be consistent with non-soa packing for configuration
    Gaudi::Property<bool> m_enableChecksum{this, "EnableChecksum", false};

    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nbBufferSize{this, "# BufferSize"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING>     m_zero_key{
        this,
        "Encoding key is zero -- this implies explicit configuration of decoding will be required.. make sure you know "
        "how to do this, and will have the required configuration setup",
        10};
  };

} // namespace DataPacking::Buffer::SOA
