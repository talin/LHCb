/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/MCVertex.h"
#include "Event/PackedMCVertex.h"
#include "Event/StandardPacker.h"

#include "LHCbAlgs/Consumer.h"

#include "Gaudi/Accumulators.h"
#include "GaudiKernel/DataObjectHandle.h"

#include <algorithm>
#include <memory>

namespace LHCb {

  /**
   * The algorithm provides access to previously packed MC vertices at a TES location.
   * It reads a location of packed MC vertices and converts them to MCVertex
   * together with the associated MCParticle which is unpacked by the algorithm UnpackMCParticle
   *
   * Note that the inheritance from Consumer and the void input are misleading.
   * The algorithm is reading from and writing to TES, just via a Handles so that
   * it can deal with non existant input and write to TES at the begining of
   * the operator(), as cross pointers are used in the TES and requires this.
   *
   *  @author Olivier Callot
   *  @date   2005-03-18
   */
  struct UnpackMCVertex : Algorithm::Consumer<void()> {
    using Consumer::Consumer;

    void operator()() const override;

    DataObjectReadHandle<PackedMCVertices> m_input{this, "InputName", PackedMCVertexLocation::Default};
    DataObjectWriteHandle<MCVertices>      m_MCVertices{this, "OutputName", MCVertexLocation::Default};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_corruptedMother{
        this, "Corrupt MCVertex Mother MCParticle SmartRef detected", 10};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_corruptedDaughter{
        this, "Corrupt MCVertex Daughter MCParticle SmartRef detected", 10};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_foundDuplicate{
        this, "Found duplicate in packed MCVertex products", 10};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::UnpackMCVertex, "UnpackMCVertex" )

void LHCb::UnpackMCVertex::operator()() const {

  auto* newMCVertices = m_MCVertices.put( std::make_unique<MCVertices>() );

  if ( !m_input.exist() ) return;
  auto const* dst = m_input.get();

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Size of PackedMCVertices = " << dst->mcVerts().size() << endmsg;

  // Packing version
  const char pVer = dst->packingVersion();

  newMCVertices->reserve( dst->mcVerts().size() );
  for ( const auto& src : dst->mcVerts() ) {

    auto* vert = new MCVertex();
    newMCVertices->insert( vert, src.key );
    vert->setPosition( Gaudi::XYZPoint( StandardPacker::position( src.x ), StandardPacker::position( src.y ),
                                        StandardPacker::position( src.z ) ) );
    vert->setTime( src.tof );
    vert->setType( (MCVertex::MCVertexType)src.type );

    int hintID( 0 ), key( 0 );
    if ( -1 != src.mother ) {
      if ( ( 0 == pVer && StandardPacker::hintAndKey32( src.mother, dst, newMCVertices, hintID, key ) ) ||
           ( 0 != pVer && StandardPacker::hintAndKey64( src.mother, dst, newMCVertices, hintID, key ) ) ) {
        vert->setMother( {newMCVertices, hintID, key} );
      } else {
        ++m_corruptedMother;
      }
    }

    // List of processed refs, to check for duplicates
    std::vector<std::int64_t> processedRefs;
    processedRefs.reserve( src.products.size() );

    // loop over refs and process
    for ( const auto& I : src.products ) {
      // Check for duplicates ...
      if ( std::none_of( processedRefs.begin(), processedRefs.end(), [&I]( const auto& J ) { return I == J; } ) ) {
        // save this packed ref to the list of those already processed.
        processedRefs.push_back( I );
        // Unpack the ref and save to the vertex
        hintID = key = 0;
        if ( ( 0 == pVer && StandardPacker::hintAndKey32( I, dst, newMCVertices, hintID, key ) ) ||
             ( 0 != pVer && StandardPacker::hintAndKey64( I, dst, newMCVertices, hintID, key ) ) ) {
          vert->addToProducts( {newMCVertices, hintID, key} );
        } else {
          ++m_corruptedDaughter;
        }
      } else {
        ++m_foundDuplicate;
      }
    }
  }
}
