/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/MCParticle.h"
#include "Event/PackedMCParticle.h"
#include "Event/StandardPacker.h"

#include "LHCbAlgs/Consumer.h"

#include "Gaudi/Accumulators.h"
#include "GaudiKernel/DataObjectHandle.h"

#include <algorithm>
#include <memory>

namespace LHCb {

  /**
   * The algorithm provides access to previously packed MC particles at a TES location.
   * It reads a location of packed MC particles and converts them to MCParticle
   * together with the associated MCVertex which is unpacked by the algorithm UnpackMCVertex
   *
   * Note that the inheritance from Consumer and the void input are misleading.
   * The algorithm is reading from and writing to TES, just via a Handles so that
   * it can deal with non existant input and write to TES at the begining of
   * the operator(), as cross pointers are used in the TES and requires this.
   *
   * @author Olivier Callot
   * @date   2005-03-18
   */
  struct UnpackMCParticle : Algorithm::Consumer<void()> {
    using Consumer::Consumer;

    void operator()() const override;

    DataObjectReadHandle<PackedMCParticles> m_input{this, "InputName", PackedMCParticleLocation::Default};
    DataObjectWriteHandle<MCParticles>      m_output{this, "OutputName", MCParticleLocation::Default};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_corruptedOrig{
        this, "Corrupt MCParticle Origin MCVertex SmartRef detected", 10};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_corruptedEnd{
        this, "Corrupt MCParticle End MCVertex SmartRef detected", 10};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_foundDuplicate{
        this, "Found duplicate in packed MCParticle end vertices", 10};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::UnpackMCParticle, "UnpackMCParticle" )

void LHCb::UnpackMCParticle::operator()() const {

  auto* newMCParticles = m_output.put( std::make_unique<MCParticles>() );

  if ( !m_input.exist() ) return;
  auto const* dst = m_input.get();

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Size of PackedMCParticles = " << dst->mcParts().size() << endmsg;

  // Packing version
  const char pVer = dst->packingVersion();

  // random generator for private tests of flags.
  // static std::default_random_engine gen;
  // static std::uniform_real_distribution<float> uniform(0,1);

  newMCParticles->reserve( dst->mcParts().size() );
  for ( const auto& src : dst->mcParts() ) {

    MCParticle* part = new MCParticle();
    newMCParticles->insert( part, src.key );

    const auto px   = StandardPacker::energy( src.px );
    const auto py   = StandardPacker::energy( src.py );
    const auto pz   = StandardPacker::energy( src.pz );
    const auto mass = src.mass;
    const auto E    = std::sqrt( ( px * px ) + ( py * py ) + ( pz * pz ) + ( mass * mass ) );
    part->setMomentum( Gaudi::LorentzVector( px, py, pz, E ) );

    part->setParticleID( ParticleID( src.PID ) );

    part->setFlags( src.flags );
    // for testing, randomly set 'fromSignal' to true 5% of the time.
    // part->setFromSignal( uniform(gen) > 0.95 );

    int hintID( 0 ), key( 0 );
    if ( ( 0 == pVer && StandardPacker::hintAndKey32( src.originVertex, dst, newMCParticles, hintID, key ) ) ||
         ( 0 != pVer && StandardPacker::hintAndKey64( src.originVertex, dst, newMCParticles, hintID, key ) ) ) {
      part->setOriginVertex( {newMCParticles, hintID, key} );
    } else {
      ++m_corruptedOrig;
    }

    // List of processed refs, to check for duplicates
    std::vector<std::int64_t> processedRefs;
    processedRefs.reserve( src.endVertices.size() );

    // loop over refs and process
    for ( const auto& I : src.endVertices ) {
      // Check for duplicates ...
      if ( std::none_of( processedRefs.begin(), processedRefs.end(), [&I]( const auto& J ) { return I == J; } ) ) {
        // save this packed ref to the list of those already processed.
        processedRefs.push_back( I );
        // Unpack the ref and save to the vertex
        hintID = key = 0;
        if ( ( 0 == pVer && StandardPacker::hintAndKey32( I, dst, newMCParticles, hintID, key ) ) ||
             ( 0 != pVer && StandardPacker::hintAndKey64( I, dst, newMCParticles, hintID, key ) ) ) {
          // Construct the smart ref
          // save
          part->addToEndVertices( {newMCParticles, hintID, key} );
        } else {
          ++m_corruptedEnd;
        }
      } else {
        ++m_foundDuplicate;
      }
    }
  }
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Size of UnpackedMCParticles = " << newMCParticles->size() << endmsg;
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "corruptedOrig = " << m_corruptedOrig << endmsg;
    debug() << "corruptedEnd = " << m_corruptedEnd << endmsg;
    debug() << "foundDuplicate = " << m_foundDuplicate << endmsg;
  }
}
