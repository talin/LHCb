/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "ErrorCategory.h"
#include "Event/MCParticle.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedRelations.h"
#include "Event/PackerBase.h"
#include "Event/Particle.h"
#include "Event/RelatedInfoMap.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"
#include "expected.h"

namespace LHCb::Packers {

  namespace details {

    template <typename DataVector>
    using FromContainer = typename std::remove_pointer_t<typename DataVector::From>::Container;
    template <typename DataVector>
    using ToContainer = typename std::remove_pointer_t<typename DataVector::To>::Container;

    template <typename T>
    struct add_const_ {
      using type = std::add_const_t<T>;
    };
    template <typename T>
    struct add_const_<T*> {
      using type = std::add_const_t<T>* const;
    };

    template <typename T>
    using const_value_t = typename add_const_<typename T::value_type>::type;

    class Resolver {
      LinkManager const*                     linkMgr = nullptr;
      std::vector<std::pair<size_t, size_t>> m_missing; // for each entry in linkMgr, the number of failed lookups...

      auto& missing_key( int i ) {
        if ( m_missing.empty() ) m_missing.resize( linkMgr->size() );
        return m_missing.at( i ).first;
      }
      auto& missing_container( int i ) {
        if ( m_missing.empty() ) m_missing.resize( linkMgr->size() );
        return m_missing.at( i ).second;
      }

    public:
      Resolver( LinkManager const* p ) : linkMgr{p} {
        if ( !linkMgr ) throw GaudiException( "no linkmanager???", __func__, StatusCode::FAILURE );
      }

      static int key( std::int64_t i ) {
        int id( 0 );
        int key( 0 );
        StandardPacker::indexAndKey64( i, id, key );
        return key;
      }

      template <typename T>
      tl::expected<typename T::contained_type const*, StatusCode> object( std::int64_t i ) {
        int id( 0 );
        int key( 0 );
        StandardPacker::indexAndKey64( i, id, key );
        auto const* link = linkMgr->link( id );
        if ( !link || link->ID() == LinkManager::DirLinkType::INVALID )
          throw GaudiException( "invalid link???", __func__, StatusCode::FAILURE );
        if ( !link->object() ) {
          ++missing_container( id );
          return tl::unexpected{Packers::ErrorCode::CONTAINER_NOT_FOUND};
        }
        auto const* container = static_cast<T const*>( link->object() );
        // auto const* container = dynamic_cast<T const*>( link->object() );
        // if ( !container ) return tl::unexpected{Packers::ErrorCode::WRONG_CONTAINER_TYPE};
        auto const* obj = container->object( key );
        if ( !obj ) {
          ++missing_key( id );
          return tl::unexpected{Packers::ErrorCode::KEY_NOT_FOUND};
        }
        return obj;
      }

      std::string path( std::int64_t i ) const {
        int id( 0 );
        int key( 0 );
        StandardPacker::indexAndKey64( i, id, key );
        auto const* link = linkMgr->link( id );
        if ( !link || link->ID() == LinkManager::DirLinkType::INVALID )
          throw GaudiException( "invalid link???", __func__, StatusCode::FAILURE );
        return link->path();
      }

      bool       hasErrors() const { return !m_missing.empty(); }
      MsgStream& dump( MsgStream& msg ) const {
        if ( !hasErrors() ) return msg;
        for ( const auto& [i, m] : LHCb::range::enumerate( m_missing, 0L ) ) {
          if ( m.second )
            msg << "failed to fetch container from " << linkMgr->link( i )->path() << " - requested " << m.second
                << " times " << endmsg;
        }
        for ( const auto& [i, m] : LHCb::range::enumerate( m_missing, 0L ) ) {
          if ( m.first ) msg << "failed to fetch " << m.first << " keys from " << linkMgr->link( i )->path() << endmsg;
        }
        return msg;
      }
    };

    // Relations between two containers
    template <typename RELATION, typename PRELATION, typename ALGORITHM>
    void unpack2d( RELATION& rels, const PRELATION& prels, ALGORITHM const& parent ) {

      auto resolver = details::Resolver{prels.linkMgr()};
      for ( const auto& prel : prels.data() ) {
        for ( int kk = prel.start; kk != prel.end; ++kk ) {

          const auto& src = prels.sources()[kk];
          const auto& dst = prels.dests()[kk];

          const auto f = resolver.object<FromContainer<RELATION>>( src );
          if ( !f ) {
            if ( parent.msgLevel( MSG::DEBUG ) )
              parent.debug() << f.error() << " while retrieving From object with key " << resolver.key( src )
                             << " from " << resolver.path( src ) << endmsg;
          }

          const auto t = resolver.object<ToContainer<RELATION>>( dst );
          if ( !t ) {
            if ( parent.msgLevel( MSG::DEBUG ) )
              parent.debug() << t.error() << " while retrieving To object with key " << resolver.key( dst ) << " from "
                             << resolver.path( dst ) << endmsg;
          }

          if ( !f || !t ) continue;

          // only weighted relations have weights
          StatusCode sc;
          if constexpr ( std::is_same_v<PRELATION, LHCb::PackedWeightedRelations> ) {
            sc = rels.relate( f.value(), t.value(), prels.weights()[kk] );
          } else {
            sc = rels.relate( f.value(), t.value() );
          }
          if ( !sc ) {
            parent.warning() << "Something went wrong with relation unpacking "
                             << " sourceKey " << resolver.key( src ) << " sourceLink " << resolver.path( src )
                             << " destKey " << resolver.key( dst ) << " destLink " << resolver.path( dst ) << endmsg;
          }
          if ( parent.msgLevel( MSG::DEBUG ) ) {
            parent.debug() << "Relation build between "
                           << " sourceKey " << resolver.key( src ) << " sourceLink " << resolver.path( src )
                           << " destKey " << resolver.key( dst ) << " destLink " << resolver.path( dst )
                           << " rels size " << rels.relations().size() << endmsg;
          }
        }
        if ( resolver.hasErrors() ) resolver.dump( parent.warning() );
      }
      rels.i_sort();
    }

  } // namespace details

  template <typename To,
            typename = std::enable_if_t<std::is_same_v<To, LHCb::VertexBase> || std::is_same_v<To, LHCb::MCParticle>>>
  struct ParticleRelation : public PackerBase {

    using PackerBase::PackerBase;
    using PackedDataVector = LHCb::PackedRelations;
    using DataVector       = LHCb::Relation1D<LHCb::Particle, To>;

    static const char* propertyName() {
      if constexpr ( std::is_same_v<To, LHCb::VertexBase> ) return "P2VRelations";
      if constexpr ( std::is_same_v<To, LHCb::MCParticle> ) return "P2MCPRelations";
    }

    void unpack( const PackedDataVector& pdata, DataVector& data ) const { details::unpack2d( data, pdata, parent() ); }

    template <typename Range>
    StatusCode check( const Range& dataA, const DataVector& dataB ) const {

      // assume OK from the start
      bool ok = true;

      // checker
      const DataPacking::DataChecks ch( parent() );

      // checks here
      const bool relSizeOK = dataA.relations().size() == dataB.relations().size();
      ok &= relSizeOK;

      if ( relSizeOK ) {
        auto iDA = dataA.relations().begin();
        auto iDB = dataB.relations().begin();
        for ( ; iDA != dataA.relations().end() && iDB != dataB.relations().end(); ++iDA, ++iDB ) {
          bool entryOK = true;
          entryOK &= ch.comparePointers( "Relations From ", iDA->from(), iDB->from() );
          entryOK &= ch.comparePointers( "Relations To ", iDA->to(), iDB->to() );
          ok &= entryOK;
          if ( !entryOK ) {
            const std::string from_loc = ( iDA->from()->parent() && iDA->from()->parent()->registry()
                                               ? iDA->from()->parent()->registry()->identifier()
                                               : "Not in TES" );
            const std::string to_loc =
                ( iDA->to()->parent() && iDA->to()->parent()->registry() ? iDA->to()->parent()->registry()->identifier()
                                                                         : "Not in TES" );

            parent().warning() << "Problem with Relation data packing :-" << endmsg << " from '" << from_loc << "'"
                               << " to '" << to_loc << "'" << endmsg << dataA << endmsg << "  Unpacked Relation"
                               << endmsg << dataB << endmsg;
          }
        }
      } else {
        parent().warning() << "Relations different sizes" << endmsg;
      }

      return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
    }

    void pack( const DataVector& rels, PackedDataVector& prels ) const {

      // Make a new packed data object and save
      auto& prel = prels.data().emplace_back();

      // reference to original container
      prel.container = StandardPacker::reference64( &prels, &rels, 0 );

      // First object
      prel.start = prels.sources().size();

      // reserve size
      const auto newSize = prels.sources().size() + rels.relations().size();
      prels.sources().reserve( newSize );
      prels.dests().reserve( newSize );

      // Loop over relations
      for ( const auto& R : rels.relations() ) {
        prels.sources().push_back( StandardPacker::reference64( &prels, R.from() ) );
        prels.dests().push_back( StandardPacker::reference64( &prels, R.to() ) );
      }

      // last object
      prel.end = prels.sources().size();
    }
  };

  // pack P2IntRELATION
  struct P2IntRelation : public PackerBase {
    using PackerBase::PackerBase;
    using PackedDataVector = LHCb::PackedRelations;
    using DataVector       = LHCb::Relation1D<LHCb::Particle, int>;
    static const char* propertyName() { return "P2IntRelations"; }

    void unpack( const PackedDataVector& prels, DataVector rels ) const {
      auto resolver = details::Resolver{prels.linkMgr()};
      for ( const auto& prel : prels.data() ) {
        for ( int kk = prel.start; kk < prel.end; ++kk ) {
          const auto& src = prels.sources()[kk];
          const auto& dst = prels.dests()[kk];

          const auto f = resolver.object<details::FromContainer<DataVector>>( src );
          if ( !f ) {
            if ( parent().msgLevel( MSG::DEBUG ) )
              parent().debug() << f.error() << " while retrieving object with key " << resolver.key( src ) << " from "
                               << resolver.path( src ) << endmsg;
            continue;
          }

          StatusCode sc = rels.relate( f.value(), static_cast<int>( dst ) );
          if ( !sc )
            parent().warning() << "Something went wrong with relation unpacking "
                               << "sourceKey " << resolver.key( src ) << " sourceLink " << resolver.path( src )
                               << endmsg;
        }
      }
      if ( resolver.hasErrors() ) resolver.dump( parent().warning() );
      rels.i_sort();
    }

    template <typename Range>
    StatusCode check( const Range& dataA, const DataVector& dataB ) const {
      // assume OK from the start
      bool ok = true;

      // checker
      const DataPacking::DataChecks ch( parent() );

      // checks here
      const bool relSizeOK = dataA.relations().size() == dataB.relations().size();
      ok &= relSizeOK;

      if ( relSizeOK ) {
        auto iDA = dataA.relations().begin();
        auto iDB = dataB.relations().begin();
        for ( ; iDA != dataA.relations().end() && iDB != dataB.relations().end(); ++iDA, ++iDB ) {
          bool entryOK = true;
          entryOK &= ch.comparePointers( "Relations From ", iDA->from(), iDB->from() );
          entryOK &= ch.compareInts( "Relations To ", iDA->to(), iDB->to() );
          ok &= entryOK;
          if ( !entryOK ) {
            const std::string from_loc = ( iDA->from()->parent() && iDA->from()->parent()->registry()
                                               ? iDA->from()->parent()->registry()->identifier()
                                               : "Not in TES" );

            parent().warning() << "Problem with Relation data packing :-" << endmsg << " from '" << from_loc << "'"
                               << " to '" << iDA->to() << "'" << endmsg << dataA << endmsg << "  Unpacked Relation"
                               << endmsg << dataB << endmsg;
          }
        }
      } else {
        parent().warning() << "Relations different sizes" << endmsg;
      }

      return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
    }

    void pack( const DataVector& rels, PackedDataVector& prels ) const {

      // Make a new packed data object and save
      auto& prel = prels.data().emplace_back();

      // reference to original container
      prel.container = StandardPacker::reference64( &prels, &rels, 0 );

      // First object
      prel.start = prels.sources().size();

      // reserve size
      const auto newSize = prels.sources().size() + rels.relations().size();
      prels.sources().reserve( newSize );
      prels.dests().reserve( newSize );

      // Loop over relations
      for ( const auto& R : rels.relations() ) {
        prels.sources().emplace_back( StandardPacker::reference64( &prels, R.from() ) );
        prels.dests().emplace_back( R.to() );
      }

      // last object
      prel.end = prels.sources().size();
    }
  };

  // Pack Proto particle 2 MC particle Relation
  template <typename From, typename To, typename Weight = double>
  struct WeightedRelation : public PackerBase {
    using PackerBase::PackerBase;
    using DataVector       = LHCb::RelationWeighted1D<From, To, Weight>;
    using PackedDataVector = LHCb::PackedWeightedRelations;
    static const char* propertyName() {
      if constexpr ( std::is_same_v<From, LHCb::ProtoParticle> && std::is_same_v<To, LHCb::MCParticle> ) {
        return "PP2MCPRelations";
      }
    }

    void unpack( const PackedDataVector& pdata, DataVector& data ) const { details::unpack2d( data, pdata, parent() ); }

    template <typename Range>
    StatusCode check( const Range& dataA, const DataVector& dataB ) const {
      // assume OK from the start
      bool ok = true;

      // checker
      const DataPacking::DataChecks ch( parent() );

      // checks here
      const bool relSizeOK = dataA.relations().size() == dataB.relations().size();
      ok &= relSizeOK;

      if ( relSizeOK ) {
        auto iDA = dataA.relations().begin();
        auto iDB = dataB.relations().begin();
        for ( ; iDA != dataA.relations().end() && iDB != dataB.relations().end(); ++iDA, ++iDB ) {
          bool entryOK = true;
          entryOK &= ch.comparePointers( "Relations From ", iDA->from(), iDB->from() );
          entryOK &= ch.comparePointers( "Relations To ", iDA->to(), iDB->to() );
          entryOK &= ch.compareDoubles( "Relations Weight ", iDA->weight(), iDB->weight() );

          ok &= entryOK;
          if ( !entryOK ) {
            const std::string from_loc = ( iDA->from()->parent() && iDA->from()->parent()->registry()
                                               ? iDA->from()->parent()->registry()->identifier()
                                               : "Not in TES" );

            const std::string to_loc =
                ( iDA->to()->parent() && iDA->to()->parent()->registry() ? iDA->to()->parent()->registry()->identifier()
                                                                         : "Not in TES" );

            parent().warning() << "Problem with Relation data packing :-" << endmsg << " from '" << from_loc << "' to '"
                               << to_loc << "'" << endmsg;
          }
        }
      } else {
        parent().warning() << "Relations different sizes" << endmsg;
      }

      return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
    }

    template <typename RelationRange>
    void pack( const RelationRange& rels, PackedDataVector& prels ) const {

      // Make a new packed data object and save
      auto& prel = prels.data().emplace_back();

      // reference to original container
      prel.container = StandardPacker::reference64( &prels, identifier( rels ), 0 );

      // First object
      prel.start = prels.sources().size();

      // reserve size
      const auto newSize = prels.sources().size() + rels.relations().size();
      prels.sources().reserve( newSize );
      prels.dests().reserve( newSize );
      prels.weights().reserve( newSize );

      // Loop over relations
      for ( const auto& R : rels.relations() ) {
        prels.sources().emplace_back( StandardPacker::reference64( &prels, R.from() ) );
        prels.dests().emplace_back( StandardPacker::reference64( &prels, R.to() ) );
        prels.weights().emplace_back( R.weight() );
      }

      // last object
      prel.end = prels.sources().size();
    }
  };

  using PP2MCPRelation = WeightedRelation<LHCb::ProtoParticle, LHCb::MCParticle, double>;

  // particle 2 info relations
  class P2InfoRelation : public PackerBase {
    const LHCb::RelatedInfoRelationsPacker m_rInfoPacker;

  public:
    /// Related Info Packer
    P2InfoRelation( Gaudi::Algorithm const* p ) : PackerBase( p ), m_rInfoPacker( p ) {}

    using DataVector       = LHCb::Relation1D<LHCb::Particle, LHCb::RelatedInfoMap>;
    using PackedDataVector = LHCb::PackedRelatedInfoRelations;
    static const char* propertyName() { return "P2InfoRelations"; }

    void unpack( const PackedDataVector& prels, DataVector& rels ) const {
      auto resolver = details::Resolver{prels.linkMgr()};
      for ( const auto& prel : prels.containers() ) {
        for ( const auto& rel : LHCb::Packer::subrange( prels.relations(), prel.first, prel.last ) ) {

          auto f = resolver.object<details::FromContainer<DataVector>>( rel.reference );
          if ( !f ) {
            parent().debug() << f.error() << " while retrieving object with key " << resolver.key( rel.reference )
                             << " from " << resolver.path( rel.reference ) << endmsg;
            continue;
          }

          LHCb::RelatedInfoMap t;
          t.reserve( rel.last - rel.first );
          for ( const auto& jj : LHCb::Packer::subrange( prels.info(), rel.first, rel.last ) ) { t.insert( jj ); }

          StatusCode sc = rels.relate( f.value(), t );
          if ( !sc )
            parent().warning() << "Something went wrong with relation unpacking "
                               << "sourceKey " << resolver.key( rel.reference ) << " sourceLink "
                               << resolver.path( rel.reference ) << endmsg;
        }
      }
      if ( resolver.hasErrors() ) resolver.dump( parent().warning() );
      rels.i_sort();
    }

    template <typename Range>
    StatusCode check( const Range& dataA, const DataVector& dataB ) const {
      // assume OK from the start
      bool ok = true;

      // checker
      const DataPacking::DataChecks ch( parent() );

      // checks here
      const bool relSizeOK = dataA.relations().size() == dataB.relations().size();
      ok &= relSizeOK;

      if ( relSizeOK ) {
        auto iDA = dataA.relations().begin();
        auto iDB = dataB.relations().begin();
        for ( ; iDA != dataA.relations().end() && iDB != dataB.relations().end(); ++iDA, ++iDB ) {
          bool entryOK = true;
          entryOK &= ch.comparePointers( "Relations From ", iDA->from(), iDB->from() );

          // TODO add a check for related info
          ok &= entryOK;
          if ( !entryOK ) {
            const std::string from_loc = ( iDA->from()->parent() && iDA->from()->parent()->registry()
                                               ? iDA->from()->parent()->registry()->identifier()
                                               : "Not in TES" );

            parent().warning() << "Problem with Relation data packing :-" << endmsg << " from '" << from_loc << "'"
                               << endmsg << dataA << endmsg << "  Unpacked Relation" << endmsg << dataB << endmsg;
          }
        }
      } else {
        parent().warning() << "Relations different sizes" << endmsg;
      }

      return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
    }

    void pack( const DataVector& rels, PackedDataVector& prels ) const {

      // Make a entry in the containers vector, for this TES location
      prels.containers().emplace_back();
      auto& pcont = prels.containers().back();

      // reference to original container and key
      pcont.reference = StandardPacker::reference64( &prels, &rels, 0 );

      // First entry in the relations vector
      pcont.first = prels.data().size();

      // Loop over the relations and fill
      prels.data().reserve( prels.data().size() + rels.relations().size() );

      // Use the packer to pack this location ...
      m_rInfoPacker.pack( rels, prels );

      // last entry in the relations vector
      pcont.last = prels.data().size();
    }
  };

} // namespace LHCb::Packers
