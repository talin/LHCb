/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/PackedCaloAdc.h"
#include "Event/PackedCaloCluster.h"
#include "Event/PackedCaloDigit.h"
#include "Event/PackedCaloHypo.h"
#include "Event/PackedCluster.h"
#include "Event/PackedData.h"
#include "Event/PackedDataBuffer.h"
#include "Event/PackedFlavourTag.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedProtoParticle.h"
#include "Event/PackedRecVertex.h"
#include "Event/PackedRelations.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedTrack.h"
#include "Event/PackedVertex.h"
#include "Event/PackedWeightsVector.h"
#include "Interfaces/IProtoParticleTool.h"
#include "Kernel/IIndexedANNSvc.h"
#include "LHCbAlgs/Consumer.h"
#include "RawbankV2Compatibility.h"
#include "RegistryWrapper.h"
#include "RelationPackers.h"
#include "expected.h"

namespace {
  static const Gaudi::StringKey PackedObjectLocations{"PackedObjectLocations"};

  template <typename T>
  using Expected   = tl::expected<T, StatusCode>;
  using Unexpected = tl::unexpected<StatusCode>;
  using ReadBuffer = LHCb::Hlt::PackedData::PackedDataInBuffer;
  using LHCb::Hlt::PackedData::ObjectHeader;
  using LHCb::Packers::ErrorCode;

  template <typename C>
  auto size( C const& c ) -> decltype( c.size() ) {
    return c.size();
  }
  template <typename From, typename To>
  auto size( LHCb::Relation1D<From, To> const& r ) -> decltype( r.relations().size() ) {
    return r.relations().size();
  }
  template <typename From, typename To, typename Weight>
  auto size( LHCb::RelationWeighted1D<From, To, Weight> const& r ) -> decltype( r.relations().size() ) {
    return r.relations().size();
  }

} // namespace

namespace LHCb::Hlt::PackedData {

  struct Counters {
    Gaudi::Accumulators::MsgCounter<MSG::ERROR>       inconsistentSize;
    Gaudi::Accumulators::MsgCounter<MSG::ERROR>       no_buffers;
    Gaudi::Accumulators::MsgCounter<MSG::ERROR>       unknown;
    Gaudi::Accumulators::MsgCounter<MSG::WARNING>     absent;
    Gaudi::Accumulators::MsgCounter<MSG::WARNING>     missing_dependency;
    Gaudi::Accumulators::BinomialCounter<>            nothing_to_do;
    Gaudi::Accumulators::SummingCounter<unsigned int> nbUnpackedData;
    Gaudi::Accumulators::SummingCounter<unsigned int> nbBufferData;
    Gaudi::Accumulators::SummingCounter<unsigned int> nbLinkedBufferData;
    Gaudi::Accumulators::SummingCounter<unsigned int> missingLinks;
    Gaudi::Accumulators::SummingCounter<unsigned int> wrong_LinkID;

    Counters( Gaudi::Algorithm* parent )
        : inconsistentSize{parent, "Size read does not match size expected"}
        , no_buffers{parent, "Input has empty map of buffers -- can not do anything at this point. Verify "
                             "HltPackedBufferDecoder "
                             "configuration"}
        , unknown{parent,
                  "Configured output location not known to ANNSvc for decoding key in current event - unable to unpack"}
        , absent{parent, "DstData buffer for configured output not available - unable to unpack", 0}
        , missing_dependency{parent, "DstData buffer for dependency not available - unable to unpack", 0}
        , nothing_to_do{parent, "Target data already present"}
        , nbUnpackedData{parent, "# UnpackedData"}
        , nbBufferData{parent, "# BufferData"}
        , nbLinkedBufferData{parent, "# Linked BufferData"}
        , missingLinks{parent, "# Missing Link Locations"}
        , wrong_LinkID{parent, "Mismatched LinkMgr id"} {}
  };

  class Loader;

  using LoaderFn_t = Expected<std::pair<std::string, DataObject const*>> ( * )( PackedDataInBuffer&,
                                                                                ObjectHeader const&, Loader& );

  class Loader {

    class Decoder {
      bool                             m_needs_old_lookup;
      IIndexedANNSvc::inv_map_t const* m_s2i;
      IIndexedANNSvc::map_t const*     m_i2s;
      std::uint32_t                    m_key;
      std::uint32_t                    m_sourceID;
      std::string m_anonymous_prefix; // "/Event/DependencyOnly/DoNotUseDirectly/0x{:08x}" } //TODO: must include
                                      // sourceID somehow...

    public:
      template <typename Buffer>
      Decoder( Buffer const& buffer, IIndexedANNSvc const& ann, std::string anonymous_prefix )
          : m_needs_old_lookup{buffer.bankVersion() < 3}
          , m_s2i{&ann.s2i( buffer.key(), PackedObjectLocations )}
          , m_i2s{&ann.i2s( buffer.key(), PackedObjectLocations )}
          , m_key{buffer.key()}
          , m_sourceID{buffer.sourceID()}
          , m_anonymous_prefix{std::move( anonymous_prefix )} {}

      auto key() const { return m_key; }
      auto sourceID() const { return m_sourceID; }

      std::optional<std::int32_t> operator()( std::string_view loc ) const {
        auto j =
            m_needs_old_lookup
                ? std::find_if( m_s2i->begin(), m_s2i->end(),
                                DataPacking::Buffer::v2_compatibility::match_first_with_missing_p_after_slash( loc ) )
                : m_s2i->find( std::string{loc} ); // FIXME: support transparant lookup...
        if ( j == m_s2i->end() ) return std::nullopt;
        return j->second;
      }

      std::optional<std::string> operator()( unsigned int id ) const {
        // if id & 0x8000 0000 is set, assume it is an implicit dependency => skip ANN svc, generate on the fly instead
        //  -- note this is _impossible_ in v2 data...
        if ( id & 0x80000000 ) return fmt::format( m_anonymous_prefix, m_sourceID, id );
        auto j = m_i2s->find( id );
        if ( j == m_i2s->end() ) return std::nullopt;
        return j->second;
      }

      std::optional<std::int32_t> remap_v2( unsigned int id ) const {
        if ( !m_needs_old_lookup ) return id;
        auto loc = ( *this )( id ); // loc should have a have '/p' for this to make sense...
        if ( !loc ) return std::nullopt;
        auto j = std::find_if( m_s2i->begin(), m_s2i->end(),
                               DataPacking::Buffer::v2_compatibility::match_first_with_extra_p_after_slash( *loc ) );
        if ( j == m_s2i->end() ) return std::nullopt;
        return j->second;
      }
    };

    static const std::map<CLID, LoaderFn_t>       s_map;
    Gaudi::Algorithm const*                       m_algo     = nullptr;
    LHCb::Hlt::PackedData::MappedInBuffers const* m_buffers  = nullptr;
    Counters*                                     m_counters = nullptr;
    Decoder                                       m_decoder;

    Expected<std::pair<std::string, DataObject const*>> load( ReadBuffer& readBuffer ) {
      ObjectHeader header{readBuffer};
      auto         i = s_map.find( header.classID );
      if ( i == s_map.end() ) return Unexpected{ErrorCode::NO_LOADER_FOR_CLID};
      auto r = std::invoke( i->second, readBuffer, header, *this );
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Unpacked linked location  " << r->first << " (CLID=" << header.classID
                << " locationID=" << header.locationID << ") "
                << "   " << header.storedSize << " were stored!"
                << " and " << header.linkLocationIDs.size() << " links were stored!" << endmsg;
      }
      return r;
    }

  public:
    Loader( Gaudi::Algorithm const* algo, LHCb::Hlt::PackedData::MappedInBuffers const& buffers, Counters* counters,
            IIndexedANNSvc const& ann, std::string const& anonymous_prefix )
        : m_algo{algo}, m_buffers{&buffers}, m_counters{counters}, m_decoder{buffers, ann, anonymous_prefix} {}

    // make Loader a drop-in replacement / front-end (in some aspects) for a Gaudi::Algorithm...
               operator Gaudi::Algorithm const*() const { return m_algo; }
    bool       msgLevel( MSG::Level l ) const { return m_algo->msgLevel( l ); }
    MsgStream& debug() const { return m_algo->debug(); }
    MsgStream& error() const { return m_algo->error(); }
    MsgStream& warning() const { return m_algo->warning(); }

    Decoder& decoder() { return m_decoder; }

    template <typename Fn>
    void walk( Fn const& fn, std::int32_t id, size_t level = 0 ) {
      auto loc = m_decoder( id ).value_or( fmt::format( "UnknownID:{}", id ) );
      auto buf = m_buffers->find( id );
      std::invoke( fn, loc, id, buf, level );
      if ( buf ) {
        auto readBuffer = ReadBuffer{*buf};
        auto header     = ObjectHeader{readBuffer};
        auto currentID  = decoder().remap_v2( header.locationID ).value();
        for ( auto lid : header.linkLocationIDs )
          if ( lid != currentID ) walk( fn, lid, level + 1 );
      }
    }

    Expected<DataObject const*> get( const std::string& loc ) const {
      DataObject* ptr = nullptr;
      if ( auto sc = m_algo->evtSvc()->retrieveObject( loc, ptr ); sc.isFailure() ) return Unexpected{sc};
      return ptr;
    }

    StatusCode put( std::string const& loc, std::unique_ptr<DataObject> ptr ) const {
      return m_algo->evtSvc()->registerObject( loc, ptr.release() );
    }

    Expected<std::pair<std::string, DataObject const*>> load( std::int32_t id ) {
      auto loc = m_decoder( m_decoder.remap_v2( id ).value_or( id ) ); // TODO: have decoder rewrite the v2 table when
                                                                       // constructed with v2 buffers
      if ( !loc ) {
        error() << "could not get location for id=" << id << endmsg;
        throw GaudiException( "unknown location", __PRETTY_FUNCTION__, StatusCode::FAILURE );
      }

      if ( auto ptr = get( *loc ); ptr ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << *loc << " already exists -- no further unpacking needed" << endmsg;
        return std::pair{*loc, *ptr};
      }

      // for v3, we can just ask for `id` -- for v2, we have to ask for `loc` (which also works for v3)
      // const auto* buffer = m_decoder.buffer( id ) --> which for <3 can first do `loc(id)`
      const auto* buffer =
          ( m_buffers->bankVersion() < 3 ? m_buffers->find( m_decoder( *loc ).value() ) : m_buffers->find( id ) );
      if ( !buffer ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "No packed data present for required dependency " << *loc << endmsg;
        ++m_counters->missing_dependency;
        return std::pair{*loc, nullptr};
      }
      // Count the buffer size. Ideally also counter unpacked data size
      // but that requires casting DataObject to PACKER type.
      m_counters->nbLinkedBufferData += buffer->buffer().size();

      ReadBuffer readBuffer{*buffer}; // TODO: allow for emphemeral 'view' for reading without copying just to update
                                      // 'pos'
      return load( readBuffer );
    }

    StatusCode resolveLinks( DataObject& pdata, ObjectHeader const& header ) {
      // NOTE: we need to fill _all_ entries _in order_ accoding the the header. So even in case of error,
      //      we should continue. Hence the following code 'latches' on to the first error, and will return
      //      that in case of any error happening.
      StatusCode sc  = StatusCode::SUCCESS;
      auto*      mgr = pdata.linkMgr();
      // for v2, 'locationID' is the id of the TES location with '/p' in it... but the dependencies are withtout '/p'
      // so we need to remap the id for v2...
      auto currentID =
          ( m_buffers->bankVersion() > 2 ? header.locationID : m_decoder.remap_v2( header.locationID ).value() );
      for ( auto const& [n, id] : LHCb::range::enumerate( header.linkLocationIDs, 0l ) ) {
        if ( id == currentID ) {
          if ( n != mgr->addLink( m_decoder( currentID ).value(), &pdata ) ) {
            ++m_counters->wrong_LinkID;
            if ( sc.isSuccess() ) sc = ErrorCode::MISMATCHED_LINK_ID;
          }
        } else {
          auto r = this->load( id );
          if ( !r ) r.emplace( fmt::format( "UnknownID:0x{:08x}:{}", id, r.error().message() ), nullptr );
          auto const& [path, object] = *r;
          if ( !object ) {
            ++m_counters->missingLinks;
            // For now, make this a warning instead of an error -- the unpacking may be able to at least
            // unpack part of the object which does not require this specific dependency...
            warning() << "Unable to resolve dependency " << path << " during unpacking of "
                      << m_decoder( currentID ).value() << endmsg;
            // if ( sc.isSuccess() ) sc = ErrorCode::MISSING_DEPENDENCY;
          }
          if ( n != mgr->addLink( path, object ) ) {
            ++m_counters->wrong_LinkID;
            if ( sc.isSuccess() ) sc = ErrorCode::MISMATCHED_LINK_ID;
          }
        }
      }
      return sc;
    }
  };

  template <class PACKER>
  Expected<std::unique_ptr<typename PACKER::DataVector>> restoreObject( PackedDataInBuffer& buffer,
                                                                        ObjectHeader const& header, Loader& loader ) {
    // Sadly the pack structure expects data with valid Registry and LinkMgr. To be improved
    auto pdata      = DataPacking::Buffer::RegistryWrapper<typename PACKER::PackedDataVector>( "DummyPacked" );
    auto nBytesRead = buffer.load( *pdata );
    if ( loader.msgLevel( MSG::DEBUG ) ) {
      loader.debug() << "Loading of object (CLID=" << header.classID << " locationID=" << header.locationID << ") "
                     << " consumed " << nBytesRead << " bytes, "
                     << " and " << header.linkLocationIDs.size() << " links were stored!" << endmsg;
    }
    if ( nBytesRead != header.storedSize ) return Unexpected{ErrorCode::INCONSISTENT_SIZE};
    if ( !buffer.eof() ) {
      loader.warning() << " buffer not eof??? while restoring "
                       << loader.decoder()( header.locationID ).value_or( fmt::format( " ID {}", header.locationID ) )
                       << endmsg;
    }
    if ( auto sc = loader.resolveLinks( *pdata, header ); sc.isFailure() )
      return Unexpected{sc}; // we may want to continue here, and have the unpacker deal with this...
    PACKER packer( loader );
    auto   data = std::make_unique<typename PACKER::DataVector>();
    data->setVersion( pdata->version() );
    packer.unpack( *pdata, *data );
    return data;
  }

  template <class PACKER>
  Expected<std::unique_ptr<typename PACKER::DataVector>> restoreObject( PackedDataInBuffer const& buffer,
                                                                        Loader&                   loader ) {
    auto readBuffer =
        ReadBuffer{buffer}; // TODO: allow for emphemeral 'view' for reading without copying just to update 'pos'
    return LHCb::Hlt::PackedData::restoreObject<PACKER>( readBuffer, ObjectHeader{readBuffer}, loader );
  }

  template <class PACKER>
  Expected<std::pair<std::string, DataObject const*>> resolveObject( PackedDataInBuffer& buffer,
                                                                     ObjectHeader const& header, Loader& loader ) {
    auto loc = loader.decoder()(
        loader.decoder().remap_v2( header.locationID ).value_or( header.locationID ) ); // FIXME: have decoder 'rewrite'
                                                                                        // the tables if v2 instead...
    if ( !loc ) return Unexpected{ErrorCode::UNKNOWN_LOCATIONID};
    if ( auto obj = loader.get( *loc ); obj ) return std::pair{*loc, *obj};
    auto obj = restoreObject<PACKER>( buffer, header, loader );
    if ( !obj ) return Unexpected{obj.error()};
    auto ptr = obj->get();
    if ( auto sc = loader.put( *loc, std::move( *obj ) ); sc.isFailure() ) return Unexpected{sc};
    return std::pair{*loc, ptr};
  }

  template <class... PACKER>
  auto createLoaderFns() {
    std::map<CLID, LoaderFn_t> loaders;
    ( loaders.emplace( PACKER::PackedDataVector::classID(), &resolveObject<PACKER> ), ... );
    return loaders;
  }

  const std::map<CLID, LoaderFn_t> Loader::s_map =
      createLoaderFns<LHCb::TrackPacker, LHCb::RichPIDPacker, LHCb::MuonPIDPacker, LHCb::CaloHypoPacker,
                      LHCb::ProtoParticlePacker, LHCb::CaloClusterPacker, LHCb::ParticlePacker, LHCb::VertexPacker,
                      LHCb::RecVertexPacker, LHCb::FlavourTagPacker, LHCb::CaloDigitPacker, LHCb::CaloAdcPacker>();

} // namespace LHCb::Hlt::PackedData
/**
 *  Templated base algorithm for all unpacking algorithms
 *  except relations and MC objects
 *
 *  Note that the inheritance from Consumer and the void input are misleading.
 *  The algorithm is reading from and writing to TES, just via Handles so that
 *  it can deal with non existant input and with output failures, which is not
 *  authorized in the functional world.
 *  FIXME this should not be necessary, it's mainly due to misconfigurations in
 *  the tests
 *  Additionally packing requires the data objects to be in TES before one can add links to it
 *  https://gitlab.cern.ch/lhcb/LHCb/-/issues/180
 **/

namespace DataPacking::Buffer {
  class UnpackBase : public LHCb::Algorithm::Consumer<void( LHCb::Hlt::PackedData::MappedInBuffers const& )> {
  protected:
    ServiceHandle<IIndexedANNSvc> m_hltANNSvc{this, "ANNSvc", "HltANNSvc", "Service to resolve location IDs"};
    Gaudi::Property<bool>         m_emptyContainerForMissingBuffers{this, "WriteEmptyContainerIfBufferNotFound", true};
    Gaudi::Property<bool>         m_emptyContainerForFailedUnpacking{this, "WriteEmptyContainerIfUnpackingFails", true};
    Gaudi::Property<std::string>  m_anonymousLocationFormat{
        this, "AnonymousFormat", "/Event/DoNotUseExplicitly/AnonymousDependencyOnly/SourceID_{0}/0x{1:08x}"};

    mutable LHCb::Hlt::PackedData::Counters m_counters{this};

    DataObject* get( std::string const& location ) const {
      DataObject* fp = nullptr;
      return evtSvc()->retrieveObject( location, fp ).isSuccess() ? fp : nullptr;
    }

  public:
    UnpackBase( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer{name, pSvcLocator, {{"InputName", "/Event/DAQ/MappedDstData"}}} {}
  };

  template <class PACKER>
  class Unpack final : public UnpackBase {
    DataObjectWriteHandle<typename PACKER::DataVector> m_data{this, "OutputName", ""};

  public:
    using UnpackBase::UnpackBase;
    void operator()( LHCb::Hlt::PackedData::MappedInBuffers const& buffers ) const override {

      // First check whether this location has already been unpacked
      if ( get( m_data.fullKey().key() ) ) {
        m_counters.nothing_to_do += true;
        return;
      }
      m_counters.nothing_to_do += false;

      // check whether there is any content...
      if ( buffers.empty() ) {
        ++m_counters.no_buffers;
        return;
      }

      auto loader = LHCb::Hlt::PackedData::Loader{this, buffers, &m_counters, *m_hltANNSvc, m_anonymousLocationFormat};
      auto j      = loader.decoder()( m_data.fullKey().key() );
      if ( !j ) {
        ++m_counters.unknown;
        return;
      }

      if ( msgLevel( MSG::DEBUG ) ) {
        loader.walk(
            [&]( std::string const& loc, size_t id, auto* buf, size_t level ) {
              debug() << std::string( level * 4, ' ' );
              auto s = fmt::format( "{} (id={}) ", loc, id );
              debug() << s;
              auto sz = 4 * level + s.size();
              if ( sz < 60 ) debug() << std::string( 60 - sz, ' ' );
              auto tes = ( loader.get( loc ).value_or( nullptr ) != nullptr );
              if ( !buf && !tes ) {
                debug() << "NOT Available" << endmsg;
              } else {
                debug() << "Available in: " << ( buf ? " PackedBuffer" : "" ) << ( tes ? " TES" : "" ) << endmsg;
              }
            },
            *j );
      }

      const auto* buffer = buffers.find( *j );
      if ( !buffer ) {
        ++m_counters.absent;
        // FIXME: some tests want empty containers instead of no containers... so we allow
        // us to be configured accordingly... hrmpf.
        if ( m_emptyContainerForMissingBuffers )
          m_data.put( std::make_unique<typename PACKER::DataVector>() ); // really do not want to do this...
        return;
      }
      auto obj = LHCb::Hlt::PackedData::restoreObject<PACKER>( *buffer, loader );
      if ( !obj ) {
        if ( m_emptyContainerForFailedUnpacking ) {
          m_data.put( std::make_unique<typename PACKER::DataVector>() ); // really do not want to do this...
          warning() << "Unable to unpack " << m_data.fullKey().key() << ": " << obj.error().message()
                    << " -- created empty object instead" << endmsg;
        } else {
          error() << "Unable to unpack " << m_data.fullKey().key() << ": " << obj.error().message() << endmsg;
        }
      } else {
        // Count unpacked output
        m_counters.nbUnpackedData += size( **obj );
        m_counters.nbUnpackedData +=
            buffer->buffer().size(); // BUG: FIXME:  backwards bug-compatible. TODO: this must be a different counter..

        m_data.put( std::move( *obj ) );
      }
    }
  };

} // namespace DataPacking::Buffer
