/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/GenericBuffer.h"
#include "Event/PrimaryVertex.h"

#include "LHCbAlgs/Transformer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PackVertices
//
// 2015-10-01 : Wouter Hulsbergen
//-----------------------------------------------------------------------------
//=============================================================================
// better move this to a separate file, to keep it a bit clean
//=============================================================================

namespace {

  template <class Buffer, typename T>
  void writeFloat( Buffer& b, const T& x ) {
    float xtmp = x;
    b.write( xtmp );
  }

  template <class Buffer, typename T>
  void readFloat( Buffer& b, T& x ) {
    float xtmp;
    b.read( xtmp );
    x = xtmp;
  }

  template <class Buffer, typename T>
  void writeUInt32( Buffer& b, const T& x ) {
    unsigned int xtmp = x;
    b.write( xtmp );
  }

  template <class Buffer, typename T>
  void readUInt32( Buffer& b, T& x ) {
    uint32_t xtmp;
    b.read( xtmp );
    x = xtmp;
  }

  template <class Buffer, class Container>
  void writeContainer( Buffer& b, const Container& vec ) {
    writeUInt32( b, vec.size() );
    for ( const auto& i : vec ) i.write( b );
  }

  template <class Buffer, class Container>
  void readContainer( Buffer& b, Container& vec ) {
    size_t s;
    readUInt32( b, s );
    vec.resize( s );
    for ( size_t i = 0; i < s; ++i ) vec[i].read( b );
  }

  class CharVectorOStream {
  public:
    CharVectorOStream( std::vector<char>& buffer ) : m_buffer( buffer ) {}
    template <typename T>
    void write( const T& t ) {
      static_assert( std::is_trivially_copyable_v<T> );
      auto ptr = reinterpret_cast<const char*>( &t );
      m_buffer.insert( m_buffer.end(), ptr, ptr + sizeof( t ) );
    }

  private:
    std::vector<char>& m_buffer;
  };

  class CharVectorIStream {
  public:
    CharVectorIStream( const std::vector<char>& buffer ) : m_buffer( buffer ), m_pos( 0 ) {}
    template <typename T>
    void read( T& t ) {
      static_assert( std::is_trivially_copyable_v<T> );
      assert( m_pos + sizeof( t ) <= m_buffer.size() );
      std::memcpy( &t, m_buffer.data() + m_pos, sizeof( t ) );
      m_pos += sizeof( t );
    }
    size_t pos() const { return m_pos; }

  private:
    const std::vector<char>& m_buffer;
    size_t                   m_pos;
  };

} // namespace

//=============================================================================

namespace LHCb {
  template <class Buffer>
  void PrimaryVertexTrack::write( Buffer& buffer ) const {
    buffer.write( m_id );
    writeFloat( buffer, m_state( 0 ) );
    writeFloat( buffer, m_state( 1 ) );
    writeFloat( buffer, m_state( 2 ) );
    writeFloat( buffer, m_state( 3 ) );
    writeFloat( buffer, m_invcov( 0, 0 ) );
    writeFloat( buffer, m_invcov( 1, 0 ) );
    writeFloat( buffer, m_invcov( 1, 1 ) );
    writeFloat( buffer, m_weight );
  }

  template <class Buffer>
  void PrimaryVertexTrack::read( Buffer& buffer ) {
    buffer.read( m_id );
    readFloat( buffer, m_state( 0 ) );
    readFloat( buffer, m_state( 1 ) );
    readFloat( buffer, m_state( 2 ) );
    readFloat( buffer, m_state( 3 ) );
    readFloat( buffer, m_invcov( 0, 0 ) );
    readFloat( buffer, m_invcov( 1, 0 ) );
    readFloat( buffer, m_invcov( 1, 1 ) );
    readFloat( buffer, m_weight );
    // recompute derivatives and chi2
    initCache();
  }

  template <class Buffer>
  void PrimaryVertex::write( Buffer& b ) const {
    // vertex base
    writeUInt32( b, key() );
    writeFloat( b, position().x() );
    writeFloat( b, position().y() );
    writeFloat( b, position().z() );
    for ( int irow = 0; irow < 3; ++irow )
      for ( int icol = 0; icol <= irow; ++icol ) writeFloat( b, covMatrix()( irow, icol ) );
    writeFloat( b, chi2() );
    writeUInt32( b, nDoF() );
    // PV
    writeFloat( b, m_refZ );
    writeContainer( b, m_tracks );
  }

  template <class Buffer>
  void PrimaryVertex::read( Buffer& b ) {
    // vertex base
    int key;
    readUInt32( b, key );
    this->setKey( key );
    double x, y, z;
    readFloat( b, x );
    readFloat( b, y );
    readFloat( b, z );
    setPosition( Gaudi::XYZPoint( x, y, z ) );
    Gaudi::SymMatrix3x3 cov;
    for ( int irow = 0; irow < 3; ++irow )
      for ( int icol = 0; icol <= irow; ++icol ) readFloat( b, cov( irow, icol ) );
    setCovMatrix( cov );
    double chi2;
    int    ndof;
    readFloat( b, chi2 );
    readUInt32( b, ndof );
    this->setChi2AndDoF( chi2, ndof );
    // PV
    readFloat( b, m_refZ );
    readContainer( b, m_tracks );
    // recreate derivative sum
    initCache();
  }

  class PackPrimaryVertices : public Algorithm::Transformer<GenericBuffer( const PrimaryVertex::Range& )> {
  public:
    PackPrimaryVertices( const std::string& name, ISvcLocator* svc )
        : Transformer{name,
                      svc,
                      {"InputLocation", PrimaryVertexLocation::Default},
                      {"OutputLocation", PrimaryVertexLocation::Packed}} {}

    GenericBuffer operator()( PrimaryVertex::Range const& vertices ) const override {
      GenericBuffer     buffer;
      CharVectorOStream os{buffer.data()};
      os.write( m_version );
      unsigned int N = vertices.size();
      os.write( N );
      for ( const auto& v : vertices ) v->write( os );
      return buffer;
    }

  private:
    static constexpr int m_version{1};
  };

  class UnpackPrimaryVertices : public Algorithm::Transformer<PrimaryVertex::Container( const GenericBuffer& )> {
  public:
    UnpackPrimaryVertices( const std::string& name, ISvcLocator* svc )
        : Transformer{name,
                      svc,
                      {"InputLocation", PrimaryVertexLocation::Packed},
                      {"OutputLocation", PrimaryVertexLocation::Default}} {}

    PrimaryVertex::Container operator()( const GenericBuffer& buffer ) const override {
      PrimaryVertex::Container vertices;
      CharVectorIStream        is{buffer.data()};
      int                      version{0};
      is.read( version );
      if ( version != m_version ) {
        warning() << "Wrong version of persisted data" << endmsg;
        throw StatusCode::FAILURE;
      }
      unsigned int N{0};
      is.read( N );
      for ( unsigned int i = 0; i < N; ++i ) {
        auto pv = std::make_unique<PrimaryVertex>();
        pv->read( is );
        vertices.insert( pv.release() );
      }
      if ( is.pos() != buffer.data().size() ) {
        warning() << "Did not correctly decode PackedPVContainer" << endmsg;
        throw StatusCode::FAILURE;
      }
      return vertices;
    }

  private:
    static constexpr int m_version{1};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::PackPrimaryVertices, "PackPrimaryVertices" )
DECLARE_COMPONENT_WITH_ID( LHCb::UnpackPrimaryVertices, "UnpackPrimaryVertices" )
