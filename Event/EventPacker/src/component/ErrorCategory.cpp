/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ErrorCategory.h"

namespace LHCb::Packers {
  const char* ErrorCategory::name() const { return "Packers"; }
  bool        ErrorCategory::isRecoverable( StatusCode::code_t ) const { return false; }
  std::string ErrorCategory::message( StatusCode::code_t code ) const {
    switch ( static_cast<ErrorCode>( code ) ) {
    case ErrorCode::CONTAINER_NOT_FOUND:
      return "Could not retrieve container";
    case ErrorCode::KEY_NOT_FOUND:
      return "No object corresponding to specified key found in container";
    case ErrorCode::INCONSISTENT_SIZE:
      return "Bytes read does not match expected size";
    case ErrorCode::UNKNOWN_LOCATIONID:
      return "Unknown LocationID";
    case ErrorCode::MISSING_DEPENDENCY:
      return "Missing dependency";
    case ErrorCode::WRONG_CONTAINER_TYPE:
      return "Wrong container type";
    case ErrorCode::NO_LOADER_FOR_CLID:
      return "No Loader available for ClassID";
    case ErrorCode::NOT_ALL_DATA_USED:
      return "Not all packed data used";
    case ErrorCode::MISMATCHED_LINK_ID:
      return "LinkID mismatch";
    default:
      return StatusCode::default_category().message( code );
    }
  }
} // namespace LHCb::Packers

STATUSCODE_ENUM_IMPL( LHCb::Packers::ErrorCode, LHCb::Packers::ErrorCategory )
