/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedCaloDigit.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void CaloDigitPacker::pack( const Data& dig, PackedData& pdig, PackedDataVector& pdigs ) const {
  // check versions
  const auto pver = pdigs.packingVersion();

  if ( !isSupportedVer( pver ) ) { return; }
  // general
  pdig.key = dig.key().all();
  pdig.e   = StandardPacker::energy( dig.e() );
  if ( parent().msgLevel( MSG::DEBUG ) ) {
    parent().debug() << "Pack CaloDigit energy " << dig.e() << " key " << dig.key().all() << endmsg;
  }
}

void CaloDigitPacker::unpack( const PackedDataVector& pdigs, DataVector& digs ) const {
  if ( !isSupportedVer( pdigs.packingVersion() ) ) return;
  digs.reserve( pdigs.data().size() );
  for ( const auto& pdig : pdigs.data() ) {
    // make and save new digit container, with original key
    auto* dig = new Data();
    digs.insert( dig, LHCb::Detector::Calo::CellID( pdig.key ) );
    // general
    dig->setE( StandardPacker::energy( pdig.e ) );
  }
}

StatusCode CaloDigitPacker::check( const Data& dataA, const Data& dataB ) const {

  // assume OK from the start
  bool ok = true;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // checks here
  // key
  ok &= ch.compareInts( "key", dataA.key(), dataB.key() );
  // energy
  ok &= ch.compareEnergies( "energy", dataA.e(), dataB.e() );

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc =
        ( dataA.parent() && dataA.parent()->registry() ? dataA.parent()->registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with CaloDigit data packing :-" << endmsg << "  Original Digit key=" << dataA.key()
                       << " in '" << loc << "'" << endmsg << dataA << endmsg << "  Unpacked Digit" << endmsg << dataB
                       << endmsg;
  }
  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}
