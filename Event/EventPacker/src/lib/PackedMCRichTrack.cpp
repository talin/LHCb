/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedMCRichTrack.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void MCRichTrackPacker::pack( const DataVector& tracks, PackedDataVector& ptracks ) const {
  const auto ver = ptracks.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  ptracks.data().reserve( tracks.size() );
  for ( const auto* track : tracks ) {
    auto& ptrack = ptracks.data().emplace_back();

    ptrack.key = track->key();

    for ( const auto& S : track->mcSegments() ) {
      ptrack.mcSegments.emplace_back( 0 == ver ? StandardPacker::reference32( &parent(), &ptracks, S )
                                               : StandardPacker::reference64( &ptracks, S ) );
    }

    if ( track->mcParticle() ) {
      ptrack.mcParticle = ( 0 == ver ? StandardPacker::reference32( &parent(), &ptracks, track->mcParticle() )
                                     : StandardPacker::reference64( &ptracks, track->mcParticle() ) );
    }
  }
}

void MCRichTrackPacker::unpack( const PackedDataVector& ptracks, DataVector& tracks ) const {
  const auto ver = ptracks.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  tracks.reserve( ptracks.data().size() );
  for ( const auto& ptrack : ptracks.data() ) {
    // make and save new hit in container
    auto* track = new Data();
    tracks.insert( track, ptrack.key );

    int hintID( 0 ), key( 0 );

    for ( const auto& S : ptrack.mcSegments ) {
      if ( ( 0 != ver && StandardPacker::hintAndKey64( S, &ptracks, &tracks, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( S, &ptracks, &tracks, hintID, key ) ) ) {
        track->addToMcSegments( {&tracks, hintID, key} );
      } else {
        parent().error() << "Corrupt MCRichTrack MCRichSegment SmartRef detected." << endmsg;
      }
    }

    if ( -1 != ptrack.mcParticle ) {
      if ( ( 0 != ver && StandardPacker::hintAndKey64( ptrack.mcParticle, &ptracks, &tracks, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( ptrack.mcParticle, &ptracks, &tracks, hintID, key ) ) ) {
        track->setMcParticle( {&tracks, hintID, key} );
      } else {
        parent().error() << "Corrupt MCRichTrack MCParticle SmartRef detected." << endmsg;
      }
    }
  }
}

StatusCode MCRichTrackPacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  StatusCode sc = StatusCode::SUCCESS;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // Loop over data containers together and compare
  auto iA( dataA.begin() ), iB( dataB.begin() );
  for ( ; iA != dataA.end() && iB != dataB.end(); ++iA, ++iB ) {
    // assume OK from the start
    bool ok = true;
    // Key
    ok &= ch.compareInts( "Key", ( *iA )->key(), ( *iB )->key() );
    // MCParticle
    ok &= ch.comparePointers( "MCParticle", ( *iA )->mcParticle(), ( *iB )->mcParticle() );
    // MCSegments

    const bool sameSize = ch.compareInts( "#MCSegments", ( *iA )->mcSegments().size(), ( *iB )->mcSegments().size() );
    ok &= sameSize;
    if ( sameSize ) {
      auto jA( ( *iA )->mcSegments().begin() ), jB( ( *iB )->mcSegments().begin() );
      for ( ; jA != ( *iA )->mcSegments().end() && jB != ( *iB )->mcSegments().end(); ++jA, ++jB ) {
        ok &= ch.comparePointers( "MCSegment", jA->target(), jB->target() );
      }
    }

    // force printout for tests
    // ok = false;
    // If comparison not OK, print full information
    if ( !ok ) {
      parent().warning() << "Problem with MCRichTrack data packing :-" << endmsg << "  Original Track : " << **iA
                         << endmsg << "  Unpacked Track : " << **iB << endmsg;
      sc = StatusCode::FAILURE;
    }
  }

  // return
  return sc;
}
