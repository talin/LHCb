/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedCaloHypo.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;
using LHCb::Packer::Utils::safe_sqrt;

void CaloHypoPacker::pack( const Data& H, PackedData& pH, PackedDataVector& phypos ) const {

  const auto ver = phypos.packingVersion();
  // Save the data
  pH.key        = H.key();
  pH.hypothesis = H.hypothesis();
  pH.lh         = StandardPacker::fltPacked( H.lh() );
  if ( !H.position() ) {
    pH.z      = 0;
    pH.posX   = 0;
    pH.posY   = 0;
    pH.posE   = 0;
    pH.cov00  = 0;
    pH.cov10  = 0;
    pH.cov20  = 0;
    pH.cov11  = 0;
    pH.cov21  = 0;
    pH.cov22  = 0;
    pH.centX  = 0;
    pH.centY  = 0;
    pH.cerr00 = 0;
    pH.cerr10 = 0;
    pH.cerr11 = 0;
  } else {
    const auto* pos = H.position();
    pH.z            = StandardPacker::position( pos->z() );
    pH.posX         = StandardPacker::position( pos->x() );
    pH.posY         = StandardPacker::position( pos->y() );
    pH.posE         = StandardPacker::energy( pos->e() );

    // convariance Matrix
    const auto err0 = safe_sqrt( pos->covariance()( 0, 0 ) );
    const auto err1 = safe_sqrt( pos->covariance()( 1, 1 ) );
    const auto err2 = safe_sqrt( pos->covariance()( 2, 2 ) );
    pH.cov00        = StandardPacker::position( err0 );
    pH.cov11        = StandardPacker::position( err1 );
    pH.cov22        = StandardPacker::energy( err2 );
    pH.cov10        = StandardPacker::fraction( pos->covariance()( 1, 0 ), err1 * err0 );
    pH.cov20        = StandardPacker::fraction( pos->covariance()( 2, 0 ), err2 * err0 );
    pH.cov21        = StandardPacker::fraction( pos->covariance()( 2, 1 ), err2 * err1 );

    pH.centX = StandardPacker::position( pos->center()( 0 ) );
    pH.centY = StandardPacker::position( pos->center()( 1 ) );

    const auto serr0 = safe_sqrt( pos->spread()( 0, 0 ) );
    const auto serr1 = safe_sqrt( pos->spread()( 1, 1 ) );
    pH.cerr00        = StandardPacker::position( serr0 );
    pH.cerr11        = StandardPacker::position( serr1 );
    pH.cerr10        = StandardPacker::fraction( pos->spread()( 1, 0 ), serr1 * serr0 );
  }

  //== Store the CaloDigits
  pH.firstDigit = phypos.refs().size();
  if ( m_packDigitRefs ) {
    for ( const auto& dig : H.digits() ) {
      if ( dig.target() ) {
        phypos.refs().push_back(
            0 == ver ? StandardPacker::reference32( &parent(), &phypos, dig->parent(), dig->key().all() )
                     : StandardPacker::reference64( &phypos, dig->parent(), dig->key().all() ) );
      } else {
        parent().warning() << "Null CaloDigit SmartRef found" << endmsg;
      }
    }
  }
  pH.lastDigit = phypos.refs().size();

  //== Store the CaloClusters
  pH.firstCluster = phypos.refs().size();
  if ( m_packClusterRefs ) {
    for ( const auto& clu : H.clusters() ) {
      if ( clu.target() ) {
        phypos.refs().push_back( 0 == ver ? StandardPacker::reference32( &parent(), &phypos, clu )
                                          : StandardPacker::reference64( &phypos, clu ) );
      } else {
        parent().warning() << "Null CaloCluster SmartRef found" << endmsg;
      }
    }
  }
  pH.lastCluster = phypos.refs().size();

  //== Store the CaloHypos
  pH.firstHypo = phypos.refs().size();
  for ( const auto& iH : H.hypos() ) {
    if ( iH.target() ) {
      phypos.refs().push_back( 0 == ver ? StandardPacker::reference32( &parent(), &phypos, iH )
                                        : StandardPacker::reference64( &phypos, iH ) );
    } else {
      parent().warning() << "Null CaloCluster CaloHypo found" << endmsg;
    }
  }
  pH.lastHypo = phypos.refs().size();
}

void CaloHypoPacker::unpack( const PackedDataVector& phypos, DataVector& hypos ) const {

  // packing version
  const auto ver = phypos.packingVersion();
  if ( !isSupportedVer( ver ) ) return;

  hypos.reserve( phypos.data().size() );

  for ( const auto& src : phypos.data() ) {

    // make new unpacked object
    auto* hypo = new LHCb::CaloHypo();
    hypos.insert( hypo, src.key );

    // fill data objects
    hypo->setHypothesis( (LHCb::CaloHypo::Hypothesis)src.hypothesis );
    hypo->setLh( StandardPacker::fltPacked( src.lh ) );
    if ( 0 != src.z ) {
      auto pos = std::make_unique<LHCb::CaloPosition>();
      pos->setZ( StandardPacker::position( src.z ) );
      pos->setParameters( LHCb::CaloPosition::Parameters( StandardPacker::position( src.posX ),
                                                          StandardPacker::position( src.posY ),
                                                          StandardPacker::energy( src.posE ) ) );

      auto&      cov  = pos->covariance();
      const auto err0 = StandardPacker::position( src.cov00 );
      const auto err1 = StandardPacker::position( src.cov11 );
      const auto err2 = StandardPacker::energy( src.cov22 );
      cov( 0, 0 )     = err0 * err0;
      cov( 1, 0 )     = err1 * err0 * StandardPacker::fraction( src.cov10 );
      cov( 1, 1 )     = err1 * err1;
      cov( 2, 0 )     = err2 * err0 * StandardPacker::fraction( src.cov20 );
      cov( 2, 1 )     = err2 * err1 * StandardPacker::fraction( src.cov21 );
      cov( 2, 2 )     = err2 * err2;

      pos->setCenter(
          LHCb::CaloPosition::Center( StandardPacker::position( src.centX ), StandardPacker::position( src.centY ) ) );

      auto&      spr   = pos->spread();
      const auto serr0 = StandardPacker::position( src.cerr00 );
      const auto serr1 = StandardPacker::position( src.cerr11 );
      spr( 0, 0 )      = serr0 * serr0;
      spr( 1, 0 )      = serr1 * serr0 * StandardPacker::fraction( src.cerr10 );
      spr( 1, 1 )      = serr1 * serr1;

      hypo->setPosition( std::move( pos ) );
    }

    int hintID( 0 ), key( 0 );
    for ( const auto reference : Packer::subrange( phypos.refs(), src.firstDigit, src.lastDigit ) ) {
      if ( ( 0 != ver && StandardPacker::hintAndKey64( reference, &phypos, &hypos, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( reference, &phypos, &hypos, hintID, key ) ) ) {
        hypo->addToDigits( {&hypos, hintID, key} );
      } else {
        parent().error() << "Corrupt CaloHypo CaloDigit SmartRef detected." << endmsg;
      }
    }
    for ( const auto reference : Packer::subrange( phypos.refs(), src.firstCluster, src.lastCluster ) ) {
      if ( ( 0 != ver && StandardPacker::hintAndKey64( reference, &phypos, &hypos, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( reference, &phypos, &hypos, hintID, key ) ) ) {
        hypo->addToClusters( {&hypos, hintID, key} );
      } else {
        parent().error() << "Corrupt CaloHypo CaloCluster SmartRef detected." << endmsg;
      }
    }
    for ( const auto reference : Packer::subrange( phypos.refs(), src.firstHypo, src.lastHypo ) ) {
      if ( ( 0 != ver && StandardPacker::hintAndKey64( reference, &phypos, &hypos, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( reference, &phypos, &hypos, hintID, key ) ) ) {
        hypo->addToHypos( {&hypos, hintID, key} );
      } else {
        parent().error() << "Corrupt CaloHypo CaloHypo SmartRef detected." << endmsg;
      }
    }

  } // loop over hypos
}

StatusCode CaloHypoPacker::check( const Data& oHypo, const Data& tHypo ) const {

  StatusCode sc = StatusCode::SUCCESS;
  // assume OK from the start
  bool ok = true;
  // checker
  const DataPacking::DataChecks ch( parent() );

  // key
  ok &= ch.compareInts( "key", oHypo.key(), tHypo.key() );
  ok &= ch.compareInts( "key", oHypo.hypothesis(), tHypo.hypothesis() );
  ok &= ch.compare( "lh", oHypo.lh(), tHypo.lh(), 1.e-7 );

  if ( !( oHypo.position() || tHypo.position() ) )
    ok &= ch.comparePointers( "Position", oHypo.position(), tHypo.position() );

  if ( oHypo.position() != 0 && tHypo.position() != 0 ) {
    ok &= ch.comparePositions( "pos_x", oHypo.position()->x(), tHypo.position()->x() );
    ok &= ch.comparePositions( "pos_y", oHypo.position()->y(), tHypo.position()->y() );
    ok &= ch.comparePositions( "pos_z", oHypo.position()->z(), tHypo.position()->z() );
    ok &= ch.compareEnergies( "pos_e", oHypo.position()->e(), tHypo.position()->e() );

    ok &= ch.comparePositions( "center0", oHypo.position()->center()( 0 ), tHypo.position()->center()( 0 ) );
    ok &= ch.comparePositions( "center1", oHypo.position()->center()( 1 ), tHypo.position()->center()( 1 ) );

    // -------------------------------------------------------------------------
    const std::array<double, 3> tolDiag3 = {{Packer::POSITION_TOL, Packer::POSITION_TOL, Packer::ENERGY_TOL}};

    const std::array<double, 2> tolDiag2 = {{Packer::POSITION_TOL, Packer::POSITION_TOL}};

    ok &= ch.compareCovMatrices<Gaudi::SymMatrix3x3, 3>(
        "Covariance", oHypo.position()->covariance(), tHypo.position()->covariance(), tolDiag3, Packer::FRACTION_TOL );

    ok &= ch.compareCovMatrices<Gaudi::SymMatrix2x2, 2>( "Spread", oHypo.position()->spread(),
                                                         tHypo.position()->spread(), tolDiag2, Packer::FRACTION_TOL );

    auto frac = []( auto el, auto e1, auto e2, bool cov = true ) {
      if ( cov ) {
        auto diag1   = safe_sqrt( el.position()->covariance()( e1, e1 ) );
        auto diag2   = safe_sqrt( el.position()->covariance()( e2, e2 ) );
        auto offdiag = safe_sqrt( el.position()->covariance()( e1, e2 ) );
        return offdiag / diag1 / diag2;
      } else {
        auto diag1   = safe_sqrt( el.position()->spread()( e1, e1 ) );
        auto diag2   = safe_sqrt( el.position()->spread()( e2, e2 ) );
        auto offdiag = safe_sqrt( el.position()->spread()( e1, e2 ) );
        return offdiag / diag1 / diag2;
      };
    };

    ok &= ch.compare( "Fraction10", frac( oHypo, 1, 0 ), frac( tHypo, 1, 0 ), 2.e-4 );
    ok &= ch.compare( "Fraction20", frac( oHypo, 2, 0 ), frac( tHypo, 2, 0 ), 2.e-4 );
    ok &= ch.compare( "Fraction21", frac( oHypo, 2, 1 ), frac( tHypo, 2, 1 ), 2.e-4 );
    ok &= ch.compare( "FractionSpread", frac( oHypo, 1, 0, false ), frac( tHypo, 1, 0, false ), 2.e-4 );
  };

  // -------------------------------------------------------------------------
  // Digits
  const bool dOK = oHypo.digits().size() == tHypo.digits().size();
  ok &= dOK;
  if ( dOK ) {
    for ( unsigned int kk = 0; oHypo.digits().size() > kk; kk++ ) {
      const LHCb::CaloDigit* dum  = oHypo.digits()[kk]; // convert smartref to pointers
      const LHCb::CaloDigit* dum1 = tHypo.digits()[kk];
      ok &= ch.comparePointers( "Digits", dum, dum1 );
    }
  }
  // Clusters
  const bool cOK = oHypo.clusters().size() == tHypo.clusters().size();
  ok &= cOK;
  if ( cOK ) {
    for ( unsigned int kk = 0; oHypo.clusters().size() > kk; kk++ ) {
      const LHCb::CaloCluster* dum  = oHypo.clusters()[kk]; // convert smartref to pointers
      const LHCb::CaloCluster* dum1 = tHypo.clusters()[kk];
      ok &= ch.comparePointers( "Clusters", dum, dum1 );
    }
  }

  // Hypos
  const bool hOK = oHypo.hypos().size() == tHypo.hypos().size();
  ok &= hOK;
  if ( hOK ) {
    for ( unsigned int kk = 0; oHypo.hypos().size() > kk; kk++ ) {
      const LHCb::CaloHypo* dum  = oHypo.hypos()[kk]; // convert smartref to pointers
      const LHCb::CaloHypo* dum1 = tHypo.hypos()[kk];
      ok &= ch.comparePointers( "Hypos", dum, dum1 );
    }
  }

  if ( !ok || MSG::DEBUG >= parent().msgLevel() ) {
    parent().warning() << "Problem with CaloHypo data packing :-" << endmsg;
    parent().warning() << endmsg;
    parent().warning() << "  Original CaloHypo key=" << oHypo.key() << endmsg << oHypo << endmsg
                       << "  Unpacked CaloHypo" << endmsg << tHypo << endmsg;
  }
  return sc;
}
