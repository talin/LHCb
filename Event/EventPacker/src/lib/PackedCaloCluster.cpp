/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedCaloCluster.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;
using LHCb::Packer::Utils::safe_sqrt;

void CaloClusterPacker::pack( const Data& clu, PackedData& pclu, PackedDataVector& pclus ) const {

  // general
  pclu.key  = clu.key();
  pclu.type = (int)clu.type();
  pclu.seed = clu.seed().all();

  // position object
  pclu.pos_x = StandardPacker::position( clu.position().x() );
  pclu.pos_y = StandardPacker::position( clu.position().y() );
  pclu.pos_z = StandardPacker::position( clu.position().z() );
  pclu.pos_e = StandardPacker::energy( clu.position().e() );

  pclu.pos_c0 = StandardPacker::position( clu.position().center()[0] );
  pclu.pos_c1 = StandardPacker::position( clu.position().center()[1] );

  const auto err0 = safe_sqrt( clu.position().covariance()( 0, 0 ) );
  const auto err1 = safe_sqrt( clu.position().covariance()( 1, 1 ) );
  const auto err2 = safe_sqrt( clu.position().covariance()( 2, 2 ) );
  pclu.pos_cov00  = StandardPacker::position( err0 );
  pclu.pos_cov11  = StandardPacker::position( err1 );
  pclu.pos_cov22  = StandardPacker::energy( err2 );
  pclu.pos_cov10  = StandardPacker::fraction( clu.position().covariance()( 1, 0 ), err1 * err0 );
  pclu.pos_cov20  = StandardPacker::fraction( clu.position().covariance()( 2, 0 ), err2 * err0 );
  pclu.pos_cov21  = StandardPacker::fraction( clu.position().covariance()( 2, 1 ), err2 * err1 );

  const auto serr0  = safe_sqrt( clu.position().spread()( 0, 0 ) );
  const auto serr1  = safe_sqrt( clu.position().spread()( 1, 1 ) );
  pclu.pos_spread00 = StandardPacker::position( serr0 );
  pclu.pos_spread11 = StandardPacker::position( serr1 );
  pclu.pos_spread10 = StandardPacker::fraction( clu.position().spread()( 1, 0 ), serr1 * serr0 );

  // entries
  pclu.firstEntry = pclus.entries().size();
  if ( m_packDigitRefs ) {
    pclus.entries().reserve( pclus.entries().size() + clu.entries().size() );
    for ( const auto& En : clu.entries() ) {
      pclus.entries().emplace_back();
      auto& pEnt = pclus.entries().back();
      if ( En.digit().target() ) {
        pEnt.digit = StandardPacker::reference64( &pclus, En.digit()->parent(), En.digit()->key().all() );
      }
      pEnt.status   = En.status().data();
      pEnt.fraction = StandardPacker::fraction( En.fraction() );
    }
  }
  pclu.lastEntry = pclus.entries().size();
}

void CaloClusterPacker::unpack( const PackedDataVector& pclus, DataVector& clus ) const {
  if ( !isSupportedVer( pclus.packingVersion() ) ) return;
  clus.reserve( pclus.data().size() );
  for ( const auto& pclu : pclus.data() ) {
    // make and save new clUster container, with original key
    auto* clu = new Data();
    clus.insert( clu, pclu.key );

    // Fill data from packed object
    // general
    clu->setType( (LHCb::CaloCluster::Type)pclu.type );
    clu->setSeed( LHCb::Detector::Calo::CellID( pclu.seed ) );

    // position
    typedef LHCb::CaloPosition CaloP;

    clu->position().setZ( StandardPacker::position( pclu.pos_z ) );
    clu->position().setParameters( CaloP::Parameters( StandardPacker::position( pclu.pos_x ),
                                                      StandardPacker::position( pclu.pos_y ),
                                                      StandardPacker::energy( pclu.pos_e ) ) );

    clu->position().setCenter(
        CaloP::Center( StandardPacker::position( pclu.pos_c0 ), StandardPacker::position( pclu.pos_c1 ) ) );

    auto&      cov  = clu->position().covariance();
    const auto err0 = StandardPacker::position( pclu.pos_cov00 );
    const auto err1 = StandardPacker::position( pclu.pos_cov11 );
    const auto err2 = StandardPacker::energy( pclu.pos_cov22 );
    cov( 0, 0 )     = err0 * err0;
    cov( 1, 0 )     = err1 * err0 * StandardPacker::fraction( pclu.pos_cov10 );
    cov( 1, 1 )     = err1 * err1;
    cov( 2, 0 )     = err2 * err0 * StandardPacker::fraction( pclu.pos_cov20 );
    cov( 2, 1 )     = err2 * err1 * StandardPacker::fraction( pclu.pos_cov21 );
    cov( 2, 2 )     = err2 * err2;

    auto&      spr   = clu->position().spread();
    const auto serr0 = StandardPacker::position( pclu.pos_spread00 );
    const auto serr1 = StandardPacker::position( pclu.pos_spread11 );
    spr( 0, 0 )      = serr0 * serr0;
    spr( 1, 0 )      = serr1 * serr0 * StandardPacker::fraction( pclu.pos_spread10 );
    spr( 1, 1 )      = serr1 * serr1;

    // entries
    clu->entries().reserve( pclu.lastEntry - pclu.firstEntry );
    for ( auto iE = pclu.firstEntry; iE < pclu.lastEntry; ++iE ) {
      // get the packed entry
      const auto& pEnt = pclus.entries()[iE];
      // make a new unpacked one
      auto& ent = clu->entries().emplace_back();
      // Set data
      if ( -1 != pEnt.digit ) {
        int hintID( 0 ), key( 0 );
        if ( StandardPacker::hintAndKey64( pEnt.digit, &pclus, &clus, hintID, key ) ) {
          ent.setDigit( LHCb::CaloClusterEntry::Digit( &clus, hintID, key ) );
        } else {
          parent().error() << "Corrupt CaloCluster Digit SmartRef found" << endmsg;
        }
      }
      ent.setStatus( LHCb::CaloDigitStatus::Status{pEnt.status} );
      ent.setFraction( StandardPacker::fraction( pEnt.fraction ) );
    }
  }
}

StatusCode CaloClusterPacker::check( const Data& dataA, const Data& dataB ) const {
  // assume OK from the start
  bool ok = true;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // checks here

  // key
  ok &= ch.compareInts( "key", dataA.key(), dataB.key() );
  // type
  ok &= ch.compareInts( "type", dataA.type(), dataB.type() );
  // seed
  ok &= ch.compareInts( "seed", dataA.seed().all(), dataB.seed().all() );

  // 'positions'
  ok &= ch.comparePositions( "Position-X", dataA.position().x(), dataB.position().x() );
  ok &= ch.comparePositions( "Position-Y", dataA.position().y(), dataB.position().y() );
  ok &= ch.comparePositions( "Position-Z", dataA.position().z(), dataB.position().z() );
  ok &= ch.compareEnergies( "Position-E", dataA.position().e(), dataB.position().e() );
  ok &= ch.compareVectors( "Position-Center", dataA.position().center(), dataB.position().center() );

  // Same checks as in caloHypo for covariance and spread
  // -------------------------------------------------------------------------
  const std::array<double, 3> tolDiag3 = {{Packer::POSITION_TOL, Packer::POSITION_TOL, Packer::ENERGY_TOL}};
  const std::array<double, 2> tolDiag2 = {{Packer::POSITION_TOL, Packer::POSITION_TOL}};

  ok &= ch.compareCovMatrices<Gaudi::SymMatrix3x3, 3>( "Covariance", dataA.position().covariance(),
                                                       dataB.position().covariance(), tolDiag3, Packer::FRACTION_TOL );

  ok &= ch.compareCovMatrices<Gaudi::SymMatrix2x2, 2>( "Spread", dataA.position().spread(), dataB.position().spread(),
                                                       tolDiag2, Packer::FRACTION_TOL );

  auto frac = []( auto el, auto e1, auto e2, bool cov = true ) {
    if ( cov ) {
      auto diag1   = safe_sqrt( el.position().covariance()( e1, e1 ) );
      auto diag2   = safe_sqrt( el.position().covariance()( e2, e2 ) );
      auto offdiag = safe_sqrt( el.position().covariance()( e1, e2 ) );
      return offdiag / diag1 / diag2;
    } else {
      auto diag1   = safe_sqrt( el.position().spread()( e1, e1 ) );
      auto diag2   = safe_sqrt( el.position().spread()( e2, e2 ) );
      auto offdiag = safe_sqrt( el.position().spread()( e1, e2 ) );
      return offdiag / diag1 / diag2;
    };
  };

  ok &= ch.compare( "Fraction10", frac( dataA, 1, 0 ), frac( dataB, 1, 0 ), 2.e-4 );
  ok &= ch.compare( "Fraction20", frac( dataA, 2, 0 ), frac( dataB, 2, 0 ), 2.e-4 );
  ok &= ch.compare( "Fraction21", frac( dataA, 2, 1 ), frac( dataB, 2, 1 ), 2.e-4 );
  ok &= ch.compare( "FractionSpread", frac( dataA, 1, 0, false ), frac( dataB, 1, 0, false ), 2.e-4 );

  // -------------------------------------------------------------------------

  // Entries
  const bool entsSizeOK = dataA.entries().size() == dataB.entries().size();
  ok &= entsSizeOK;
  if ( entsSizeOK ) {
    auto iEA( dataA.entries().begin() ), iEB( dataB.entries().begin() );
    for ( ; iEA != dataA.entries().end() && iEB != dataB.entries().end(); ++iEA, ++iEB ) {
      ok &= ch.compareInts( "Digits", ( *iEA ).digit(), ( *iEB ).digit() );
      ok &= ch.compareInts( "Status", ( *iEA ).status(), ( *iEB ).status() );
      ok &= ch.compareFractions( "Entry-Fraction", ( *iEA ).fraction(), ( *iEB ).fraction() );
    }
  }

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc =
        ( dataA.parent() && dataA.parent()->registry() ? dataA.parent()->registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with CaloCluster data packing :-" << endmsg
                       << "  Original Cluster key=" << dataA.key() << " in '" << loc << "'" << endmsg << dataA << endmsg
                       << "  Unpacked Cluster" << endmsg << dataB << endmsg;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}
