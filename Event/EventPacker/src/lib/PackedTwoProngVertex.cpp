/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedTwoProngVertex.h"
#include "Event/PackedEventChecks.h"

#include <algorithm>

using namespace LHCb;

void TwoProngVertexPacker::pack( const Data& vert, PackedData& pvert, PackedDataVector& pverts ) const {
  if ( !isSupportedVer( pverts.packingVersion() ) ) return;

  // Key -- fixme -- how do we really want the key???
  pvert.key = StandardPacker::reference64( &pverts, &vert );

  pvert.key       = vert.key();
  pvert.technique = vert.technique();
  pvert.chi2      = StandardPacker::fltPacked( vert.chi2() );
  pvert.nDoF      = vert.nDoF();
  pvert.x         = StandardPacker::position( vert.position().x() );
  pvert.y         = StandardPacker::position( vert.position().y() );
  pvert.z         = StandardPacker::position( vert.position().z() );

  double pA = 1. / vert.momA()( 2 );
  double pB = 1. / vert.momB()( 2 );

  pvert.txA = StandardPacker::slope( vert.momA()( 0 ) );
  pvert.tyA = StandardPacker::slope( vert.momA()( 1 ) );
  pvert.pA  = StandardPacker::energy( pA );

  pvert.txB = StandardPacker::slope( vert.momB()( 0 ) );
  pvert.tyB = StandardPacker::slope( vert.momB()( 1 ) );
  pvert.pB  = StandardPacker::energy( pB );

  // convariance Matrix
  double err0 = std::sqrt( vert.covMatrix()( 0, 0 ) );
  double err1 = std::sqrt( vert.covMatrix()( 1, 1 ) );
  double err2 = std::sqrt( vert.covMatrix()( 2, 2 ) );
  double err3 = std::sqrt( vert.momcovA()( 0, 0 ) );
  double err4 = std::sqrt( vert.momcovA()( 1, 1 ) );
  double err5 = std::sqrt( vert.momcovA()( 2, 2 ) );
  double err6 = std::sqrt( vert.momcovB()( 0, 0 ) );
  double err7 = std::sqrt( vert.momcovB()( 1, 1 ) );
  double err8 = std::sqrt( vert.momcovB()( 2, 2 ) );

  pvert.cov00 = StandardPacker::position( err0 );
  pvert.cov11 = StandardPacker::position( err1 );
  pvert.cov22 = StandardPacker::position( err2 );
  pvert.cov33 = StandardPacker::slope( err3 );
  pvert.cov44 = StandardPacker::slope( err4 );
  pvert.cov55 = StandardPacker::energy( err5 * std::abs( pA ) * 1.e5 );
  pvert.cov66 = StandardPacker::slope( err6 );
  pvert.cov77 = StandardPacker::slope( err7 );
  pvert.cov88 = StandardPacker::energy( err8 * std::abs( pB ) * 1.e5 );

  pvert.cov10 = StandardPacker::fraction( vert.covMatrix()( 1, 0 ) / err1 / err0 );
  pvert.cov20 = StandardPacker::fraction( vert.covMatrix()( 2, 0 ) / err2 / err0 );
  pvert.cov21 = StandardPacker::fraction( vert.covMatrix()( 2, 1 ) / err2 / err1 );

  pvert.cov30 = StandardPacker::fraction( vert.momposcovA()( 0, 0 ) / err3 / err0 );
  pvert.cov31 = StandardPacker::fraction( vert.momposcovA()( 0, 1 ) / err3 / err1 );
  pvert.cov32 = StandardPacker::fraction( vert.momposcovA()( 0, 2 ) / err3 / err2 );

  pvert.cov40 = StandardPacker::fraction( vert.momposcovA()( 1, 0 ) / err4 / err0 );
  pvert.cov41 = StandardPacker::fraction( vert.momposcovA()( 1, 1 ) / err4 / err1 );
  pvert.cov42 = StandardPacker::fraction( vert.momposcovA()( 1, 2 ) / err4 / err2 );
  pvert.cov43 = StandardPacker::fraction( vert.momcovA()( 1, 0 ) / err4 / err3 );

  pvert.cov50 = StandardPacker::fraction( vert.momposcovA()( 2, 0 ) / err5 / err0 );
  pvert.cov51 = StandardPacker::fraction( vert.momposcovA()( 2, 1 ) / err5 / err1 );
  pvert.cov52 = StandardPacker::fraction( vert.momposcovA()( 2, 2 ) / err5 / err2 );
  pvert.cov53 = StandardPacker::fraction( vert.momcovA()( 2, 0 ) / err5 / err3 );
  pvert.cov54 = StandardPacker::fraction( vert.momcovA()( 2, 1 ) / err5 / err4 );

  pvert.cov60 = StandardPacker::fraction( vert.momposcovB()( 0, 0 ) / err6 / err0 );
  pvert.cov61 = StandardPacker::fraction( vert.momposcovB()( 0, 1 ) / err6 / err1 );
  pvert.cov62 = StandardPacker::fraction( vert.momposcovB()( 0, 2 ) / err6 / err2 );
  pvert.cov63 = StandardPacker::fraction( vert.mommomcov()( 0, 0 ) / err6 / err3 );
  pvert.cov64 = StandardPacker::fraction( vert.mommomcov()( 0, 1 ) / err6 / err4 );
  pvert.cov65 = StandardPacker::fraction( vert.mommomcov()( 0, 2 ) / err6 / err5 );

  pvert.cov70 = StandardPacker::fraction( vert.momposcovB()( 1, 0 ) / err7 / err0 );
  pvert.cov71 = StandardPacker::fraction( vert.momposcovB()( 1, 1 ) / err7 / err1 );
  pvert.cov72 = StandardPacker::fraction( vert.momposcovB()( 1, 2 ) / err7 / err2 );
  pvert.cov73 = StandardPacker::fraction( vert.mommomcov()( 1, 0 ) / err7 / err3 );
  pvert.cov74 = StandardPacker::fraction( vert.mommomcov()( 1, 1 ) / err7 / err4 );
  pvert.cov75 = StandardPacker::fraction( vert.mommomcov()( 1, 2 ) / err7 / err5 );
  pvert.cov76 = StandardPacker::fraction( vert.momcovB()( 1, 0 ) / err7 / err6 );

  pvert.cov80 = StandardPacker::fraction( vert.momposcovB()( 2, 0 ) / err8 / err0 );
  pvert.cov81 = StandardPacker::fraction( vert.momposcovB()( 2, 1 ) / err8 / err1 );
  pvert.cov82 = StandardPacker::fraction( vert.momposcovB()( 2, 2 ) / err8 / err2 );
  pvert.cov83 = StandardPacker::fraction( vert.mommomcov()( 2, 0 ) / err8 / err3 );
  pvert.cov84 = StandardPacker::fraction( vert.mommomcov()( 2, 1 ) / err8 / err4 );
  pvert.cov85 = StandardPacker::fraction( vert.mommomcov()( 2, 2 ) / err8 / err5 );
  pvert.cov86 = StandardPacker::fraction( vert.momcovB()( 2, 0 ) / err8 / err6 );
  pvert.cov87 = StandardPacker::fraction( vert.momcovB()( 2, 1 ) / err8 / err7 );

  //== Store the Tracks
  pvert.firstTrack = pverts.refs().size();
  for ( auto itT = vert.tracks().begin(); vert.tracks().end() != itT; ++itT ) {
    pverts.refs().push_back( StandardPacker::reference64( &pverts, *itT ) );
  }
  pvert.lastTrack = pverts.refs().size();

  //== Store the ParticleID
  pvert.firstPid = pverts.refs().size();
  for ( auto itP = vert.compatiblePIDs().begin(); vert.compatiblePIDs().end() != itP; ++itP ) {
    pverts.refs().emplace_back( ( *itP ).pid() );
  }
  pvert.lastPid = pverts.refs().size();

  //== Handles the ExtraInfo
  pvert.firstInfo = pverts.extras().size();
  for ( const auto& itE : vert.extraInfo() ) {
    pverts.extras().emplace_back( itE.first, StandardPacker::fltPacked( itE.second ) );
  }
  pvert.lastInfo = pverts.extras().size();
}

void TwoProngVertexPacker::unpack( const PackedData& pvert, Data& vert, const PackedDataVector& pverts,
                                   DataVector& verts ) const {
  if ( !isSupportedVer( pverts.packingVersion() ) ) return;
  const auto pVer = pverts.packingVersion();

  vert.setTechnique( (RecVertex::RecVertexType)pvert.technique );
  vert.setChi2AndDoF( StandardPacker::fltPacked( pvert.chi2 ), pvert.nDoF );
  vert.setPosition( Gaudi::XYZPoint( StandardPacker::position( pvert.x ), StandardPacker::position( pvert.y ),
                                     StandardPacker::position( pvert.z ) ) );

  //== Store the Tracks
  int hintID{0};
  int key{0};

  for ( auto ref : Packer::subrange( pverts.refs(), pvert.firstTrack, pvert.lastTrack ) ) {
    if ( ( 0 == pVer && StandardPacker::hintAndKey32( ref, &pverts, &verts, hintID, key ) ) ||
         ( 0 != pVer && StandardPacker::hintAndKey64( ref, &pverts, &verts, hintID, key ) ) ) {
      SmartRef<Track> ref( &verts, hintID, key );
      vert.addToTracks( ref );
    } else {
      parent().error() << "Corrupt TwoProngVertex Track SmartRef detected." << endmsg;
    }
  }

  //== Handles the ExtraInfo
  for ( const auto& [k, v] : Packer::subrange( pverts.extras(), pvert.firstInfo, pvert.lastInfo ) ) {
    vert.addInfo( k, StandardPacker::fltPacked( v ) );
  }
  //== Momentum of the two prongs
  const auto pA = StandardPacker::energy( pvert.pA );
  const auto pB = StandardPacker::energy( pvert.pB );
  vert.setMomA( ROOT::Math::SVector<double, 3>( StandardPacker::slope( pvert.txA ), StandardPacker::slope( pvert.tyA ),
                                                1.0 / pA ) );

  vert.setMomB( ROOT::Math::SVector<double, 3>( StandardPacker::slope( pvert.txB ), StandardPacker::slope( pvert.tyB ),
                                                1.0 / pB ) );

  // convariance Matrix
  const auto err0 = StandardPacker::position( pvert.cov00 );
  const auto err1 = StandardPacker::position( pvert.cov11 );
  const auto err2 = StandardPacker::position( pvert.cov22 );
  const auto err3 = StandardPacker::slope( pvert.cov33 );
  const auto err4 = StandardPacker::slope( pvert.cov44 );
  const auto err5 = StandardPacker::energy( pvert.cov55 ) / std::abs( pA ) * 1.e-5;
  const auto err6 = StandardPacker::slope( pvert.cov66 );
  const auto err7 = StandardPacker::slope( pvert.cov77 );
  const auto err8 = StandardPacker::energy( pvert.cov88 ) / std::abs( pB ) * 1.e-5;

  Gaudi::SymMatrix3x3 cov;
  cov( 0, 0 ) = err0 * err0;
  cov( 1, 0 ) = err1 * err0 * StandardPacker::fraction( pvert.cov10 );
  cov( 1, 1 ) = err1 * err1;
  cov( 2, 0 ) = err2 * err0 * StandardPacker::fraction( pvert.cov20 );
  cov( 2, 1 ) = err2 * err1 * StandardPacker::fraction( pvert.cov21 );
  cov( 2, 2 ) = err2 * err2;
  vert.setCovMatrix( cov );

  cov( 0, 0 )    = err3 * err3;
  cov( 1, 0 )    = err4 * err3 * StandardPacker::fraction( pvert.cov43 );
  cov( 1, 1 )    = err4 * err4;
  cov( 2, 0 )    = err5 * err3 * StandardPacker::fraction( pvert.cov53 );
  cov( 2, 1 )    = err5 * err4 * StandardPacker::fraction( pvert.cov54 );
  cov( 2, 2 )    = err5 * err5;
  vert.momcovA() = cov;

  cov( 0, 0 )    = err6 * err6;
  cov( 1, 0 )    = err7 * err6 * StandardPacker::fraction( pvert.cov76 );
  cov( 1, 1 )    = err7 * err7;
  cov( 2, 0 )    = err8 * err6 * StandardPacker::fraction( pvert.cov86 );
  cov( 2, 1 )    = err8 * err7 * StandardPacker::fraction( pvert.cov87 );
  cov( 2, 2 )    = err8 * err8;
  vert.momcovB() = cov;

  ROOT::Math::SMatrix<double, 3, 3> mat;
  mat( 0, 0 )       = err3 * err0 * StandardPacker::fraction( pvert.cov30 );
  mat( 0, 1 )       = err3 * err1 * StandardPacker::fraction( pvert.cov31 );
  mat( 0, 2 )       = err3 * err2 * StandardPacker::fraction( pvert.cov32 );
  mat( 1, 0 )       = err4 * err0 * StandardPacker::fraction( pvert.cov40 );
  mat( 1, 1 )       = err4 * err1 * StandardPacker::fraction( pvert.cov41 );
  mat( 1, 2 )       = err4 * err2 * StandardPacker::fraction( pvert.cov42 );
  mat( 2, 0 )       = err5 * err0 * StandardPacker::fraction( pvert.cov50 );
  mat( 2, 1 )       = err5 * err1 * StandardPacker::fraction( pvert.cov51 );
  mat( 2, 2 )       = err5 * err2 * StandardPacker::fraction( pvert.cov52 );
  vert.momposcovA() = mat;

  mat( 0, 0 ) = err6 * err0 * StandardPacker::fraction( pvert.cov60 );
  mat( 0, 1 ) = err6 * err1 * StandardPacker::fraction( pvert.cov61 );
  mat( 0, 2 ) = err6 * err2 * StandardPacker::fraction( pvert.cov62 );
  mat( 1, 0 ) = err7 * err0 * StandardPacker::fraction( pvert.cov70 );
  mat( 1, 1 ) = err7 * err1 * StandardPacker::fraction( pvert.cov71 );
  mat( 1, 2 ) = err7 * err2 * StandardPacker::fraction( pvert.cov72 );
  mat( 2, 0 ) = err8 * err0 * StandardPacker::fraction( pvert.cov80 );
  mat( 2, 1 ) = err8 * err1 * StandardPacker::fraction( pvert.cov81 );
  mat( 2, 2 ) = err8 * err2 * StandardPacker::fraction( pvert.cov82 );

  vert.momposcovB() = mat;
  mat( 0, 0 )       = err6 * err3 * StandardPacker::fraction( pvert.cov63 );
  mat( 0, 1 )       = err6 * err4 * StandardPacker::fraction( pvert.cov64 );
  mat( 0, 2 )       = err6 * err5 * StandardPacker::fraction( pvert.cov65 );
  mat( 1, 0 )       = err7 * err3 * StandardPacker::fraction( pvert.cov73 );
  mat( 1, 1 )       = err7 * err4 * StandardPacker::fraction( pvert.cov74 );
  mat( 1, 2 )       = err7 * err5 * StandardPacker::fraction( pvert.cov75 );
  mat( 2, 0 )       = err8 * err3 * StandardPacker::fraction( pvert.cov83 );
  mat( 2, 1 )       = err8 * err4 * StandardPacker::fraction( pvert.cov84 );
  mat( 2, 2 )       = err8 * err5 * StandardPacker::fraction( pvert.cov85 );
  vert.mommomcov()  = mat;
  //== Unpack the ParticleID
  std::vector<ParticleID> pids;
  for ( auto i : Packer::subrange( pverts.refs(), pvert.firstPid, pvert.lastPid ) ) pids.emplace_back( i );
  vert.setCompatiblePIDs( pids );
}

void TwoProngVertexPacker::unpack( const PackedDataVector& pverts, DataVector& verts ) const {
  verts.reserve( pverts.data().size() );

  for ( const auto& pvert : pverts.data() ) {
    // make and save new pid in container
    auto* vert = new Data();
    verts.insert( vert, pvert.key );

    // Fill data from packed object
    unpack( pvert, *vert, pverts, verts );
  }
}

TwoProngVertexPacker::DataVector TwoProngVertexPacker::unpack( const PackedDataVector& pverts ) const {

  DataVector verts;
  verts.reserve( pverts.data().size() );
  verts.setVersion( pverts.version() );

  for ( const auto& pvert : pverts.data() ) {
    // make and save new pid in container
    auto* vert = new Data();
    int   key( 0 ), linkID( 0 );
    StandardPacker::indexAndKey64( pvert.key, linkID, key );
    verts.insert( vert, key );

    // Fill data from packed object
    unpack( pvert, *vert, pverts, verts );
  }
  return verts;
}

StatusCode TwoProngVertexPacker::check( const Data& dataA, const Data& dataB ) const {
  // assume OK from the start
  bool ok = true;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // checks here

  // key
  ok &= ch.compareInts( "Key", dataA.key(), dataB.key() );
  // technique
  ok &= ch.compareInts( "Technique", dataA.technique(), dataB.technique() );
  // Chi^2
  const double chiTol = std::max( dataA.chi2() * 1.0e-6, 1.0e-3 );
  ok &= ch.compareDoubles( "Chi^2", dataA.chi2(), dataB.chi2(), chiTol );
  // NDOF
  ok &= ch.compareInts( "nDOF", dataA.nDoF(), dataB.nDoF() );
  // Position
  ok &= ch.comparePoints( "Position", dataA.position(), dataB.position() );
  // Cov matrix
  const std::array<double, 3> tolDiag = {{5.0e-3, 5.0e-3, 5.0e-3}};
  ok &= ch.compareCovMatrices<Gaudi::SymMatrix3x3, 3>( "Covariance", dataA.covMatrix(), dataB.covMatrix(), tolDiag,
                                                       2.0e-5 );

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc =
        ( dataA.parent() && dataA.parent()->registry() ? dataA.parent()->registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with TwoProngVertex data packing :-" << endmsg
                       << "  Original TwoProngVertex key=" << dataA.key() << " in '" << loc << "'" << endmsg << dataA
                       << endmsg << "  Unpacked TwoProngVertex" << endmsg << dataB << endmsg;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}
