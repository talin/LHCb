/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedEventChecks.h"

#include "GaudiKernel/DataObjectHandle.h"

using namespace LHCb;

void RelatedInfoRelationsPacker::pack( const DataVector& rels, PackedDataVector& prels ) const {
  const auto ver = prels.packingVersion();
  if ( isSupportedVer( ver ) ) {

    // loop over the relations
    for ( const auto& rel : rels.relations() ) {
      if ( !rel.from() ) continue;

      // make a new entry for this relation
      auto& prel = prels.relations().emplace_back();

      // reference to the particle
      prel.reference = StandardPacker::reference64( &prels, rel.from() );

      // First entry in the info vector
      prel.first = prels.info().size();

      // Loop over the related info map and fill into info vector
      const auto& rMap = rel.to();
      prels.info().reserve( prels.info().size() + rMap.size() );
      for ( const auto& [k, v] : rMap ) { prels.info().emplace_back( k, v ); }

      // Last entry in the info vector
      prel.last = prels.info().size();
    }
  }
}

void RelatedInfoRelationsPacker::unpack( const PackedDataVector& prels, DataVector& rels,
                                         const std::string& location ) const {
  const auto ver = prels.packingVersion();
  if ( !isSupportedVer( ver ) ) return;

  // If location is empty, unpacked everything
  if ( location.empty() ) {
    for ( const auto& rel : prels.relations() ) { unpack( rel, prels, rels ); }
  } else {
    // loop over containers
    for ( const auto& cont : prels.containers() ) {
      // Reconstruct container name for this entry
      const int   indx          = cont.reference >> 32;
      const auto& containerName = prels.linkMgr()->link( indx )->path();
      // if name matches, unpack
      if ( containerName == location ) {
        // Loop over the relations saved at this container location and unpack
        for ( auto kk : Packer::subrange( prels.relations(), cont.first, cont.last ) ) { unpack( kk, prels, rels ); }
      }
    }
  }
}

void RelatedInfoRelationsPacker::unpack( const LHCb::PackedRelatedInfoMap& pmap, const PackedDataVector& prels,
                                         DataVector& rels ) const {
  const auto ver = prels.packingVersion();
  if ( isSupportedVer( ver ) ) {

    // reconstruct the particle SmartRef and its container
    int srcLink( 0 ), srcKey( 0 );
    StandardPacker::indexAndKey64( pmap.reference, srcLink, srcKey );

    // Load the particles container
    auto*                  linkMgr      = prels.linkMgr();
    const auto*            link         = ( linkMgr ? linkMgr->link( srcLink ) : nullptr );
    const auto&            srcName      = ( link ? link->path() : "" );
    LHCb::Particles const* srcContainer = nullptr;
    if ( !srcName.empty() ) {
      DataObject* fp = nullptr;
      parent().evtSvc()->retrieveObject( srcName, fp ).ignore();
      srcContainer = static_cast<LHCb::Particles*>( fp );
    }
    if ( !srcContainer ) {
      parent().error() << "Failed to load container '" + srcName + "'" << endmsg;
    } else {
      // Get the source object
      auto* from = srcContainer->object( srcKey );
      if ( from ) {
        // Recreate the RelatedInfoMap
        TO to;
        to.reserve( pmap.last - pmap.first );
        for ( const auto& jj : Packer::subrange( prels.info(), pmap.first, pmap.last ) ) { to.insert( jj ); }
        // Save the relation
        auto sc = rels.relate( from, to );
        if ( !sc ) { parent().error() << "Problem forming relation" << endmsg; }
      }
    }
  }
}

StatusCode RelatedInfoRelationsPacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  // assume OK from the start
  bool ok = true;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // Number of relations
  ok &= ch.compareInts( "#Relations", dataA.size(), dataB.size() );

  // Loop over the relations (only if same size...)
  if ( ok ) {
    auto iA( dataA.relations().begin() ), iB( dataB.relations().begin() );
    for ( ; iA != dataA.relations().end() && iB != dataB.relations().end(); ++iA, ++iB ) {
      // Check the relations
      ok &= ch.comparePointers( "Particle", ( *iA ).from(), ( *iB ).from() );
      ok &= ( *iA ).to() == ( *iB ).to();
    }
  }

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc = ( dataA.registry() ? dataA.registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with RelatedInfo data packing :-" << endmsg << " Location '" << loc << "'" << endmsg
                       << " Size : Original=" << dataA.size() << " Unpacked=" << dataB.size() << endmsg;
    // if same size, print contents of relations
    if ( dataA.size() == dataB.size() ) {
      auto iA( dataA.relations().begin() ), iB( dataB.relations().begin() );
      for ( ; iA != dataA.relations().end() && iB != dataB.relations().end(); ++iA, ++iB ) {
        parent().warning() << " Original : From=" << ( *iA ).from() << " To=" << ( *iA ).to() << endmsg;
        parent().warning() << " Unpacked : From=" << ( *iB ).from() << " To=" << ( *iB ).to() << endmsg;
      }
    }
  }

  // finally return
  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}
