/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedMCRichHit.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void MCRichHitPacker::pack( const DataVector& hits, PackedDataVector& phits ) const {
  const auto ver = phits.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  phits.data().reserve( hits.size() );
  for ( const auto* hit : hits ) {
    auto& phit     = phits.data().emplace_back();
    phit.x         = StandardPacker::position( hit->entry().x() );
    phit.y         = StandardPacker::position( hit->entry().y() );
    phit.z         = StandardPacker::position( hit->entry().z() );
    phit.energy    = StandardPacker::energy( hit->energy() );
    phit.tof       = StandardPacker::time( hit->timeOfFlight() );
    phit.sensDetID = hit->sensDetID().key();
    phit.history   = hit->historyCode();
    if ( hit->mcParticle() ) {
      phit.mcParticle = ( 0 == ver ? StandardPacker::reference32( &parent(), &phits, hit->mcParticle() )
                                   : StandardPacker::reference64( &phits, hit->mcParticle() ) );
    }
  }
}

void MCRichHitPacker::unpack( const PackedDataVector& phits, DataVector& hits ) const {
  const auto ver = phits.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  hits.reserve( phits.data().size() );
  for ( const auto& phit : phits.data() ) {
    // make and save new hit in container
    auto* hit = new Data();
    hits.add( hit );
    // Fill data from packed object
    hit->setEntry( Gaudi::XYZPoint( StandardPacker::position( phit.x ), StandardPacker::position( phit.y ),
                                    StandardPacker::position( phit.z ) ) );
    hit->setEnergy( StandardPacker::energy( phit.energy ) );
    hit->setTimeOfFlight( StandardPacker::time( phit.tof ) );
    hit->setSensDetID( LHCb::RichSmartID( phit.sensDetID ) );
    hit->setHistoryCode( phit.history );
    if ( -1 != phit.mcParticle ) {
      int hintID( 0 ), key( 0 );
      if ( ( 0 != ver && StandardPacker::hintAndKey64( phit.mcParticle, &phits, &hits, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( phit.mcParticle, &phits, &hits, hintID, key ) ) ) {
        hit->setMCParticle( {&hits, hintID, key} );
      } else {
        parent().error() << "Corrupt MCRichHit MCParticle SmartRef detected." << endmsg;
      }
    }
  }
}

StatusCode MCRichHitPacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  StatusCode sc = StatusCode::SUCCESS;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // Loop over data containers together and compare
  auto iA( dataA.begin() ), iB( dataB.begin() );
  for ( ; iA != dataA.end() && iB != dataB.end(); ++iA, ++iB ) {
    // assume OK from the start
    bool ok = true;
    // Hit position
    ok &= ch.comparePoints( "Entry Point", ( *iA )->entry(), ( *iB )->entry() );
    // energy
    ok &= ch.compareEnergies( "Energy", ( *iA )->energy(), ( *iB )->energy() );
    // tof
    ok &= ch.compareDoubles( "TOF", ( *iA )->timeOfFlight(), ( *iB )->timeOfFlight() );
    // Detector ID
    ok &= ch.compareInts( "SensDetID", ( *iA )->sensDetID(), ( *iB )->sensDetID() );
    // History code
    ok &= ch.compareInts( "HistoryCode", ( *iA )->historyCode(), ( *iB )->historyCode() );
    // MCParticle reference
    ok &= ch.comparePointers( "MCParticle", ( *iA )->mcParticle(), ( *iB )->mcParticle() );

    // force printout for tests
    // ok = false;
    // If comparison not OK, print full information
    if ( !ok ) {
      parent().warning() << "Problem with MCRichHit data packing :-" << endmsg << "  Original Hit : " << **iA << endmsg
                         << "  Unpacked Hit : " << **iB << endmsg;
      sc = StatusCode::FAILURE;
    }
  }

  // Return final status
  return sc;
}
