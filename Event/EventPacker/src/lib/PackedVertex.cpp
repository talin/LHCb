/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedVertex.h"

#include <algorithm>

using namespace LHCb;

using LHCb::Packer::Utils::safe_sqrt;

void VertexPacker::pack( const Data& vert, PackedData& pvert, PackedDataVector& pverts ) const {
  if ( !isSupportedVer( pverts.packingVersion() ) ) return;
  // Key
  pvert.key = StandardPacker::reference64( &pverts, &vert );
  // technique
  pvert.technique = static_cast<int>( vert.technique() );
  pvert.chi2      = StandardPacker::fltPacked( vert.chi2() );
  pvert.nDoF      = vert.nDoF();
  pvert.x         = StandardPacker::position( vert.position().x() );
  pvert.y         = StandardPacker::position( vert.position().y() );
  pvert.z         = StandardPacker::position( vert.position().z() );

  // convariance Matrix
  const auto err0 = safe_sqrt( vert.covMatrix()( 0, 0 ) );
  const auto err1 = safe_sqrt( vert.covMatrix()( 1, 1 ) );
  const auto err2 = safe_sqrt( vert.covMatrix()( 2, 2 ) );
  pvert.cov00     = StandardPacker::position( err0 );
  pvert.cov11     = StandardPacker::position( err1 );
  pvert.cov22     = StandardPacker::position( err2 );
  pvert.cov10     = StandardPacker::fraction( vert.covMatrix()( 1, 0 ), err1 * err0 );
  pvert.cov20     = StandardPacker::fraction( vert.covMatrix()( 2, 0 ), err2 * err0 );
  pvert.cov21     = StandardPacker::fraction( vert.covMatrix()( 2, 1 ), err2 * err1 );

  // outgoing particles
  pvert.firstOutgoingPart = pverts.outgoingParticles().size();
  for ( const auto& P : vert.outgoingParticles() ) {
    if ( P.target() ) { pverts.outgoingParticles().push_back( StandardPacker::reference64( &pverts, P ) ); }
  }
  pvert.lastOutgoingPart = pverts.outgoingParticles().size();

  //== Handles the ExtraInfo
  pvert.firstInfo = pverts.extras().size();
  for ( const auto& [k, v] : vert.extraInfo() ) { pverts.addExtra( k, StandardPacker::fltPacked( v ) ); }
  pvert.lastInfo = pverts.extras().size();
}

void VertexPacker::unpack( const PackedData& pvert, Data& vert, const PackedDataVector& pverts,
                           DataVector& verts ) const {
  if ( !isSupportedVer( pverts.packingVersion() ) ) return;
  // technique
  vert.setTechnique( static_cast<Vertex::CreationMethod>( pvert.technique ) );
  vert.setChi2AndDoF( StandardPacker::fltPacked( pvert.chi2 ), pvert.nDoF );
  vert.setPosition( Gaudi::XYZPoint( StandardPacker::position( pvert.x ), StandardPacker::position( pvert.y ),
                                     StandardPacker::position( pvert.z ) ) );

  // convariance Matrix
  const auto err0 = StandardPacker::position( pvert.cov00 );
  const auto err1 = StandardPacker::position( pvert.cov11 );
  const auto err2 = StandardPacker::position( pvert.cov22 );
  auto&      cov  = *( const_cast<Gaudi::SymMatrix3x3*>( &vert.covMatrix() ) );
  cov( 0, 0 )     = err0 * err0;
  cov( 1, 0 )     = err1 * err0 * StandardPacker::fraction( pvert.cov10 );
  cov( 1, 1 )     = err1 * err1;
  cov( 2, 0 )     = err2 * err0 * StandardPacker::fraction( pvert.cov20 );
  cov( 2, 1 )     = err2 * err1 * StandardPacker::fraction( pvert.cov21 );
  cov( 2, 2 )     = err2 * err2;

  // outgoing particles
  for ( const auto& iP :
        Packer::subrange( pverts.outgoingParticles(), pvert.firstOutgoingPart, pvert.lastOutgoingPart ) ) {
    int hintID( 0 ), key( 0 );
    if ( StandardPacker::hintAndKey64( iP, &pverts, &verts, hintID, key ) ) {
      vert.addToOutgoingParticles( {&verts, hintID, key} );
    } else {
      parent().error() << "Corrupt Vertex Particle SmartRef found" << endmsg;
    }
  }
  //== Handles the ExtraInfo
  for ( const auto& [k, v] : Packer::subrange( pverts.extras(), pvert.firstInfo, pvert.lastInfo ) ) {
    vert.addInfo( k, StandardPacker::fltPacked( v ) );
  }
}

void VertexPacker::unpack( const PackedDataVector& pverts, DataVector& verts ) const {
  verts.reserve( pverts.data().size() );

  for ( const auto& pvert : pverts.data() ) {
    // make and save new pid in container
    auto* vert = new Data();
    verts.insert( vert, pvert.key );

    // Fill data from packed object
    unpack( pvert, *vert, pverts, verts );
  }
}

VertexPacker::DataVector VertexPacker::unpack( const PackedDataVector& pverts ) const {

  DataVector verts;
  verts.reserve( pverts.data().size() );
  verts.setVersion( pverts.version() );

  for ( const auto& pvert : pverts.data() ) {
    // make and save new pid in container
    auto* vert = new Data();
    int   key( 0 ), linkID( 0 );
    StandardPacker::indexAndKey64( pvert.key, linkID, key );
    verts.insert( vert, key );

    // Fill data from packed object
    unpack( pvert, *vert, pverts, verts );
  }
  return verts;
}

StatusCode VertexPacker::check( const Data& dataA, const Data& dataB ) const {
  // assume OK from the start
  bool ok = true;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // checks here

  // key
  ok &= ch.compareInts( "Key", dataA.key(), dataB.key() );
  // technique
  ok &= ch.compareInts( "Technique", dataA.technique(), dataB.technique() );
  // Chi^2
  const double chiTol = std::max( dataA.chi2() * 1.0e-6, 1.0e-3 );
  ok &= ch.compareDoubles( "Chi^2", dataA.chi2(), dataB.chi2(), chiTol );
  // NDOF
  ok &= ch.compareInts( "nDOF", dataA.nDoF(), dataB.nDoF() );
  // Position
  ok &= ch.comparePoints( "Position", dataA.position(), dataB.position() );
  // Cov matrix
  const std::array<double, 3> tolDiag = {{Packer::POSITION_TOL, Packer::POSITION_TOL, Packer::POSITION_TOL}};
  ok &= ch.compareCovMatrices<Gaudi::SymMatrix3x3, 3>( "Covariance", dataA.covMatrix(), dataB.covMatrix(), tolDiag,
                                                       Packer::FRACTION_TOL );

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc =
        ( dataA.parent() && dataA.parent()->registry() ? dataA.parent()->registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with Vertex data packing :-" << endmsg << "  Original Vertex key=" << dataA.key()
                       << " in '" << loc << "'" << endmsg << dataA << endmsg << "  Unpacked Vertex" << endmsg << dataB
                       << endmsg;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}
