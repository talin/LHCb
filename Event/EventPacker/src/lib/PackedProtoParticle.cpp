/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedProtoParticle.h"
#include "Event/PackedEventChecks.h"

#include "fmt/format.h"

#include <boost/numeric/conversion/bounds.hpp>

using namespace LHCb;

//-----------------------------------------------------------------------------

void ProtoParticlePacker::pack( const Data& proto, PackedData& pproto, PackedDataVector& pprotos ) const {
  // packing version
  const auto ver = pprotos.packingVersion();
  if ( !isSupportedVer( ver ) ) return;

  // save the key
  pproto.key = proto.key();

  if ( proto.track() ) {
    pproto.track = ( 0 == ver ? StandardPacker::reference32( &parent(), &pprotos, proto.track() )
                              : StandardPacker::reference64( &pprotos, proto.track() ) );
    parent().debug() << "Found a track with parent " << proto.track()->parent()->name() << " and key "
                     << proto.track()->key() << endmsg;
    parent().debug() << "Packed a track with key " << pproto.track << endmsg;
  } else {
    pproto.track = -1;
  }

  if ( proto.richPID() ) {
    pproto.richPID = ( 0 == ver ? StandardPacker::reference32( &parent(), &pprotos, proto.richPID() )
                                : StandardPacker::reference64( &pprotos, proto.richPID() ) );
    parent().debug() << "Found a richPID with parent " << proto.richPID()->parent()->name() << " and key "
                     << proto.richPID()->key() << endmsg;
    parent().debug() << "Packed a richPID with key " << pproto.richPID << endmsg;
  } else {
    pproto.richPID = -1;
  }

  if ( proto.muonPID() ) {
    pproto.muonPID = ( 0 == ver ? StandardPacker::reference32( &parent(), &pprotos, proto.muonPID() )
                                : StandardPacker::reference64( &pprotos, proto.muonPID() ) );
    parent().debug() << "Found a muonPID with parent " << proto.muonPID()->parent()->name() << " and key "
                     << proto.muonPID()->key() << endmsg;
    parent().debug() << "Packed a muonPID with key " << pproto.muonPID << endmsg;
  } else {
    pproto.muonPID = -1;
  }

  //== Store the CaloHypos
  pproto.firstHypo = pprotos.refs().size();
  for ( const auto& caloH : proto.calo() ) {
    pprotos.refs().push_back( 0 == ver ? StandardPacker::reference32( &parent(), &pprotos, caloH )
                                       : StandardPacker::reference64( &pprotos, caloH ) );
    parent().debug() << "Found a caloHYPO with parent " << caloH->parent()->name() << " and key " << caloH->key()
                     << endmsg;
  }
  pproto.lastHypo = pprotos.refs().size();
  parent().debug() << "Packed first caloHypo with key " << pproto.firstHypo << endmsg;
  parent().debug() << "Packed last caloHypo with key " << pproto.lastHypo << endmsg;
  parent().debug() << "Size of references " << pprotos.refs().size() << endmsg;

  //== Handles the ExtraInfo
  pproto.firstExtra = pprotos.extras().size();
  const double high = boost::numeric::bounds<float>::highest();
  const double low  = boost::numeric::bounds<float>::lowest();
  for ( const auto& einfo : proto.extraInfo() ) {
    const auto& info = einfo.second;
    if ( info > high || info < low ) {
      parent().warning() << fmt::format( "ExtraInfo '{}' out of floating point range. Truncating value.",
                                         (LHCb::ProtoParticle::additionalInfo)einfo.first )
                         << endmsg;
    }
    pprotos.extras().emplace_back( einfo.first, StandardPacker::fltPacked( info ) );
  }
  pproto.lastExtra = pprotos.extras().size();
}

void ProtoParticlePacker::unpack( const PackedData& pproto, Data& proto, const PackedDataVector& pprotos,
                                  DataVector& protos, LHCb::Packer::Carry& with_carry ) const {
  // packing version
  const auto ver = pprotos.packingVersion();

  if ( !isSupportedVer( ver ) ) return;

  int hintID( 0 ), key( 0 );

  if ( -1 != pproto.track ) {
    if ( ( 0 != ver && StandardPacker::hintAndKey64( pproto.track, &pprotos, &protos, hintID, key ) ) ||
         ( 0 == ver && StandardPacker::hintAndKey32( pproto.track, &pprotos, &protos, hintID, key ) ) ) {
      proto.setTrack( {&protos, hintID, key} );
    } else {
      parent().error() << "Corrupt ProtoParticle Track SmartRef detected." << pproto.track << endmsg;
    }
  }

  if ( -1 != pproto.richPID ) {
    if ( ( 0 != ver && StandardPacker::hintAndKey64( pproto.richPID, &pprotos, &protos, hintID, key ) ) ||
         ( 0 == ver && StandardPacker::hintAndKey32( pproto.richPID, &pprotos, &protos, hintID, key ) ) ) {
      proto.setRichPID( {&protos, hintID, key} );
    } else {
      parent().error() << "Corrupt ProtoParticle RichPID SmartRef detected." << endmsg;
    }
  }

  if ( -1 != pproto.muonPID ) {
    if ( ( 0 != ver && StandardPacker::hintAndKey64( pproto.muonPID, &pprotos, &protos, hintID, key ) ) ||
         ( 0 == ver && StandardPacker::hintAndKey32( pproto.muonPID, &pprotos, &protos, hintID, key ) ) ) {
      proto.setMuonPID( {&protos, hintID, key} );
    } else {
      parent().error() << "Corrupt ProtoParticle MuonPID SmartRef detected." << endmsg;
    }
  }

  for ( auto reference : Packer::subrange( pprotos.refs(), pproto.firstHypo, pproto.lastHypo ) ) {
    if ( ( 0 != ver && StandardPacker::hintAndKey64( reference, &pprotos, &protos, hintID, key ) ) ||
         ( 0 == ver && StandardPacker::hintAndKey32( reference, &pprotos, &protos, hintID, key ) ) ) {
      SmartRef<LHCb::CaloHypo> ref( &protos, hintID, key );
      proto.addToCalo( ref );
    } else {
      parent().error() << "Corrupt ProtoParticle CaloHypo SmartRef detected." << endmsg;
    }
  }

  for ( const auto& [k, v] : with_carry( pprotos.extras(), pproto.firstExtra, pproto.lastExtra ) ) {
    proto.addInfo( k, StandardPacker::fltPacked( v ) );
  }
}

void ProtoParticlePacker::unpack( const PackedDataVector& pprotos, DataVector& protos ) const {
  protos.reserve( pprotos.data().size() );
  parent().debug() << "version " << (int)pprotos.version() << endmsg;
  parent().debug() << "packing version " << (int)pprotos.packingVersion() << endmsg;

  LHCb::Packer::Carry carry{};
  for ( const auto& pproto : pprotos.data() ) {
    auto* part = new LHCb::ProtoParticle();
    protos.insert( part, pproto.key );

    unpack( pproto, *part, pprotos, protos, carry );
  }
  if ( carry ) {
    parent().warning() << "overflow detected while unpacking protoparticle extrainfo -- I _hope_ the correction for "
                          "this worked properly... good luck!!!"
                       << endmsg;
  }
}

StatusCode ProtoParticlePacker::check( const Data& dataA, const Data& dataB ) const {
  // checker
  const DataPacking::DataChecks ch( parent() );

  bool isOK = true;

  // key
  isOK &= ch.compareInts( "Key", dataA.key(), dataB.key() );

  // check referenced objects
  isOK &= ch.comparePointers( "Track", dataA.track(), dataB.track() );
  isOK &= ch.comparePointers( "RichPID", dataA.richPID(), dataB.richPID() );
  isOK &= ch.comparePointers( "MuonPID", dataA.muonPID(), dataB.muonPID() );

  // calo hypos
  isOK &= ch.compareInts( "#CaloHypos", dataA.calo().size(), dataB.calo().size() );
  if ( isOK ) {
    for ( auto iC = std::make_pair( dataA.calo().begin(), dataB.calo().begin() );
          iC.first != dataA.calo().end() && iC.second != dataB.calo().end(); ++iC.first, ++iC.second ) {
      isOK &= ch.comparePointers( "CaloHypo", iC.first->target(), iC.second->target() );
    }
  }

  // extra info
  isOK &= ch.compareInts( "#ExtraInfo", dataA.extraInfo().size(), dataB.extraInfo().size() );
  if ( isOK ) {
    for ( auto iE = std::make_pair( dataA.extraInfo().begin(), dataB.extraInfo().begin() );
          iE.first != dataA.extraInfo().end() && iE.second != dataB.extraInfo().end(); ++iE.first, ++iE.second ) {
      isOK &= ch.compareInts( "ExtraInfoKey", iE.first->first, iE.second->first );
      if ( isOK ) {
        if ( ( iE.second->second == 0 && iE.second->second != iE.first->second ) ||
             ( iE.second->second != 0 &&
               1.e-7 < std::abs( ( iE.second->second - iE.first->second ) / iE.second->second ) ) )
          isOK = false;
      }
    }
  }

  // isOK = false; // force false for testing

  if ( !isOK || MSG::DEBUG >= parent().msgLevel() ) {
    parent().info() << "===== ProtoParticle key " << dataA.key() << " Check OK = " << isOK << endmsg;
    parent().info() << format( "Old   track %8x  richPID %8X  muonPID%8x  nCaloHypo%4d nExtra%4d", dataA.track(),
                               dataA.richPID(), dataA.muonPID(), dataA.calo().size(), dataA.extraInfo().size() )
                    << endmsg;
    parent().info() << format( "Test  track %8x  richPID %8X  muonPID%8x  nCaloHypo%4d nExtra%4d", dataB.track(),
                               dataB.richPID(), dataB.muonPID(), dataB.calo().size(), dataB.extraInfo().size() )
                    << endmsg;
    for ( auto iC = std::make_pair( dataA.calo().begin(), dataB.calo().begin() );
          iC.first != dataA.calo().end() && iC.second != dataB.calo().end(); ++iC.first, ++iC.second ) {
      parent().info() << format( "   old CaloHypo %8x   new %8x", iC.first->target(), iC.second->target() ) << endmsg;
    }
    for ( auto iE = std::make_pair( dataA.extraInfo().begin(), dataB.extraInfo().begin() );
          iE.first != dataA.extraInfo().end() && iE.second != dataB.extraInfo().end(); ++iE.first, ++iE.second ) {
      parent().info() << format( "   old Extra %5d %12.4f     new %5d %12.4f", iE.first->first, iE.first->second,
                                 iE.second->first, iE.second->second )
                      << endmsg;
    }
  }

  return ( isOK ? StatusCode::SUCCESS : StatusCode::FAILURE );
}