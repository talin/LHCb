/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedRecSummary.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void RecSummaryPacker::pack( const DataVector& sums, PackedDataVector& psums ) const {
  const auto ver = psums.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  psums.data().reserve( sums.summaryData().size() * 2 );
  // This can probably be packed tighter
  for ( auto value : sums.summaryData() ) {
    psums.data().push_back( value.first );
    psums.data().push_back( value.second );
  }
}

void RecSummaryPacker::unpack( const PackedDataVector& psums, DataVector& sums ) const {
  sums.reserve( psums.data().size() / 2 );
  for ( unsigned int i = 0; i < psums.data().size() / 2; i++ ) {
    sums.addInfo( psums.data()[2 * i], psums.data()[2 * i + 1] );
  }
}

StatusCode RecSummaryPacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  return ( dataA.summaryData() == dataB.summaryData() ) ? StatusCode::SUCCESS : StatusCode::FAILURE;
}
