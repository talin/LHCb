/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedMCHit.h"
#include "Event/PackedEventChecks.h"

#include <sstream>

using namespace LHCb;

void MCHitPacker::pack( const DataVector& hits, PackedDataVector& phits ) const {
  const auto ver = phits.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  phits.data().reserve( hits.size() );
  for ( const auto* hit : hits ) {
    auto& phit     = phits.data().emplace_back();
    phit.sensDetID = hit->sensDetID();
    phit.entx      = StandardPacker::position( hit->entry().x() );
    phit.enty      = StandardPacker::position( hit->entry().y() );
    phit.entz      = StandardPacker::position( hit->entry().z() );
    phit.vtxx      = StandardPacker::position( m_dispScale * hit->displacement().x() );
    phit.vtxy      = StandardPacker::position( m_dispScale * hit->displacement().y() );
    phit.vtxz      = StandardPacker::position( m_dispScale * hit->displacement().z() );
    phit.energy    = StandardPacker::energy( m_enScale * hit->energy() );
    phit.tof       = StandardPacker::time( hit->time() );
    phit.mp        = StandardPacker::energy( hit->p() );
    if ( hit->mcParticle() ) {
      phit.mcParticle = ( 0 == ver ) ? StandardPacker::reference32( &parent(), &phits, hit->mcParticle() )
                                     : StandardPacker::reference64( &phits, hit->mcParticle() );
    }
  }
}

void MCHitPacker::unpack( const PackedDataVector& phits, DataVector& hits ) const {
  const auto ver = phits.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  hits.reserve( phits.data().size() );
  for ( const auto& phit : phits.data() ) {
    // make and save new hit in container
    auto* hit = new Data();
    hits.add( hit );
    // Fill data from packed object
    hit->setSensDetID( phit.sensDetID );
    hit->setEntry( Gaudi::XYZPoint( StandardPacker::position( phit.entx ), StandardPacker::position( phit.enty ),
                                    StandardPacker::position( phit.entz ) ) );
    hit->setDisplacement( Gaudi::XYZVector( StandardPacker::position( phit.vtxx ) / m_dispScale,
                                            StandardPacker::position( phit.vtxy ) / m_dispScale,
                                            StandardPacker::position( phit.vtxz ) / m_dispScale ) );
    hit->setEnergy( StandardPacker::energy( phit.energy ) / m_enScale );
    hit->setTime( StandardPacker::time( phit.tof ) );
    hit->setP( StandardPacker::energy( phit.mp ) );
    if ( -1 != phit.mcParticle ) {
      int hintID( 0 ), key( 0 );
      if ( ( 0 != ver && StandardPacker::hintAndKey64( phit.mcParticle, &phits, &hits, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( phit.mcParticle, &phits, &hits, hintID, key ) ) ) {
        hit->setMCParticle( {&hits, hintID, key} );
      } else {
        parent().error() << "Corrupt MCHit MCParticle SmartRef detected." << endmsg;
      }
    }
  }
}

StatusCode MCHitPacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  StatusCode sc = StatusCode::SUCCESS;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // Loop over data containers together and compare
  auto iA( dataA.begin() ), iB( dataB.begin() );
  for ( ; iA != dataA.end() && iB != dataB.end(); ++iA, ++iB ) {
    // assume OK from the start
    bool ok = true;
    // Detector ID
    ok &= ch.compareInts( "SensDetID", ( *iA )->sensDetID(), ( *iB )->sensDetID() );
    // Hit position
    ok &= ch.comparePoints( "Entry Point", ( *iA )->entry(), ( *iB )->entry() );
    // displacement
    ok &= ch.compareVectors( "Displacement", ( *iA )->displacement(), ( *iB )->displacement() );
    // energy
    ok &= ch.compareEnergies( "Energy", ( *iA )->energy(), ( *iB )->energy() );
    // tof
    ok &= ch.compareDoubles( "TOF", ( *iA )->time(), ( *iB )->time() );
    // mp
    ok &= ch.compareEnergies( "Parent |P|", ( *iA )->p(), ( *iB )->p() );
    // MCParticle reference
    ok &= ch.comparePointers( "MCParticle", ( *iA )->mcParticle(), ( *iB )->mcParticle() );

    // force printout for tests
    // ok = false;
    // If comparison not OK, print full information
    if ( !ok ) {
      parent().warning() << "Problem with MCHit data packing :-" << endmsg << "  Original Hit : " << **iA << endmsg
                         << "  Unpacked Hit : " << **iB << endmsg;
      sc = StatusCode::FAILURE;
    }
  }

  // Return final status
  return sc;
}
