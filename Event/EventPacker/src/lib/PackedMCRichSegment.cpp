/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedMCRichSegment.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void MCRichSegmentPacker::pack( const DataVector& segs, PackedDataVector& psegs ) const {
  const auto ver = psegs.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  psegs.data().reserve( segs.size() );
  for ( const auto* seg : segs ) {
    auto& pseg = psegs.data().emplace_back();

    pseg.key = seg->key();

    pseg.history = seg->historyCode();

    pseg.trajPx.reserve( seg->trajectoryPoints().size() );
    pseg.trajPy.reserve( seg->trajectoryPoints().size() );
    pseg.trajPz.reserve( seg->trajectoryPoints().size() );
    for ( const auto& T : seg->trajectoryPoints() ) {
      pseg.trajPx.push_back( StandardPacker::position( T.x() ) );
      pseg.trajPy.push_back( StandardPacker::position( T.y() ) );
      pseg.trajPz.push_back( StandardPacker::position( T.z() ) );
    }

    pseg.trajMx.reserve( seg->trajectoryMomenta().size() );
    pseg.trajMy.reserve( seg->trajectoryMomenta().size() );
    pseg.trajMz.reserve( seg->trajectoryMomenta().size() );
    for ( const auto& M : seg->trajectoryMomenta() ) {
      pseg.trajMx.push_back( StandardPacker::energy( M.x() ) );
      pseg.trajMy.push_back( StandardPacker::energy( M.y() ) );
      pseg.trajMz.push_back( StandardPacker::energy( M.z() ) );
    }

    if ( seg->mcParticle() ) {
      pseg.mcParticle = ( 0 == ver ? StandardPacker::reference32( &parent(), &psegs, seg->mcParticle() )
                                   : StandardPacker::reference64( &psegs, seg->mcParticle() ) );
    }

    if ( seg->mcRichTrack() ) {
      pseg.mcRichTrack = ( 0 == ver ? StandardPacker::reference32( &parent(), &psegs, seg->mcRichTrack() )
                                    : StandardPacker::reference64( &psegs, seg->mcRichTrack() ) );
    }

    pseg.mcPhotons.reserve( seg->mcRichOpticalPhotons().size() );
    for ( const auto& P : seg->mcRichOpticalPhotons() ) {
      pseg.mcPhotons.push_back( 0 == ver ? StandardPacker::reference32( &parent(), &psegs, P )
                                         : StandardPacker::reference64( &psegs, P ) );
    }

    pseg.mcHits.reserve( seg->mcRichHits().size() );
    for ( const auto& H : seg->mcRichHits() ) {
      pseg.mcHits.push_back( 0 == ver ? StandardPacker::reference32( &parent(), &psegs, H->parent(), H.linkID() )
                                      : StandardPacker::reference64( &psegs, H->parent(), H.linkID() ) );
    }
  }
}

void MCRichSegmentPacker::unpack( const PackedDataVector& psegs, DataVector& segs ) const {
  const auto ver = psegs.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  segs.reserve( psegs.data().size() );
  for ( const auto& pseg : psegs.data() ) {
    auto* seg = new Data();
    segs.insert( seg, pseg.key );

    seg->setHistoryCode( pseg.history );

    auto ix( pseg.trajPx.begin() ), iy( pseg.trajPy.begin() ), iz( pseg.trajPz.begin() );
    for ( ; ix != pseg.trajPx.end(); ++ix, ++iy, ++iz ) {
      seg->addToTrajectoryPoints( Gaudi::XYZPoint( StandardPacker::position( *ix ), StandardPacker::position( *iy ),
                                                   StandardPacker::position( *iz ) ) );
    }

    auto jx( pseg.trajMx.begin() ), jy( pseg.trajMy.begin() ), jz( pseg.trajMz.begin() );
    for ( ; jx != pseg.trajMx.end(); ++jx, ++jy, ++jz ) {
      seg->addToTrajectoryMomenta( Gaudi::XYZVector( StandardPacker::energy( *jx ), StandardPacker::energy( *jy ),
                                                     StandardPacker::energy( *jz ) ) );
    }

    int hintID( 0 ), key( 0 );

    if ( -1 != pseg.mcParticle ) {
      if ( ( 0 != ver && StandardPacker::hintAndKey64( pseg.mcParticle, &psegs, &segs, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( pseg.mcParticle, &psegs, &segs, hintID, key ) ) ) {
        seg->setMcParticle( {&segs, hintID, key} );
      } else {
        parent().error() << "Corrupt MCRichSegment MCParticle SmartRef detected." << endmsg;
      }
    }

    if ( -1 != pseg.mcRichTrack ) {
      if ( ( 0 != ver && StandardPacker::hintAndKey64( pseg.mcRichTrack, &psegs, &segs, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( pseg.mcRichTrack, &psegs, &segs, hintID, key ) ) ) {
        seg->setMCRichTrack( {&segs, hintID, key} );
      } else {
        parent().error() << "Corrupt MCRichSegment MCRichTrack SmartRef detected." << endmsg;
      }
    }

    for ( const auto& P : pseg.mcPhotons ) {
      if ( ( 0 != ver && StandardPacker::hintAndKey64( P, &psegs, &segs, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( P, &psegs, &segs, hintID, key ) ) ) {
        seg->addToMCRichOpticalPhotons( {&segs, hintID, key} );
      } else {
        parent().error() << "Corrupt MCRichSegment MCRichOpticalPhoton SmartRef detected." << endmsg;
      }
    }

    for ( const auto& H : pseg.mcHits ) {
      if ( ( 0 != ver && StandardPacker::hintAndKey64( H, &psegs, &segs, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( H, &psegs, &segs, hintID, key ) ) ) {
        seg->addToMCRichHits( {&segs, hintID, key} );
      } else {
        parent().error() << "Corrupt MCRichSegment MCRichHit SmartRef detected." << endmsg;
      }
    }
  }
}

StatusCode MCRichSegmentPacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  StatusCode sc = StatusCode::SUCCESS;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // Loop over data containers together and compare
  auto iA( dataA.begin() ), iB( dataB.begin() );
  for ( ; iA != dataA.end() && iB != dataB.end(); ++iA, ++iB ) {
    // assume OK from the start
    bool ok = true;
    // Key
    ok &= ch.compareInts( "Key", ( *iA )->key(), ( *iB )->key() );
    // History code
    ok &= ch.compareInts( "HistoryCode", ( *iA )->historyCode(), ( *iB )->historyCode() );
    // Trajectory points
    const bool sameSizeTrajP =
        ch.compareInts( "#TrajPoints", ( *iA )->trajectoryPoints().size(), ( *iB )->trajectoryPoints().size() );
    ok &= sameSizeTrajP;
    if ( sameSizeTrajP ) {
      auto tA( ( *iA )->trajectoryPoints().begin() ), tB( ( *iB )->trajectoryPoints().begin() );
      for ( ; tA != ( *iA )->trajectoryPoints().end() && tB != ( *iB )->trajectoryPoints().end(); ++tA, ++tB ) {
        ok &= ch.comparePoints( "Traj. Point", *tA, *tB );
      }
    }
    // Trajectory momenta
    const bool sameSizeTrajM =
        ch.compareInts( "#TrajMomenta", ( *iA )->trajectoryMomenta().size(), ( *iB )->trajectoryMomenta().size() );
    ok &= sameSizeTrajM;
    if ( sameSizeTrajM ) {
      auto tA( ( *iA )->trajectoryMomenta().begin() ), tB( ( *iB )->trajectoryMomenta().begin() );
      for ( ; tA != ( *iA )->trajectoryMomenta().end() && tB != ( *iB )->trajectoryMomenta().end(); ++tA, ++tB ) {
        ok &= ch.compareEnergies( "Traj. Momenta", *tA, *tB );
      }
    }
    // MCParticle
    ok &= ch.comparePointers( "MCParticle", ( *iA )->mcParticle(), ( *iB )->mcParticle() );
    // MCRichTrack
    ok &= ch.comparePointers( "MCRichTrack", ( *iA )->mcRichTrack(), ( *iB )->mcRichTrack() );
    // MCPhotons
    const bool sameSizePhots = ch.compareInts( "#MCRichPhotons", ( *iA )->mcRichOpticalPhotons().size(),
                                               ( *iB )->mcRichOpticalPhotons().size() );
    ok &= sameSizePhots;
    if ( sameSizePhots ) {
      auto jA( ( *iA )->mcRichOpticalPhotons().begin() ), jB( ( *iB )->mcRichOpticalPhotons().begin() );
      for ( ; jA != ( *iA )->mcRichOpticalPhotons().end() && jB != ( *iB )->mcRichOpticalPhotons().end(); ++jA, ++jB ) {
        ok &= ch.comparePointers( "MCRichPhoton", jA->target(), jB->target() );
      }
    }
    // MCHits
    const bool sameSizeHits =
        ch.compareInts( "#MCRichHits", ( *iA )->mcRichHits().size(), ( *iB )->mcRichHits().size() );
    ok &= sameSizeHits;
    if ( sameSizeHits ) {
      auto jA( ( *iA )->mcRichHits().begin() ), jB( ( *iB )->mcRichHits().begin() );
      for ( ; jA != ( *iA )->mcRichHits().end() && jB != ( *iB )->mcRichHits().end(); ++jA, ++jB ) {
        ok &= ch.comparePointers( "MCRichHit", jA->target(), jB->target() );
      }
    }

    // force printout for tests
    // ok = false;
    // If comparison not OK, print full information
    if ( !ok ) {
      parent().warning() << "Problem with MCRichSegment data packing :-" << endmsg << "  Original Segment : " << **iA
                         << endmsg << "  Unpacked Segment : " << **iB << endmsg;
      sc = StatusCode::FAILURE;
    }
  }

  // return final status
  return sc;
}
