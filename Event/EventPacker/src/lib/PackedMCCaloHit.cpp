/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedMCCaloHit.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void MCCaloHitPacker::pack( const DataVector& hits, PackedDataVector& phits ) const {
  const auto ver = phits.packingVersion();
  if ( isSupportedVer( ver ) ) {
    phits.data().reserve( hits.size() );
    for ( const auto* hit : hits ) {
      auto& phit     = phits.data().emplace_back();
      phit.activeE   = StandardPacker::energy( hit->activeE() * m_energyScale );
      phit.sensDetID = hit->sensDetID();
      phit.time      = hit->time();
      if ( hit->particle() ) {
        phit.mcParticle = ( 0 == ver ? StandardPacker::reference32( &parent(), &phits, hit->particle() )
                                     : StandardPacker::reference64( &phits, hit->particle() ) );
      }
    }
  }
}

void MCCaloHitPacker::unpack( const PackedDataVector& phits, DataVector& hits ) const {
  const auto ver = phits.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  hits.reserve( phits.data().size() );
  for ( const auto& phit : phits.data() ) {
    // make and save new hit in container
    auto* hit = new Data();
    hits.add( hit );
    // Fill data from packed object
    hit->setActiveE( StandardPacker::energy( phit.activeE ) / m_energyScale );
    hit->setSensDetID( phit.sensDetID );
    hit->setTime( phit.time );
    if ( -1 != phit.mcParticle ) {
      int hintID( 0 ), key( 0 );
      if ( ( 0 != ver && StandardPacker::hintAndKey64( phit.mcParticle, &phits, &hits, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( phit.mcParticle, &phits, &hits, hintID, key ) ) ) {
        hit->setParticle( {&hits, hintID, key} );
      } else {
        parent().error() << "Corrupt MCCaloHit MCParticle SmartRef detected." << endmsg;
      }
    }
  }
}

StatusCode MCCaloHitPacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  StatusCode sc = StatusCode::SUCCESS;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // Loop over data containers together and compare
  auto iA( dataA.begin() ), iB( dataB.begin() );
  for ( ; iA != dataA.end() && iB != dataB.end(); ++iA, ++iB ) {
    // assume OK from the start
    bool ok = true;
    // Detector ID
    ok &= ch.compareInts( "SensDetID", ( *iA )->sensDetID(), ( *iB )->sensDetID() );
    // energy
    ok &= ch.compareEnergies( "Energy", ( *iA )->activeE(), ( *iB )->activeE() );
    // tof
    ok &= ch.compareDoubles( "TOF", ( *iA )->time(), ( *iB )->time() );
    // MCParticle reference
    ok &= ch.comparePointers( "MCParticle", ( *iA )->particle(), ( *iB )->particle() );

    // force printout for tests
    // ok = false;
    // If comparison not OK, print full information
    if ( !ok ) {
      parent().warning() << "Problem with MCCaloHit data packing :-" << endmsg << "  Original Hit : " << **iA << endmsg
                         << "  Unpacked Hit : " << **iB << endmsg;
      sc = StatusCode::FAILURE;
    }
  }

  // Return final status
  return sc;
}
