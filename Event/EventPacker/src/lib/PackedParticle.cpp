/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedParticle.h"
#include "Event/PackedEventChecks.h"

#include "fmt/format.h"

using namespace LHCb;
using LHCb::Packer::Utils::safe_sqrt;

void ParticlePacker::pack( const Data& part, PackedData& ppart, PackedDataVector& pparts ) const {
  // Only support version 1 for packing, as 0 is buggy.
  if ( 1 != pparts.packingVersion() ) {
    throw GaudiException( fmt::format( "Unknown packed data version {}", (int)pparts.packingVersion() ),
                          "ParticlePacker", StatusCode::FAILURE );
  }

  // fill ppart key from part
  ppart.key = StandardPacker::reference64( &pparts, &part );

  // Particle ID
  ppart.particleID = part.particleID().pid();

  // Mass and error
  ppart.measMass    = StandardPacker::mass( part.measuredMass() );
  ppart.measMassErr = StandardPacker::mass( part.measuredMassErr() );

  // Lorentz vector
  ppart.lv_px   = StandardPacker::energy( part.momentum().px() );
  ppart.lv_py   = StandardPacker::energy( part.momentum().py() );
  ppart.lv_pz   = StandardPacker::energy( part.momentum().pz() );
  ppart.lv_mass = (float)part.momentum().M();

  // reference point
  ppart.refx = StandardPacker::position( part.referencePoint().x() );
  ppart.refy = StandardPacker::position( part.referencePoint().y() );
  ppart.refz = StandardPacker::position( part.referencePoint().z() );

  // Mom Cov
  const auto merr00 = safe_sqrt( part.momCovMatrix()( 0, 0 ) );
  const auto merr11 = safe_sqrt( part.momCovMatrix()( 1, 1 ) );
  const auto merr22 = safe_sqrt( part.momCovMatrix()( 2, 2 ) );
  const auto merr33 = safe_sqrt( part.momCovMatrix()( 3, 3 ) );
  ppart.momCov00    = StandardPacker::energy( merr00 );
  ppart.momCov11    = StandardPacker::energy( merr11 );
  ppart.momCov22    = StandardPacker::energy( merr22 );
  ppart.momCov33    = StandardPacker::energy( merr33 );
  ppart.momCov10    = StandardPacker::fraction( part.momCovMatrix()( 1, 0 ), ( merr11 * merr00 ) );
  ppart.momCov20    = StandardPacker::fraction( part.momCovMatrix()( 2, 0 ), ( merr22 * merr00 ) );
  ppart.momCov21    = StandardPacker::fraction( part.momCovMatrix()( 2, 1 ), ( merr22 * merr11 ) );
  ppart.momCov30    = StandardPacker::fraction( part.momCovMatrix()( 3, 0 ), ( merr33 * merr00 ) );
  ppart.momCov31    = StandardPacker::fraction( part.momCovMatrix()( 3, 1 ), ( merr33 * merr11 ) );
  ppart.momCov32    = StandardPacker::fraction( part.momCovMatrix()( 3, 2 ), ( merr33 * merr22 ) );

  // Pos Cov
  const auto perr00 = safe_sqrt( part.posCovMatrix()( 0, 0 ) );
  const auto perr11 = safe_sqrt( part.posCovMatrix()( 1, 1 ) );
  const auto perr22 = safe_sqrt( part.posCovMatrix()( 2, 2 ) );
  ppart.posCov00    = StandardPacker::position( perr00 );
  ppart.posCov11    = StandardPacker::position( perr11 );
  ppart.posCov22    = StandardPacker::position( perr22 );
  ppart.posCov10    = StandardPacker::fraction( part.posCovMatrix()( 1, 0 ), ( perr11 * perr00 ) );
  ppart.posCov20    = StandardPacker::fraction( part.posCovMatrix()( 2, 0 ), ( perr22 * perr00 ) );
  ppart.posCov21    = StandardPacker::fraction( part.posCovMatrix()( 2, 1 ), ( perr22 * perr11 ) );

  // PosMom Cov
  ppart.pmCov00 = StandardPacker::fltPacked( part.posMomCovMatrix()( 0, 0 ) );
  ppart.pmCov01 = StandardPacker::fltPacked( part.posMomCovMatrix()( 0, 1 ) );
  ppart.pmCov02 = StandardPacker::fltPacked( part.posMomCovMatrix()( 0, 2 ) );
  ppart.pmCov10 = StandardPacker::fltPacked( part.posMomCovMatrix()( 1, 0 ) );
  ppart.pmCov11 = StandardPacker::fltPacked( part.posMomCovMatrix()( 1, 1 ) );
  ppart.pmCov12 = StandardPacker::fltPacked( part.posMomCovMatrix()( 1, 2 ) );
  ppart.pmCov20 = StandardPacker::fltPacked( part.posMomCovMatrix()( 2, 0 ) );
  ppart.pmCov21 = StandardPacker::fltPacked( part.posMomCovMatrix()( 2, 1 ) );
  ppart.pmCov22 = StandardPacker::fltPacked( part.posMomCovMatrix()( 2, 2 ) );
  ppart.pmCov30 = StandardPacker::fltPacked( part.posMomCovMatrix()( 3, 0 ) );
  ppart.pmCov31 = StandardPacker::fltPacked( part.posMomCovMatrix()( 3, 1 ) );
  ppart.pmCov32 = StandardPacker::fltPacked( part.posMomCovMatrix()( 3, 2 ) );

  // extra info
  ppart.firstExtra = pparts.extra().size();
  for ( const auto& [k, v] : part.extraInfo() ) {
    pparts.extra().emplace_back( k, StandardPacker::fltPacked( v ) );
    parent().debug() << "extra info " << k << "  " << v << endmsg;
  }
  ppart.lastExtra = pparts.extra().size();

  // end vertex
  if ( part.endVertex() ) {
    ppart.vertex = StandardPacker::reference64( &pparts, part.endVertex() );
    parent().debug() << "endvertex " << part.endVertex()->key() << endmsg;
  }

  // protoparticle
  if ( part.proto() ) {
    ppart.proto = StandardPacker::reference64( &pparts, part.proto() );
    parent().debug() << "proto " << part.proto()->key() << endmsg;
  }

  // daughters
  ppart.firstDaughter = pparts.daughters().size();
  for ( const auto& P : part.daughters() ) {
    if ( P.target() ) {
      pparts.daughters().push_back( StandardPacker::reference64( &pparts, P ) );
      parent().debug() << "daughter " << P->key() << endmsg;
    }
  }
  ppart.lastDaughter = pparts.daughters().size();
}

void ParticlePacker::unpack( const PackedData& ppart, Data& part, const PackedDataVector& pparts, DataVector& parts,
                             LHCb::Packer::Carry& with_carry ) const {
  if ( 0 != pparts.packingVersion() && 1 != pparts.packingVersion() ) {
    throw GaudiException( fmt::format( "Unknown packed data version {}", (int)pparts.packingVersion() ),
                          "ParticlePacker", StatusCode::FAILURE );
  }

  const bool isVZero = ( 0 == pparts.packingVersion() );

  // particle ID
  part.setParticleID( LHCb::ParticleID( ppart.particleID ) );

  // Mass and error
  part.setMeasuredMass( StandardPacker::mass( ppart.measMass ) );
  part.setMeasuredMassErr( StandardPacker::mass( ppart.measMassErr ) );

  // Lorentz momentum vector
  const auto pz   = StandardPacker::energy( ppart.lv_pz );
  const auto px   = ( isVZero ? StandardPacker::slope( ppart.lv_px ) * pz : StandardPacker::energy( ppart.lv_px ) );
  const auto py   = ( isVZero ? StandardPacker::slope( ppart.lv_py ) * pz : StandardPacker::energy( ppart.lv_py ) );
  const auto mass = ppart.lv_mass;
  const auto E    = safe_sqrt( ( px * px ) + ( py * py ) + ( pz * pz ) + ( mass * mass ) );
  part.setMomentum( Gaudi::LorentzVector( px, py, pz, E ) );

  // reference point
  part.setReferencePoint( Gaudi::XYZPoint( StandardPacker::position( ppart.refx ),
                                           StandardPacker::position( ppart.refy ),
                                           StandardPacker::position( ppart.refz ) ) );

  // Mom Cov
  auto&      momCov = *( const_cast<Gaudi::SymMatrix4x4*>( &part.momCovMatrix() ) );
  const auto merr00 =
      ( isVZero ? StandardPacker::slope( ppart.momCov00 ) * px : StandardPacker::energy( ppart.momCov00 ) );
  const auto merr11 =
      ( isVZero ? StandardPacker::slope( ppart.momCov11 ) * py : StandardPacker::energy( ppart.momCov11 ) );
  const auto merr22 = StandardPacker::energy( ppart.momCov22 );
  const auto merr33 = StandardPacker::energy( ppart.momCov33 );
  momCov( 0, 0 )    = std::pow( merr00, 2 );
  momCov( 1, 1 )    = std::pow( merr11, 2 );
  momCov( 2, 2 )    = std::pow( merr22, 2 );
  momCov( 3, 3 )    = std::pow( merr33, 2 );
  momCov( 1, 0 )    = merr11 * merr00 * StandardPacker::fraction( ppart.momCov10 );
  momCov( 2, 0 )    = merr22 * merr00 * StandardPacker::fraction( ppart.momCov20 );
  momCov( 2, 1 )    = merr22 * merr11 * StandardPacker::fraction( ppart.momCov21 );
  momCov( 3, 0 )    = merr33 * merr00 * StandardPacker::fraction( ppart.momCov30 );
  momCov( 3, 1 )    = merr33 * merr11 * StandardPacker::fraction( ppart.momCov31 );
  momCov( 3, 2 )    = merr33 * merr22 * StandardPacker::fraction( ppart.momCov32 );

  // Pos Cov
  auto&      posCov = *( const_cast<Gaudi::SymMatrix3x3*>( &part.posCovMatrix() ) );
  const auto perr00 = StandardPacker::position( ppart.posCov00 );
  const auto perr11 = StandardPacker::position( ppart.posCov11 );
  const auto perr22 = StandardPacker::position( ppart.posCov22 );
  posCov( 0, 0 )    = std::pow( perr00, 2 );
  posCov( 1, 1 )    = std::pow( perr11, 2 );
  posCov( 2, 2 )    = std::pow( perr22, 2 );
  posCov( 1, 0 )    = perr11 * perr00 * StandardPacker::fraction( ppart.posCov10 );
  posCov( 2, 0 )    = perr22 * perr00 * StandardPacker::fraction( ppart.posCov20 );
  posCov( 2, 1 )    = perr22 * perr11 * StandardPacker::fraction( ppart.posCov21 );

  // Pos Mom Cov
  auto& pmCov   = *( const_cast<Gaudi::Matrix4x3*>( &part.posMomCovMatrix() ) );
  pmCov( 0, 0 ) = StandardPacker::fltPacked( ppart.pmCov00 );
  pmCov( 0, 1 ) = StandardPacker::fltPacked( ppart.pmCov01 );
  pmCov( 0, 2 ) = StandardPacker::fltPacked( ppart.pmCov02 );
  pmCov( 1, 0 ) = StandardPacker::fltPacked( ppart.pmCov10 );
  pmCov( 1, 1 ) = StandardPacker::fltPacked( ppart.pmCov11 );
  pmCov( 1, 2 ) = StandardPacker::fltPacked( ppart.pmCov12 );
  pmCov( 2, 0 ) = StandardPacker::fltPacked( ppart.pmCov20 );
  pmCov( 2, 1 ) = StandardPacker::fltPacked( ppart.pmCov21 );
  pmCov( 2, 2 ) = StandardPacker::fltPacked( ppart.pmCov22 );
  pmCov( 3, 0 ) = StandardPacker::fltPacked( ppart.pmCov30 );
  pmCov( 3, 1 ) = StandardPacker::fltPacked( ppart.pmCov31 );
  pmCov( 3, 2 ) = StandardPacker::fltPacked( ppart.pmCov32 );

  // extra info
  for ( const auto& [k, v] : with_carry( pparts.extra(), ppart.firstExtra, ppart.lastExtra ) ) {
    part.addInfo( k, StandardPacker::fltPacked( v ) );
  }

  // end vertex
  if ( -1 != ppart.vertex ) {
    int hintID( 0 ), key( 0 );
    if ( StandardPacker::hintAndKey64( ppart.vertex, &pparts, &parts, hintID, key ) ) {
      part.setEndVertex( {&parts, hintID, key} );
      parent().debug() << "endvertex " << ppart.vertex << endmsg;
    } else {
      parent().error() << "Corrupt Particle Vertex SmartRef found" << endmsg;
    }
  }

  // protoparticle
  if ( -1 != ppart.proto ) {
    int hintID( 0 ), key( 0 );
    if ( StandardPacker::hintAndKey64( ppart.proto, &pparts, &parts, hintID, key ) ) {
      part.setProto( {&parts, hintID, key} );
      parent().debug() << "proto " << ppart.proto << endmsg;
    } else {
      parent().error() << "Corrupt Particle ProtoParticle SmartRef found" << endmsg;
    }
  }

  // daughters
  for ( auto& d : LHCb::Packer::subrange( pparts.daughters(), ppart.firstDaughter, ppart.lastDaughter ) ) {
    int hintID( 0 ), key( 0 );
    if ( StandardPacker::hintAndKey64( d, &pparts, &parts, hintID, key ) ) {
      part.addToDaughters( {&parts, hintID, key} );
      parent().debug() << "daughter " << d << endmsg;
    } else {
      parent().error() << "Corrupt Particle Daughter Particle SmartRef found" << endmsg;
    }
  }
}

void ParticlePacker::unpack( const PackedDataVector& pparts, DataVector& parts ) const {
  parts.reserve( pparts.data().size() );
  parts.setVersion( pparts.version() );
  LHCb::Packer::Carry carry{};
  for ( const auto& ppart : pparts.data() ) {
    // make and save new pid in container
    auto* part = new Data();
    int   key( 0 ), linkID( 0 );
    StandardPacker::indexAndKey64( ppart.key, linkID, key );
    parts.insert( part, ppart.key );
    // Fill data from packed object
    unpack( ppart, *part, pparts, parts, carry );
    parent().debug() << " number of daughters " << part->daughters().size() << endmsg;
  }
}

ParticlePacker::DataVector ParticlePacker::unpack( const PackedDataVector& pparts ) const {

  DataVector parts;

  parts.reserve( pparts.data().size() );
  parts.setVersion( pparts.version() );

  LHCb::Packer::Carry carry{};
  for ( const auto& ppart : pparts.data() ) {
    // make and save new pid in container
    auto* part = new Data();
    int   key( 0 ), linkID( 0 );
    StandardPacker::indexAndKey64( ppart.key, linkID, key );
    parts.insert( part, key );
    // Fill data from packed object
    unpack( ppart, *part, pparts, parts, carry );
  }
  return parts;
}

StatusCode ParticlePacker::check( const Data& dataA, const Data& dataB ) const {
  // assume OK from the start
  bool ok = true;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // checks here

  // key
  ok &= ch.compareInts( "Key", dataA.key(), dataB.key() );

  // PID
  ok &= ch.compareInts( "PID", dataA.particleID(), dataB.particleID() );

  // Mass
  ok &= ch.compareMasses( "MeasuredMass", dataA.measuredMass(), dataB.measuredMass() );

  ok &= ch.compareMasses( "MeasuredMassError", dataA.measuredMassErr(), dataB.measuredMassErr() );

  // momentum
  ok &= ch.compareLorentzVectors( "Momentum", dataA.momentum(), dataB.momentum() );

  // reference position
  ok &= ch.comparePoints( "ReferencePoint", dataA.referencePoint(), dataB.referencePoint() );

  // Mom Cov
  const std::array<double, 4> tolDiagMomCov = {
      {Packer::ENERGY_TOL, Packer::ENERGY_TOL, Packer::ENERGY_TOL, Packer::ENERGY_TOL}};
  ok &= ch.compareCovMatrices<Gaudi::SymMatrix4x4, 4>( "MomCov", dataA.momCovMatrix(), dataB.momCovMatrix(),
                                                       tolDiagMomCov, Packer::FRACTION_TOL );

  // Pos Cov
  const std::array<double, 3> tolDiagPosCov = {{Packer::POSITION_TOL, Packer::POSITION_TOL, Packer::POSITION_TOL}};
  ok &= ch.compareCovMatrices<Gaudi::SymMatrix3x3, 3>( "PosCov", dataA.posCovMatrix(), dataB.posCovMatrix(),
                                                       tolDiagPosCov, Packer::FRACTION_TOL );

  // PosMom Cov
  ok &= ch.compareMatrices<Gaudi::Matrix4x3, 4, 3>( "PosMomCov", dataA.posMomCovMatrix(), dataB.posMomCovMatrix() );

  // Extra info
  const bool extraSizeOK = dataA.extraInfo().size() == dataB.extraInfo().size();
  ok &= extraSizeOK;
  if ( extraSizeOK ) {
    auto iEA = dataA.extraInfo().begin();
    auto iEB = dataB.extraInfo().begin();
    for ( ; iEA != dataA.extraInfo().end() && iEB != dataB.extraInfo().end(); ++iEA, ++iEB ) {
      auto       mess  = fmt::format( "ExtraInfo:{}", (LHCb::Particle::additionalInfo)iEA->first );
      const bool keyOK = iEA->first == iEB->first;
      if ( !keyOK ) parent().warning() << mess << " Different Keys" << endmsg;
      ok &= keyOK;
      const double relTol = 1.0e-3;
      double       tol    = relTol * std::abs( iEA->second );
      if ( tol < relTol ) tol = relTol;
      const bool valueOK = ch.compareDoubles( mess, iEA->second, iEB->second, tol );
      ok &= valueOK;
    }
  } else {
    parent().warning() << "ExtraInfo has different sizes" << endmsg;
  }

  // end vertex
  ok &= ch.comparePointers( "EndVertex", dataA.endVertex(), dataB.endVertex() );

  // proto particle
  ok &= ch.comparePointers( "ProtoParticle", dataA.proto(), dataB.proto() );

  // daughters
  const bool dauSizeOK = dataA.daughters().size() == dataB.daughters().size();
  ok &= dauSizeOK;
  if ( dauSizeOK ) {
    auto iDA = dataA.daughters().begin();
    auto iDB = dataB.daughters().begin();
    for ( ; iDA != dataA.daughters().end() && iDB != dataB.daughters().end(); ++iDA, ++iDB ) {
      ok &= ch.comparePointers( "Daughters", &**iDA, &**iDB );
    }
  } else {
    parent().warning() << "Daughters different sizes" << endmsg;
  }

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc =
        ( dataA.parent() && dataA.parent()->registry() ? dataA.parent()->registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with Particle data packing :-" << endmsg << "  Original Particle key=" << dataA.key()
                       << " in '" << loc << "'" << endmsg << dataA << endmsg << "  Unpacked Particle" << endmsg << dataB
                       << endmsg;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}
