/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedWeightsVector.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void WeightsVectorPacker::unpack( const PackedDataVector& pweightsV, DataVector& weightsV ) const {
  const auto pVer = pweightsV.packingVersion();
  if ( !isSupportedVer( pVer ) ) return;
  weightsV.reserve( pweightsV.data().size() );
  for ( const PackedData& pweights : pweightsV.data() ) {

    // unpack the weights vector
    Data::WeightDataVector wWeights;
    wWeights.reserve( pweights.lastWeight - pweights.firstWeight );
    for ( const auto& pweight : Packer::subrange( pweightsV.weights(), pweights.firstWeight, pweights.lastWeight ) ) {
      wWeights.emplace_back( pweight.key, StandardPacker::fraction( pweight.weight ) );
    }

    // make and save new unpacked data
    auto* weights = new Data( std::move( wWeights ) );
    if ( 0 == pVer ) {
      weightsV.insert( weights );
    } else {
      weightsV.insert( weights, pweights.pvKey );
    }
  }
}

StatusCode WeightsVectorPacker::check( const Data& dataA, const Data& dataB ) const {

  // assume OK from the start
  bool ok = true;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // loop over weights and test
  const bool sizeOK = ch.compareInts( "#Weights", dataA.weights().size(), dataB.weights().size() );
  ok &= sizeOK;
  if ( sizeOK ) {
    auto iWA( dataA.weights().begin() ), iWB( dataB.weights().begin() );
    for ( ; iWA != dataA.weights().end() && iWB != dataB.weights().end(); ++iWA, ++iWB ) {
      ok &= ( *iWA ).first == ( *iWB ).first;
      ok &= ch.compareFractions( "Weight", ( *iWA ).second, ( *iWB ).second );
    }
  }

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    parent().warning() << "Problem with WeightsVector data packing :-" << endmsg << "  Original Weight : " << dataA
                       << endmsg << "  Unpacked Weight : " << dataB << endmsg;
  }
  // Return final status
  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}
