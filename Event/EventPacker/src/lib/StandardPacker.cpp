/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/StandardPacker.h"

#include "Gaudi/Algorithm.h"
#include "GaudiKernel/GaudiException.h"

#include "fmt/format.h"

#include <atomic>

//-----------------------------------------------------------------------------
// Implementation file for class : StandardPacker
//
// 2015-03-07 : Chris Jones
//-----------------------------------------------------------------------------

namespace {
  /// Counter of deprecation warning messages
  static std::atomic<unsigned int> nbReference32Warning{0};

  /// warns about usage of reference32 method, max 10 times
  inline void warnRef32Usage( Gaudi::Algorithm const& alg ) {
    int nb = ++nbReference32Warning;
    if ( nb < 10 ) {
      alg.warning() << "Creating obsolete 32bit packed reference" << endmsg;
    } else if ( nb == 10 ) {
      alg.warning() << "The WARNING message is suppressed : 'Creating obsolete 32bit packed reference'" << endmsg;
    }
  }
} // namespace

int StandardPacker::reference32( Gaudi::Algorithm const* alg, DataObject* out, const DataObject* parent,
                                 const int key ) {
  if ( alg ) warnRef32Usage( *alg );
  if ( key != ( key & 0x0FFFFFFF ) ) {
    auto mess =
        fmt::format( "************************* Key over 28 bits in StandardPacker *********************** {}", key );
    std::cerr << mess << std::endl;
    throw GaudiException( mess, "StandardPacker", StatusCode::FAILURE );
  }

  const int rawLinkID = (int)linkID( out, parent );
  if ( rawLinkID != ( rawLinkID & 0x0000000F ) ) {
    auto mess = fmt::format(
        "************************* LinkID over 4 bits in StandardPacker *********************** {}", rawLinkID );
    std::cerr << mess << std::endl;
    throw GaudiException( mess, "StandardPacker", StatusCode::FAILURE );
  }

  const int myLinkID = rawLinkID << 28;
  return key + myLinkID;
}

int StandardPacker::reference32( Gaudi::Algorithm const* alg, DataObject* out, const std::string& targetName,
                                 const int key ) {
  if ( alg ) warnRef32Usage( *alg );
  if ( key != ( key & 0x0FFFFFFF ) ) {
    auto mess =
        fmt::format( "************************* Key over 28 bits in StandardPacker *********************** {}", key );
    std::cerr << mess << std::endl;
    throw GaudiException( mess, "StandardPacker", StatusCode::FAILURE );
  }

  const int ID = (int)linkID( out, targetName );
  if ( ID != ( ID & 0x0000000F ) ) {
    auto mess =
        fmt::format( "************************* LinkID over 4 bits in StandardPacker *********************** {}", ID );
    std::cerr << mess << std::endl;
    throw GaudiException( mess, "StandardPacker", StatusCode::FAILURE );
  }

  const int myLinkID = ( ID << 28 );
  return key + myLinkID;
}

bool StandardPacker::hintAndKey32( const int data, const DataObject* source, DataObject* target, int& hint, int& key ) {
  // return status is bad by default
  bool OK = false;

  // Proceed if target and source are OK
  if ( target && source && target->linkMgr() && source->linkMgr() ) {
    // Extract the packed index and key from the data word
    int indx( 0 );
    indexAndKey32( data, indx, key );

    // Get the source link
    const auto* sourceLink = source->linkMgr()->link( indx );
    if ( sourceLink ) {
      // If link is valid, saved to target and get the hint
      hint = target->linkMgr()->addLink( sourceLink->path(), 0 );
      // finally return status is OK
      OK = true;
    }
  }

  // If failed to extract the data, reset values
  if ( !OK ) { hint = key = 0; }

  // return final status
  return OK;
}

bool StandardPacker::hintAndKey64( const std::int64_t data, const DataObject* source, DataObject* target, int& hint,
                                   int& key ) {
  // return status is bad by default
  bool OK = false;

  // Proceed if target and source are OK
  if ( target && source && target->linkMgr() && source->linkMgr() ) {
    // Extract the packed index and key from the data word
    int indx( 0 );
    indexAndKey64( data, indx, key );

    // Get the source link
    const auto* sourceLink = source->linkMgr()->link( indx );
    if ( sourceLink ) {
      // If link is valid, saved to target and get the hint
      hint = target->linkMgr()->addLink( sourceLink->path(), 0 );
      // finally return status is OK
      OK = true;
    }
  }

  // If failed to extract the data, reset values
  if ( !OK ) { hint = key = 0; }

  // return final status
  return OK;
}
