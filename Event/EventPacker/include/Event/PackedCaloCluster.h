/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackerBase.h"
#include "Event/PackerUtils.h"

#include "Event/CaloCluster.h"
#include "Event/StandardPacker.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"
#include "fmt/format.h"
#include <string>

namespace LHCb {

  /**
   *  Packed LHCb::CaloClusterEntry
   *
   *  @author Christopher Rob Jones
   *  @date   2012-03-30
   */
  struct PackedCaloClusterEntry {
    std::int64_t  digit{-1};
    std::uint32_t status{0};
    std::int16_t  fraction{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  /** @struct PackedCaloCluster Event/PackedCaloCluster.h
   *
   *  Packed LHCb::CaloCluster
   *
   *  @author Christopher Rob Jones
   *  @date   2012-03-30
   */
  struct PackedCaloCluster {
    std::int32_t  key{0};
    std::int32_t  type{0};
    std::uint32_t seed{0};
    std::int32_t  pos_x{0}, pos_y{0}, pos_z{0}, pos_e{0};
    std::int32_t  pos_c0{0}, pos_c1{0};
    std::int32_t  pos_cov00{0}, pos_cov11{0}, pos_cov22{0};
    std::int16_t  pos_cov10{0}, pos_cov20{0}, pos_cov21{0};
    std::int32_t  pos_spread00{0}, pos_spread11{0};
    std::int16_t  pos_spread10{0};
    std::uint16_t firstEntry{0}, lastEntry{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  // -----------------------------------------------------------------------

  constexpr CLID CLID_PackedCaloClusters = 1541;

  /// Namespace for locations in TDS
  namespace PackedCaloClusterLocation {
    inline const std::string Default = "pRec/Calo/Clusters";
  }

  /** @class PackedCaloClusters Event/PackedCaloCluster.h
   *
   *  Packed Calo Clusters
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class PackedCaloClusters : public DataObject {

  public:
    /// Vector of PackedCaloCluster objects
    typedef std::vector<LHCb::PackedCaloCluster> ClusterVector;

    /// Vector of PackedCaloClusterEntry objects
    typedef std::vector<LHCb::PackedCaloClusterEntry> ClusterEntryVector;

  public:
    /// Default Packing Version
    static char defaultPackingVersion() { return 0; }

  public:
    /// Class ID
    static const CLID& classID() { return CLID_PackedCaloClusters; }

    /// Class ID
    const CLID& clID() const override { return PackedCaloClusters::classID(); }

  public:
    /// Write access to the data vector
    [[nodiscard]] ClusterVector& data() { return m_clusters; }

    /// Read access to the data vector
    [[nodiscard]] const ClusterVector& data() const { return m_clusters; }

    /// Write access to the data vector
    [[nodiscard]] ClusterEntryVector& entries() { return m_entries; }

    /// Read access to the data vector
    [[nodiscard]] const ClusterEntryVector& entries() const { return m_entries; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_clusters );
      buf.save( m_entries );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedCaloClusters packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_clusters, m_packingVersion );
      buf.load( m_entries, m_packingVersion );
    }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    ClusterVector m_clusters;

    /// Packed Calo Entries
    ClusterEntryVector m_entries;
  };

  /** @class CaloClusterPacker Event/PackedCaloCluster.h
   *
   *  Utility class to handle the packing and unpacking of the Calo Clusters
   *
   *  @author Christopher Rob Jones
   *  @date   2012-03-30
   */
  class CaloClusterPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::CaloCluster        Data;
    typedef LHCb::PackedCaloCluster  PackedData;
    typedef LHCb::CaloClusters       DataVector;
    typedef LHCb::PackedCaloClusters PackedDataVector;
    static const std::string&        packedLocation() { return LHCb::PackedCaloClusterLocation::Default; }
    static const std::string&        unpackedLocation() { return LHCb::CaloClusterLocation::Default; }
    static const char*               propertyName() { return "CaloClusters"; }

    using PackerBase::PackerBase;

    bool m_packDigitRefs = true;

  public:
    /// Pack Calo Clusters
    template <typename CaloClusterRange>
    void pack( const CaloClusterRange& clus, PackedDataVector& pclus ) const {
      if ( !isSupportedVer( pclus.packingVersion() ) ) return;
      pclus.data().reserve( clus.size() );
      for ( const auto* clu : clus ) pack( *clu, pclus.data().emplace_back(), pclus );
    }

    auto& packDigitRefs( bool b ) {
      m_packDigitRefs = b;
      return *this;
    }

    /// Unpack Calo Clusters
    void unpack( const PackedDataVector& pclus, DataVector& cluss ) const;

    /// Compare two Calo Clusters to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    //
    /// Pack a CaloCluster
    void pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "RichPIDPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
