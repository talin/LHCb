/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"
#include "Event/TwoProngVertex.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <vector>

namespace LHCb {

  // Namespace for locations in TDS
  namespace PackedTwoProngVertexLocation {
    inline const std::string Default = "pRec/Vertex/V0";
  }

  /** @class PackedTwoProngVertex Event/PackedTwoProngVertex.h
   *
   *  Packed representation of LHCb::TwoProngVertex
   *
   *  @author Olivier Callot
   *  @date   2009-01-21
   */
  struct PackedTwoProngVertex {

    std::int32_t key{0};
    std::int32_t technique{0};
    std::int32_t chi2{0};
    std::int32_t nDoF{0};
    std::int32_t x{0};
    std::int32_t y{0};
    std::int32_t z{0};

    std::int32_t txA{0};
    std::int32_t tyA{0};
    std::int32_t pA{0};

    std::int32_t txB{0};
    std::int32_t tyB{0};
    std::int32_t pB{0};

    std::int32_t cov00{0};
    std::int32_t cov11{0};
    std::int32_t cov22{0};
    std::int32_t cov33{0};
    std::int32_t cov44{0};
    std::int32_t cov55{0};
    std::int32_t cov66{0};
    std::int32_t cov77{0};
    std::int32_t cov88{0};
    std::int32_t cov99{0};

    std::int16_t cov10{0};
    std::int16_t cov20{0}, cov21{0};
    std::int16_t cov30{0}, cov31{0}, cov32{0};
    std::int16_t cov40{0}, cov41{0}, cov42{0}, cov43{0};
    std::int16_t cov50{0}, cov51{0}, cov52{0}, cov53{0}, cov54{0};
    std::int16_t cov60{0}, cov61{0}, cov62{0}, cov63{0}, cov64{0}, cov65{0};
    std::int16_t cov70{0}, cov71{0}, cov72{0}, cov73{0}, cov74{0}, cov75{0}, cov76{0};
    std::int16_t cov80{0}, cov81{0}, cov82{0}, cov83{0}, cov84{0}, cov85{0}, cov86{0}, cov87{0};

    std::uint16_t firstTrack{0};
    std::uint16_t lastTrack{0};
    std::uint16_t firstInfo{0};
    std::uint16_t lastInfo{0};
    std::uint16_t firstPid{0};
    std::uint16_t lastPid{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this );
      // identical operation until version is incremented
    }
#endif
  };

  constexpr CLID CLID_PackedTwoProngVertices = 1554;

  /** @class PackedTwoProngVertices Event/PackedTwoProngVertex.h
   *
   *  Container of packed LHCb::TwoProngVertex objects.
   *
   *  @author Olivier Callot
   *  @date   2009-01-21
   */

  /// Namespace for locations in TDS
  namespace PackedTwoProngVertexLocation {
    inline const std::string User     = "pPhys/User/TwoProngVertices";
    inline const std::string InStream = "/pPhys/TwoProngVertices";
  } // namespace PackedTwoProngVertexLocation
  // namespace PackedVertexLocation

  class PackedTwoProngVertices : public DataObject {

  public:
    /// Default Packing Version
    static char defaultPackingVersion() { return 1; }

  public:
    const CLID&        clID() const override { return PackedTwoProngVertices::classID(); }
    static const CLID& classID() { return CLID_PackedTwoProngVertices; }

  public:
    std::vector<PackedTwoProngVertex>&       data() { return m_vect; }
    const std::vector<PackedTwoProngVertex>& data() const { return m_vect; }

    std::vector<std::int64_t>&       refs() { return m_refs; }
    const std::vector<std::int64_t>& refs() const { return m_refs; }

    std::vector<std::pair<int, int>>&       extras() { return m_extra; }
    const std::vector<std::pair<int, int>>& extras() const { return m_extra; }

  public:
    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
      buf.save( m_refs );
      buf.save( m_extra );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion < 1 || m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedParticles packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_vect, m_packingVersion );
      buf.load( m_refs );
      buf.load( m_extra, m_packingVersion );
    }

  private:
    std::vector<PackedTwoProngVertex> m_vect;
    std::vector<std::int64_t>         m_refs;
    std::vector<std::pair<int, int>>  m_extra;

    // Data packing version.
    char m_packingVersion{defaultPackingVersion()};
  };

  class TwoProngVertexPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::TwoProngVertex         Data;
    typedef LHCb::PackedTwoProngVertex   PackedData;
    typedef LHCb::TwoProngVertices       DataVector;
    typedef LHCb::PackedTwoProngVertices PackedDataVector;
    static const std::string&            packedLocation() { return LHCb::PackedTwoProngVertexLocation::User; }
    static const std::string&            unpackedLocation() { return LHCb::TwoProngVertexLocation::Default; }
    static const char*                   propertyName() { return "TwoProngVertices"; }

    using PackerBase::PackerBase;

    /// Pack TwoProngVertices
    template <typename Range>
    void pack( const Range& verts, PackedDataVector& pverts ) const {
      pverts.data().reserve( verts.size() );
      for ( const auto* vert : verts ) pack( *vert, pverts.data().emplace_back(), pverts );
    }

    /// Unpack a TwoProngVertex
    void unpack( const PackedData& pvert, Data& vert, const PackedDataVector& pverts, DataVector& verts ) const;

    /// Unpack TwoProngVertices
    void unpack( const PackedDataVector& pverts, DataVector& verts ) const;

    /// Unpack TwoProngVertices
    DataVector unpack( const PackedDataVector& pverts ) const;

    /// Compare two TwoProngVertices to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    /// Pack a TwoProngVertex
    void pack( const Data& vert, PackedData& pvert, PackedDataVector& pverts ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 1 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "TwoProngVertexPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
