/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/MCRichOpticalPhoton.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <string>

namespace LHCb {

  /**
   *  Packed MCRichOpticalPhoton
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct PackedMCRichOpticalPhoton {
    std::int32_t key{0};
    std::int32_t hpdx{0}, hpdy{0}, hpdz{0};
    std::int32_t pmirx{0}, pmiry{0}, pmirz{0};
    std::int32_t smirx{0}, smiry{0}, smirz{0};
    std::int32_t aerox{0}, aeroy{0}, aeroz{0};
    std::int32_t theta{0}, phi{0};
    std::int32_t emisx{0}, emisy{0}, emisz{0};
    std::int32_t energy{0};
    std::int32_t pmomx{0}, pmomy{0}, pmomz{0};
    std::int32_t hpdqwx{0}, hpdqwy{0}, hpdqwz{0};
    std::int64_t mcrichhit{-1};
  };

  constexpr CLID CLID_PackedMCRichOpticalPhotons = 1522;

  /// Namespace for locations in TDS
  namespace PackedMCRichOpticalPhotonLocation {
    inline const std::string Default = "pSim/Rich/OpticalPhotons";
  }

  /**
   *  Packed MCRichOpticalPhotons
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class PackedMCRichOpticalPhotons : public DataObject {
  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedMCRichOpticalPhoton> Vector;

  public:
    /// Default Packing Version
    static char defaultPackingVersion() { return 1; }

  public:
    /// Class ID
    static const CLID& classID() { return CLID_PackedMCRichOpticalPhotons; }

    /// Class ID
    const CLID& clID() const override { return PackedMCRichOpticalPhotons::classID(); }

  public:
    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;
  };

  /**
   *  Utility class to handle the packing and unpacking of the MCRichOpticalPhotons
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class MCRichOpticalPhotonPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::MCRichOpticalPhoton        Data;
    typedef LHCb::PackedMCRichOpticalPhoton  PackedData;
    typedef LHCb::MCRichOpticalPhotons       DataVector;
    typedef LHCb::PackedMCRichOpticalPhotons PackedDataVector;
    static const std::string& packedLocation() { return LHCb::PackedMCRichOpticalPhotonLocation::Default; }
    static const std::string& unpackedLocation() { return LHCb::MCRichOpticalPhotonLocation::Default; }

  public:
    using PackerBase::PackerBase;

    /// Pack an MCRichOpticalPhoton
    void pack( const DataVector& phots, PackedDataVector& pphots ) const;

    /// Unpack an MCRichOpticalPhoton
    void unpack( const PackedDataVector& pphots, DataVector& phots ) const;

    /// Compare two MCRichHits to check the packing -> unpacking performance
    StatusCode check( const DataVector& dataA, const DataVector& dataB ) const;

  private:
    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "MCRichOpticalPhotonPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }

    /// Scale factor for photon energies
    double PhotEnScale{5.0e8};
  };

} // namespace LHCb
