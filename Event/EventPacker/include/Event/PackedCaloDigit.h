/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/CaloDigit.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <string>

namespace LHCb {

  /**
   *  Packed LHCb::CaloDigit
   *
   *  @author Olivier Deschamps
   *  @date   2017-06-08
   */
  struct PackedCaloDigit {
    std::int32_t key{0};
    std::int32_t e{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  constexpr CLID CLID_PackedCaloDigits = 1542;

  /// Namespace for locations in TDS
  namespace PackedCaloDigitLocation {
    inline const std::string Default = "pRec/Calo/Digits";
    inline const std::string Ecal    = "pRec/Ecal/Digits";
    inline const std::string Hcal    = "pRec/Hcal/Digits";
    inline const std::string Prs     = "pRec/Prs/Digits";
    inline const std::string Spd     = "pRec/Spd/Digits";
  } // namespace PackedCaloDigitLocation

  /**
   *  Packed Calo Digits
   *
   *  @author Olivier Deschamps
   *  @date   2017-06-08
   */
  class PackedCaloDigits : public DataObject {
  public:
    /// Vector of PackedCaloDigit objects
    typedef std::vector<LHCb::PackedCaloDigit> CaloDigitVector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 0; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedCaloDigits; }

    /// Class ID
    const CLID& clID() const override { return PackedCaloDigits::classID(); }

    /// Write access to the data vector
    [[nodiscard]] CaloDigitVector& data() { return m_digits; }

    /// Read access to the data vector
    [[nodiscard]] const CaloDigitVector& data() const { return m_digits; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_digits );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedCaloDigits packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_digits, m_packingVersion );
    }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    CaloDigitVector m_digits;
  };

  /**
   *  Utility class to handle the packing and unpacking of the Calo Digits
   *
   *  @author Olivier Deschamps
   *  @date   2017-06-08
   */
  class CaloDigitPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::CaloDigit        Data;
    typedef LHCb::PackedCaloDigit  PackedData;
    typedef LHCb::CaloDigits       DataVector;
    typedef LHCb::PackedCaloDigits PackedDataVector;
    static const std::string&      packedLocation() { return LHCb::PackedCaloDigitLocation::Default; }
    static const std::string&      unpackedLocation() { return LHCb::CaloDigitLocation::Default; }
    static const char*             propertyName() { return "CaloDigits"; }

    using PackerBase::PackerBase;

    /// Pack Calo Clusters
    template <typename CaloDigitRange>
    void pack( const CaloDigitRange& digits, PackedDataVector& pdigits ) const {
      if ( !isSupportedVer( pdigits.packingVersion() ) ) return;
      pdigits.data().reserve( digits.size() );
      for ( const auto* digit : digits ) pack( *digit, pdigits.data().emplace_back(), pdigits );
    }

    /// Unpack Calo Digits
    void unpack( const PackedDataVector& pdig, DataVector& digs ) const;

    /// Compare two Calo Clusters to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    /// Pack a CaloDigit
    void pack( const Data& dig, PackedData& pdig, PackedDataVector& pdigs ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "CaloDigitPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
