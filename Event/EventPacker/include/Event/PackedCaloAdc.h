/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/CaloAdc.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <string>

namespace LHCb {

  /**
   *  Packed LHCb::CaloAdc
   *
   *  @author Olivier Deschamps
   *  @date   2017-06-08
   */
  struct PackedCaloAdc {
    std::int32_t key{0};
    std::int32_t adc{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  constexpr CLID CLID_PackedCaloAdcs = 1543;

  /// Namespace for locations in TDS
  namespace PackedCaloAdcLocation {
    inline const std::string Ecal = "pRec/Ecal/Adcs";
    inline const std::string Hcal = "pRec/Hcal/Adcs";
    inline const std::string Prs  = "pRec/Prs/Adcs";
    inline const std::string Spd  = "pRec/Spd/Adcs";
  } // namespace PackedCaloAdcLocation

  /**
   *  Packed Calo ADCs
   *
   *  @author Olivier Deschamps
   *  @date   2017-06-08
   */
  class PackedCaloAdcs : public DataObject {
  public:
    /// Vector of PackedCaloAdc objects
    typedef std::vector<LHCb::PackedCaloAdc> CaloAdcVector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 0; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedCaloAdcs; }

    /// Class ID
    const CLID& clID() const override { return PackedCaloAdcs::classID(); }

    /// Write access to the data vector
    [[nodiscard]] CaloAdcVector& data() { return m_adcs; }

    /// Read access to the data vector
    [[nodiscard]] const CaloAdcVector& data() const { return m_adcs; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_adcs );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedCaloAdcs packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_adcs, m_packingVersion );
    }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    CaloAdcVector m_adcs;
  };

  /**
   *  Utility class to handle the packing and unpacking of the Calo ADCs
   *
   *  @author Olivier Deschamps
   *  @date   2017-06-08
   */
  class CaloAdcPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::CaloAdc        Data;
    typedef LHCb::PackedCaloAdc  PackedData;
    typedef LHCb::CaloAdcs       DataVector;
    typedef LHCb::PackedCaloAdcs PackedDataVector;
    static const std::string&    packedLocation() { return LHCb::PackedCaloAdcLocation::Ecal; }
    static const std::string&    unpackedLocation() { return LHCb::CaloAdcLocation::Ecal; }
    static const char*           propertyName() { return "CaloAdcs"; }

    using PackerBase::PackerBase;

  public:
    /// Pack Calo ADCS
    template <typename CaloAdcRange>
    void pack( const CaloAdcRange& adcs, PackedDataVector& padcs ) const {
      if ( !isSupportedVer( padcs.packingVersion() ) ) return;
      padcs.data().reserve( adcs.size() );
      for ( const auto* adc : adcs ) pack( *adc, padcs.data().emplace_back(), padcs );
    }

    /// Unpack Calo ADCs
    void unpack( const PackedDataVector& padc, DataVector& adcs ) const;

    /// Compare two Calo ADCs to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    /// Pack a CaloADC
    void pack( const Data& adc, PackedData& padc, PackedDataVector& padcs ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "CaloAdcPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
