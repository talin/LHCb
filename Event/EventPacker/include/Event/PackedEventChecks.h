/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Gaudi/Algorithm.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "StandardPacker.h"
#include "fmt/format.h"
#include <type_traits>

namespace LHCb {
  namespace Packer {
    constexpr double ENERGY_TOL   = 0.51 * ENERGY_INV_SCALE;   ///< .01 MeV steps
    constexpr double POSITION_TOL = 0.51 * POSITION_INV_SCALE; ///< 0.1 micron steps
    constexpr double SLOPE_TOL    = 0.51 * SLOPE_INV_SCALE;    ///< full scale +- 20 radians
    constexpr double FRACTION_TOL = 0.51 * FRACTION_INV_SCALE; ///< store in std::int16_t.
    constexpr double TIME_TOL     = 0.51 * TIME_INV_SCALE;     ///< 0.0001 ns resolution
    constexpr double DELTALL_TOL  = 0.51 * DELTALL_INV_SCALE;  ///< 0.0001 precision
    constexpr double MASS_TOL     = 0.51 * MASS_INV_SCALE;     ///< 1 keV steps
    constexpr double MVA_TOL      = 0.51 * MVA_SCALE;          ///< Scale for MVAs
  }                                                            // namespace Packer
} // namespace LHCb

namespace DataPacking {

  /** @class DataChecks Event/PackedEventChecks.h
   *
   *  Basic utilities to check the packed data
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-15
   */

  template <typename, typename = void>
  constexpr bool is_iterable = false;

  template <typename T>
  constexpr bool is_iterable<T, std::void_t<decltype( std::declval<T>().begin() )>> = true;

  class DataChecks {

  public:
    /// Constructor
    explicit DataChecks( const Gaudi::Algorithm* p ) : parent( p ) {}

    /// Constructor
    explicit DataChecks( const Gaudi::Algorithm& p ) : parent( &p ) {}

  public:
    template <typename Packer, typename Range, typename DataVector>
    auto check( Packer& p, Range&& dataA, DataVector& dataB ) const {

      if constexpr ( is_iterable<DataVector> )
        return dataA.size() == dataB.size()
                   ? std::inner_product(
                         dataA.begin(), dataA.end(), dataB.begin(), StatusCode{StatusCode::SUCCESS},
                         []( StatusCode sc1, StatusCode sc2 ) { return sc1.isSuccess() ? sc2 : sc1; },
                         [&]( const auto& a, const auto& b ) { return p.check( *a, *b ); } )
                   : StatusCode{StatusCode::FAILURE};
      else
        return p.check( dataA, dataB );
    }

    /// Compare two Matrices
    template <class TYPE, unsigned int N, unsigned int M>
    bool compareMatrices( const std::string& name, const TYPE& a, const TYPE& b, const double tol = 5.0e-3 ) const {
      bool ok = true;
      for ( unsigned int n = 0; n < N; ++n ) {
        for ( unsigned int m = 0; m < M; ++m ) {
          double tolRel = tol * std::abs( a( n, m ) );
          if ( tolRel < tol ) tolRel = tol;
          ok &= compareDoubles( fmt::format( "{}:{}{}", name, n, m ), a( n, m ), b( n, m ), tolRel );
        }
      }
      return ok;
    }

    /// Compare two 'Covariance' Matrices
    template <class TYPE, unsigned int N>
    bool compareCovMatrices( const std::string& name, const TYPE& a, const TYPE& b,
                             const std::array<double, N> tolOnDiag, const double tolOffDiag ) const {
      bool ok = true;
      for ( unsigned int n = 0; n < N; ++n ) {
        for ( unsigned int m = n; m < N; ++m ) {
          auto text = fmt::format( "{}:{}{}", name, n, m );
          if ( m == n ) { // On diagonal
            ok &= compareDoubles( text, std::sqrt( a( n, m ) ), std::sqrt( b( n, m ) ), tolOnDiag[n] );
          } else { // Off diagonal
            const auto testA = ( std::abs( a( n, n ) ) > 0 && std::abs( a( m, m ) ) > 0
                                     ? a( n, m ) / std::sqrt( a( n, n ) * a( m, m ) )
                                     : 0 );
            const auto testB = ( std::abs( b( n, n ) ) > 0 && std::abs( b( m, m ) ) > 0
                                     ? b( n, m ) / std::sqrt( b( n, n ) * b( m, m ) )
                                     : 0 );
            ok &= compareDoubles( text, testA, testB, tolOffDiag );
          }
        }
      }
      return ok;
    }

    /// Compare two Lorentz vector
    bool compareLorentzVectors( const std::string& name, const Gaudi::LorentzVector& a, const Gaudi::LorentzVector& b,
                                const double tolV = 5.0e-3, const double tolE = 1.0e-2 ) const;

    /// Compare two points to within the given tolerance
    bool comparePoints( const std::string& name, const Gaudi::XYZPoint& a, const Gaudi::XYZPoint& b,
                        const double tol = LHCb::Packer::POSITION_TOL ) const;

    /// Compare two XYZ vectors to within the given tolerance
    bool compareVectors( const std::string& name, const Gaudi::XYZVector& a, const Gaudi::XYZVector& b,
                         const double tol = 1.0e-4 ) const;

    /// Compare two 3D vectors to within the given tolerance
    bool compareVectors( const std::string& name, const Gaudi::Vector3& a, const Gaudi::Vector3& b,
                         const double tol = 1.0e-4 ) const;

    /// Compare two 2D vectors to within the given tolerance
    bool compareVectors( const std::string& name, const Gaudi::Vector2& a, const Gaudi::Vector2& b,
                         const double tol = 1.0e-4 ) const;

    /// Compare two double values
    bool compareDoubles( const std::string& name, const double& a, const double& b, const double tol = 1.0e-4 ) const;

    /// Compare two float values
    bool compareFloats( const std::string& name, const float& a, const float& b, const float tol = 1.0e-4 ) const;

    bool compare( const std::string& name, const float& a, const float& b, const float tol = 1.0e-4 ) const {
      return compareFloats( name, a, b, tol );
    }

    bool compare( const std::string& name, const double& a, const double& b, const double tol = 1.0e-4 ) const {
      return compareDoubles( name, a, b, tol );
    }

    /// Compare two unsigned int values
    template <class TYPE>
    bool compareInts( const std::string& name, const TYPE& a, const TYPE& b ) const {
      const bool ok = ( a == b );
      if ( !ok && parent ) {
        parent->warning() << name << " comparison failed :-" << endmsg << " Original = " << a << endmsg
                          << " Unpacked = " << b << endmsg;
      }
      return ok;
    }

    /// Compare two double 'energy' values
    bool compareEnergies( const std::string& name, const double& a, const double& b,
                          const double tol = LHCb::Packer::ENERGY_TOL ) const {
      return compareDoubles( name, a, b, tol );
    }

    /// Compare two double 'energy' vectors (e.g. Momentum vectors)
    bool compareEnergies( const std::string& name, const Gaudi::XYZVector& a, const Gaudi::XYZVector& b,
                          const double tol = LHCb::Packer::ENERGY_TOL ) const {
      return compareVectors( name, a, b, tol );
    }

    template <class TYPE>
    bool comparePointers( const std::string& name, const TYPE* a, const TYPE* b ) const {
      const bool ok = ( a == b );
      if ( !ok && parent ) {
        parent->warning() << name << " comparison failed :-" << endmsg << " Original = " << a << endmsg
                          << " Unpacked = " << b << endmsg;
      }
      return ok;
    }

    /// Compare two double 'position' values
    bool comparePositions( const std::string& name, const double& a, const double& b,
                           const double tol = LHCb::Packer::POSITION_TOL ) const {
      return compareDoubles( name, a, b, tol );
    }

    /// Compare two double 'slope' values
    bool compareSlopes( const std::string& name, const double& a, const double& b,
                        const double tol = LHCb::Packer::SLOPE_TOL ) const {
      return compareDoubles( name, a, b, tol );
    }
    /// Compare two double 'fraction' values
    bool compareFractions( const std::string& name, const double& a, const double& b,
                           const double tol = LHCb::Packer::FRACTION_TOL ) const {
      return compareDoubles( name, a, b, tol );
    }

    /// Compare two double 'mva' values
    bool compareMVAs( const std::string& name, const double& a, const double& b,
                      const double tol = LHCb::Packer::MVA_TOL ) const {
      return compareDoubles( name, a, b, tol );
    }

    /// Compare two double 'time' values
    bool compareTimes( const std::string& name, const double& a, const double& b,
                       const double tol = LHCb::Packer::TIME_TOL ) const {
      return compareDoubles( name, a, b, tol );
    }

    /// Compare two double 'deltaLL' values
    bool compareDeltaLLs( const std::string& name, const double& a, const double& b,
                          const double tol = LHCb::Packer::DELTALL_TOL ) const {
      return compareDoubles( name, a, b, tol );
    }

    /// Compare two double 'mass' values
    bool compareMasses( const std::string& name, const double& a, const double& b,
                        const double tol = LHCb::Packer::MASS_TOL ) const {
      return compareDoubles( name, a, b, tol );
    }

  private:
    /// MessageStream
    const Gaudi::Algorithm* parent = nullptr;
  };

} // namespace DataPacking
