/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackerBase.h"
#include "Event/RecSummary.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <string>

namespace LHCb {

  /**
   *  Packed RecSummary
   *
   */

  constexpr CLID CLID_PackedRecSummary = 1006; // Fixme how is this defined????

  /// Namespace for locations in TDS
  namespace PackedRecSummaryLocation {
    inline const std::string Default = "pRec/RecSummary";
  }
  /**
   *  Packed RecSummarys
   *
   */
  class PackedRecSummary : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<int> Vector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 1; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedRecSummary; }

    /// Class ID
    const CLID& clID() const override { return PackedRecSummary::classID(); }

    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion < 1 || m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedRecSummary packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_vect );
    }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;
  };

  /**
   *  Utility class to handle the packing and unpacking of the RecSummary
   *
   */
  class RecSummaryPacker : public PackerBase {
  public:
    typedef LHCb::RecSummary       DataVector;
    typedef LHCb::PackedRecSummary PackedDataVector;
    static const std::string&      packedLocation() { return PackedRecSummaryLocation::Default; }
    static const std::string&      unpackedLocation() { return RecSummaryLocation::Default; }
    static const char*             propertyName() { return "RecSummary"; }

    using PackerBase::PackerBase;

    /// Pack RecSummary
    void pack( const DataVector& hits, PackedDataVector& phits ) const;

    /// Unpack RecSummary
    void unpack( const PackedDataVector& phits, DataVector& hits ) const;

    /// Compare two RecSummarys to check the packing -> unpacking performance
    StatusCode check( const DataVector& dataA, const DataVector& dataB ) const;

  private:
    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "RecSummaryPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
