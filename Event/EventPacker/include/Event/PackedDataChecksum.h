/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <boost/crc.hpp>
#include <boost/pfr/core.hpp>
#include <map>
#include <type_traits>
#include <vector>

namespace LHCb::Hlt::PackedData {

  /** @class PackedDataChecksum PackedDataChecksum.h
   *  Helper class that calculates packed object checksums
   *
   *  @author Rosen Matev
   *  @date   2016-01-03
   */
  class PackedDataChecksum {
    struct Adaptor {
      PackedDataChecksum* parent;
      std::string_view    key;
      template <typename U>
      void save( const U& arg ) {
        parent->process( key, arg );
      }
    };

  public:
    template <typename T>
    void processObject( const T& x, std::string_view key ) {
      auto processor = Adaptor{this, key};
      x.save( processor );
    }

    /// Obtain the current value of a checksum
    int checksum( std::string_view key = "" ) { return cksum_for( key ).checksum(); }

    /// Obtain the current value of all checksums
    std::map<std::string, int> checksums() const {
      std::map<std::string, int> result;
      for ( const auto& x : m_result ) { result.emplace( x.first, x.second.checksum() ); }
      return result;
    }

    /// Reset the checksums
    void reset() {
      for ( auto& x : m_result ) { x.second.reset( 0 ); }
    }

  private:
    template <typename T>
    void process( std::string_view key, const T& x ) {
      static_assert( std::is_trivially_copyable_v<T> );
      cksum_for( key ).process_bytes( &x, sizeof( x ) );
      // Add to a "global" checksum, too
      cksum_for( "_global_" ).process_bytes( &x, sizeof( x ) );
    }

    // Specialization for pairs as they might be padded
    template <typename T1, typename T2>
    void process( std::string_view key, const std::pair<T1, T2>& x ) {
      process( key, std::forward_as_tuple( x.first, x.second ) );
    }

    template <typename... T>
    void process( std::string_view key, const std::tuple<T...>& x ) {
      std::apply( [&]( const auto&... arg ) { ( process( key, arg ), ... ); }, x );
    }

    template <typename T, typename Allocator>
    void process( std::string_view key, const std::vector<T, Allocator>& v ) {
      for ( const auto& x : v ) process( key, x );
    }

    std::map<std::string, boost::crc_32_type, std::less<>> m_result;

    [[nodiscard]] boost::crc_32_type& cksum_for( std::string_view k ) {
      auto i = m_result.find( k );
      if ( i == m_result.end() ) { i = m_result.emplace( k, boost::crc_32_type{} ).first; }
      return i->second;
    }
  };

} // namespace LHCb::Hlt::PackedData
