/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <cmath>

namespace LHCb::Packer::Utils {

  /// Safe sqrt ...
  template <typename TYPE>
  static auto safe_sqrt( const TYPE x ) {
    return ( x > TYPE( 0 ) ? std::sqrt( x ) : TYPE( 0 ) );
  }

  /// Safe divide ...
  template <typename TYPE>
  static auto safe_divide( const TYPE a, const TYPE b ) {
    return ( b != TYPE( 0 ) ? a / b : TYPE( 9e9 ) );
  }

} // namespace LHCb::Packer::Utils
