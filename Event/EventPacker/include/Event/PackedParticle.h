/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackerBase.h"
#include "Event/Particle.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/StatusCode.h"
#include "PackerUtils.h"

#include <string>

namespace LHCb {

  /**
   *  Packed Particle
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct PackedParticle {

    // packed data members
    std::int64_t key{0};                       ///< reference to the original container + key of the particle
    std::int32_t particleID{0};                ///< PID Code
    std::int32_t measMass{0};                  ///< Measured mass
    std::int32_t measMassErr{0};               ///< Error on the measured mass
    std::int32_t lv_px{0}, lv_py{0}, lv_pz{0}; ///< 3D Momemtum part of Lorentz vector
    float        lv_mass{0};                   ///< Mass part of Lorentz vector
    std::int32_t refx{0}, refy{0}, refz{0};    ///< reference point

    // Momentum Cov matrix
    std::int32_t momCov00{0}, momCov11{0}, momCov22{0}, momCov33{0};
    std::int16_t momCov10{0};
    std::int16_t momCov20{0}, momCov21{0};
    std::int16_t momCov30{0}, momCov31{0}, momCov32{0};

    // Position Cov matrix
    std::int32_t posCov00{0}, posCov11{0}, posCov22{0};
    std::int16_t posCov10{0};
    std::int16_t posCov20{0}, posCov21{0};

    // PosMom Cov matrix
    std::int32_t pmCov00{0}, pmCov01{0}, pmCov02{0};
    std::int32_t pmCov10{0}, pmCov11{0}, pmCov12{0};
    std::int32_t pmCov20{0}, pmCov21{0}, pmCov22{0};
    std::int32_t pmCov30{0}, pmCov31{0}, pmCov32{0};

    // Extra info
    std::uint32_t firstExtra{0}, lastExtra{0};

    // End Vertex
    std::int64_t vertex{-1};

    // ProtoParticle
    std::int64_t proto{-1};

    // daughters
    std::uint32_t firstDaughter{0}, lastDaughter{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  constexpr CLID CLID_PackedParticles = 1581;

  /// Namespace for locations in TDS
  namespace PackedParticleLocation {
    inline const std::string User     = "pPhys/User/Particles";
    inline const std::string InStream = "/pPhys/Particles";
  } // namespace PackedParticleLocation

  /**
   *  Packed Particles
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class PackedParticles : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedParticle> Vector;

    /// Extra info pair
    typedef std::pair<std::int32_t, std::int32_t> PackedExtraInfo;

    /// Extra info vector
    typedef std::vector<PackedExtraInfo> PackedExtraInfoVector;

    /// Daughters
    typedef std::vector<std::int64_t> Daughters;

    /// Default Packing Version
    static char defaultPackingVersion() { return 1; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedParticles; }

    /// Class ID
    const CLID& clID() const override { return PackedParticles::classID(); }

    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Write access to the extra info
    PackedExtraInfoVector& extra() { return m_extra; }

    /// Read access to the extra info
    const PackedExtraInfoVector& extra() const { return m_extra; }

    /// Write access to the daughters
    Daughters& daughters() { return m_daughters; }

    /// Read access to the extra info
    const Daughters& daughters() const { return m_daughters; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
      buf.save( m_extra );
      buf.save( m_daughters );
    }
    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion < 1 || m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedParticles packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_vect, m_packingVersion );
      buf.load( m_extra, m_packingVersion );
      buf.load( m_daughters );
    }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;

    /// The extra info
    PackedExtraInfoVector m_extra;

    /// Vector of packed daughter smartrefs
    Daughters m_daughters;
  };

  /**
   *  Utility class to handle the packing and unpacking of the Particles
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class ParticlePacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::Particle        Data;
    typedef LHCb::PackedParticle  PackedData;
    typedef LHCb::Particles       DataVector;
    typedef LHCb::PackedParticles PackedDataVector;
    static const std::string&     packedLocation() { return LHCb::PackedParticleLocation::User; }
    static const std::string&     unpackedLocation() { return LHCb::ParticleLocation::User; }
    static const char*            propertyName() { return "Particles"; }

    using PackerBase::PackerBase;

    /// Pack Particles
    template <typename ParticleRange>
    void pack( ParticleRange&& parts, PackedDataVector& pparts ) const {
      pparts.data().reserve( parts.size() );
      // TODO: check that all 'part' have the same parent
      for ( const Data* part : parts ) pack( *part, pparts.data().emplace_back(), pparts );
    }

    /// Unpack Particles
    void unpack( const PackedDataVector& pparts, DataVector& parts ) const;

    /// Unpack Particles
    DataVector unpack( const PackedDataVector& pparts ) const;

    /// Unpack a single Particle
    void unpack( const PackedData& ppart, Data& part, const PackedDataVector& pparts, DataVector& parts,
                 LHCb::Packer::Carry& ) const;

    /// Compare two Particles to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    /// Pack a single Particle
    void pack( const Data& part, PackedData& ppart, PackedDataVector& pparts ) const;
  };

} // namespace LHCb
