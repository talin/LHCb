/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackerBase.h"
#include "Event/ProtoParticle.h"
#include "Event/StandardPacker.h"

#include "Gaudi/Algorithm.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <string>
#include <vector>

namespace LHCb {

  /**
   *  This is the packed version of the ProtoParticle
   *
   *  @author Olivier Callot
   *  @date   2008-11-13
   */
  struct PackedProtoParticle {
    std::int64_t  key{0};
    std::int64_t  track{0};
    std::int64_t  richPID{0};
    std::int64_t  muonPID{0};
    std::uint16_t firstHypo{0};
    std::uint16_t lastHypo{0};
    std::uint16_t firstExtra{0};
    std::uint16_t lastExtra{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  constexpr CLID CLID_PackedProtoParticles = 1552;

  // Namespace for locations in TDS
  namespace PackedProtoParticleLocation {
    inline const std::string Charged  = "pRec/ProtoP/Charged";
    inline const std::string Neutral  = "pRec/Protop/Neutral";
    inline const std::string InStream = "/pRec/ProtoP/Custom";
  } // namespace PackedProtoParticleLocation

  /** @class PackedProtoParticles PackedProtoParticle.h Event/PackedProtoParticle.h
   *
   *  Container of PackedProtoParticle objects
   *
   *  @author Olivier Callot
   *  @date   2008-11-13
   */
  class PackedProtoParticles : public DataObject {

  public:
    /// Default Packing Version
    static char defaultPackingVersion() { return 1; }

  public:
    const CLID&        clID() const override { return PackedProtoParticles::classID(); }
    static const CLID& classID() { return CLID_PackedProtoParticles; }

  public:
    std::vector<PackedProtoParticle>&       data() { return m_vect; }
    const std::vector<PackedProtoParticle>& data() const { return m_vect; }

    std::vector<std::int64_t>&       refs() { return m_refs; }
    const std::vector<std::int64_t>& refs() const { return m_refs; }

    std::vector<std::pair<int, int>>&       extras() { return m_extra; }
    const std::vector<std::pair<int, int>>& extras() const { return m_extra; }

  public:
    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
      buf.save( m_refs );
      buf.save( m_extra );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion < 1 || m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedProtoParticles packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_vect, m_packingVersion );
      buf.load( m_refs );
      buf.load( m_extra, m_packingVersion );
    }

  private:
    std::vector<PackedProtoParticle> m_vect;
    std::vector<std::int64_t>        m_refs;
    std::vector<std::pair<int, int>> m_extra;

    // Data packing version
    char m_packingVersion{defaultPackingVersion()};
  };

  /**
   *  Utility class to handle the packing and unpacking of ProtoParticles
   *
   *  @author Christopher Rob Jones
   *  @date   05/04/2012
   */
  class ProtoParticlePacker : public PackerBase {

  public:
    typedef LHCb::ProtoParticle        Data;
    typedef LHCb::PackedProtoParticle  PackedData;
    typedef LHCb::ProtoParticles       DataVector;
    typedef LHCb::PackedProtoParticles PackedDataVector;
    static const std::string&          packedLocation() { return LHCb::PackedProtoParticleLocation::Charged; }
    static const std::string&          unpackedLocation() { return LHCb::ProtoParticleLocation::Charged; }
    static const char*                 propertyName() { return "ProtoParticles"; }

  public:
    using PackerBase::PackerBase;

    /// Pack ProtoParticles
    template <typename ProtoParticlesRange>
    void pack( const ProtoParticlesRange& protos, PackedDataVector& pprotos ) const {
      pprotos.data().reserve( protos.size() );
      pprotos.setVersion( 2 ); // Not sure why this is different than  protos.version()

      for ( const LHCb::ProtoParticle* proto : protos ) {
        if ( proto ) pack( *proto, pprotos.data().emplace_back(), pprotos );
      }
    }

    /// Unpack ProtoParticles
    void unpack( const PackedDataVector& pprotos, DataVector& protos ) const;

    /// Unpack a single ProtoParticle -- this only works if `unpack` is called walking sequentially through the vector,
    /// without skipping any entries
    void unpack( const PackedData& pproto, Data& proto, const PackedDataVector& pprotos, DataVector& protos,
                 LHCb::Packer::Carry& carry ) const;

    /// Compare two ProtoParticles to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    /// Pack a ProtoParticle
    void pack( const Data& proto, PackedData& pproto, PackedDataVector& pprotos ) const;
    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "ProtoParticlePacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
