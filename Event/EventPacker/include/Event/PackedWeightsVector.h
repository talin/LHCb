/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"
#include "Event/WeightsVector.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <string>

namespace LHCb {

  /**
   *  Packed int to weight
   *
   *  @author Christopher Rob Jones
   *  @date   2010-09-22
   */
  struct PackedWeight {
    /// Default constructor
    PackedWeight() = default;

    /// Constructor from values
    PackedWeight( const int _key, const std::int16_t _weight ) : key( _key ), weight( _weight ) {}

    std::int32_t key{0};    ///< key of the track this weight is associated with
    std::int16_t weight{0}; ///< Weight of this track in the vertex
  };

  /**
   *  Packed vector of Weights
   *
   *  @author Christopher Rob Jones
   *  @date   2010-09-22
   */
  struct PackedWeights {
    std::uint16_t firstWeight{0}; ///< index to first weight
    std::uint16_t lastWeight{0};  ///< index to last weight
    std::uint32_t pvKey{0};       ///< The PV Key

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }
    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  constexpr CLID CLID_PackedWeightsVector = 1555;

  /// Namespace for locations in TDS
  namespace PackedWeightsVectorLocation {
    inline const std::string Default = "pRec/Vertex/Weights";
  }

  /**
   *  Packed WeightsVectors
   *
   *  @author Christopher Rob Jones
   *  @date   2010-09-22
   */
  class PackedWeightsVector : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedWeights> WeightsVector;

    /// Vector of raw weights
    typedef std::vector<LHCb::PackedWeight> WeightVector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 0; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedWeightsVector; }

    /// Class ID
    const CLID& clID() const override { return PackedWeightsVector::classID(); }

    /// Write access to the data vector
    WeightsVector& data() { return m_weights; }

    /// Read access to the data vector
    const WeightsVector& data() const { return m_weights; }

    /// Write access to the data vector
    WeightVector& weights() { return m_weight; }

    /// Read access to the data vector
    const WeightVector& weights() const { return m_weight; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_weights );
    }
    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion < 1 || m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedParticles packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_weights, m_packingVersion );
    }

  private:
    /// Data packing version (not used as yet, but for any future schema evolution)
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    WeightsVector m_weights;

    /// Vector of all weights
    WeightVector m_weight;
  };

  /**
   *  Utility class to handle the packing and unpacking of the WeightsVectors
   *
   *  @author Christopher Rob Jones
   *  @date   2010-09-22
   */
  class WeightsVectorPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::WeightsVector       Data;
    typedef LHCb::PackedWeights       PackedData;
    typedef LHCb::WeightsVectors      DataVector;
    typedef LHCb::PackedWeightsVector PackedDataVector;
    static const std::string&         packedLocation() { return LHCb::PackedWeightsVectorLocation::Default; }
    static const std::string&         unpackedLocation() { return LHCb::WeightsVectorLocation::Default; }
    static const char*                propertyName() { return "WeightsVectors"; }

    using PackerBase::PackerBase;

    /// Pack Data
    template <typename Range>
    void pack( Range&& weightsV, PackedDataVector& pweightsV ) const {
      const auto pVer = pweightsV.packingVersion();
      if ( !isSupportedVer( pVer ) ) return;
      pweightsV.data().reserve( weightsV.size() );
      for ( const Data* weights : weightsV ) {
        // new packed data
        auto& pweights = pweightsV.data().emplace_back();

        // Save the PV key
        pweights.pvKey = weights->key();

        // fill packed data
        pweights.firstWeight = pweightsV.weights().size();
        pweightsV.weights().reserve( pweightsV.weights().size() + weights->weights().size() );
        for ( const auto& [k, v] : weights->weights() ) {
          pweightsV.weights().emplace_back( k, StandardPacker::fraction( v ) );
        }
        pweights.lastWeight = pweightsV.weights().size();
      }
    }

    /// Unpack Data
    void unpack( const PackedDataVector& pweightsV, DataVector& weightsV ) const;

    /// Compare two WeightsVectors to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "WeightsVectorPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
