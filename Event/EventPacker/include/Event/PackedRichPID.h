/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackerBase.h"
#include "Event/RichPID.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <string>

namespace LHCb {

  /**
   *  Packed RichPID
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct PackedRichPID {
    std::int32_t pidResultCode{0};
    std::int32_t dllEl{0}, dllMu{0}, dllPi{0}, dllKa{0}, dllPr{0};
    std::int64_t track{-1};
    std::int32_t dllBt{0};
    std::int64_t key{0};
    std::int32_t dllDe{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this );

      // - Example 1, adding fields
      // if (version >= 5) buf.io(dllIon)

      // - Example 2, expanding (changing) field type
      // int -> std::int64_t pidResultCode
      // if (version >= 5) {
      //   buf.io(pidResultCode);
      // } else {
      //   int tmp;
      //   buf.io(tmp);
      //   pidResultCode = tmp;
      // }
    }
#endif
  };

  constexpr CLID CLID_PackedRichPIDs = 1561;

  /// Namespace for locations in TDS
  namespace PackedRichPIDLocation {
    inline const std::string Default  = "pRec/Rich/PIDs";
    inline const std::string InStream = "/pRec/Rich/CustomPIDs";
  } // namespace PackedRichPIDLocation

  /**
   *  Packed RichPIDs
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class PackedRichPIDs : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedRichPID> Vector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 4; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedRichPIDs; }

    /// Class ID
    const CLID& clID() const override { return PackedRichPIDs::classID(); }

    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion < 4 || m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedRichPIDs packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_vect, m_packingVersion );
    }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;
  };

  /**
   *  Utility class to handle the packing and unpacking of the RichPIDs
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class RichPIDPacker : public PackerBase {

  public:
    // These are required by the templated algorithms
    typedef LHCb::RichPID        Data;
    typedef LHCb::PackedRichPID  PackedData;
    typedef LHCb::RichPIDs       DataVector;
    typedef LHCb::PackedRichPIDs PackedDataVector;
    static const std::string&    packedLocation() { return LHCb::PackedRichPIDLocation::Default; }
    static const std::string&    unpackedLocation() { return LHCb::RichPIDLocation::Default; }
    static const char*           propertyName() { return "RichPIDs"; }

    using PackerBase::PackerBase;

    /// Pack RichPIDs
    template <typename RichPIDsRange>
    void pack( const RichPIDsRange& pids, PackedDataVector& ppids ) const {
      const auto ver = ppids.packingVersion();
      if ( !isSupportedVer( ver ) ) return;
      ppids.data().reserve( pids.size() );
      for ( const auto* pid : pids ) pack( *pid, ppids.data().emplace_back(), ppids );
    }

    /// Unpack a single RichPID
    void unpack( const PackedData& ppid, Data& pid, const PackedDataVector& ppids, DataVector& pids ) const;

    /// Unpack RichPIDs
    void unpack( const PackedDataVector& ppids, DataVector& pids ) const;

    /// Compare two MuonPIDs to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    /// Pack a RichPID
    void pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 0 <= ver && ver <= 4 );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "RichPIDPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

  // -----------------------------------------------------------------------

} // namespace LHCb
