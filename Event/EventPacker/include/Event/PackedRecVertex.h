/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackerBase.h"
#include "Event/PackerUtils.h"
#include "Event/RecVertex.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"

#include "fmt/format.h"

#include <string>
#include <vector>

namespace LHCb {

  /**
   *  Structure to describe a reconstructed vertex
   *
   *  @author Olivier Callot
   *  @date   2008-11-14
   */
  struct PackedRecVertex {
    std::int32_t  key{0};
    std::int32_t  technique{0};
    std::int32_t  chi2{0};
    std::int32_t  nDoF{0};
    std::int32_t  x{0};
    std::int32_t  y{0};
    std::int32_t  z{0};
    std::int32_t  cov00{0};
    std::int32_t  cov11{0};
    std::int32_t  cov22{0};
    std::int16_t  cov10{0};
    std::int16_t  cov20{0};
    std::int16_t  cov21{0};
    std::uint16_t firstTrack{0}, lastTrack{0};
    std::uint16_t firstInfo{0}, lastInfo{0};
    std::int32_t  container{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int version ) {
      if ( version == 1 ) {
        buf.io( key, technique, chi2, nDoF, x, y, z, cov00, cov11, cov22, cov10, cov20, cov21, firstTrack, lastTrack,
                firstInfo, lastInfo );
        // in packing version 1, container was not serialized!
        container = 0; // initialize "container" with a well defined value
                       // as it is not serialized
      } else {
        Packer::io( buf, *this ); // identical operation for the latest version
      }
    }
#endif
  };

  constexpr CLID CLID_PackedRecVertices = 1553;

  // Namespace for locations in TDS
  namespace PackedRecVertexLocation {
    inline const std::string Primary  = "pRec/Vertex/Primary";
    inline const std::string InStream = "/pPhys/RecVertices";
  } // namespace PackedRecVertexLocation

  /**
   *  Container of packed RecVertex objects
   *
   *  @author Olivier Callot
   *  @date   2008-11-14
   */
  class PackedRecVertices : public DataObject {

  public:
    /// Default Packing Version
    static char defaultPackingVersion() { return 2; }

    /// Vector of packed objects
    typedef std::vector<LHCb::PackedRecVertex> Vector;

    /// Standard constructor
    PackedRecVertices() {
      m_vect.reserve( 5 );
      m_refs.reserve( 100 );
      m_weights.reserve( 100 );
      m_extra.reserve( 250 );
    }

    const CLID&        clID() const override { return PackedRecVertices::classID(); }
    static const CLID& classID() { return CLID_PackedRecVertices; }

    std::vector<PackedRecVertex>&       data() { return m_vect; }
    const std::vector<PackedRecVertex>& data() const { return m_vect; }

    std::vector<std::int64_t>&       refs() { return m_refs; }
    const std::vector<std::int64_t>& refs() const { return m_refs; }

    void addExtra( const int a, const int b ) { m_extra.emplace_back( std::make_pair( a, b ) ); }
    std::vector<std::pair<int, int>>&       extras() { return m_extra; }
    const std::vector<std::pair<int, int>>& extras() const { return m_extra; }

    std::vector<std::int16_t>&       weights() { return m_weights; }
    const std::vector<std::int16_t>& weights() const { return m_weights; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
      buf.save( m_refs );
      buf.save( m_extra );
      buf.save( m_weights );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion < 1 || m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "RecVertices packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_vect, m_packingVersion );
      buf.load( m_refs );
      buf.load( m_extra, m_packingVersion );
      buf.load( m_weights );
    }

  private:
    std::vector<PackedRecVertex>     m_vect;
    std::vector<std::int64_t>        m_refs;
    std::vector<std::pair<int, int>> m_extra;
    std::vector<std::int16_t>        m_weights;

    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};
  };

  /**
   *  Utility class to handle the packing and unpacking of the RecVertices
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class RecVertexPacker : public PackerBase {

  public:
    // These are required by the templated algorithms
    typedef LHCb::RecVertex         Data;
    typedef LHCb::PackedRecVertex   PackedData;
    typedef LHCb::RecVertices       DataVector;
    typedef LHCb::PackedRecVertices PackedDataVector;
    static const std::string&       packedLocation() { return LHCb::PackedRecVertexLocation::Primary; }
    static const std::string&       unpackedLocation() { return LHCb::RecVertexLocation::Primary; }
    static const char*              propertyName() { return "PVs"; }

    using PackerBase::PackerBase;

    /// Pack Vertices
    template <typename Range>
    void pack( const Range& verts, PackedDataVector& pverts ) const {
      pverts.data().reserve( verts.size() );
      for ( const auto* vert : verts ) pack( *vert, pverts.data().emplace_back(), pverts );
    }

    /// Unpack a Vertex
    void unpack( const PackedData& pvert, Data& vert, const PackedDataVector& pverts, DataVector& verts ) const;

    /// Unpack Vertices
    void unpack( const PackedDataVector& pverts, DataVector& verts ) const;

    auto& packTrackRefs( bool b ) {
      m_packTrackRefs = b;
      return *this;
    }
    /// Compare two ProtoParticles to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    bool m_packTrackRefs = true;

    /// Pack a Vertex
    void pack( const Data& vert, PackedData& pvert, PackedDataVector& pverts ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 2 == ver || 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "RecVertexPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
