/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/MCCaloHit.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <string>

namespace LHCb {

  /**
   *  Packed MCCaloHit
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct PackedMCCaloHit {
    std::int32_t activeE{0};
    std::int32_t sensDetID{0};
    std::int8_t  time{0};
    std::int64_t mcParticle{-1};
  };

  constexpr CLID CLID_PackedMCCaloHits = 1526;

  /// Namespace for locations in TDS
  namespace PackedMCCaloHitLocation {
    inline const std::string Spd  = "pSim/Spd/Hits";
    inline const std::string Prs  = "pSim/Prs/Hits";
    inline const std::string Ecal = "pSim/Ecal/Hits";
    inline const std::string Hcal = "pSim/Hcal/Hits";
  } // namespace PackedMCCaloHitLocation

  /**
   *  Packed MCCaloHits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class PackedMCCaloHits : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedMCCaloHit> Vector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 1; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedMCCaloHits; }

    /// Class ID
    const CLID& clID() const override { return PackedMCCaloHits::classID(); }

    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;
  };

  /**
   *  Utility class to handle the packing and unpacking of the MCCaloHits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class MCCaloHitPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::MCCaloHit        Data;
    typedef LHCb::PackedMCCaloHit  PackedData;
    typedef LHCb::MCCaloHits       DataVector;
    typedef LHCb::PackedMCCaloHits PackedDataVector;

    using PackerBase::PackerBase;

    /// Pack MCCaloHits
    void pack( const DataVector& hits, PackedDataVector& phits ) const;

    /// Unpack MCCaloHits
    void unpack( const PackedDataVector& phits, DataVector& hits ) const;

    /// Compare two MCCaloHits to check the packing -> unpacking performance
    StatusCode check( const DataVector& dataA, const DataVector& dataB ) const;

  private:
    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "MCCaloHitPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }

  protected:
    /// Scale factor for energy
    double m_energyScale{1.0e2};
  };

  // -----------------------------------------------------------------------

  /** @class MCSpdHitPacker Event/PackedMCHit.h
   *
   *  Utility class to handle the packing and unpacking of the MC CALO Spd Hits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct MCSpdHitPacker : MCCaloHitPacker {
    using MCCaloHitPacker::MCCaloHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCCaloHitLocation::Spd; }
    static const std::string unpackedLocation() { return LHCb::MCCaloHitLocation::Spd; }
  };

  /** @class MCPrsHitPacker Event/PackedMCHit.h
   *
   *  Utility class to handle the packing and unpacking of the MC CALO Prs Hits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct MCPrsHitPacker : MCCaloHitPacker {
    using MCCaloHitPacker::MCCaloHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCCaloHitLocation::Prs; }
    static const std::string unpackedLocation() { return LHCb::MCCaloHitLocation::Prs; }
  };

  /** @class MCEcalHitPacker Event/PackedMCHit.h
   *
   *  Utility class to handle the packing and unpacking of the MC CALO Ecal Hits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct MCEcalHitPacker : MCCaloHitPacker {
    using MCCaloHitPacker::MCCaloHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCCaloHitLocation::Ecal; }
    static const std::string unpackedLocation() { return LHCb::MCCaloHitLocation::Ecal; }
  };

  /** @class MCHcalHitPacker Event/PackedMCHit.h
   *
   *  Utility class to handle the packing and unpacking of the MC CALO Hcal Hits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct MCHcalHitPacker : MCCaloHitPacker {
    using MCCaloHitPacker::MCCaloHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCCaloHitLocation::Hcal; }
    static const std::string unpackedLocation() { return LHCb::MCCaloHitLocation::Hcal; }
  };

} // namespace LHCb
