/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/StatusCode.h"
#include <iomanip>
#include <string>

namespace LHCb::Hlt::PackedData {

  /// Compression algorithms -- note: these values are written to the RawBank, so have to remain stable
  /// hence we do _not_ use native ROOT values, which could change between ROOT versions...
  enum struct Compression { NoCompression = 0, ZLIB = 1, LZMA = 2, LZ4 = 3, ZSTD = 4 };

  inline std::string toString( Compression c ) {
    switch ( c ) {
    default:
      throw std::runtime_error( "unknown compression value" );
    case Compression::NoCompression:
      return "NoCompression";
    case Compression::ZLIB:
      return "ZLIB";
    case Compression::LZMA:
      return "LZMA";
    case Compression::LZ4:
      return "LZ4";
    case Compression::ZSTD:
      return "ZSTD";
    }
  }
  inline std::ostream& toStream( Compression e, std::ostream& os ) { return os << std::quoted( toString( e ), '\'' ); }
  inline std::ostream& operator<<( std::ostream& s, Compression e ) { return toStream( e, s ); }

  inline StatusCode parse( Compression& c, std::string in ) {
    auto sv = std::string_view( in );
    if ( sv.size() > 1 && ( sv.front() == '\'' || sv.front() == '\"' ) && sv.front() == sv.back() ) {
      sv.remove_prefix( 1 );
      sv.remove_suffix( 1 );
    };
    constexpr auto algs = std::array{Compression::NoCompression, Compression::ZLIB, Compression::LZMA, Compression::LZ4,
                                     Compression::ZSTD};
    auto           i    = std::find_if( algs.begin(), algs.end(), [sv]( auto a ) { return sv == toString( a ); } );
    if ( i == algs.end() ) return StatusCode::FAILURE;
    c = *i;
    return StatusCode::SUCCESS;
  }

} // namespace LHCb::Hlt::PackedData
