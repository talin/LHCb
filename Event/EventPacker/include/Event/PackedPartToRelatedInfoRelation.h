/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/PackerBase.h"
#include "Event/Particle.h"
#include "Event/RelatedInfoMap.h"
#include "Event/StandardPacker.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/LinkManager.h"
#include "GaudiKernel/StatusCode.h"
#include "Relations/Relation1D.h"
#include "fmt/format.h"
#include <string>
#include <vector>

namespace LHCb {

  /**
   *  Utility class to implement the packing of Particle RelatedInfo relations
   *
   *  @author Christopher Rob Jones
   *  @date   2014-08-01
   */
  struct PackedRelatedInfoMap {
    // first and last entry in the info vector
    std::uint32_t first{0}, last{0};
    // reference
    std::int64_t reference{-1};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }
    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  constexpr CLID CLID_PackedRelatedInfoRelations = 1584; // Tell Marco !!

  /// Namespace for locations in TDS
  namespace PackedRelatedInfoLocation {
    inline const std::string User     = "pPhys/User/PartToRelatedInfoRelations";
    inline const std::string InStream = "/pPhys/PartToRelatedInfoRelations";
  } // namespace PackedRelatedInfoLocation

  /**
   *  Packed Particle to Related info relations
   *
   *  @author Christopher Rob Jones
   *  @date   2014-08-01
   */
  class PackedRelatedInfoRelations : public DataObject {

  public:
    /// Related info pair (key,data)
    typedef std::pair<short, float> PackedRelatedInfo;

    /// Vector of info objects
    typedef std::vector<PackedRelatedInfo> InfoVector;

    /// Vector of Info objects
    typedef std::vector<LHCb::PackedRelatedInfoMap> RelationVector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 0; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedRelatedInfoRelations; }
    const CLID&        clID() const override { return PackedRelatedInfoRelations::classID(); }

    /// Write access to the info vector
    InfoVector& info() { return m_info; }

    /// Read access to the data vector
    const InfoVector& info() const { return m_info; }

    /// Write access to the relation vector
    RelationVector& relations() { return m_relations; }

    /// Read access to the relation vector
    const RelationVector& relations() const { return m_relations; }

    /// Write access to the containers vector
    RelationVector& containers() { return m_containers; }

    /// Read access to the containers vector
    const RelationVector& containers() const { return m_containers; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

  public: // For templated algorithms
    typedef RelationVector Vector;

    /// Write access to the data vector
    Vector& data() { return relations(); }

    /// Read access to the data vector
    const Vector& data() const { return relations(); }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_info );
      buf.save( m_relations );
      buf.save( m_containers );
    }
    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedPartToRelatedInfoRelations packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_info, m_packingVersion );
      buf.load( m_relations, m_packingVersion );
      buf.load( m_containers, m_packingVersion );
    }

  private:
    /// Data packing version (not used as yet, but for any future schema evolution)
    char m_packingVersion{defaultPackingVersion()};

    /// The packed info vector
    InfoVector m_info;

    /// The packed relation vector
    RelationVector m_relations;

    /// The packed containers vector
    RelationVector m_containers;
  };

  /**
   *  Utility class to handle the packing and unpacking of the Particles
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class RelatedInfoRelationsPacker : PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::Particle                   FROM;
    typedef LHCb::RelatedInfoMap             TO;
    typedef LHCb::Relation1D<FROM, TO>       DataVector;
    typedef LHCb::PackedRelatedInfoRelations PackedDataVector;
    static const std::string&                packedLocation() { return LHCb::PackedRelatedInfoLocation::User; }
    static const std::string unpackedLocation() { return "/Event/Phys/User/PartToRelatedInfoRelations"; }

    using PackerBase::PackerBase;

    /// Pack the relations
    void pack( const DataVector& rels, PackedDataVector& prels ) const;

    /// Unpack the relations
    void unpack( const PackedDataVector& prels, DataVector& rels, const std::string& location = "" ) const;

    /// Unpack the relations
    void unpack( const LHCb::PackedRelatedInfoMap& pmap, const PackedDataVector& prels, DataVector& rels ) const;

    /// Compare two sets of relations to check the packing -> unpacking performance
    StatusCode check( const DataVector& dataA, const DataVector& dataB ) const;

  private:
    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "RelatedInfoRelationsPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
