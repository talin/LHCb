/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/CaloHypo.h"
#include "Event/PackerBase.h"
#include "Event/PackerUtils.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"

#include <string>
#include <vector>

namespace LHCb {

  /**
   *  Packed description of a CaloHypo
   *
   *  @author Olivier Callot
   *  @date   2008-11-10
   */
  struct PackedCaloHypo {
    std::int32_t key{0};
    std::int32_t hypothesis{0};
    std::int32_t lh{0};
    // from CaloPosition
    std::int32_t z{0};
    // position (3) + 3x3 symmetric covariance matrix
    std::int32_t posX{0}, posY{0}, posE{0};

    std::int32_t cov00{0}, cov11{0}, cov22{0};
    std::int16_t cov10{0}, cov20{0}, cov21{0};
    std::int16_t cerr10{0}; // non diagonal terms of the x,y spread matrix.
    // center in x,y + 2x2 symetric covariance matrix
    std::int32_t centX{0};
    std::int32_t centY{0};
    std::int32_t cerr00{0};
    std::int32_t cerr11{0};

    std::uint16_t firstDigit{0};
    std::uint16_t lastDigit{0};
    std::uint16_t firstCluster{0};
    std::uint16_t lastCluster{0};
    std::uint16_t firstHypo{0};
    std::uint16_t lastHypo{0};

#ifndef __CLING__
    template <typename Buf>
    void save( Buf& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename Buf>
    void load( Buf& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  constexpr CLID CLID_PackedCaloHypos = 1551;

  // Namespace for locations in TDS
  namespace PackedCaloHypoLocation {
    inline const std::string Photons      = "pRec/Calo/Photons";
    inline const std::string Electrons    = "pRec/Calo/Electrons";
    inline const std::string MergedPi0s   = "pRec/Calo/MergedPi0s";
    inline const std::string SplitPhotons = "pRec/Calo/SplitPhotons";
  } // namespace PackedCaloHypoLocation

  /**
   *  Vector of packed CaloHypos
   *
   *  @author Olivier Callot
   *  @date   2008-11-10
   */

  class PackedCaloHypos : public DataObject {

  public:
    /// Default Packing Version
    static char defaultPackingVersion() { return 1; }

    /// Standard constructor
    PackedCaloHypos() {
      m_vect.reserve( 100 );
      m_refs.reserve( 1000 );
    }

    const CLID&        clID() const override { return PackedCaloHypos::classID(); }
    static const CLID& classID() { return CLID_PackedCaloHypos; }

    std::vector<PackedCaloHypo>&       data() { return m_vect; }
    const std::vector<PackedCaloHypo>& data() const { return m_vect; }

    std::vector<std::int64_t>&       refs() { return m_refs; }
    const std::vector<std::int64_t>& refs() const { return m_refs; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
      buf.save( m_refs );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion < 1 || m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedCaloHypos packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_vect, m_packingVersion );
      buf.load( m_refs );
    }

  private:
    std::vector<PackedCaloHypo> m_vect;
    std::vector<std::int64_t>   m_refs;

    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};
  };

  /**
   *  Utility class to handle the packing and unpacking of CaloHypos
   *
   *  @author Christopher Rob Jones
   *  @date   05/04/2012
   */
  class CaloHypoPacker : public PackerBase {

    bool m_packClusterRefs = true;
    bool m_packDigitRefs   = true;

  public:
    typedef LHCb::CaloHypo        Data;
    typedef LHCb::PackedCaloHypo  PackedData;
    typedef LHCb::CaloHypos       DataVector;
    typedef LHCb::PackedCaloHypos PackedDataVector;

    static const std::string& packedLocation() { return LHCb::PackedCaloHypoLocation::Electrons; }
    static const std::string& unpackedLocation() { return LHCb::CaloHypoLocation::Electrons; }
    static const char*        propertyName() { return "CaloHypos"; }

    using PackerBase::PackerBase;

    auto& packClusterRefs( bool b ) {
      m_packClusterRefs = b;
      return *this;
    }
    auto& packDigitRefs( bool b ) {
      m_packDigitRefs = b;
      return *this;
    }

    /// Pack CaloHypos
    template <typename CaloHyposRange>
    void pack( const CaloHyposRange& hypos, PackedDataVector& phypos ) const {
      // packing version
      phypos.setVersion( 1 );
      const auto ver = phypos.packingVersion();
      if ( !isSupportedVer( ver ) ) return;

      phypos.data().reserve( hypos.size() );
      for ( const auto* H : hypos ) pack( *H, phypos.data().emplace_back(), phypos );
    }

    /// Unpack CaloHypos
    void unpack( const PackedDataVector& phypos, DataVector& hypos ) const;

    /// Compare two hypos
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    /// Pack a CaloHypo
    void pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( "Unknown packed data version " + std::to_string( (int)ver ), "CaloHypoPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
