/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/MCRichTrack.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <string>
#include <vector>

namespace LHCb {

  /**
   *  Packed MCRichTrack
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct PackedMCRichTrack {
    int                       key{0};
    std::vector<std::int64_t> mcSegments;
    std::int64_t              mcParticle{-1};
  };

  constexpr CLID CLID_PackedMCRichTracks = 1524;

  /// Namespace for locations in TDS
  namespace PackedMCRichTrackLocation {
    inline const std::string Default = "pSim/Rich/Tracks";
  }

  /**
   *  Packed MCRichTracks
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class PackedMCRichTracks : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedMCRichTrack> Vector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 1; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedMCRichTracks; }

    /// Class ID
    const CLID& clID() const override { return PackedMCRichTracks::classID(); }

    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;
  };

  /**
   *  Utility class to handle the packing and unpacking of the MCRichTracks
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class MCRichTrackPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::MCRichTrack        Data;
    typedef LHCb::PackedMCRichTrack  PackedData;
    typedef LHCb::MCRichTracks       DataVector;
    typedef LHCb::PackedMCRichTracks PackedDataVector;
    static const std::string&        packedLocation() { return LHCb::PackedMCRichTrackLocation::Default; }
    static const std::string&        unpackedLocation() { return LHCb::MCRichTrackLocation::Default; }

    using PackerBase::PackerBase;

    /// Pack MCRichTracks
    void pack( const DataVector& tracks, PackedDataVector& ptracks ) const;

    /// Unpack MCRichTracks
    void unpack( const PackedDataVector& ptracks, DataVector& tracks ) const;

    /// Compare two MCRichHits to check the packing -> unpacking performance
    StatusCode check( const DataVector& dataA, const DataVector& dataB ) const;

  private:
    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "MCRichTrackPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
