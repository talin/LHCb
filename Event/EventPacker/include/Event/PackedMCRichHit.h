/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/MCRichHit.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <string>

namespace LHCb {

  /**
   *  Packed MCRichHit
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct PackedMCRichHit {
    std::int32_t x{0}, y{0}, z{0};
    std::int32_t energy{0};
    std::int32_t tof{0};
    std::int32_t sensDetID{0};
    std::int32_t history{0};
    std::int64_t mcParticle{-1};
  };

  constexpr CLID CLID_PackedMCRichHits = 1521;

  /// Namespace for locations in TDS
  namespace PackedMCRichHitLocation {
    inline const std::string Default = "pSim/Rich/Hits";
  }

  /**
   *  Packed MCRichHits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class PackedMCRichHits : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedMCRichHit> Vector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 1; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedMCRichHits; }

    /// Class ID
    const CLID& clID() const override { return PackedMCRichHits::classID(); }

    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;
  };

  /**
   *  Utility class to handle the packing and unpacking of the MCRichHits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class MCRichHitPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::MCRichHit        Data;
    typedef LHCb::PackedMCRichHit  PackedData;
    typedef LHCb::MCRichHits       DataVector;
    typedef LHCb::PackedMCRichHits PackedDataVector;
    static const std::string&      packedLocation() { return LHCb::PackedMCRichHitLocation::Default; }
    static const std::string&      unpackedLocation() { return LHCb::MCRichHitLocation::Default; }

    using PackerBase::PackerBase;

    /// Pack MCRichHits
    void pack( const DataVector& hits, PackedDataVector& phits ) const;

    /// Unpack MCRichHits
    void unpack( const PackedDataVector& phits, DataVector& hits ) const;

    /// Compare two MCRichHits to check the packing -> unpacking performance
    StatusCode check( const DataVector& dataA, const DataVector& dataB ) const;

  private:
    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "MCRichHitPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
