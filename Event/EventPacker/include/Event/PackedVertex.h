/***************************************************************************** \
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"
#include "Event/Vertex.h"

#include "Event/PackedEventChecks.h"
#include "Event/PackerUtils.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"
#include "fmt/format.h"

#include <string>

namespace LHCb {

  /**
   *  Packed Vertex
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct PackedVertex {

    /// Key and possibly container index.
    std::int64_t key{0};

    std::int32_t technique{0}; ///< packed technique
    std::int32_t chi2{0};      ///< packed chi^2
    std::int32_t nDoF{0};      ///< packed nDOF

    // Position
    std::int32_t x{0}, y{0}, z{0};

    // Covariance matrix
    std::int32_t cov00{0}, cov11{0}, cov22{0};
    std::int16_t cov10{0}, cov20{0}, cov21{0};

    /// first outgoing particle
    std::uint32_t firstOutgoingPart{0};
    /// last outgoing particle
    std::uint32_t lastOutgoingPart{0};

    /// first info
    std::uint32_t firstInfo{0};
    /// last info
    std::uint32_t lastInfo{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }
    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  constexpr CLID CLID_PackedVertices = 1582;

  /// Namespace for locations in TDS
  namespace PackedVertexLocation {
    inline const std::string User     = "pPhys/User/Vertices";
    inline const std::string InStream = "/pPhys/Vertices";
  } // namespace PackedVertexLocation

  /**
   *  Packed Vertices
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class PackedVertices : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedVertex> Vector;

    /// Outgoing Particles
    typedef std::vector<std::int64_t> OutgoingParticles;

    /// Packed Extra Info
    typedef std::pair<int, int> ExtraInfo;

    /// Packed Extra Info Vector
    typedef std::vector<ExtraInfo> ExtraInfoVector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 1; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedVertices; }

    /// Class ID
    const CLID& clID() const override { return PackedVertices::classID(); }

    /// Write access to the data vector
    [[nodiscard]] Vector& data() { return m_vect; }

    /// Read access to the data vector
    [[nodiscard]] const Vector& data() const { return m_vect; }

    /// Write access to the data vector
    [[nodiscard]] OutgoingParticles& outgoingParticles() { return m_parts; }

    /// Read access to the data vector
    [[nodiscard]] const OutgoingParticles& outgoingParticles() const { return m_parts; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// add an extra info
    PackedVertices& addExtra( const int a, const int b ) {
      m_extra.emplace_back( ExtraInfo( a, b ) );
      return *this;
    }

    /// Write access the extra info
    [[nodiscard]] ExtraInfoVector& extras() { return m_extra; }

    /// Read access the extra info
    [[nodiscard]] const ExtraInfoVector& extras() const { return m_extra; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
      buf.save( m_parts );
      buf.save( m_extra );
    }
    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion < 1 || m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedParticles packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_vect, m_packingVersion );
      buf.load( m_parts );
      buf.load( m_extra, m_packingVersion );
    }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;

    /// Outgoing Particles
    OutgoingParticles m_parts;

    /// Extra info
    ExtraInfoVector m_extra;
  };

  /**
   *  Utility class to handle the packing and unpacking of the Vertices
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class VertexPacker : public PackerBase {

  public:
    // These are required by the templated algorithms
    typedef LHCb::Vertex         Data;
    typedef LHCb::PackedVertex   PackedData;
    typedef LHCb::Vertices       DataVector;
    typedef LHCb::PackedVertices PackedDataVector;
    static const std::string&    packedLocation() { return LHCb::PackedVertexLocation::User; }
    static const std::string&    unpackedLocation() { return LHCb::VertexLocation::User; }
    static const char*           propertyName() { return "Vertices"; }

    using PackerBase::PackerBase;

    /// Pack Vertices
    template <typename VertexRange>
    void pack( const VertexRange& verts, PackedDataVector& pverts ) const {
      pverts.data().reserve( verts.size() );
      for ( const LHCb::Vertex* vert : verts ) pack( *vert, pverts.data().emplace_back(), pverts );
    }

    /// Unpack a Vertex
    void unpack( const PackedData& pvert, Data& vert, const PackedDataVector& pverts, DataVector& verts ) const;

    /// Unpack Vertices
    void unpack( const PackedDataVector& pverts, DataVector& verts ) const;

    /// Unpack Vertices
    DataVector unpack( const PackedDataVector& pverts ) const;

    /// Compare two Vertices to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    /// Pack a Vertex
    void pack( const Data& vert, PackedData& pvert, PackedDataVector& pverts ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 1 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "VertexPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
