/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/MCHit.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <string>

namespace LHCb {

  /**
   *  Packed MCHit
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct PackedMCHit {
    std::int32_t sensDetID{0};
    std::int32_t entx{0}, enty{0}, entz{0};
    std::int32_t vtxx{0}, vtxy{0}, vtxz{0};
    std::int32_t energy{0};
    std::int32_t tof{0};
    std::int32_t mp{0};
    std::int64_t mcParticle{-1};
  };

  constexpr CLID CLID_PackedMCHits = 1525;

  /// Namespace for locations in TDS
  namespace PackedMCHitLocation {
    inline const std::string Velo   = "pSim/Velo/Hits";
    inline const std::string PuVeto = "pSim/PuVeto/Hits";
    inline const std::string VP     = "pSim/VP/Hits";
    inline const std::string TT     = "pSim/TT/Hits";
    inline const std::string UT     = "pSim/UT/Hits";
    inline const std::string IT     = "pSim/IT/Hits";
    inline const std::string SL     = "pSim/SL/Hits";
    inline const std::string OT     = "pSim/OT/Hits";
    inline const std::string FT     = "pSim/FT/Hits";
    inline const std::string Muon   = "pSim/Muon/Hits";
    inline const std::string HC     = "pSim/HC/Hits";
    inline const std::string Bcm    = "pSim/Bcm/Hits";
    inline const std::string Bls    = "pSim/Bls/Hits";
    inline const std::string Plume  = "pSim/Plume/Hits";
  } // namespace PackedMCHitLocation

  /**
   *  Packed MCHits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class PackedMCHits : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedMCHit> Vector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 1; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedMCHits; }

    /// Class ID
    const CLID& clID() const override { return PackedMCHits::classID(); }

    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;
  };

  /**
   *  Utility class to handle the packing and unpacking of the MCHits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class MCHitPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::MCHit        Data;
    typedef LHCb::PackedMCHit  PackedData;
    typedef LHCb::MCHits       DataVector;
    typedef LHCb::PackedMCHits PackedDataVector;

    using PackerBase::PackerBase;

    /// Pack MCHits
    void pack( const DataVector& hits, PackedDataVector& phits ) const;

    /// Unpack MCHits
    void unpack( const PackedDataVector& phits, DataVector& hits ) const;

    /// Compare two MCHits to check the packing -> unpacking performance
    StatusCode check( const DataVector& dataA, const DataVector& dataB ) const;

  private:
    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "MCHitPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }

  protected:
    /// Scale factor for displacement vector
    double m_dispScale{1.0e2};

    /// Scale factor for energy
    double m_enScale{5.0e3};
  };

  /**
   *  Utility class to handle the packing and unpacking of the MC Velo Hits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct MCVeloHitPacker : MCHitPacker {
    using MCHitPacker::MCHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCHitLocation::Velo; }
    static const std::string unpackedLocation() { return LHCb::MCHitLocation::Velo; }
  };

  /**
   *  Utility class to handle the packing and unpacking of the MC PuVeto Hits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct MCPuVetoHitPacker : MCHitPacker {
    using MCHitPacker::MCHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCHitLocation::PuVeto; }
    static const std::string unpackedLocation() { return LHCb::MCHitLocation::PuVeto; }
  };

  /**
   *  Utility class to handle the packing and unpacking of the MC VP Hits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct MCVPHitPacker : MCHitPacker {
    using MCHitPacker::MCHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCHitLocation::VP; }
    static const std::string unpackedLocation() { return LHCb::MCHitLocation::VP; }
  };

  /**
   *  Utility class to handle the packing and unpacking of the MC Plume Hits
   *  FIXME: The plan is to not use MCHits in the long term but instead have dedicated
   *         MCPlumeHits. When this is implemented, we can remove/adapt this class.
   */
  class MCPlumeHitPacker : public MCHitPacker {
  public:
    MCPlumeHitPacker( Gaudi::Algorithm const* p ) : MCHitPacker( p ) { m_enScale = 1.0e8; }
    static const std::string packedLocation() { return LHCb::PackedMCHitLocation::Plume; }
    static const std::string unpackedLocation() { return LHCb::MCHitLocation::Plume; }
  };

  /**
   *
   *  Utility class to handle the packing and unpacking of the MC TT Hits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct MCTTHitPacker : MCHitPacker {
    using MCHitPacker::MCHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCHitLocation::TT; }
    static const std::string unpackedLocation() { return LHCb::MCHitLocation::TT; }
  };

  /**
   *
   *  Utility class to handle the packing and unpacking of the MC UT Hits
   *
   *  @author Jianchun Wang
   *  @date   2012-07-13
   */
  struct MCUTHitPacker : MCHitPacker {
    using MCHitPacker::MCHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCHitLocation::UT; }
    static const std::string unpackedLocation() { return LHCb::MCHitLocation::UT; }
  };

  /**
   *  Utility class to handle the packing and unpacking of the MC IT Hits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct MCITHitPacker : MCHitPacker {
    using MCHitPacker::MCHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCHitLocation::IT; }
    static const std::string unpackedLocation() { return LHCb::MCHitLocation::IT; }
  };

  /**
   *  Utility class to handle the packing and unpacking of the MC IT Hits
   *
   *  @author Paul Szczypka
   *  @date   2013-04-24
   */
  struct MCSLHitPacker : MCHitPacker {
    using MCHitPacker::MCHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCHitLocation::SL; }
    static const std::string unpackedLocation() { return LHCb::MCHitLocation::SL; }
  };

  /**
   *  Utility class to handle the packing and unpacking of the MC OT Hits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct MCOTHitPacker : MCHitPacker {
    using MCHitPacker::MCHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCHitLocation::OT; }
    static const std::string unpackedLocation() { return LHCb::MCHitLocation::OT; }
  };

  /**
   *  Utility class to handle the packing and unpacking of the MC FT Hits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct MCFTHitPacker : MCHitPacker {
    using MCHitPacker::MCHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCHitLocation::FT; }
    static const std::string unpackedLocation() { return LHCb::MCHitLocation::FT; }
  };

  /**
   *  Utility class to handle the packing and unpacking of the MC Muon Hits
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct MCMuonHitPacker : MCHitPacker {
    using MCHitPacker::MCHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCHitLocation::Muon; }
    static const std::string unpackedLocation() { return LHCb::MCHitLocation::Muon; }
  };

  /**
   *  Utility class to handle the packing and unpacking of the MC HC Hits
   *
   *  @author Heinrich Schindler
   *  @date   2014-05-08
   */
  struct MCHCHitPacker : MCHitPacker {
    using MCHitPacker::MCHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCHitLocation::HC; }
    static const std::string unpackedLocation() { return LHCb::MCHitLocation::HC; }
  };

  /**
   *  Utility class to handle the packing and unpacking of the MC Bcm Hits
   *
   *  @author Heinrich Schindler
   *  @date   2015-02-12
   */
  struct MCBcmHitPacker : MCHitPacker {
    using MCHitPacker::MCHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCHitLocation::Bcm; }
    static const std::string unpackedLocation() { return LHCb::MCHitLocation::Bcm; }
  };

  /**
   *  Utility class to handle the packing and unpacking of the MC Bls Hits
   *
   *  @author Heinrich Schindler
   *  @date   2015-02-12
   */
  struct MCBlsHitPacker : MCHitPacker {
    using MCHitPacker::MCHitPacker;
    static const std::string packedLocation() { return LHCb::PackedMCHitLocation::Bls; }
    static const std::string unpackedLocation() { return LHCb::MCHitLocation::Bls; }
  };

} // namespace LHCb
