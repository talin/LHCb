/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/MCRichSegment.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <string>
#include <vector>

namespace LHCb {

  /**
   *  Packed MCRichSegment
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct PackedMCRichSegment {
    std::int32_t              key{0};
    std::int32_t              history{0};
    std::vector<std::int32_t> trajPx, trajPy, trajPz;
    std::vector<std::int32_t> trajMx, trajMy, trajMz;
    std::int64_t              mcParticle{-1};
    std::int64_t              mcRichTrack{-1};
    std::vector<std::int64_t> mcPhotons, mcHits;
  };

  constexpr CLID CLID_PackedMCRichSegments = 1523;

  /// Namespace for locations in TDS
  namespace PackedMCRichSegmentLocation {
    inline const std::string Default = "pSim/Rich/Segments";
  }

  /**
   *  Packed MCRichSegments
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class PackedMCRichSegments : public DataObject {
  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedMCRichSegment> Vector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 1; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedMCRichSegments; }

    /// Class ID
    const CLID& clID() const override { return classID(); }

    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;
  };

  /**
   *  Utility class to handle the packing and unpacking of the MCRichSegments
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class MCRichSegmentPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::MCRichSegment        Data;
    typedef LHCb::PackedMCRichSegment  PackedData;
    typedef LHCb::MCRichSegments       DataVector;
    typedef LHCb::PackedMCRichSegments PackedDataVector;
    static const std::string&          packedLocation() { return LHCb::PackedMCRichSegmentLocation::Default; }
    static const std::string&          unpackedLocation() { return LHCb::MCRichSegmentLocation::Default; }

    using PackerBase::PackerBase;

    /// Pack an MCRichSegment
    void pack( const DataVector& segs, PackedDataVector& psegs ) const;

    /// Unpack an MCRichSegment
    void unpack( const PackedDataVector& psegs, DataVector& segs ) const;

    /// Compare two MCRichHits to check the packing -> unpacking performance
    StatusCode check( const DataVector& dataA, const DataVector& dataB ) const;

  private:
    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "MCRichSegmentPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
