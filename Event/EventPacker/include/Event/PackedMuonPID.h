/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/MuonPID.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

namespace LHCb {

  /**
   *  Packed MuonPID
   *
   *  Version = 3, adds new variables to the muonPID: chi2 of correlated hits + MVA methods
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct PackedMuonPID {
    std::int32_t MuonLLMu{0};
    std::int32_t MuonLLBg{0};
    std::int32_t nShared{0};
    std::int32_t status{0};
    std::int64_t idtrack{-1};
    std::int64_t mutrack{-1};
    std::int64_t key{-1};
    std::int32_t chi2Corr{0};
    std::int32_t muonMVA1{0};
    std::int32_t muonMVA2{0};
    std::int32_t muonMVA3{0};
    std::int32_t muonMVA4{0};

#ifndef __CLING__
    template <typename Buf>
    void save( Buf& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename Buf>
    void load( Buf& buf, unsigned int version ) {
      if ( version == 2 ) {
        buf.io( MuonLLMu, MuonLLBg, nShared, status, idtrack, mutrack, key );
      } else {
        Packer::io( buf, *this ); // identical operation for the latest version
      }
    }
#endif
  };

  constexpr CLID CLID_PackedMuonPIDs = 1571;

  /// Namespace for locations in TDS
  namespace PackedMuonPIDLocation {
    inline const std::string Default  = "pRec/Muon/MuonPID";
    inline const std::string InStream = "/pRec/Muon/CustomPIDs";
  } // namespace PackedMuonPIDLocation

  /**
   *  Packed MuonPIDs
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class PackedMuonPIDs : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedMuonPID> Vector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 3; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedMuonPIDs; }

    /// Class ID
    const CLID& clID() const override { return PackedMuonPIDs::classID(); }

    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );

      if ( m_packingVersion < 2 || m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedMuonPIDs packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_vect, m_packingVersion );
    }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;
  };

  /**
   *  Utility class to handle the packing and unpacking of the MuonPIDs
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class MuonPIDPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::MuonPID        Data;
    typedef LHCb::PackedMuonPID  PackedData;
    typedef LHCb::MuonPIDs       DataVector;
    typedef LHCb::PackedMuonPIDs PackedDataVector;
    static const std::string&    packedLocation() { return LHCb::PackedMuonPIDLocation::Default; }
    static const std::string&    unpackedLocation() { return LHCb::MuonPIDLocation::Default; }
    static const char*           propertyName() { return "MuonPIDs"; }

    using PackerBase::PackerBase;

    /// Pack MuonPIDs
    template <typename MuonPIDsRange>
    void pack( const MuonPIDsRange& pids, PackedDataVector& ppids ) const {
      const auto ver = ppids.packingVersion();
      if ( !isSupportedVer( ver ) ) return;
      ppids.data().reserve( pids.size() );
      for ( const auto* pid : pids ) pack( *pid, ppids.data().emplace_back(), ppids );
    }

    /// Unpack a single MuonPID
    void unpack( const PackedData& ppid, Data& pid, const PackedDataVector& ppids, DataVector& pids ) const;

    /// Unpack MuonPIDs
    void unpack( const PackedDataVector& ppids, DataVector& pids ) const;

    /// Compare two MuonPIDs to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    /// Pack a MuonPID
    void pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 3 == ver || 2 == ver || 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "MuonPIDPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
