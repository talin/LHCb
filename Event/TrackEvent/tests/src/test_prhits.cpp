/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestPrHits

#include "Event/PrHits.h"
#include "LHCbMath/SIMDWrapper.h"

#include <boost/test/unit_test.hpp>
#include <type_traits>

namespace {

  using simd   = SIMDWrapper::best::types;
  using scalar = SIMDWrapper::scalar::types;
  using namespace LHCb::Pr;

  template <typename Tag, typename Value, typename Hits>
  void fill( Value val, Hits& hits ) {
    auto maskV  = simd::mask_true();
    auto myhitV = hits.template compress_back<SIMDWrapper::InstructionSet::Best>( maskV );
    myhitV.template field<Tag>().set( val );

    auto maskS  = scalar::mask_true();
    auto myhitS = hits.template compress_back<SIMDWrapper::InstructionSet::Scalar>( maskS );
    myhitS.template field<Tag>().set( val );

    maskS  = scalar::mask_false();
    myhitS = hits.template compress_back<SIMDWrapper::InstructionSet::Scalar>( maskS );
    myhitS.template field<Tag>().set( val );
  }

} // namespace

// The test fixture: Create a container of hits
// This can be used as a common starting point for all tests

BOOST_AUTO_TEST_CASE( test_hits_size ) {

  Hits<HitType::VP> VPHits{};
  Hits<HitType::UT> UTHits{};
  Hits<HitType::FT> FTHits{};

  fill<VP::VPHitsTag::ChannelId>( 2, VPHits );
  fill<UT::UTHitsTag::xAtYEq0>( 1.234f, UTHits );
  FTHits.appendColumn( {}, {}, {}, {} );

  BOOST_CHECK( ( VPHits.size() == simd::size + scalar::size ) );
  BOOST_CHECK( ( UTHits.size() == simd::size + scalar::size ) );
  BOOST_CHECK( ( FTHits.size() == 1 ) );
}

BOOST_AUTO_TEST_CASE( test_hits_constructors ) {
  static_assert( std::is_default_constructible_v<Hits<HitType::VP>> );
  static_assert( std::is_move_constructible_v<Hits<HitType::VP>> );
  static_assert( !std::is_copy_constructible_v<Hits<HitType::VP>> );

  static_assert( std::is_default_constructible_v<Hits<HitType::UT>> );
  static_assert( std::is_move_constructible_v<Hits<HitType::UT>> );
  static_assert( !std::is_copy_constructible_v<Hits<HitType::UT>> );

  static_assert( std::is_default_constructible_v<Hits<HitType::FT>> );
  static_assert( std::is_move_constructible_v<Hits<HitType::FT>> );
  static_assert( !std::is_copy_constructible_v<Hits<HitType::FT>> );
}
