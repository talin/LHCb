/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/ITrackFitResult.h"
#include "Event/State.h"
#include "Event/TrackParameters.h"
#include "Event/TrackTags.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/Range.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "GaudiKernel/SmartRefVector.h"
#include "GaudiKernel/VectorMap.h"
#include "Kernel/LHCbID.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/Traits.h"
#include "LHCbMath/MatVec.h"
#include "TrackEnums.h"
#include <algorithm>
#include <ostream>
#include <vector>

// Forward declarations

/** @namespace v1
 *
 * Track is the base class for offline and online tracks.
 *
 * @author Jose Hernando, Eduardo Rodrigues
 *
 */

namespace LHCb::Event {
  // Forward declarations
  inline namespace v1 {
    // Forward declarations2

    // Class ID definition
    static const CLID CLID_Track = 10010;

    // Namespace for locations in TDS
    namespace TrackLocation {
      inline const std::string Default             = "Rec/Track/Best";
      inline const std::string Ideal               = "MC/Track/Ideal";
      inline const std::string Seed                = "Rec/Track/Seed";
      inline const std::string RZVelo              = "Rec/Track/RZVelo";
      inline const std::string Velo                = "Rec/Track/Velo";
      inline const std::string Forward             = "Rec/Track/Forward";
      inline const std::string VeloTT              = "Rec/Track/VeloTT";
      inline const std::string TT                  = "Rec/Track/TT";
      inline const std::string Match               = "Rec/Track/Match";
      inline const std::string Downstream          = "Rec/Track/Downstream";
      inline const std::string Tsa                 = "Rec/Track/Tsa";
      inline const std::string Muon                = "Rec/Track/Muon";
      inline const std::string VP                  = "Rec/Track/VP";
      inline const std::string VPTT                = "Rec/Track/VPTT";
      inline const std::string VeloUT              = "Rec/Track/VeloUT";
      inline const std::string UT                  = "Rec/Track/UT";
      inline const std::string CaloCosmicsForward  = "Calo/Track/Forward";
      inline const std::string CaloCosmicsBackward = "Calo/Track/Backward";
      inline const std::string HltImport           = "Red/Track/HLTImport";
    } // namespace TrackLocation

    /** @class Track Track.h
     *
     * Track is the base class for offline and online tracks.
     *
     * @author Jose Hernando, Eduardo Rodrigues
     *
     */

    class Track final : public KeyedObject<int> {
    public:
      /// typedef for std::vector of Track
      using Vector      = std::vector<Track*>;
      using ConstVector = std::vector<const Track*>;

      /// typedef for KeyedContainer of Track
      typedef KeyedContainer<Track, Containers::HashMap> Container;

      /// For defining SharedObjectContainer
      using Selection = SharedObjectsContainer<Track>;
      /// For accessing a list of tracks which is either a SharedObjectContainer, a KeyedContainer or a ConstVector
      using Range = Gaudi::Range_<ConstVector>;
      /// Container for LHCbIDs on track
      using LHCbIDContainer = std::vector<LHCbID>;
      /// Container for States on track
      using StateContainer = std::vector<State*>;
      /// Vector of additional information
      typedef GaudiUtils::VectorMap<int, double> ExtraInfo;

      /// Track history enumerations: each entry corresponds to the name of the pattern recognition algorithm that
      using History = Enum::Track::History;

      /// Track fit history enumerations
      using FitHistory = Enum::Track::FitHistory;

      /// Track type enumerations
      using Types = Enum::Track::Type;

      /// Track pattern recognition status flag enumerations: The flag specifies in which state of the pattern
      /// recognition phase the track is. The status flag is set by the relevant algorithms
      using PatRecStatus = Enum::Track::PatRecStatus;
      /// Track fitting status flag enumerations: The flag specifies in which state of the fitting phase the track is.
      /// The status flag is set by the relevant algorithms
      using FitStatus = Enum::Track::FitStatus;
      /// Track general flags enumerations
      using Flags = Enum::Track::Flag;
      /// Additional information assigned to this Track by pattern recognition
      using AdditionalInfo = Enum::Track::AdditionalInfo;

      /// Constructor with assigned key
      explicit Track( int key ) : KeyedObject<int>( key ) {}

      /// Move constructor
      Track( Track&& track )
          : KeyedObject<int>()
          , m_chi2PerDoF( track.m_chi2PerDoF )
          , m_nDoF( track.m_nDoF )
          , m_likelihood( track.m_likelihood )
          , m_ghostProbability( track.m_ghostProbability )
          , m_flags( track.m_flags )
          , m_lhcbIDs( std::move( track.m_lhcbIDs ) )
          , m_states( std::move( track.m_states ) )
          , m_fitResult( std::exchange( track.m_fitResult, nullptr ) )
          , m_extraInfo{std::move( track.m_extraInfo )}
          , m_ancestors( std::move( track.m_ancestors ) ) {}

      /// Move constructor
      Track( Track&& track, const int key )
          : KeyedObject<int>( key )
          , m_chi2PerDoF( track.m_chi2PerDoF )
          , m_nDoF( track.m_nDoF )
          , m_likelihood( track.m_likelihood )
          , m_ghostProbability( track.m_ghostProbability )
          , m_flags( track.m_flags )
          , m_lhcbIDs( std::move( track.m_lhcbIDs ) )
          , m_states( std::move( track.m_states ) )
          , m_fitResult( std::exchange( track.m_fitResult, nullptr ) )
          , m_extraInfo{std::move( track.m_extraInfo )}
          , m_ancestors( std::move( track.m_ancestors ) ) {}

      /// Constructor
      explicit Track( const History& history, const Types& trackType, const PatRecStatus& patStatus,
                      const std::vector<LHCbID>& ids, const State& aState );

      /// Constructor
      explicit Track( const History& history, const Types& trackType, const PatRecStatus& patStatus );

      /// Copy constructor
      Track( const Track& track );

      /// Copy constructor
      Track( const Track& track, const int key );

      /// Default Constructor
      Track() = default;

      /// Track destructor
      ~Track() override;

      // Retrieve pointer to class definition structure
      const CLID&        clID() const override;
      static const CLID& classID();

      /// Move assignement operator
      Track& operator=( Track&& track );

      /// Retrieve the position and momentum vectors and the corresponding 6D covariance matrix (pos:0->2,mom:3-5) at
      /// the first state
      void positionAndMomentum( Gaudi::XYZPoint& pos, Gaudi::XYZVector& mom, Gaudi::SymMatrix6x6& cov6D ) const;

      /// Retrieve the position and momentum vectors at the first state
      void positionAndMomentum( Gaudi::XYZPoint& pos, Gaudi::XYZVector& mom ) const;

      /// Retrieve the 3D-position vector at the first state
      Gaudi::XYZPoint position() const;

      /// Retrieve the 3D-position (+ errors) at the first state
      void position( Gaudi::XYZPoint& pos, Gaudi::SymMatrix3x3& errPos ) const;

      /// Retrieve the slopes (Tx=dx/dz,Ty=dy/dz,1.) and errors at the first state
      Gaudi::XYZVector slopes() const;

      /// Retrieve the slopes (Tx=dx/dz,Ty=dy/dz,1.) at the first state
      void slopes( Gaudi::XYZVector& slopes, Gaudi::SymMatrix3x3& errSlopes ) const;

      /// Retrieve the momentum vector at the first state
      Gaudi::XYZVector momentum() const;

      /// Retrieve the momentum at the first state
      double p() const;

      /// Retrieve the transverse momentum at the first state
      double pt() const;

      /// Retrieve the pseudorapidity at the first state
      double pseudoRapidity() const;

      /// Retrieve the phi at the first state
      double phi() const;

      /// Retrieve the momentum vector (and errors) at the first state
      void momentum( Gaudi::XYZVector& mom, Gaudi::SymMatrix3x3& errMom ) const;

      /// Retrieve the 6D (x,y,z,px,py,pz) covariance matrix at the first state
      void posMomCovariance( Gaudi::SymMatrix6x6& cov6D ) const;

      /// Retrieve the first state on the track
      State& firstState();

      /// Retrieve the first state on the track
      const State& firstState() const;

      /// Retrieve the charge assigned to the track
      int charge() const;

      /// Retrieve the Chi^2 of the track (fit)
      double chi2() const;

      /// Set the Chi^2 and the DoF of the track (fit)
      void setChi2AndDoF( double chi2, int ndof );

      /// Probability of chi2^2 of the track
      double probChi2() const;

      /// Retrieve the number of states on the track
      unsigned int nStates() const;

      /// Add a State to the list of States associated to the track
      void addToStates( const State& state );

      /// Add a set of states to the track. Track takes ownership. (note: const refers to the pointer, not the State!)
      void addToStates( span<State* const> states, Tag::State::AssumeUnordered_tag = {} );

      /// Add a set of sorted states by increasing Z to the track. Track takes ownership. (note: const refers to the
      /// pointer, not the State!)
      void addToStates( span<State* const> states, Tag::State::AssumeSorted_tag );

      /// Remove a State from the list of states associated to the track
      void removeFromStates( State* value );

      /// Clear the State vector
      void clearStates();

      /// Retrieve the reference to the state closest to the given z-position
      State& closestState( double z );

      /// Retrieve the reference to the state closest to the given z-position
      const State& closestState( double z ) const;

      /// Retrieve the reference to the state closest to the given plane
      const State& closestState( const Gaudi::Plane3D& plane ) const;

      /// Check the existence of a state at a certain predefined location (see the Location enum in State.h)
      bool hasStateAt( const State::Location& location ) const;

      /// Retrieve the state at a certain predefined location (see the Location enum in State.h)
      State* stateAt( const State::Location& location );

      /// Retrieve the const state at a certain predefined location (see the Location enum in State.h)
      const State* stateAt( const State::Location& location ) const;

      auto const& closestToBeamState() const {
        auto state = stateAt( LHCb::State::ClosestToBeam );
        if ( !state ) { throw GaudiException{"No ClosestToBeam state", "Track_v1.h", StatusCode::FAILURE}; }
        return *state;
      }

      /// Retrieve the number of LHCbIDs on the track
      unsigned int nLHCbIDs() const;

      /// Add an LHCbID to the list of LHCbIDs associated to the track. Return true if LHCbID was not yet on track.
      bool addToLhcbIDs( const LHCbID& value );

      /// Add a sorted (!) sequence of LHCbID to the list of LHCbIDs associated to the track. Returns true if none of
      /// the LHCb ids was on the track.
      bool addSortedToLhcbIDs( span<const LHCbID> ids );

      /// Add an unsorted sequence of LHCbID to the list of LHCbIDs associated to the track. The vector will first be
      /// sorted, which makes this more expensive than the method above. Returns true if none of the LHCb ids was on the
      /// track.
      bool addToLhcbIDs( span<const LHCbID> ids );

      /// Sets the list of LHCbIDs associated to this track. The input vector will be sorted.
      void setLhcbIDs( span<const LHCbID> ids );

      /// Sets the list of LHCbIDs associated to this track. The input vector must be sorted.
      void setSortedLhcbIDs( span<const LHCbID> ids );

      /// Sets the list of LHCbIDs associated to this track. The input vector must be sorted.
      void setSortedLhcbIDs( LHCbIDContainer&& value );

      /// Returns true if the LHCbIDs of track are a subset is the LHCbIDs of this track.
      bool containsLhcbIDs( const Track& track ) const;

      /// Returns true if LHCbIDs in ids are a subset of the LHCbIDs of this track.
      bool containsLhcbIDs( span<const LHCbID> ids ) const;

      /// Returns the number of common LHCbIDs.
      size_t nCommonLhcbIDs( const Track& track ) const;

      /// Remove an LHCbID from the list of LHCbIDs associated to the track
      void removeFromLhcbIDs( const LHCbID& value );

      /// Add a track to the list of ancestors of this track
      void addToAncestors( const Track& ancestor );

      /// Set pointer to object holding track fit data. Track becomes owner.
      void setFitResult( ITrackFitResult* trackfit );

      /// get pointer to the object holding the trackfit data.
      ITrackFitResult* fitResult();

      /// get const pointer to the object holding the trackfit data.
      const ITrackFitResult* fitResult() const;

      /// Clear the track before re-use
      void reset();

      /// Copy the info from the argument track into this track
      void copy( const Track& track );

      /// Check the type of the track (see the Types enum)
      bool checkType( const Types& value ) const;

      /// Check the history of the track (see the History enum)
      bool checkHistory( const History& value ) const;

      /// Check the fit history of the track (see the FitHistory enum)
      bool checkFitHistory( const FitHistory& value ) const;

      /// Check the pattern recognition status of the track (see the PatRecStatus enum)
      bool checkPatRecStatus( const PatRecStatus& value ) const;

      /// Check the fitting status of the track (see the FitStatus enum)
      bool checkFitStatus( const FitStatus& value ) const;

      /// Update the flag (see the Flags enum)
      void setFlag( Flags flag, bool ok );

      /// Check the status of the flag (see the Flags enum)
      bool checkFlag( const Flags& flag ) const;

      /// Check if track is Velo backward
      bool isVeloBackward() const;

      /// Check if track is of a type that goes thro T stations
      bool hasT() const;

      /// Check if track is of a type that goes thro Velo
      bool hasVelo() const;

      /// Check if track is of a type that goes thro TT
      bool hasTT() const;

      /// Check if track is of a type that goes thro UT
      bool hasUT() const;

      /// Check if track is of a type that goes thro Muon
      bool hasMuon() const;

      /// Number of hits on the track
      int nHits() const;

      /// Number of VP hits on the track
      int nVPHits() const;

      /// Number of UT hits on the track
      int nUTHits() const;

      /// Number of FT hits on the track
      int nFTHits() const;

      /// Check whether the given LHCbID is on the track
      bool isOnTrack( const LHCbID& value ) const;

      /// printOut method to Gaudi message stream
      std::ostream& fillStream( std::ostream& os ) const override;

      /// Check whether the track has information for the specified key
      bool hasInfo( AdditionalInfo key ) const;

      /// Add new information, associated with the specified key. This method cannot be used to modify information for
      /// an already existing key
      bool addInfo( AdditionalInfo key, const double info );

      /// Extract the information associated with the specified key. If there is no such information the default value
      /// will be returned.
      double info( AdditionalInfo key, const double def ) const;

      /// Erase the information associated with the specified key
      Track::ExtraInfo::size_type eraseInfo( AdditionalInfo key );

      /// Retrieve const  Chi^2 per degree of freedom of the track
      double chi2PerDoF() const;

      /// Update  Chi^2 per degree of freedom of the track
      void setChi2PerDoF( double value );

      /// Retrieve const  Number of degrees of freedom of the track
      int nDoF() const;

      /// Update  Number of degrees of freedom of the track
      void setNDoF( int value );

      /// Retrieve const  Likelihood variable
      double likelihood() const;

      /// Update  Likelihood variable
      void setLikelihood( double value );

      /// Retrieve const  ghost probability variable
      double ghostProbability() const;

      /// Update  ghost probability variable
      void setGhostProbability( double value );

      /// Retrieve const  The variety of track flags
      unsigned int flags() const;

      /// Update  The variety of track flags
      void setFlags( unsigned int value );

      /// Retrieve Track type
      Types type() const;

      /// Update Track type
      void setType( const Types& value );

      /// Retrieve Specifies the pattern recognition algorithm that created the track
      History history() const;

      /// Update Specifies the pattern recognition algorithm that created the track
      void setHistory( const History& value );

      /// Retrieve Specifies the fitting algorithm the fitted the track)
      FitHistory fitHistory() const;

      /// Update Specifies the fitting algorithm the fitted the track)
      void setFitHistory( const FitHistory& value );

      /// Retrieve Pattern recognition status of the track
      PatRecStatus patRecStatus() const;

      /// Update Pattern recognition status of the track
      void setPatRecStatus( const PatRecStatus& value );

      /// Retrieve Fitting status of the track
      FitStatus fitStatus() const;

      /// Update Fitting status of the track
      void setFitStatus( const FitStatus& value );

      /// Retrieve Track flags
      Flags flag() const;

      /// Retrieve Track specific bits
      unsigned int specific() const;

      /// Update Track specific bits
      void setSpecific( unsigned int value );

      /// Retrieve const  Container of (sorted) LHCbIDs
      const std::vector<LHCbID>& lhcbIDs() const;

      /// Retrieve const  Container with pointers to all the states
      const std::vector<State*>& states() const;

      /// Retrieve  Container with pointers to all the states
      std::vector<State*>& states();

      /// Retrieve const  Additional pattern recognition information. Do not access directly, use *Info() methods
      /// instead.
      const ExtraInfo& extraInfo() const;

      /// Update  Additional pattern recognition information. Do not access directly, use *Info() methods instead.
      void setExtraInfo( const ExtraInfo& value );

      /// Retrieve (const)  Ancestor tracks that created this one
      const SmartRefVector<Track>& ancestors() const;

      /// Retrieve  Ancestor tracks that created this one
      SmartRefVector<Track>& ancestors();

      /// Add to  Ancestor tracks that created this one
      void addToAncestors( const SmartRef<Track>& value );

      /// Att to (pointer)  Ancestor tracks that created this one
      void addToAncestors( const Track* value );

      /// Remove from  Ancestor tracks that created this one
      void removeFromAncestors( const SmartRef<Track>& value );

      /// Clear  Ancestor tracks that created this one
      void clearAncestors();

      friend std::ostream& operator<<( std::ostream& str, const Track& obj ) { return obj.fillStream( str ); }

      // flag which indicates client code can go for 'threeMomCovMatrix', `momPosCovMatrix` and `posCovMatrix` and not
      // for a track-like stateCov -- possible values: yes (for track-like objects ) no (for neutrals, composites),
      // maybe (particle, check at runtime, calls may return invalid results)
      static constexpr auto canBeExtrapolatedDownstream = CanBeExtrapolatedDownstream::yes;

      [[nodiscard, gnu::always_inline]] friend auto trackState( Track const& t ) { return t.firstState(); }
      [[nodiscard, gnu::always_inline]] friend auto threeMomentum( Track const& t ) {
        auto m = t.momentum();
        return LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{m.x(), m.y(), m.z()};
      }
      [[nodiscard, gnu::always_inline]] friend auto slopes( Track const& t ) {
        auto sl = t.slopes();
        return LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{sl.x(), sl.y(), sl.z()};
      }
      [[nodiscard, gnu::always_inline]] friend auto referencePoint( Track const& t ) {
        auto pos = t.position();
        return LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{pos.x(), pos.y(), pos.z()};
      }

    private:
      /// Offsets of bitfield flags
      enum flagsBits {
        typeBits         = 0,
        historyBits      = 4,
        fitHistoryBits   = 11,
        patRecStatusBits = 14,
        fitStatusBits    = 16,
        flagBits         = 18,
        specificBits     = 28
      };

      /// Bitmasks for bitfield flags
      enum flagsMasks {
        typeMask         = 0xfL,
        historyMask      = 0x7f0L,
        fitHistoryMask   = 0x3800L,
        patRecStatusMask = 0xc000L,
        fitStatusMask    = 0x30000L,
        flagMask         = 0xffc0000L,
        specificMask     = 0xf0000000L
      };

      double                           m_chi2PerDoF{0.0};       ///< Chi^2 per degree of freedom of the track
      int                              m_nDoF{0};               ///< Number of degrees of freedom of the track
      double                           m_likelihood{999};       ///< Likelihood variable
      double                           m_ghostProbability{999}; ///< ghost probability variable
      unsigned int                     m_flags{0};              ///< The variety of track flags
      std::vector<LHCbID>              m_lhcbIDs;               ///< Container of (sorted) LHCbIDs
      std::vector<State*>              m_states;                ///< Container with pointers to all the states
      std::unique_ptr<ITrackFitResult> m_fitResult; ///< Transient data related to track fit (nodes, material, etc)
      ExtraInfo m_extraInfo; ///< Additional pattern recognition information. Do not access directly, use *Info()
                             ///< methods instead.
      SmartRefVector<Track> m_ancestors; ///< Ancestor tracks that created this one
    };                                   // class Track

    /// Definition of Keyed Container for Track
    typedef KeyedContainer<Track, Containers::HashMap> Tracks;
    // -----------------------------------------------------------------------------
    // end of class
    // -----------------------------------------------------------------------------

    // Including forward declarations

    inline Track::Track( const History& history, const Types& trackType, const PatRecStatus& patStatus,
                         const std::vector<LHCbID>& ids, const State& aState )
        : KeyedObject<int>(), m_lhcbIDs( std::move( ids ) ) {
      addToStates( aState );
      setHistory( history );
      setType( trackType );
      setPatRecStatus( patStatus );
      std::sort( m_lhcbIDs.begin(), m_lhcbIDs.end() );
    }

    inline Track::Track( const History& history, const Types& trackType, const PatRecStatus& patStatus )
        : KeyedObject<int>() {
      setHistory( history );
      setType( trackType );
      setPatRecStatus( patStatus );
    }

    inline Track::Track( const Track& track ) : KeyedObject<int>() { this->copy( track ); }

    inline Track::Track( const Track& track, const int key ) : KeyedObject<int>( key ) { this->copy( track ); }

    inline Track::~Track() {
      std::for_each( m_states.begin(), m_states.end(), []( auto& i ) { delete i; } );
    }

    inline const CLID& Track::clID() const { return Track::classID(); }

    inline const CLID& Track::classID() { return CLID_Track; }

    inline double Track::chi2PerDoF() const { return m_chi2PerDoF; }

    inline void Track::setChi2PerDoF( double value ) { m_chi2PerDoF = value; }

    inline int Track::nDoF() const { return m_nDoF; }

    inline void Track::setNDoF( int value ) { m_nDoF = value; }

    inline double Track::likelihood() const { return m_likelihood; }

    inline void Track::setLikelihood( double value ) { m_likelihood = value; }

    inline double Track::ghostProbability() const { return m_ghostProbability; }

    inline void Track::setGhostProbability( double value ) { m_ghostProbability = value; }

    inline unsigned int Track::flags() const { return m_flags; }

    inline void Track::setFlags( unsigned int value ) { m_flags = value; }

    inline Track::Types Track::type() const { return ( Track::Types )( ( m_flags & typeMask ) >> typeBits ); }

    inline void Track::setType( const Types& value ) {
      auto val = (unsigned int)value;
      m_flags &= ~typeMask;
      m_flags |= ( ( ( (unsigned int)val ) << typeBits ) & typeMask );
    }

    inline Track::History Track::history() const {
      return ( Track::History )( ( m_flags & historyMask ) >> historyBits );
    }

    inline void Track::setHistory( const History& value ) {
      auto val = (unsigned int)value;
      m_flags &= ~historyMask;
      m_flags |= ( ( ( (unsigned int)val ) << historyBits ) & historyMask );
    }

    inline Track::FitHistory Track::fitHistory() const {
      return ( Track::FitHistory )( ( m_flags & fitHistoryMask ) >> fitHistoryBits );
    }

    inline void Track::setFitHistory( const FitHistory& value ) {
      auto val = (unsigned int)value;
      m_flags &= ~fitHistoryMask;
      m_flags |= ( ( ( (unsigned int)val ) << fitHistoryBits ) & fitHistoryMask );
    }

    inline Track::PatRecStatus Track::patRecStatus() const {
      return ( Track::PatRecStatus )( ( m_flags & patRecStatusMask ) >> patRecStatusBits );
    }

    inline void Track::setPatRecStatus( const PatRecStatus& value ) {
      auto val = (unsigned int)value;
      m_flags &= ~patRecStatusMask;
      m_flags |= ( ( ( (unsigned int)val ) << patRecStatusBits ) & patRecStatusMask );
    }

    inline Track::FitStatus Track::fitStatus() const {
      return ( Track::FitStatus )( ( m_flags & fitStatusMask ) >> fitStatusBits );
    }

    inline void Track::setFitStatus( const FitStatus& value ) {
      auto val = (unsigned int)value;
      m_flags &= ~fitStatusMask;
      m_flags |= ( ( ( (unsigned int)val ) << fitStatusBits ) & fitStatusMask );
    }

    inline Track::Flags Track::flag() const { return ( Track::Flags )( ( m_flags & flagMask ) >> flagBits ); }

    inline unsigned int Track::specific() const { return (unsigned int)( ( m_flags & specificMask ) >> specificBits ); }

    inline void Track::setSpecific( unsigned int value ) {
      auto val = (unsigned int)value;
      m_flags &= ~specificMask;
      m_flags |= ( ( ( (unsigned int)val ) << specificBits ) & specificMask );
    }

    inline const std::vector<LHCbID>& Track::lhcbIDs() const { return m_lhcbIDs; }

    inline const std::vector<State*>& Track::states() const { return m_states; }

    inline std::vector<State*>& Track::states() { return m_states; }

    inline const Track::ExtraInfo& Track::extraInfo() const { return m_extraInfo; }

    inline void Track::setExtraInfo( const ExtraInfo& value ) { m_extraInfo = value; }

    inline const SmartRefVector<Track>& Track::ancestors() const { return m_ancestors; }

    inline SmartRefVector<Track>& Track::ancestors() { return m_ancestors; }

    inline void Track::addToAncestors( const SmartRef<Track>& value ) { m_ancestors.push_back( value ); }

    inline void Track::addToAncestors( const Track* value ) { m_ancestors.push_back( value ); }

    inline void Track::removeFromAncestors( const SmartRef<Track>& value ) {
      auto i = std::remove( m_ancestors.begin(), m_ancestors.end(), value );
      m_ancestors.erase( i, m_ancestors.end() );
    }

    inline void Track::clearAncestors() { m_ancestors.clear(); }

    inline void Track::positionAndMomentum( Gaudi::XYZPoint& pos, Gaudi::XYZVector& mom,
                                            Gaudi::SymMatrix6x6& cov6D ) const {
      firstState().positionAndMomentum( pos, mom, cov6D );
    }

    inline void Track::positionAndMomentum( Gaudi::XYZPoint& pos, Gaudi::XYZVector& mom ) const {
      firstState().positionAndMomentum( pos, mom );
    }

    inline Gaudi::XYZPoint Track::position() const { return firstState().position(); }

    inline void Track::position( Gaudi::XYZPoint& pos, Gaudi::SymMatrix3x3& errPos ) const {
      pos    = firstState().position();
      errPos = firstState().errPosition();
    }

    inline Gaudi::XYZVector Track::slopes() const { return firstState().slopes(); }

    inline void Track::slopes( Gaudi::XYZVector& slopes, Gaudi::SymMatrix3x3& errSlopes ) const {
      slopes    = firstState().slopes();
      errSlopes = firstState().errSlopes();
    }

    inline Gaudi::XYZVector Track::momentum() const { return this->firstState().momentum(); }

    inline double Track::p() const { return this->firstState().p(); }

    inline double Track::pt() const { return this->firstState().pt(); }

    inline double Track::pseudoRapidity() const { return this->slopes().eta(); }

    inline double Track::phi() const { return this->slopes().phi(); }

    inline void Track::momentum( Gaudi::XYZVector& mom, Gaudi::SymMatrix3x3& errMom ) const {
      mom    = firstState().momentum();
      errMom = firstState().errMomentum();
    }

    inline void Track::posMomCovariance( Gaudi::SymMatrix6x6& cov6D ) const { cov6D = firstState().posMomCovariance(); }

    inline State& Track::firstState() {
      // check at least the "first state" exists
      if ( m_states.empty() ) throw GaudiException( "first state not defined!", "Track.h", StatusCode::FAILURE );
      return *m_states[0];
    }

    inline const State& Track::firstState() const {
      // check at least the "first state" exists
      if ( m_states.empty() ) throw GaudiException( "first state not defined!", "Track.h", StatusCode::FAILURE );
      return *m_states[0];
    }

    inline int Track::charge() const {
      double qP = firstState().qOverP();
      return std::abs( qP ) < TrackParameters::lowTolerance ? 0 : qP < 0 ? -1 : +1;
    }

    inline double Track::chi2() const { return ( m_chi2PerDoF * double( m_nDoF ) ); }

    inline void Track::setChi2AndDoF( double chi2, int ndof ) {

      m_chi2PerDoF = ( ndof != 0 ) ? chi2 / ( (double)( ndof ) ) : 0.0;
      m_nDoF       = ndof;
    }

    inline unsigned int Track::nStates() const { return m_states.size(); }

    inline unsigned int Track::nLHCbIDs() const { return m_lhcbIDs.size(); }

    inline bool Track::addToLhcbIDs( span<const LHCbID> ids ) {

      LHCbIDContainer copy( ids.begin(), ids.end() );
      std::sort( copy.begin(), copy.end() );
      return addSortedToLhcbIDs( copy );
    }

    inline void Track::setLhcbIDs( span<const LHCbID> ids ) {

      m_lhcbIDs.assign( ids.begin(), ids.end() );
      std::sort( m_lhcbIDs.begin(), m_lhcbIDs.end() );
    }

    inline void Track::setSortedLhcbIDs( span<const LHCbID> ids ) {

      m_lhcbIDs.assign( ids.begin(), ids.end() );
      assert( std::is_sorted( begin( m_lhcbIDs ), end( m_lhcbIDs ) ) );
    }

    inline void Track::setSortedLhcbIDs( LHCbIDContainer&& value ) {

      m_lhcbIDs = std::move( value );
      assert( std::is_sorted( begin( m_lhcbIDs ), end( m_lhcbIDs ) ) );
    }

    inline bool Track::containsLhcbIDs( const Track& track ) const { return containsLhcbIDs( track.lhcbIDs() ); }

    inline bool Track::containsLhcbIDs( span<const LHCbID> ids ) const {
      return std::includes( m_lhcbIDs.begin(), m_lhcbIDs.end(), ids.begin(), ids.end() );
    }

    inline void Track::addToAncestors( const Track& ancestor ) {
      m_ancestors.push_back( const_cast<Track*>( &ancestor ) );
    }

    inline bool Track::isVeloBackward() const { return type() == Track::Types::VeloBackward; }

    inline bool Track::checkType( const Types& value ) const { return type() == value; }

    inline bool Track::checkHistory( const History& value ) const { return history() == value; }

    inline bool Track::checkFitHistory( const FitHistory& value ) const { return fitHistory() == value; }

    inline bool Track::checkPatRecStatus( const PatRecStatus& value ) const { return patRecStatus() == value; }

    inline bool Track::checkFitStatus( const FitStatus& value ) const { return fitStatus() == value; }

    inline void Track::setFlag( Flags flag, bool ok ) {
      unsigned int val = ( ( (unsigned int)flag ) << flagBits ) & flagMask;
      if ( ok )
        m_flags |= val;
      else
        m_flags &= ~val;
    }

    inline bool Track::checkFlag( const Flags& flag ) const {
      unsigned int val = ( (unsigned int)flag << flagBits );
      return ( 0 != ( m_flags & flagMask & val ) );
    }

    inline bool Track::hasT() const {
      const auto t = type();
      return t == Track::Types::Ttrack || t == Track::Types::Downstream || t == Track::Types::Long ||
             t == Track::Types::SeedMuon || t == Track::Types::LongMuon;
    }

    inline bool Track::hasVelo() const {
      const auto t = type();
      /// add velo state for SeedMuon and MuonUT tracks specifically for Track efficiency study
      return t == Track::Types::Velo || t == Track::Types::VeloBackward || t == Track::Types::Upstream ||
             t == Track::Types::Long || t == Track::Types::VeloMuon || t == Track::Types::LongMuon;
    }

    inline bool Track::hasUT() const {
      const auto t = type();
      return t == Track::Types::Downstream || t == Track::Types::Upstream || t == Track::Types::Long ||
             t == Track::Types::MuonUT || t == Track::Types::LongMuon;
    }

    inline bool Track::hasMuon() const {
      const auto t = type();
      return t == Track::Types::MuonUT || t == Track::Types::LongMuon || t == Track::Types::VeloMuon ||
             t == Track::Types::SeedMuon;
    }

    /* [[deprecated( "please use hasUT instead" )]] */ inline bool Track::hasTT() const { return hasUT(); }

    inline int Track::nHits() const { return lhcbIDs().size(); }

    inline int Track::nVPHits() const {
      return std::count_if( lhcbIDs().begin(), lhcbIDs().end(), []( const auto& id ) { return id.isVP(); } );
    }

    inline int Track::nUTHits() const {
      return std::count_if( lhcbIDs().begin(), lhcbIDs().end(), []( const auto& id ) { return id.isUT(); } );
    }

    inline int Track::nFTHits() const {
      return std::count_if( lhcbIDs().begin(), lhcbIDs().end(), []( const auto& id ) { return id.isFT(); } );
    }

  } // namespace v1
} // namespace LHCb::Event

// -----------------------------------------------------------------------------
// end of namespace
// -----------------------------------------------------------------------------
