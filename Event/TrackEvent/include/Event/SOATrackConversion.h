/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/State.h"
#include "Event/Track_v1.h"
#include "Event/Track_v2.h"
#include "Event/Track_v3.h"
#include "Event/UniqueIDGenerator.h"
#include "GaudiAlg/FunctionalDetails.h"

#include <stdexcept>
#include <tuple>
#include <type_traits>

namespace LHCb::Event::conversion {
  // Conversion to v1::Track and v2::Track State locations
  constexpr State::Location to_aos_state_loc( v3::Tracks::StateLocation loc ) {
    switch ( loc ) {
    case ( v3::Tracks::StateLocation::ClosestToBeam ):
      return State::ClosestToBeam;
    case ( v3::Tracks::StateLocation::FirstMeasurement ):
      return State::FirstMeasurement;
    case ( v3::Tracks::StateLocation::LastMeasurement ):
      return State::LastMeasurement;
    case ( v3::Tracks::StateLocation::BegRich1 ):
      return State::BegRich1;
    case ( v3::Tracks::StateLocation::EndRich1 ):
      return State::EndRich1;
    case ( v3::Tracks::StateLocation::BegRich2 ):
      return State::BegRich2;
    case ( v3::Tracks::StateLocation::EndRich2 ):
      return State::EndRich2;
    case ( v3::Tracks::StateLocation::Unknown ):
      return State::LocationUnknown;
    default:
      throw std::invalid_argument( "invalid v3::Tracks::StateLocation" );
    }
  }

  // This is what will be used most often
  template <v3::Tracks::StateLocation L>
  inline constexpr auto to_aos_state_loc_v = to_aos_state_loc( L );

  // Conversion from v3 Track types to v1 types
  constexpr v1::Track::Types to_v1_track_type( v3::TrackType t ) {
    switch ( t ) {
    case v3::TrackType::Velo:
      return v1::Track::Types::Velo;
    case v3::TrackType::Long:
    case v3::TrackType::FittedForward:
      return v1::Track::Types::Long;
    case v3::TrackType::Upstream:
      return v1::Track::Types::Upstream;
    case v3::TrackType::Downstream:
      return v1::Track::Types::Downstream;
    case v3::TrackType::Ttrack:
      return v1::Track::Types::Ttrack;
    case v3::TrackType::Muon:
      return v1::Track::Types::Muon;
    case v3::TrackType::UT:
      return v1::Track::Types::UT;
    default:
      return v1::Track::Types::Unknown;
    }
  }

  template <class Enum>
  constexpr v3::TrackType to_v3_track_type( Enum t ) {
    switch ( t ) {
    case Enum::Velo:
      return v3::TrackType::Velo;
    case Enum::VeloBackward:
      return v3::TrackType::Velo;
    case Enum::Long:
      return v3::TrackType::Long;
    case Enum::Upstream:
      return v3::TrackType::Upstream;
    case Enum::Downstream:
      return v3::TrackType::Downstream;
    case Enum::Ttrack:
      return v3::TrackType::Ttrack;
    case Enum::Muon:
      return v3::TrackType::Muon;
    case Enum::UT:
      return v3::TrackType::UT;
    default:
      return v3::TrackType::Unknown;
    }
  }

  /// Check if an object is valid
  template <class Proxy>
  constexpr std::enable_if_t<!std::is_pointer_v<Proxy>, bool> ref_is_valid( Proxy const& ) {
    return true;
  }

  template <class Proxy>
  constexpr std::enable_if_t<std::is_pointer_v<Proxy>, bool> ref_is_valid( Proxy const& v ) {
    return ( v != nullptr );
  }

  /// Update the fit result
  template <class TrackProxy, class OldTrack>
  void update_fit_result( TrackProxy& outTrack, OldTrack const& track ) {
    outTrack.template field<v3::Tag::Chi2>().set( track.chi2() );
    outTrack.template field<v3::Tag::nDoF>().set( track.nDoF() );
  }

  /// Update a single state
  template <v3::Tracks::StateLocation L, class TrackProxy, class OldTrack>
  bool update_state( TrackProxy& outTrack, OldTrack const& t ) {

    auto rstate = t.stateAt( to_aos_state_loc_v<L> );

    if ( !ref_is_valid( rstate ) ) return false;

    auto state = Gaudi::Functional::details::deref( rstate );

    // positions, slopes, q/p
    outTrack.template field<v3::Tag::States>()[outTrack.state_index( L )].setPosition( state.x(), state.y(),
                                                                                       state.z() );
    outTrack.template field<v3::Tag::States>()[outTrack.state_index( L )].setDirection( state.tx(), state.ty() );
    outTrack.template field<v3::Tag::States>()[outTrack.state_index( L )].setQOverP( state.qOverP() );

    // covariance
    auto const& cov = state.covariance();

    outTrack.template field<v3::Tag::StateCovs>()[outTrack.state_index( L )].set(
        cov[0][0], cov[0][1], cov[0][2], cov[0][3], cov[0][4], cov[1][1], cov[1][2], cov[1][3], cov[1][4], cov[2][2],
        cov[2][3], cov[2][4], cov[3][3], cov[3][4], cov[4][4] );
    return true;
  }

  namespace detail {
    /// Actual implementation of the "update_states" function
    template <class TrackProxy, class OldTrack, v3::Tracks::StateLocation... L>
    static bool update_states_impl( TrackProxy& outTrack, OldTrack const& t, v3::state_collection<L...> ) {
      return ( update_state<L>( outTrack, t ) && ... );
    }
  } // namespace detail

  /// Update all the states of a track
  template <class TrackProxy, class OldTrack>
  bool update_states( v3::TrackType type, TrackProxy& outTrack, OldTrack& t ) {

    switch ( type ) {
    case v3::TrackType::Unknown:
      return detail::update_states_impl( outTrack, t, v3::available_states_t<v3::TrackType::Unknown>{} );
    case v3::TrackType::Velo:
      return detail::update_states_impl( outTrack, t, v3::available_states_t<v3::TrackType::Velo>{} );
    case v3::TrackType::VeloBackward:
      return detail::update_states_impl( outTrack, t, v3::available_states_t<v3::TrackType::VeloBackward>{} );
    case v3::TrackType::Long:
      return detail::update_states_impl( outTrack, t, v3::available_states_t<v3::TrackType::Long>{} );
    case v3::TrackType::FittedForward:
      return detail::update_states_impl( outTrack, t, v3::available_states_t<v3::TrackType::FittedForward>{} );
    case v3::TrackType::Upstream:
      return detail::update_states_impl( outTrack, t, v3::available_states_t<v3::TrackType::Upstream>{} );
    case v3::TrackType::Downstream:
      return detail::update_states_impl( outTrack, t, v3::available_states_t<v3::TrackType::Downstream>{} );
    case v3::TrackType::Ttrack:
      return detail::update_states_impl( outTrack, t, v3::available_states_t<v3::TrackType::Ttrack>{} );
    case v3::TrackType::Muon:
      return detail::update_states_impl( outTrack, t, v3::available_states_t<v3::TrackType::Muon>{} );
    case v3::TrackType::UT:
      return detail::update_states_impl( outTrack, t, v3::available_states_t<v3::TrackType::UT>{} );
    case v3::TrackType::SeedMuon:
      return detail::update_states_impl( outTrack, t, v3::available_states_t<v3::TrackType::SeedMuon>{} );
    case v3::TrackType::VeloMuon:
      return detail::update_states_impl( outTrack, t, v3::available_states_t<v3::TrackType::VeloMuon>{} );
    case v3::TrackType::MuonUT:
      return detail::update_states_impl( outTrack, t, v3::available_states_t<v3::TrackType::MuonUT>{} );
    case v3::TrackType::LongMuon:
      return detail::update_states_impl( outTrack, t, v3::available_states_t<v3::TrackType::LongMuon>{} );
    case v3::TrackType::Last:
      throw GaudiException( "Last is not a valid Track Type.", "LHCb::Event::conversion::update_states",
                            StatusCode::FAILURE );
    }

    __builtin_unreachable();
  }

  /// Update the LHCb IDs
  template <class TrackProxy, class OldTrack>
  void update_lhcb_ids( TrackProxy& outTrack, OldTrack& track ) {

    int numVPLHCbIDs = 0;
    int numUTLHCbIDs = 0;
    int numFTLHCbIDs = 0;

    // -- This respects the sorting of the original track.
    for ( auto& id : track.lhcbIDs() ) {
      if ( id.isVP() ) {
        outTrack.template field<v3::Tag::VPHits>().resize( numVPLHCbIDs + 1 );
        outTrack.template field<v3::Tag::VPHits>()[numVPLHCbIDs++].template field<v3::Tag::LHCbID>().set( id );
      }

      if ( id.isUT() ) {
        outTrack.template field<v3::Tag::UTHits>().resize( numUTLHCbIDs + 1 );
        outTrack.template field<v3::Tag::UTHits>()[numUTLHCbIDs++].template field<v3::Tag::LHCbID>().set( id );
      }

      if ( id.isFT() ) {
        outTrack.template field<v3::Tag::FTHits>().resize( numFTLHCbIDs + 1 );
        outTrack.template field<v3::Tag::FTHits>()[numFTLHCbIDs++].template field<v3::Tag::LHCbID>().set( id );
      }
    }

    assert( numVPLHCbIDs + numUTLHCbIDs + numFTLHCbIDs == static_cast<int>( track.lhcbIDs().size() ) );
  }

  using Status = int;

  enum StatusEnum : Status { Success, DifferentType, InvalidStates };

  static constexpr Status StatusShift = 10; // Only valid as long as we have less than 10 Status items

  /// Extend the result adding an additional status
  constexpr Status extend_result( Status r, StatusEnum s ) { return ( r * StatusShift ) + s; }

  /// Parse a result getting any possible new status and returning the new combined result
  constexpr std::tuple<Status, Status> process_result( Status r ) { return {r / StatusShift, r % StatusShift}; }

  /** Fill the values of a PrFittedGenericTrack with those from an old track
   *
   * The old track can be of v1::Track or v2::Track type. The "type" argument
   * must correspond to the type to check (that of the "outTrack" container).
   */
  template <class TrackProxy, class OldTrack>
  Status convert_track( v3::TrackType type, TrackProxy& outTrack, OldTrack const& rtrack,
                        LHCb::UniqueIDGenerator const& unique_id_generator ) {

    if ( !ref_is_valid( rtrack ) ) throw std::runtime_error( "Null track pointer detected" );

    auto const& track = Gaudi::Functional::details::deref( rtrack );

    Status status = Success;

    if ( to_v3_track_type( track.type() ) != type ) status = extend_result( status, DifferentType );

    auto sc = update_states( type, outTrack, track );
    if ( !sc ) status = extend_result( status, InvalidStates );

    update_fit_result( outTrack, track );
    update_lhcb_ids( outTrack, track );

    using int_v = decltype( outTrack.template field<v3::Tag::UniqueID>().get() );

    outTrack.template field<v3::Tag::UniqueID>().set( unique_id_generator.generate<int_v>().value() );

    outTrack.template field<v3::Tag::history>().set( track.history() );

    return status;
  }
} // namespace LHCb::Event::conversion
