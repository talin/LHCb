/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "Detector/UT/ChannelID.h"
#include "Detector/VP/VPChannelID.h"
#include "Event/PrProxyHelpers.h"
#include "Event/PrSeedTracks.h"
#include "Event/PrTracksTag.h"
#include "Event/PrUpstreamTracks.h"
#include "Event/PrVeloTracks.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/LHCbID.h"
#include "Kernel/Traits.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"
#include "TrackEnums.h"

/**
 * Track data for exchanges between FT and Fit
 *
 * @author: Arthur Hennequin
 * 2020-05-04
 * add hit indices by Peilian Li
 */

namespace LHCb::Pr::Long {
  struct Tag {
    struct trackVP : Event::int_field {};
    struct trackUT : Event::int_field {};
    struct trackSeed : Event::int_field {};
    struct Index : Event::int_field {};
    struct LHCbID : Event::lhcbid_field {};
    struct VPHits : Event::vector_field<Event::struct_field<Index, LHCbID>> {};
    struct UTHits : Event::vector_field<Event::struct_field<Index, LHCbID>> {};
    struct FTHits : Event::vector_field<Event::struct_field<Index, LHCbID>> {};
    struct States : Event::states_field<TracksInfo::NumLongStates> {};

    template <typename T>
    using long_t = Event::SOACollection<T, trackVP, trackUT, trackSeed, VPHits, UTHits, FTHits, States>;
  };

  namespace SP = LHCb::Event::StateParameters;

  struct Tracks : Tag::long_t<Tracks> {
    using base_t = typename Tag::long_t<Tracks>;
    using tag_t  = Tag; // Needed for tag_type_t helper defined in PrTracksTag.h

    constexpr static auto NumLongStates = TracksInfo::NumLongStates; // number of state 0: vstate, 1: Tstate
    constexpr static auto MaxVPHits     = TracksInfo::MaxVPHits;
    constexpr static auto MaxUTHits     = TracksInfo::MaxUTHits;
    constexpr static auto MaxFTHits     = TracksInfo::MaxFTHits;
    constexpr static auto MaxLHCbIDs    = TracksInfo::MaxFTHits + TracksInfo::MaxVPHits + TracksInfo::MaxUTHits;

    using base_t::allocator_type;
    using History = Event::Enum::Track::History;

    Tracks( Velo::Tracks const* velo_ancestors, Upstream::Tracks const* upstream_ancestors,
            Seeding::Tracks const* seed_ancestors, History history,
            Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(), allocator_type alloc = {} )
        : base_t{std::move( zipIdentifier ), std::move( alloc )}
        , m_velo_ancestors{velo_ancestors}
        , m_upstream_ancestors{upstream_ancestors}
        , m_seed_ancestors{seed_ancestors}
        , m_history{history} {}

    // Constructor used by zipping machinery when making a copy of a zip
    Tracks( Zipping::ZipFamilyNumber zn, Tracks const& old )
        : base_t{std::move( zn ), old}
        , m_velo_ancestors{old.m_velo_ancestors}
        , m_upstream_ancestors{old.m_upstream_ancestors}
        , m_seed_ancestors{old.m_seed_ancestors}
        , m_history{old.m_history} {}

    // Return pointer to ancestor container
    [[nodiscard, gnu::always_inline]] Velo::Tracks const*     getVeloAncestors() const { return m_velo_ancestors; };
    [[nodiscard, gnu::always_inline]] Upstream::Tracks const* getUpstreamAncestors() const {
      return m_upstream_ancestors;
    };
    [[nodiscard, gnu::always_inline]] Seeding::Tracks const* getSeedAncestors() const { return m_seed_ancestors; };
    [[nodiscard, gnu::always_inline]] History                history() const { return m_history; };

    template <SIMDWrapper::InstructionSet simd, ProxyBehaviour behaviour, typename ContainerType>
    struct LongProxy : Event::Proxy<simd, behaviour, ContainerType> {
      using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
      using base_t::base_t;
      using base_t::width;
      using simd_t  = SIMDWrapper::type_map_t<simd>;
      using int_v   = typename simd_t::int_v;
      using float_v = typename simd_t::float_v;

      [[nodiscard, gnu::always_inline]] auto qOverP( std::size_t iState = 0 ) const {
        return this->template get<Tag::States>( iState ).qOverP();
      }
      [[nodiscard, gnu::always_inline]] auto p( std::size_t iState = 0 ) const { return abs( 1 / qOverP( iState ) ); }
      [[nodiscard, gnu::always_inline]] auto nVPHits() const { return this->template field<Tag::VPHits>().size(); }
      [[nodiscard, gnu::always_inline]] auto nUTHits() const { return this->template field<Tag::UTHits>().size(); }
      [[nodiscard, gnu::always_inline]] auto nFTHits() const { return this->template field<Tag::FTHits>().size(); }
      [[nodiscard, gnu::always_inline]] auto nHits() const { return nVPHits() + nUTHits() + nFTHits(); }
      [[nodiscard, gnu::always_inline]] auto trackVP() const { return this->template get<Tag::trackVP>(); }
      [[nodiscard, gnu::always_inline]] auto trackUT() const { return this->template get<Tag::trackUT>(); }
      [[nodiscard, gnu::always_inline]] auto trackSeed() const { return this->template get<Tag::trackSeed>(); }
      [[nodiscard, gnu::always_inline]] auto vp_index( std::size_t i ) const {
        return this->template field<Tag::VPHits>()[i].template get<Tag::Index>();
      }
      [[nodiscard, gnu::always_inline]] auto ut_index( std::size_t i ) const {
        return this->template field<Tag::UTHits>()[i].template get<Tag::Index>();
      }
      [[nodiscard, gnu::always_inline]] auto ft_index( std::size_t i ) const {
        return this->template field<Tag::FTHits>()[i].template get<Tag::Index>();
      }
      [[nodiscard, gnu::always_inline]] auto vp_lhcbID( std::size_t i ) const {
        return this->template field<Tag::VPHits>()[i].template get<Tag::LHCbID>();
      }
      [[nodiscard, gnu::always_inline]] auto ut_lhcbID( std::size_t i ) const {
        return this->template field<Tag::UTHits>()[i].template get<Tag::LHCbID>();
      }
      [[nodiscard, gnu::always_inline]] auto ft_lhcbID( std::size_t i ) const {
        return this->template field<Tag::FTHits>()[i].template get<Tag::LHCbID>();
      }
      // Retrieve state Info
      [[nodiscard, gnu::always_inline]] auto StatePosDir( std::size_t i ) const {
        auto const state = this->template get<Tag::States>( i );
        return LHCb::LinAlg::Vec<float_v, TracksInfo::NumPosDirPars>{state.x(), state.y(), state.z(), state.tx(),
                                                                     state.ty()};
      }
      [[nodiscard, gnu::always_inline]] auto StatePos( std::size_t i ) const {
        auto const state = this->template get<Tag::States>( i );
        return LHCb::LinAlg::Vec<float_v, 3>{state.x(), state.y(), state.z()};
      }
      [[nodiscard, gnu::always_inline]] auto StateDir( std::size_t i ) const {
        auto const state = this->template get<Tag::States>( i );
        return LHCb::LinAlg::Vec<float_v, 3>{state.tx(), state.ty(), 1.f};
      }
      // Retrieve the (sorted) set of LHCbIDs
      [[nodiscard, gnu::always_inline]] std::vector<LHCbID> lhcbIDs() const {
        static_assert( width() == 1, "lhcbIDs() method cannot be used on vector proxies" );
        std::vector<LHCbID> ids;
        ids.reserve( nHits().cast() );
        for ( auto i = 0; i < nVPHits().cast(); i++ ) { ids.emplace_back( vp_lhcbID( i ).LHCbID() ); }
        for ( auto i = 0; i < nUTHits().cast(); i++ ) { ids.emplace_back( ut_lhcbID( i ).LHCbID() ); }
        for ( auto i = 0; i < nFTHits().cast(); i++ ) { ids.emplace_back( ft_lhcbID( i ).LHCbID() ); }
        std::sort( ids.begin(), ids.end() );
        return ids;
      }

      [[nodiscard, gnu::always_inline]] friend auto slopes( const LongProxy& p ) { return p.StateDir( 0 ); }
      [[nodiscard, gnu::always_inline]] friend auto threeMomentum( const LongProxy& p ) {
        auto s = slopes( p );
        return s * ( p.p() / s.mag() );
      }
      [[nodiscard, gnu::always_inline]] friend auto referencePoint( const LongProxy& p ) { return p.StatePos( 0 ); }
      [[nodiscard, gnu::always_inline]] friend auto trackState( const LongProxy& p ) {
        return p.template get<Tag::States>( 0 );
      }
      // flag which indicates client code can go for 'threeMomCovMatrix', `momPosCovMatrix` and `posCovMatrix` and not
      // for a track-like stateCov -- possible values: yes (for track-like objects ) no (for neutrals, composites),
      // maybe (particle, check at runtime, calls may return invalid results)
      static constexpr auto canBeExtrapolatedDownstream = Event::CanBeExtrapolatedDownstream::yes;
    };

    template <SIMDWrapper::InstructionSet simd, ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = LongProxy<simd, behaviour, ContainerType>;

  private:
    Velo::Tracks const*     m_velo_ancestors{nullptr};
    Upstream::Tracks const* m_upstream_ancestors{nullptr};
    Seeding::Tracks const*  m_seed_ancestors{nullptr};
    History                 m_history{};
  };
} // namespace LHCb::Pr::Long
