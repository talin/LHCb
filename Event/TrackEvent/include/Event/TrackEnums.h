/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "Kernel/meta_enum.h"
#include <string>
#include <vector>

namespace LHCb::Event::Enum::Track {

  meta_enum_class( History, int,
                   // numbers are (almost!) consistent with LHCb::Event::v1::Track::History
                   Unknown              = 0,  // unknown history (i.e. history not set)
                   TrackIdealPR         = 1,  // track produced with the ideal pattern recognition
                   MuonID               = 10, // track produced with the Muon pattern recognition
                   PrForward            = 30, // track produced with the PrForward pattern recognition
                   PrSeeding            = 31, // track produced with the PrSeeding pattern recognition
                   PrMatch              = 32, // track produced with the PrMatch pattern recognition
                   PrDownstream         = 33, // track produced with the PrDownstream pattern recognition
                   PrVeloUT             = 34, // track produced with the PrVeloUT pattern recognition
                   SciFiTrackForwarding = 35, // track produced with the SciFiTrackForwarding pattern recognition
                   PrPixel              = 36, // track produced with the VeloClusterTrackingSIMD pattern recognition
                   Last )

      /// Track fit history enumerations
      meta_enum_class( FitHistory, int,
                       Unknown = 0,      // track not fitted yet (fit history not set)
                       StdKalman,        // track fitted with the standard Kalman fitter
                       BiKalmanm, Last ) // track fitted with the bi-directional Kalman fitter

      /// Track type enumerations, note that these are not consecutive numbers (to not break downstream code)
      meta_enum_class( Type, int,
                       Unknown       = 0,         // track of undefined type
                       Velo          = 1,         // VELO track
                       VeloBackward  = 2,         // VELO backward track
                       Long          = 3,         // forward track
                       Upstream      = 4,         // upstream track
                       Downstream    = 5,         // downstream track
                       Ttrack        = 6,         // seed track
                       Muon          = 7,         // muon track
                       UT            = 10,        // UT track
                       SeedMuon      = 11,        // SeedMuon track
                       VeloMuon      = 12,        // VeloMuon track
                       MuonUT        = 13,        // MuonUT track
                       LongMuon      = 14,        // LongMuon track
                       FittedForward = 15, Last ) // FittedForward track

      /// Track pattern recognition status flag enumerations: The flag specifies in which state of the pattern
      /// recognition phase the track is. The status flag is set by the relevant algorithms
      meta_enum_class( PatRecStatus, int,
                       Unknown = 0,       // track in an undefined PR status
                       PatRecIDs,         // pattern recognition track with LHCbIDs
                       PatRecMeas, Last ) // pattern recognition track with Measurements added

      /// Track fitting status flag enumerations: The flag specifies in which state of the fitting phase the track
      /// is. The status flag is set by the relevant algorithms
      meta_enum_class( FitStatus, int,
                       Unknown = 0,      // track in an undefined fitting status
                       Fitted,           // fitted track
                       FitFailed, Last ) // track for which the track fit failed

      /// Track general flag enumerations
      meta_enum_class( Flag, int,
                       Unknown     = 0,      //
                       Invalid     = 2,      // invalid track for physics
                       Clone       = 4,      // clone track (of a corresponding unique track)
                       Used        = 8,      //
                       IPSelected  = 16,     //
                       PIDSelected = 32,     //
                       Selected    = 64, Last ) //

      /// Additional information assigned to this Track by pattern recognition
      meta_enum_class( AdditionalInfo, int,
                       Unknown         = 0,  //
                       PatQuality      = 2,  // Quality variable from PrForward Tracking
                       Cand1stQPat     = 3,  //  Quality of the first candidate
                       Cand2ndQPat     = 4,  //  Quality of the second candidate
                       NCandCommonHits = 5,  //  NCand with common hits
                       Cand1stChi2Mat  = 6,  //  Chi2 of the first candidate
                       Cand2ndChi2Mat  = 7,  //  Chi2 of the second candidate
                       MatchChi2       = 16, // Chi2 from the velo-seed matching (TrackMatching)
                       FitVeloChi2     = 17, // Chi2 of the velo segment (from TrackFitResult)
                       FitVeloNDoF     = 18, // NDoF of the velo segment chisq
                       FitTChi2        = 19, // Chi2 of the T station segment (from TrackFitResult)
                       FitTNDoF        = 20, // NDoF of the T station segment chisq
                       FitMatchChi2    = 21, // Chi2 of the breakpoint between T and TT (from TrackFitResult)
                       FitMuonChi2     = 22,
                       NUTOutliers     = 25, // Number of UT outlier hits (from TrackFitResult)
                       TsaLikelihood   = 32, // Likelihood from tsa seeding
                       CloneDist = 101, // Track is flagged as being a (rejected) clone of another track. Value is the
                                        // KL clone distance
                       nPRVeloRZExpect    = 103, // Number of expected Velo clusters from VELO RZ pattern recognition
                       nPRVelo3DExpect    = 104, // Number of expected Velo clusters from VELO 3D pattern recognition
                       MuonChi2perDoF     = 300, // Chi2/nDoF of muon track fit
                       MuonMomentumPreSel = 301, // 1 if pass Momentum pre-selection, 0 if not
                       MuonInAcceptance   = 302, // 1 if in Muon system InAcceptance, 0 if not
                       IsMuonLoose        = 303, // 1 if pass IsMuonLoose criteria, 0 if not
                       IsMuon             = 304, // 1 if pass IsMuon criteria, 0 if not
                       MuonDist2          = 305, // Squared distance of the closest muon hit to the extrapolated track
                       MuonDLL            = 306, // DLL (from muon system only)
                       MuonNShared = 307,   // NShared (number of additional IsMuon tracks with at least one shared hit
                                            // with the current track and a smaller Dist value)
                       MuonCLQuality = 308, // CLQuality
                       MuonCLArrival = 309, // CLArrival
                       IsMuonTight   = 310, // 1 if pass IsMuonTight criteria, 0 if not
                       Last )

}

namespace Gaudi::Parsers {
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::History>&, const std::string& );
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::FitHistory>&, const std::string& );
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::Type>&, const std::string& );
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::PatRecStatus>&, const std::string& );
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::FitStatus>&, const std::string& );
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::Flag>&, const std::string& );
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::AdditionalInfo>&, const std::string& );
} // namespace Gaudi::Parsers
