/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/Track_v3.h"
#include "Event/UniqueIDGenerator.h"

namespace LHCb::Event::v3 {
  /** Helper for tests and benchmarks that generates a
   *  LHCb::Event::v3::Tracks container that is populated with
   *  fake-but-sane data.
   */
  Tracks generate_tracks( std::size_t nTracks, LHCb::UniqueIDGenerator const&, unsigned int seed = 0,
                          Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(),
                          Tracks::allocator_type   alloc         = {} );
} // namespace LHCb::Event::v3
