/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "Event/SIMDEventTypes.h"
#include "Event/SOACollection.h"
#include "Event/State.h"
#include "Kernel/EventLocalAllocator.h"
#include "PrTracksTag.h"

/**
 * Track data for exchanges between FT and Fit
 *
 * @author: Michel De Cian (copying from Arthur Hennequin's Track classes)
 * @date:   2020-05-07
 * 2020-08-07 updated by Peilian Li according to Olli's Composites struct
 */

/**
 * This file is also an example on how to use SOACollections.
 *
 * An SOACollection is a container of muliple fields in Struct of Array layout.
 * The minimal code to define it is as follow:
 *
 * ```cpp
 * namespace MyNamespace {
 *   struct Tag {
 *     struct MyFirstTag : LHCb::Event::int_field {}; // define a single integer tag
 *     struct MyArrayTag : LHCb::Event::floats_field<10> {}; // define an array of 10 floats
 *
 *     // Create the SOACollection baseclass with the tags defined above:
 *     template <typename T>
 *     using my_struct_t = LHCb::Event::SOACollection<T, MyFirstTag, MyArrayTag>
 *   };
 *
 *   // Declare the new container type, inheriting from the SOACollection using CRTP
 *   struct MyStruct : Tag::my_struct_t<MyStruct> {
 *     using base_t = typename Tag::my_struct_t<MyStruct>;
 *     using base_t::base_t;
 *   };
 * }
 *
 * ```
 *
 * This structure will comme with all the SOACollections features and a default proxy that will allow to
 * read and write the fields.
 *
 * ```cpp
 * // Iterate on the container:
 * for (auto proxy : mystruct.scalar()) {
 *   auto a = proxy.get<MyNamespace::Tag::MyFirstTag>();
 *   auto b = proxy.get<MyNamespace::Tag::MyArrayTag>( 0 ); // get the first element of the array
 * }
 * ```
 *
 * ```cpp
 * // Add some elements at the end
 * auto proxy = mystruct.emplace_back<SIMDWrapper::InstructionSet::AVX2>(); // emplace 8 objects at a time
 * proxy.field<MyNamespace::Tag::MyFirstTag>().set( 42 );
 * proxy.field<MyNamespace::Tag::MyArrayTag>( 0 ).set( 2.f ); // fill the first element of the array
 * ```
 *
 * Additionally, an optional custom proxy can be defined. It is needed when the proxy must define a function
 * with a specific name, eg in the functors. This proxy must be defined in the container struct and override
 * the proxy_type property:
 *
 * ```cpp
 * // Define an optional custom proxy for this track
 * template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
 * struct CustomProxy : LHCb::Event::Proxy<simd, behaviour, ContainerType> {
 *    // use default constructor
 *    using LHCb::Event::Proxy<simd, behaviour, ContainerType>::Proxy;
 *
 *    // declare custom functions
 *    [[nodiscard]] auto customFunction() const {
 *      return this->template get<MyNamespace::Tag::MyFirstTag>() + 5; // can include custom computations
 *    }
 * };
 *
 * // Register the proxy:
 * template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
 * using proxy_type = CustomProxy<simd, behaviour, ContainerType>;
 * ```
 *
 * When zipped with other containers' Proxy in a ZipProxy, the custom defined functions will be available as
 * function of the zip proxy. As a result, in case of collision of custom function names defined by different
 * Proxies, these functions will be unusable
 */

namespace LHCb::Pr::Seeding {

  struct Tag {
    struct Chi2PerDoF : Event::float_field {};
    struct Index : Event::int_field {};
    struct LHCbID : Event::lhcbid_field {};
    struct FTHits : Event::vector_field<Event::struct_field<Index, LHCbID>> {};
    struct States : Event::states_field<TracksInfo::NumSeedStates> {};

    template <typename T>
    using seed_t = Event::SOACollection<T, Chi2PerDoF, FTHits, States>;
  };

  namespace SP = LHCb::Event::StateParameters;

  struct Tracks : Tag::seed_t<Tracks> {
    using base_t = typename Tag::seed_t<Tracks>;
    using tag_t  = Tag; // Needed for tag_type_t helper defined in PrTracksTag.h
    using base_t::base_t;

    constexpr static auto NumSeedStates = TracksInfo::NumSeedStates;
    constexpr static auto MaxFTHits     = TracksInfo::MaxFTHits;

    // Define an optional custom proxy for this track
    template <SIMDWrapper::InstructionSet simd, ProxyBehaviour behaviour, typename ContainerType>
    struct SeedProxy : Event::Proxy<simd, behaviour, ContainerType> {
      using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
      using base_t::base_t;
      using base_t::width;
      using simd_t  = SIMDWrapper::type_map_t<simd>;
      using float_v = typename simd_t::float_v;

      [[nodiscard]] auto qOverP( std::size_t iState = 0 ) const {
        return this->template get<Tag::States>( iState ).qOverP();
      }
      [[nodiscard]] auto p() const { return abs( 1.0 / qOverP() ); }
      [[nodiscard]] auto chi2PerDoF() const { return this->template get<Tag::Chi2PerDoF>(); }
      [[nodiscard]] auto nHits() const { return this->template field<Tag::FTHits>().size(); }
      [[nodiscard]] auto nFTHits() const { return nHits(); }
      [[nodiscard]] auto ft_index( std::size_t i ) const {
        return this->template field<Tag::FTHits>()[i].template get<Tag::Index>();
      }
      [[nodiscard]] auto ft_lhcbID( std::size_t i ) const {
        return this->template field<Tag::FTHits>()[i].template get<Tag::LHCbID>();
      }

      // Retrieve state info
      [[nodiscard, gnu::always_inline]] auto StatePosDir( std::size_t i ) const {
        return this->template get<Tag::States>( i );
      }

      [[nodiscard, gnu::always_inline]] auto StatePos( std::size_t i ) const {
        auto state = StatePosDir( i );
        return Vec3<float_v>( state.x(), state.y(), state.z() );
      }
      [[nodiscard, gnu::always_inline]] auto StateDir( std::size_t i ) const {
        auto state = StatePosDir( i );
        return Vec3<float_v>( state.tx(), state.ty(), 1.f );
      }

      [[nodiscard]] LHCb::State getLHCbState( std::size_t iState ) const {
        auto              v = this->template get<Tag::States>( iState );
        LHCb::StateVector sv{
            {v.x().cast(), v.y().cast(), v.z().cast()}, {v.tx().cast(), v.ty().cast(), 1.f}, qOverP( iState ).cast()};

        LHCb::State state;
        state.setState( sv );
        state.setLocation( LHCb::State::Location::AtT );

        return state;
      }

      //  Retrieve the (sorted) set of LHCbIDs
      [[nodiscard]] std::vector<LHCbID> lhcbIDs() const {
        static_assert( width() == 1, "lhcbIDs() method cannot be used on vector proxies" );
        std::vector<LHCbID> ids;
        ids.reserve( nFTHits().cast() );
        for ( auto i = 0; i < nFTHits().cast(); i++ ) { ids.emplace_back( ft_lhcbID( i ).LHCbID() ); }
        std::sort( ids.begin(), ids.end() );
        return ids;
      }
    };
    template <SIMDWrapper::InstructionSet simd, ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = SeedProxy<simd, behaviour, ContainerType>;
  };
} // namespace LHCb::Pr::Seeding
