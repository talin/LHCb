/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "FTDAQ/FTInfo.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/LHCbID.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Utils.h"

#include "Detector/FT/FTChannelID.h"

#include <algorithm>
#include <iterator>
#include <utility>
#include <vector>

/**
 *  @brief Namespace for code related to SciFi hit container
 */
namespace LHCb::Pr::FT {

  using simd = SIMDWrapper::best::types;
  /**
   * @brief increase n such that it's a multiple of simd::size
   * @param n size to increase
   * @returns size as multiple of simd::size
   */
  inline constexpr int align_size( int n ) { return ( n / simd::size + 1 ) * simd::size; }

  inline constexpr auto nan = std::numeric_limits<float>::signaling_NaN();

  namespace details {

    /**
     *  @brief SoA container for SciFi hits
     */
    struct Hits {

      using allocator_type = LHCb::Allocators::EventLocal<float>;
      struct HotInfo {
        float w{nan};
        float dzDy{nan};
        float dxDy{nan};
        float z0{nan};
      };
      struct ColdInfo {
        LHCb::Detector::FTChannelID id{};
        float                       yMin{nan};
        float                       yMax{nan};
      };

      constexpr static auto maxNumberOfHits = align_size( LHCb::Detector::FT::maxNumberHits );
      /**
       * 24 zones plus 2 extra entries for padding
       * the index in the array corresponds to the zone,
       * i.e. zoneIndexes[0] gives the start index of zone 0
       * upper zones have uneven, lower zones have even numbers
       */
      constexpr static std::size_t paddedSize = LHCb::Detector::FT::NFTZones + 2;

      Hits( allocator_type alloc = {}, std::size_t size = maxNumberOfHits )
          : m_planeCodes{alloc}, m_x{alloc}, m_hotHitPart{alloc}, m_coldHitPart{alloc} {
        m_planeCodes.reserve( size );
        m_x.reserve( size );
        m_hotHitPart.reserve( size );
        m_coldHitPart.reserve( size );
      }

      Hits( Hits&& )      = default;
      Hits( const Hits& ) = delete;

      /**
       *  @brief Push back values to all storage fields, i.e. populate a column of values.
       */
      Hits& appendColumn( uint8_t pc, float x, HotInfo&& hot, ColdInfo&& cold ) {
        m_planeCodes.push_back( pc );
        m_x.push_back( x );
        m_hotHitPart.push_back( hot );
        m_coldHitPart.push_back( cold );
        return *this;
      }
      /**
       *  @brief Set beginning of zone to index.
       */
      Hits& setZoneIndex( int zone, int index ) {
        m_zoneIndexes[zone] = index;
        return *this;
      }

      /**
       * accessors for all above defined variables
       * the vectors can also be accessed directly as they are public
       */
      ColdInfo coldHitInfo( int index ) const {
        assert( index < static_cast<int>( size() ) );
        return m_coldHitPart[index];
      };
      HotInfo hotHitInfo( int index ) const {
        assert( index < static_cast<int>( size() ) );
        return m_hotHitPart[index];
      };
      LHCb::Detector::FTChannelID id( int index ) const {
        assert( index < static_cast<int>( size() ) );
        return m_coldHitPart[index].id;
      }
      LHCb::LHCbID lhcbid( int index ) const {
        assert( index < static_cast<int>( size() ) );
        return LHCb::LHCbID{id( index )};
      }
      template <typename F>
      F x( int index ) const {
        assert( index < static_cast<int>( size() ) );
        return F{m_x.data() + index};
      }
      float x( int index ) const {
        assert( index < static_cast<int>( size() ) );
        return m_x[index];
      }
      float z( int index ) const {
        assert( index < static_cast<int>( size() ) );
        return m_hotHitPart[index].z0;
      }
      unsigned planeCode( int index ) const {
        assert( index < static_cast<int>( size() ) );
        return m_planeCodes[index];
      }
      float w( int index ) const {
        assert( index < static_cast<int>( size() ) );
        return m_hotHitPart[index].w;
      }
      float dzDy( int index ) const {
        assert( index < static_cast<int>( size() ) );
        return m_hotHitPart[index].dzDy;
      }
      float dxDy( int index ) const {
        assert( index < static_cast<int>( size() ) );
        return m_hotHitPart[index].dxDy;
      }
      auto yEnd( int index ) const {
        assert( index < static_cast<int>( size() ) );
        return std::pair{m_coldHitPart[index].yMin, m_coldHitPart[index].yMax};
      }
      /**
       * @brief method to access zone indices
       * @param zone zone number as assigned in FTConstants.h
       * @returns std::pair containing the start and end index of the zone
       * @details A sentinel is placed at the end of each zone. Therefore decrement end index by 1.
       */
      auto getZoneIndices( int zone ) const { return std::pair{m_zoneIndexes[zone], m_zoneIndexes[zone + 2] - 1}; }
      /**
       *  @brief Get start index of zone.
       */
      auto getZoneIndex( int zone ) const { return m_zoneIndexes[zone]; }

      float distanceXHit( int index, float x_track ) const { return x( index ) - x_track; }
      float z( int index, float y ) const { return z( index ) + y * dzDy( index ); }
      float x( int index, float y ) const { return x( index ) + y * dxDy( index ); }
      float distance( int index, float x_track, float y_track ) const { return x( index, y_track ) - x_track; }
      float yOnTrack( int index, float y0, float dyDz ) const {
        return ( y0 + dyDz * z( index ) ) / ( 1.f - dyDz * dzDy( index ) );
      }

      /**
       * @brief get size of vectors in SciFiHits
       * @returns size of the _x vector as proxy for the others
       */
      size_t size() const {
        assert( m_x.size() == m_coldHitPart.size() );
        return m_x.size();
      }

      /**
       * @brief check if the container is empty
       * @returns boolean value of check for emptiness
       * @details Note: this does not check for size being equal to
       * zero! This is because it is expected (by other algorithms) that
       * the different zones of the SciFi detector are protected by sentinels
       * that are not real hits and are always put into the container, which thus
       * never has size == 0.
       */
      bool empty() const {
        for ( auto i{0}; i < LHCb::Detector::FT::NFTZones; ++i ) {
          const auto [start, end] = getZoneIndices( i );
          if ( start != end ) return false;
        }
        return true;
      }

      /**
       * @brief a faster binary search
       * @param N controls number of runrolled steps in the search
       * @param start first index of search interval
       * @param end last index of search interval (same behaviour as last iterator)
       * @param val value to compare elements to
       * @returns index of first element that is not less than val, or end if no such element is found
       */
      template <int N>
      [[gnu::always_inline]] auto get_lower_bound_fast( unsigned start, unsigned end, float value ) const {
        auto size = end - start;
        auto low  = start;
        LHCb::Utils::unwind<0, N>( [&]( int ) {
          auto half = size / 2;
          // this multiplication avoids branching
          low += ( m_x[low + half] < value ) * ( size - half );
          size = half;
        } );
        do {
          auto half = size / 2;
          low += ( m_x[low + half] < value ) * ( size - half );
          size = half;
        } while ( size > 0 );
        return low;
      }

      /**
       * @brief Convenient method to (linearly) search the index of a given LHCbID.
       * @returns index of hit with given LHCbID or size of the container if none found.
       */
      auto find_index_of( LHCb::LHCbID id ) const {
        assert( id.isFT() );
        const auto     ftid         = LHCb::Detector::FTChannelID{id.channelID()};
        constexpr auto uniqueOffset = LHCb::Detector::FT::NFTLayers / LHCb::Detector::FT::NFTStations;
        const auto     zone    = LHCb::Detector::FT::ZonesLower[ftid.globalLayerID() - uniqueOffset] + ftid.isTop();
        const auto     indices = getZoneIndices( zone );
        const auto     start   = std::next( m_coldHitPart.begin(), indices.first );
        const auto     end     = std::next( m_coldHitPart.begin(), indices.second );
        const auto     it      = std::find_if( start, end, [=]( auto hit ) { return ftid == hit.id; } );
        assert( it != end && "Id must be in hit container!" );
        return std::distance( m_coldHitPart.begin(), it );
      }

      /**
       * @brief Performs a full copy of the other container.
       *
       * @param other Another Hits<HitType::FT> Container
       * @return Hits&
       * @details To avoid accidental copies of potentially large hit containers their copy
       * constructors are deleted. This function provides an explicit way to still copy if
       * needed.
       */
      Hits& copy_from( const Hits& other ) {
        for ( size_t i{0}; i < other.size(); ++i ) {
          appendColumn( other.planeCode( i ), other.x( i ), other.hotHitInfo( i ), other.coldHitInfo( i ) );
        }
        for ( size_t i{0}; i < m_zoneIndexes.size(); ++i ) { setZoneIndex( i, other.getZoneIndex( i ) ); }
        return *this;
      }

    private:
      alignas( 64 ) std::array<int, paddedSize> m_zoneIndexes{};
      std::vector<uint8_t, std::allocator_traits<allocator_type>::rebind_alloc<uint8_t>>   m_planeCodes;
      std::vector<float, allocator_type>                                                   m_x;
      std::vector<HotInfo, std::allocator_traits<allocator_type>::rebind_alloc<HotInfo>>   m_hotHitPart;
      std::vector<ColdInfo, std::allocator_traits<allocator_type>::rebind_alloc<ColdInfo>> m_coldHitPart;
    };

  } // namespace details

  using PrSciFiHits [[deprecated]] = details::Hits;

} // namespace LHCb::Pr::FT
