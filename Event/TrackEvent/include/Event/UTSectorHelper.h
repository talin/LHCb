/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/PrTracksTag.h"
#include "Event/SOACollection.h"
#include "Kernel/STLExtensions.h"
#include "LHCbMath/SIMDWrapper.h"
#include "UTDAQ/UTDAQHelper.h"
#include "UTDAQ/UTInfo.h"
#include "UTTrackUtils.h"

namespace LHCb::UTDAQ {

  namespace {

    using simd = SIMDWrapper::best::types;

    // -- needed in several places to add 'dummy' sector numbers
    static const int paddingSectorNumber = 10000;

    const int totalUTLayers = static_cast<int>( UTInfo::DetectorNumbers::TotalLayers );

    //=============================================================================
    // -- Function to assure sanity of sector numbers
    //=============================================================================
    template <typename B, typename BTag>
    [[maybe_unused]] bool badSectorNumbers( const std::array<B, totalUTLayers>& bounds ) {
      for ( int iLay = 0; iLay < totalUTLayers; ++iLay ) {
        [[maybe_unused]] const int maxSectors = 2304;
        // With the new UT chID definition the sector number can go up to 2304 given the following equation
        //     ( ( ( ( ( ( side * static_cast<int>( UTInfo::DetectorNumbers::HalfLayers ) + halflayer ) *
        //                          static_cast<int>( UTInfo::DetectorNumbers::Staves ) ) +
        //                        stave ) *
        //                          static_cast<int>( UTInfo::DetectorNumbers::Faces ) +
        //                      face ) *
        //                    static_cast<int>( UTInfo::DetectorNumbers::Modules ) ) +
        //                  module ) *
        //                    static_cast<int>( UTInfo::DetectorNumbers::SubSectors ) +
        //                subsector;
        for ( auto proxy : bounds[iLay].scalar() ) {
          for ( int i = 0; i < proxy.template get<typename BTag::nPos>().cast(); ++i ) {
            auto sector = proxy.template get<typename BTag::sects>( i ).cast();
            assert( sector != paddingSectorNumber && "sector number is padding element" );
            assert( sector >= 0 && sector <= maxSectors && "Found out-of-bound UT sector" );
          }
        }
      }
      return true;
    }

    //=============================================================================
    // -- Helper for non-trivial index calculation
    //=============================================================================
    int boundsIndex( const int maxNumSectors, const int layer, const int offset ) {
      return maxNumSectors * layer + offset;
    }

    //=============================================================================
    // -- Helper for non-trivial position calculation
    //=============================================================================
    simd::int_v position( const simd::int_v positions, const int maxNumSectors, const int layer ) {
      return positions - maxNumSectors * layer;
    }

    //=============================================================================
    // -- Bubble sort
    //=============================================================================
    // -- bubble sort is slow, but we never have more than 9 elements (horizontally)
    // -- and can act on 8 elements at once vertically (with AVX)
    template <typename I, unsigned int maxNumSectors>
    void bubbleSortSIMD( const int maxColsMaxRows, std::array<I, maxNumSectors * totalUTLayers>& helper,
                         const int start ) {
      for ( int i = 0; i < maxColsMaxRows - 1; i++ ) {
        for ( int j = 0; j < maxColsMaxRows - i - 1; j++ ) {
          swap( helper[start + j] > helper[start + j + 1], helper[start + j], helper[start + j + 1] );
        }
      }
    }

    //=============================================================================
    // -- Make unique
    //=============================================================================
    // -- not sure that is the smartest solution
    // -- but I could not come up with anything better
    // -- inspired by: https://lemire.me/blog/2017/04/10/removing-duplicates-from-lists-quickly/
    template <typename I, unsigned int maxNumSectors>
    I makeUniqueSIMD( std::array<I, maxNumSectors * totalUTLayers>& out, int start, size_t len ) {
      if ( len == 0 ) return start;
      I pos  = start + 1;
      I oldv = out[start];
      for ( size_t j = start + 1; j < start + len; ++j ) {
        I newv = out[j];
        for ( size_t k = start + 1; k < start + len; ++k ) { out[k] = select( pos == k, newv, out[k] ); }
        pos  = pos + select( newv == oldv, I{0}, I{1} );
        oldv = newv;
      }

      // -- Need to decrease pos by one if the last element is the padding one
      for ( size_t k = start; k < start + len; ++k ) {
        pos = select( ( pos == k + 1 ) && out[k] == paddingSectorNumber, pos - 1, pos );
      }

      return pos;
    }

  } // namespace

  //=============================================================================
  // -- Extrapolate to each layer to find all sectors
  //=============================================================================
  template <typename B, typename BTag, int maxNumRows, int maxNumCols, typename ExtrapFunc>
  __attribute__( ( flatten ) ) auto findAllSectorsExtrap( LHCb::UT::TrackUtils::MiniStates& filteredStates,
                                                          const GeomCache& geom, ExtrapFunc extrapolate,
                                                          int minLayers = totalUTLayers - 1 ) {
    constexpr int maxNumSectors = maxNumRows * maxNumCols;

    // -- create the structure for the boundaries
    auto      bounds   = LHCb::make_object_array<B, totalUTLayers>( Zipping::generateZipIdentifier(),
                                                             filteredStates.get_allocator().resource() );
    const int contSize = filteredStates.size();
    std::for_each( bounds.begin(), bounds.end(), [contSize]( auto& b ) { b.reserve( contSize ); } );
    filteredStates.resize( 0 );

    // -- helper structures
    // -- end positions of bounds, for each layer
    std::array<simd::int_v, totalUTLayers> positions{};
    // -- 4 layers x maximum number of sectors
    std::array<simd::int_v, maxNumSectors * totalUTLayers> boundsHelper{};
    // -- maximum of cols*rows
    std::array<int, totalUTLayers> maxColsRows;

    // -- Main loop over all filtered states
    for ( int t = 0; t < contSize; t += simd::size ) {
      auto loopMask = simd::loop_mask( t, contSize );

      simd::int_v nLayers{0};

      boundsHelper.fill( paddingSectorNumber );

      const auto fState = filteredStates.simd()[t];
      const auto x      = fState.get<LHCb::UT::TrackUtils::MiniStateTag::State>().x();
      const auto y      = fState.get<LHCb::UT::TrackUtils::MiniStateTag::State>().y();
      const auto z      = fState.get<LHCb::UT::TrackUtils::MiniStateTag::State>().z();
      const auto tx     = fState.get<LHCb::UT::TrackUtils::MiniStateTag::State>().tx();
      const auto ty     = fState.get<LHCb::UT::TrackUtils::MiniStateTag::State>().ty();
      const auto qop    = fState.get<LHCb::UT::TrackUtils::MiniStateTag::State>().qOverP();

      // -- loop over all 4 layers of the UT to collect the sectors per layer
      for ( int layerIndex = 0; layerIndex < totalUTLayers; ++layerIndex ) {

        const simd::int_v regionBoundary1 = ( 2 * geom.layers[layerIndex].nColsPerSide + 3 );
        const simd::int_v regionBoundary2 = ( 2 * geom.layers[layerIndex].nColsPerSide - 5 );

        // -- Get the positions of the extrapolated states
        const auto [xLayer, yLayer, xTol, yTol] = extrapolate( layerIndex, x, y, z, tx, ty, qop );

        simd::int_v subcolmin{0};
        simd::int_v subcolmax{0};
        simd::int_v subrowmin{0};
        simd::int_v subrowmax{0};

        // --------------------------
        simd::mask_v mask = findSectors( layerIndex, xLayer, yLayer, xTol, yTol, geom.layers[layerIndex], subcolmin,
                                         subcolmax, subrowmin, subrowmax );

        const simd::mask_v gathermask = loopMask && mask;

        // -- Determine the maximum number of rows and columns we have to take into account
        // -- maximum 3, minimum 0
        // -- The 'clamp' is needed to prevent large negative values from 'hmax' when gathermask has no true entries
        const int maxCols = std::clamp( ( subcolmax - subcolmin ).hmax( gathermask ) + 1, 0, maxNumCols );
        const int maxRows = std::clamp( ( subrowmax - subrowmin ).hmax( gathermask ) + 1, 0, maxNumRows );

        maxColsRows[layerIndex] = maxCols * maxRows;

        // -- Loop over rows and columns
        // -- This leads to duplications of sectors
        // -- which is dealed with later
        int counter = 0;
        for ( int sc = 0; sc < maxCols; sc++ ) {

          const simd::int_v realSC = min( subcolmax, subcolmin + sc );

          for ( int sr = 0; sr < maxRows; sr++ ) {

            const simd::int_v realSR = min( subrowmax, subrowmin + sr );
            const simd::int_v sectorIndex =
                realSR + static_cast<int>( UTInfo::SectorNumbers::EffectiveSectorsPerColumn ) * realSC;

            // -- only gather when we are not outside the acceptance
            // -- if we are outside, fill 1. This will be overwritten by paddingSectorNumber later
            const simd::int_v sect =
                ( layerIndex < 2 ) ? geom.sectorLUT.maskgather_station1<simd::int_v>( sectorIndex, gathermask, 1 )
                                   : geom.sectorLUT.maskgather_station2<simd::int_v>( sectorIndex, gathermask, 1 );

            // -- ID is: sectorIndex (from LUT) + (layerIndex * 3 + region - 1 ) * 98
            // -- The regions are already calculated with a -1
            const int idx     = boundsIndex( maxNumSectors, layerIndex, counter );
            boundsHelper[idx] = select( gathermask, sectorFullID( layerIndex, realSC, realSR ), paddingSectorNumber );
            counter++;
          }
        }

        // -- This is sorting
        bubbleSortSIMD<simd::int_v, maxNumSectors>( maxCols * maxRows, boundsHelper, maxNumSectors * layerIndex );
        // -- This is uniquifying
        positions[layerIndex] =
            makeUniqueSIMD<simd::int_v, maxNumSectors>( boundsHelper, maxNumSectors * layerIndex, maxCols * maxRows );
        // -- count the number of layers which are 'valid'
        nLayers += gathermask;
      }

      // -- We need at least three layers
      const simd::mask_v compressMask = ( nLayers > ( minLayers - 1 ) ) && loopMask;

      if ( none( compressMask ) ) continue;

      // -- All information is now in 'boundsHelper', 'positions' and 'maxColsRows'
      // -- Now compress it back into 'bounds'
      for ( int iLayer = 0; iLayer < totalUTLayers; ++iLayer ) {
        auto compBounds = bounds[iLayer].template compress_back<SIMDWrapper::InstructionSet::Best>( compressMask );

        const simd::int_v pos = position( positions[iLayer], maxNumSectors, iLayer );
        compBounds.template field<typename BTag::nPos>().set( pos );

        // -- make sure that the position is never beyond the maximum number of sectors
        assert( none( maxColsRows[iLayer] < pos ) && "count more UT sectors than allowed" );

        // -- Go 1 beyond the end to add a '0', needed for reading out the sectors later.
        // -- Fill value when it is before the end, otherwise fill 0
        for ( int iSector = 0; iSector < maxColsRows[iLayer]; ++iSector ) {
          const int bIdx = boundsIndex( maxNumSectors, iLayer, iSector );
          compBounds.template field<typename BTag::sects>( iSector ).set(
              select( iSector < pos, boundsHelper[bIdx], paddingSectorNumber ) );
        }
        const auto xTol = std::get<2>( extrapolate( iLayer, x, y, z, tx, ty, qop ) );
        compBounds.template field<typename BTag::xTol>().set( xTol );
      }

      // -- Now need to compress the filtered states, such that they are
      // -- in sync with the sectors
      filteredStates.copy_back<simd>( filteredStates, t, compressMask );
    }

    assert( ( badSectorNumbers<B, BTag>( bounds ) ) );

    return bounds;
  }
} // namespace LHCb::UTDAQ
