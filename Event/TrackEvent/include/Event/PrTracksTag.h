/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/SOACollection.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "LHCbMath/MatVec.h"

namespace LHCb::Event::StateParameters {

  enum struct StateVector { x, y, tx, ty, qOverP, z };

  enum struct CovMatrix {
    x_x,
    x_y,
    x_tx,
    x_ty,
    x_qOverP,
    y_y,
    y_tx,
    y_ty,
    y_qOverP,
    tx_tx,
    tx_ty,
    tx_qOverP,
    ty_ty,
    ty_qOverP,
    qOverP_qOverP
  };

  constexpr unsigned int operator+( unsigned int i, StateVector s ) { return i + static_cast<unsigned>( s ); }

  constexpr unsigned int operator+( unsigned int i, CovMatrix s ) { return i + static_cast<unsigned>( s ); }
} // namespace LHCb::Event::StateParameters

namespace LHCb::Event::PosDirParameters {

  enum struct PosDirVector { x, y, z, tx, ty };

  constexpr unsigned int operator+( unsigned int i, PosDirVector s ) { return i + static_cast<unsigned>( s ); }

} // namespace LHCb::Event::PosDirParameters

namespace LHCb::Event {
  /**
   * Tag and Proxy baseclasses for representing a 6 parameters state vector (x, y, tx, ty, q/p, z)
   */
  struct state_field : floats_field<6> {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct NDStateProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using type          = typename simd_t::float_v;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type =
          NumericProxy<Simd, Behaviour, ContainerTypeBase, typename Path::template extend_t<std::size_t>, float>;

      NDStateProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      template <StateParameters::StateVector i>
      [[nodiscard, gnu::always_inline]] constexpr auto proxy() const {
        return proxy_type{m_container, m_offset, m_path.append( static_cast<std::size_t>( i ) )};
      }

      [[nodiscard, gnu::always_inline]] constexpr auto x() const {
        return proxy<StateParameters::StateVector::x>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto y() const {
        return proxy<StateParameters::StateVector::y>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto tx() const {
        return proxy<StateParameters::StateVector::tx>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto ty() const {
        return proxy<StateParameters::StateVector::ty>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto qOverP() const {
        return proxy<StateParameters::StateVector::qOverP>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto z() const {
        return proxy<StateParameters::StateVector::z>().get();
      }

      [[gnu::always_inline]] constexpr decltype( auto ) setPosition( type const& x, type const& y, type const& z ) {
        proxy<StateParameters::StateVector::x>().set( x );
        proxy<StateParameters::StateVector::y>().set( y );
        proxy<StateParameters::StateVector::z>().set( z );
        return *this;
      }

      template <typename F>
      [[gnu::always_inline]] constexpr decltype( auto ) setPosition( LHCb::LinAlg::Vec<F, 3> const& pos ) {
        proxy<StateParameters::StateVector::x>().set( pos.x() );
        proxy<StateParameters::StateVector::y>().set( pos.y() );
        proxy<StateParameters::StateVector::z>().set( pos.z() );
        return *this;
      }

      [[gnu::always_inline]] constexpr decltype( auto ) setDirection( type const& tx, type const& ty ) {
        proxy<StateParameters::StateVector::tx>().set( tx );
        proxy<StateParameters::StateVector::ty>().set( ty );
        return *this;
      }

      template <typename F>
      [[gnu::always_inline]] constexpr decltype( auto ) setDirection( LHCb::LinAlg::Vec<F, 3> const& dir ) {
        proxy<StateParameters::StateVector::tx>().set( dir.x() );
        proxy<StateParameters::StateVector::ty>().set( dir.y() );
        return *this;
      }

      [[gnu::always_inline]] constexpr decltype( auto ) setQOverP( type const& qOverP ) {
        proxy<StateParameters::StateVector::qOverP>().set( qOverP );
        return *this;
      }

      [[nodiscard, gnu::always_inline]] constexpr auto get() const { return *this; }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = NDStateProxy<Simd, Behaviour, ContainerType, Path>;
  };
  template <std::size_t... N>
  using states_field = ndarray_field<state_field, N...>;

  /**
   * Tag and Proxy baseclasses for representing a position (x,y,z) and a direction (tx, ty), no momentum
   */
  struct pos_dir_field : floats_field<5> {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct NDStateProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using type          = typename simd_t::float_v;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type =
          NumericProxy<Simd, Behaviour, ContainerTypeBase, typename Path::template extend_t<std::size_t>, float>;

      NDStateProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      template <PosDirParameters::PosDirVector i>
      [[nodiscard, gnu::always_inline]] constexpr auto proxy() const {
        return proxy_type{m_container, m_offset, m_path.append( static_cast<std::size_t>( i ) )};
      }

      [[nodiscard, gnu::always_inline]] constexpr auto x() const {
        return proxy<PosDirParameters::PosDirVector::x>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto y() const {
        return proxy<PosDirParameters::PosDirVector::y>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto tx() const {
        return proxy<PosDirParameters::PosDirVector::tx>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto ty() const {
        return proxy<PosDirParameters::PosDirVector::ty>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto z() const {
        return proxy<PosDirParameters::PosDirVector::z>().get();
      }

      [[gnu::always_inline, gnu::flatten]] constexpr decltype( auto ) setPosition( type const& x, type const& y,
                                                                                   type const& z ) {
        proxy<PosDirParameters::PosDirVector::x>().set( x );
        proxy<PosDirParameters::PosDirVector::y>().set( y );
        proxy<PosDirParameters::PosDirVector::z>().set( z );
        return *this;
      }

      template <typename F>
      [[gnu::always_inline, gnu::flatten]] constexpr decltype( auto )
      setPosition( LHCb::LinAlg::Vec<F, 3> const& pos ) {
        proxy<PosDirParameters::PosDirVector::x>().set( pos.x() );
        proxy<PosDirParameters::PosDirVector::y>().set( pos.y() );
        proxy<PosDirParameters::PosDirVector::z>().set( pos.z() );
        return *this;
      }

      [[gnu::always_inline, gnu::flatten]] constexpr decltype( auto ) setDirection( type const& tx, type const& ty ) {
        proxy<PosDirParameters::PosDirVector::tx>().set( tx );
        proxy<PosDirParameters::PosDirVector::ty>().set( ty );
        return *this;
      }

      template <typename F>
      [[gnu::always_inline, gnu::flatten]] constexpr decltype( auto )
      setDirection( LHCb::LinAlg::Vec<F, 3> const& dir ) {
        proxy<PosDirParameters::PosDirVector::tx>().set( dir.x() );
        proxy<PosDirParameters::PosDirVector::ty>().set( dir.y() );
        return *this;
      }

      [[gnu::always_inline, gnu::flatten]] constexpr decltype( auto ) set( type const& x, type const& y, type const& z,
                                                                           type const& tx, type const& ty ) {
        setPosition( x, y, z );
        setDirection( tx, ty );
        return *this;
      }

      [[nodiscard, gnu::always_inline]] constexpr auto get() const { return *this; }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = NDStateProxy<Simd, Behaviour, ContainerType, Path>;
  };
  template <std::size_t... N>
  using pos_dirs_field = ndarray_field<pos_dir_field, N...>;

  /**
   * Tag and Proxy baseclasses for representing a 5 X 5 state covariance matrix:
   *          | x  |  y | tx | ty | q/p|
   *         x|____|____|____|____|____|
   *         y|____|____|____|____|____|
   *        tx|____|____|____|____|____|
   *        ty|____|____|____|____|____|
   *       q/p|____|____|____|____|____|
   * The matrices are stored as a flattened vector length 15 vector
   * (as the matrix is symmetric) in the following form:
   * (x_x, x_y, x_tx, x_ty, x_q/p, y_y, y_tx, y_ty, y_q/p, tx_tx, tx_ty, tx_q/p, ty_ty, ty_q/p, q/p_q/p)
   *
   */
  struct state_cov_field : floats_field<15> {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct StateCovProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using type          = typename simd_t::float_v;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type =
          NumericProxy<Simd, Behaviour, ContainerTypeBase, typename Path::template extend_t<std::size_t>, float>;

      StateCovProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      template <StateParameters::CovMatrix i>
      [[nodiscard, gnu::always_inline]] constexpr auto proxy() const {
        return proxy_type{m_container, m_offset, m_path.append( static_cast<std::size_t>( i ) )};
      }

      [[nodiscard, gnu::always_inline]] constexpr auto x_x() const {
        return proxy<StateParameters::CovMatrix::x_x>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto x_y() const {
        return proxy<StateParameters::CovMatrix::x_y>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto x_tx() const {
        return proxy<StateParameters::CovMatrix::x_tx>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto x_ty() const {
        return proxy<StateParameters::CovMatrix::x_ty>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto x_QoverP() const {
        return proxy<StateParameters::CovMatrix::x_qOverP>().get();
      }

      [[nodiscard, gnu::always_inline]] constexpr auto y_y() const {
        return proxy<StateParameters::CovMatrix::y_y>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto y_tx() const {
        return proxy<StateParameters::CovMatrix::y_tx>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto y_ty() const {
        return proxy<StateParameters::CovMatrix::y_ty>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto y_QoverP() const {
        return proxy<StateParameters::CovMatrix::y_qOverP>().get();
      }

      [[nodiscard, gnu::always_inline]] constexpr auto tx_tx() const {
        return proxy<StateParameters::CovMatrix::tx_tx>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto tx_ty() const {
        return proxy<StateParameters::CovMatrix::tx_ty>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto tx_QoverP() const {
        return proxy<StateParameters::CovMatrix::tx_qOverP>().get();
      }

      [[nodiscard, gnu::always_inline]] constexpr auto ty_ty() const {
        return proxy<StateParameters::CovMatrix::ty_ty>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto ty_QoverP() const {
        return proxy<StateParameters::CovMatrix::ty_qOverP>().get();
      }

      [[nodiscard, gnu::always_inline]] constexpr auto QoverP_QoverP() const {
        return proxy<StateParameters::CovMatrix::qOverP_qOverP>().get();
      }

      [[gnu::always_inline]] constexpr void setXCovariance( type const& x_x, type const& x_y, type const& x_tx,
                                                            type const& x_ty, type const& x_QoverP ) {
        proxy<StateParameters::CovMatrix::x_x>().set( x_x );
        proxy<StateParameters::CovMatrix::x_y>().set( x_y );
        proxy<StateParameters::CovMatrix::x_tx>().set( x_tx );
        proxy<StateParameters::CovMatrix::x_ty>().set( x_ty );
        proxy<StateParameters::CovMatrix::x_qOverP>().set( x_QoverP );
      }

      [[gnu::always_inline]] constexpr void setYCovariance( type const& y_y, type const& y_tx, type const& y_ty,
                                                            type const& y_QoverP ) {
        proxy<StateParameters::CovMatrix::y_y>().set( y_y );
        proxy<StateParameters::CovMatrix::y_tx>().set( y_tx );
        proxy<StateParameters::CovMatrix::y_ty>().set( y_ty );
        proxy<StateParameters::CovMatrix::y_qOverP>().set( y_QoverP );
      }

      [[gnu::always_inline]] constexpr void setTXCovariance( type const& tx_tx, type const& tx_ty,
                                                             type const& tx_QoverP ) {
        proxy<StateParameters::CovMatrix::tx_tx>().set( tx_tx );
        proxy<StateParameters::CovMatrix::tx_ty>().set( tx_ty );
        proxy<StateParameters::CovMatrix::tx_qOverP>().set( tx_QoverP );
      }

      [[gnu::always_inline]] constexpr void setTYCovariance( type const& ty_ty, type const& ty_QoverP ) {
        proxy<StateParameters::CovMatrix::ty_ty>().set( ty_ty );
        proxy<StateParameters::CovMatrix::ty_qOverP>().set( ty_QoverP );
      }

      [[gnu::always_inline]] constexpr void setQoverPCovariance( type const& QoverP_QoverP ) {
        proxy<StateParameters::CovMatrix::qOverP_qOverP>().set( QoverP_QoverP );
      }
      [[gnu::always_inline]] constexpr void set( type const& x_x, type const& x_y, type const& x_tx, type const& x_ty,
                                                 type const& x_QoverP, type const& y_y, type const& y_tx,
                                                 type const& y_ty, type const& y_QoverP, type const& tx_tx,
                                                 type const& tx_ty, type const& tx_QoverP, type const& ty_ty,
                                                 type const& ty_QoverP, type const& QoverP_QoverP ) {
        setXCovariance( x_x, x_y, x_tx, x_ty, x_QoverP );
        setYCovariance( y_y, y_tx, y_ty, y_QoverP );
        setTXCovariance( tx_tx, tx_ty, tx_QoverP );
        setTYCovariance( ty_ty, ty_QoverP );
        setQoverPCovariance( QoverP_QoverP );
      }

      [[gnu::always_inline]] constexpr auto& get() const { return *this; }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = StateCovProxy<Simd, Behaviour, ContainerType, Path>;
  };
  template <std::size_t... N>
  using state_covs_field = ndarray_field<state_cov_field, N...>;
} // namespace LHCb::Event

/*
 * Define maximum numbers of hits and states for Pr::Tracks which are detector specific
 */
namespace LHCb::Pr::TracksInfo {

  // The total layers of each sub-detector
  enum class DetLayers { VPLayers = 26, FTLayers = 12, UTLayers = 4 };

  // The maximum number of hits for PrLongTracks, depends on the pattern recognition
  inline constexpr std::size_t NumPosDirPars   = 5; // statePosVec vector(5D) (x,y,z,dx,dy)
  inline constexpr std::size_t NumSeedStates   = 3;
  inline constexpr std::size_t NumLongStates   = 2;
  inline constexpr std::size_t NumVeloStates   = 2;
  inline constexpr std::size_t NumCovXY        = 3;
  inline constexpr std::size_t NumVeloStateCov = NumCovXY * NumVeloStates;
  inline constexpr std::size_t MaxVPHits       = 52;
  inline constexpr std::size_t MaxUTHits       = 8;
  inline constexpr std::size_t MaxFTHits       = 12;
} // namespace LHCb::Pr::TracksInfo

// define a helper function to obtain the type of the Tag given the type of the tracks container
// e.g LHCb::Pr::Velo::Tracks --> LHCb::Pr::Velo::Tag
namespace LHCb::Pr {
  template <typename container>
  using tag_type_t = typename container::tag_t;
} // namespace LHCb::Pr
