/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "LHCbMath/SIMDWrapper.h"
#include <tuple>

#include "SOAData.h"

#include <boost/mp11/algorithm.hpp>

namespace LHCb::Pr {
  /** This enum is used to tag the data-loading and data-writing behaviour of a
   *  proxy object.
   *   -        Compress: write-only, proxy is initialised with an offset and a
   *                      mask, setters generate compress-stores operations using
   *                      the offset and mask.
   *   -      Contiguous: getters read contiguously from the given scalar offset,
   *                      setters write contiguously to the same offset.
   *   -      ScalarFill: read-only, getters read a single value from the given
   *                      scalar offset and return a SIMD vector filled with
   *                      copies of this value.
   *   -   ScatterGather: proxy is initialised with a SIMD vector of offsets,
   *                      getters gather from these offsets, setters scatter to
   *                      them [the scatter is not implemented yet].
   *   - ScatterGather2D: same as ScatterGather but supports multiple containers.
   */
  enum struct ProxyBehaviour { Compress, Contiguous, ScalarFill, ScatterGather, ScatterGather2D };

  namespace detail {
    template <ProxyBehaviour Behaviour, SIMDWrapper::InstructionSet Backend>
    using offset_t = std::conditional_t<
        Behaviour == ProxyBehaviour::Compress,
        // Compress
        std::tuple<int, typename SIMDWrapper::type_map_t<Backend>::mask_v>,
        std::conditional_t<
            Behaviour == ProxyBehaviour::Contiguous || Behaviour == ProxyBehaviour::ScalarFill,
            // Contiguous or ScalarFill
            int,
            std::conditional_t<Behaviour == ProxyBehaviour::ScatterGather,
                               // ScatterGather
                               std::tuple<typename SIMDWrapper::type_map_t<Backend>::int_v,
                                          typename SIMDWrapper::type_map_t<Backend>::mask_v>,
                               std::conditional_t<Behaviour == ProxyBehaviour::ScatterGather2D,
                                                  // ScatterGather2D
                                                  std::tuple<typename SIMDWrapper::type_map_t<Backend>::int_v,
                                                             typename SIMDWrapper::type_map_t<Backend>::int_v,
                                                             typename SIMDWrapper::type_map_t<Backend>::mask_v>,
                                                  // Someone added a new ProxyBehaviour and forgot to
                                                  // update offset_t
                                                  void>>>>;

    template <ProxyBehaviour Behaviour, SIMDWrapper::InstructionSet Backend, typename ContainerType>
    using container_t = std::conditional_t<Behaviour == ProxyBehaviour::ScatterGather2D,
                                           // ScatterGather2D
                                           std::array<ContainerType*, SIMDWrapper::type_map_t<Backend>::size>,
                                           // others
                                           ContainerType*>;
  } // namespace detail
} // namespace LHCb::Pr

/**
 * A proxy represent a slice of data of a certain width (defined by the simd).
 * It can be used to read / write or perform computation on the stored data.
 *
 * Three level of proxies are defined corresponding to the three levels of
 * containers:
 *
 * Zip           -> ZipProxy
 * SOACollection -> Proxy
 * Tag           -> NumericProxy
 *
 * Each container base class have an associated proxy base class that can
 * be used as default proxy or customized.
 */
namespace LHCb::Event {

  // Helper function to compute the validity mask of elements in a proxy
  template <LHCb::Pr::ProxyBehaviour Behaviour, typename simd_t, typename OffsetType, typename ContainerType>
  constexpr auto validity_mask( OffsetType offset, ContainerType container ) {
    // TODO: add path dependent validity
    if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Contiguous ) {
      return simd_t::loop_mask( offset, container->size() );
    } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScalarFill ) {
      return simd_t::mask_true();
    } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather ) {
      return std::get<1>( offset );
    } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather2D ) {
      return std::get<2>( offset );
    } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Compress ) {
      return std::get<1>( offset );
    }
  }

  // Helper function to compute the index of each element in a proxy
  template <LHCb::Pr::ProxyBehaviour Behaviour, typename simd_t, typename OffsetType>
  constexpr auto indices( OffsetType offset ) {
    if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScalarFill ) {
      return typename simd_t::int_v{offset};
    } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather ) {
      return std::get<0>( offset );
    } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather2D ) {
      return std::get<1>( offset );
    } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Contiguous ) {
      return simd_t::indices( offset );
    } else {
      static_assert( Behaviour == LHCb::Pr::ProxyBehaviour::Compress,
                     "New ProxyBehaviour was added but not implemented everywhere!" );
      static_assert( Behaviour != LHCb::Pr::ProxyBehaviour::Compress,
                     "indices() not implemented for write-only Compress proxy type" );
      // TODO what makes sense here?
      // What would make sense is to have an expansion of compressed indices.
      // ie if 00100111 is a mask, the indices should be ..1..234 where . means undefined values
      // this could be useful in case we want to record where the index in the compressed container
    }
  }

  /**
   * Proxy representing an N-Dimensional array of numerical data (ints or floats)
   * numeric_t : type of the scalar plain old data (eg int or float)
   * Unique : true if this proxy represents a field backed by a single value
   */
  template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
            typename Path, typename numeric_t, bool Unique = false>
  struct NumericProxy {
    using simd_t        = SIMDWrapper::type_map_t<Simd>;
    using type          = SIMDWrapper::vector_type_map_t<numeric_t, simd_t>;
    using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
    using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;

    NumericProxy( ContainerType container, OffsetType offset, Path path )
        : m_container( container ), m_offset( offset ), m_path( path ) {}

    [[nodiscard, gnu::always_inline]] inline constexpr auto get() const {
      if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather2D ) {
        auto const [i, j, mask] = m_offset;

        std::array<int, simd_t::size> i_v;
        i.store( i_v );

        type out{0};
        auto progressMask = mask;
        int  first        = ffs( progressMask ) - 1;
        while ( first >= 0 ) {
          auto gatherMask = ( i == i_v[first] ) && mask;
          auto base       = m_container[i_v[first]]->data( m_path );
          if constexpr ( Unique ) {
            out = select( gatherMask, type{*base}, out );
          } else {
            out = maskgather( base, j, gatherMask, out );
          }
          progressMask = progressMask && !gatherMask; // Update progress
          first        = ffs( progressMask ) - 1;
        }

        return out;
      } else {
        auto ptr = m_container->data( m_path );
        if constexpr ( Unique ) return type{*ptr};
        if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather ) {
          auto const [i, mask] = m_offset;
          return maskgather( ptr, i, mask, type{0} );
        } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScalarFill ) {
          return type{*std::next( ptr, m_offset )};
        } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Contiguous ) {
          return type{std::next( ptr, m_offset )};
        } else {
          static_assert( Behaviour == LHCb::Pr::ProxyBehaviour::Compress,
                         "New ProxyBehaviour was added but not implemented everywhere!" );
          static_assert( Behaviour != LHCb::Pr::ProxyBehaviour::Compress, "Compress proxies are write-only." );
        }
      }
    }

    // broadcast and store, allows to avoid permuting in compressstore when all elements are the same
    [[gnu::always_inline]] inline constexpr void set( numeric_t value ) {
      auto ptr = m_container->data( m_path );
      if constexpr ( Unique ) {
        *ptr = value;
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Contiguous ) {
        type{value}.store( std::next( ptr, m_offset ) );
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Compress ) {
        auto const start_index = std::get<0>( m_offset );
        type{value}.store( std::next( ptr, start_index ) );
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScalarFill ) {
        static_assert( Behaviour != LHCb::Pr::ProxyBehaviour::ScalarFill, "ScalarFill proxies are read-only." );
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather ) {
        auto const [i, mask] = m_offset;
        value.maskscatter( ptr, i, mask, type{value} );
      } else {
        static_assert( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather2D,
                       "New ProxyBehaviour was added but not implemented everywhere!" );
        static_assert( Behaviour != LHCb::Pr::ProxyBehaviour::ScatterGather2D,
                       "ScatterGather2D writing is not implemented yet." );
      }
    }

    [[gnu::always_inline]] inline constexpr void set( type value ) {
      static_assert( !Unique, "Setting multiple value to a unique field is not supported" );
      auto ptr = m_container->data( m_path );
      if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Contiguous ) {
        value.store( std::next( ptr, m_offset ) );
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Compress ) {
        auto const [start_index, mask] = m_offset;
        value.compressstore( mask, std::next( ptr, start_index ) );
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScalarFill ) {
        static_assert( Behaviour != LHCb::Pr::ProxyBehaviour::ScalarFill, "ScalarFill proxies are read-only." );
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather ) {
        auto const [i, mask] = m_offset;
        value.maskscatter( ptr, i, mask, value );
      } else {
        static_assert( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather2D,
                       "New ProxyBehaviour was added but not implemented everywhere!" );
        static_assert( Behaviour != LHCb::Pr::ProxyBehaviour::ScatterGather2D,
                       "ScatterGather2D writing is not implemented yet." );
      }
    }

  private:
    ContainerType m_container;
    OffsetType    m_offset;
    const Path    m_path;
  };

  template <typename ElementTag, std::size_t... N>
  struct ndarray_field {
    using data_tree_t = SOADataNDArray<typename ElementTag::data_tree_t, N...>;
    using packer_t    = SOAPackNDArray<typename ElementTag::packer_t, N...>;

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct NDArrayProxy {
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;

      NDArrayProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      template <typename... Ts>
      [[nodiscard, gnu::always_inline]] inline constexpr auto field( Ts... Is ) const {
        using tag_proxy = typename ElementTag::template proxy_type<Simd, Behaviour, ContainerTypeBase,
                                                                   typename Path::template extend_t<Ts...>>;
        return tag_proxy{m_container, m_offset, m_path.append( Is... )};
      }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = NDArrayProxy<Simd, Behaviour, ContainerType, Path>;
  };

  template <bool Unique = false>
  struct int_field_ {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type  = NumericProxy<Simd, Behaviour, ContainerType, Path, int, Unique>;
    using data_tree_t = SOAData<int, Unique>;
    using packer_t    = SOAPackNumeric<int, Unique>;
  };
  template <bool Unique = false>
  struct float_field_ {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type  = NumericProxy<Simd, Behaviour, ContainerType, Path, float, Unique>;
    using data_tree_t = SOAData<float, Unique>;
    using packer_t    = SOAPackNumeric<float, Unique>;
  };

  using int_field   = int_field_<>;
  using float_field = float_field_<>;

  template <std::size_t... N>
  using ints_field = ndarray_field<int_field, N...>;
  template <std::size_t... N>
  using floats_field = ndarray_field<float_field, N...>;

  template <typename ChildContainerType>
  struct index_field : int_field {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct IndexProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using int_v         = typename simd_t::int_v;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type    = NumericProxy<Simd, Behaviour, ContainerTypeBase, Path, int>;

      IndexProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      [[nodiscard, gnu::always_inline]] constexpr auto proxy() const {
        return proxy_type{m_container, m_offset, m_path};
      }
      [[nodiscard, gnu::always_inline]] constexpr auto index() const { return proxy().get(); }
      template <typename I>
      [[gnu::always_inline]] constexpr void set( I i ) {
        proxy().set( i );
      }

      template <typename M = std::true_type>
      [[nodiscard, gnu::always_inline]] constexpr auto get( [[maybe_unused]] M&& withmask = {} ) const {
        // TODO: support scatterGather2D
        static_assert( Behaviour != LHCb::Pr::ProxyBehaviour::ScatterGather2D,
                       "Index does not support ScatterGather2D proxies yet." );

        auto container = m_container->template container<typename Path::Tag>();
        auto mask      = LHCb::Event::validity_mask<Behaviour, simd_t>( m_offset, m_container );
        if constexpr ( !std::is_same_v<std::true_type, std::decay_t<M>> ) { mask = mask && withmask; }
        return container->template simd<Simd>().gather( index(), mask );
      }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = IndexProxy<Simd, Behaviour, ContainerType, Path>;
  };

  template <typename>
  struct is_index_field : std::false_type {};
  template <typename T>
  struct is_index_field<index_field<T>> : std::true_type {};

  template <typename ElementTag>
  struct vector_field {
    struct size_field : int_field {};
    using data_tree_t = SOADataVector<typename ElementTag::data_tree_t, size_field>;
    using packer_t    = SOAPackVector<typename ElementTag::packer_t, size_field>;

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct VectorProxy {
      using simd_t         = SIMDWrapper::type_map_t<Simd>;
      using OffsetType     = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType  = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using elt_proxy_type = typename ElementTag::template proxy_type<Simd, Behaviour, ContainerTypeBase,
                                                                      typename Path::template extend_t<std::size_t>>;
      using size_proxy_type =
          NumericProxy<Simd, Behaviour, ContainerTypeBase, typename Path::template extend_t<size_field>, int>;
      using size_type = typename size_proxy_type::type;

      VectorProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      struct Iterator {
        using value_type        = elt_proxy_type;
        using pointer           = value_type const*;
        using reference         = value_type&;
        using const_reference   = value_type const&;
        using difference_type   = int;
        using iterator_category = std::random_access_iterator_tag;

        VectorProxy<Simd, Behaviour, ContainerTypeBase, Path> m_vec_proxy;
        int                                                   m_index{0};

        Iterator() = default;
        Iterator( const VectorProxy<Simd, Behaviour, ContainerTypeBase, Path> vec_proxy, int index )
            : m_vec_proxy( vec_proxy ), m_index( index ) {}

        auto operator*() const { return m_vec_proxy[m_index]; }

        Iterator& operator++() {
          m_index += 1;
          return *this;
        }
        Iterator& operator--() {
          m_index -= 1;
          return *this;
        }
        Iterator operator++( int ) {
          Iterator retval = *this;
          m_index += 1;
          return retval;
        }
        Iterator operator--( int ) {
          Iterator retval = *this;
          m_index -= 1;
          return retval;
        }
        Iterator& operator+=( difference_type n ) {
          m_index += n;
          return *this;
        }
        auto operator[]( difference_type n ) const {
          return m_vec_proxy[m_index + n]; // TODO: mask invalid elements
        }
        friend Iterator operator+( Iterator lhs, difference_type rhs ) { return lhs += rhs; }
        friend Iterator operator+( difference_type lhs, Iterator rhs ) { return rhs += lhs; }
        friend bool     operator==( Iterator const& lhs, Iterator const& rhs ) {
          assert( lhs.m_vec_proxy == rhs.m_vec_proxy );
          return lhs.m_index == rhs.m_index;
        }
        friend bool            operator!=( Iterator const& lhs, Iterator const& rhs ) { return !( lhs == rhs ); }
        friend difference_type operator-( Iterator const& lhs, Iterator const& rhs ) {
          return lhs.m_index - rhs.m_index;
        }
      };

      auto begin() const { return Iterator{*this, 0}; }
      auto end() const {
        auto max_size =
            select( LHCb::Event::validity_mask<Behaviour, simd_t>( m_offset, m_container ), size(), size_type{0} )
                .hmax();
        return Iterator{*this, max_size};
      }

      auto operator[]( std::size_t i ) const {
        if constexpr ( is_index_field<ElementTag>::value ) {
          return elt_proxy_type{m_container, m_offset, m_path.append( i )}.get(); // TODO: mask invalid elements
        } else {
          return elt_proxy_type{m_container, m_offset, m_path.append( i )}; // TODO: mask invalid elements
        }
      }

      template <typename I>
      auto set( std::size_t i, I value ) {
        elt_proxy_type{m_container, m_offset, m_path.append( i )}.set( value );
      }

      auto size() const { return size_proxy_type{m_container, m_offset, m_path.template append<size_field>()}.get(); }

      auto empty() const {
        return select( LHCb::Event::validity_mask<Behaviour, simd_t>( m_offset, m_container ), size(), size_type{0} )
                   .hmax() == 0;
      }

      auto maxSize() const { return m_container->data( m_path.template append<std::true_type>() )->maxSize(); }

      void resize( size_type size ) const {
        auto max_size =
            select( LHCb::Event::validity_mask<Behaviour, simd_t>( m_offset, m_container ), size, size_type{0} ).hmax();
        m_container->data( m_path.template append<std::true_type>() )->resize( max_size );
        size_proxy_type{m_container, m_offset, m_path.template append<size_field>()}.set( size );
      }

      auto emplace_back() {
        static_assert( Simd == SIMDWrapper::InstructionSet::Scalar, "emplace_back only available for scalar" );
        resize( size() + 1 );
        return elt_proxy_type{m_container, m_offset, m_path.append( ( size_t )( size().cast() - 1 ) )};
      }

      void clear() const { size_proxy_type{m_container, m_offset, m_path.template append<size_field>()}.set( 0 ); }

      friend bool operator==( const VectorProxy<Simd, Behaviour, ContainerTypeBase, Path>& lhs,
                              const VectorProxy<Simd, Behaviour, ContainerTypeBase, Path>& rhs ) {
        return lhs.m_container ==
               rhs.m_container; // TODO: must also test the offset (at least the index, and maybe the mask ?)
      }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = VectorProxy<Simd, Behaviour, ContainerType, Path>;
  };

  template <typename... Tags>
  struct struct_field {
    using data_tree_t = SOADataStruct<Tags...>;
    using packer_t    = SOAPackStruct<Tags...>;

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct StructProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;

      StructProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      template <typename Tag>
      [[nodiscard, gnu::always_inline]] inline constexpr auto field() const {
        using tag_proxy = typename Tag::template proxy_type<Simd, Behaviour, ContainerTypeBase,
                                                            typename Path::template extend_t<Tag>>;
        return tag_proxy{m_container, m_offset, m_path.template append<Tag>()};
      }

      template <typename Tag>
      [[nodiscard, gnu::always_inline]] inline constexpr auto get() const {
        return field<Tag>().get();
      }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = StructProxy<Simd, Behaviour, ContainerType, Path>;
  };

  namespace detail {
    template <std::size_t... Ns>
    inline auto make_bitindex( std::index_sequence<Ns...> ) {
      return 0;
    }
    template <std::size_t... Ns, typename I>
    inline auto make_bitindex( std::index_sequence<Ns...>, I i ) {
      return i;
    }
    template <std::size_t N, std::size_t... Ns, typename I, typename... Is>
    inline auto make_bitindex( std::index_sequence<N, Ns...>, I i, Is... is ) {
      if constexpr ( sizeof...( Ns ) >= sizeof...( Is ) ) {
        return make_bitindex( std::index_sequence<Ns...>{}, i, is... );
      }
      return ( i << ( N + ... + Ns ) ) | make_bitindex( std::index_sequence<Ns...>{}, is... );
    }

    template <typename int_v>
    inline auto decompose_bitindex( std::index_sequence<>, int_v ) {
      return std::make_tuple();
    }
    template <std::size_t N, std::size_t... Ns, typename int_v>
    inline auto decompose_bitindex( std::index_sequence<N, Ns...>, int_v i ) {
      return std::tuple_cat( std::make_tuple( ( i >> ( 0 + ... + Ns ) ) & ( ( 1u << N ) - 1u ) ),
                             decompose_bitindex( std::index_sequence<Ns...>{}, i ) );
    }

    template <typename>
    struct is_tuple : std::false_type {};
    template <typename... T>
    struct is_tuple<std::tuple<T...>> : std::true_type {};

    template <class T, class Tuple>
    struct tuple_index;
    template <class T, class... Types>
    struct tuple_index<T, std::tuple<T, Types...>> {
      static const std::size_t value = 0;
    };
    template <class T, class U, class... Types>
    struct tuple_index<T, std::tuple<U, Types...>> {
      static const std::size_t value = 1 + tuple_index<T, std::tuple<Types...>>::value;
    };

    // identity T -> type = T; note that id_<T> is a complete type, even if T is not
    template <typename T>
    struct id_ {
      using type = T;
    };
    template <class T>
    struct type_list_t;
    template <class... Ts>
    struct type_list_t<std::tuple<Ts...>> {
      using type = std::tuple<id_<Ts>...>;
    };
  } // namespace detail

  /**
   * ND Tag and proxy for representing a 2D index into multiple containers `base[i]->field[j]`
   */
  template <typename ChildContainerTypes, typename BitLayout = std::index_sequence<24>>
  struct index2D_field;

  template <typename ChildContainerTypes, std::size_t... Bits>
  struct index2D_field<ChildContainerTypes, std::index_sequence<Bits...>> : int_field {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct NDIndex2DProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using int_v         = typename simd_t::int_v;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type    = NumericProxy<Simd, Behaviour, ContainerTypeBase, Path, int>;

      NDIndex2DProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      [[nodiscard, gnu::always_inline]] inline constexpr auto proxy() const {
        return proxy_type{m_container, m_offset, m_path};
      }

      [[nodiscard, gnu::always_inline]] inline constexpr auto index() const {
        return detail::decompose_bitindex( std::index_sequence<32 - ( 0 + ... + Bits ), Bits...>{}, proxy().get() );
      }

      template <typename... Ids>
      [[gnu::always_inline]] inline constexpr void set( Ids... i ) {
        proxy().set( detail::make_bitindex( std::index_sequence<Bits...>{}, i... ) );
      }

      template <typename M = std::true_type>
      [[nodiscard, gnu::always_inline]] inline constexpr auto get( M&& withmask = {} ) {
        if constexpr ( detail::is_tuple<ChildContainerTypes>::value ) {
          using type_list = typename detail::type_list_t<ChildContainerTypes>::type;
          return std::apply(
              [&]( auto... x ) {
                return std::make_tuple(
                    get<typename decltype( x )::type,
                        detail::tuple_index<typename decltype( x )::type, ChildContainerTypes>::value>( withmask )... );
              },
              type_list{} );
        } else {
          return get<ChildContainerTypes>( withmask );
        }
      }

    private:
      template <typename ChildContainerType, int tuple_idx = 0, typename M = std::true_type>
      [[nodiscard, gnu::always_inline]] inline constexpr auto get( M&& withmask = {} ) {
        static_assert( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather2D,
                       "NDIndex2D is only supported by ScatterGather2D proxies." );

        using proxy_type =
            typename ChildContainerType::template proxy_type<Simd, LHCb::Pr::ProxyBehaviour::ScatterGather2D,
                                                             ChildContainerType>;
        std::array<ChildContainerType*, simd_t::size> base{nullptr};
        int_v                                         k_{0};

        auto idx = index();
        auto j2  = std::get<std::tuple_size_v<decltype( idx )> - 1>( idx );
        auto i2  = std::get<std::tuple_size_v<decltype( idx )> - 2>( idx );

        std::array<int, simd_t::size> i2_v;
        i2.store( i2_v );

        auto [i1, j1, mask] = m_offset;
        std::array<int, simd_t::size> i1_v;
        i1.store( i1_v );

        if constexpr ( detail::is_tuple<ChildContainerTypes>::value ) {
          mask = mask && ( std::get<0>( idx ) == int_v{tuple_idx} );
        }

        if constexpr ( !std::is_same_v<std::true_type, std::decay_t<M>> ) { mask = mask && withmask; }

        auto progressMask = mask;
        int  first        = ffs( progressMask ) - 1;
        int  k            = 0;
        while ( first >= 0 ) {
          auto updateMask = ( i1_v[first] == i1 ) && ( i2_v[first] == i2 ) && mask;
          k_              = select( updateMask, int_v{k}, k_ );
          // Should be the last Tag (non-index) instead of the first
          // TODO: change it when arrays and values are separated
          if constexpr ( detail::is_tuple<ChildContainerTypes>::value ) {
            base[k++] =
                std::get<tuple_idx>( m_container[i1_v[first]]->template containers<typename Path::Tag>() )[i2_v[first]];
          } else {
            base[k++] = m_container[i1_v[first]]->template containers<typename Path::Tag>()[i2_v[first]];
          }
          progressMask = progressMask && !updateMask;
          first        = ffs( progressMask ) - 1;
        }

        return proxy_type{base, {k_, j2, mask}};
      }

      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = NDIndex2DProxy<Simd, Behaviour, ContainerType, Path>;
  };

  template <typename ChildContainerTypes, typename BitLayout = std::index_sequence<24>, std::size_t... N>
  using indices2D_field = ndarray_field<index2D_field<ChildContainerTypes, BitLayout>, N...>;

  /**
   * Proxy representing an SOACollection
   */
  template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase>
  struct Proxy {
    static_assert(
        Simd != SIMDWrapper::Best,
        "To allow proxy objects to be passed between conventionally compiled and JIT-compiled code, where `Best` has "
        "different meanings, `Best` should be translated into a concrete backend before proxy objects are created." );
    // Perhaps `simd` should be given a better name, but this name is assumed in
    // Functors::Adapters::CombinationFromComposite
    static constexpr SIMDWrapper::InstructionSet simd = Simd;
    using simd_t                                      = SIMDWrapper::type_map_t<Simd>;
    using mask_v                                      = typename simd_t::mask_v;
    using OffsetType                                  = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
    using container_t                                 = ContainerTypeBase;
    using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;

    template <typename Tag>
    using hasTag = typename ContainerTypeBase::template hasTag<Tag>;

    Proxy( ContainerType container, OffsetType offset ) : m_container( container ), m_offset( offset ) {}

    // Make a const proxy from non-const proxy
    template <typename T = ContainerTypeBase, typename std::enable_if_t<std::is_const_v<T>, int> = 0>
    Proxy( Proxy<Simd, Behaviour, std::remove_const_t<T>> other )
        : m_container{other.container()}, m_offset{other.offset()} {}

    template <typename Tag, typename... Ts>
    [[nodiscard, gnu::always_inline, gnu::flatten]] inline constexpr auto field( Ts... Is ) const {
      using tag_proxy = typename Tag::template proxy_type<Simd, Behaviour, ContainerTypeBase, SOAPath<Tag>>;
      if constexpr ( sizeof...( Is ) == 0 ) {
        return tag_proxy{m_container, m_offset, SOAPath<Tag>{std::make_tuple( Tag{} )}};
      } else {
        return tag_proxy{m_container, m_offset, SOAPath<Tag>{std::make_tuple( Tag{} )}}.field( Is... );
      }
    }

    template <typename Tag, typename... Ts>
    [[nodiscard, gnu::always_inline, gnu::flatten]] inline constexpr auto get( Ts... Is ) const {
      using tag_proxy = typename Tag::template proxy_type<Simd, Behaviour, ContainerTypeBase, SOAPath<Tag>>;
      if constexpr ( sizeof...( Is ) == 0 ) {
        return tag_proxy{m_container, m_offset, SOAPath<Tag>{std::make_tuple( Tag{} )}}.get();
      } else {
        return tag_proxy{m_container, m_offset, SOAPath<Tag>{std::make_tuple( Tag{} )}}.field( Is... ).get();
      }
    }

    constexpr auto& container() {
      if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather2D &&
                     simd == SIMDWrapper::InstructionSet::Scalar ) {
        return m_container[0];
      } else {
        return m_container;
      }
    }

    constexpr auto const& container() const {
      if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather2D &&
                     simd == SIMDWrapper::InstructionSet::Scalar ) {
        return m_container[0];
      } else {
        return m_container;
      }
    }

    constexpr auto loop_mask() const { return LHCb::Event::validity_mask<Behaviour, simd_t>( m_offset, m_container ); }
    constexpr auto indices() const { return LHCb::Event::indices<Behaviour, simd_t>( m_offset ); }

    OffsetType            offset() const { return m_offset; }
    static constexpr auto width() { return simd_t::size; }

    // Make functors return type deduction happy ...
    static constexpr mask_v mask_true() { return mask_v{true}; }

    constexpr auto zipIdentifier() const { return m_container->zipIdentifier(); }

  private:
    ContainerType m_container;
    OffsetType    m_offset;
  };

  namespace detail {
    template <typename First, typename... Others>
    struct first {
      using type = First;
    };

    // Search the list of Proxy T... the first Proxy that contains Tag
    template <typename Tag, typename... T>
    struct find_proxy_with_tag {
      using type                  = void;
      static constexpr bool found = false;
      static constexpr bool value = found;
    };
    template <typename Tag, typename First, typename... T>
    struct find_proxy_with_tag<Tag, First, T...> {
      using type                  = typename std::conditional<First::template hasTag<Tag>::value, First,
                                             typename find_proxy_with_tag<Tag, T...>::type>::type;
      static constexpr bool found = First::template hasTag<Tag>::value || find_proxy_with_tag<Tag, T...>::found;
      static constexpr bool value = found;
    };

    // Search the list of Proxy T... the first Proxy that is for Container
    template <typename Container, typename... T>
    struct find_proxy_with_container {
      using type                  = void;
      static constexpr bool found = false;
      static constexpr bool value = found;
    };
    template <typename Container, typename First, typename... T>
    struct find_proxy_with_container<Container, First, T...> {
      using type = typename std::conditional<std::is_same_v<typename First::container_t, Container>, First,
                                             typename find_proxy_with_container<Container, T...>::type>::type;
      static constexpr bool found =
          std::is_same_v<typename First::container_t, Container> || find_proxy_with_container<Container, T...>::found;
      static constexpr bool value = found;
    };

    // Turn Proxy<Simd, Behaviour, T const> into ProxyBehaviour<Simd, Behaviour, T>
    template <typename>
    struct non_const_proxy {};

    template <template <SIMDWrapper::InstructionSet, LHCb::Pr::ProxyBehaviour, typename> typename Proxy,
              SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType>
    struct non_const_proxy<Proxy<Simd, Behaviour, ContainerType>> {
      using type = Proxy<Simd, Behaviour, std::remove_const_t<ContainerType>>;
    };

    template <typename Proxy>
    using non_const_proxy_t = typename non_const_proxy<Proxy>::type;
  } // namespace detail

  /**
   * Proxy representing a Zip of SOACollections
   */
  template <typename... Proxies>
  struct ZipProxy : Proxies... {
    ZipProxy( Proxies... p ) : Proxies( p )... {}

    /** @brief Make a ZipProxy-of-const from a ZipProxy-of-non-const proxies
     *
     *  This overload only participates if at least one of `Proxies` refers to
     *  a const qualified container; i.e. this does *not* shadow the copy
     *  constructor, as it is only enabled if the argument type is different to
     *  `this`.
     */
    template <
        typename ProxiesList = boost::mp11::mp_list<Proxies...>,
        typename std::enable_if_t<
            !std::is_same_v<ProxiesList, boost::mp11::mp_transform<detail::non_const_proxy_t, ProxiesList>>, int> = 0>
    ZipProxy( ZipProxy<detail::non_const_proxy_t<Proxies>...> const& old )
        : Proxies{static_cast<detail::non_const_proxy_t<Proxies> const&>( old )}... {}

    template <typename Tag, typename... Ts>
    [[nodiscard, gnu::always_inline, gnu::flatten]] inline auto field( Ts... Is ) const {
      using prox = typename detail::find_proxy_with_tag<Tag, Proxies...>::type;
      static_assert( detail::find_proxy_with_tag<Tag, Proxies...>::found );
      return static_cast<prox const*>( this )->template field<Tag>( Is... );
    }

    template <typename Tag, typename... Ts>
    [[nodiscard, gnu::always_inline, gnu::flatten]] inline auto get( Ts... Is ) const {
      using prox = typename detail::find_proxy_with_tag<Tag, Proxies...>::type;
      static_assert( detail::find_proxy_with_tag<Tag, Proxies...>::found );
      return static_cast<prox const*>( this )->template get<Tag>( Is... );
    }

    template <typename Container>
    [[nodiscard, gnu::always_inline]] auto& as() const {
      using prox = typename detail::find_proxy_with_container<Container, Proxies...>::type;
      static_assert( detail::find_proxy_with_container<Container, Proxies...>::found );
      return *static_cast<prox const*>( this );
    }

    template <typename Tag>
    using hasTag = detail::find_proxy_with_tag<Tag, Proxies...>;
    using detail::first<Proxies...>::type::loop_mask;
    using detail::first<Proxies...>::type::indices;
    using detail::first<Proxies...>::type::offset;
    using detail::first<Proxies...>::type::width;
    using detail::first<Proxies...>::type::mask_true;
    using detail::first<Proxies...>::type::zipIdentifier;
    using typename detail::first<Proxies...>::type::simd_t;
    using typename detail::first<Proxies...>::type::OffsetType;
  };
} // namespace LHCb::Event
