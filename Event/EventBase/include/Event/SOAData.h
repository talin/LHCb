/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <iostream>

#include "Kernel/EventLocalAllocator.h"
#include "Kernel/STLExtensions.h"
#include "SOAPacker.h"
#include "SOAPath.h"
#include "SOAUtils.h"
#include <boost/align/is_aligned.hpp>

namespace LHCb::Event {

  // SOAData is the type of the leaf nodes of the data tree
  // it can hold pointers to int of float types
  template <typename T, bool Unique = false>
  struct SOAData {

    template <typename simd_t>
    using vec_type = typename SIMDWrapper::vector_type_map_t<T, simd_t>;

    using allocator_type = LHCb::Allocators::EventLocal<T>;

    using packer_t = SOAPackNumeric<T>;

    SOAData( LHCb::Allocators::MemoryResource* ) {}

    SOAData( SOAData&& rhs ) noexcept : m_data{std::exchange( rhs.m_data, nullptr )} {}

    void deallocate( allocator_type alloc, std::size_t capacity ) {
      if ( m_data ) {
        std::allocator_traits<allocator_type>::deallocate( alloc, m_data, capacity );
        m_data = nullptr;
      }
    }

    SOAData( const SOAData& ) = delete;
    SOAData& operator=( const SOAData& ) = delete;

    template <typename Path>
    T* data( const Path ) noexcept {
      return m_data;
    }

    template <typename Path>
    T const* data( const Path ) const noexcept {
      return m_data;
    }

    T* data() noexcept { return m_data; }

    T const* data() const noexcept { return m_data; }

    // Helper for reserve methods that assumes its argument is nonzero, and is large enough that we should
    // really reallocate.
    void reallocate( allocator_type alloc, std::size_t size, std::size_t old_capacity, std::size_t new_capacity ) {
      auto new_data = std::allocator_traits<allocator_type>::allocate( alloc, new_capacity );
      assert( boost::alignment::is_aligned( new_data, alignof( T ) ) );
      // Copy size entries from the old storage to the new storage and then
      // free the old storage, if old storage was allocated.
      if ( m_data ) {
        std::copy( m_data, std::next( m_data, size ), new_data );
        std::allocator_traits<allocator_type>::deallocate( alloc, m_data, old_capacity );
      }
      m_data = new_data;
    }

    template <typename simd_t, typename mask_t>
    void copy_back( int size, SOAData<T> const& from, int at, mask_t const& mask ) {
      auto const*      from_data_ptr   = std::next( from.data(), at );
      auto*            this_target_ptr = std::next( m_data, size );
      vec_type<simd_t> from_data{from_data_ptr};
      if constexpr ( std::is_same_v<mask_t, std::true_type> ) {
        from_data.store( this_target_ptr );
      } else {
        from_data.compressstore( mask, this_target_ptr );
      }
    }

    void construct( std::size_t, std::size_t ) {
      // TODO: user-defined constructor ?
    }

  private:
    T* m_data{nullptr};
  };

  template <typename T>
  struct SOAData<T, true> {
    template <typename simd_t>
    using vec_type = typename SIMDWrapper::vector_type_map_t<T, simd_t>;

    using allocator_type = LHCb::Allocators::EventLocal<T>;

    using packer_t = SOAPackNumeric<T, true>;

    SOAData( LHCb::Allocators::MemoryResource* ) {}

    SOAData( SOAData&& rhs ) noexcept : m_data{rhs.m_data} {}

    void deallocate( allocator_type, std::size_t ) {}

    SOAData( const SOAData& ) = delete;
    SOAData& operator=( const SOAData& ) = delete;

    template <typename Path>
    T* data( const Path ) noexcept {
      return &m_data;
    }

    template <typename Path>
    T const* data( const Path ) const noexcept {
      return &m_data;
    }

    T* data() noexcept { return &m_data; }

    T const* data() const noexcept { return &m_data; }

    void reallocate( allocator_type, std::size_t, std::size_t, std::size_t ) {}

    template <typename simd_t, typename mask_t>
    void copy_back( int, SOAData<T, true> const& from, int, mask_t const& ) {
      m_data = from.m_data;
    }

    void construct( std::size_t, std::size_t ) { m_data = 0; }

  private:
    T m_data{0};
  };

  namespace detail {
    template <typename Path>
    [[nodiscard, gnu::always_inline]] inline constexpr std::size_t idx( Path, std::size_t i ) {
      return i;
    }
    template <typename Path, std::size_t N, std::size_t... Ns>
    [[nodiscard, gnu::always_inline]] inline constexpr std::size_t idx( Path p, std::size_t i ) {
      return idx<decltype( p.next() ), Ns...>( p.next(), i * N + p.index() );
    }

    template <typename Path>
    [[nodiscard, gnu::always_inline]] inline constexpr auto next( Path p ) {
      return p;
    }
    template <typename Path, std::size_t N, std::size_t... Ns>
    [[nodiscard, gnu::always_inline]] inline constexpr auto next( Path p ) {
      return next<decltype( p.next() ), Ns...>( p.next() );
    }
  } // namespace detail

  template <typename T, std::size_t... Ns>
  struct SOADataNDArray { // TODO remove and replace by SOADataArray<SOADataArray<T..., N>, N>
    using packer_t = SOAPackNDArray<T, Ns...>;

    template <std::size_t... Is>
    SOADataNDArray( LHCb::Allocators::MemoryResource* f, std::index_sequence<Is...> const& )
        : m_array{( (void)Is, f )...} {}

    SOADataNDArray( LHCb::Allocators::MemoryResource* res )
        : SOADataNDArray( res, std::make_index_sequence<( 1 * ... * Ns )>{} ) {}

    void deallocate( LHCb::Allocators::MemoryResource* alloc, std::size_t capacity ) {
      for ( auto& element : m_array ) { element.deallocate( alloc, capacity ); }
    }

    template <typename Path>
    auto data( const Path p ) noexcept {
      return m_array[detail::idx<Path, Ns...>( p, 0 )].data( detail::next<Path, Ns...>( p ) );
    }

    auto& child( std::size_t idx ) noexcept { return m_array[idx]; }

    auto const& child( std::size_t idx ) const noexcept { return m_array[idx]; }

    void reallocate( LHCb::Allocators::MemoryResource* alloc, std::size_t size, std::size_t old_capacity,
                     std::size_t new_capacity ) {
      for ( auto& element : m_array ) { element.reallocate( alloc, size, old_capacity, new_capacity ); }
    }

    template <typename simd_t, typename mask_t>
    void copy_back( int size, SOADataNDArray<T, Ns...> const& from, int at, mask_t const& mask ) {
      for ( std::size_t i = 0; i < ( 1 * ... * Ns ); i++ ) {
        child( i ).template copy_back<simd_t>( size, from.child( i ), at, mask );
      }
    }

    void construct( std::size_t from, std::size_t to ) {
      for ( auto& element : m_array ) { element.construct( from, to ); }
    }

  private:
    std::array<T, ( 1 * ... * Ns )> m_array;
  };

  template <typename T, std::size_t N>
  struct SOADataArray {
    using packer_t = SOAPackArray<T, N>;

    template <std::size_t... Is>
    SOADataArray( LHCb::Allocators::MemoryResource* f, std::index_sequence<Is...> const& )
        : m_array{( (void)Is, f )...} {}

    SOADataArray( LHCb::Allocators::MemoryResource* res ) : SOADataArray( res, std::make_index_sequence<N>{} ) {}

    void deallocate( LHCb::Allocators::MemoryResource* alloc, std::size_t capacity ) {
      for ( auto& element : m_array ) { element.deallocate( alloc, capacity ); }
    }

    template <typename Path>
    auto data( const Path p ) noexcept {
      return m_array[p.index()].data( p.next() );
    }

    auto& child( std::size_t i ) noexcept { return m_array[i]; }

    auto const& child( std::size_t i ) const noexcept { return m_array[i]; }

    void reallocate( LHCb::Allocators::MemoryResource* alloc, std::size_t size, std::size_t old_capacity,
                     std::size_t new_capacity ) {
      for ( auto& element : m_array ) { element.reallocate( alloc, size, old_capacity, new_capacity ); }
    }

    template <typename simd_t, typename mask_t>
    void copy_back( int size, SOADataArray<T, N> const& from, int at, mask_t const& mask ) {
      for ( std::size_t i = 0; i < N; i++ ) {
        child( i ).template copy_back<simd_t>( size, from.child( i ), at, mask );
      }
    }

    void construct( std::size_t from, std::size_t to ) {
      for ( auto& element : m_array ) { element.construct( from, to ); }
    }

  private:
    std::array<T, N> m_array;
  };

  template <typename T, typename SizeTag>
  struct SOADataVector {
    using allocator_type = LHCb::Allocators::EventLocal<T>;
    using packer_t       = SOAPackVector<T, SizeTag>;

    SOADataVector( LHCb::Allocators::MemoryResource* res ) : m_sizes{res}, m_vector{allocator_type{res}} {}

    void deallocate( LHCb::Allocators::MemoryResource* alloc, std::size_t capacity ) {
      assert( capacity == m_capacity );
      m_sizes.deallocate( alloc, capacity );
      for ( auto& element : m_vector ) { element.deallocate( alloc, capacity ); }
      m_vector.clear();
      m_vector.shrink_to_fit();
      // deallocate is supposed to de-allocate all memory used by an instance of this class, and in that sense is used
      // where otherwise a destructor would be used, but without terminating the lifetime, the above clear() and
      // shrink_to_fit() are indeed necessary over here in order to really de-allocate all memory used by an instance.
      // An alternative would be to remove the need for deallocate by replacing its use by (implicitly) calling the
      // destructor and in-place allocating it for the next 'life-cycle'. But given that both alloc and capacity are
      // needed here, that doesn't quite work: one would still have to call deallocate as-is, then call the destructor
      // explicitly, and then inplace allocate -- at which point it is easier to just add the above clear() and
      // shrink_to_fit() directly in deallocate and skip the latter two steps.
    }

    template <typename Path>
    auto data( const Path p ) noexcept {
      if constexpr ( std::is_same_v<typename Path::Tag, SizeTag> ) {
        return m_sizes.data( p.next() );
      } else if constexpr ( std::is_same_v<typename Path::Tag, std::true_type> ) {
        return this;
      } else {
        return m_vector[p.index()].data( p.next() );
      }
    }

    auto& child( std::size_t i ) noexcept { return m_vector[i]; }

    auto const& child( std::size_t i ) const noexcept { return m_vector[i]; }

    auto& sizes() noexcept { return m_sizes; }

    auto const& sizes() const noexcept { return m_sizes; }

    std::size_t maxSize() const noexcept { return m_vector.size(); }

    void resize( std::size_t newSize ) {
      if ( newSize > m_vector.size() ) {
        auto oldSize = m_vector.size();
        m_vector.reserve( newSize );
        for ( std::size_t i = oldSize; i < newSize; i++ ) {
          m_vector.emplace_back( m_vector.get_allocator().resource() );
          m_vector[i].reallocate( m_vector.get_allocator().resource(), 0, 0, m_capacity );
          m_vector[i].construct( 0, m_capacity ); // TODO: construct up to the container size, not capacity
        }
      }
    }

    void reallocate( LHCb::Allocators::MemoryResource* alloc, std::size_t size, std::size_t old_capacity,
                     std::size_t new_capacity ) {
      m_sizes.reallocate( alloc, size, old_capacity, new_capacity );

      for ( auto& element : m_vector ) { element.reallocate( alloc, size, old_capacity, new_capacity ); }
      m_capacity = new_capacity;
    }

    template <typename simd_t, typename mask_t>
    void copy_back( int size, SOADataVector<T, SizeTag> const& from, int at, mask_t const& mask ) {
      sizes().template copy_back<simd_t>( size, from.sizes(), at, mask );
      resize( from.maxSize() );
      for ( std::size_t i = 0; i < from.maxSize(); i++ ) {
        child( i ).template copy_back<simd_t>( size, from.child( i ), at, mask );
      }
    }

    void construct( std::size_t from, std::size_t to ) {
      std::memset( m_sizes.data() + from, 0, ( to - from ) * sizeof( int ) );
      for ( auto& element : m_vector ) { element.construct( from, to ); }
    }

  private:
    SOAData<int>                   m_sizes;
    std::vector<T, allocator_type> m_vector;
    std::size_t                    m_capacity{0};
  };

  template <typename... Tags>
  struct SOADataStruct {
    using packer_t = SOAPackStruct<Tags...>;

    SOADataStruct( LHCb::Allocators::MemoryResource* res )
        : m_children{std::make_tuple( typename Tags::data_tree_t( res )... )} {}

    void deallocate( LHCb::Allocators::MemoryResource* alloc, std::size_t capacity ) {
      ( child<Tags>().deallocate( alloc, capacity ), ... );
    }

    template <typename Path>
    auto data( const Path p ) noexcept {
      return std::get<LHCb::index_of_v<typename Path::Tag, std::tuple<Tags...>>>( m_children ).data( p.next() );
    }

    template <typename Tag>
    typename Tag::data_tree_t& child() noexcept {
      return std::get<LHCb::index_of_v<Tag, std::tuple<Tags...>>>( m_children );
    }

    template <typename Tag>
    typename Tag::data_tree_t const& child() const noexcept {
      return std::get<LHCb::index_of_v<Tag, std::tuple<Tags...>>>( m_children );
    }

    void reallocate( LHCb::Allocators::MemoryResource* alloc, std::size_t size, std::size_t old_capacity,
                     std::size_t new_capacity ) {
      ( child<Tags>().reallocate( alloc, size, old_capacity, new_capacity ), ... );
    }

    template <typename simd_t, typename mask_t>
    void copy_back( int size, SOADataStruct<Tags...> const& from, int at, mask_t const& mask ) {
      ( child<Tags>().template copy_back<simd_t>( size, from.child<Tags>(), at, mask ), ... );
    }

    void construct( std::size_t from, std::size_t to ) { ( child<Tags>().construct( from, to ), ... ); }

  private:
    std::tuple<typename Tags::data_tree_t...> m_children;
  };

} // namespace LHCb::Event