/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class SOACollectionMerger.h SOACollectionMerger
 *
 *  Merge SOACollections into one. Template to work with all containers based on SOACollection.
 *
 *  @author Michel De Cian
 *  @date   05/07/2022
 */

#include "Event/SOACollection.h"
#include "LHCbAlgs/MergingTransformer.h"
#include <string>
namespace LHCb {

  template <typename Type>
  struct SOACollectionMerger final
      : Algorithm::MergingTransformer<Type( const Gaudi::Functional::vector_of_const_<Type>& )> {
    SOACollectionMerger( const std::string& name, ISvcLocator* pSvcLocator )
        : Algorithm::MergingTransformer<Type( const Gaudi::Functional::vector_of_const_<Type>& )>{
              name, pSvcLocator, {"InputLocations", {}}, {"OutputLocation", {}}} {}

    Type operator()( const Gaudi::Functional::vector_of_const_<Type>& lists ) const override {

      if ( lists.size() == 0 ) return {};

      Type        out( lists[0].zipIdentifier(), lists[0].get_allocator() );
      std::size_t overallSize = 0;
      for ( const auto& list : lists ) overallSize += list.size();

      out.reserve( overallSize );
      m_nbObjectsCounter += overallSize;

      for ( const auto& list : lists ) {

        if ( list.empty() ) continue;
        for ( const auto& obj : list.simd() ) {
          out.template copy_back<SIMDWrapper::best::types>( list, obj.offset(), obj.loop_mask() );
        }
      }

      return out;
    }

  private:
    mutable Gaudi::Accumulators::SummingCounter<> m_nbObjectsCounter{this, "Nb of Merged Objects"};
  };
} // namespace LHCb
