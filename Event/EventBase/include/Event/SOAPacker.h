/*****************************************************************************\
* (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "SOAPath.h"
#include <GaudiKernel/System.h>
#include <LHCbMath/SIMDWrapper.h>
#include <cmath>
#include <ratio>

namespace LHCb::Event {

  template <typename T, bool Unique = false>
  struct SOAPackNumeric {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer& buffer, const Collection& collection, Path path ) {
      auto* dataIn = collection.data( path );
      if constexpr ( Unique ) {
        buffer.write( dataIn[0] );
      } else {
        for ( std::size_t i = 0; i < collection.size(); i++ ) {
          auto mask = path.template isValid<SIMDWrapper::scalar::types>( collection, i );
          if ( mask.cast() ) { buffer.write( dataIn[i] ); }
        }
      }
    }
    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer& buffer, Collection& collection, Path path ) {
      auto* dataOut = collection.data( path );
      if constexpr ( Unique ) {
        buffer.read( dataOut[0] );
      } else {
        for ( std::size_t i = 0; i < collection.size(); i++ ) {
          auto mask = path.template isValid<SIMDWrapper::scalar::types>( collection, i );
          if ( mask.cast() ) { buffer.read( dataOut[i] ); }
        }
      }
    }
    template <typename Collection, typename Path>
    static bool checkEqual( const Collection& A, const Collection& B, Path path ) {
      auto* dataA = A.data( path );
      auto* dataB = B.data( path );
      for ( std::size_t i = 0; i < A.size(); i++ ) {
        auto mask = path.template isValid<SIMDWrapper::scalar::types>( A, i );
        if ( mask.cast() ) {
          if ( std::isnan( dataA[i] ) && std::isnan( dataB[i] ) ) {
            std::cout << "NAN elements " << i << ": " << dataA[i] << " or  " << dataB[i] << std::endl;
          } else if ( dataA[i] != dataB[i] ) {
            std::cout << "Mismatch at element " << i << ": " << dataA[i] << " != " << dataB[i] << std::endl;
            return false;
          }
        }
      }
      return true;
    }
    template <typename Collection, typename Path>
    static void jsonDump( std::ostream& os, std::size_t at, const Collection& collection, Path path ) {
      os << collection.data( path )[at];
    }
  };

  template <typename T, typename scale = std::ratio<1, 1>>
  struct SOAPackFloatAs {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer& buffer, const Collection& collection, Path path ) {
      auto*       dataIn = collection.data( path );
      const float scalef = (float)scale::num / scale::den;
      for ( std::size_t i = 0; i < collection.size(); i++ ) {
        auto mask = path.template isValid<SIMDWrapper::scalar::types>( collection, i );
        if ( mask.cast() ) {
          T val = std::min( std::max( std::roundf( dataIn[i] * scalef ), (float)std::numeric_limits<T>::lowest() ),
                            (float)std::numeric_limits<T>::max() );
          buffer.write( val );
        }
      }
    }
    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer& buffer, Collection& collection, Path path ) {
      auto*       dataOut = collection.data( path );
      const float scalef  = (float)scale::den / scale::num;
      for ( std::size_t i = 0; i < collection.size(); i++ ) {
        auto mask = path.template isValid<SIMDWrapper::scalar::types>( collection, i );
        if ( mask.cast() ) {
          T val;
          buffer.read( val );
          dataOut[i] = (float)val * scalef;
        }
      }
    }
    template <typename Collection, typename Path>
    static bool checkEqual( const Collection& A, const Collection& B, Path path ) {
      auto*       dataA  = A.data( path );
      auto*       dataB  = B.data( path );
      const float scalef = (float)scale::den / scale::num;
      for ( std::size_t i = 0; i < A.size(); i++ ) {
        auto mask = path.template isValid<SIMDWrapper::scalar::types>( A, i );
        if ( mask.cast() ) {
          T valA = std::min( std::max( std::roundf( dataA[i] * scalef ), (float)std::numeric_limits<T>::lowest() ),
                             (float)std::numeric_limits<T>::max() );
          T valB = std::min( std::max( std::roundf( dataB[i] * scalef ), (float)std::numeric_limits<T>::lowest() ),
                             (float)std::numeric_limits<T>::max() );
          if ( std::isnan( valA ) && std::isnan( valB ) ) {
            std::cout << "NAN elements " << i << ": " << valA << " or  " << valB << std::endl;
          } else if ( valA != valB ) {
            std::cout << "Mismatch at element " << i << ": " << valA << " != " << valB << std::endl;
            return false;
          }
        }
      }
      return true;
    }
    template <typename Collection, typename Path>
    static void jsonDump( std::ostream& os, std::size_t at, const Collection& collection, Path path ) {
      os << collection.data( path )[at];
    }
  };

  struct SOADontPack {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer&, const Collection&, Path ) {}
    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer&, Collection&, Path ) {}
    template <typename Collection, typename Path>
    static bool checkEqual( const Collection&, const Collection&, Path ) {
      return true;
    }
    template <typename Collection, typename Path>
    static void jsonDump( std::ostream& os, std::size_t, const Collection&, Path ) {
      os << "\"NOT_PACKED\"";
    }
  };

  template <std::size_t version, typename trueType, typename falseType = SOADontPack>
  struct SOAPackIfVersionNewerOrEqual {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer& buffer, const Collection& collection, Path path ) {
      if ( collection.version() >= version ) {
        trueType::pack( buffer, collection, path );
      } else {
        falseType::pack( buffer, collection, path );
      }
    }
    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer& buffer, Collection& collection, Path path ) {
      if ( collection.version() >= version ) {
        trueType::unpack( buffer, collection, path );
      } else {
        falseType::unpack( buffer, collection, path );
      }
    }
    template <typename Collection, typename Path>
    static bool checkEqual( const Collection& A, const Collection& B, Path path ) {
      if ( A.version() >= version ) { return trueType::checkEqual( A, B, path ); }
      return falseType::checkEqual( A, B, path );
    }
    template <typename Collection, typename Path>
    static void jsonDump( std::ostream& os, std::size_t at, const Collection& collection, Path path ) {
      if ( collection.version() >= version ) {
        trueType::jsonDump( os, at, collection, path );
      } else {
        falseType::jsonDump( os, at, collection, path );
      }
    }
  };

  template <typename T, typename SizeTag>
  struct SOAPackVector {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer& buffer, const Collection& collection, Path path ) {
      // serialize sizes
      auto path2size = path.template append<SizeTag>();
      SizeTag::packer_t::pack( buffer, collection, path2size );
      // compress-serialize elements
      const auto* vector_tree = collection.data( path.template append<std::true_type>() );
      for ( std::size_t i = 0; i < vector_tree->maxSize(); i++ ) {
        T::pack( buffer, collection, path.append( SOAPathIfSmallerThan{i, path2size} ) );
      }
    }

    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer& buffer, Collection& collection, Path path ) {
      // deserialize sizes
      auto path2size = path.template append<SizeTag>();
      SizeTag::packer_t::unpack( buffer, collection, path2size );
      auto*             sizes    = collection.data( path2size );
      const std::size_t max_size = *std::max_element( sizes, sizes + collection.size() );
      // expand-deserialize elements
      auto* vector_tree = collection.data( path.template append<std::true_type>() );
      vector_tree->resize( max_size );
      for ( std::size_t i = 0; i < max_size; i++ ) {
        T::unpack( buffer, collection, path.append( SOAPathIfSmallerThan{i, path2size} ) );
      }
    }

    template <typename Collection, typename Path>
    static bool checkEqual( const Collection& A, const Collection& B, Path path ) {
      auto path2size = path.template append<SizeTag>();
      if ( !SizeTag::packer_t::checkEqual( A, B, path2size ) ) return false;
      const auto* vector_tree = A.data( path.template append<std::true_type>() );
      for ( std::size_t i = 0; i < vector_tree->maxSize(); i++ ) {
        if ( !T::checkEqual( A, B, path.append( SOAPathIfSmallerThan{i, path2size} ) ) ) return false;
      }
      return true;
    }
    template <typename Collection, typename Path>
    static void jsonDump( std::ostream& os, std::size_t at, const Collection& collection, Path path ) {
      os << "[";
      std::size_t size = collection.data( path.template append<std::true_type>() )->sizes().data()[at];
      for ( std::size_t i = 0; i < size; i++ ) {
        T::jsonDump( os, at, collection, path.append( i ) );
        if ( i < size - 1 ) os << ",";
      }
      os << "]";
    }
  };

  template <typename T, std::size_t... Ns>
  struct SOAPackNDArray {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer& buffer, const Collection& collection, Path path ) {
      for ( std::size_t i = 0; i < ( 1 * ... * Ns ); i++ ) { T::pack( buffer, collection, path.append( i ) ); }
    }

    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer& buffer, Collection& collection, Path path ) {
      for ( std::size_t i = 0; i < ( 1 * ... * Ns ); i++ ) { T::unpack( buffer, collection, path.append( i ) ); }
    }

    template <typename Collection, typename Path>
    static bool checkEqual( const Collection& A, const Collection& B, Path path ) {
      for ( std::size_t i = 0; i < ( 1 * ... * Ns ); i++ ) {
        if ( !T::checkEqual( A, B, path.append( i ) ) ) return false;
      }
      return true;
    }
    template <typename Collection, typename Path>
    static void jsonDump( std::ostream& os, std::size_t at, const Collection& collection, Path path ) {
      os << "[";
      for ( std::size_t i = 0; i < ( 1 * ... * Ns ); i++ ) { // TODO: true Nd array
        T::jsonDump( os, at, collection, path.append( i ) );
        if ( i < ( 1 * ... * Ns ) - 1 ) os << ",";
      }
      os << "]";
    }
  };

  template <typename T, std::size_t N>
  struct SOAPackArray {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer& buffer, const Collection& collection, Path path ) {
      for ( std::size_t i = 0; i < N; i++ ) { T::pack( buffer, collection, path.append( i ) ); }
    }

    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer& buffer, Collection& collection, Path path ) {
      for ( std::size_t i = 0; i < N; i++ ) { T::unpack( buffer, collection, path.append( i ) ); }
    }

    template <typename Collection, typename Path>
    static bool checkEqual( const Collection& A, const Collection& B, Path path ) {
      for ( std::size_t i = 0; i < N; i++ ) {
        if ( !T::checkEqual( A, B, path.append( i ) ) ) return false;
      }
      return true;
    }
    template <typename Collection, typename Path>
    static void jsonDump( std::ostream& os, std::size_t at, const Collection& collection, Path path ) {
      os << "[";
      for ( std::size_t i = 0; i < N; i++ ) {
        T::jsonDump( os, at, collection, path.append( i ) );
        if ( i < N - 1 ) os << ",";
      }
      os << "]";
    }
  };

  template <typename... Tags>
  struct SOAPackStruct {

    template <typename Tag, typename Buffer, typename Collection, typename Path>
    static void pack_debug( Buffer& buffer, const Collection& collection, Path path ) {
      auto pos_before = buffer.pos();
      Tag::packer_t::pack( buffer, collection, path );
      std::cout << "Packed " << System::typeinfoName( typeid( Tag ) ) << " from " << pos_before << " to "
                << buffer.pos() << " (" << ( buffer.pos() - pos_before ) << " bytes)" << std::endl;
    }

    template <typename Tag, typename Buffer, typename Collection, typename Path>
    static void unpack_debug( Buffer& buffer, const Collection& collection, Path path ) {
      auto pos_before = buffer.pos();
      Tag::packer_t::unpack( buffer, collection, path );
      std::cout << "Unpacked " << System::typeinfoName( typeid( Tag ) ) << " from " << pos_before << " to "
                << buffer.pos() << " (" << ( buffer.pos() - pos_before ) << " bytes)" << std::endl;
    }

    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer& buffer, const Collection& collection, Path path ) {
      ( Tags::packer_t::pack( buffer, collection, path.template append<Tags>() ), ... );
    }

    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer& buffer, Collection& collection, Path path ) {
      ( Tags::packer_t::unpack( buffer, collection, path.template append<Tags>() ), ... );
    }

    template <bool debug, typename Buffer, typename Collection>
    static void pack( Buffer& buffer, const Collection& collection ) {
      if constexpr ( debug ) {
        ( pack_debug<Tags>( buffer, collection, SOAPath<Tags>{std::make_tuple( Tags{} )} ), ... );
      } else {
        ( Tags::packer_t::pack( buffer, collection, SOAPath<Tags>{std::make_tuple( Tags{} )} ), ... );
      }
    }

    template <bool debug, typename Buffer, typename Collection>
    static void unpack( Buffer& buffer, Collection& collection ) {
      if constexpr ( debug ) {
        ( unpack_debug<Tags>( buffer, collection, SOAPath<Tags>{std::make_tuple( Tags{} )} ), ... );
      } else {
        ( Tags::packer_t::unpack( buffer, collection, SOAPath<Tags>{std::make_tuple( Tags{} )} ), ... );
      }
    }

    template <typename Tag, typename Collection, typename Path>
    static bool checkEqual_debug( const Collection& A, const Collection& B, Path path ) {
      std::cout << "Checking " << System::typeinfoName( typeid( Tag ) ) << std::endl;
      return Tag::packer_t::checkEqual( A, B, path );
    }

    template <typename Collection, typename Path>
    static bool checkEqual( const Collection& A, const Collection& B, Path path ) {
      return ( Tags::packer_t::checkEqual( A, B, path.template append<Tags>() ) && ... );
    }

    template <typename Collection>
    static bool checkEqual( const Collection& A, const Collection& B ) {
      return ( checkEqual_debug<Tags>( A, B, SOAPath<Tags>{std::make_tuple( Tags{} )} ) && ... );
    }

    template <typename Tag>
    static std::string tagName() {
      auto n = System::typeinfoName( typeid( Tag ) );
      return n.substr( n.find_last_of( ':' ) + 1 );
    }

    template <typename Collection, typename Path>
    static void jsonDump( std::ostream& os, std::size_t at, const Collection& collection, Path path ) {
      os << "{";
      std::size_t i{0};
      ( ( os << "\"" << tagName<Tags>() << "\":",
          Tags::packer_t::jsonDump( os, at, collection, path.template append<Tags>() ),
          os << ( ++i != sizeof...( Tags ) ? "," : "" ) ),
        ... );
      os << "}";
    }

    template <typename Collection>
    static void jsonDump( std::ostream& os, std::size_t at, const Collection& collection ) {
      os << "{";
      std::size_t i{0};
      ( ( os << "\"" << tagName<Tags>() << "\":",
          Tags::packer_t::jsonDump( os, at, collection, SOAPath<Tags>{std::make_tuple( Tags{} )} ),
          os << ( ++i != sizeof...( Tags ) ? "," : "" ) ),
        ... );
      os << "}";
    }
  };
} // namespace LHCb::Event
