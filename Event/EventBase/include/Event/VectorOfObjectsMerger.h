/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class SOACollectionMerger.h SOACollectionMerger
 *
 *  Merge several std::vector<Type> into one. Template to work with all std::vector<Type>
 *
 *  @author Michel De Cian
 *  @date   24/08/2022
 */

#include "LHCbAlgs/MergingTransformer.h"
#include <string>
namespace LHCb {

  template <typename Type, typename Container = std::vector<Type>>
  struct VectorOfObjectsMerger final
      : Algorithm::MergingTransformer<Container( const Gaudi::Functional::vector_of_const_<Container>& )> {
    VectorOfObjectsMerger( const std::string& name, ISvcLocator* pSvcLocator )
        : Algorithm::MergingTransformer<Container( const Gaudi::Functional::vector_of_const_<Container>& )>{
              name, pSvcLocator, {"InputLocations", {}}, {"OutputLocation", {}}} {}

    Container operator()( const Gaudi::Functional::vector_of_const_<Container>& lists ) const override {

      if ( lists.size() == 0 ) return {};

      Container   out;
      std::size_t overallSize = 0;
      for ( const auto& list : lists ) overallSize += list.size();

      out.reserve( overallSize );
      m_nbObjectsCounter += overallSize;

      for ( const auto& list : lists ) {
        if ( list.empty() ) continue;
        out.insert( out.end(), list.begin(), list.end() );
      }

      return out;
    }

  private:
    mutable Gaudi::Accumulators::SummingCounter<> m_nbObjectsCounter{this, "Nb of Merged Objects"};
  };
} // namespace LHCb
