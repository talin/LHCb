/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Event/LumiEventCounter.h>

#define BOOST_TEST_MODULE test_LumiEventCounter
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <Gaudi/MonitoringHub.h>
#include <algorithm>
#include <utility>
#include <vector>

using LC = LHCb::LumiEventCounter;

BOOST_AUTO_TEST_CASE( empty ) {
  LC cnt;

  BOOST_TEST( cnt.empty() );
  BOOST_TEST( cnt.get( 1000 ) == 0 );
  BOOST_TEST( cnt.empty() );
}

BOOST_AUTO_TEST_CASE( filling ) {
  LC cnt;

  cnt.inc( 1000 );
  BOOST_TEST( !cnt.empty() );
  BOOST_TEST( cnt.get( 1000 ) == 1 );

  cnt.inc( 1000 );
  cnt.inc( 1000 );
  BOOST_TEST( cnt.get( 1000 ) == 3 );

  cnt.inc( 2002, 100 );
  BOOST_TEST( cnt.get( 1000 ) == 3 );
  BOOST_TEST( cnt.get( 2002 ) == 100 );

  BOOST_TEST( cnt.get( 3001 ) == 0 );
  cnt.inc( 3001 );
  BOOST_TEST( cnt.get( 3001 ) == 1 );

  BOOST_TEST( !cnt.empty() );
}

BOOST_AUTO_TEST_CASE( reset ) {
  LC cnt;

  BOOST_TEST( cnt.empty() );
  cnt.inc( 1000 );
  BOOST_TEST( !cnt.empty() );
  BOOST_TEST( cnt.get( 1000 ) == 1 );
  cnt.reset();
  BOOST_TEST( cnt.empty() );
}

BOOST_AUTO_TEST_CASE( to_json ) {
  LC cnt;

  nlohmann::json j = cnt;
  BOOST_TEST( j["type"].get<std::string>() == "LumiEventCounter" );
  BOOST_TEST( j["empty"].get<bool>() );
  BOOST_TEST( j["counts"].empty() );

  cnt.inc( 1000 );
  cnt.inc( 2000, 5 );
  j = cnt;
  BOOST_TEST( !j["empty"].get<bool>() );
  BOOST_TEST( j["counts"].size() == 2 );
  BOOST_TEST( j["counts"]["1000"].get<std::size_t>() == 1 );
  BOOST_TEST( j["counts"]["2000"].get<std::size_t>() == 5 );
}

BOOST_AUTO_TEST_CASE( monitoring ) {
  using Gaudi::Monitoring::Hub;
  using Entity = Gaudi::Monitoring::Hub::Entity;
  using Sink   = Gaudi::Monitoring::Hub::Sink;
  // Mock code for the test
  struct MonitoringHub : Hub {};
  struct ServiceLocator {
    MonitoringHub& monitoringHub() { return m_monitHub; }
    MonitoringHub  m_monitHub{};
  };
  struct Component {
    ServiceLocator* serviceLocator() { return &m_serviceLocator; }
    std::string     name() const { return "owner"; }
    ServiceLocator  m_serviceLocator{};
  } alg;
  struct MySink : Sink {
    void registerEntity( Entity ent ) override { entities.emplace_back( std::move( ent ) ); }
    void removeEntity( Entity const& ent ) override {
      auto it = std::remove( entities.begin(), entities.end(), ent );
      entities.erase( it, entities.end() );
    }
    std::vector<Entity> entities;
    Hub&                hub;
    MySink( Hub& h ) : hub{h} { hub.addSink( this ); }
    ~MySink() { hub.removeSink( this ); }
  };
  MySink sink( alg.serviceLocator()->monitoringHub() );

  BOOST_TEST( sink.entities.empty() );

  {
    LC cnt{&alg, "lumi_cnt"};
    BOOST_TEST( sink.entities.size() == 1 );

    auto j = sink.entities[0].toJSON();
    BOOST_TEST( j["type"].get<std::string>() == "LumiEventCounter" );
    BOOST_TEST( j["empty"].get<bool>() );
    BOOST_TEST( j["counts"].empty() );

    cnt.inc( 1000 );
    j = sink.entities[0].toJSON();
    BOOST_TEST( !j["empty"].get<bool>() );
    BOOST_TEST( j["counts"].size() == 1 );
    BOOST_TEST( j["counts"]["1000"].get<std::size_t>() == 1 );
  }

  BOOST_TEST( sink.entities.empty() );
}
