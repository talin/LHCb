/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/LumiFSR.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/VectorMap.h"
#include <math.h>
#include <ostream>
#include <vector>

#if defined( __clang__ ) && ( __clang_major__ > 10 )
#  define LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_BEGIN                                                      \
    _Pragma( "clang diagnostic push" ) _Pragma( "clang diagnostic ignored \"-Wrange-loop-construct\"" )
#  define LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_END _Pragma( "clang diagnostic pop" )
#elif defined( __GNUC__ ) && __GNUC__ > 10
#  define LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_BEGIN                                                      \
    _Pragma( "GCC diagnostic push" ) _Pragma( "GCC diagnostic ignored \"-Wrange-loop-construct\"" )
#  define LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_END _Pragma( "GCC diagnostic pop" )
#else
#  define LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_BEGIN
#  define LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_END
#endif

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_LumiIntegral = 13502;

  // Namespace for locations in TDS
  namespace LumiIntegralLocation {
    inline const std::string Default = "/FileRecords/EOR/LumiIntegral";
  }

  /** @class LumiIntegral LumiIntegral.h
   *
   * Accounting class for Lumi in FSR
   *
   * @author Jaap Panman
   *
   */

  class LumiIntegral final : public KeyedObject<int> {
  public:
    /// typedef for std::vector of LumiIntegral
    typedef std::vector<LumiIntegral*>       Vector;
    typedef std::vector<const LumiIntegral*> ConstVector;

    /// typedef for KeyedContainer of LumiIntegral
    typedef KeyedContainer<LumiIntegral, Containers::HashMap> Container;

    /// For User information
    typedef std::pair<int, double> ValuePair;
    /// User information
    typedef GaudiUtils::VectorMap<int, ValuePair> ExtraInfo;
    /// For User information
    typedef std::pair<int, long long> LongValuePair;
    /// User information
    typedef GaudiUtils::VectorMap<int, LongValuePair> LongExtraInfo;
    /// Set of runNumbers
    typedef std::vector<unsigned int> RunNumbers;
    /// Set of file IDs
    typedef std::vector<std::string> FileIDs;

    /// Default Constructor
    LumiIntegral() : m_runNumbers(), m_fileIDs(), m_extraInfo() {}

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::LumiIntegral::classID(); }
    static const CLID& classID() { return CLID_LumiIntegral; }

    /// Assignment operator
    LumiIntegral& operator=( const LumiIntegral& rhs );

    /// add a RunNumber to the set
    void addRunNumber( unsigned int number ) { m_runNumbers.push_back( number ); }

    /// has information for specified runNumber
    bool hasRunNumber( unsigned int number ) const {
      return m_runNumbers.end() != find( m_runNumbers.begin(), m_runNumbers.end(), number );
    }

    /// Addition operator
    LumiIntegral& mergeRuns( const RunNumbers& rhs );

    /// add a fileID to the set
    void addFileID( std::string& idString ) { m_fileIDs.push_back( idString ); }

    /// has information for specified fileID
    bool hasFileID( const std::string& idString ) const {
      return m_fileIDs.end() != find( m_fileIDs.begin(), m_fileIDs.end(), idString );
    }

    /// Addition operator
    LumiIntegral& mergeFileIDs( const FileIDs& rhs );

    /// ExtraInformation. Don't use directly, use hasInfo, info, addInfo...
    const ExtraInfo& extraInfo() const { return m_extraInfo; }

    /// has information for specified key
    bool hasInfo( int key ) const { return m_extraInfo.end() != m_extraInfo.find( key ); }

    ///  Add new information associated with the specified key. This method cannot be used
    ///  to modify information for a pre-existing key.
    bool addInfo( const int key, const int incr, const double count ) {
      return m_extraInfo.insert( key, {incr, count} ).second;
    }

    /// extract the information associated with the given key. If there is no such
    /// information the default value will be returned.
    ValuePair info( const int key, const ValuePair& def ) const;

    /// erase the information associated with the given key
    int eraseInfo( int key ) { return m_extraInfo.erase( key ); }

    /// scales the values for existing keys with a vector of double
    LumiIntegral& scale( const std::vector<double>& scalefactor );

    /// sums the values for existing keys and inserts a new key if needed
    LumiIntegral& mergeInfo( const ExtraInfo& rhs );

    /// sums the values for existing keys and inserts a new key if needed
    LumiIntegral& mergeInfo( const LongExtraInfo& rhs );

    /// sums the values for existing keys normalized to *this
    LumiIntegral& mergeInfoNormalized( const ExtraInfo& rhs, const double factor );

    /// sums the values for existing keys normalized to *this
    LumiIntegral& mergeInfoNormalized( const LongExtraInfo& rhs, const double factor );

    /// sums the values for existing keys normalized to *this
    LumiIntegral& mergeInfoNormalized( const ExtraInfo& rhs, const double factor, const int fromkey );

    /// sums the values for existing keys normalized to *this
    LumiIntegral& mergeInfoNormalized( const LongExtraInfo& rhs, const double factor, const int fromkey );

    /// increments the values for existing keys and inserts a new key if needed
    LumiIntegral& incrementInfo( const int key, const double count );

    /// Addition (increment) operator
    LumiIntegral& operator+=( const LHCb::LumiIntegral& rhs );

    /// Addition (increment) operator
    LumiIntegral& operator+=( const LHCb::LumiFSR& rhs );

    /// Add scaled to norm
    LumiIntegral& addNormalized( const LHCb::LumiIntegral& rhs, const double factor );

    /// Add scaled to norm
    LumiIntegral& addNormalized( const LHCb::LumiFSR& rhs, const double factor );

    /// Add scaled to norm
    LumiIntegral& addNormalized( const LHCb::LumiIntegral& rhs, const double factor, const int fromkey );

    /// Add scaled to norm
    LumiIntegral& addNormalized( const LHCb::LumiFSR& rhs, const double factor, const int fromkey );

    /// intelligent printout
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve const  Set of run numbers summed up in this job
    const RunNumbers& runNumbers() const { return m_runNumbers; }

    /// Update  Set of run numbers summed up in this job
    LumiIntegral& setRunNumbers( const RunNumbers& value ) {
      m_runNumbers = value;
      return *this;
    }

    /// Retrieve const  Set of file IDs summed up in this job
    const FileIDs& fileIDs() const { return m_fileIDs; }

    /// Update  Set of file IDs summed up in this job
    LumiIntegral& setFileIDs( const FileIDs& value ) {
      m_fileIDs = value;
      return *this;
    }

    /// Update  Some additional user information. Don't use directly. Use *Info() methods.
    LumiIntegral& setExtraInfo( const ExtraInfo& value ) {
      m_extraInfo = value;
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const LumiIntegral& obj ) { return obj.fillStream( str ); }

  private:
    RunNumbers m_runNumbers; ///< Set of run numbers summed up in this job
    FileIDs    m_fileIDs;    ///< Set of file IDs summed up in this job
    ExtraInfo  m_extraInfo;  ///< Some additional user information. Don't use directly. Use
                             ///< *Info() methods.

  }; // class LumiIntegral

  /// Definition of Keyed Container for LumiIntegral
  typedef KeyedContainer<LumiIntegral, Containers::HashMap> LumiIntegrals;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline LHCb::LumiIntegral& LHCb::LumiIntegral::operator=( const LumiIntegral& rhs ) {
  if ( this != &rhs ) {
    m_runNumbers = rhs.m_runNumbers;
    m_fileIDs    = rhs.m_fileIDs;
    m_extraInfo  = rhs.m_extraInfo;
  }
  return *this;
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::mergeRuns( const RunNumbers& rhs ) {
  // concatenate run number set (cannot use "set" - have to do it by hand!)
  for ( auto rn : rhs ) {
    if ( !hasRunNumber( rn ) ) { m_runNumbers.push_back( rn ); }
  }
  return *this;
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::mergeFileIDs( const FileIDs& rhs ) {
  // concatenate fileID set (cannot use "set" - have to do it by hand!)
  for ( auto id : rhs ) {
    if ( !hasFileID( id ) ) { m_fileIDs.push_back( id ); }
  }
  return *this;
}

inline LHCb::LumiIntegral::ValuePair LHCb::LumiIntegral::info( const int key, const ValuePair& def ) const {
  auto i = m_extraInfo.find( key );
  return m_extraInfo.end() == i ? def : i->second;
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::scale( const std::vector<double>& scalefactor ) {
  // scale existing info values
  LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_BEGIN
  for ( auto const [key, value] : m_extraInfo ) { // copy, as eraseInfo will modify value otherwise
    eraseInfo( key );
    double scale = scalefactor[key];
    addInfo( key, value.first, value.second * scale );
  }
  LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_END
  return *this;
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::mergeInfo( const ExtraInfo& rhs ) {
  // sum info or add new key
  LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_BEGIN
  for ( auto const [key, valueB] : rhs ) { // copy, as eraseInfo will modify valueB otherwise
    ValuePair valueA = info( key, {0, 0} );
    if ( hasInfo( key ) ) { eraseInfo( key ); }
    addInfo( key, valueA.first + valueB.first, valueA.second + valueB.second );
  }
  LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_END
  return *this;
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::mergeInfo( const LongExtraInfo& rhs ) {
  // sum info or add new key
  LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_BEGIN
  for ( auto const [key, valueB] : rhs ) { // copy, as eraseInfo will modify valueB
    ValuePair valueA = info( key, {0, 0} );
    if ( hasInfo( key ) ) { eraseInfo( key ); }
    addInfo( key, valueA.first + valueB.first, valueA.second + (double)valueB.second );
  }
  LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_END
  return *this;
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::mergeInfoNormalized( const ExtraInfo& rhs, const double factor ) {
  // sum info or add new key
  LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_BEGIN
  for ( auto const [key, valueB] : rhs ) { // copy, as eraseInfo will modify valueB otherwise
    if ( valueB.first ) {
      ValuePair valueA = info( key, {0, 0} );
      if ( hasInfo( key ) ) { eraseInfo( key ); }
      double scale = (double)valueA.first / (double)valueB.first;
      addInfo( key, valueA.first, valueA.second + factor * valueB.second * scale );
    }
  }
  LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_END
  return *this;
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::mergeInfoNormalized( const LongExtraInfo& rhs, const double factor ) {

  // sum info or add new key
  LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_BEGIN
  for ( auto const [key, valueB] : rhs ) { // copy, as eraseInfo will modify valueB otherwise
    ValuePair valueA = info( key, {0, 0} );
    if ( valueB.first ) {
      if ( hasInfo( key ) ) { eraseInfo( key ); }
      double scale = (double)valueA.first / (double)valueB.first;
      addInfo( key, valueA.first, valueA.second + factor * (double)valueB.second * scale );
    }
  }
  LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_END
  return *this;
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::mergeInfoNormalized( const ExtraInfo& rhs, const double factor,
                                                                    const int fromkey ) {

  // sum info or add new key
  LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_BEGIN
  for ( auto [key, valueB] : rhs ) { // copy, as eraseInfo will modify valueB otherwise
    ValuePair valueA = info( key, {0, 0} );
    if ( valueB.first ) {
      if ( hasInfo( key ) ) { eraseInfo( key ); }

      // scale with the existing data except if factor == 0
      double scale;
      double usefactor;
      int    increment;
      double initial;
      if ( factor == 0 ) {
        scale     = 1.;
        usefactor = 1.;
        increment = valueB.first;
        initial   = 0;
      } else {
        scale     = (double)valueA.first / (double)valueB.first;
        usefactor = factor;
        increment = valueA.first;
        initial   = valueA.second;
      }

      // add normalized - use -log from the from key
      double addition;
      if ( key < fromkey ) {
        addition = usefactor * (double)valueB.second * scale;
      } else {
        double frac = (double)valueB.second / (double)valueB.first;
        if ( frac == 1.0 ) frac = 0.995;
        addition = usefactor * ( -1. ) * log( 1.0 - frac ) * (double)increment;
      }
      addInfo( key, increment, initial + addition );
    }
  }
  LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_END
  return *this;
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::mergeInfoNormalized( const LongExtraInfo& rhs, const double factor,
                                                                    const int fromkey ) {

  // sum info or add new key
  LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_BEGIN
  for ( auto const [key, valueB] : rhs ) { // copy, as eraseInfo will modify valueB otherwise
    ValuePair valueA = info( key, {0, 0} );
    if ( valueB.first ) {
      if ( hasInfo( key ) ) { eraseInfo( key ); }

      // scale with the existing data except if factor == 0
      double scale;
      double usefactor;
      int    increment;
      double initial;
      if ( factor == 0 ) {
        scale     = 1.;
        usefactor = 1.;
        increment = valueB.first;
        initial   = 0;
      } else {
        scale     = (double)valueA.first / (double)valueB.first;
        usefactor = factor;
        increment = valueA.first;
        initial   = valueA.second;
      }

      // add normalized - use -log from the from key
      double addition;
      if ( key < fromkey ) {
        addition = usefactor * (double)valueB.second * scale;
      } else {
        double frac = (double)valueB.second / (double)valueB.first;
        if ( frac == 1.0 ) frac = 0.995;
        addition = usefactor * ( -1. ) * log( 1.0 - frac ) * (double)increment;
      }
      addInfo( key, increment, initial + addition );
    }
  }
  LHCB_LUMIINTEGRAL_SUPPRESS_FALSE_POSITIVE_WARNING_END
  return *this;
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::incrementInfo( const int key, const double count ) {

  // increment info or add new key, only if data exists
  if ( count != -1 ) {
    ValuePair value = info( key, {0, 0} );
    if ( hasInfo( key ) ) { eraseInfo( key ); }
    addInfo( key, ++value.first, value.second + count );
  }
  return *this;
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::operator+=( const LHCb::LumiIntegral& rhs ) {

  if ( this != &rhs ) {
    // merge run number sets
    mergeRuns( rhs.runNumbers() );
    // merge file ID sets
    mergeFileIDs( rhs.fileIDs() );
    // merge the extraInfo
    mergeInfo( rhs.extraInfo() );
  }
  return *this;
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::operator+=( const LHCb::LumiFSR& rhs ) {
  // merge run number sets
  mergeRuns( rhs.runNumbers() );
  // merge file ID sets
  mergeFileIDs( rhs.fileIDs() );
  // merge the extraInfo
  mergeInfo( rhs.extraInfo() );
  return *this;
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::addNormalized( const LHCb::LumiIntegral& rhs, const double factor ) {
  // merge the extraInfo
  return mergeInfoNormalized( rhs.extraInfo(), factor );
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::addNormalized( const LHCb::LumiFSR& rhs, const double factor ) {
  // merge the extraInfo
  return mergeInfoNormalized( rhs.extraInfo(), factor );
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::addNormalized( const LHCb::LumiIntegral& rhs, const double factor,
                                                              const int fromkey ) {
  // merge the extraInfo
  return mergeInfoNormalized( rhs.extraInfo(), factor, fromkey );
}

inline LHCb::LumiIntegral& LHCb::LumiIntegral::addNormalized( const LHCb::LumiFSR& rhs, const double factor,
                                                              const int fromkey ) {
  // merge the extraInfo
  return mergeInfoNormalized( rhs.extraInfo(), factor, fromkey );
}

inline std::ostream& LHCb::LumiIntegral::fillStream( std::ostream& s ) const {
  s << "{ "
    << " runs : ";
  for ( auto run : m_runNumbers ) { s << run << " "; }
  s << " files : ";
  for ( const auto& file : m_fileIDs ) { s << file << " "; }

  // overall sum info
  s << " info (key/incr/integral) : ";
  for ( auto const& [key, values] : m_extraInfo ) { s << key << " " << values.first << " " << values.second << " / "; }
  s << " }";

  return s;
}
