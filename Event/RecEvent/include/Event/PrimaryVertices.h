/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/SOACollection.h"
#include "Event/VertexBase.h"
#include "GaudiKernel/Point3DTypes.h"
#include "LHCbMath/MatVec.h"
#include <limits>

// Forward declaration
namespace LHCb::Pr::Velo {
  struct Tracks;
}

namespace LHCb::Event::PV {

  using PrVeloTracks = LHCb::Pr::Velo::Tracks;

  namespace PVTrackTag {
    // This will just be a copy of the data of the first state in the original track. At some later point we can
    // directly use that data.
    struct z : float_field {};  /// z of track state
    struct x : float_field {};  /// x of track state
    struct y : float_field {};  /// y of track state
    struct tx : float_field {}; /// tx of track state
    struct ty : float_field {}; /// ty of track state
    enum struct XTxCovMatrixElement { xx = 0, xtx, txtx, N };
    struct Vx : floats_field<int( XTxCovMatrixElement::N )> {}; /// Vxx, Vxtx, Vtxtx
    struct Vy : floats_field<int( XTxCovMatrixElement::N )> {}; /// Vyy, Tyty, Vtyty

    struct pvindex : int_field {}; /// index to the PV to which this track belongs
    struct veloindex : Event::index2D_field<const PrVeloTracks, std::index_sequence<31>> {}; /// pointer to prvelotrack
    struct weight : float_field {};                                                          /// Tukey weight
    struct ipchi2 : float_field {}; /// ip chi2 of track (which determines the Tukery weight)

    enum struct PosVectorElement { x = 0, y, z, N };
    struct halfDChi2DX : floats_field<int( PosVectorElement::N )> {}; /// Contribution the first derivative of the chi2
    enum struct PosCovMatrixElement { v00 = 0, v11, v20, v21, v22, N };
    struct halfD2Chi2DX2 : floats_field<int( PosCovMatrixElement::N )> {
    }; /// Contribution to the second derivative of the chi2, non-zero elements only (0,0),(1,1),(0,2),(1,2),(2,2)

    struct Wx : float_field {}; /// Weight of track in x (inverse of Cov(x) after extrapolation to mother vertex z)
    struct Wy : float_field {}; /// Weight of track in y (inverse of Cov(y) after extrapolation to mother vertex z)

    template <typename T>
    using pvtrack_t = SOACollection<T, z, x, y, tx, ty, Vx, Vy, pvindex, veloindex, weight, ipchi2, halfDChi2DX,
                                    halfD2Chi2DX2, Wx, Wy>;
  } // namespace PVTrackTag

  struct PVTracks : PVTrackTag::pvtrack_t<PVTracks> {
    using base_t = typename PVTrackTag::pvtrack_t<PVTracks>;
    using base_t::base_t;

    // Required to get to the velo tracks
    template <typename Tag>
    auto containers() const {
      if constexpr ( std::is_same_v<Tag, PVTrackTag::veloindex> ) { return prvelocontainers; }
    }
    std::array<const PrVeloTracks*, 2> prvelocontainers; // forward, backward

    // Define a custom proxy for user friendlyness
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct PVTrackProxy : LHCb::Event::Proxy<simd, behaviour, ContainerType> {
      // use default constructor
      using LHCb::Event::Proxy<simd, behaviour, ContainerType>::Proxy;

      // declare custom functions
      [[nodiscard]] auto z() const { return this->template get<PVTrackTag::z>(); }
      [[nodiscard]] auto x() const { return this->template get<PVTrackTag::x>(); }
      [[nodiscard]] auto y() const { return this->template get<PVTrackTag::y>(); }
      [[nodiscard]] auto tx() const { return this->template get<PVTrackTag::tx>(); }
      [[nodiscard]] auto ty() const { return this->template get<PVTrackTag::ty>(); }
      [[nodiscard]] auto covXX() const {
        return this->template get<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::xx );
      }
      [[nodiscard]] auto covXTx() const { return this->template get<PVTrackTag::Vx>( 1 ); }
      [[nodiscard]] auto covTxTx() const { return this->template get<PVTrackTag::Vx>( 2 ); }
      [[nodiscard]] auto covYY() const { return this->template get<PVTrackTag::Vy>( 0 ); }
      [[nodiscard]] auto covYTy() const { return this->template get<PVTrackTag::Vy>( 1 ); }
      [[nodiscard]] auto covTyTy() const { return this->template get<PVTrackTag::Vy>( 2 ); }
      [[nodiscard]] auto pvindex() const { return this->template get<PVTrackTag::pvindex>(); }
      [[nodiscard]] auto weight() const { return this->template get<PVTrackTag::weight>(); }
      [[nodiscard]] auto ipchi2() const { return this->template get<PVTrackTag::ipchi2>(); }
      [[nodiscard]] auto halfDChi2DX( int i ) const { return this->template get<PVTrackTag::halfDChi2DX>( i ); }
      [[nodiscard]] auto halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement i ) const {
        return this->template get<PVTrackTag::halfD2Chi2DX2>( i );
      }
      [[nodiscard]] auto Wx() const { return this->template get<PVTrackTag::Wx>(); }
      [[nodiscard]] auto Wy() const { return this->template get<PVTrackTag::Wy>(); }

      [[nodiscard, gnu::always_inline]] auto veloTrack() const {
        if constexpr ( behaviour != LHCb::Pr::ProxyBehaviour::ScatterGather2D ) {
          auto proxy = this->container()->template simd<simd>().gather2D( this->indices(), this->loop_mask() );
          return proxy.template get<PVTrackTag::veloindex>();
        } else {
          return this->template get<PVTrackTag::veloindex>();
        }
      }
    };

    // Register the proxy:
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = PVTrackProxy<simd, behaviour, ContainerType>;
  };

  // The following to function need to go into LHCbMath/MatVec.h
  // Convert between different basic types
  template <typename Out, auto N, typename In>
  auto convert( const LinAlg::MatSym<In, N>& in ) {
    using Utils::unwind;
    LinAlg::MatSym<Out, N> out;
    unwind<0, N>( [&]( auto i ) { unwind<0, i + 1>( [&]( auto j ) { out( i, j ) = in( i, j ); } ); } );
    return out;
  }
  // Convert to SMatrix
  template <typename Out, auto N, typename In>
  auto convertToSMatrix( const LHCb::LinAlg::MatSym<In, N>& in ) {
    using LHCb::Utils::unwind;
    ROOT::Math::SMatrix<Out, N, N, ROOT::Math::MatRepSym<Out, N>> out;
    unwind<0, N>( [&]( auto i ) { unwind<0, i + 1>( [&]( auto j ) { out( i, j ) = in( i, j ); } ); } );
    return out;
  }

  /// PrimaryVertex class derived from LHCb::VertexBase (which actually needs to be cleaned up)
  /// This class holds pointers (simple indices) to the first and last corresponding track in the PVTrack container.
  class PrimaryVertex final : public LHCb::VertexBase {
  private:
    using SymMatrix3x3 = LHCb::LinAlg::MatSym<double, 3>;
    uint16_t     m_begin{0};      /// Index of the first track of this PV in the track list
    uint16_t     m_end{0};        /// Last+1 index of the first track of this PV in the track list
    SymMatrix3x3 m_halfD2Chi2DX2; /// Weight matrix = inverse of covariance. Needed for unbiasing. (This
                                  /// better also be the thing we persist.)
  public:
    PrimaryVertex() {}
    // template<typename Position>
    PrimaryVertex( const Gaudi::XYZPoint& pos ) : VertexBase{pos} {}
    auto        begin() const { return m_begin; }
    auto        end() const { return m_end; }
    auto        size() const { return m_end - m_begin; }
    auto        nTracks() const { return ( nDoF() + 3 ) / 2; }
    const auto& weightMatrix() const { return m_halfD2Chi2DX2; }
    void        setWeightMatrix( const SymMatrix3x3& halfD2Chi2DX2 ) { m_halfD2Chi2DX2 = halfD2Chi2DX2; }
    template <typename T>
    void setRange( T begin, T end ) {
      m_begin = begin;
      m_end   = end;
    }
    template <typename T>
    void setSize( T size ) {
      m_end = m_begin + size;
    }
    // Interface compatibility with ThOr. This is already in the base class. Do we actually need it?
    [[nodiscard, gnu::always_inline]] friend LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>
    endVertexPos( const PrimaryVertex& vtx ) {
      auto pos = vtx.position();
      return {pos.x(), pos.y(), pos.z()};
    }
    [[nodiscard, gnu::always_inline]] friend auto posCovMatrix( const PrimaryVertex& vtx ) {
      return LHCb::LinAlg::convert<SIMDWrapper::scalar::float_v>( vtx.covMatrix() );
    }
    friend struct PrimaryVertexContainer;
  };

  using PrimaryVertices  = std::vector<PrimaryVertex, LHCb::Allocators::EventLocal<PrimaryVertex>>;
  using PVTrackIndexList = std::vector<short, LHCb::Allocators::EventLocal<short>>;
  using PVIndex          = uint8_t;

  /// Event store object that holds the PVs, the tracks in the PVs, and a table for navigating from the original velo
  /// tracks to the PV tracks, needed for unbiasing.
  struct PrimaryVertexContainer {
    PrimaryVertices  vertices;     /// vertices
    PVTracks         tracks;       /// tracks
    PVTrackIndexList velotrackmap; /// table to get from velo track index (e.g. v3::Track::trackVP) to pv track
    template <typename Allocator>
    PrimaryVertexContainer( const Allocator& alloc )
        : vertices{alloc}, tracks{Zipping::generateZipIdentifier(), alloc}, velotrackmap{alloc} {}
    PrimaryVertexContainer() = default;
    PrimaryVertexContainer( const PrimaryVertexContainer& rhs )
        : vertices{rhs.vertices}
        , tracks{Zipping::generateZipIdentifier(), rhs.tracks}
        , velotrackmap{rhs.velotrackmap} {}
    PrimaryVertexContainer( PrimaryVertexContainer&& ) = default;
    /// Forwarding functions to make this look like a container of vertices.
    auto        begin() const { return vertices.begin(); }
    auto        end() const { return vertices.end(); }
    auto        size() const { return vertices.size(); }
    auto        empty() const { return vertices.empty(); }
    const auto& front() const { return vertices.front(); }
    const auto& operator[]( size_t i ) const { return vertices[i]; }
    using value_type     = PrimaryVertices::value_type;
    using allocator_type = PrimaryVertices::allocator_type;
    /// Trivial routine to set the key of the keyed objects
    void setIndices() {
      for ( size_t i = 0; i < size(); ++i ) vertices[i].setKey( i );
    }
  };

  /// Default PV location
  inline std::string const DefaultLocation = "Rec/PrimaryVertexContainer";

  /// (re)fill the PrimaryVertex data object from the velo tracks. This needs the 'pvtrackindex' column to be filled,
  /// since that is used for navigation.
  void populateFromVeloTracks( PrimaryVertexContainer& pvdata, const LHCb::Pr::Velo::Tracks& tracksForward,
                               const LHCb::Pr::Velo::Tracks& tracksBackward );

  /// Fit a single PV. Warning: due to padding this will invalidate some data in the next vertex. So, make sure to fit
  /// all of them in order if you need more than one vertex.
  void fitAdaptive( PrimaryVertex& pv, PVTracks& pvtracks, float chi2max = 10.0, float maxDeltaZConverged = 0.001,
                    float maxDeltaChi2Converged = 0.01, uint16_t maxNumIter = 10, uint16_t numAnnealingSteps = 0 );

  /// Perform the adaptive vertex fit to all PVs.
  void fitAdaptive( PrimaryVertices& vertices, PVTracks& pvtracks, float chi2max = 10.0,
                    float maxDeltaZConverged = 0.001, float maxDeltaChi2Converged = 0.01, uint16_t maxNumIter = 10,
                    uint16_t numAnnealingSteps = 0 );

  /// Returns the list of unbiased vertices
  PrimaryVertices unbiasedVertices( const PrimaryVertexContainer& pvdata, LHCb::span<int> vetoedvelotrackindices );

  /// Returns a single unbiased primary vertex.
  PrimaryVertex unbiasedVertex( const PrimaryVertexContainer& pvdata, int pvindex,
                                LHCb::span<int> vetoedvelotrackindices );

  /// Refits the entire set of vertices updating the track weights
  PrimaryVertices refittedVertices( const PrimaryVertexContainer& pvdata, LHCb::span<int> vetoedvelotrackindices,
                                    float chi2max, float maxDeltaZConverged = 0.001, float maxDeltaChi2Converged = 0.01,
                                    uint16_t maxNumIter = 10 );

  /// Retrieves the closest PV for this velo track.
  inline auto closestPV( const PrimaryVertexContainer& pvdata, int veloindex ) {
    return pvdata.tracks.scalar()[pvdata.velotrackmap[veloindex]].pvindex();
  }

  /// Return the index of the best pointing PV for this state (which could bederived from a particle). This can be
  /// vectorized.
  template <typename FTYPE>
  inline auto bestPVIndex( const PrimaryVertexContainer& pvdata, const FTYPE x, const FTYPE y, const FTYPE z,
                           const FTYPE tx, const FTYPE ty ) {
    PVIndex index{0};
    auto    mind2 = std::numeric_limits<FTYPE>::max();
    for ( PVIndex ipv = 0; ipv < pvdata.size(); ++ipv ) {
      const auto& pv = pvdata.vertices[ipv];
      auto        dz = pv.position().z() - z;
      auto        dx = pv.position().x() - ( x + dz * tx );
      auto        dy = pv.position().y() - ( y + dz * ty );
      auto        d2 = dx * dx + dy * dy;
      if ( mind2 > d2 ) {
        index = ipv;
        mind2 = d2;
      }
    }
    return index;
  }

  /// Return a pointer to the best PrimaryVertex for a given track state.
  template <typename FTYPE>
  inline auto bestPV( const PrimaryVertexContainer& pvdata, const FTYPE x, const FTYPE y, const FTYPE z, const FTYPE tx,
                      const FTYPE ty ) {
    return pvdata.size() > 0 ? &( pvdata[bestPVIndex( pvdata, x, y, z, tx, ty )] ) : (const PrimaryVertex*)( nullptr );
  }

  template <typename Position, typename Direction>
  inline auto bestPV( const PrimaryVertexContainer& pvdata, const Position pos, const Direction dir ) {
    return bestPV( pvdata, pos.x(), pos.y(), pos.z(), dir.x() / dir.z(), dir.y() / dir.z() );
  }

  // Retrieves a fast estimate for the ip chi2 of this velo track
  inline auto fastIPChi2( const PrimaryVertexContainer& pvdata, int veloindex ) {
    return pvdata.tracks.scalar()[pvdata.velotrackmap[veloindex]].ipchi2();
  }

} // namespace LHCb::Event::PV
