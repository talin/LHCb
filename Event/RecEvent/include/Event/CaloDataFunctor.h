/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Detector/Calo/CaloCellID.h"
#include "Event/CaloCluster.h"
#include "Event/CaloClusterEntry.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDigit.h"
#include "Event/CaloDigitStatus.h"
#include "Event/CaloHypo.h"
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/detected.h"
#include <cmath>
#include <functional>
#include <iterator>
#include <optional>
#include <type_traits>

template <typename TYPE>
class SmartRef; // GaudiKernel

/** @namespace CaloDataFunctor CaloDataFunctor.h Event/CaloDataFunctor.h
 *
 *  collection of some  useful functors, which could be used for
 *  manipulation with CaloDigit, MCCaloDigit, MCCaloHit,
 *  MCCaloSensPlaneHit, CaloCluster and CaloParticle objects
 *
 *  @author  Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date    26/11/1999
 */
namespace LHCb::Calo::Functor {
  // ==========================================================================
  /** @class CellID
   *
   *  The simple structure that "extracts" CaloCellID code from the class
   *  The 'generic implemntation relies on existence of TYPE::cellID() method
   *
   *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
   *  @date 2004-10-22
   */
  // ==========================================================================
  namespace details {
    template <typename T>
    using has_cellID_ = decltype( std::declval<const T&>().cellID() );

    template <typename T>
    inline constexpr bool has_cellID_v = Gaudi::cpp17::is_detected_v<has_cellID_, T>;

    template <typename T>
    using has_seed_ = decltype( std::declval<const T&>().seed() );

    template <typename T>
    inline constexpr bool has_seed_v = Gaudi::cpp17::is_detected_v<has_seed_, T>;

  } // namespace details
  struct CellID_t {
    template <typename TYPE, typename = std::enable_if_t<details::has_cellID_v<TYPE> || details::has_seed_v<TYPE>>>
    Detector::Calo::CellID operator()( const TYPE& obj ) const {
      if constexpr ( details::has_cellID_v<TYPE> ) {
        return obj.cellID();
      } else {
        return obj.seed();
      }
    }

    template <typename TYPE>
    constexpr Detector::Calo::CellID operator()( const TYPE* obj ) const {
      return obj ? ( *this )( *obj ) : Detector::Calo::CellID{};
    }

    template <typename TYPE>
    Detector::Calo::CellID operator()( const SmartRef<TYPE>& ref ) const {
      return ( *this )( ref.target() );
    }

    constexpr Detector::Calo::CellID operator()( Detector::Calo::CellID id ) const { return id; };

    Detector::Calo::CellID operator()( const CaloHypo& hypo ) const {
      return !hypo.clusters().empty() ? ( *this )( hypo.clusters().front() ) : Detector::Calo::CellID{};
    }
  };
  inline constexpr CellID_t cellID{};
  // ==========================================================================

  // ==========================================================================
  /** @class Over_E_Threshold CaloDataFunctor.h Event/CaloDataFuctor.h
   *
   *  Comparison of the energy of the object with given threshold
   *
   *  Example:
   *  select all digits with are over the threshold:
   *  @code
   *  CaloDigits* digits = ... ;
   *  Over_E_Threshold cmp( 1 * GeV );
   *  CaloDigits::const_iterator it =
   *  std::stable_partition( digits->begin() ,
   *                         digits->end()   ,
   *                         cmp             );
   *  @endcode
   *  Here for all digits with exceed the threshold are placed
   *  before @p it
   *
   *  @see MCCaloHit
   *  @see MCCaloDigit
   *  @see CaloDigit
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 26/11/1999
   */
  class Over_E_Threshold {
  public:
    /** constructor (explicit)
     *  @param threshold   threshold on energy of the object
     */
    explicit Over_E_Threshold( float threshold = 0.0f ) : m_threshold( threshold ){};
    /** compare the energy of the object with threshold value
     *  @param  obj  object
     *  @return result of comparison with threshold
     */
    template <typename TYPE>
    bool operator()( const TYPE& obj ) const {
      return obj && obj->e() > m_threshold;
    }

    bool operator()( const CaloClusterEntry& obj ) const {
      return obj.digit() && ( obj.digit()->e() * obj.fraction() > m_threshold );
    };

  private:
    float m_threshold{0}; ///< the actual threshold value for the energy
  };
  // ==========================================================================

  // ==========================================================================
  /** @class EnergyTransverse CaloDataFunctor.h Event/CaloDataFucntor.h
   *
   *  Calculate the transverse energy for the object
   *
   *  "TYPE" is required to have valid comparison with 0 and
   *  implemented "->e()" methods and valid CellID, e.g. pointer
   *  or smart reference to CaloDigit, MCCaloDigit, CaloCluster
   *  objects.
   *
   *  "DETECTOR"  is required to have implemented methods,
   *  e.g. pointer or smart pointer to DeCalorimeter object:
   *
   *   -  float cellCenter( const CellID& ) const;
   *
   *   since the correct definition of "z" is a quite delicate task,
   *   functor allows to modify the z-value from Detector
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 26/11/1999
   */
  template <typename DETECTOR>
  class EnergyTransverse {
  public:
    /** constructor
     *  @param detector  "DETECTOR" object
     *  @param deltaZ    z-correction to position of shower maximum
     */
    EnergyTransverse( DETECTOR detector, float deltaZ = 0.0f ) : m_det( detector ), m_dz( deltaZ ) {}

    /** calculate the transverse energy of the object
     *  @param  obj   object
     *  @return the transverse energy of the object
     */
    template <typename TYPE>
    auto operator()( const TYPE& obj ) const {
      return ( obj ? obj->e() * std::sin( m_det->cellCenter( cellID( obj ) ).Theta() ) : 0.0f );
    }

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
    auto operator()( Event::Calo::Clusters::const_reference<simd, behaviour> cluster ) const {
      return cluster.e() * std::sin( m_det->cellCenter( cluster.cellID() ).Theta() );
    }

  private:
    mutable DETECTOR m_det; ///< detector element
    float            m_dz;  ///< dz correction
  };
  // ==========================================================================

  // ==========================================================================
  /** @class Over_Et_Threshold CaloDataFunctor.h Event/CaloDataFucntor.h
   *
   *  Comparison of the transverse energy of the object with
   *  given threshold value
   *
   *  "DETECTOR"  is required to have implemented methods,
   *    e.g. pointer or smart pointer to DeCalorimeter object:
   *
   *   - float cellCenter( const CellID& ) const;
   *
   *  since the correct definition of "z" is a quite delicate task,
   *  functor allows to modify the z-value from Detector
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 26/11/1999
   */
  template <typename DETECTOR>
  class Over_Et_Threshold {

  public:
    /** constructor
     *  @param Detector         "DETECTOR" object
     *  @param Threshold        transverse energy threshold
     *  @param DeltaZ           z-correction to position of shower maximum
     */
    Over_Et_Threshold( DETECTOR Detector, float Threshold = 0.0f, float DeltaZ = 0.0f )
        : m_et( Detector, DeltaZ ), m_threshold( Threshold ) {}
    /** compare the threshold energy of the object with threshold value
     *  @param  obj  object
     *  @return result of comparison with threshold
     */
    template <typename TYPE>
    bool operator()( const TYPE& obj ) const {
      return obj && ( m_et( obj ) > m_threshold );
    }

  private:
    EnergyTransverse<DETECTOR> m_et;        ///< e_t estimator
    float                      m_threshold; ///< threshold value for energy
  };
  // ==========================================================================

  // ==========================================================================
  /** @class Less_by_Energy CaloDataFunctor.h Event/CaloDataFunctor.h
   *
   *  Comparison of the energy of one object with the energy of
   *  another object. Types of objects could be different!
   *
   *  "TYPE1","TYPE2" are required to have valid comparison with 0,
   *  and implemented "->e()" method, e.g. pointer or smart reference
   *  to CaloDigit, MCCaloDigit, or CaloCluster object.
   *
   *  Example:
   *  sort container of MCCaloDigits in ascending order
   *  @code
   *  MCCaloDigits* digits = ... ;
   *  std::stable_sort( digits->begin() ,
   *                    digits->end()   ,
   *                    Less_by_Energy             );
   *  @endcode
   *
   *  For "inversion" of comparison criteria use Inverse
   *  @see Inverse
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 26/11/1999
   */
  struct Less_by_Energy_t {
    /** compare the energy of one object with the energy of
     *  another object
     *  @param obj1   first  object
     *  @param obj2   second object
     *  @return  result of energy comparison
     */
    template <typename TYPE1, class TYPE2 = TYPE1>
    bool operator()( const TYPE1& obj1, const TYPE2& obj2 ) const {
      return !obj1 || ( obj2 && obj1->e() < obj2->e() );
    }
    bool operator()( const CaloClusterEntry& obj1, const CaloClusterEntry& obj2 ) const {
      return ( !obj1.digit() ) ? true
                               : ( !obj2.digit() ) ? false
                                                   : ( obj1.digit()->e() * obj1.fraction() ) <
                                                         ( obj2.digit()->e() * obj2.fraction() );
    }
    ///
  };
  inline constexpr Less_by_Energy_t lessByEnergy{};
  // ==========================================================================

  // ==========================================================================

  // ==========================================================================
  /** @class Less_by_TransverseEnergy CaloDataFunctor.h Event/CaloDataFunctor.h
   *
   *  Comparison of the transverse energy of one object with
   *  the transverse energy of another object
   *
   *  "DETECTOR"  is required to have implemented methods,
   *   e.g. pointer or smart pointer to DeCalorimeter object:
   *
   *   - float cellCenter( const CellID& ) const;
   *
   *  since the correct definition of "z" is a quite delicate task,
   *  functor allows to modify the z-value from Detector
   *
   *  Example:
   *  sort container of CaloDigits in ascending order of
   *  transverse energy
   *  @code
   *  CaloDigits* digits = ... ;
   *  Less_by_TrnasverseEnergy<Detector*> cmp( detector );
   *  std::stable_sort( digits->begin() ,
   *                    digits->end()   ,
   *                    cmp             );
   *  @endcode
   *
   *  For "inversion" of comparison criteria use Inverse
   *  @see Inverse
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 26/11/1999
   */
  template <typename DETECTOR>
  class Less_by_TransverseEnergy {

  public:
    /** constructor
     *  @param Detector "DETECTOR" object
     *  @param DeltaZ   z-correction to position of shower maximum
     */
    Less_by_TransverseEnergy( DETECTOR Detector, float DeltaZ = 0.0f ) : m_et( Detector, DeltaZ ) {}
    /** compare the transverse energy of one object with the
     *  transverse energy of  another object
     *  @param obj1   first  object
     *  @param obj2   second object
     *  @return  result of energy comparison
     */
    template <typename TYPE, typename TYPE2>
    bool operator()( const TYPE& obj1, const TYPE2& obj2 ) const {
      return !obj1 || ( obj2 && ( m_et( obj1 ) < m_et( obj2 ) ) );
    }

  private:
    EnergyTransverse<DETECTOR> m_et; ///< e_t estimator
  };
  // ==========================================================================

  // ==========================================================================
  /** @class Accumulate_Energy CaloDataFunctor.h Event/CaloDataFunctor.h
   *
   *  Accumulate the energy of the objects
   *
   *  "TYPE" is required to have valid comparison with 0, and
   *  implemented "->e()" method, e.g. pointer or smart reference to
   *  CaloDigit,MCCaloDigit or CaloCluster objects
   *
   *  Example:
   *  accumulate the total energy of MCCaloHits
   *  @code
   *  MCCaloHits* hits = ... ;
   *  Accumulate_Energy<const MCCaloHit*> sum;
   *  const float totalEnergy =
   *   std::accumulate( hits->begin() , hits->end() , 0.0 , sum );
   *  @endcode
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 26/11/1999
   */
  struct Accumulate_Energy_t {
  public:
    /** accumulate the energy of the objkect
     *  @param   Energy  accumulated energy
     *  @param   object  object
     *  @return  accumulated energy
     */
    template <typename TYPE>
    auto operator()( float& Energy, const TYPE& object ) const {
      return ( !object ) ? Energy : Energy += object->e();
    }
  };
  inline constexpr Accumulate_Energy_t accumulateEnergy{};
  // ==========================================================================

  // ==========================================================================
  /** @class Accumulate_TransverseEnergy CaloDataFunctor.h
   *
   *  Accumulate the transverse energy of the object
   *
   *  "TYPE" is required to have valid comparison with 0, and
   *  implemented "->e()" method and valid
   *  EnergyTtransverse<TYPE,DETECTOR> structure, e.g.
   *  pointer or smart reference to CaloDigit or MCCaloDigit objects.
   *
   *  "DETECTOR" is required to have an implemented methods
   *  e.g. pointer or smart pointer to DeCalorimeter object:
   *
   *   - float cellCenter( const CellID& ) const;
   *
   *  Since the correct definition of "z" is a quite delicate task,
   *  implemented functor allows to apply correction to the the z-value
   *
   *  Example:
   *  accumulate the total transverse energy of CaloDigits
   *  @code
   *  CaloDigits* digits = ... ;
   *  Accumulate_TransverseEnergy<const CaloDigit*,Detector*> sum( detector );
   *  const float totalEnergy =
   *   std::accumulate( digits->begin() , digits->end() , 0.0 , sum );
   *  @endcode
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 26/11/1999
   */
  template <typename DETECTOR>
  class Accumulate_TransverseEnergy {

  public:
    /** constructor
     *  @param  Detector  "DETECTOR" object
     *  @param  DeltaZ     additional Z-correction to be applied
     */
    Accumulate_TransverseEnergy( DETECTOR Detector, float DeltaZ = 0.0f ) : m_et( Detector, DeltaZ ) {}
    /** accumulate the transverse energy of the objects
     *  @param   Energy  accumulated transverce energy
     *  @param   obj     object
     *  @return  accumulated transverse energy
     */
    template <typename TYPE>
    auto operator()( float& Energy, const TYPE& obj ) const {
      return ( !obj ) ? Energy : Energy += m_et( obj );
    }

  private:
    EnergyTransverse<DETECTOR> m_et; ///< e_t estimator
  };
  // ==========================================================================

  // ==========================================================================
  /** @class IsCaloCellID CaloDataFunctor.h Event/CaloDataFunctor.h
   *
   *  compare calorimtery cell identifier of the object with
   *  given value (equality test). Coudl be used for location
   *  of objects with given cellID  within the containers.
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 26/11/1999
   */
  inline constexpr auto isCellID( Detector::Calo::CellID id ) {
    return [=]( const auto& obj ) { return cellID( obj ) == id; };
  }
  // ==========================================================================

  // ==========================================================================
  /** The special functor for CaloCluster class
   *  Calculate the "energy" of the cluster as a sum of
   *  energies of its digits, weighted with energy fractions
   *  "IT" could be either iterator or const_iterator
   *
   *  @param    begin iterator to first element of sequence
   *  @param    end   iterator to last+1 element of sequance
   *  @return      "energy" of sequence
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 26/11/1999
   */
  template <typename IT>
  auto clusterEnergy( IT begin, IT end ) {
    return std::accumulate( begin, end, 0.f, []( float e, const auto& entry ) {
      const CaloDigit* digit = entry.digit();
      /// skip nulls and useless digits
      if ( digit && entry.status().test( DigitStatus::Mask::UseForEnergy ) ) {
        // accumulate the energy
        e += digit->e() * entry.fraction();
      }
      return e;
    } );
  }
  // ==========================================================================
  /** This function truncates the value of energy
   *  to 0.2 MeV precision because an ADC count
   *  corresponds to more than 2 MeV
   *
   *  @param	energy	energy value of a cluster
   *  			entry or digit
   *  @return 	energy truncated at 0.2 precision
   *
   *  @author Nuria Valls nuria.valls.canudas@cern.ch
   *  @date 10/05/2022
   *
   */
  inline constexpr auto truncateEnergy( float energy ) { return std::nearbyint( 5 * energy ) / 5; }
  // ==========================================================================

  // ==========================================================================
  /** The special functor for CaloCluster class.
   *  Calculate the "energy", X and Y position
   *  of the cluster as a sum of
   *  energies/x/y of its digits,
   *  weighted with energy fractions
   *  "IT" could be either iterator or const_iterator
   *
   *  Error Codes:
   *
   *   - 200   invalid pointer to detector element
   *   - 201   empty input sequence
   *   - 201   no selected digits
   *   - 202   accumulated energy = 0
   *
   *  @param   begin iterator to first element of sequence
   *  @param   end   iterator to last+1 element of sequance
   *  @param   de  pointer to DeCalorimeter object
   *  @param   e   energy
   *  @param   x   x-position
   *  @param   y   y-position
   *  @return    status code
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 26/11/1999
   */
  template <typename IT, class DE>
  StatusCode calculateClusterEXY( IT begin, IT end, DE de, float& e, float& x, float& y ) {
    // reset initial parameters
    e = 0;
    x = 0;
    y = 0;
    // no detector
    if ( !de ) { return StatusCode( 200 ); }
    // empty sequence
    if ( begin == end ) { return StatusCode( 201 ); }

    constexpr auto energy_f = []( const auto& entry ) -> std::optional<float> {
      const LHCb::CaloDigit* digit = entry.digit();
      if ( !digit ) return std::nullopt;
      auto eDigit = digit->e() * entry.fraction();
      return truncateEnergy( eDigit );
    };

    auto [leading_id, _] =
        std::accumulate( begin, end, std::pair{Detector::Calo::CellID{}, 0.}, [energy_f]( auto p, const auto& entry ) {
          if ( entry.status().test( LHCb::CaloDigitStatus::Mask::UseForPosition ) ) {
            auto e = energy_f( entry );
            if ( e && ( !p.first || *e > p.second ) ) p = {entry.digit()->cellID(), *e};
          }
          return p;
        } );
    // at least one useful digit ?
    if ( !leading_id ) return StatusCode( 202 );
    const Gaudi::XYZPoint& refPos = de->cellCenter( leading_id );

    // energy for position
    auto epos = 0.0f;
    // explicit loop over all entries
    for ( ; begin != end; ++begin ) {
      auto eDigit = energy_f( *begin );
      if ( !eDigit ) { continue; }
      if ( begin->status().test( DigitStatus::Mask::UseForEnergy ) ) e += *eDigit; // accumulate digit energy
      if ( begin->status().test( DigitStatus::Mask::UseForPosition ) ) {
        epos += *eDigit; // accumulate digit energy for position
        auto id = begin->digit()->cellID();
        if ( id != leading_id ) {
          const Gaudi::XYZPoint& pos = de->cellCenter( id );
          x += *eDigit * ( pos.x() - refPos.x() );
          y += *eDigit * ( pos.y() - refPos.y() );
        }
      }
    }
    // accumulated energy is NULL!
    if ( 0 == epos ) { return StatusCode( 203 ); }
    // rescale x and y
    x = refPos.x() + x / epos;
    y = refPos.y() + y / epos;
    //
    return StatusCode::SUCCESS;
  }
  // ==========================================================================
  struct EXY {
    float Etot;
    float x;
    float y;
    float Epos;
  };
  template <typename Range, class DE>
  std::optional<EXY> calculateClusterEXY( Range&& r, DE de ) {
    // reset initial parameters
    EXY exy{0, 0, 0, 0};
    // no detector
    if ( !de ) return std::nullopt;
    // empty sequence
    if ( r.empty() ) return std::nullopt;
    //
    constexpr auto energy_f = []( const auto& entry ) {
      auto e = entry.energy() * entry.fraction();
      return truncateEnergy( e );
    };
    // explicit loop over all entries
    auto [leading_id, _] = std::accumulate(
        r.begin(), r.end(), std::pair{Detector::Calo::CellID{}, 0.}, [energy_f]( auto p, const auto& entry ) {
          if ( entry.status().test( LHCb::CaloDigitStatus::Mask::UseForPosition ) ) {
            auto e = energy_f( entry );
            if ( !p.first || e > p.second ) p = {entry.cellID(), e};
          }
          return p;
        } );
    // at least one useful digit ?
    if ( !leading_id ) return std::nullopt;
    const Gaudi::XYZPoint& refPos = de->cellCenter( leading_id );

    // energy for position
    for ( const auto& entry : r ) {
      auto e = energy_f( entry );
      if ( entry.status().test( DigitStatus::Mask::UseForEnergy ) ) exy.Etot += e; // accumulate digit energy
      if ( entry.status().test( DigitStatus::Mask::UseForPosition ) ) {
        exy.Epos += e; // accumulate digit energy for position
        if ( entry.cellID() != leading_id ) {
          const Gaudi::XYZPoint& pos = de->cellCenter( entry.cellID() );
          exy.x += e * ( pos.x() - refPos.x() );
          exy.y += e * ( pos.y() - refPos.y() );
        }
      }
    }
    // accumulated energy is NULL!
    if ( 0 == exy.Epos ) return std::nullopt;
    // rescale x and y
    exy.x = refPos.x() + exy.x / exy.Epos;
    exy.y = refPos.y() + exy.y / exy.Epos;

    return exy;
  }
  // ==========================================================================

  // ==========================================================================
  /** The special fucntor to CaloCluster class.
   *  useful function to find first common digit
   *  from two sequences. It returns the pair of
   *  iterators (first one for first sequence and
   *  the secons one for second sequences).
   *  "IT" could be either iterator or const_iterator
   *
   *  @param  begin1 iterator pointing to 1st    element of 1st sequence
   *  @param  end1   iterator pointing to last+1 element of 1st sequence
   *  @param  begin2 iterator pointing to 1st    element of 2nd sequence
   *  @param  end2   iterator pointing to last+1 element of 2nd sequence
   *  @return pair of iterators
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 26/11/1999
   */
  template <typename IT>
  std::pair<IT, IT> clusterCommonDigit( IT begin1, IT end1, IT begin2, IT end2 ) {
    // loop over the sequences
    for ( ; begin1 != end1; ++begin1 ) {
      auto id1 = begin1->cellID();
      if ( !id1 ) continue; // skip NULLS !
      auto it2 = std::find_if( begin2, end2, isCellID( id1 ) );
      if ( it2 != end2 ) { return {begin1, it2}; }
    }
    return {end1, end2};
  }
  // ==========================================================================

  // ==========================================================================
  /** The special function for CaloCluster class
   *  The useful function to locate the digit within the sequence
   *
   *  "IT" could be either iterator or const_iterator
   *
   *  @param begin iterator pointing to the 1st    element of sequence
   *  @param end   iterator pointing to teh last+1 element of sequence
   *  @param digit pointer to CaloDigit
   *  @return location of digit within the sequence
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 26/11/1999
   */
  template <typename IT>
  IT clusterLocateDigit( IT begin, IT end, const CaloDigit* digit ) {
    return std::find_if( begin, end, [&]( const auto& arg ) { return arg->digit() == digit; } );
  }
  // ==========================================================================

  // ==========================================================================
  /** The special function for CaloCluster class
   *  The useful function to locate the digit with given status
   *  from sequence of digits
   *
   *  "IT" could be either iterator or const_iterator
   *
   *  @param begin  iterator pointing to the 1st    element of sequence
   *  @param end    iterator pointing to the last+1 element of sequence
   *  @param st     status
   *  @return location of digit within the sequence
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 26/11/1999
   */
  template <typename IT>
  IT clusterLocateDigit( IT begin, IT end, DigitStatus::Mask st ) {
    return std::find_if( begin, end, [&]( const auto& arg ) { return arg.status().test( st ); } );
  }
  // ==========================================================================

  // ==========================================================================
  /** helpful function (in STL spirit) for creation of "inverse"
   *  comparison criteria:
   *
   *  @see Inverse
   *  Example:
   *  @code
   *  CaloClusters* clusters = ... ;
   *  Less_by_Energy cmp;
   *  // sort clusters in ascending order:
   *  std::stable_sort( clusters->begin() ,
   *                    clusters->end()   ,          cmp   ) ;
   *  // sort clusters in descensing order
   *  std::stable_sort( clusters->begin() ,
   *                    clusters->end()   , inverse( cmp ) ) ;
   *  @endcode
   */
  template <typename CMP>
  inline constexpr auto inverse( CMP&& cmp ) {
    return [cmp = std::forward<CMP>( cmp )]( const auto& lhs, const auto& rhs ) { return cmp( rhs, lhs ); };
  }

  // ==========================================================================

  // ==========================================================================
  /** @class DigitFromCalo
   *  simple utility to count digits from certain calorimeter
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date 31/03/2002
   */
  class DigitFromCalo {
  public:
    /** constructor
     *  @param calo  calorimeter name
     */
    explicit DigitFromCalo( const std::string& calo ) : m_calo( Detector::Calo::CellCode::CaloNumFromName( calo ) ) {}
    /** constructor
     *  @param calo  calorimeter index
     */
    explicit DigitFromCalo( Detector::Calo::CellCode::Index calo ) : m_calo( calo ) {}
    /** the only essential method
     *  @param digit pointer to CaloDigit object
     *  @return true if digit belongs to the predefined calorimeter
     */
    bool operator()( const CaloDigit* digit ) const { return digit && digit->cellID().calo() == m_calo; }

  private:
    Detector::Calo::CellCode::Index m_calo;
  };
  // ==========================================================================

  // ==========================================================================
  /** @class Calo
   *  simple class to extract the Calorimeter index (SPD/PRS/ECAL/HCAL)
   *  from the objects
   *  The TYPE is reuired to have the valid CellID<TYPE> structure
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  @date 2004-10-22
   */
  inline constexpr auto calo = []( const auto& obj ) { return cellID( obj ).calo(); };
  // ==========================================================================

  // ==========================================================================
  /** @class IsFromCalo
   *  simple class (predicate) to determine if the object
   *  'is from the given calrimeter'
   *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
   *  @date 2004-10-22
   */
  class IsFromCalo {
  public:
    /// constructor from calorimeter index
    explicit IsFromCalo( Detector::Calo::CellCode::Index index ) : m_index( index ) {}
    /// constructor from calorimeter name
    explicit IsFromCalo( const std::string& name ) : IsFromCalo( Detector::Calo::CellCode::CaloNumFromName( name ) ) {}
    template <typename TYPE>
    bool operator()( const TYPE& obj ) const {
      return calo( obj ) == m_index;
    }

  private:
    Detector::Calo::CellCode::Index m_index;
  };
  // ==========================================================================

  // ==========================================================================
  /** @class CaloArea
   *  simple class to extract the Calorimeter area
   *  from the objects
   *  The TYPE is reuired to have the valid CellID<TYPE> structure
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  @date 2004-10-22
   */
  inline constexpr auto caloArea = []( const auto& obj ) { return cellID( obj ).area; };
  // ==========================================================================

  // ==========================================================================
  /** @class IsFromArea
   *  simple class (predicate) to determine if the object
   *  'is from the given area'
   *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
   *  @date 2004-10-22
   */
  inline constexpr auto isFromArea( int area ) {
    return [=]( const auto& obj ) { return caloArea( obj ) == area; };
  }
  // ==========================================================================

  // ==========================================================================
  /** @class CaloRow
   *  simple class to extract the Calorimeter Row
   *  from the objects
   *  The TYPE is reuired to have the valid CellID<TYPE> structure
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  @date 2004-10-22
   */
  inline constexpr auto caloRow = []( const auto& obj ) { return cellID( obj ).row(); };
  // ==========================================================================

  // ==========================================================================
  /** @class IsFromRow
   *  simple class to determine if the object 'is from the given row'
   *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
   *  @date 2004-10-22
   */
  inline constexpr auto isFromRow( int row ) {
    return [=]( const auto& obj ) {
      return caloRow( obj ) == row;
      ;
    };
  }
  // ==========================================================================

  // ==========================================================================
  /** @class CaloColumn
   *  simple class to extract the Calorimeter column
   *  from the objects
   *  The TYPE is reuired to have the valid CellID<TYPE> structure
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  @date 2004-10-22
   */
  inline constexpr auto caloColumn = []( const auto& obj ) { return cellID( obj ).col(); };
  // ==========================================================================

  // ==========================================================================
  /** @class IsFromColumn
   *  simple class to determine if the object 'is from the given column'
   *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
   *  @date 2004-10-22
   */
  inline constexpr auto isFromColumn( int column ) {
    return [=]( const auto& obj ) { return caloColumn( obj ) == column; };
  }
  // ==========================================================================

} // namespace LHCb::Calo::Functor

// backwards compatibility
namespace LHCb {
  namespace CaloDataFunctor {
    using namespace Calo::Functor;
    inline constexpr auto CellID         = Calo::Functor::cellID;
    inline constexpr auto Less_by_Energy = Calo::Functor::lessByEnergy;
  } // namespace CaloDataFunctor
} // namespace LHCb
// ============================================================================
// The End
// ============================================================================
