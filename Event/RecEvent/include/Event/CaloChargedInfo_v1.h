/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Detector/Calo/CaloCellID.h"
#include "Event/Track.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SmartRef.h"

namespace LHCb::Event::Calo {

  inline namespace v1 {

    // Class ID definitions
    static const CLID CLID_ChargedPID = 2005;
    static const CLID CLID_BremInfo   = 2006;

    // class for Calo Charged PID info
    class ChargedPID final : public KeyedObject<int> {
      using CellID = LHCb::Detector::Calo::CellID;

    public:
      /// typedef for KeyedContainer of ChargedPID
      typedef KeyedContainer<ChargedPID, Containers::HashMap> Container;

      // Retrieve pointer to class definition structure
      const CLID&        clID() const override { return classID(); }
      static const CLID& classID() { return CLID_ChargedPID; }

      // related track
      const LHCb::Track* idTrack() const { return m_IDTrack; }
      void               setIDTrack( const SmartRef<LHCb::Track>& value ) { m_IDTrack = value; }
      void               setIDTrack( const LHCb::Track* value ) { m_IDTrack = value; }

      /// retreive const parameters
      // Ecal
      bool   InEcal() const { return m_InEcal; }
      CellID ClusterID() const { return m_ClusterID; }
      float  ClusterMatch() const { return m_ClusterMatch; }
      CellID ElectronID() const { return m_ElectronID; }
      float  ElectronMatch() const { return m_ElectronMatch; }
      float  ElectronEnergy() const { return m_ElectronEnergy; }
      float  ElectronShowerEoP() const { return m_ElectronShowerEoP; }
      float  ElectronShowerDLL() const { return m_ElectronShowerDLL; }
      float  EcalPIDe() const { return m_EcalPIDe; }
      float  EcalPIDmu() const { return m_EcalPIDmu; }
      // Hcal
      bool  InHcal() const { return m_InHcal; }
      float HcalEoP() const { return m_HcalEoP; }
      float HcalPIDe() const { return m_HcalPIDe; }
      float HcalPIDmu() const { return m_HcalPIDmu; }

      /// setting function for v3 tracks related pid objects
      template <typename ProxyV3>
      void setParametersFromV3( ProxyV3 const& proxy ) {
        // Ecal
        m_InEcal            = proxy.InEcal().cast();
        m_ClusterID         = proxy.ClusterID();
        m_ClusterMatch      = proxy.ClusterMatch().cast();
        m_ElectronID        = proxy.ElectronID();
        m_ElectronMatch     = proxy.ElectronMatch().cast();
        m_ElectronEnergy    = proxy.ElectronEnergy().cast();
        m_ElectronShowerEoP = proxy.ElectronShowerEoP().cast();
        m_ElectronShowerDLL = proxy.ElectronShowerDLL().cast();
        m_EcalPIDe          = proxy.EcalPIDe().cast();
        m_EcalPIDmu         = proxy.EcalPIDmu().cast();
        // Hcal
        m_InHcal    = proxy.InHcal().cast();
        m_HcalEoP   = proxy.HcalEoP().cast();
        m_HcalPIDe  = proxy.HcalPIDe().cast();
        m_HcalPIDmu = proxy.HcalPIDmu().cast();
      }

    private:
      // Ecal
      bool   m_InEcal{false};
      CellID m_ClusterID{};
      float  m_ClusterMatch{-1.f};
      CellID m_ElectronID{};
      float  m_ElectronMatch{-1.f};
      float  m_ElectronEnergy{0.f};
      float  m_ElectronShowerEoP{-1.f};
      float  m_ElectronShowerDLL{0.f};
      float  m_EcalPIDe{0.f};
      float  m_EcalPIDmu{0.f};
      // Hcal
      bool  m_InHcal{false};
      float m_HcalEoP{-1.f};
      float m_HcalPIDe{0.f};
      float m_HcalPIDmu{0.f};
      // related track
      SmartRef<LHCb::Track> m_IDTrack;

    }; // class ChargedPID

    // class bremsstrahlung information
    class BremInfo final : public KeyedObject<int> {
      using CellID = LHCb::Detector::Calo::CellID;

    public:
      /// typedef for KeyedContainer of BremInfo
      typedef KeyedContainer<BremInfo, Containers::HashMap> Container;

      // Retrieve pointer to class definition structure
      const CLID&        clID() const override { return classID(); }
      static const CLID& classID() { return CLID_BremInfo; }

      // related track
      const LHCb::Track* idTrack() const { return m_IDTrack; }
      void               setIDTrack( const SmartRef<LHCb::Track>& value ) { m_IDTrack = value; }
      void               setIDTrack( const LHCb::Track* value ) { m_IDTrack = value; }

      /// retreive const parameters
      bool   InBrem() const { return m_InBrem; }
      CellID BremHypoID() const { return m_BremHypoID; }
      float  BremHypoMatch() const { return m_BremHypoMatch; }
      float  BremHypoEnergy() const { return m_BremHypoEnergy; }
      float  BremHypoDeltaX() const { return m_BremHypoDeltaX; }
      float  BremTrackBasedEnergy() const { return m_BremTrackBasedEnergy; }
      float  BremBendingCorrection() const { return m_BremBendingCorrection; }
      bool   HasBrem() const { return m_HasBrem; }
      float  BremEnergy() const { return m_BremEnergy; }
      float  BremPIDe() const { return m_BremPIDe; }

      /// setting function for v3 tracks related pid objects
      template <typename ProxyV3>
      void setParametersFromV3( ProxyV3 const& proxy ) {
        m_InBrem                = proxy.InBrem().cast();
        m_BremHypoID            = proxy.BremHypoID();
        m_BremHypoMatch         = proxy.BremHypoMatch().cast();
        m_BremHypoEnergy        = proxy.BremHypoEnergy().cast();
        m_BremHypoDeltaX        = proxy.BremHypoDeltaX().cast();
        m_BremTrackBasedEnergy  = proxy.BremTrackBasedEnergy().cast();
        m_BremBendingCorrection = proxy.BremBendingCorrection().cast();
        m_HasBrem               = proxy.HasBrem().cast();
        m_BremEnergy            = proxy.BremEnergy().cast();
        m_BremPIDe              = proxy.BremPIDe().cast();
      }

    private:
      bool                  m_InBrem{false};
      CellID                m_BremHypoID{};
      float                 m_BremHypoMatch{-1.f};
      float                 m_BremHypoEnergy{-1.f};
      float                 m_BremHypoDeltaX{-1000.f};
      float                 m_BremTrackBasedEnergy{-1.f};
      float                 m_BremBendingCorrection{1.f};
      bool                  m_HasBrem{false};
      float                 m_BremEnergy{0.f};
      float                 m_BremPIDe{0.f};
      SmartRef<LHCb::Track> m_IDTrack;

    }; // class BremInfo

    /// Definition of Keyed Containers
    typedef KeyedContainer<ChargedPID, Containers::HashMap> ChargedPIDs;
    typedef KeyedContainer<BremInfo, Containers::HashMap>   BremInfos;

  } // namespace v1
} // namespace LHCb::Event::Calo
