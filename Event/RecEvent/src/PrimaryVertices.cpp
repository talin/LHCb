/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PrimaryVertices.h"
#include "Event/PrVeloTracks.h"
#include "LHCbMath/MatVec.h"

namespace LHCb::Event::PV {

  using simd    = SIMDWrapper::best::types;
  using float_v = simd::float_v;
  using int_v   = simd::int_v;

  template <typename FTYPE>
  inline auto sqr( FTYPE x ) {
    return x * x;
  }

  void populateFromVeloTracks( PrimaryVertexContainer& pvdata ) {
    // we cannot vectorise this, but in the previous version we also had un unvectorised 'permute' loop.
    auto& pvtracks        = pvdata.tracks;
    auto  pvtracks_scalar = pvtracks.scalar();
    auto  pvtracks_simd   = pvtracks.simd();

    // if we add reasonable values in the padding, we can save time in the addition in the vertex fit.
    {
      const float_v zero  = 0.;
      int           index = pvtracks.size() - 1;
      pvtracks.store<PVTrackTag::z>( index, zero );
      pvtracks.store<PVTrackTag::x>( index, zero );
      pvtracks.store<PVTrackTag::y>( index, zero );
      pvtracks.store<PVTrackTag::tx>( index, zero );
      pvtracks.store<PVTrackTag::ty>( index, zero );
      std::true_type nomask;
      pvtracks.store<PVTrackTag::Vx>( index, PVTrackTag::XTxCovMatrixElement::xx, float_v{100.}, nomask );
      pvtracks.store<PVTrackTag::Vx>( index, PVTrackTag::XTxCovMatrixElement::xtx, zero, nomask );
      pvtracks.store<PVTrackTag::Vx>( index, PVTrackTag::XTxCovMatrixElement::txtx, float_v{100.}, nomask );
      pvtracks.store<PVTrackTag::Vy>( index, PVTrackTag::XTxCovMatrixElement::xx, float_v{100.}, nomask );
      pvtracks.store<PVTrackTag::Vy>( index, PVTrackTag::XTxCovMatrixElement::xtx, zero, nomask );
      pvtracks.store<PVTrackTag::Vy>( index, PVTrackTag::XTxCovMatrixElement::txtx, float_v{100.}, nomask );

      // The next we only need if we store with a mask in the vertex fit.
      for ( unsigned int index = 0; index < pvtracks.size() + simd::size; index += simd::size ) {
        auto pvtrack = pvtracks_simd[index];
        pvtrack.field<PVTrackTag::Wx>().set( 0 );
        pvtrack.field<PVTrackTag::Wy>().set( 0 );
        for ( int i = 0; i < 3; ++i ) pvtrack.field<PVTrackTag::halfDChi2DX>( i ).set( 0 );
        for ( int i = 0; i < 5; ++i ) pvtrack.field<PVTrackTag::halfD2Chi2DX2>( i ).set( 0 );
      }
    }

    // We run the loop from the pv tracks. The advantage is that we
    // can allow multiple pvtracks to point to the same velo
    // track.

    // for (auto pvtrack : pvtracks_simd ) {
    for ( auto pvtrack :
          pvtracks_scalar ) { // This loop seems >2x slower in simd than in scalar, so leave scalar for now.
      // auto [containerIdx, veloIdx] = pvtrack.field<PVTrackTag::veloindex>().index();
      // const int containerIdx_int = containerIdx.cast() ;
      // const int veloIdx_int = veloIdx.cast() ;
      // onst auto track     = pvtracks.containers<PVTrackTag::veloindex>()[containerIdx_int]->scalar()[veloIdx_int] ;
      const auto track = pvtrack.veloTrack();
      const auto pos   = track.StatePos( 0 );
      const auto dir   = track.StateDir( 0 );
      const auto covX  = track.StateCovX( 0 );
      const auto covY  = track.StateCovY( 0 );
      pvtrack.field<PVTrackTag::z>().set( pos.z() );
      pvtrack.field<PVTrackTag::x>().set( pos.x() );
      pvtrack.field<PVTrackTag::y>().set( pos.y() );
      pvtrack.field<PVTrackTag::tx>().set( dir.x() );
      pvtrack.field<PVTrackTag::ty>().set( dir.y() );
      pvtrack.field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::xx ).set( covX.x() );
      pvtrack.field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::xtx ).set( covX.y() );
      pvtrack.field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::txtx ).set( covX.z() );
      pvtrack.field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::xx ).set( covY.x() );
      pvtrack.field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::xtx ).set( covY.y() );
      pvtrack.field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::txtx ).set( covY.z() );
    }

    // const auto pvtrack = pvtracks.simd()[pvtracks.size()-1] ;
    // std::cout << "Padding check: "
    // 	      << pvtrack.get<PVTrackTag::z>() << " "
    // 	      << pvtrack.get<PVTrackTag::Vx>(0) << std::endl ;
  }

  void populateFromVeloTracks( PrimaryVertexContainer& pvdata, const LHCb::Pr::Velo::Tracks& tracksForward,
                               const LHCb::Pr::Velo::Tracks& tracksBackward ) {
    pvdata.tracks.prvelocontainers[0] = &tracksForward;
    pvdata.tracks.prvelocontainers[1] = &tracksBackward;
    populateFromVeloTracks( pvdata );
  }

  template <typename T>
  void initialize_with_zeros( T& mat ) {
    mat = LinAlg::initialize_with_zeros<T>();
  }

  /// Helper class to hold accumulated chi2 and its derivatives
  struct Chi2Accumulator {
    using Vector3      = LHCb::LinAlg::Vec<double, 3>;
    using SymMatrix3x3 = LHCb::LinAlg::MatSym<double, 3>;
    int          entries{0};
    double       chi2{0};
    Vector3      halfDChi2DX{LinAlg::initialize_with_zeros<Vector3>()};
    SymMatrix3x3 halfD2Chi2DX2{LinAlg::initialize_with_zeros<SymMatrix3x3>()};
    void         reset() {
      entries = 0;
      chi2    = 0;
      initialize_with_zeros( halfDChi2DX );
      initialize_with_zeros( halfD2Chi2DX2 );
    }
  };

  /// Updates the PV with residuals accumulated from the tracks. Returns true if converged.
  bool updatePrimaryVertex( PrimaryVertex& pv, const Chi2Accumulator& accumulator, float maxDeltaZConverged,
                            float maxDeltaChi2Converged ) {
    auto cov = accumulator.halfD2Chi2DX2.invChol();
    // compute the delta w.r.t. the reference
    const auto delta = cov * accumulator.halfDChi2DX * -1.0;
    // update the position
    pv.setPosition( pv.position() + Gaudi::XYZVector{delta( 0 ), delta( 1 ), delta( 2 )} );
    // update the chi2
    const auto deltachi2 = delta.dot( accumulator.halfDChi2DX );
    pv.setChi2( accumulator.chi2 + deltachi2 );
    pv.setNDoF( 2 * accumulator.entries - 3 );
    pv.setWeightMatrix( accumulator.halfD2Chi2DX2 );
    pv.setCovMatrix( convertToSMatrix<double>( cov ) );
    return std::abs( delta( 2 ) ) < maxDeltaZConverged && abs( deltachi2 ) < maxDeltaChi2Converged;
  };

  /// With all PVs adaptively. They used to be fitted simultaneous but
  /// the time consumption of 'gather' and 'scatter' routines was too
  /// large. Therefore, tracks are now sorted by PV and the PVs just
  /// fitted sequentially.
  void fitAdaptive( PrimaryVertices& vertices, PVTracks& pvtracks, float chi2max, float maxDeltaZConverged,
                    float maxDeltaChi2Converged, uint16_t maxNumIter, uint16_t numAnnealingSteps ) {

    for ( auto& pv : vertices )
      fitAdaptive( pv, pvtracks, chi2max, maxDeltaZConverged, maxDeltaChi2Converged, maxNumIter, numAnnealingSteps );
  }

  /// Applies one step of the vertex fit. Returns true if converged.
  template <bool UpdateWeightMatrix>
  bool fitSingleStep( PrimaryVertex& pv, PVTracks& pvtracks, float chi2max, float maxDeltaZConverged,
                      float maxDeltaChi2Converged ) {
    const auto pvtrack_simd = pvtracks.simd();
    const auto start        = pv.begin();
    const auto end          = pv.end();

    // FIXME: the only reason we need the loop mask here, is because
    // otherwise we overwrite the contents of the next vertex. If we
    // always fit the vertices in order, this is not problem. We can
    // also add padding at the end of each vertex.

    Chi2Accumulator accumulator;
    // A lambda to do the uncompressed store with a mask. If we add padding at the end of a vertex, we can simplify
    // this.
    // auto store = []( auto field, auto value, auto mask ) { field.set( select(mask,value,field.get() ) ) ; } ;
    auto store = []( auto field, auto value, auto /*mask*/ ) { field.set( value ); };

    float const vtxx = pv.position().x();
    float const vtxy = pv.position().y();
    float const vtxz = pv.position().z();

    for ( auto offset = start; offset < end; offset += simd::size ) {
      const auto loop_mask = simd::loop_mask( offset, end );
      const auto pvtrack   = pvtrack_simd[offset];
      const auto dz        = vtxz - pvtrack.z();
      const auto tx        = pvtrack.tx();
      const auto ty        = pvtrack.ty();

      if constexpr ( UpdateWeightMatrix ) {
        auto const Vx = pvtrack.covXX() + 2 * dz * pvtrack.covXTx() + dz * dz * pvtrack.covTxTx();
        auto const Vy = pvtrack.covYY() + 2 * dz * pvtrack.covYTy() + dz * dz * pvtrack.covTyTy();
        const auto Wx = select( loop_mask, 1 / Vx, 0.f );
        const auto Wy = select( loop_mask, 1 / Vy, 0.f );

        // ( 1     0 )
        // ( 0     1 )      ( Wx   0  )     ( 1  0  -tx )
        // ( -tx -ty )      (  0   Wy )     ( 0  1  -ty )
        //
        //        (  Wx         0     )
        //   =    (  0          Wy    )      ( 1  0  -tx )
        //        (  -tx Wx    -ty Wy )      ( 0  1  -ty )
        //
        //        (    Wx        0              -tx Wx        )
        //   =    (    0         Wy             -ty Wy        )
        //        ( -tx Wx    -ty Wy    (tx*tx*Wx + ty*ty*Wy) )

        const auto halfD2Chi2DX2_00 = Wx;
        const auto halfD2Chi2DX2_11 = Wy;
        const auto halfD2Chi2DX2_20 = -tx * Wx;
        const auto halfD2Chi2DX2_21 = -ty * Wy;
        const auto halfD2Chi2DX2_22 = tx * Wx * tx + ty * Wy * ty;
        store( pvtrack.field<PVTrackTag::Wx>(), Wx, loop_mask );
        store( pvtrack.field<PVTrackTag::Wy>(), Wy, loop_mask );
        store( pvtrack.field<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v00 ), halfD2Chi2DX2_00,
               loop_mask );
        store( pvtrack.field<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v11 ), halfD2Chi2DX2_11,
               loop_mask );
        store( pvtrack.field<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v20 ), halfD2Chi2DX2_20,
               loop_mask );
        store( pvtrack.field<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v21 ), halfD2Chi2DX2_21,
               loop_mask );
        store( pvtrack.field<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v22 ), halfD2Chi2DX2_22,
               loop_mask );
      }

      const auto Wx = pvtrack.Wx();
      const auto Wy = pvtrack.Wy();
      const auto dx = vtxx - ( pvtrack.x() + dz * tx );
      const auto dy = vtxy - ( pvtrack.y() + dz * ty );

      const auto chi2      = dx * Wx * dx + dy * Wy * dy;
      const auto chi2_mask = loop_mask && chi2 < chi2max && pvtrack.pvindex() > -1;
      accumulator.entries += popcount( chi2_mask );
      const auto weight = select( chi2_mask, sqr( 1.f - chi2 / chi2max ), 0.f );

      // compute the derivatives
      const auto halfDChi2DX_0 = Wx * dx;
      const auto halfDChi2DX_1 = Wy * dy;
      const auto halfDChi2DX_2 = -tx * Wx * dx - ty * Wy * dy;
      // Store these such that we can use them for unbiasing, etc.
      store( pvtrack.field<PVTrackTag::weight>(), weight, loop_mask );
      store( pvtrack.field<PVTrackTag::ipchi2>(), chi2, loop_mask );
      store( pvtrack.field<PVTrackTag::halfDChi2DX>( 0 ), halfDChi2DX_0, loop_mask );
      store( pvtrack.field<PVTrackTag::halfDChi2DX>( 1 ), halfDChi2DX_1, loop_mask );
      store( pvtrack.field<PVTrackTag::halfDChi2DX>( 2 ), halfDChi2DX_2, loop_mask );

      // Now that we have stored everything, add to the chi2 derivatives of the vertex. We could probably get rid of
      // chi2_mask here.
      accumulator.chi2 += ( weight * chi2 ).hadd( /*chi2_mask*/ );
      accumulator.halfDChi2DX( 0 ) += ( weight * halfDChi2DX_0 ).hadd( /*chi2_mask*/ );
      accumulator.halfDChi2DX( 1 ) += ( weight * halfDChi2DX_1 ).hadd( /*chi2_mask*/ );
      accumulator.halfDChi2DX( 2 ) += ( weight * halfDChi2DX_2 ).hadd( /*chi2_mask*/ );
      accumulator.halfD2Chi2DX2( 0, 0 ) +=
          ( weight * pvtrack.get<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v00 ) )
              .hadd( /*chi2_mask*/ );
      accumulator.halfD2Chi2DX2( 1, 1 ) +=
          ( weight * pvtrack.get<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v11 ) )
              .hadd( /*chi2_mask*/ );
      accumulator.halfD2Chi2DX2( 2, 0 ) +=
          ( weight * pvtrack.get<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v20 ) )
              .hadd( /*chi2_mask*/ );
      accumulator.halfD2Chi2DX2( 2, 1 ) +=
          ( weight * pvtrack.get<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v21 ) )
              .hadd( /*chi2_mask*/ );
      accumulator.halfD2Chi2DX2( 2, 2 ) +=
          ( weight * pvtrack.get<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v22 ) )
              .hadd( /*chi2_mask*/ );

#ifdef MYDEBUGGING
      int foundnan{isnan( accumulator.chi2 )};
      for ( int row = 0; row < 3; ++row )
        for ( int col = 0; col <= row; ++col ) foundnan = foundnan || isnan( accumulator.halfD2Chi2DX2( row, col ) );
      if ( foundnan ) {
        std::cout << "Fitting PV found nan: " << iter << " " << offset << " " << pv.begin() << " " << pv.end() << " "
                  << pvtracks.size() << " " << pv.position() << std::endl;
        std::cout << "chi2 " << accumulator.chi2 << " " << offset << std::endl;
        std::cout << accumulator.halfD2Chi2DX2 << std::endl;
        std::cout << "weight: " << weight << std::endl;
        std::cout << "chi2: " << chi2 << std::endl;
        std::cout << "dx=" << dx << "dy=" << dy << "Wx=" << Wx << "Wy=" << Wy << std::endl;
        std::cout << "covXX: " << pvtrack.covXX() << " " << pvtrack.covXTx() << pvtrack.covTxTx() << std::endl;
        std::cout << "covYY: " << pvtrack.covYY() << " " << pvtrack.covYTy() << pvtrack.covTyTy() << std::endl;
        for ( int i = 0; i < 5; ++i )
          std::cout << "ii: " << i << " " << pvtrack.get<PVTrackTag::halfD2Chi2DX2>( i ) << std::endl;
      }
#endif
    }

    // call the primary vertex update
    const bool converged = updatePrimaryVertex( pv, accumulator, maxDeltaZConverged, maxDeltaChi2Converged );
    return converged;
  }

  void fitAdaptive( PrimaryVertex& pv, PVTracks& pvtracks, float chi2max, float maxDeltaZConverged,
                    float maxDeltaChi2Converged, uint16_t maxNumIter, uint16_t numAnnealingSteps ) {

    // std::cout << "In fitAdaptive: begin=" << pv.begin() << " end=" << pv.end() << " size=" << pv.size() << "
    // ntracks=" << pv.nTracks() << std::endl ;

    bool converged{false};
    auto zcache = pv.position().z();
    for ( uint16_t iter = 0; iter < maxNumIter && !converged; ++iter ) {
      // dumb 1-step annealing scheme. don't use for refits.
      const auto chi2maxmultiplier = std::max( numAnnealingSteps - iter + 1, 1 );
      const auto thischi2max       = chi2maxmultiplier * chi2max;

      // Update the 2nd derivative cache only when vtx moved significantly, or on the first iteration
      const auto maxDeltaZForCache = 0.2;
      const auto vtxz              = pv.position().z();
      if ( iter == 0 || std::abs( vtxz - zcache ) > maxDeltaZForCache ) {
        converged = fitSingleStep<true>( pv, pvtracks, thischi2max, maxDeltaZConverged, maxDeltaChi2Converged );
        zcache    = vtxz;
      } else {
        converged = fitSingleStep<false>( pv, pvtracks, thischi2max, maxDeltaZConverged, maxDeltaChi2Converged );
      }
      converged = converged && iter >= numAnnealingSteps;
    }
  }

  PrimaryVertices unbiasedVertices( const PrimaryVertexContainer& pvdata, LHCb::span<int> velotrackindices ) {
    // 1. copy the original vertices
    auto unbiasedvertices = pvdata.vertices;
    // 2. subtract the contribution from these tracks from the chi2
    // values.
    // There is something to say for using Math::Vec here :-(
    const auto&                                         tracks = pvdata.tracks.scalar();
    boost::container::small_vector<bool, 16>            pvchanged( unbiasedvertices.size(), false );
    boost::container::small_vector<Chi2Accumulator, 16> accumulators( unbiasedvertices.size() );
    for ( const auto& i : velotrackindices ) {
      const auto& pvtrack = tracks[i];
      const auto  weight  = pvtrack.weight().cast();
      if ( weight > 0 ) {
        const auto ipv         = pvtrack.pvindex().cast();
        auto&      accumulator = accumulators[ipv];
        accumulator.halfDChi2DX( 0 ) -= weight * pvtrack.halfDChi2DX( 0 ).cast();
        accumulator.halfDChi2DX( 1 ) -= weight * pvtrack.halfDChi2DX( 1 ).cast();
        accumulator.halfDChi2DX( 2 ) -= weight * pvtrack.halfDChi2DX( 2 ).cast();
        accumulator.halfD2Chi2DX2( 0, 0 ) -=
            weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v00 ).cast();
        accumulator.halfD2Chi2DX2( 1, 1 ) -=
            weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v11 ).cast();
        accumulator.halfD2Chi2DX2( 2, 0 ) -=
            weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v20 ).cast();
        accumulator.halfD2Chi2DX2( 2, 1 ) -=
            weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v21 ).cast();
        accumulator.halfD2Chi2DX2( 2, 2 ) -=
            weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v22 ).cast();
        accumulator.chi2 -= weight * pvtrack.ipchi2().cast();
        accumulator.entries -= 1;
      }
    }
    // 3. update the vertex positions.
    for ( unsigned ipv = 0; ipv < unbiasedvertices.size(); ++ipv ) {
      auto& accumulator = accumulators[ipv];
      if ( accumulator.entries != 0 ) {
        auto& pv                  = unbiasedvertices[ipv];
        accumulator.halfD2Chi2DX2 = accumulator.halfD2Chi2DX2 + pv.weightMatrix();
        accumulator.chi2 += pv.chi2();
        accumulator.entries += ( pv.nDoF() + 3 ) / 2; // reverse engineer
        updatePrimaryVertex( pv, accumulator, 0., 0. );
      }
    }
    return unbiasedvertices;
  }

  // Returns a single unbiased primary vertex. I actually need a base class that contains less information. VertexBase
  // would be fine, but it derived from KeyedObkect.
  PrimaryVertex unbiasedVertex( const PrimaryVertexContainer& pvdata, int pvindex, LHCb::span<int> velotrackindices ) {
    // make a copy
    auto            pv            = pvdata.vertices[pvindex];
    const auto&     tracks_scalar = pvdata.tracks.scalar();
    const auto&     velotrackmap  = pvdata.velotrackmap;
    Chi2Accumulator accumulator;
    for ( auto velotrkindex : velotrackindices ) {
      const auto pvtrkindex = velotrackmap[velotrkindex];
      if ( pvtrkindex >= 0 ) {
        const auto& pvtrack = tracks_scalar[pvtrkindex];
        if ( pvtrack.pvindex().cast() == pvindex ) {
          const auto weight = pvtrack.weight().cast();
          if ( weight > 0 ) {
            accumulator.halfDChi2DX( 0 ) -= weight * pvtrack.halfDChi2DX( 0 ).cast();
            accumulator.halfDChi2DX( 1 ) -= weight * pvtrack.halfDChi2DX( 1 ).cast();
            accumulator.halfDChi2DX( 2 ) -= weight * pvtrack.halfDChi2DX( 2 ).cast();
            accumulator.halfD2Chi2DX2( 0, 0 ) -=
                weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v00 ).cast();
            accumulator.halfD2Chi2DX2( 1, 1 ) -=
                weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v11 ).cast();
            accumulator.halfD2Chi2DX2( 2, 0 ) -=
                weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v20 ).cast();
            accumulator.halfD2Chi2DX2( 2, 1 ) -=
                weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v21 ).cast();
            accumulator.halfD2Chi2DX2( 2, 2 ) -=
                weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v22 ).cast();
            accumulator.chi2 -= weight * pvtrack.ipchi2().cast();
            accumulator.entries -= 1;
          }
        }
      }
    }
    // 3. update the vertex position
    if ( accumulator.entries != 0 ) {
      accumulator.halfD2Chi2DX2 = accumulator.halfD2Chi2DX2 + pv.weightMatrix();
      accumulator.chi2 += pv.chi2();
      accumulator.entries += ( pv.nDoF() + 3 ) / 2; // reverse engineer
      updatePrimaryVertex( pv, accumulator, 0., 0. );
    }
    return pv;
  };

  PrimaryVertices refittedVertices( const PrimaryVertexContainer& pvdata, LHCb::span<int> velotrackindices,
                                    float chi2max, float maxDeltaZConverged, float maxDeltaChi2Converged,
                                    uint16_t maxNumIter ) {
    /// copy the pvdata
    auto newpvdata{pvdata};
    if ( velotrackindices.size() > 0 ) {
      // mask the tracks
      const auto&                              tracks_scalar = newpvdata.tracks.scalar();
      const auto&                              velotrackmap  = pvdata.velotrackmap;
      boost::container::small_vector<bool, 16> pvchanged( newpvdata.vertices.size(), false );
      for ( auto velotrkindex : velotrackindices ) {
        const auto pvtrkindex = velotrackmap[velotrkindex];
        if ( pvtrkindex >= 0 ) {
          // we exploit that the vertex fit masks tracks with negative pvindex
          const auto pvindex = tracks_scalar[pvtrkindex].get<PVTrackTag::pvindex>().cast();
          if ( pvindex >= 0 ) {
            pvchanged[pvindex] = true;
            tracks_scalar[pvtrkindex].field<PVTrackTag::pvindex>().set( -1 );
          }
        }
      }
      // call the vertex fit: this refits all PVs, but should be very quick
      // fitAdaptive( newpvdata.vertices, newpvdata.tracks,chi2max ) ;
      // Warning: the track data is not returned. if we return that
      // too, then we need to fit all vertices, because (depending on
      // what we do in the vertex fit), we may be overwriting data
      // that belongs to the next track.
      for ( size_t pvindex = 0; pvindex < newpvdata.vertices.size(); ++pvindex )
        if ( pvchanged[pvindex] )
          fitAdaptive( newpvdata.vertices[pvindex], newpvdata.tracks, chi2max, maxDeltaZConverged,
                       maxDeltaChi2Converged, maxNumIter );
    }
    return std::move( newpvdata.vertices );
  }

} // namespace LHCb::Event::PV
