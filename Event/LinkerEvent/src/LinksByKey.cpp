/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/LinkManager.h"
#include "GaudiKernel/SmartDataPtr.h"

// local
#include "Event/LinksByKey.h"

//-----------------------------------------------------------------------------
// Implementation file for class : LinksByKey
//
// 2004-01-06 : Olivier Callot
//-----------------------------------------------------------------------------
namespace {
  template <typename T1, typename T2, typename F>
  constexpr decltype( auto ) with_selection_invoke( bool b, T1&& t1, T2&& t2, F&& f ) {
    return b ? std::invoke( std::forward<F>( f ), std::forward<T1>( t1 ) )
             : std::invoke( std::forward<F>( f ), std::forward<T2>( t2 ) );
  }
} // namespace

//=========================================================================
// Resolve the links, loading the containers if needed
//=========================================================================
void LHCb::LinksByKey::resolveLinks( IDataProviderSvc* eventSvc ) {
  for ( LinkManager::Link const& link : *linkMgr() ) {
    if ( link.object() ) continue;
    SmartDataPtr<DataObject> tmp( eventSvc, link.path() );
    const_cast<LinkManager::Link&>( link ).setObject( tmp );
  }
}
//=========================================================================
//  Add a reference for a given key and container link.
//=========================================================================
void LHCb::LinksByKey::addReference( int srcKey, int srcLinkID, int destKey, int destLinkID, double weight ) {

  //== Create the LinkReference, and push it in the vector

  LHCb::LinkReference temp( srcLinkID, destLinkID, destKey, -1, weight );
  unsigned int        refNum = m_linkReference.size();

  //== Now get the map entry for this key, if any.

  int indx = -1;
  if ( !findIndex( srcKey, indx ) ) {
    assert( indx >= 0 && static_cast<size_t>( indx ) <= m_keyIndex.size() );
    m_keyIndex.insert( std::next( m_keyIndex.begin(), indx ), {srcKey, refNum} );
    m_linkReference.push_back( temp );

    //== Test of proper ordering

    // int before = -1;
    // for ( unsigned int kk = 0 ; m_keyIndex.size() > kk ; kk++ ) {
    //  if ( before >= m_keyIndex[kk].first ) {
    //    std::cout << "=== Not sorted Link key " << srcKey
    //              << " index inserted " << indx
    //              << " fault at " << kk
    //              << " before = " << before
    //              << " current " <<  m_keyIndex[kk].first
    //              << std::endl;
    //  }
    //  before = m_keyIndex[kk].first;
    //}
  }

  //== Test if the same entry exists; if so, update weight
  for ( int prevIndex = m_keyIndex[indx].second; prevIndex >= 0; prevIndex = m_linkReference[prevIndex].nextIndex() ) {
    if ( srcLinkID == m_linkReference[prevIndex].srcLinkID() && destLinkID == m_linkReference[prevIndex].linkID() &&
         destKey == m_linkReference[prevIndex].objectKey() ) {
      m_linkReference[prevIndex].setWeight( weight );
      return;
    }
  }

  // Store new linkReference
  m_linkReference.push_back( temp );

  // Put the new entry at the proper position in the chain...
  with_selection_invoke( m_increasing, std::less<>{}, std::greater<>{}, [&]( auto cmp ) {
    int prevIndex = m_keyIndex[indx].second;
    if ( cmp( weight, m_linkReference[prevIndex].weight() ) ) {
      m_linkReference[refNum].setNextIndex( prevIndex );
      m_keyIndex[indx].second = refNum;
    } else {
      for ( int nextIndex = m_linkReference[prevIndex].nextIndex(); nextIndex >= 0;
            prevIndex     = std::exchange( nextIndex, m_linkReference[nextIndex].nextIndex() ) ) {
        if ( cmp( weight, m_linkReference[nextIndex].weight() ) ) {
          m_linkReference[prevIndex].setNextIndex( refNum );
          m_linkReference[refNum].setNextIndex( nextIndex );
          break;
        }
      }
      m_linkReference[prevIndex].setNextIndex( refNum );
    }
  } );
}
//=========================================================================
//  Returns the first reference for the given key
//=========================================================================
bool LHCb::LinksByKey::firstReference( int key, const DataObject* container, LHCb::LinkReference& reference ) const {
  int linkID = -1; // Case with only a key
  if ( container ) {
    LinkManager::Link const* link = linkMgr()->link( container ); // test with pointer
    if ( !link ) {                                                // try with name, and store pointer if OK
      LinkManager::Link* mlink =
          const_cast<LinksByKey*>( this )->linkMgr()->link( container->registry()->identifier() );
      if ( mlink ) mlink->setObject( container );
      link = mlink;
    }
    if ( !link ) return false;
    linkID = link->ID();
  }

  int index{-1};
  if ( findIndex( key, index ) ) {
    reference = m_linkReference[m_keyIndex[index].second];
    if ( 0 <= linkID ) {
      while ( linkID != reference.srcLinkID() ) {
        if ( reference.nextIndex() < 0 ) return false;
        reference = m_linkReference[reference.nextIndex()];
      }
    } else {
      reference.setSrcLinkID( -1 );
    }
    return true;
  }
  return false;
}

//=========================================================================
//  Returns the next reference from the specified reference.
//  returns false if no more
//=========================================================================
bool LHCb::LinksByKey::nextReference( LHCb::LinkReference& reference ) const {
  if ( reference.nextIndex() < 0 ) return false;

  int linkID = reference.srcLinkID();
  reference  = m_linkReference[reference.nextIndex()];
  if ( 0 <= linkID ) {
    while ( linkID != reference.srcLinkID() ) {
      if ( reference.nextIndex() < 0 ) { return false; }
      reference = m_linkReference[reference.nextIndex()];
    }
  } else {
    reference.setSrcLinkID( -1 );
  }
  return true;
}

//=========================================================================
//  Checks whether a given object has any link
//=========================================================================
bool LHCb::LinksByKey::hasEntry( const KeyedObject<int>& obj ) const {
  int index{-1};
  return findIndex( obj.index(), index );
}

//=========================================================================
// Returns the first key for which the specified reference exists
//=========================================================================
int LHCb::LinksByKey::firstSource( LHCb::LinkReference&                              reference,
                                   std::vector<std::pair<int, int>>::const_iterator& iter ) const {
  iter = m_keyIndex.begin();
  reference.setNextIndex( -1 ); // Indicate to restart at this iter's content
  return nextSource( reference, iter );
}

//=========================================================================
// Returns the next key for which the specified reference exists
//=========================================================================
int LHCb::LinksByKey::nextSource( LHCb::LinkReference&                              reference,
                                  std::vector<std::pair<int, int>>::const_iterator& iter ) const {
  int refNum = reference.nextIndex(); // next entry
  while ( iter != m_keyIndex.end() ) {
    if ( refNum < 0 ) refNum = iter->second; // first of this iter
    while ( refNum >= 0 ) {
      auto const& temp = m_linkReference[refNum];
      if ( temp.linkID() == reference.linkID() && temp.objectKey() == reference.objectKey() ) {
        reference = temp; // keep track of where we stopped
        int key   = iter->first;
        if ( temp.nextIndex() < 0 ) ++iter; // If last for entry, go to next
        return key;
      }
      refNum = temp.nextIndex();
    }
    ++iter;
  }
  // indicate that we have finished: Key is no longer the one asked first !
  reference.setObjectKey( -1 );
  return -1;
}

//=========================================================================
//  Returns the ID in the link table of the given object
//=========================================================================
int LHCb::LinksByKey::linkID( const DataObject* obj ) {
  LinkManager::Link const* link = linkMgr()->link( obj );
  return link ? link->ID() : linkMgr()->addLink( obj->registry()->identifier(), obj );
}

//=========================================================================
//  Find the index of a given key in m_keyIndex. False if not found.
//=========================================================================
bool LHCb::LinksByKey::findIndex( int key, int& index ) const {
  auto i =
      std::partition_point( m_keyIndex.begin(), m_keyIndex.end(), [=]( const auto& ki ) { return ki.first < key; } );
  index = std::distance( m_keyIndex.begin(), i );
  return i != m_keyIndex.end() && i->first == key;
}
//=============================================================================

LHCb::LinksByKey* LHCb::LinksByKey::createOnTES( IDataProviderSvc* evtSvc, std::string const& containerName,
                                                 const CLID& source, const CLID& target,
                                                 LHCb::LinksByKey::Order order ) {
  LHCb::LinksByKey* lbk = nullptr;
  if ( containerName.empty() ) {
    throw GaudiException{"Empty containerName", "LinkerWithKey::create", StatusCode::FAILURE};
  }
  std::string name = linkerName( containerName );

  //== If it exists, just append to it.
  SmartDataPtr<LHCb::LinksByKey> links( evtSvc, name );
  if ( links ) {
    lbk = links;
    lbk->requireSourceID( source );
    lbk->requireTargetID( target );
    if ( lbk->order() != order ) {
      throw GaudiException( "Invalid LinkerWithKey: inconsistent prior ordering;", "LinkerWithKey::LinkerWithKey",
                            StatusCode::FAILURE );
    }
  } else {
    lbk           = new LHCb::LinksByKey{source, target, LHCb::LinksByKey::Order::decreasingWeight};
    StatusCode sc = evtSvc->registerObject( name, lbk );
    if ( !sc ) { throw GaudiException( "Failed to register " + name, "LinkerWithKey::LinkerWithKey", sc ); }
  }

  return lbk;
};

//=============================================================================
