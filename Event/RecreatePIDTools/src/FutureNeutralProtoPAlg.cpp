/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/TrackUtils.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "DetDesc/IDetectorElement.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloHypo.h"
#include "Event/ProtoParticle.h"
#include "Event/RelationTables.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/Relation1D.h"
#include "fmt/format.h"

namespace LHCb {
  /**
   *  Creator of the neutral ProtoParticles from CaloHypos
   *
   *  The current version fills the following estimators for ProtoParticle
   *
   *  <ul>
   *  <li>  <i>CaloTrMatch</i>     as <b>minimal</b> of this estimator for all
   *        linked <i>CaloHypo</i> objects. The value is extracted from
   *        the relation table/associator as a relation weigth between
   *        <i>CaloCluster</i> and <i>TrStoredTrack</i> objects </li>
   *  <li>  <i>CaloDepositID</i>   as <b>maximal</b> of this estimator for all
   *        linked <i>CaloHypo</i> objects using Spd/Prs estimator tool
   *        written by Frederic Machefert </li>
   *  <li>  <i>CaloShowerShape</i> as <b>maximal</b> of the estimator for
   *        all linked <i>CaloHypo</i> objects. Estimator is equal to the
   *        sum of diagonal elements of cluster spread matrix (2nd order
   *        moments of the cluster) </li>
   *  <li>  <i>ClusterMass</i>     as <b>maximal</b> of the estimator of
   *        cluster mass using smart algorithm by Olivier Deschamp </li>
   *  <li>  <i>PhotonID</i>        as the estimator of PhotonID
   *        using nice identifiaction tool
   *        CaloPhotonEstimatorTool by Frederic Machefert *
   *  </ul>
   *
   *
   *  @author Olivier Deschamps
   *  @date   2006-06-09
   *  Adapted from NeutralPPsFromCPsAlg class (Vanya Belyaev Ivan.Belyaev@itep.ru)
   */

  using TrackMatchTable = LHCb::Calo::TrackUtils::Clusters2BestTrackMatch;

  class FutureNeutralProtoPAlg final
      : public LHCb::Algorithm::MultiTransformer<
            std::tuple<ProtoParticles, Relation1D<ProtoParticle, double>, Relation1D<ProtoParticle, double>,
                       Relation1D<ProtoParticle, double>, Relation1D<ProtoParticle, double>>(
                const CaloHypos&, const CaloHypos&, const CaloHypos&, const TrackMatchTable&, const DeCalorimeter&,
                const DeCalorimeter& ),
            DetDesc::usesConditions<DeCalorimeter, DeCalorimeter>> {

  public:
    /// Standard constructor
    FutureNeutralProtoPAlg( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator,
                            {KeyValue{"MergedPi0s", CaloHypoLocation::MergedPi0s},
                             KeyValue{"Photons", CaloHypoLocation::Photons},
                             KeyValue{"SplitPhotons", CaloHypoLocation::SplitPhotons}, KeyValue{"TrackMatchTable", ""},
                             KeyValue{"DeEcal", Calo::Utilities::DeCaloFutureLocation( "Ecal" )},
                             KeyValue{"DeHcal", Calo::Utilities::DeCaloFutureLocation( "Hcal" )}},
                            {
                                KeyValue{"ProtoParticleLocation", ProtoParticleLocation::Neutrals},
                                KeyValue{"ClusterMass", ""},
                                KeyValue{"CaloClusterCode", ""},
                                KeyValue{"IsNotH", ""},
                                KeyValue{"IsPhoton", ""},
                            } ){};

    StatusCode initialize() override; ///< Algorithm initialization

    std::tuple<ProtoParticles, Relation1D<ProtoParticle, double>, Relation1D<ProtoParticle, double>,
               Relation1D<ProtoParticle, double>, Relation1D<ProtoParticle, double>>
    operator()( const CaloHypos&, const CaloHypos&, const CaloHypos&, const TrackMatchTable&, const DeCalorimeter&,
                const DeCalorimeter& ) const override;

  private:
    void pushData( std::map<Calo::Enum::DataType, double> const& data, ProtoParticle* proto,
                   ProtoParticle::additionalInfo pflag, Calo::Enum::DataType hflag, CaloHypo::Hypothesis hypothesis,
                   double def = Calo::Enum::Default, bool force = false ) const;

    void pushTable( std::map<Calo::Enum::DataType, double> const& data, ProtoParticle* proto, relationTables pflag,
                    Calo::Enum::DataType hflag, CaloHypo::Hypothesis hypothesis,
                    std::map<relationTables, Relation1D<ProtoParticle, double>*>& relations ) const;

  private: // data
    Gaudi::Property<bool> m_light_mode{this, "LightMode", false,
                                       "Use 'light' mode and do not collect all information. Useful for Calibration."};

    ToolHandle<Calo::Interfaces::IHypoEstimator> m_estimator{this, "CaloHypoEstimator", "CaloFutureHypoEstimator"};
    mutable Gaudi::Accumulators::StatCounter<>   m_countMergedPi0s{this, "Neutral Protos from MergedPi0s"};
    mutable Gaudi::Accumulators::StatCounter<>   m_countPhotons{this, "Neutral Protos from Photons"};
    mutable Gaudi::Accumulators::StatCounter<>   m_countSplitPhotons{this, "Neutral Protos from SplitPhotons"};
    mutable Gaudi::Accumulators::StatCounter<>   m_countProtos{this, "Neutral Protos"};
    // for now, lazily use std::map.
    // TODO: switch to index obtained from perfect hashing of all possible keys
    // AdditionalInfo - remove from here
    using Key_AI_t = std::pair<CaloHypo::Hypothesis, ProtoParticle::additionalInfo>;
    std::map<Key_AI_t, Gaudi::Accumulators::StatCounter<>>         init_AI_counters();
    mutable std::map<Key_AI_t, Gaudi::Accumulators::StatCounter<>> m_AI_hypoDataCounters = init_AI_counters();
    // AdditionalInfo - remove until here
    // RelationTables counter - replaces above AI counter
    using Key_t = std::pair<CaloHypo::Hypothesis, relationTables>;
    std::map<Key_t, Gaudi::Accumulators::SigmaCounter<>>         init_counters();
    mutable std::map<Key_t, Gaudi::Accumulators::SigmaCounter<>> m_hypoDataCounters = init_counters();
  };

  // AdditionalInfo - remove from here
  void FutureNeutralProtoPAlg::pushData( std::map<Calo::Enum::DataType, double> const& data, ProtoParticle* proto,
                                         ProtoParticle::additionalInfo pflag, Calo::Enum::DataType hflag,
                                         CaloHypo::Hypothesis hypothesis, const double def, const bool force ) const {
    auto it    = data.find( hflag );
    auto value = ( it != data.end() ? it->second : def );
    if ( value != def || force ) {
      proto->addInfo( pflag, value ); // only store when different from default
      m_AI_hypoDataCounters.at( Key_AI_t{hypothesis, pflag} ) += value;
    }
  }
  // AdditionalInfo - remove until here
  // RelationTables - replaces above
  void
  FutureNeutralProtoPAlg::pushTable( std::map<Calo::Enum::DataType, double> const& data, ProtoParticle* proto,
                                     relationTables pflag, Calo::Enum::DataType hflag, CaloHypo::Hypothesis hypothesis,
                                     std::map<relationTables, Relation1D<ProtoParticle, double>*>& relations ) const {
    auto it = data.find( hflag );
    if ( it != data.end() ) {
      relations[pflag]->relate( proto, it->second ).ignore();
      m_hypoDataCounters.at( Key_t{hypothesis, pflag} ) += it->second;
    }
  }
  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( FutureNeutralProtoPAlg, "FutureNeutralProtoPAlg" )
} // namespace LHCb

StatusCode LHCb::FutureNeutralProtoPAlg::initialize() {
  return MultiTransformer::initialize().andThen( [&] {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

    if ( m_light_mode.value() ) info() << "FutureNeutral protoparticles will be created in 'Light' Mode" << endmsg;

    // TODO: Move to configuration
    auto& h2c = dynamic_cast<IProperty&>( *m_estimator->hypo2Calo() );
    h2c.setProperty( "Seed", "false" ).ignore();
    h2c.setProperty( "PhotonLine", "true" ).ignore();
    h2c.setProperty( "AddNeighbors", "false" ).ignore();
  } );
}

std::tuple<LHCb::ProtoParticles, LHCb::Relation1D<LHCb::ProtoParticle, double>,
           LHCb::Relation1D<LHCb::ProtoParticle, double>, LHCb::Relation1D<LHCb::ProtoParticle, double>,
           LHCb::Relation1D<LHCb::ProtoParticle, double>>
LHCb::FutureNeutralProtoPAlg::
operator()( const LHCb::CaloHypos& hypos_from_mergedPi0s, const LHCb::CaloHypos& hypos_from_Photons,
            const LHCb::CaloHypos& hypos_from_SplitPhotons, const TrackMatchTable& tracktable,
            const DeCalorimeter& ecal, const DeCalorimeter& hcal ) const {

  auto ret =
      std::tuple{LHCb::ProtoParticles{}, LHCb::Relation1D<LHCb::ProtoParticle, double>{},
                 LHCb::Relation1D<LHCb::ProtoParticle, double>{}, LHCb::Relation1D<LHCb::ProtoParticle, double>{},
                 LHCb::Relation1D<LHCb::ProtoParticle, double>{}};

  auto& [protos, table_ClusterMass, table_CaloClusterCode, table_IsNotH, table_IsPhoton] = ret;

  auto relations = std::map{std::pair{LHCb::relationTables::ClusterMass, &table_ClusterMass},
                            std::pair{LHCb::relationTables::CaloClusterCode, &table_CaloClusterCode},
                            std::pair{LHCb::relationTables::IsNotH, &table_IsNotH},
                            std::pair{LHCb::relationTables::IsPhoton, &table_IsPhoton}};

  // prepare cluster access, both to find clusters (indexed by cellids) and relation tabled viewed from clusters
  auto clusters  = tracktable.from()->index();
  auto tableview = tracktable.buildView();

  // wrapper around data map getter, to include track match table the estimator doesn't have access to
  auto getData = [&]( auto const* hypo ) {
    // hypo data
    auto data = m_estimator->get_data( ecal, hcal, *hypo );
    // track relation
    using namespace LHCb::Calo::Enum;
    auto it      = data.find( DataType::CellID );
    auto cellid  = ( it != data.end() ) ? static_cast<unsigned>( it->second ) : 0u;
    auto cluster = clusters.find( LHCb::Detector::Calo::CellID( cellid ) );
    if ( cluster != clusters.end() ) {
      auto clsview = tableview.scalar()[cluster->indices().cast()];
      if ( clsview.hasRelation() )
        data[DataType::ClusterMatch] = clsview.relation().template get<LHCb::Calo::TrackUtils::ClusterMatch>().cast();
    }
    return data;
  };

  // -- reset mass storage
  std::map<const int, double> mass_per_cell = {{}};
  // Get masses
  for ( const auto* hypo : hypos_from_mergedPi0s ) {
    if ( !hypo ) continue;
    using namespace LHCb::Calo::Enum;
    // The call to process is happening twice for mergedPi0s, should be made smarter.
    auto      data          = getData( hypo );
    const int cellCode      = data.at( DataType::CellID );
    auto      it            = data.find( DataType::HypoM );
    mass_per_cell[cellCode] = ( it != data.end() ? it->second : Default );
  }

  //------ loop over all caloHypo containers
  auto append_protos_from_location = [&]( const auto& hypos, auto count_protos ) {
    // this needs to be re-declared because of the bug in clang 12 that reference to local binding fails in enclosing
    // function
    auto& [protos, table_ClusterMass, table_CaloClusterCode, table_IsNotH, table_IsPhoton] = ret;

    int count = 0;

    // == Loop over CaloHypos
    for ( const auto* hypo : hypos ) {
      if ( !hypo ) { continue; }
      count++;

      // == create and store the corresponding ProtoParticle
      auto* proto = new LHCb::ProtoParticle();
      protos.insert( proto );

      // == link CaloHypo to ProtoP
      using namespace LHCb::Calo::Enum;
      proto->addToCalo( hypo );
      if ( m_light_mode.value() ) continue;

      const auto hypothesis = hypo->hypothesis();

      auto data = getData( hypo );

      // AdditionalInfo - remove from here
      auto pushAI = [&]( LHCb::ProtoParticle::additionalInfo ai, DataType dt, auto&&... args ) {
        pushData( data, proto, ai, dt, hypothesis, std::forward<decltype( args )>( args )... );
      };

      pushAI( LHCb::ProtoParticle::additionalInfo::CaloNeutralID, DataType::CellID ); // seed cellID

      // retrieve HypoM for photon
      auto      ite      = data.find( DataType::CellID );
      const int CellCode = ( ite != data.end() ) ? ite->second : Default;
      if ( hypothesis == LHCb::CaloHypo::Hypothesis::Photon || hypothesis == LHCb::CaloHypo::Hypothesis::Pi0Merged ) {
        const auto ite  = mass_per_cell.find( CellCode );
        const auto Mass = ( ite == mass_per_cell.end() ) ? 0.0 : ite->second;
        if ( Mass > 0. ) { proto->addInfo( LHCb::ProtoParticle::additionalInfo::ClusterMass, Mass ); }
      }

      if ( hypothesis != LHCb::CaloHypo::Hypothesis::Pi0Merged ) {
        pushAI( LHCb::ProtoParticle::additionalInfo::ClusterAsX, DataType::ClusterAsX, 0 );
        pushAI( LHCb::ProtoParticle::additionalInfo::ClusterAsY, DataType::ClusterAsY, 0 );
      }

      pushAI( LHCb::ProtoParticle::additionalInfo::CaloNeutralEcal, DataType::ClusterE );

      // old isNotX  inputs :
      pushAI( LHCb::ProtoParticle::additionalInfo::CaloTrMatch, DataType::ClusterMatch, +1.e+06 );
      pushAI( LHCb::ProtoParticle::additionalInfo::ShowerShape, DataType::Spread );
      pushAI( LHCb::ProtoParticle::additionalInfo::CaloNeutralHcal2Ecal, DataType::Hcal2Ecal );
      pushAI( LHCb::ProtoParticle::additionalInfo::CaloNeutralE49, DataType::E49 );
      pushAI( LHCb::ProtoParticle::additionalInfo::CaloNeutralE19, DataType::E19 );

      pushAI( LHCb::ProtoParticle::additionalInfo::CaloClusterCode, DataType::ClusterCode );
      pushAI( LHCb::ProtoParticle::additionalInfo::CaloClusterFrac, DataType::ClusterFrac, 1 );
      pushAI( LHCb::ProtoParticle::additionalInfo::Saturation, DataType::Saturation, 0 );

      // isPhoton and isNotH outputs (photon & mergedPi0 only)
      if ( hypothesis != LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 ) {
        pushAI( LHCb::ProtoParticle::additionalInfo::IsNotH, DataType::isNotH, -1.,
                true ); // NN-based neutral-ID (anti-h and PU)
        pushAI( LHCb::ProtoParticle::additionalInfo::IsPhoton, DataType::isPhoton, +1.,
                true ); // NN-based neutral-ID (anti-pi0) // FORCE to +1 when missing (i.e. PT < 2 GeV)
        pushAI( LHCb::ProtoParticle::additionalInfo::IsPhotonXGB, DataType::isPhotonXGB, +1.,
                true ); // XGBoost-based neutral-ID (anti-pi0) // FORCE to +1 when missing (i.e. PT < 2 GeV)
      }
      // AdditionalInfo - remove until here

      // add data to Relation tables
      auto push = [&]( LHCb::relationTables ai, DataType dt,
                       std::map<LHCb::relationTables, LHCb::Relation1D<LHCb::ProtoParticle, double>*> relations ) {
        pushTable( data, proto, ai, dt, hypothesis, relations );
      };

      // retrieve HypoM for photon
      auto      it       = data.find( DataType::CellID );
      const int cellCode = ( it != data.end() ) ? it->second : Default;
      if ( hypothesis == LHCb::CaloHypo::Hypothesis::Photon || hypothesis == LHCb::CaloHypo::Hypothesis::Pi0Merged ) {
        const auto it   = mass_per_cell.find( cellCode );
        const auto mass = ( it == mass_per_cell.end() ) ? 0.0 : it->second;
        if ( mass > 0. ) { relations[LHCb::relationTables::ClusterMass]->relate( proto, mass ).ignore(); }
      }

      // old isNotX  inputs :
      push( LHCb::relationTables::CaloClusterCode, DataType::ClusterCode, relations );

      // isPhoton and isNotH outputs (photon & mergedPi0 only)
      if ( hypothesis != LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 ) {
        push( LHCb::relationTables::IsNotH, DataType::isNotH,
              relations ); // NN-based neutral-ID (anti-h and PU)
        push( LHCb::relationTables::IsPhoton, DataType::isPhoton,
              relations ); // NN-based neutral-ID (anti-pi0)
      }

    } // loop over CaloHypos
    count_protos += count;
  };

  append_protos_from_location( hypos_from_mergedPi0s, m_countMergedPi0s.buffer() );
  append_protos_from_location( hypos_from_Photons, m_countPhotons.buffer() );
  append_protos_from_location( hypos_from_SplitPhotons, m_countSplitPhotons.buffer() );

  m_countProtos += protos.size();

  return ret;
}

// AdditionalInfo - remove from here
std::map<LHCb::FutureNeutralProtoPAlg::Key_AI_t, Gaudi::Accumulators::StatCounter<>>
LHCb::FutureNeutralProtoPAlg::init_AI_counters() {

  std::map<Key_AI_t, Gaudi::Accumulators::StatCounter<>> m;
  for ( auto i :
        {LHCb::ProtoParticle::additionalInfo::CaloNeutralID, LHCb::ProtoParticle::additionalInfo::ClusterMass,
         LHCb::ProtoParticle::additionalInfo::ClusterAsX, LHCb::ProtoParticle::additionalInfo::ClusterAsY,
         LHCb::ProtoParticle::additionalInfo::CaloNeutralEcal, LHCb::ProtoParticle::additionalInfo::CaloTrMatch,
         LHCb::ProtoParticle::additionalInfo::ShowerShape, LHCb::ProtoParticle::additionalInfo::CaloNeutralHcal2Ecal,
         LHCb::ProtoParticle::additionalInfo::CaloClusterCode, LHCb::ProtoParticle::additionalInfo::CaloClusterFrac,
         LHCb::ProtoParticle::additionalInfo::Saturation, LHCb::ProtoParticle::additionalInfo::CaloNeutralE49,
         LHCb::ProtoParticle::additionalInfo::CaloNeutralE19, LHCb::ProtoParticle::additionalInfo::IsNotH,
         LHCb::ProtoParticle::additionalInfo::IsPhoton, LHCb::ProtoParticle::additionalInfo::IsPhotonXGB

        } ) {
    for ( auto j : {LHCb::CaloHypo::Hypothesis::Undefined,
                    LHCb::CaloHypo::Hypothesis::Mip,
                    LHCb::CaloHypo::Hypothesis::MipPositive,
                    LHCb::CaloHypo::Hypothesis::MipNegative,
                    LHCb::CaloHypo::Hypothesis::Photon,
                    LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0,
                    LHCb::CaloHypo::Hypothesis::BremmstrahlungPhoton,
                    LHCb::CaloHypo::Hypothesis::Pi0Resolved,
                    LHCb::CaloHypo::Hypothesis::Pi0Overlapped,
                    LHCb::CaloHypo::Hypothesis::Pi0Merged,
                    LHCb::CaloHypo::Hypothesis::EmCharged,
                    LHCb::CaloHypo::Hypothesis::Positron,
                    LHCb::CaloHypo::Hypothesis::Electron,
                    LHCb::CaloHypo::Hypothesis::EmChargedSeed,
                    LHCb::CaloHypo::Hypothesis::PositronSeed,
                    LHCb::CaloHypo::Hypothesis::ElectronSeed,
                    LHCb::CaloHypo::Hypothesis::NeutralHadron,
                    LHCb::CaloHypo::Hypothesis::ChargedHadron,
                    LHCb::CaloHypo::Hypothesis::PositiveHadron,
                    LHCb::CaloHypo::Hypothesis::NegativeHadron,
                    LHCb::CaloHypo::Hypothesis::Jet,
                    LHCb::CaloHypo::Hypothesis::Other} ) {
      std::ostringstream mess;
      mess << i << " for " << j << " (additionalInfo)";
      m.emplace( std::piecewise_construct, std::tuple{j, i}, std::tuple{this, mess.str()} );
    }
  }
  return m;
}
// AdditionalInfo - remove until here

std::map<LHCb::FutureNeutralProtoPAlg::Key_t, Gaudi::Accumulators::SigmaCounter<>>
LHCb::FutureNeutralProtoPAlg::init_counters() {

  std::map<Key_t, Gaudi::Accumulators::SigmaCounter<>> m;
  for ( auto i : {LHCb::relationTables::ClusterMass, LHCb::relationTables::CaloClusterCode,
                  LHCb::relationTables::IsNotH, LHCb::relationTables::IsPhoton

        } ) {
    for ( auto j : {LHCb::CaloHypo::Hypothesis::Undefined,
                    LHCb::CaloHypo::Hypothesis::Mip,
                    LHCb::CaloHypo::Hypothesis::MipPositive,
                    LHCb::CaloHypo::Hypothesis::MipNegative,
                    LHCb::CaloHypo::Hypothesis::Photon,
                    LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0,
                    LHCb::CaloHypo::Hypothesis::BremmstrahlungPhoton,
                    LHCb::CaloHypo::Hypothesis::Pi0Resolved,
                    LHCb::CaloHypo::Hypothesis::Pi0Overlapped,
                    LHCb::CaloHypo::Hypothesis::Pi0Merged,
                    LHCb::CaloHypo::Hypothesis::EmCharged,
                    LHCb::CaloHypo::Hypothesis::Positron,
                    LHCb::CaloHypo::Hypothesis::Electron,
                    LHCb::CaloHypo::Hypothesis::EmChargedSeed,
                    LHCb::CaloHypo::Hypothesis::PositronSeed,
                    LHCb::CaloHypo::Hypothesis::ElectronSeed,
                    LHCb::CaloHypo::Hypothesis::NeutralHadron,
                    LHCb::CaloHypo::Hypothesis::ChargedHadron,
                    LHCb::CaloHypo::Hypothesis::PositiveHadron,
                    LHCb::CaloHypo::Hypothesis::NegativeHadron,
                    LHCb::CaloHypo::Hypothesis::Jet,
                    LHCb::CaloHypo::Hypothesis::Other} ) {
      std::ostringstream mess;
      mess << i << " for " << j;
      m.emplace( std::piecewise_construct, std::tuple{j, i}, std::tuple{this, mess.str()} );
    }
  }
  return m;
}
