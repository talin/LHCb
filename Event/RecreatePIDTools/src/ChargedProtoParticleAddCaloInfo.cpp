/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/CaloChargedInfo_v1.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiTool.h"
#include "Interfaces/IProtoParticleTool.h"

/** @class ChargedProtoParticleAddCaloInfo ChargedProtoParticleAddCaloInfo.h
 *
 *  Adds calo info to protoparticles used for v3 to v1 conversion
 *
 */

namespace LHCb::Rec::ProtoParticle::Charged {

  namespace v2 {

    template <typename CaloPID>
    using TrackToCaloPID = typename std::map<const Track*, CaloPID const*>;

    // base class
    template <typename CaloPID>
    class AddCaloInfoBase : public extends<GaudiTool, Interfaces::IProtoParticles> {

    public:
      using extends::extends;
      using CaloPIDs = typename CaloPID::Container;

      // initialize inputs
      StatusCode initialize() override {
        auto sc = GaudiTool::initialize();
        if ( !sc ) return sc;
        // declare inputs
        int i = 0;
        for ( auto input : m_inputs ) {
          m_inputhandles.push_back( std::make_unique<DataObjectReadHandle<CaloPIDs>>( input, this ) );
          declareProperty( "input_" + std::to_string( i ), *( m_inputhandles.back() ) );
          i++;
        }
        return sc;
      }

    protected:
      /// Load the pid/track relations in a map
      TrackToCaloPID<CaloPID> getPIDsMap() const {
        TrackToCaloPID<CaloPID> map{};
        for ( auto const& input : m_inputhandles ) {
          CaloPIDs const* pids = input->get();
          for ( auto const& pid : *pids ) {
            if ( pid->idTrack() ) { map[pid->idTrack()] = pid; }
          }
        }
        return map;
      };

    private:
      Gaudi::Property<std::vector<std::string>> m_inputs{this, "InputPIDs", {}, ""};

      std::vector<std::unique_ptr<DataObjectReadHandle<CaloPIDs>>> m_inputhandles;
    };

    //=============================================================================
    // specific implementations
    //=============================================================================
    using namespace LHCb::Event::Calo::v1;
    using additionalInfo = LHCb::ProtoParticle::additionalInfo;

    // Ecal info
    struct AddEcalInfo final : AddCaloInfoBase<ChargedPID> {
      // constructor
      AddEcalInfo( std::string const& type, std::string const& name, IInterface const* parent )
          : AddCaloInfoBase<ChargedPID>::AddCaloInfoBase( type, name, parent ) {}

      // main execution
      StatusCode operator()( ProtoParticles& protos, IGeometryInfo const& ) const override {
        // make map
        auto map = getPIDsMap();
        // update all relevant protoparticles
        for ( auto proto : protos ) {
          // remove current info
          proto->removeCaloEcalInfo();
          // find pp with track
          if ( !proto->track() ) continue;
          auto relation = map.find( proto->track() );
          if ( map.end() == relation ) {
            if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> NO associated CaloPID object found" << endmsg;
            continue;
          }

          auto const* pid = relation->second;
          // skip if not in acceptance
          if ( !pid->InEcal() ) continue;
          // store pid info
          auto track_p = proto->track()->p();
          proto->addInfo( additionalInfo::InAccEcal, true );
          proto->addInfo( additionalInfo::CaloClusChi2, pid->ClusterMatch() );
          proto->addInfo( additionalInfo::CaloTrMatch, pid->ClusterMatch() );
          proto->addInfo( additionalInfo::CaloChargedID, pid->ElectronID().all() );
          proto->addInfo( additionalInfo::CaloElectronMatch, pid->ElectronMatch() );
          proto->addInfo( additionalInfo::CaloChargedEcal, pid->ElectronEnergy() );
          proto->addInfo( additionalInfo::CaloEoverP, pid->ElectronShowerEoP() );
          proto->addInfo( additionalInfo::CaloEcalE, pid->ElectronShowerEoP() * track_p );
          proto->addInfo( additionalInfo::EcalPIDe, pid->EcalPIDe() );
          proto->addInfo( additionalInfo::EcalPIDmu, pid->EcalPIDmu() );
        }
        return StatusCode::SUCCESS;
      }
    };

    // Hcal info
    struct AddHcalInfo final : AddCaloInfoBase<ChargedPID> {
      // constructor
      AddHcalInfo( std::string const& type, std::string const& name, IInterface const* parent )
          : AddCaloInfoBase<ChargedPID>::AddCaloInfoBase( type, name, parent ) {}

      // main execution
      StatusCode operator()( ProtoParticles& protos, IGeometryInfo const& ) const override {
        // make map
        auto map = getPIDsMap();
        // update all relevant protoparticles
        for ( auto proto : protos ) {
          // remove current info
          proto->removeCaloHcalInfo();
          // find pp with track
          if ( !proto->track() ) continue;
          auto relation = map.find( proto->track() );
          if ( map.end() == relation ) {
            if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> NO associated CaloPID object found" << endmsg;
            continue;
          }

          auto const* pid = relation->second;
          // skip if not in acceptance
          if ( !pid->InHcal() ) continue;
          // store pid info
          auto track_p = proto->track()->p();
          proto->addInfo( additionalInfo::InAccHcal, true );
          proto->addInfo( additionalInfo::CaloHcalE, pid->HcalEoP() * track_p );
          proto->addInfo( additionalInfo::HcalPIDe, pid->HcalPIDe() );
          proto->addInfo( additionalInfo::HcalPIDmu, pid->HcalPIDmu() );
        }
        return StatusCode::SUCCESS;
      }
    };

    // Brem info
    struct AddBremInfo final : AddCaloInfoBase<BremInfo> {
      // constructor
      AddBremInfo( std::string const& type, std::string const& name, IInterface const* parent )
          : AddCaloInfoBase<BremInfo>::AddCaloInfoBase( type, name, parent ) {}

      // main execution
      StatusCode operator()( ProtoParticles& protos, IGeometryInfo const& ) const override {
        // make map
        auto map = getPIDsMap();
        // update all relevant protoparticles
        for ( auto proto : protos ) {
          // remove current info
          proto->removeCaloBremInfo();
          // find pp with track
          if ( !proto->track() ) continue;
          auto relation = map.find( proto->track() );
          if ( map.end() == relation ) {
            if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> NO associated CaloPID object found" << endmsg;
            continue;
          }

          auto const* pid = relation->second;
          // skip if not in acceptance
          if ( !pid->InBrem() ) continue;
          // store pid info
          proto->addInfo( additionalInfo::InAccBrem, true );
          proto->addInfo( additionalInfo::CaloBremMatch, pid->BremHypoMatch() );
          proto->addInfo( additionalInfo::CaloBremHypoID, pid->BremHypoID().all() );
          proto->addInfo( additionalInfo::CaloBremHypoEnergy, pid->BremHypoEnergy() );
          proto->addInfo( additionalInfo::CaloBremHypoDeltaX, pid->BremHypoDeltaX() );
          proto->addInfo( additionalInfo::CaloBremTBEnergy, pid->BremTrackBasedEnergy() );
          proto->addInfo( additionalInfo::CaloBremBendingCorr, pid->BremBendingCorrection() );
          proto->addInfo( additionalInfo::CaloBremEnergy, pid->BremEnergy() );
          proto->addInfo( additionalInfo::CaloHasBrem, pid->HasBrem() );
          proto->addInfo( additionalInfo::BremPIDe, pid->BremPIDe() );
        }
        return StatusCode::SUCCESS;
      }
    };

  } // namespace v2

  DECLARE_COMPONENT_WITH_ID( v2::AddEcalInfo, "ChargedProtoParticleAddEcalInfo" )
  DECLARE_COMPONENT_WITH_ID( v2::AddHcalInfo, "ChargedProtoParticleAddHcalInfo" )
  DECLARE_COMPONENT_WITH_ID( v2::AddBremInfo, "ChargedProtoParticleAddBremInfo" )

} // namespace LHCb::Rec::ProtoParticle::Charged
