/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ProtoParticle.h"
#include "LHCbAlgs/Consumer.h"

namespace LHCb {

  struct PrintProtoParticles final : Algorithm::Consumer<void( LHCb::ProtoParticles const& )> {
    PrintProtoParticles( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer{name, pSvcLocator, {"Input", ""}} {};

    void operator()( LHCb::ProtoParticles const& protos ) const override {
      always() << "Print all proto particles of an event" << endmsg;
      for ( auto const* proto : protos ) {
        always() << "{"
                 << " Track " << ( proto->track() != nullptr ) << " CaloHypos " << proto->calo().size() << " RichPID "
                 << ( proto->richPID() != nullptr ) << " MuonPID " << ( proto->muonPID() != nullptr ) << "\n"
                 << "ExtraInfo ["
                 << "\n";
        for ( const auto& i : proto->extraInfo() ) {
          always() << std::left << std::setw( 25 ) << static_cast<LHCb::ProtoParticle::additionalInfo>( i.first )
                   << "= " << i.second << "\n";
        }
        always() << " ] } \n";
      }
      always() << "Printing ended" << endmsg;
    }
  };

  DECLARE_COMPONENT_WITH_ID( PrintProtoParticles, "PrintProtoParticles" )

} // namespace LHCb
