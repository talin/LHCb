/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Event
#include "Event/MCParticle.h"

// STL
#include <algorithm>
#include <limits>

//-----------------------------------------------------------------------------
// Implementation file for class : MCParticle
//
// 2012-11-20 : Chris Jones
//-----------------------------------------------------------------------------

std::ostream& LHCb::MCParticle::fillStream( std::ostream& s ) const {
  s << "{ Momentum = " << momentum() << " ParticleID = " << particleID() << " OriginVertex = " << originVertex()
    << " EndVertices : #=" << endVertices().size() << " [";
  for ( const auto& v : endVertices() ) { s << " " << v.target(); }
  s << " ] }";
  return s;
}

namespace LHCb {

  LinAlg::Vec<SIMDWrapper::scalar::float_v, 3> referencePoint( const MCParticle& p ) {
    auto const& pos = p.originVertex()->position();
    return LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{pos.x(), pos.y(), pos.z()};
  }

  /**
   * @brief Get MCParticle's first end-vertex that likely destroyed the particle
   *
   * @return const LHCb::MCVertex*
   * @note Choosing an endVertex is not trivial and might depend on the physics case.
   * This implementation was presented in
   * https://indico.cern.ch/event/1118300/contributions/4734729/attachments/2389881.
   * It first tries to find the first vertex where the particle is actually destroyed.
   * If no such vertex is found, it checks for an hadronic IA and returns this vertex or
   * nullptr if nothing was found.
   */
  const MCVertex* MCParticle::goodEndVertex() const {
    const auto& v            = endVertices();
    const auto  destructiveV = std::find_if( v.begin(), v.end(), []( auto vtx ) {
      switch ( vtx->type() ) {
      case MCVertex::DecayVertex:
      case MCVertex::OscillatedAndDecay:
      case MCVertex::Annihilation:
      case MCVertex::PairProduction:
        return true;
      default:
        return false;
      }
    } );
    if ( destructiveV != v.end() ) {
      return destructiveV->data();
    } else {
      const auto hadronIAV =
          std::find_if( v.begin(), v.end(), []( auto vtx ) { return vtx->type() == MCVertex::HadronicInteraction; } );
      return hadronIAV != v.end() ? hadronIAV->data() : nullptr;
    }
  }

  /**
   * @brief Get position of a good endVertex for MCParticle
   *
   * @param p LHCb::MCParticle
   * @return LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>
   * @note Returns LinAlg::Vec<SIMDWrapper::scalar::float_v, 3> filled with NaN if no end-vertex
   * found. This function is used by ThOr functors via ADL.
   */
  LinAlg::Vec<SIMDWrapper::scalar::float_v, 3> endVertexPos( const MCParticle& p ) {
    const auto* goodVtx = p.goodEndVertex();
    if ( goodVtx ) {
      const auto pos = goodVtx->position();
      return LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{pos.x(), pos.y(), pos.z()};
    } else {
      constexpr auto nan = std::numeric_limits<float>::quiet_NaN();
      return LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{nan, nan, nan};
    }
  }

} // namespace LHCb

bool LHCb::MCParticle::hasOscillated() const {
  return std::any_of( endVertices().begin(), endVertices().end(),
                      []( const auto& v ) { return v->type() == LHCb::MCVertex::MCVertexType::OscillatedAndDecay; } );
}
