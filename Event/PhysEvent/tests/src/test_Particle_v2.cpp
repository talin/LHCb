/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestParticlev2
#undef NDEBUG
#include "Event/GenerateSOATracks.h"
#include "Event/SOACollection.h"
#include "Event/UniqueIDGenerator.h"

#include "Event/Particle_v2.h"
#include "GaudiKernel/SerializeSTL.h"
#include "LHCbMath/SIMDWrapper.h"

#include <boost/test/unit_test.hpp>

static_assert( LHCb::Event::is_zippable_v<LHCb::Event::Composites>,
               "v2 event model classes should be zippable with the v2 zip machinery" );

static const LHCb::UniqueIDGenerator unique_id_gen;

BOOST_AUTO_TEST_CASE( test_v2_particle_instantiation ) {
  LHCb::Event::Composites parts{unique_id_gen};
  BOOST_CHECK_EQUAL( parts.size(), 0 );
  BOOST_CHECK_EQUAL( parts.capacity(), 0 );
  BOOST_CHECK( parts.empty() );
  constexpr auto target_size = 10;
  parts.resize( target_size );
  BOOST_CHECK( !parts.empty() );
  BOOST_CHECK_EQUAL( parts.size(), target_size );
  auto const new_capacity = parts.capacity();
  BOOST_CHECK_GE( new_capacity, target_size );
  parts.clear();
  BOOST_CHECK( parts.empty() );
  BOOST_CHECK_EQUAL( parts.size(), 0 );
  BOOST_CHECK_EQUAL( parts.capacity(), new_capacity );
}

BOOST_AUTO_TEST_CASE( test_v2_particle ) {
  LHCb::Event::Composites threebody{unique_id_gen};
  using scalar_t                           = SIMDWrapper::scalar::types;
  using int_v                              = scalar_t::int_v;
  using float_v                            = scalar_t::float_v;
  constexpr auto                   ntracks = 10;
  LHCb::LinAlg::MatSym<float_v, 3> pos_cov{};
  LHCb::LinAlg::MatSym<float_v, 4> p4_cov{};
  LHCb::LinAlg::Mat<float_v, 4, 3> mom_pos_cov{};
  LHCb::LinAlg::Vec<float_v, 4>    p4{};
  LHCb::LinAlg::Vec<float_v, 3>    pos{};
  for ( auto i = 0; i < ntracks; ++i ) {
    std::array<int_v, 3>                            child_indices{i, i * 2, i * 3}, child_zip_ids{0, 1, 0};
    std::vector<LHCb::UniqueIDGenerator::ID<int_v>> descendant_unique_ids{
        unique_id_gen.generate<int_v>(), unique_id_gen.generate<int_v>(), unique_id_gen.generate<int_v>()};
    pos( 0 )            = i * 4;
    pos( 1 )            = i * 5;
    pos( 2 )            = i * 6;
    pos_cov( 0, 0 )     = i * 7;
    pos_cov( 0, 1 )     = i * 8;
    pos_cov( 0, 2 )     = i * 9;
    pos_cov( 1, 1 )     = i * 10;
    pos_cov( 1, 2 )     = i * 11;
    pos_cov( 2, 2 )     = i * 12;
    p4_cov( 0, 0 )      = i * 13;
    p4_cov( 0, 1 )      = i * 14;
    p4_cov( 0, 2 )      = i * 15;
    p4_cov( 0, 3 )      = i * 16;
    p4_cov( 1, 1 )      = i * 17;
    p4_cov( 1, 2 )      = i * 18;
    p4_cov( 1, 3 )      = i * 19;
    p4_cov( 2, 2 )      = i * 20;
    p4_cov( 2, 3 )      = i * 21;
    p4_cov( 3, 3 )      = i * 22;
    mom_pos_cov( 0, 0 ) = i * 23;
    mom_pos_cov( 0, 1 ) = i * 24;
    mom_pos_cov( 0, 2 ) = i * 25;
    mom_pos_cov( 1, 0 ) = i * 26;
    mom_pos_cov( 1, 1 ) = i * 27;
    mom_pos_cov( 1, 2 ) = i * 28;
    mom_pos_cov( 2, 0 ) = i * 29;
    mom_pos_cov( 2, 1 ) = i * 30;
    mom_pos_cov( 2, 2 ) = i * 31;
    mom_pos_cov( 3, 0 ) = i * 32;
    mom_pos_cov( 3, 1 ) = i * 33;
    mom_pos_cov( 3, 2 ) = i * 34;
    p4( 0 )             = i * 35;
    p4( 1 )             = i * 36;
    p4( 2 )             = i * 37;
    p4( 3 )             = i * 38;
    threebody.emplace_back<SIMDWrapper::InstructionSet::Scalar>(
        pos, p4, int_v{i * 39} /* pid */, float_v{i * 40} /* chi2 */, int_v{i * 41} /* ndof */, pos_cov, p4_cov,
        mom_pos_cov, child_indices, child_zip_ids, descendant_unique_ids );
  }
  BOOST_CHECK_EQUAL( threebody.size(), ntracks );
  auto const iterable = LHCb::Event::make_zip( threebody );
  auto       counter  = 0;
  for ( auto const& particle : threebody.simd() ) {
    // the generating loop was scalar, but 'particle' yields vectors. The
    // equivalent to i * 4 in the generating loop is particle.indices() * 4
    auto const inds = particle.indices();
    auto const mask = particle.loop_mask();
    // Accessors for container-level information
    BOOST_CHECK_EQUAL( particle.numChildren(), 3 );
    BOOST_CHECK( all( !mask || particle.x() == inds * 4 ) );
    BOOST_CHECK( all( !mask || particle.y() == inds * 5 ) );
    BOOST_CHECK( all( !mask || particle.z() == inds * 6 ) );
    BOOST_CHECK( all( !mask || particle.childRelationIndex( 0 ) == inds ) );
    BOOST_CHECK( all( !mask || particle.childRelationIndex( 1 ) == inds * 2 ) );
    BOOST_CHECK( all( !mask || particle.childRelationIndex( 2 ) == inds * 3 ) );
    BOOST_CHECK( all( !mask || particle.childRelationFamily( 0 ) == 0 ) );
    BOOST_CHECK( all( !mask || particle.childRelationFamily( 1 ) == 1 ) );
    BOOST_CHECK( all( !mask || particle.childRelationFamily( 2 ) == 0 ) );
    BOOST_CHECK( all( !mask || particle.posCovElement( 0, 0 ) == inds * 7 ) );
    BOOST_CHECK( all( !mask || particle.posCovElement( 0, 1 ) == inds * 8 ) );
    BOOST_CHECK( all( !mask || particle.posCovElement( 1, 0 ) == inds * 8 ) );
    BOOST_CHECK( all( !mask || particle.posCovElement( 0, 2 ) == inds * 9 ) );
    BOOST_CHECK( all( !mask || particle.posCovElement( 2, 0 ) == inds * 9 ) );
    BOOST_CHECK( all( !mask || particle.posCovElement( 1, 1 ) == inds * 10 ) );
    BOOST_CHECK( all( !mask || particle.posCovElement( 1, 2 ) == inds * 11 ) );
    BOOST_CHECK( all( !mask || particle.posCovElement( 2, 1 ) == inds * 11 ) );
    BOOST_CHECK( all( !mask || particle.posCovElement( 2, 2 ) == inds * 12 ) );
    auto const momCovMatrix = particle.momCovMatrix();
    using momCovMatrix_t    = std::decay_t<decltype( momCovMatrix )>;
    static_assert( momCovMatrix_t::n_rows == 4 );
    static_assert( momCovMatrix_t::n_cols == 4 );
    BOOST_CHECK( all( !mask || momCovMatrix( 0, 0 ) == inds * 13 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 0, 1 ) == inds * 14 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 1, 0 ) == inds * 14 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 0, 2 ) == inds * 15 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 2, 0 ) == inds * 15 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 0, 3 ) == inds * 16 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 3, 0 ) == inds * 16 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 1, 1 ) == inds * 17 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 1, 2 ) == inds * 18 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 2, 1 ) == inds * 18 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 1, 3 ) == inds * 19 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 3, 1 ) == inds * 19 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 2, 2 ) == inds * 20 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 2, 3 ) == inds * 21 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 3, 2 ) == inds * 21 ) );
    BOOST_CHECK( all( !mask || momCovMatrix( 3, 3 ) == inds * 22 ) );
    auto const threeMomCovMatrix = particle.threeMomCovMatrix();
    auto const threeMomCovMatrix_sub =
        momCovMatrix.sub<LHCb::LinAlg::MatSym<typename momCovMatrix_t::value_type, 3>, 0, 0>();
    BOOST_CHECK( all( !mask || threeMomCovMatrix_sub == threeMomCovMatrix ) );
    auto const momPosCovMatrix = particle.momPosCovMatrix();
    using momPosCovMatrix_t    = std::decay_t<decltype( momPosCovMatrix )>;
    static_assert( momPosCovMatrix_t::n_rows == 4 );
    static_assert( momPosCovMatrix_t::n_cols == 3 );
    BOOST_CHECK( all( !mask || momPosCovMatrix( 0, 0 ) == inds * 23 ) );
    BOOST_CHECK( all( !mask || momPosCovMatrix( 0, 1 ) == inds * 24 ) );
    BOOST_CHECK( all( !mask || momPosCovMatrix( 0, 2 ) == inds * 25 ) );
    BOOST_CHECK( all( !mask || momPosCovMatrix( 1, 0 ) == inds * 26 ) );
    BOOST_CHECK( all( !mask || momPosCovMatrix( 1, 1 ) == inds * 27 ) );
    BOOST_CHECK( all( !mask || momPosCovMatrix( 1, 2 ) == inds * 28 ) );
    BOOST_CHECK( all( !mask || momPosCovMatrix( 2, 0 ) == inds * 29 ) );
    BOOST_CHECK( all( !mask || momPosCovMatrix( 2, 1 ) == inds * 30 ) );
    BOOST_CHECK( all( !mask || momPosCovMatrix( 2, 2 ) == inds * 31 ) );
    BOOST_CHECK( all( !mask || momPosCovMatrix( 3, 0 ) == inds * 32 ) );
    BOOST_CHECK( all( !mask || momPosCovMatrix( 3, 1 ) == inds * 33 ) );
    BOOST_CHECK( all( !mask || momPosCovMatrix( 3, 2 ) == inds * 34 ) );
    auto const threeMomPosCovMatrix = particle.threeMomPosCovMatrix();
    auto const threeMomPosCovMatrix_sub =
        momPosCovMatrix.sub<LHCb::LinAlg::Mat<typename momPosCovMatrix_t::value_type, 3, 3>, 0, 0>();
    BOOST_CHECK( all( !mask || threeMomPosCovMatrix_sub == threeMomPosCovMatrix ) );
    BOOST_CHECK( all( !mask || particle.px() == inds * 35 ) );
    BOOST_CHECK( all( !mask || particle.py() == inds * 36 ) );
    BOOST_CHECK( all( !mask || particle.pz() == inds * 37 ) );
    BOOST_CHECK( all( !mask || particle.e() == inds * 38 ) );
    BOOST_CHECK( all( !mask || particle.pid() == inds * 39 ) );
    BOOST_CHECK( all( !mask || particle.chi2() == inds * 40 ) );
    BOOST_CHECK( all( !mask || particle.nDoF() == inds * 41 ) );
    counter += popcount( mask );
  }
  BOOST_CHECK( counter == ntracks );
  // Check the gather functionality
  using simd_t = SIMDWrapper::best::types;
  // Take even entries
  auto indices = simd_t::indices() * 2;
  // Make sure not to request any out-of-range entries
  auto const mask = indices < threebody.size();
  // Get a gathering proxy for these indices
  auto even_chunk = iterable.gather( indices, mask );
  BOOST_CHECK( all( !mask || even_chunk.x() == simd_t::float_v{indices * 4} ) );
}

BOOST_AUTO_TEST_CASE( test_charged_basics ) {

  using namespace LHCb::Event;
  using simd_t      = SIMDWrapper::type_map_t<SIMDWrapper::InstructionSet::Scalar>;
  using StatusMasks = LHCb::Event::v2::Muon::StatusMasks;

  auto zn = Zipping::generateZipIdentifier();

  auto n_elements = 100u;

  auto           tracks = v3::generate_tracks( n_elements, unique_id_gen, 1, zn );
  v2::Muon::PIDs muon_pids{zn};
  muon_pids.reserve( n_elements );
  LHCb::Event::ChargedBasics charged_basic_v2{&tracks, &muon_pids};
  charged_basic_v2.reserve( n_elements );
  for ( unsigned int i = 0; i < n_elements; ++i ) {
    auto part = charged_basic_v2.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    part.field<ChargedBasicsTag::RichPIDCode>().set( 0 );

    auto mu_pid = muon_pids.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    part.field<ChargedBasicsTag::MuonPID>().set( mu_pid.offset() );
    LHCb::Event::flags_v<simd_t, StatusMasks> flags;
    flags.set<StatusMasks::IsMuon>( false );
    mu_pid.field<v2::Muon::Tag::Status>().set( flags );
    mu_pid.field<v2::Muon::Tag::Chi2Corr>().set( std::numeric_limits<float>::lowest() );

    part.field<ChargedBasicsTag::Track>().set( i );

    part.field<ChargedBasicsTag::Mass>().set( 0 );
    part.field<ChargedBasicsTag::ParticleID>().set( 0 );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::p ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::e ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::pi ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::K ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::mu ).set( std::numeric_limits<float>::lowest() );
  }

  // access via ADL the four-momentum
  for ( auto const& proxy : charged_basic_v2.scalar() )
    BOOST_CHECK( all( fourMomentum( proxy ).x() == threeMomentum( proxy ).x() ) );
}
