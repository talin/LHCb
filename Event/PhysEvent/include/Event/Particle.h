/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/PrVeloTracks.h"
#include "Event/PrimaryVertices.h"
#include "Event/ProtoParticle.h"
#include "Event/VertexBase.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/NamedRange.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "GaudiKernel/SmartRef.h"
#include "GaudiKernel/SmartRefVector.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "GaudiKernel/VectorMap.h"
#include "Kernel/ParticleID.h"
#include "Kernel/Traits.h"
#include "LHCbMath/MatVec.h"
#include <algorithm>
#include <ostream>
#include <vector>

// Forward declarations
//

namespace LHCb {

  // Forward declarations
  class Vertex;

  // Class ID definition
  static const CLID CLID_Particle = 801;

  // Namespace for locations in TDS
  namespace ParticleLocation {
    inline const std::string User       = "Phys/User/Particles";
    inline const std::string Production = "Phys/Prod/Particles";
  } // namespace ParticleLocation

  /** @class Particle Particle.h
   *
   * Physics Analysis Particle. A chosen hypotesis for a possible candidate
   * particle.
   *
   * @author Patrick Koppenburg
   *
   */

  class Particle : public KeyedObject<int> {
  public:
    /// typedef for std::vector of Particle
    typedef std::vector<Particle*>       Vector;
    typedef std::vector<const Particle*> ConstVector;

    /// typedef for KeyedContainer of Particle
    typedef KeyedContainer<Particle, Containers::HashMap> Container;

    /// Vector of information for combined PIDs
    typedef GaudiUtils::VectorMap<int, double> ExtraInfo;
    /// The container type for shared particles (without ownership)
    typedef SharedObjectsContainer<LHCb::Particle> Selection;
    /// For uniform access to containers in TES (KeyedContainer,SharedContainer)
    typedef Gaudi::NamedRange_<ConstVector> Range;

    /// Additional information
    enum additionalInfo {
      Unknown   = -1,                     // Unknown/illegal value of the index for 'additional info'
      ConfLevel = 0,                      // Confidence Level for the particle
      Weight,                             // 'Weight' for the particle
      Chi2OfMassConstrainedFit      = 10, // Chi2 of Mass Constrained Fit
      Chi2OfDirectionConstrainedFit = 11, // Chi2 of Direction Constrained Fit
      Chi2OfVertexConstrainedFit    = 12, // Chi2 of Vertex  Constrained Fit
      Chi2OfParticleReFitter        = 15, // Chi2 From Particle ReFitter
      HasBremAdded                  = 16, // particle momentum is brem-corrected (0/1)
      NumVtxWithinChi2WindowOneTrack =
          7000, // Number of compatible vertices found within a given chi2 window when adding one track
      SmallestDeltaChi2OneTrack =
          NumVtxWithinChi2WindowOneTrack + 1, // Value of the smallest vertex DeltaChi2 with an extra track
      SmallestDeltaChi2MassOneTrack = NumVtxWithinChi2WindowOneTrack + 2, // Mass of the vertex giving the smallest
                                                                          // vertex DeltaChi2 with an extra track
      SmallestDeltaChi2TwoTracks =
          NumVtxWithinChi2WindowOneTrack + 3, // Value of the smallest vertex DeltaChi2 with two extra tracks
      SmallestDeltaChi2MassTwoTracks = NumVtxWithinChi2WindowOneTrack + 4, // Mass of the vertex giving the smallest
                                                                           // vertex DeltaChi2 with two extra tracks
      FlavourTaggingIPPUs    = 8000,              // IPs wrt PU vertices (for FlavourTagging internal use)
      FlavourTaggingTaggerID = 8001,              // ID of tagger if !=0 (for FlavourTagging internal use)
      Cone1Angle             = 9000,              // Cone angle, cone size 1
      Cone1Mult              = Cone1Angle + 1,    // Cone multiplicity, cone size 1
      Cone1PX                = Cone1Angle + 2,    // Cone Px, cone size 1
      Cone1PY                = Cone1Angle + 3,    // Cone Py, cone size 1
      Cone1PZ                = Cone1Angle + 4,    // Cone Pz, cone size 1
      Cone1P                 = Cone1Angle + 5,    // Cone P, cone size 1
      Cone1PT                = Cone1Angle + 6,    // Cone Pt, cone size 1
      Cone1PXAsym            = Cone1Angle + 7,    // Cone Px asymmetry, cone size 1
      Cone1PYAsym            = Cone1Angle + 8,    // Cone Py asymmetry, cone size 1
      Cone1PZAsym            = Cone1Angle + 9,    // Cone Pz asymmetry, cone size 1
      Cone1PAsym             = Cone1Angle + 10,   // Cone P asymmetry, cone size 1
      Cone1PTAsym            = Cone1Angle + 11,   // Cone Pt asymmetry, cone size 1
      Cone1DeltaEta          = Cone1Angle + 12,   // Cone DeltaEta, cone size 1
      Cone1DeltaPhi          = Cone1Angle + 13,   // Cone DeltaPhi, cone size 1
      Cone2Angle             = Cone1Angle + 14,   // Cone angle, cone size 2
      Cone2Mult              = Cone2Angle + 1,    // Cone multiplicity, cone size 2
      Cone2PX                = Cone2Angle + 2,    // Cone Px, cone size 2
      Cone2PY                = Cone2Angle + 3,    // Cone Py, cone size 2
      Cone2PZ                = Cone2Angle + 4,    // Cone Pz, cone size 2
      Cone2P                 = Cone2Angle + 5,    // Cone P, cone size 2
      Cone2PT                = Cone2Angle + 6,    // Cone Pt, cone size 2
      Cone2PXAsym            = Cone2Angle + 7,    // Cone Px asymmetry, cone size 2
      Cone2PYAsym            = Cone2Angle + 8,    // Cone Py asymmetry, cone size 2
      Cone2PZAsym            = Cone2Angle + 9,    // Cone Pz asymmetry, cone size 2
      Cone2PAsym             = Cone2Angle + 10,   // Cone P asymmetry, cone size 2
      Cone2PTAsym            = Cone2Angle + 11,   // Cone Pt asymmetry, cone size 2
      Cone2DeltaEta          = Cone2Angle + 12,   // Cone DeltaEta, cone size 2
      Cone2DeltaPhi          = Cone2Angle + 13,   // Cone DeltaPhi, cone size 2
      Cone3Angle             = Cone2Angle + 14,   // Cone angle, cone size 3
      Cone3Mult              = Cone3Angle + 1,    // Cone multiplicity, cone size 3
      Cone3PX                = Cone3Angle + 2,    // Cone Px, cone size 3
      Cone3PY                = Cone3Angle + 3,    // Cone Py, cone size 3
      Cone3PZ                = Cone3Angle + 4,    // Cone Pz, cone size 3
      Cone3P                 = Cone3Angle + 5,    // Cone P, cone size 3
      Cone3PT                = Cone3Angle + 6,    // Cone Pt, cone size 3
      Cone3PXAsym            = Cone3Angle + 7,    // Cone Px asymmetry, cone size 3
      Cone3PYAsym            = Cone3Angle + 8,    // Cone Py asymmetry, cone size 3
      Cone3PZAsym            = Cone3Angle + 9,    // Cone Pz asymmetry, cone size 3
      Cone3PAsym             = Cone3Angle + 10,   // Cone P asymmetry, cone size 3
      Cone3PTAsym            = Cone3Angle + 11,   // Cone Pt asymmetry, cone size 3
      Cone3DeltaEta          = Cone3Angle + 12,   // Cone DeltaEta, cone size 3
      Cone3DeltaPhi          = Cone3Angle + 13,   // Cone DeltaPhi, cone size 3
      Cone4Angle             = Cone3Angle + 14,   // Cone angle, cone size 4
      Cone4Mult              = Cone4Angle + 1,    // Cone multiplicity, cone size 4
      Cone4PX                = Cone4Angle + 2,    // Cone Px, cone size 4
      Cone4PY                = Cone4Angle + 3,    // Cone Py, cone size 4
      Cone4PZ                = Cone4Angle + 4,    // Cone Pz, cone size 4
      Cone4P                 = Cone4Angle + 5,    // Cone P, cone size 4
      Cone4PT                = Cone4Angle + 6,    // Cone Pt, cone size 4
      Cone4PXAsym            = Cone4Angle + 7,    // Cone Px asymmetry, cone size 4
      Cone4PYAsym            = Cone4Angle + 8,    // Cone Py asymmetry, cone size 4
      Cone4PZAsym            = Cone4Angle + 9,    // Cone Pz asymmetry, cone size 4
      Cone4PAsym             = Cone4Angle + 10,   // Cone P asymmetry, cone size 4
      Cone4PTAsym            = Cone4Angle + 11,   // Cone Pt asymmetry, cone size 4
      Cone4DeltaEta          = Cone4Angle + 12,   // Cone DeltaEta, cone size 4
      Cone4DeltaPhi          = Cone4Angle + 13,   // Cone DeltaPhi, cone size 4
      EWCone1Index           = Cone4Angle + 14,   // First index for EW cone variables, cone size 1
      EWCone2Index           = EWCone1Index + 23, // First index for EW cone variables, cone size 2
      EWCone3Index           = EWCone2Index + 23, // First index for EW cone variables, cone size 3
      EWCone4Index           = EWCone3Index + 23, // First index for EW cone variables, cone size 4
      FirstJetIndex          = 9500,              // The first index allocated for jet-related studies
      JetActiveArea,                              // Jet Active Area estimation
      JetActiveAreaError,                         // Uncertanty in Jet Active Area evaluation
      JetActiveAreaPx,                            // Px-component of Jet active area four-momentum
      JetActiveAreaPy,                            // Py-component of Jet active area four-momentum
      JetActiveAreaPz,                            // Pz-component of Jet active area four-momentum
      JetActiveAreaE,                             // E-component  of Jet active area four-momentum
      JetPtPerUnitArea,                           // Underlying event activity per unit of Active Area
      LastJetIndex = FirstJetIndex + 199,         // The last  index allocated for jet-related studies
      LastGlobal   = 10000 // The last 'global' index, other values are specific for user analysis
    };

    /// Copy constructor
    Particle( const Particle& part )
        : KeyedObject<int>()
        , m_particleID( part.m_particleID )
        , m_measuredMass( part.m_measuredMass )
        , m_measuredMassErr( part.m_measuredMassErr )
        , m_momentum( part.m_momentum )
        , m_referencePoint( part.m_referencePoint )
        , m_momCovMatrix( part.m_momCovMatrix )
        , m_posCovMatrix( part.m_posCovMatrix )
        , m_posMomCovMatrix( part.m_posMomCovMatrix )
        , m_extraInfo( part.m_extraInfo )
        , m_endVertex( part.m_endVertex )
        , m_proto( part.m_proto )
        , m_daughters( part.m_daughters ) {}

    /// Constructor from ParticleID
    explicit Particle( const LHCb::ParticleID& pid ) : m_particleID( pid ) {}

    /// Constructor from ParticleID and key
    Particle( const LHCb::ParticleID& pid, int key ) : KeyedObject<int>( key ), m_particleID( pid ) {}

    /// Default Constructor
    Particle() {}

    /// Default Destructor
    ~Particle() override = default;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::Particle::classID(); }
    static const CLID& classID() { return CLID_Particle; }

    /// conversion of string to enum for type additionalInfo
    static LHCb::Particle::additionalInfo additionalInfoToType( const std::string& aName ) {
      auto iter = s_additionalInfoTypMap().find( aName );
      return iter != s_additionalInfoTypMap().end() ? iter->second : Unknown;
    }

    /// conversion to string for enum type additionalInfo -- should be deprecated...
    static const std::string& additionalInfoToString( int aEnum ) {
      return toString( static_cast<additionalInfo>( aEnum ) );
    }

    friend const std::string& toString( LHCb::Particle::additionalInfo ai ) {
      using namespace std::string_literals;
      auto iter = std::find_if( s_additionalInfoTypMap().begin(), s_additionalInfoTypMap().end(),
                                [ai]( const auto& i ) { return i.second == ai; } );
      if ( iter == s_additionalInfoTypMap().end() )
        throw std::out_of_range( "ERROR wrong value " + std::to_string( static_cast<int>( ai ) ) +
                                 " for enum LHCb::Particle::additionalInfo" );
      return iter->first;
    }
    friend std::ostream& toStream( additionalInfo e, std::ostream& os ) {
      return os << std::quoted( toString( e ), '\'' );
    }
    friend std::ostream& operator<<( std::ostream& s, additionalInfo e ) { return toStream( e, s ); }
    friend StatusCode    parse( additionalInfo& bt, const std::string& in );

    /// Clone particle
    Particle* clone() const { return new Particle( *this ); }

    /// Assignment operator, note that the original vertex and protoParticle are kept
    Particle& operator=( const Particle& rhs );

    /// Confidence Level of the particleID. If not set the default is -1.
    double confLevel() const { return info( LHCb::Particle::additionalInfo::ConfLevel, -1. ); }

    /// set confidence Level of the particleID.
    void setConfLevel( double cl ) { addInfo( LHCb::Particle::additionalInfo::ConfLevel, cl ); }

    /// Weight of the particle, whatever that means. If not set the default is 1.
    double weight() const { return info( LHCb::Particle::additionalInfo::Weight, 1. ); }

    /// set Weight of the particle, whatever that means.
    void setWeight( double w ) { addInfo( LHCb::Particle::additionalInfo::Weight, w ); }

    /// Charge in units of +e (i.e. electron charge = -1)
    int charge() const { return particleID().threeCharge() / 3; }

    /// short cut for transerve momentum
    double pt() const { return m_momentum.Pt(); }

    /// short cut for |P|
    double p() const { return m_momentum.R(); }

    /// full error matrix on position, 4-momentum. @todo Check the maths of this
    Gaudi::SymMatrix7x7 covMatrix() const;

    /// a basic particle has no daughters
    bool isBasicParticle() const { return m_daughters.empty(); }

    /// Get daughters as a ConstVector
    Particle::ConstVector daughtersVector() const {
      return LHCb::Particle::ConstVector( m_daughters.begin(), m_daughters.end() );
    }

    /// has information for specified key
    bool hasInfo( int key ) const { return m_extraInfo.end() != m_extraInfo.find( key ); }

    ///  add new information, associated with the key
    bool addInfo( int key, double info ) { return m_extraInfo.insert( key, info ).second; }

    /// extract the information associated with the given key. If there is no such infomration the default value will be
    /// returned.
    double info( int key, double def ) const {
      auto i = m_extraInfo.find( key );
      return m_extraInfo.end() == i ? def : i->second;
    }

    /// erase the information associated with the given key
    unsigned long eraseInfo( int key ) { return m_extraInfo.erase( key ); }

    /// Returns slopes ( Px/Pz, Py/Pz, 1 ) at reference point
    Gaudi::XYZVector slopes() const {
      return Gaudi::XYZVector( fabs( m_momentum.Z() ) > 0 ? m_momentum.X() / m_momentum.Z() : 0,
                               fabs( m_momentum.Z() ) > 0 ? m_momentum.Y() / m_momentum.Z() : 0, 1.0 );
    }

    /// Print this Particle in a human readable way
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve const  PDG code
    const ParticleID& particleID() const { return m_particleID; }

    /// Update  PDG code
    Particle& setParticleID( const ParticleID& value ) {
      m_particleID = value;
      return *this;
    }

    /// Retrieve const  Measured Mass
    double measuredMass() const { return m_measuredMass; }

    /// Update  Measured Mass
    void setMeasuredMass( double value ) { m_measuredMass = value; }

    /// Retrieve const  Error on measured mass
    double measuredMassErr() const { return m_measuredMassErr; }

    /// Update  Error on measured mass
    void setMeasuredMassErr( double value ) { m_measuredMassErr = value; }

    /// Retrieve const  Momentum four vector
    const Gaudi::LorentzVector&            momentum() const { return m_momentum; }
    [[nodiscard, gnu::always_inline]] auto e() const { return momentum().e(); }
    [[nodiscard, gnu::always_inline]] auto px() const { return momentum().x(); }
    [[nodiscard, gnu::always_inline]] auto py() const { return momentum().y(); }
    [[nodiscard, gnu::always_inline]] auto pz() const { return momentum().z(); }

    /// Update  Momentum four vector
    void setMomentum( const Gaudi::LorentzVector& value ) { m_momentum = value; }

    /// Retrieve const  Point at which the momentum is given in LHCb reference frame - convention: take the one that
    /// minimizes the extrapolations needs
    const Gaudi::XYZPoint&                 referencePoint() const { return m_referencePoint; }
    [[nodiscard, gnu::always_inline]] auto x() const { return referencePoint().x(); }
    [[nodiscard, gnu::always_inline]] auto y() const { return referencePoint().y(); }
    [[nodiscard, gnu::always_inline]] auto z() const { return referencePoint().z(); }

    /// Update  Point at which the momentum is given in LHCb reference frame - convention: take the one that minimizes
    /// the extrapolations needs
    void setReferencePoint( const Gaudi::XYZPoint& value ) { m_referencePoint = value; }

    /// Retrieve const  Covariance matrix relative to momentum (4x4)
    const Gaudi::SymMatrix4x4& momCovMatrix() const { return m_momCovMatrix; }

    /// Update  Covariance matrix relative to momentum (4x4)
    void setMomCovMatrix( const Gaudi::SymMatrix4x4& value ) { m_momCovMatrix = value; }

    /// Retrieve const  Covariance matrix relative to point at which the momentum is given (3x3)
    const Gaudi::SymMatrix3x3& posCovMatrix() const { return m_posCovMatrix; }

    /// Update  Covariance matrix relative to point at which the momentum is given (3x3)
    void setPosCovMatrix( const Gaudi::SymMatrix3x3& value ) { m_posCovMatrix = value; }

    /// Retrieve const  Matrix with correlation errors between momemtum and pointOnTrack (momentum x point)
    const Gaudi::Matrix4x3& posMomCovMatrix() const { return m_posMomCovMatrix; }

    /// Update  Matrix with correlation errors between momemtum and pointOnTrack (momentum x point)
    void setPosMomCovMatrix( const Gaudi::Matrix4x3& value ) { m_posMomCovMatrix = value; }

    /// Retrieve const  Some addtional user information. Don't use directly. Use *Info() methods.
    const ExtraInfo& extraInfo() const { return m_extraInfo; }

    /// Retrieve (const)  Reference to end vertex (ex. decay)
    const LHCb::Vertex* endVertex() const { return m_endVertex; }

    /// Retrieve  Reference to end vertex (ex. decay)
    LHCb::Vertex* endVertex() { return m_endVertex; }

    /// Update  Reference to end vertex (ex. decay)
    void setEndVertex( const SmartRef<LHCb::Vertex>& value ) { m_endVertex = value; }

    /// Update (pointer)  Reference to end vertex (ex. decay)
    void setEndVertex( const LHCb::Vertex* value ) { m_endVertex = value; }

    /// Retrieve (const)  Reference to original ProtoParticle
    const LHCb::ProtoParticle* proto() const { return m_proto; }

    /// Update  Reference to original ProtoParticle
    void setProto( const SmartRef<LHCb::ProtoParticle>& value ) { m_proto = value; }

    /// Update (pointer)  Reference to original ProtoParticle
    void setProto( const LHCb::ProtoParticle* value ) { m_proto = value; }

    /// Retrieve (const)  Reference to daughter particles. Users are strongly discouraged to use setDaughters method.
    const SmartRefVector<LHCb::Particle>& daughters() const { return m_daughters; }

    /// Update  Reference to daughter particles. Users are strongly discouraged to use setDaughters method.
    void setDaughters( const SmartRefVector<LHCb::Particle>& value ) { m_daughters = value; }

    /// Add to  Reference to daughter particles. Users are strongly discouraged to use setDaughters method.
    void addToDaughters( const SmartRef<LHCb::Particle>& value ) { m_daughters.push_back( value ); }

    /// Att to (pointer)  Reference to daughter particles. Users are strongly discouraged to use setDaughters method.
    void addToDaughters( const LHCb::Particle* value ) { m_daughters.push_back( value ); }

    /// Remove from  Reference to daughter particles. Users are strongly discouraged to use setDaughters method.
    void removeFromDaughters( const SmartRef<LHCb::Particle>& value ) {
      auto i = std::remove( m_daughters.begin(), m_daughters.end(), value );
      m_daughters.erase( i, m_daughters.end() );
    }

    /// Clear  Reference to daughter particles. Users are strongly discouraged to use setDaughters method.
    void clearDaughters() { m_daughters.clear(); }

    friend std::ostream& operator<<( std::ostream& str, const Particle& obj ) { return obj.fillStream( str ); }

    // opt-in to ThOr functors

    [[nodiscard, gnu::always_inline]] friend auto referencePoint( const Particle& p ) {
      auto const& pos = p.referencePoint();
      return LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{pos.x(), pos.y(), pos.z()};
    }

    [[nodiscard, gnu::always_inline]] friend auto threeMomentum( const Particle& p ) {
      auto const& mom = p.momentum();
      return LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{mom.px(), mom.py(), mom.pz()};
    }
    [[nodiscard, gnu::always_inline]] friend auto slopes( const Particle& p ) {
      auto mom = threeMomentum( p );
      return mom / Z( mom );
    }

    [[nodiscard, gnu::always_inline]] friend auto fourMomentum( const Particle& p ) {
      auto const& mom = p.momentum();
      return LinAlg::Vec<SIMDWrapper::scalar::float_v, 4>{mom.px(), mom.py(), mom.pz(), mom.e()};
    }
    friend LinAlg::Vec<SIMDWrapper::scalar::float_v, 3> endVertexPos( const Particle& p );

    [[nodiscard, gnu::always_inline]] friend auto threeMomCovMatrix( const Particle& p ) {
      return LinAlg::convert<SIMDWrapper::scalar::float_v>(
          p.momCovMatrix().template Sub<Gaudi::SymMatrix3x3>( 0, 0 ) );
    }

    [[nodiscard, gnu::always_inline]] friend auto momCovMatrix( const Particle& p ) {
      return LinAlg::convert<SIMDWrapper::scalar::float_v>( p.momCovMatrix() );
    }

    [[nodiscard, gnu::always_inline]] friend auto threeMomPosCovMatrix( const Particle& p ) {
      return LinAlg::convert<SIMDWrapper::scalar::float_v>(
          p.posMomCovMatrix().template Sub<Gaudi::Matrix3x3>( 0, 0 ) );
    }

    [[nodiscard, gnu::always_inline]] friend auto momPosCovMatrix( const Particle& p ) {
      return LinAlg::convert<SIMDWrapper::scalar::float_v>( p.posMomCovMatrix() );
    }

    [[nodiscard, gnu::always_inline]] friend auto posCovMatrix( const Particle& p ) {
      return LinAlg::convert<SIMDWrapper::scalar::float_v>( p.posCovMatrix() );
    }

    [[nodiscard, gnu::always_inline]] friend auto mass2( const Particle& p ) { return p.momentum().mass2(); }

    friend auto pid( const Particle& p ) { return p.particleID().pid(); }

    // flag which indicates client code can go for 'threeMomCovMatrix', `momPosCovMatrix` and `posCovMatrix` and not
    // for a track-like stateCov -- possible values: yes (for track-like objects ) no (for neutrals, composites), maybe
    // (particle, check at runtime, calls may return invalid results)
    static constexpr auto canBeExtrapolatedDownstream = Event::CanBeExtrapolatedDownstream::maybe;

  private:
    LHCb::ParticleID     m_particleID;                                                ///< PDG code
    double               m_measuredMass    = -1;                                      ///< Measured Mass
    double               m_measuredMassErr = 0;                                       ///< Error on measured mass
    Gaudi::LorentzVector m_momentum        = {0.0, 0.0, 0.0, -1 * Gaudi::Units::GeV}; ///< Momentum four vector
    Gaudi::XYZPoint      m_referencePoint  = {
        0.0, 0.0, -100 * Gaudi::Units::m}; ///< Point at which the momentum is given in LHCb reference frame -
                                                 ///< convention: take the one that minimizes the extrapolations needs
    Gaudi::SymMatrix4x4 m_momCovMatrix;          ///< Covariance matrix relative to momentum (4x4)
    Gaudi::SymMatrix3x3 m_posCovMatrix; ///< Covariance matrix relative to point at which the momentum is given (3x3)
    Gaudi::Matrix4x3 m_posMomCovMatrix; ///< Matrix with correlation errors between momemtum and pointOnTrack (momentum
                                        ///< x point)
    ExtraInfo              m_extraInfo; ///< Some addtional user information. Don't use directly. Use *Info() methods.
    SmartRef<LHCb::Vertex> m_endVertex; ///< Reference to end vertex (ex. decay)
    SmartRef<LHCb::ProtoParticle>  m_proto;     ///< Reference to original ProtoParticle
    SmartRefVector<LHCb::Particle> m_daughters; ///< Reference to daughter particles. Users are strongly discouraged to
                                                ///< use setDaughters method.

    static const GaudiUtils::VectorMap<std::string, additionalInfo>& s_additionalInfoTypMap();

  }; // class Particle

  /// Definition of Keyed Container for Particle
  typedef KeyedContainer<Particle, Containers::HashMap> Particles;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations
#include "Event/ParticleCombination.h"
#include "Event/Vertex.h"

namespace LHCb {

  [[nodiscard, gnu::always_inline]] inline LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>
  endVertexPos( const LHCb::Particle& p ) {
    if ( !p.endVertex() ) throw std::runtime_error{"Particle instance has no endVertex"};
    assert( p.endVertex() );
    const auto& pos = p.endVertex()->position();
    return {pos.x(), pos.y(), pos.z()};
  }

  inline auto decayProducts( const LHCb::Particle& p ) {
    auto const& d = p.daughtersVector();
    switch ( d.size() ) {
    case 2:
      return ParticleCombination<LHCb::Particle>{d[0], d[1]};
    case 3:
      return ParticleCombination<LHCb::Particle>{d[0], d[1], d[2]};
    case 4:
      return ParticleCombination<LHCb::Particle>{d[0], d[1], d[2], d[3]};
    case 5:
      return ParticleCombination<LHCb::Particle>{d[0], d[1], d[2], d[3], d[4]};
    case 6:
      return ParticleCombination<LHCb::Particle>{d[0], d[1], d[2], d[3], d[4], d[5]};
    case 7:
      return ParticleCombination<LHCb::Particle>{d[0], d[1], d[2], d[3], d[4], d[5], d[6]};
    case 8:
      return ParticleCombination<LHCb::Particle>{d[0], d[1], d[2], d[3], d[4], d[5], d[6], d[7]};
    default:
      throw GaudiException{"Combination of less than 2, or more than 8 -- this should never happen",
                           "decayProducts(LHCb::Particle const&)", StatusCode::FAILURE};
      __builtin_unreachable();
    }
  }

  inline Particle& Particle::operator=( const Particle& rhs ) {
    if ( this != &rhs ) {
      m_particleID      = rhs.m_particleID;
      m_momentum        = rhs.m_momentum;
      m_referencePoint  = rhs.m_referencePoint;
      m_measuredMass    = rhs.m_measuredMass;
      m_measuredMassErr = rhs.m_measuredMassErr;
      m_posCovMatrix    = rhs.m_posCovMatrix;
      m_momCovMatrix    = rhs.m_momCovMatrix;
      m_posMomCovMatrix = rhs.m_posMomCovMatrix;
      m_extraInfo       = rhs.m_extraInfo;
      m_endVertex       = rhs.m_endVertex;
      m_proto           = rhs.m_proto;
      m_daughters       = rhs.m_daughters;
    }
    return *this;
  }

  inline const LHCb::Event::PV::PrimaryVertex* bestPV( const LHCb::Event::PV::PrimaryVertexContainer& allPVs,
                                                       const Particle&                                particle ) {
    if ( particle.isBasicParticle() )
      return LHCb::Event::PV::bestPV( allPVs, particle.referencePoint(), particle.momentum() );
    return LHCb::Event::PV::bestPV( allPVs, particle.endVertex()->position(), particle.momentum() );
  }

  inline LHCb::Event::PV::PrimaryVertices unbiasedPVs( const LHCb::Event::PV::PrimaryVertexContainer& PVs,
                                                       const LHCb::Particle&                          particle ) {
    LHCb::Event::PV::PrimaryVertices unbiasedPVs;
    std::vector<int>                 VeloIdxToRemove;
    VeloIdxToRemove.reserve( 10 );

    std::vector<const LHCb::Track*> signal_daughter_tracks;
    signal_daughter_tracks.reserve( 10 );
    LHCb::Particle::ConstVector all_daughters = particle.daughtersVector();

    while ( all_daughters.size() > 0 ) {
      LHCb::Particle::ConstVector new_daughters;
      for ( const auto& daughter : all_daughters ) {
        if ( daughter->isBasicParticle() ) {
          const LHCb::ProtoParticle* daughter_proto = daughter->proto();
          if ( daughter_proto ) {
            const LHCb::Track* daughter_track = daughter_proto->track();
            if ( daughter_track ) signal_daughter_tracks.emplace_back( daughter_track );
          }
        } else {
          LHCb::Particle::ConstVector more_daughters = daughter->daughtersVector();
          new_daughters.insert( new_daughters.end(), more_daughters.begin(), more_daughters.end() );
        }
      }
      all_daughters = new_daughters;
    }

    auto pv_velotracks = PVs.tracks.scalar();

    for ( const auto daughter_track : signal_daughter_tracks ) {

      const auto& daughter_allIDs = daughter_track->lhcbIDs();
      const auto  endvelo_pos =
          std::find_if( daughter_allIDs.begin(), daughter_allIDs.end(), []( auto id ) { return !( id.isVP() ); } );

      std::vector<LHCb::LHCbID> daughter_veloIDs;
      daughter_veloIDs.reserve( endvelo_pos - daughter_allIDs.begin() );
      for ( auto id = daughter_allIDs.begin(); id != endvelo_pos; ++id ) daughter_veloIDs.emplace_back( *id );

      for ( const auto& pv_velotrack : pv_velotracks ) {
        auto vtrack = pv_velotrack.veloTrack();
        if ( daughter_veloIDs == vtrack.lhcbIDs() ) {
          VeloIdxToRemove.emplace_back( pv_velotrack.offset() );
          break;
        }
      }
    }

    auto newvertices = LHCb::Event::PV::unbiasedVertices( PVs, VeloIdxToRemove );
    unbiasedPVs.insert( unbiasedPVs.end(), newvertices.begin(), newvertices.end() );

    return unbiasedPVs;
  }

} // namespace LHCb

namespace Relations {
  template <typename>
  struct ObjectTypeTraits;

  template <>
  struct ObjectTypeTraits<LHCb::Particle> {
    // ========================================================================
    using Type   = LHCb::Particle;
    using Input  = const LHCb::Particle*;
    using Output = const LHCb::Particle*;
    using Inner  = SmartRef<LHCb::Particle>;
    struct Less {
      bool operator()( const LHCb::Particle& lhs, const LHCb::Particle& rhs ) const {
        // relations should only be between objects in the event store, hence there _should_ be a
        // valid `parent()`, and that `parent()` should have a non-nullptr `registry()`.
        return lhs.parent() == rhs.parent()
                   ? ( lhs.key() < rhs.key() )
                   : ( lhs.parent()->registry()->identifier() < rhs.parent()->registry()->identifier() );
      }
      bool operator()( const LHCb::Particle* lhs, const LHCb::Particle* rhs ) const {
        // there should not be any nullptrs in the 'from' side of a relation table...
        return ( *this )( *lhs, *rhs );
      }
    };
    template <typename T = void>
    using Equal = std::equal_to<T>;
    // ========================================================================
  };
} // namespace Relations
