/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/CaloHypos_v2.h"
#include "Event/MuonPIDs_v2.h"
#include "Event/SOACollection.h"
#include "Event/Track_v3.h"
#include "Event/UniqueIDGenerator.h"
#include "Kernel/ParticleID.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/Traits.h"
#include "Kernel/Variant.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"
#include <array>
#include <limits>
#include <tuple>
#include <type_traits>
#include <vector>

namespace LHCb::Event {
  namespace ChargedBasicsTag {
    struct Track : index_field<v3::Tracks const> {};
    struct RichPIDCode : int_field {};
    struct MuonPID : index_field<v2::Muon::PIDs const> {};
    struct Mass : float_field {};
    struct ParticleID : int_field {};
    enum struct Hypo { e, mu, pi, K, p };
    struct CombDLL : floats_field<5> {};

    template <typename T>
    using ChargedBasics_t = SOACollection<T, Track, RichPIDCode, MuonPID, Mass, ParticleID, CombDLL>;
  } // namespace ChargedBasicsTag

  struct ChargedBasics : ChargedBasicsTag::ChargedBasics_t<ChargedBasics> {
    using base_t = typename ChargedBasicsTag::ChargedBasics_t<ChargedBasics>;

    using base_t::allocator_type;
    using base_t::base_t;

    ChargedBasics( v3::Tracks const* tracks, v2::Muon::PIDs const* mupids,
                   Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(),
                   allocator_type           alloc         = {} )
        : base_t{std::move( zipIdentifier ), std::move( alloc )}, m_tracks( tracks ), m_mupids( mupids ) {}

    ChargedBasics( ChargedBasics&& old ) noexcept
        : base_t{std::move( old )}, m_tracks( old.m_tracks ), m_mupids( old.m_mupids ) {}

    // Special constructor for zipping machinery
    ChargedBasics( Zipping::ZipFamilyNumber zipIdentifier, ChargedBasics const& other )
        : base_t{std::move( zipIdentifier ), other}, m_tracks{other.m_tracks}, m_mupids{other.m_mupids} {}

    template <typename Tag>
    decltype( auto ) container() const {
      if constexpr ( std::is_same_v<Tag, ChargedBasicsTag::Track> ) { return m_tracks; }
      if constexpr ( std::is_same_v<Tag, ChargedBasicsTag::MuonPID> ) { return m_mupids; }
    }

    auto const& tracks() const { return *m_tracks; }
    auto const& muon_pids() const { return *m_mupids; }

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct ChargedBasicsProxy : Event::Proxy<simd, behaviour, ContainerType> {
      using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
      using base_t::loop_mask;
      using base_t::Proxy;
      using base_t::width;

      static constexpr auto canBeExtrapolatedDownstream = Event::CanBeExtrapolatedDownstream::yes;
      static constexpr auto isBasicParticle             = Event::IsBasicParticle::yes;
      static constexpr auto hasTrack                    = Event::HasTrack::yes;

      [[nodiscard, gnu::always_inline]] auto trackV3() const {
        // Not named track() to avoid being falsely detected as an old Particle/Protoparticle
        // TODO: improve detection
        return this->template get<ChargedBasicsTag::Track>();
      }

      [[nodiscard, gnu::always_inline]] auto muonpid() const { return this->template get<ChargedBasicsTag::MuonPID>(); }

      [[nodiscard, gnu::always_inline]] auto pid_code() const {
        return this->template get<ChargedBasicsTag::RichPIDCode>();
      }
      [[nodiscard, gnu::always_inline]] auto mass() const { return this->template get<ChargedBasicsTag::Mass>(); }
      [[nodiscard, gnu::always_inline]] auto pid() const { return this->template get<ChargedBasicsTag::ParticleID>(); }
      [[nodiscard, gnu::always_inline]] auto CombDLLp() const {
        return this->template get<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::p );
      }
      [[nodiscard, gnu::always_inline]] auto CombDLLe() const {
        return this->template get<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::e );
      }
      [[nodiscard, gnu::always_inline]] auto CombDLLpi() const {
        return this->template get<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::pi );
      }
      [[nodiscard, gnu::always_inline]] auto CombDLLk() const {
        return this->template get<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::K );
      }
      [[nodiscard, gnu::always_inline]] auto CombDLLmu() const {
        return this->template get<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::mu );
      }

      // Forward track proxy -- TODO: adapt code to do explicit access
      using StateLocation = typename v3::detail::StateLocation;
      [[nodiscard]] auto                     backward() const { return this->trackV3().backward(); }
      [[nodiscard, gnu::always_inline]] auto trackVP() const { return this->trackV3().trackVP(); }
      [[nodiscard, gnu::always_inline]] auto trackUT() const { return this->trackV3().trackUT(); }
      [[nodiscard, gnu::always_inline]] auto trackSeed() const { return this->trackV3().trackSeed(); }
      [[nodiscard, gnu::always_inline]] auto unique_id() const { return this->trackV3().unique_id(); }

      [[nodiscard, gnu::always_inline]] auto unique_id_gen_tag() const { return this->trackV3().unique_id_gen_tag(); }
      [[nodiscard, gnu::always_inline]] auto vp_lhcbID( std::size_t i ) const { return this->trackV3().vp_lhcbID( i ); }
      [[nodiscard, gnu::always_inline]] auto ut_lhcbID( std::size_t i ) const { return this->trackV3().ut_lhcbID( i ); }
      [[nodiscard, gnu::always_inline]] auto ft_lhcbID( std::size_t i ) const { return this->trackV3().ft_lhcbID( i ); }
      [[nodiscard, gnu::always_inline]] auto nVPHits() const { return this->trackV3().nVPHits(); }
      [[nodiscard, gnu::always_inline]] auto nUTHits() const { return this->trackV3().nUTHits(); }
      [[nodiscard, gnu::always_inline]] auto nFTHits() const { return this->trackV3().nFTHits(); }
      [[nodiscard, gnu::always_inline]] auto nHits() const { return this->trackV3().nHits(); }

      [[nodiscard, gnu::always_inline]] auto history() const { return this->trackV3().history(); }

      [[nodiscard, gnu::always_inline]] auto chi2() const { return this->trackV3().chi2(); }
      [[nodiscard, gnu::always_inline]] auto nDoF() const { return this->trackV3().nDoF(); }
      [[nodiscard, gnu::always_inline]] auto chi2PerDoF() const { return this->trackV3().chi2PerDoF(); }

      [[nodiscard, gnu::always_inline]] auto has_state( StateLocation StateLoc ) const {
        return this->trackV3().has_state( StateLoc );
      }
      [[nodiscard, gnu::always_inline]] StateLocation defaultState() const { return this->trackV3().defaultState(); }
      [[nodiscard, gnu::always_inline]] auto          qOverP( StateLocation StateLoc ) const {
        return this->trackV3().qOverP( StateLoc );
      }
      [[nodiscard, gnu::always_inline]] auto p( StateLocation StateLoc ) const { return this->trackV3().p( StateLoc ); }
      [[nodiscard, gnu::always_inline]] auto pt( StateLocation StateLoc ) const {
        return this->trackV3().pt( StateLoc );
      }
      [[nodiscard, gnu::always_inline]] auto charge() const { return this->trackV3().charge(); }
      [[nodiscard]] auto covariance( StateLocation StateLoc ) const { return this->trackV3().covariance( StateLoc ); }
      [[nodiscard, gnu::always_inline]] auto position( StateLocation StateLoc ) const {
        return this->trackV3().position( StateLoc );
      }
      [[nodiscard, gnu::always_inline]] auto direction( StateLocation StateLoc ) const {
        return this->trackV3().direction( StateLoc );
      }
      [[nodiscard, gnu::always_inline]] auto state( StateLocation StateLoc ) const {
        return this->trackV3().state( StateLoc );
      }
      [[nodiscard, gnu::always_inline]] auto positionDirection( StateLocation StateLoc ) const {
        return this->trackV3().positionDirection( StateLoc );
      }
      [[nodiscard, gnu::always_inline]] auto pseudoRapidity( StateLocation StateLoc ) const {
        return this->trackV3().pseudoRapidity( StateLoc );
      }
      [[nodiscard, gnu::always_inline]] auto phi( StateLocation StateLoc ) const {
        return this->trackV3().phi( StateLoc );
      }
      [[nodiscard, gnu::always_inline]] auto threeMomentum( StateLocation StateLoc ) const {
        return this->trackV3().threeMomentum( StateLoc );
      }
      [[nodiscard, gnu::always_inline]] friend auto threeMomentum( ChargedBasicsProxy const& fp ) {
        return fp.threeMomentum( fp.defaultState() );
      }
      [[nodiscard, gnu::always_inline]] friend auto fourMomentum( ChargedBasicsProxy const& proxy ) {
        auto p3 = proxy.threeMomentum( proxy.defaultState() );
        ;
        auto px   = p3.x();
        auto py   = p3.y();
        auto pz   = p3.z();
        auto mass = proxy.mass();
        return LinAlg::Vec{px, py, pz, sqrt( px * px + py * py + pz * pz + mass * mass )};
      }
      [[nodiscard, gnu::always_inline]] friend auto slopes( ChargedBasicsProxy const& fp ) {
        return fp.direction( fp.defaultState() );
      }
      [[nodiscard, gnu::always_inline]] friend auto referencePoint( ChargedBasicsProxy const& fp ) {
        return fp.position( fp.defaultState() );
      }
      [[nodiscard, gnu::always_inline]] friend auto trackState( ChargedBasicsProxy const& fp ) {
        return fp.state( StateLocation::ClosestToBeam );
      }
      [[nodiscard, gnu::always_inline]] auto type() const { return this->trackV3().type(); }
      [[nodiscard]] std::vector<LHCbID>      lhcbIDs() const { return this->trackV3().lhcbIDs(); }

      // Forward muon proxy -- TODO: adapt code to do explicit access
      [[nodiscard]] auto IsMuon() const { return this->muonpid().IsMuon(); }
      [[nodiscard]] auto IsMuonTight() const { return this->muonpid().IsMuonTight(); }
      [[nodiscard]] auto InAcceptance() const { return this->muonpid().InAcceptance(); }
      [[nodiscard]] auto PreSelMomentum() const { return this->muonpid().PreSelMomentum(); }
      [[nodiscard]] auto Chi2Corr() const { return this->muonpid().Chi2Corr(); }
      [[nodiscard]] auto CatBoost() const { return this->muonpid().CatBoost(); }
      [[nodiscard]] auto LLMu() const { return this->muonpid().LLMu(); }
      [[nodiscard]] auto LLBg() const { return this->muonpid().LLBg(); }
      [[nodiscard]] auto nTileIDs() const { return this->muonpid().nTileIDs(); }
      [[nodiscard]] auto tileID( std::size_t i ) const { return this->muonpid().tileID( i ); }
      //[[nodiscard]] std::vector<LHCb::LHCbID> lhcbIDs() const { return this->muonpid().lhcbIDs(); }} // clashes with
      // track proxy
    };

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = ChargedBasicsProxy<simd, behaviour, ContainerType>;

    v3::Tracks const*     m_tracks;
    v2::Muon::PIDs const* m_mupids;
  };

  namespace NeutralBasicsTag {
    struct UniqueID : Event::int_field {};
    struct Direction : Event::Vec_field<3> {};
    struct Hypothesis : Event::index_field<Calo::v2::Hypotheses> {};

    template <typename T>
    using NeutralBasics_t = SOACollection<T, UniqueID, Direction, Hypothesis>;
  } // namespace NeutralBasicsTag

  struct NeutralBasics : NeutralBasicsTag::NeutralBasics_t<NeutralBasics> {
    using base_t = typename NeutralBasicsTag::NeutralBasics_t<NeutralBasics>;

    using base_t::allocator_type;
    using base_t::base_t;

    NeutralBasics( Calo::v2::Hypotheses const* hypos, LHCb::UniqueIDGenerator const& unique_id_gen,
                   Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(),
                   allocator_type           alloc         = {} )
        : base_t{std::move( zipIdentifier ), std::move( alloc )}
        , m_hypos( hypos )
        , m_unique_id_gen_tag{unique_id_gen.tag()} {}

    NeutralBasics( NeutralBasics&& old ) noexcept
        : base_t{std::move( old )}, m_hypos( old.m_hypos ), m_unique_id_gen_tag{old.m_unique_id_gen_tag} {}

    // Special constructor for zipping machinery
    NeutralBasics( Zipping::ZipFamilyNumber zipIdentifier, NeutralBasics const& other )
        : base_t{std::move( zipIdentifier ), other}
        , m_hypos{other.m_hypos}
        , m_unique_id_gen_tag{other.m_unique_id_gen_tag} {}

    template <typename Tag>
    decltype( auto ) container() const {
      if constexpr ( std::is_same_v<Tag, NeutralBasicsTag::Hypothesis> ) { return m_hypos; }
    }

    auto const& hypotheses() const { return *m_hypos; }

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct NeutralBasicsProxy : Event::Proxy<simd, behaviour, ContainerType> {
      using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
      using base_t::loop_mask;
      using base_t::Proxy;
      using base_t::width;
      using simd_t  = SIMDWrapper::type_map_t<simd>;
      using int_v   = typename simd_t::int_v;
      using float_v = typename simd_t::float_v;

      static constexpr auto canBeExtrapolatedDownstream = CanBeExtrapolatedDownstream::no;
      static constexpr auto isBasicParticle             = Event::IsBasicParticle::yes;
      static constexpr auto hasTrack                    = Event::HasTrack::no;

      [[nodiscard, gnu::always_inline]] auto direction() const {
        return this->template get<NeutralBasicsTag::Direction>();
      }
      [[nodiscard, gnu::always_inline]] auto hypothesis() const {
        return this->template get<NeutralBasicsTag::Hypothesis>();
      }
      [[nodiscard, gnu::always_inline]] auto energy() const { return hypothesis().energy(); }
      [[nodiscard, gnu::always_inline]] auto position() const {
        // return hypothesis().position(); // in scalar, returns Root vector for backward compatibility, but not
        // compatible with soa
        return hypothesis().clusters()[0].template get<LHCb::Event::Calo::v2::ClusterTag::Position>();
      }
      [[nodiscard, gnu::always_inline]] auto mass() const { return float_v{0}; } // TODO: Fix for pi0

      [[nodiscard, gnu::always_inline]] auto p() const {
        const auto e = energy();
        const auto m = mass();
        return sqrt( e * e - m * m );
      }
      [[nodiscard, gnu::always_inline]] auto pt() const {
        const auto d = direction();
        return p() * sqrt( d.perp2() / d.mag2() );
      }
      [[nodiscard, gnu::always_inline]] auto threeMomentum() const {
        const auto d = direction();
        return d * ( p() / d.mag() );
      }
      [[nodiscard, gnu::always_inline]] auto px() const { return threeMomentum().x(); }
      [[nodiscard, gnu::always_inline]] auto py() const { return threeMomentum().y(); }
      [[nodiscard, gnu::always_inline]] auto pz() const { return threeMomentum().z(); }
      [[nodiscard, gnu::always_inline]] auto phi() const { return direction().phi(); }
      [[nodiscard, gnu::always_inline]] auto eta() const { return direction().eta(); }

      [[nodiscard, gnu::always_inline]] auto pid() const {
        const auto hypo     = hypothesis().hypothesis();
        const auto isPhoton = hypo == LHCb::Event::Calo::v2::Hypotheses::Type::Photon ||
                              hypo == LHCb::Event::Calo::v2::Hypotheses::Type::BremmstrahlungPhoton ||
                              hypo == LHCb::Event::Calo::v2::Hypotheses::Type::PhotonFromMergedPi0;
        return select( isPhoton, int_v{22}, int_v{111} );
      }

      [[nodiscard, gnu::always_inline]] auto        endVertex() const { return position(); }
      [[nodiscard, gnu::always_inline]] friend auto threeMomentum( NeutralBasicsProxy const& proxy ) {
        return proxy.threeMomentum();
      }
      [[nodiscard, gnu::always_inline]] friend auto slopes( NeutralBasicsProxy const& fp ) { return fp.direction(); }
      [[nodiscard, gnu::always_inline]] friend auto referencePoint( NeutralBasicsProxy const& fp ) {
        return fp.position();
      }

      [[nodiscard, gnu::always_inline]] auto unique_id() const {
        return LHCb::UniqueIDGenerator::ID<int_v>{this->template get<NeutralBasicsTag::UniqueID>(),
                                                  unique_id_gen_tag()};
      }

      [[nodiscard, gnu::always_inline]] auto unique_id_gen_tag() const {
        return this->container()->unique_id_gen_tag();
      }
    };

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = NeutralBasicsProxy<simd, behaviour, ContainerType>;

    auto const& unique_id_gen_tag() const { return m_unique_id_gen_tag; }

  private:
    Calo::v2::Hypotheses const* m_hypos;
    boost::uuids::uuid          m_unique_id_gen_tag;
  };

  struct Composites;

  namespace CompositeTags {
    // Tag types
    struct PID : Event::int_field {};
    struct nDoF : Event::int_field {};
    struct Chi2 : Event::float_field {};
    // Child relations -- two int columns per child.
    struct ChildIndices
        : Event::vector_field<Event::index2D_field<
              std::tuple<ChargedBasics const, NeutralBasics const, Composites const>, std::index_sequence<6, 24>>> {};
    struct ChildRels : public Event::vector_field<Event::ints_field<2>> {
      enum { IndexOffset = 0, ZipFamilyOffset, NumColumns };
    };
    /** Descendant unique IDs -- one int column per descendant.
     *
     * By descendant we mean any decay product (including intermediate
     * states). e.g. For Bs0 -> J/psi(-> mu+ mu-) K*0(892)(-> K+ pi-) the
     * J/psi has as descendants (mu+, mu-) and the Bs0 has (J/psi, mu+, mu-,
     * K*0(892), K+, pi-).
     *
     * Unique IDs must be created using LHCb::UniqueIDGenerator (unless for test
     * and development scripts).
     */
    struct DescendantUniqueIDs : Event::vector_field<Event::int_field> {};
    // Position vector (3D)
    struct Position : Event::Vec_field<3> {};
    // Position covariance (3D symmetric)
    struct PositionCov : Event::MatSym_field<3> {};
    // Four-momentum (4D...)
    struct FourMom : Event::Vec_field<4> {};
    // Four-momentum covariance (4D symmetric)
    struct FourMomCov : Event::MatSym_field<4> {};
    // Position-four-momentum covariance (4x3)
    struct PositionFourMomCov : Event::Mat_field<4, 3> {};

    template <typename T>
    using base_t = Event::SOACollection<T, PID, nDoF, Chi2, Position, PositionCov, FourMom, FourMomCov,
                                        PositionFourMomCov, ChildIndices, ChildRels, DescendantUniqueIDs>;
  } // namespace CompositeTags

  struct Composites : CompositeTags::base_t<Composites> {
    using base_t = typename CompositeTags::base_t<Composites>;
    using base_t::allocator_type;
    using base_t::emplace_back;

    std::tuple<std::vector<ChargedBasics const*>, std::vector<NeutralBasics const*>, std::vector<Composites const*>>
        m_containers;

    Composites( UniqueIDGenerator const& unique_id_gen,
                /*std::vector<ChargedBasics const*> charged_children_container   = {},
                std::vector<NeutralBasics const*> neutral_children_container = {},
                std::vector<Composites const*>    composite_children_container = {},*/
                Zipping::ZipFamilyNumber zip_identifier = Zipping::generateZipIdentifier(), allocator_type alloc = {} )
        : base_t{std::move( zip_identifier ), std::move( alloc )}, m_unique_id_gen_tag{unique_id_gen.tag()} {}
    // Constructor used by zipping machinery when making a copy of a zip
    Composites( Zipping::ZipFamilyNumber zn, Composites const& old )
        : base_t{std::move( zn ), old}, m_containers{old.m_containers}, m_unique_id_gen_tag{old.m_unique_id_gen_tag} {}

    template <typename Tag>
    decltype( auto ) containers() const {
      if constexpr ( std::is_same_v<Tag, CompositeTags::ChildIndices> ) { return m_containers; }
    }

    // Define a custom proxy class for composites, allowing generic code to use
    // uniformly-named accessors.
    template <SIMDWrapper::InstructionSet simd, Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct Proxy : LHCb::Event::Proxy<simd, behaviour, ContainerType> {
      using base_t = LHCb::Event::Proxy<simd, behaviour, ContainerType>;
      using base_t::base_t;
      using simd_t  = SIMDWrapper::type_map_t<simd>;
      using dType   = simd_t;
      using float_v = typename dType::float_v;

    public:
      // flag which indicates client code can go for 'threeMomCovMatrix', `momPosCovMatrix` and `posCovMatrix` and not
      // for a track-like stateCov -- possible values: yes (for track-like objects ) no (for neutrals, composites),
      // maybe (particle, check at runtime, calls may return invalid results)
      static constexpr auto canBeExtrapolatedDownstream = CanBeExtrapolatedDownstream::no;
      static constexpr auto isBasicParticle             = Event::IsBasicParticle::no;
      static constexpr auto hasTrack                    = Event::HasTrack::no;

      // Standard accessors
      [[nodiscard, gnu::always_inline]] auto chi2() const { return this->template get<CompositeTags::Chi2>(); }
      [[nodiscard, gnu::always_inline]] auto nDoF() const { return this->template get<CompositeTags::nDoF>(); }
      [[nodiscard, gnu::always_inline]] auto pid() const { return this->template get<CompositeTags::PID>(); }
      [[nodiscard, gnu::always_inline]] auto e() const { return this->momentum()( 3 ); }
      [[nodiscard, gnu::always_inline]] auto px() const { return this->momentum().x(); }
      [[nodiscard, gnu::always_inline]] auto py() const { return this->momentum().y(); }
      [[nodiscard, gnu::always_inline]] auto pz() const { return this->momentum().z(); }
      [[nodiscard, gnu::always_inline]] auto momentum() const { return this->template get<CompositeTags::FourMom>(); }
      [[nodiscard, gnu::always_inline]] auto threeMomentum() const { return LinAlg::Vec{px(), py(), pz()}; }
      [[nodiscard, gnu::always_inline]] auto slopes() const {
        auto mom = threeMomentum();
        return mom / Z( mom );
      }

      [[nodiscard, gnu::always_inline]] auto x() const { return this->endVertex().x(); }
      [[nodiscard, gnu::always_inline]] auto y() const { return this->endVertex().y(); }
      [[nodiscard, gnu::always_inline]] auto z() const { return this->endVertex().z(); }
      [[nodiscard, gnu::always_inline]] auto endVertex() const {
        return this->template get<CompositeTags::Position>();
      } // FIXME: it is not guaranteed that the position at which the momentum is given is the 'endVertex' -- so fix
        // names!

      [[nodiscard, gnu::always_inline]] auto referencePoint() const {
        return endVertex();
      } // the point at which the momentum is given

      [[nodiscard, gnu::always_inline]] auto childRelationIndex( std::size_t n_child ) const {
        return this->template field<CompositeTags::ChildRels>()[n_child]
            .field( CompositeTags::ChildRels::IndexOffset )
            .get();
      }
      [[nodiscard, gnu::always_inline]] auto childRelationFamily( std::size_t n_child ) const {
        return this->template field<CompositeTags::ChildRels>()[n_child]
            .field( CompositeTags::ChildRels::ZipFamilyOffset )
            .get();
      }
      [[nodiscard, gnu::always_inline]] auto descendant_unique_id( std::size_t n_child ) const {
        return UniqueIDGenerator::ID<typename simd_t::int_v>{
            this->template field<CompositeTags::DescendantUniqueIDs>()[n_child].get(),
            this->container()->unique_id_gen_tag()};
      }
      [[nodiscard, gnu::always_inline]] auto momCovElement( std::size_t i, std::size_t j ) const {
        return this->template get<CompositeTags::FourMomCov>()( i, j );
      }
      [[nodiscard, gnu::always_inline]] auto momPosCovElement( std::size_t i, std::size_t j ) const {
        return this->template get<CompositeTags::PositionFourMomCov>()( i, j );
      }
      [[nodiscard, gnu::always_inline]] auto numDescendants() const {
        return this->template field<CompositeTags::DescendantUniqueIDs>().maxSize();
      }
      [[nodiscard, gnu::always_inline]] auto posCovElement( std::size_t i, std::size_t j ) const {
        return posCovMatrix()( i, j );
      }
      [[nodiscard, gnu::always_inline]] auto posCovMatrix() const {
        return this->template get<CompositeTags::PositionCov>();
      }
      [[nodiscard, gnu::always_inline]] auto momCovMatrix() const {
        return this->template get<CompositeTags::FourMomCov>();
      }
      // Note tacit assumption that the order is (px, py, pz, pe)
      [[nodiscard, gnu::always_inline]] auto threeMomCovMatrix() const {
        return this->momCovMatrix().template sub<LinAlg::MatSym<float_v, 3>, 0, 0>();
      }
      [[nodiscard, gnu::always_inline]] auto momPosCovMatrix() const {
        return this->template get<CompositeTags::PositionFourMomCov>();
      }
      [[nodiscard, gnu::always_inline]] auto threeMomPosCovMatrix() const {
        // ignore the 4th/energy part of the momentum covariance and return a 3x3
        return this->momPosCovMatrix().template sub<LinAlg::Mat<float_v, 3, 3>, 0, 0>();
      }
      /** Return the full 7x7 covariance matrix.
       *  Order: {x, y, z, px, py, pz, pe}
       */
      [[nodiscard, gnu::always_inline]] auto covMatrix() const {
        LinAlg::MatSym<float_v, 3 + 4> out;
        out = out.template place_at<0, 0>( this->posCovMatrix() );
        out = out.template place_at<3, 0>( this->momPosCovMatrix() );
        return out.template place_at<3, 3>( this->momCovMatrix() );
      }

      // Accessors for derived quantities
      [[nodiscard, gnu::always_inline]] auto pt() const {
        auto const px = this->px();
        auto const py = this->py();
        return sqrt( px * px + py * py );
      }
      [[nodiscard, gnu::always_inline]] auto mom2() const {
        auto const px = this->px();
        auto const py = this->py();
        auto const pz = this->pz();
        return px * px + py * py + pz * pz;
      }
      [[nodiscard, gnu::always_inline]] auto p() const { return sqrt( this->mom2() ); }
      [[nodiscard, gnu::always_inline]] auto mass2() const {
        auto const energy = this->e();
        return energy * energy - this->mom2();
      }
      [[nodiscard, gnu::always_inline]] auto mass() const { return sqrt( this->mass2() ); }
      // Accessors for container-level information
      [[nodiscard, gnu::always_inline]] auto numChildren() const {
        return this->template field<CompositeTags::ChildRels>().maxSize();
      }
      [[nodiscard, gnu::always_inline]] auto zipIdentifier() const { return this->container()->zipIdentifier(); }
    };
    template <SIMDWrapper::InstructionSet simd, Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = Proxy<simd, behaviour, ContainerType>;

    template <SIMDWrapper::InstructionSet simd = SIMDWrapper::InstructionSet::Best, typename F, typename I,
              typename M                       = std::true_type>
    void emplace_back( LinAlg::Vec<F, 3> const& pos, LinAlg::Vec<F, 4> const& p4, I const& pid, F const& chi2,
                       I const& ndof, LinAlg::MatSym<F, 3> const& pos_cov, LinAlg::MatSym<F, 4> const& p4_cov,
                       LinAlg::Mat<F, 4, 3> const& mom_pos_cov, span<std::add_const_t<I>> child_indices,
                       span<std::add_const_t<I>>                    child_zip_ids,
                       std::vector<UniqueIDGenerator::ID<I>> const& descendant_unique_ids, M&& mask = {} ) {

      auto const& proxy = [&]() {
        if constexpr ( std::is_same_v<std::true_type, std::decay_t<M>> ) {
          return emplace_back<simd>();
        } else {
          return compress_back<simd>( mask );
        }
      }();

      proxy.template field<CompositeTags::PID>().set( pid );
      proxy.template field<CompositeTags::Chi2>().set( chi2 );
      proxy.template field<CompositeTags::nDoF>().set( ndof );
      proxy.template field<CompositeTags::Position>().set( pos );
      proxy.template field<CompositeTags::FourMom>().set( p4 );
      proxy.template field<CompositeTags::PositionCov>().set( pos_cov );
      proxy.template field<CompositeTags::FourMomCov>().set( p4_cov );
      proxy.template field<CompositeTags::PositionFourMomCov>().set( mom_pos_cov );

      assert( child_indices.size() == child_zip_ids.size() );
      proxy.template field<CompositeTags::ChildRels>().resize( child_indices.size() );
      for ( auto i = 0u; i < child_indices.size(); ++i ) {
        proxy.template field<CompositeTags::ChildRels>()[i]
            .field( CompositeTags::ChildRels::IndexOffset )
            .set( child_indices[i] );
        proxy.template field<CompositeTags::ChildRels>()[i]
            .field( CompositeTags::ChildRels::ZipFamilyOffset )
            .set( child_zip_ids[i] );
      }

      proxy.template field<CompositeTags::DescendantUniqueIDs>().resize( descendant_unique_ids.size() );
      for ( auto i = 0u; i < descendant_unique_ids.size(); ++i ) {
        assert( descendant_unique_ids[i].generator_tag() == m_unique_id_gen_tag );
        proxy.template field<CompositeTags::DescendantUniqueIDs>()[i].set( descendant_unique_ids[i].value() );
      }
    }

    template <SIMDWrapper::InstructionSet simd = SIMDWrapper::InstructionSet::Best, typename F, typename I,
              std::size_t                 NChildren, typename M = std::true_type>
    void emplace_back( LinAlg::Vec<F, 3> const& pos, LinAlg::Vec<F, 4> const& p4, I const& pid, F const& chi2,
                       I const& ndof, LinAlg::MatSym<F, 3> const& pos_cov, LinAlg::MatSym<F, 4> const& p4_cov,
                       LinAlg::Mat<F, 4, 3> const& mom_pos_cov, std::array<I, NChildren> const& child_indices,
                       std::array<I, NChildren> const&              child_zip_ids,
                       std::vector<UniqueIDGenerator::ID<I>> const& descendant_unique_ids, M&& mask = {} ) {
      emplace_back<simd>( pos, p4, pid, chi2, ndof, pos_cov, p4_cov, mom_pos_cov, span{child_indices},
                          span{child_zip_ids}, descendant_unique_ids, mask );
    }

    auto const& unique_id_gen_tag() const { return m_unique_id_gen_tag; }

  private:
    /// Keep the identifier of the generator used to build this container
    boost::uuids::uuid m_unique_id_gen_tag;
  };
} // namespace LHCb::Event

namespace LHCb::Event {
  using Particles = LHCb::variant<ChargedBasics, NeutralBasics, Composites>;
} // namespace LHCb::Event
