/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/Particle.h"

/* On-demand bremsstrahlung recovery, using wrapper around Particle / Composite
 * / ChargedBasic/Zip-of-Track-BremInfo that has all the right friend functions
 * that are relevant for bremsstrahlung recovery:
 *  - pure momenta:
 *    - threeMomentum
 *    - fourMomentum
 *    - mass2
 *  - including covariance:
 *    - threeMomCovMatrix
 *    - momCovMatrix
 *    - threeMomPosCovMatrix
 *    - momPosCovMatrix
 * including those that just need the underlying class (see e.g. Particle.h),
 * e.g. can be needed for DecayTreeFitter (which will just see the wrapper)
 *  - referencePoint
 *  - slopes
 *  - endVertexPos
 *  - posCovMatrix
 *
 *  Implementation of what is in SelectiveBremAdder, but calculation on demand, but no overlap checking
 *
 *  NOTE: More specialized brem options should be done / implemented in DecayTreeFitter,
 *        like overlap checking (as it is decay tree specific),
 *        or use DiElectronMaker like builders
 */

namespace LHCb::Event::Bremsstrahlung {

  namespace details {
    using int_t       = SIMDWrapper::scalar::int_v;
    using float_t     = SIMDWrapper::scalar::float_v;
    using momentum3_t = LHCb::LinAlg::Vec<float_t, 3>;
    using momentum4_t = LHCb::LinAlg::Vec<float_t, 4>;

    namespace defaults {
      // relative momentum uncertainty squared
      // effectively variance on 'bremfrac'
      // for in and out of brem acceptance
      float_t constexpr err2_in  = 0.0625f;
      float_t constexpr err2_out = 0.1225f;
    } // namespace defaults

    struct bremid_t {
      int_t   id;
      float_t chi2;
      // for cluster overlap check
      friend auto operator==( bremid_t const& lhs, bremid_t const& rhs ) {
        return ( lhs.id != 0 ) && ( lhs.id == rhs.id );
      }
      // priority according to matching chi2
      friend auto operator<( bremid_t const& lhs, bremid_t const& rhs ) { return lhs.chi2 < rhs.chi2; }
    };

    // general functions and lambdas for kinematic calculations
    template <typename Momentum>
    float_t bremFrac( Momentum const& mom, float_t energy, float_t bendcorr = 1. ) {
      return ( 1. + bendcorr * energy / mom.mag() );
    }

    template <typename Wrapper>
    auto threeMomentumCorr = []( Wrapper const& wrapper ) {
      auto mom = threeMomentum( wrapper.base() );
      return wrapper.shouldAddBrem() && wrapper.hasBrem() ? wrapper.bremFrac( mom ) * mom : mom;
    };

    template <typename Wrapper>
    auto fourMomentumCorr = []( Wrapper const& wrapper ) {
      using RetType = decltype( fourMomentum( wrapper.base() ) );
      if ( wrapper.shouldAddBrem() && wrapper.hasBrem() ) {
        auto mom    = threeMomentum( wrapper );
        auto energy = sqrt( mass2( wrapper.base() ) + mom.mag2() );
        return RetType{mom.X(), mom.Y(), mom.Z(), energy};
      } else {
        return fourMomentum( wrapper.base() );
      }
    };

    template <typename Wrapper>
    auto threeMomCovMatrixCorr = []( Wrapper const& wrapper ) {
      if ( wrapper.shouldAddBrem() ) {
        auto mom      = threeMomentum( wrapper.base() );
        auto cov0     = threeMomCovMatrix( wrapper.base() );
        auto cov1     = decltype( cov0 ){X( mom ) * X( mom ), X( mom ) * Y( mom ), Y( mom ) * Y( mom ),
                                     X( mom ) * Z( mom ), Y( mom ) * Z( mom ), Z( mom ) * Z( mom )};
        auto bremfrac = wrapper.hasBrem() ? wrapper.bremFrac( mom ) : 1;
        auto fcov     = wrapper.inBrem() ? wrapper.relErr2InBrem() : wrapper.relErr2OutBrem();
        return cov0 * ( bremfrac * bremfrac ) + cov1 * fcov;
      } else {
        return threeMomCovMatrix( wrapper.base() );
      }
    };

    template <typename Wrapper>
    auto momCovMatrixCorr = []( Wrapper const& wrapper ) {
      if ( wrapper.shouldAddBrem() ) {
        auto mom  = fourMomentum( wrapper.base() );
        auto cov0 = momCovMatrix( wrapper.base() );
        auto cov1 = decltype( cov0 ){X( mom ) * X( mom ), X( mom ) * Y( mom ), Y( mom ) * Y( mom ), X( mom ) * Z( mom ),
                                     Y( mom ) * Z( mom ), Z( mom ) * Z( mom ), X( mom ) * E( mom ), Y( mom ) * E( mom ),
                                     Z( mom ) * E( mom ), E( mom ) * E( mom )};
        auto bremfrac = wrapper.hasBrem() ? wrapper.bremFrac( threeMomentum( wrapper.base() ) ) : 1;
        auto fcov     = wrapper.inBrem() ? wrapper.relErr2InBrem() : wrapper.relErr2OutBrem();
        return cov0 * ( bremfrac * bremfrac ) + cov1 * fcov;
      } else {
        return momCovMatrix( wrapper.base() );
      }
    };

    template <typename Wrapper>
    auto threeMomPosCovMatrixCorr = []( Wrapper const& wrapper ) {
      auto mom = threeMomentum( wrapper.base() );
      return ( wrapper.shouldAddBrem() && wrapper.hasBrem() )
                 ? threeMomPosCovMatrix( wrapper.base() ) * wrapper.bremFrac( mom )
                 : threeMomPosCovMatrix( wrapper.base() );
    };

    template <typename Wrapper>
    auto momPosCovMatrixCorr = []( Wrapper const& wrapper ) {
      auto mom = threeMomentum( wrapper.base() );
      return ( wrapper.shouldAddBrem() && wrapper.hasBrem() )
                 ? momPosCovMatrix( wrapper.base() ) * wrapper.bremFrac( mom )
                 : momPosCovMatrix( wrapper.base() );
    };

    /////////////////////////////////////////////////////////////////
    // base class to collect all functionality overlapping between //
    // Basics and Composites both in v1 and v2 event model         //
    /////////////////////////////////////////////////////////////////
    template <typename Base>
    class BremWrapperBase {
    public:
      // constructors
      BremWrapperBase( Base const& base, bool only_electrons = false,
                       details::float_t rel_err2_inbrem  = details::defaults::err2_in,
                       details::float_t rel_err2_outbrem = details::defaults::err2_out )
          : m_base_ptr( &base )
          , m_only_electrons( only_electrons )
          , m_overlap_veto( false )
          , m_rel_err2_inbrem( rel_err2_inbrem )
          , m_rel_err2_outbrem( rel_err2_outbrem ) {}

      // setting functions
      void setOverlapVeto( bool veto = true ) { m_overlap_veto = veto; }

      // access to underlying data
      Base const& base() const { return *m_base_ptr; }
      bool        onlyElectrons() const { return m_only_electrons; }
      bool        overlapVeto() const { return m_overlap_veto; }

      // squared uncertainties relative to momentum for in/out brem acceptance
      details::float_t relErr2InBrem() const { return m_rel_err2_inbrem; }
      details::float_t relErr2OutBrem() const { return m_rel_err2_outbrem; }

      // functions specific to base
      virtual bool              shouldAddBrem() const                         = 0;
      virtual bool              hasBrem() const                               = 0;
      virtual bool              inBrem() const                                = 0;
      virtual details::bremid_t bremID() const                                = 0;
      virtual details::float_t  bremFrac( details::momentum3_t const& ) const = 0;

    private:
      /// input
      // underlying particle it wraps around
      Base const* m_base_ptr;
      // allows to calculate brem for non-composites that are not electrons
      bool m_only_electrons;
      // overlap bookkeeping
      bool m_overlap_veto;
      // covariance constants
      details::float_t m_rel_err2_inbrem;
      details::float_t m_rel_err2_outbrem;
    };
  } // namespace details

  ////////////////////////////////////////////////////////////////////////////////
  //  General (for V2) event model implementation, both basic and composites    //
  ////////////////////////////////////////////////////////////////////////////////
  template <typename Base>
  struct BremsstrahlungWrapper : public details::BremWrapperBase<Base> {
    using details::BremWrapperBase<Base>::BremWrapperBase;

  private:
    template <typename Correction>
    friend auto runCorrection( BremsstrahlungWrapper const& wrapper, Correction const& corr ) {
      // TODO add composite option
      return corr( wrapper );
    }

  public:
    bool              shouldAddBrem() const { return true; }
    bool              hasBrem() const { return this->base().HasBrem(); }
    bool              inBrem() const { return this->base().InBrem(); }
    details::bremid_t bremID() const { return {this->base().BremHypoID(), this->base().BremHypoMatch()}; }

    details::float_t bremFrac( details::momentum3_t const& mom ) const {
      using Info      = ProtoParticle::additionalInfo;
      auto bremenergy = details::float_t{this->base().BremEnergy()};
      auto bendcorr   = details::float_t{this->base().BremBendingCorrection()};
      return details::bremFrac( mom, bremenergy, bendcorr );
    }

  public:
    // MAIN USAGE friend functions returning kinematic info
    [[nodiscard]] friend auto threeMomentum( BremsstrahlungWrapper const& wrapper ) {
      return runCorrection( wrapper, details::threeMomentumCorr<BremsstrahlungWrapper> );
    }

    [[nodiscard]] friend auto fourMomentum( BremsstrahlungWrapper const& wrapper ) {
      return runCorrection( wrapper, details::fourMomentumCorr<BremsstrahlungWrapper> );
    }

    [[nodiscard]] friend auto mass2( BremsstrahlungWrapper const& wrapper ) {
      return LHCb::LinAlg::mass2( fourMomentum( wrapper ) );
    }

    // covariances
    [[nodiscard]] friend auto threeMomCovMatrix( BremsstrahlungWrapper const& wrapper ) {
      return runCorrection( wrapper, details::threeMomCovMatrixCorr<BremsstrahlungWrapper> );
    }

    [[nodiscard]] friend auto momCovMatrix( BremsstrahlungWrapper const& wrapper ) {
      return runCorrection( wrapper, details::momCovMatrixCorr<BremsstrahlungWrapper> );
    }

    [[nodiscard]] friend auto threeMomPosCovMatrix( BremsstrahlungWrapper const& wrapper ) {
      return runCorrection( wrapper, details::threeMomPosCovMatrixCorr<BremsstrahlungWrapper> );
    }

    [[nodiscard]] friend auto momPosCovMatrix( BremsstrahlungWrapper const& wrapper ) {
      return runCorrection( wrapper, details::momPosCovMatrixCorr<BremsstrahlungWrapper> );
    }

    // friend functions that are unaffected by bremsstrahlung
    [[nodiscard]] friend auto referencePoint( BremsstrahlungWrapper const& wrapper ) {
      return referencePoint( wrapper.base() );
    }

    [[nodiscard]] friend auto slopes( BremsstrahlungWrapper const& wrapper ) { return slopes( wrapper.base() ); }

    [[nodiscard]] friend auto endVertexPos( BremsstrahlungWrapper const& wrapper ) {
      return endVertexPos( wrapper.base() );
    }

    [[nodiscard]] friend auto posCovMatrix( BremsstrahlungWrapper const& wrapper ) {
      return posCovMatrix( wrapper.base() );
    }
  };

  ////////////////////////////////////////////////////////////////////////////////
  // specific V1 event model implementation, both basic and composite Particles //
  ////////////////////////////////////////////////////////////////////////////////
  template <>
  struct BremsstrahlungWrapper<LHCb::Particle> : public details::BremWrapperBase<LHCb::Particle> {
    using base_t = details::BremWrapperBase<LHCb::Particle>;
    using base_t::BremWrapperBase;

  private:
    template <typename Correction>
    friend auto runCorrection( BremsstrahlungWrapper const& wrapper, Correction const& corr, bool onlyBasics = false ) {
      auto const& part = wrapper.base();
      if ( part.isBasicParticle() ) {
        return corr( wrapper );
      } else {
        if ( onlyBasics ) {
          throw GaudiException(
              "Running this ADL function on a Composite with Bremsstrahlung wrapper is not supported.",
              "BremsstrahlungWrapper", StatusCode::FAILURE );
        } else {
          // assumes the combined correction is a sum (like for momentum)
          // check daughters of Particle if it has charged-track Particles
          // adds brem only if ID is electron, as:
          // - for composites, don't know otherwise where to calculate brem for
          // - 'only to all electrons' is the most common use case
          auto newval = decltype( corr( wrapper ) )();
          for ( LHCb::Particle const* dau : part.daughtersVector() ) {
            newval = newval +
                     runCorrection(
                         BremsstrahlungWrapper( *dau, true, wrapper.relErr2InBrem(), wrapper.relErr2OutBrem() ), corr );
          }
          return newval;
        }
      }
    }

  public:
    bool shouldAddBrem() const {
      using Info = LHCb::Particle::additionalInfo;
      return ( this->base().proto()->track() && !this->base().hasInfo( Info::HasBremAdded ) &&
               ( !this->onlyElectrons() || ( this->base().particleID().abspid() == 11 ) ) );
    }

    bool hasBrem() const {
      using Info = LHCb::ProtoParticle::additionalInfo;
      return this->base().proto() && ( this->base().proto()->info( Info::CaloHasBrem, 0 ) > 0. ) &&
             !this->overlapVeto();
    }

    bool inBrem() const {
      using Info = LHCb::ProtoParticle::additionalInfo;
      return this->base().proto() && ( this->base().proto()->info( Info::InAccBrem, 0 ) > 0. );
    }

    details::bremid_t bremID() const {
      using Info = LHCb::ProtoParticle::additionalInfo;
      auto proto = this->base().proto();
      if ( !proto ) return {0, 0.f};
      auto id   = (int)proto->info( LHCb::ProtoParticle::additionalInfo::CaloBremHypoID, 0 );
      auto chi2 = (details::float_t)proto->info( LHCb::ProtoParticle::additionalInfo::CaloBremMatch, -1. );
      return {id, chi2};
    }

    details::float_t bremFrac( details::momentum3_t const& mom ) const {
      using Info      = ProtoParticle::additionalInfo;
      auto proto      = this->base().proto();
      auto bremenergy = details::float_t{proto->info( Info::CaloBremEnergy, 0. )};
      auto bendcorr   = details::float_t{proto->info( Info::CaloBremBendingCorr, 1. )};
      return details::bremFrac( mom, bremenergy, bendcorr );
    }

  public:
    // MAIN USAGE friend functions returning kinematic info
    [[nodiscard]] friend auto threeMomentum( BremsstrahlungWrapper const& wrapper ) {
      return runCorrection( wrapper, details::threeMomentumCorr<BremsstrahlungWrapper> );
    }

    [[nodiscard]] friend auto fourMomentum( BremsstrahlungWrapper const& wrapper ) {
      return runCorrection( wrapper, details::fourMomentumCorr<BremsstrahlungWrapper> );
    }

    [[nodiscard]] friend auto mass2( BremsstrahlungWrapper const& wrapper ) {
      return LHCb::LinAlg::mass2( fourMomentum( wrapper ) );
    }

    // covariances
    [[nodiscard]] friend auto threeMomCovMatrix( BremsstrahlungWrapper const& wrapper ) {
      return runCorrection( wrapper, details::threeMomCovMatrixCorr<BremsstrahlungWrapper> );
    }

    [[nodiscard]] friend auto momCovMatrix( BremsstrahlungWrapper const& wrapper ) {
      return runCorrection( wrapper, details::momCovMatrixCorr<BremsstrahlungWrapper> );
    }

    [[nodiscard]] friend auto threeMomPosCovMatrix( BremsstrahlungWrapper const& wrapper ) {
      // covariance of composites not supported (not a simple sum)
      return runCorrection( wrapper, details::threeMomPosCovMatrixCorr<BremsstrahlungWrapper>, false );
    }

    [[nodiscard]] friend auto momPosCovMatrix( BremsstrahlungWrapper const& wrapper ) {
      // covariance of composites not supported (not a simple sum)
      return runCorrection( wrapper, details::momPosCovMatrixCorr<BremsstrahlungWrapper>, false );
    }

    // friend functions that are unaffected by bremsstrahlung
    [[nodiscard]] friend auto referencePoint( BremsstrahlungWrapper const& wrapper ) {
      return referencePoint( wrapper.base() );
    }

    [[nodiscard]] friend auto slopes( BremsstrahlungWrapper const& wrapper ) { return slopes( wrapper.base() ); }

    [[nodiscard]] friend auto endVertexPos( BremsstrahlungWrapper const& wrapper ) {
      return endVertexPos( wrapper.base() );
    }

    [[nodiscard]] friend auto posCovMatrix( BremsstrahlungWrapper const& wrapper ) {
      return posCovMatrix( wrapper.base() );
    }
  };

} // namespace LHCb::Event::Bremsstrahlung
