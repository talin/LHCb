/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/Muon/Layout.h"
#include "Event/MuonCoord.h"
#include "Event/RawBank.h"
#include "Kernel/EventLocalAllocator.h"
#include "LHCbAlgs/Transformer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonDAQHelper.h"
#include "MuonDet/MuonNamespace.h"
#include "range/v3/version.hpp"
#include "range/v3/view/drop_exactly.hpp"
#include "range/v3/view/map.hpp"
#include "range/v3/view/remove_if.hpp"
#include "range/v3/view/subrange.hpp"
#include "range/v3/view/take_exactly.hpp"
#include "range/v3/view/zip.hpp"
#include <assert.h>
#include <functional>
#include <optional>

// define error enum / category
namespace MuonRaw {
  enum class ErrorCode : StatusCode::code_t {
    BAD_MAGIC = 10,
    BAD_TYPE  = 10,
    BANK_TOO_SHORT,
    PADDING_TOO_LONG,
    TOO_MANY_HITS,
    INVALID_TELL1
  };
  struct ErrorCategory : StatusCode::Category {
    const char* name() const override { return "MuonRawBankDecoding"; }
    bool        isRecoverable( StatusCode::code_t ) const override { return false; }
    std::string message( StatusCode::code_t code ) const override {
      switch ( static_cast<MuonRaw::ErrorCode>( code ) ) {
      case ErrorCode::BAD_MAGIC:
        return "Incorrect Magic pattern in raw bank";
      case ErrorCode::BANK_TOO_SHORT:
        return "Muon bank is too short";
      case ErrorCode::PADDING_TOO_LONG:
        return "Muon bank has too much padding for its size";
      case ErrorCode::TOO_MANY_HITS:
        return "Muon bank has too many hits for its size";
      case ErrorCode::INVALID_TELL1:
        return "Invalid TELL1 source ID";
      default:
        return StatusCode::default_category().message( code );
      }
    }
  };
} // namespace MuonRaw
STATUSCODE_ENUM_DECL( MuonRaw::ErrorCode )
STATUSCODE_ENUM_IMPL( MuonRaw::ErrorCode, MuonRaw::ErrorCategory )

namespace {
  struct Digit {
    LHCb::Detector::Muon::TileID tile;
    unsigned int                 tdc;
  };
  using Digits = std::vector<Digit>;

  [[gnu::noreturn]] void throw_exception( MuonRaw::ErrorCode ec, const char* tag ) {
    auto sc = StatusCode( ec );
    throw GaudiException{sc.message(), tag, std::move( sc )};
  }
#define OOPS( x ) throw_exception( x, __PRETTY_FUNCTION__ )

  /// fills in the two readout layouts by querying the DeMuonRegion
  template <int N = 0>
  LHCb::Detector::Muon::Layout makeStripLayout( const DeMuonDetector& det, const LHCb::Detector::Muon::TileID& tile ) {
    static_assert( N == 0 || N == 1 );
    unsigned int station = tile.station();
    unsigned int region  = tile.region();
    unsigned int x       = det.getLayoutX( N, station, region );
    unsigned int y       = det.getLayoutY( N, station, region );
    return {x, y};
  }

  int nDigits( const LHCb::RawBank& rb ) {
    auto range     = rb.range<unsigned short>();
    using SpanSize = decltype( range )::size_type;
    if ( range.empty() ) return 0;
    const SpanSize preamble_size = 2 * ( ( range[0] + 3 ) / 2 );
    const SpanSize overhead      = preamble_size + 4;
    return ( range.size() > overhead ? range.size() - overhead : 0 );
  }

  int nDigits( LHCb::RawBank::View rbs ) {
    return std::accumulate( rbs.begin(), rbs.end(), 0,
                            []( int s, const LHCb::RawBank* rb ) { return rb ? s + nDigits( *rb ) : s; } );
  }

  /// convert raw data into tiles
  template <typename MakeTile, typename OutputIterator>
  OutputIterator decodeTileAndTDCV1( LHCb::span<const unsigned short> rawdata, MakeTile&& make_tile,
                                     OutputIterator out ) {
    using SpanSize = decltype( rawdata )::size_type;
    // minimum length is three 32 bit words --> 12 bytes -> 6 unsigned shorts
    if ( rawdata.size() < 6 ) OOPS( MuonRaw::ErrorCode::BANK_TOO_SHORT );
    const SpanSize preamble_size = 2 * ( ( rawdata[0] + 3 ) / 2 );
    if ( rawdata.size() < preamble_size ) { OOPS( MuonRaw::ErrorCode::PADDING_TOO_LONG ); }
    rawdata = rawdata.subspan( preamble_size );
    for ( int i = 0; i < 4; ++i ) {
      if ( rawdata.empty() ) { OOPS( MuonRaw::ErrorCode::BANK_TOO_SHORT ); }
      if ( rawdata.size() < static_cast<SpanSize>( 1 + rawdata[0] ) ) { OOPS( MuonRaw::ErrorCode::TOO_MANY_HITS ); }
      for ( unsigned int pp : rawdata.subspan( 1, rawdata[0] ) ) {
        unsigned int                                add       = ( pp & 0x0FFF );
        unsigned int                                tdc_value = ( ( pp & 0xF000 ) >> 12 );
        std::optional<LHCb::Detector::Muon::TileID> tile      = make_tile( add );
        if ( !tile.has_value() ) continue;
        *out++ = {std::move( *tile ), tdc_value};
      }
      rawdata = rawdata.subspan( 1 + rawdata[0] );
    }
    assert( rawdata.size() < 2 );
    return out;
  }

  /// Copy Detector::Muon::TileID from digits to coord by crossing the digits
  template <typename Iterator, typename Container>
  Iterator addCoordsCrossingMap( Iterator first, Iterator pivot, Iterator last, Container& retVal ) {
    assert( first != last );
    assert( first <= pivot );
    assert( pivot <= last );

    static_assert( std::is_same_v<typename std::iterator_traits<Iterator>::value_type, Digit> );

    // used flags
    std::vector<bool> used( last - first, false );

    // partition into the two directions of digits
    // vertical and horizontal stripes
    const auto N = std::distance( first, pivot );

    auto usedAndDigits = ranges::views::zip( used, ranges::make_subrange( first, last ) );
    auto digitsOne     = ( usedAndDigits | ranges::views::take_exactly( N ) );
    auto digitsTwo     = ( usedAndDigits | ranges::views::drop_exactly( N ) );

    // check how many cross
    retVal.reserve( digitsOne.size() * digitsTwo.size() + ( last - first ) );
    for ( auto&& [used_one, digit_one] : digitsOne ) {
      for ( auto&& [used_two, digit_two] : digitsTwo ) {
        LHCb::Detector::Muon::TileID pad = digit_one.tile.intercept( digit_two.tile );
        if ( !pad.isValid() ) continue;

        retVal.emplace_back( pad, digit_one.tile, digit_two.tile, digit_one.tdc, digit_two.tdc );
        // set used flag
        used_one = used_two = true;
      }
    }
    // copy over "uncrossed" digits
    for ( const Digit& digit : usedAndDigits | ranges::views::remove_if( []( const auto& p ) { return p.first; } ) |
                                   ranges::views::values ) {
      retVal.emplace_back( digit.tile, digit.tdc );
    }
    return last;
  }

  template <typename Projection, typename Cmp = std::less<>>
  auto orderByProjection( Projection&& projection, Cmp&& cmp = {} ) {
    return [proj = std::forward<Projection>( projection ), cmp = std::forward<Cmp>( cmp )]( const auto& lhs,
                                                                                            const auto& rhs ) {
      return std::invoke( cmp, std::invoke( proj, lhs ), std::invoke( proj, rhs ) );
    };
  }
  template <typename Projection, typename Value>
  auto hasEqualProjection( Projection&& projection, const Value& ref ) {
    auto proj_ref = std::invoke( projection, ref );
    return [proj_ref, proj = std::forward<Projection>( projection )]( const Value& val ) {
      return proj_ref == std::invoke( proj, val );
    };
  }
  auto stationRegion( const Digit& d ) { return std::tuple{d.tile.station(), d.tile.region()}; }

  using MuonCoords = std::vector<LHCb::MuonCoord, LHCb::Allocators::EventLocal<LHCb::MuonCoord>>;

} // namespace

//-----------------------------------------------------------------------------
// Implementation file for class : MuonRawToCoord
//-----------------------------------------------------------------------------
/** @class MuonRawToCoord MuonRawToCoord.h
 *  This is the muon reconstruction algorithm
 *  This just crosses the logical strips back into pads
 */

class MuonRawToCoord final
    : public LHCb::Algorithm::Transformer<
          MuonCoords( const EventContext&, const DeMuonDetector&, const LHCb::RawBank::View& ),
          LHCb::Algorithm::Traits::use_<
              LHCb::Algorithm::Traits::writeViewFor<MuonCoords, LHCb::span<LHCb::MuonCoord const>>,
              LHCb::Algorithm::Traits::usesConditions<DeMuonDetector>>> {

  mutable Gaudi::Accumulators::BinomialCounter<>  m_invalid_add{this, "invalid add"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_digits{this, "#digits"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_coords{this, "#coords"};
  // TODO: add min/max to m_deltaEstimate...
  mutable Gaudi::Accumulators::AveragingCounter<> m_deltaEstimate{this, "#digits - estimated # digits"};
  mutable Gaudi::Accumulators::BinomialCounter<>  m_estNotOK{this, "#digits > estimated # digits"};

public:
  /// Standard constructor
  MuonRawToCoord( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer{name,
                    pSvcLocator,
                    {KeyValue{"MuonDetectorPath", DeMuonLocation::Default}, KeyValue{"RawBanksLocation", ""}},
                    KeyValue{"MuonCoordLocation", LHCb::MuonCoordLocation::MuonCoords}} {}

  MuonCoords operator()( const EventContext& ctx, const DeMuonDetector& muonDet,
                         const LHCb::RawBank::View& banks ) const override {

    MuonCoords coords{LHCb::getMemResource( ctx )};
    if ( banks.empty() ) return coords;

    for ( const auto* r : banks ) {
      if ( LHCb::RawBank::MagicPattern != r->magic() ) OOPS( MuonRaw::ErrorCode::BAD_MAGIC );
      if ( LHCb::RawBank::Muon != r->type() ) OOPS( MuonRaw::ErrorCode::BAD_TYPE );
    }

    int                est_nDigits = nDigits( banks );
    std::vector<Digit> decoding;
    decoding.reserve( est_nDigits );
    for ( const auto* r : banks ) {
      unsigned int tell1 = r->sourceID();
      if ( tell1 >= MuonDAQHelper::maxTell1Number ) OOPS( MuonRaw::ErrorCode::INVALID_TELL1 );
      decodeTileAndTDCV1(
          r->range<unsigned short>(),
          [& di        = muonDet.getDAQInfo()->getADDInTell1( tell1 ),
           invalid_add = m_invalid_add.buffer()]( unsigned int add ) mutable {
            bool valid = add < di.size();
            invalid_add += !valid;
            return valid ? std::optional{di[add]} : std::nullopt;
          },
          std::back_inserter( decoding ) );
    }

    m_deltaEstimate += static_cast<int>( decoding.size() ) - est_nDigits;
    m_estNotOK += ( static_cast<int>( decoding.size() ) > est_nDigits );
    m_digits += decoding.size();
    if ( decoding.empty() ) {
      error() << "Error in decoding the muon raw data ";
      return coords;
    }
    auto first = decoding.begin();
    auto last  = decoding.end();
    while ( first != last ) {
      std::nth_element( first, first, last, orderByProjection( stationRegion ) );
      auto next = std::partition( std::next( first ), last, hasEqualProjection( stationRegion, *first ) );
      auto pivot =
          std::partition( first, next, [layout = makeStripLayout<0>( muonDet, first->tile )]( const Digit& digit ) {
            return digit.tile.layout() == layout;
          } );
      first = addCoordsCrossingMap( first, pivot, next, coords );
    }
    m_coords += coords.size();
    return coords;
  }
};

DECLARE_COMPONENT( MuonRawToCoord )
