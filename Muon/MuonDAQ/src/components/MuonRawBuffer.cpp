/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DAQKernel/DecoderToolBase.h"

#include "Event/MuonBankVersion.h"
#include "Event/MuonODEData.h"
#include "Event/MuonPPEventInfo.h"
#include "Event/RawBank.h"

#include "MuonDAQ/IMuonRawBuffer.h" // Interface
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonDAQHelper.h"
#include "MuonDet/MuonL1Board.h"
#include "MuonDet/MuonNamespace.h"
#include "MuonDet/MuonODEBoard.h"
#include "MuonDet/MuonStationCabling.h"
#include "MuonDet/MuonTSMap.h"

#include "GaudiAlg/GaudiTool.h"

#include "fmt/format.h"

#include <array>
#include <bitset>
#include <string>

namespace {
  // some template metaprogramming to allow easy usage of arrays of Gaudi counters
  template <std::size_t N>
  struct CounterArray : std::array<Gaudi::Accumulators::Counter<>, N> {
    template <typename OWNER, int... I>
    CounterArray( OWNER* owner, std::string_view name, std::integer_sequence<int, I...> )
        : std::array<Gaudi::Accumulators::Counter<>, N>{
              {{owner, fmt::format( "{}{}", name, std::to_string( I ) )}...}} {}
    template <typename OWNER, typename Indices = std::make_integer_sequence<int, N>>
    CounterArray( OWNER* owner, std::string_view name ) : CounterArray( owner, name, Indices{} ) {}
  };
} // namespace

namespace LHCb {
  /**
   *  @author Alessia Satta
   *  @date   2005-10-18
   */
  class MuonRawBuffer : public extends<Decoder::ToolBase, IMuonRawBuffer> {
  public:
    using extends::extends;

    IMuonRawBuffer::TileAndADCVector getTileAndTDC( RawBank::View const&, MuonDAQHelper const& ) const override;

  private:
    void putStatusOnTES( RawBankReadoutStatus const& ) const;

    IMuonRawBuffer::TileAndADCVector decodeTileAndTDCDC06( RawBank const&, MuonDAQHelper const& ) const;
    IMuonRawBuffer::TileAndADCVector decodeTileAndTDCV1( RawBank const&, MuonDAQHelper const&,
                                                         RawBankReadoutStatus& ) const;
    IMuonRawBuffer::TileAndADCVector DecodeData( RawBank const&, MuonDAQHelper const&, RawBankReadoutStatus& ) const;
    StatusCode                       checkBankSize( RawBank const&, RawBankReadoutStatus& ) const;
    StatusCode                       checkAllHeaders( RawBank::View const&, RawBankReadoutStatus& ) const;
    void                             checkTell1Header( unsigned int tell1, unsigned int data ) const;

    mutable CounterArray<MuonDAQHelper::maxTell1Number> m_counter_invalid_hit{this, "CounterInvalidHit_Tell"};
    mutable CounterArray<MuonDAQHelper::maxTell1Number> m_processed_bank{this, "ProcessedBank_Tell"};
    mutable CounterArray<MuonDAQHelper::maxTell1Number> m_hit_checkSize{this, "HitCheckSize_Tell"};
    mutable CounterArray<MuonDAQHelper::maxTell1Number> m_tell1_header_error{this, "HeaderErrors_Tell"};
    mutable CounterArray<MuonDAQHelper::maxTell1Number> m_tell1_header_ORODE_error{this, "ORODEError_Tell"};
    mutable CounterArray<MuonDAQHelper::maxTell1Number> m_tell1_header_SYNCH_data_error{this, "SYNCHdataError_Tell"};
    mutable CounterArray<MuonDAQHelper::maxTell1Number> m_tell1_header_SYNCH_BC_error{this, "SYNCHBCError_Tell"};
    mutable CounterArray<MuonDAQHelper::maxTell1Number> m_tell1_header_SYNCH_Evt_error{this, "SYNCHEvtError_Tell"};
  };

  DECLARE_COMPONENT_WITH_ID( MuonRawBuffer, "MuonRawBuffer" )
} // namespace LHCb

LHCb::IMuonRawBuffer::TileAndADCVector LHCb::MuonRawBuffer::decodeTileAndTDCDC06( RawBank const&       rawdata,
                                                                                  MuonDAQHelper const& daqInfo ) const {
  IMuonRawBuffer::TileAndADCVector storage;

  if ( RawBank::MagicPattern != rawdata.magic() ) {
    throw GaudiException( "magic pattern not correct in muon bank", "MuonRawBuffer::decodeTileAndTDCDC06",
                          StatusCode::FAILURE );
  }

  const unsigned char* it   = rawdata.begin<unsigned char>();
  short                skip = 0;

  unsigned int tell1Number = rawdata.sourceID();
  if ( tell1Number >= daqInfo.M1TellNumber() ) {
    // how many pads ?
    short nPads = *rawdata.begin<short>();
    skip        = ( nPads + 2 ) / 2;
  }
  std::advance( it, 4 * skip );

  // how many ODE in this tell1?

  unsigned int              nODE = daqInfo.ODEInTell1( rawdata.sourceID() );
  std::vector<unsigned int> firedInODE;
  firedInODE.resize( nODE );
  unsigned int itODE           = 0;
  unsigned int channelsInTell1 = 0;
  for ( itODE = 0; itODE < nODE; itODE++ ) {
    // first decode the header
    firedInODE[itODE] = *it;
    channelsInTell1 += *it;
    it++;
  }
  for ( itODE = 0; itODE < nODE; itODE++ ) {
    unsigned int channels  = 0;
    unsigned int odenumber = daqInfo.getODENumberInTell1( rawdata.sourceID(), itODE );
    for ( channels = 0; channels < firedInODE[itODE]; channels++ ) {
      unsigned int                                    address = *it;
      Detector::Muon::TileID                          tile    = daqInfo.getADDInODENoHole( odenumber - 1, address );
      std::pair<Detector::Muon::TileID, unsigned int> tileAndTDC;
      tileAndTDC.first = tile;
      storage.push_back( tileAndTDC );
      it++;
    }
  }

  // then decode the TDC info

  // first skip the byte required for padding
  // how many?
  if ( ( channelsInTell1 + nODE ) % 4 ) { std::advance( it, 4 - ( channelsInTell1 + nODE ) % 4 ); }

  unsigned int TDCword       = ( channelsInTell1 + 1 ) / 2;
  unsigned int countChannels = 0;
  unsigned int chIterator    = 0;
  for ( unsigned int ch = 0; ch < TDCword; ch++ ) {
    unsigned int time1 = ( *it ) & ( (unsigned char)15 );
    unsigned int time2 = ( ( *it ) >> 4 ) & ( (unsigned char)15 );

    storage[chIterator].second = time1;
    chIterator++;

    if ( countChannels + 1 < channelsInTell1 ) {
      storage[chIterator].second = time2;
      chIterator++;
    }
    countChannels = countChannels + 2;
    it++;
  }
  return storage;
}

LHCb::IMuonRawBuffer::TileAndADCVector LHCb::MuonRawBuffer::decodeTileAndTDCV1( RawBank const&        rawdata,
                                                                                MuonDAQHelper const&  daqInfo,
                                                                                RawBankReadoutStatus& status ) const {
  IMuonRawBuffer::TileAndADCVector storage;

  unsigned int tell1Number = rawdata.sourceID();
  if ( checkBankSize( rawdata, status ).isFailure() ) {
    ++m_hit_checkSize[tell1Number];
    return storage;
  }

  bool print_bank       = false;
  bool muon_spec_header = true;

  // printout
  if ( print_bank ) {
    info() << "Tell1 " << tell1Number << " "
           << "bank length " << rawdata.size() << endmsg;
    std::for_each( rawdata.begin<unsigned short>(), rawdata.end<unsigned short>(),
                   [&, count_word = 0]( unsigned short i ) mutable {
                     info() << count_word++ << ' ' << ( i & ( 0xFFF ) ) << ' ' << ( ( i >> 12 ) & ( 0xF ) ) << endmsg;
                   } );
  }
  const unsigned short* it   = rawdata.begin<unsigned short>();
  short                 skip = 0;

  ++m_processed_bank[tell1Number];

  if ( muon_spec_header ) {
    // how many pads ?
    unsigned short nPads = *it;
    skip                 = ( nPads + 3 ) / 2;
    for ( int k = 0; k < 2 * skip; k++ ) {
      if ( k == 1 ) checkTell1Header( tell1Number, *it );
      it++;
    }
  }

  std::array<unsigned int, 24> hit_link_cnt = {{0}};

  for ( int i = 0; i < 4; i++ ) {
    // now go to the single pp counter
    unsigned int pp_cnt = *it++;
    for ( unsigned int loop = 0; loop < pp_cnt; ++loop ) {
      unsigned int add       = ( *it ) & ( 0x0FFF );
      unsigned int tdc_value = ( ( ( *it ) & ( 0xF000 ) ) >> 12 );
      ++it;
      Detector::Muon::TileID tile = daqInfo.getADDInTell1( tell1Number, add );
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " add " << add << ' ' << tile << endmsg;
      if ( tile.isValid() ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << " valid  add " << add << ' ' << tile << endmsg;
        storage.emplace_back( tile, tdc_value );
      } else {
        ++m_counter_invalid_hit[tell1Number];
      }
      // update the hitillink counter
      unsigned int link = add / 192;
      hit_link_cnt[link]++;
    }
  }
  return storage;
}

LHCb::IMuonRawBuffer::TileAndADCVector LHCb::MuonRawBuffer::getTileAndTDC( LHCb::RawBank::View const& raw,
                                                                           MuonDAQHelper const&       daqInfo ) const {
  TileAndADCVector     storage;
  RawBankReadoutStatus status{RawBank::Muon};
  StatusCode           sc = checkAllHeaders( raw, status );
  if ( !sc.isFailure() ) {
    // first decode data and insert in buffer
    for ( const auto& r : raw ) {
      auto st = DecodeData( *r, daqInfo, status );
      std::copy( st.begin(), st.end(), std::back_inserter( storage ) );
    }
  }
  putStatusOnTES( status );
  return storage;
}

LHCb::IMuonRawBuffer::TileAndADCVector LHCb::MuonRawBuffer::DecodeData( LHCb::RawBank const&  rawdata,
                                                                        MuonDAQHelper const&  daqInfo,
                                                                        RawBankReadoutStatus& status ) const {
  unsigned int tell1Number = rawdata.sourceID();
  if ( tell1Number >= MuonDAQHelper::maxTell1Number ) {
    throw GaudiException( fmt::format( "In muon data a Tell1 Source ID is greater than maximum : {} > {}", tell1Number,
                                       MuonDAQHelper::maxTell1Number ),
                          "MuonRawBuffer::DecodeData", StatusCode::FAILURE );
  }
  switch ( rawdata.version() ) {
  case MuonBankVersion::DC06:
    return decodeTileAndTDCDC06( rawdata, daqInfo );
  case MuonBankVersion::v1:
    return decodeTileAndTDCV1( rawdata, daqInfo, status );
  default:
    throw GaudiException( fmt::format( "Unsupported RawBank version {}", rawdata.version() ),
                          "MuonRawBuffer::DecodeData", StatusCode::FAILURE );
  }
}

StatusCode LHCb::MuonRawBuffer::checkBankSize( LHCb::RawBank const& rawdata, RawBankReadoutStatus& status ) const {
  if ( RawBank::MagicPattern != rawdata.magic() ) {
    error() << "magic pattern not correct in muon bank " << endmsg;
    return StatusCode::FAILURE;
  }

  const unsigned short* it          = rawdata.begin<unsigned short>();
  unsigned int          tell1Number = rawdata.sourceID();
  int                   bank_size   = rawdata.size();
  int                   read_data   = 0;
  // minimum length is 3 words --> 12 bytes
  if ( bank_size < 12 ) {
    err() << " muon bank " << tell1Number << " is too short " << bank_size << endmsg;
    status.addStatus( tell1Number, RawBankReadoutStatus::Status::Incomplete );
    return StatusCode::FAILURE;
  }
  // how many pads ?
  unsigned short nPads = *it;
  int            skip  = ( nPads + 3 ) / 2;

  if ( ( bank_size - skip * 4 ) < 0 ) {
    err() << "bank_size " << bank_size << " pad size to read " << nPads * 4 << endmsg;
    err() << "so muon bank " << tell1Number << " is too short in pad part " << endmsg;
    status.addStatus( tell1Number, RawBankReadoutStatus::Status::Incomplete );
    return StatusCode::FAILURE;
  }

  it += 2 * skip;
  read_data = read_data + skip * 2;
  if ( read_data < 0 ) info() << nPads << " " << skip << " " << bank_size << " " << read_data << endmsg;

  for ( int i = 0; i < 4; i++ ) {
    // now go to the single pp counter
    int pp_cnt = *it++;
    read_data++;

    // check size before start looping
    if ( bank_size - read_data * 2 < pp_cnt * 2 ) {
      err() << "bank_size " << bank_size << "read data " << read_data << " hit size to read " << pp_cnt * 2 << endmsg;
      err() << "so muon bank " << tell1Number << " is too short in hit part " << endmsg;
      status.addStatus( tell1Number, RawBankReadoutStatus::Status::Incomplete );
      return StatusCode::FAILURE;
    }
    it += pp_cnt;
  }

  return StatusCode::SUCCESS;
}

StatusCode LHCb::MuonRawBuffer::checkAllHeaders( LHCb::RawBank::View const& b, RawBankReadoutStatus& status ) const {
  if ( b.empty() ) {
    for ( int i = 0; i < static_cast<int>( MuonDAQHelper::maxTell1Number ); i++ ) {
      status.addStatus( i, RawBankReadoutStatus::Status::Missing );
    }
    return StatusCode::SUCCESS;
  }
  // first decode data and insert in buffer
  bool foundError = false;

  std::vector<unsigned int> tell1InEvent;
  for ( const auto& mb : b ) {
    if ( mb->type() != RawBank::Muon )
      throw GaudiException( "Wrong RawBank::Type", __PRETTY_FUNCTION__, StatusCode::FAILURE );

    unsigned int tell1Number = mb->sourceID();
    status.addStatus( tell1Number, RawBankReadoutStatus::Status::OK );

    if ( mb->size() == 0 ) status.addStatus( tell1Number, RawBankReadoutStatus::Status::Empty );
    auto iList = std::find( tell1InEvent.begin(), tell1InEvent.end(), tell1Number );
    if ( iList != tell1InEvent.end() ) {
      foundError = true;
      status.addStatus( tell1Number, RawBankReadoutStatus::Status::NonUnique );
      break;
    }
    tell1InEvent.push_back( tell1Number );
  }

  // set missing bank readout status
  for ( int i = 0; i < static_cast<int>( MuonDAQHelper::maxTell1Number ); i++ ) {
    if ( status.status( i ) == RawBankReadoutStatus::Status::Unknown ) {
      status.addStatus( i, RawBankReadoutStatus::Status::Missing );
    }
  }
  if ( foundError ) { return StatusCode::FAILURE; }

  // there is repeated Tell1
  // now check the fw version

  // compact data  in one container
  auto ibad = std::find_if( b.begin(), b.end(), [ReferenceVersion = b[0]->version()]( const RawBank* rb ) {
    return rb->version() != ReferenceVersion;
  } );
  if ( ibad != b.end() ) {
    error() << " The muon Tell1 boards: not all the same version so  skip the event" << endmsg;
    status.addStatus( ( *ibad )->sourceID(), RawBankReadoutStatus::Status::Tell1Error );
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

void LHCb::MuonRawBuffer::checkTell1Header( unsigned int tell1Number, unsigned int data ) const {
  MuonTell1Header dataWord( data );
  if ( dataWord.getError() ) ++m_tell1_header_error[tell1Number];
  if ( dataWord.getOROfODEErrors() ) ++m_tell1_header_ORODE_error[tell1Number];
  if ( dataWord.getSYNCHDataErrorInODE() ) ++m_tell1_header_SYNCH_data_error[tell1Number];
  if ( dataWord.getSYNCHBCNCntErrorInODE() ) ++m_tell1_header_SYNCH_BC_error[tell1Number];
  if ( dataWord.getSYNCHEventCntErrorInODE() ) ++m_tell1_header_SYNCH_Evt_error[tell1Number];
}

void LHCb::MuonRawBuffer::putStatusOnTES( RawBankReadoutStatus const& newStatus ) const {
  RawBankReadoutStatuss* statusContainer =
      getOrCreate<RawBankReadoutStatuss, RawBankReadoutStatuss>( RawBankReadoutStatusLocation::Default );
  RawBankReadoutStatus* status = statusContainer->object( newStatus.key() );
  if ( !status ) {
    statusContainer->insert( new RawBankReadoutStatus( newStatus ) );
  } else if ( status->status() != newStatus.status() ) {
    Warning( fmt::format( "Status for bankType {} already exists  with different status value -> merge both",
                          toString( newStatus.key() ) ),
             StatusCode::SUCCESS )
        .ignore();
    for ( const auto& i : newStatus.statusMap() ) { status->addStatus( i.first, i.second ); }
  }
}
