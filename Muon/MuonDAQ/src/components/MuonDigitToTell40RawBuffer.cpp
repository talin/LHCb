/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonSynchFrame.h"

#include "Detector/Muon/TileID.h"
#include "MuonDAQ/MuonDAQDefinitions.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNODEBoard.h"
#include "MuonDet/MuonNamespace.h"
#include "MuonDet/MuonTell40Board.h"
#include "MuonDet/MuonTell40PCI.h"
#include "MuonDet/MuonUpgradeDAQHelper.h"
#include "MuonDet/MuonUpgradeStationCabling.h"
#include "MuonDet/MuonUpgradeTSMap.h"

#include "Event/MuonBankVersion.h"
#include "Event/MuonDigit.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"

#include "LHCbAlgs/Consumer.h"

#include <bitset>
#include <string>

namespace {
  // derived condition class
  struct MapTell40 {
    // contains "pointers" (indices) to places in a double array of SynchFrames
    // or nothing.
    // The mapping is only depending on the geometry and thus stored here in a
    // derived condition. The SynchFrames ar eevent dependent and thus stored
    // as local variable in oprator() of the algorithm

    using OptSyncFrameIndex = std::optional<std::pair<unsigned int, unsigned int>>;
    std::array<std::array<std::array<OptSyncFrameIndex, MuonUpgradeDAQHelper_linkNumber>,
                          MuonUpgradeDAQHelper_maxTell40PCINumber>,
               MuonUpgradeDAQHelper_maxTell40Number>
        synchFrames{}; // all optionals to false initially

    MapTell40() = default; // needed by DD4hep even if unused !
    MapTell40( DeMuonDetector const& muonDet ) {
      for ( unsigned int i = 0; i < MuonUpgradeDAQHelper_maxNODENumber; i++ ) {
        for ( unsigned int j = 0; j < MuonUpgradeDAQHelper_frameNumber; j++ ) {
          long int     Tell40Number{0};
          unsigned int Tell40PCINumber{0}, Tell40PCILinkNumber{0};
          muonDet.getUpgradeDAQInfo()->getSynchConnection( i + 1, j, Tell40Number, Tell40PCINumber,
                                                           Tell40PCILinkNumber );
          if ( Tell40Number > 0 ) { synchFrames[Tell40Number - 1][Tell40PCINumber][Tell40PCILinkNumber] = {i, j}; }
        }
      }
    }
  };
} // namespace

/**
 *  FIXME : This algo has a const_cast allowing to modify the input RawEvent and add a bank to it.
 *  This should be done another way in the funcitonal world
 *
 *  @author Alessia Satta
 *  @date   2020-05-11
 */
using namespace Muon::DAQ;

class MuonDigitToTell40RawBuffer
    : public LHCb::Algorithm::Consumer<void( LHCb::RawEvent const&, LHCb::MuonDigits const&, DeMuonDetector const&,
                                             MapTell40 const& ),
                                       LHCb::DetDesc::usesConditions<DeMuonDetector, MapTell40>> {
public:
  MuonDigitToTell40RawBuffer( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  {{"RawEvent", LHCb::RawEventLocation::Default},
                   {"Digits", LHCb::MuonDigitLocation::MuonDigit},
                   {"DeMuonLocation", DeMuonLocation::Default},
                   {"MapTell40Location", "MuonSynchFrame_Tell40Number"}} ) {}

  StatusCode initialize() override;
  void       operator()( LHCb::RawEvent const&, LHCb::MuonDigits const&, DeMuonDetector const&,
                   MapTell40 const& ) const override;

private:
  void ProcessSynchFrameV2( MuonSynchFrame& frame, unsigned int& frame_leght_byte,
                            unsigned int ( &out_frame )[4] ) const;

  Gaudi::Property<unsigned int> m_vtype{this, "VType", 2};
};

DECLARE_COMPONENT( MuonDigitToTell40RawBuffer )

using namespace LHCb;

StatusCode MuonDigitToTell40RawBuffer::initialize() {
  return Consumer::initialize().andThen( [&] {
    addConditionDerivation( {DeMuonLocation::Default}, this->template inputLocation<MapTell40>(),
                            []( DeMuonDetector const& det ) -> MapTell40 { return {det}; } );
    return StatusCode::SUCCESS;
  } );
}

void MuonDigitToTell40RawBuffer::operator()( LHCb::RawEvent const& raw, LHCb::MuonDigits const& digits,
                                             DeMuonDetector const& muonDet, MapTell40 const& mapTell40 ) const {
  std::array<std::array<MuonSynchFrame, MuonUpgradeDAQHelper_frameNumber>, MuonUpgradeDAQHelper_maxNODENumber> synch{};
  unsigned int bankVersion = MuonBankVersion::Run3_v1;

  // Fill digit in frame
  if ( msgLevel( MSG::DEBUG ) ) { debug() << " The number of  hits in input container is " << digits.size() << endmsg; }
  for ( const auto& idigit : digits ) {

    LHCb::Detector::Muon::TileID digitTile = idigit->key();
    // the time is in 4 bits coding
    unsigned int time = idigit->TimeStamp();

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Processing V2 digit: " << digitTile.toString() << " time " << time << endmsg;
      debug() << "Processing V2 digit: " << digitTile << " time " << time << endmsg;
    }

    unsigned int ODENumber      = 0;
    unsigned int frame          = 0;
    unsigned int channelInFrame = 0;
    muonDet.getUpgradeDAQInfo()->DAQaddressInODE( digitTile, ODENumber, frame, channelInFrame );
    if ( msgLevel( MSG::DEBUG ) ) debug() << ODENumber << " " << frame << " " << channelInFrame << endmsg;
    // fil frame
    synch[ODENumber - 1][frame].setHitAndTime( channelInFrame, time );

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "ODENumber frame channelsInFrame " << ODENumber << " " << frame << " " << channelInFrame << " pointer "
              << &( synch[ODENumber - 1][frame] ) << endmsg;
  }

  for ( unsigned int i = 0; i < MuonUpgradeDAQHelper_maxTell40Number; i++ ) {
    for ( unsigned int j = 0; j < MuonUpgradeDAQHelper_maxTell40PCINumber; j++ ) {
      unsigned int          bank_size_words = 0;
      unsigned int          bank_size_bytes = 0;
      std::vector<ByteType> bank;
      ByteType              data = 0;
      data                       = ( 0x5 );
      bank.emplace_back( data );
      if ( msgLevel( MSG::DEBUG ) ) debug() << " started new bank with data  = " << std::bitset<8>( data ) << endmsg;
      data = 0;
      bank_size_bytes++;
      for ( unsigned int m = 0; m < MuonUpgradeDAQHelper_linkNumber; m++ ) {
        unsigned int out_frame[4]      = {0, 0, 0, 0};
        unsigned int frame_length_byte = 0;
        if ( !mapTell40.synchFrames[i][j][m] ) continue;
        auto& [a, b] = *mapTell40.synchFrames[i][j][m];
        debug() << " process Tell40 PCI synch " << i << " " << j << " " << m << " " << a << " " << b << endmsg;
        ProcessSynchFrameV2( synch[a][b], frame_length_byte, out_frame );
        unsigned int jbyte = 0;
        // insert first byte
        if ( msgLevel( MSG::DEBUG ) ) debug() << " synch frame length in byte " << frame_length_byte << endmsg;

        for ( jbyte = 0; jbyte < frame_length_byte + 1; jbyte++ ) {
          bank_size_bytes++;

          unsigned int data_frame        = jbyte / 4;
          unsigned int data_pos_in_input = 3 - jbyte % 4;
          unsigned int tmp               = out_frame[data_frame];
          if ( msgLevel( MSG::DEBUG ) )
            debug() << " synch frame  and byte " << jbyte << " = " << std::bitset<32>( tmp ) << endmsg;
          // fill staring from MSB
          data |= ( ( tmp >> data_pos_in_input * 8 ) & 0xFF );
          if ( msgLevel( MSG::DEBUG ) )
            debug() << " data after byte insertion  " << jbyte << " " << std::bitset<8>( data ) << endmsg;
          bank.emplace_back( data );
          data = 0;
          bank_size_words++;
          if ( msgLevel( MSG::DEBUG ) ) debug() << "data stored " << bank_size_words << endmsg;
        }
      }

      // unsigned int PCINumber = i * 2 + j;
      auto sourceID = muonDet.getUpgradeDAQInfo()->getSourceID( i + 1, j );
      if ( msgLevel( MSG::DEBUG ) ) debug() << " source ID " << i + 1 << " " << j << " " << sourceID << endmsg;

      // alessia -- Not functional ! One should not modify RawEvent
      const_cast<LHCb::RawEvent&>( raw ).addBank( sourceID, LHCb::RawBank::Muon, bankVersion, bank );

      unsigned int bankwrittem = bank_size_bytes / 4;
      if ( bank_size_bytes % 4 ) bankwrittem++;
      debug() << "check bank size  i.e. bank size " << bank.size() << " " << bank_size_bytes << endmsg;
    }
  }
}

void MuonDigitToTell40RawBuffer::ProcessSynchFrameV2( MuonSynchFrame& frame, unsigned int& frame_leght_byte,
                                                      ShortType ( &out_frame )[4] ) const {
  // remember the definition ShortTuype std::uint32_t
  bool      edac           = false;
  ShortType synch_frame[3] = {0, 0, 0};
  ShortType hits           = frame.getSynchFrame( synch_frame, edac );

  if ( msgLevel( MSG::DEBUG ) ) {
    if ( hits > 12 ) debug() << "TDC trunctation " << hits << endmsg;
    debug() << " synch 1 = " << std::bitset<32>( synch_frame[0] ) << endmsg;
    debug() << " synch 2 = " << std::bitset<32>( synch_frame[1] ) << endmsg;
    debug() << " synch 3 = " << std::bitset<32>( synch_frame[2] ) << endmsg;
    debug() << " formato naturale synch = " << std::bitset<32>( synch_frame[0] );
    debug() << std::bitset<16>( synch_frame[1] >> 16 ) << endmsg;
    debug() << " synch TDC              = " << std::bitset<16>( synch_frame[1] );
    debug() << std::bitset<32>( synch_frame[2] ) << endmsg;

    debug() << " hits nel frame " << hits << endmsg;
  }
  out_frame[0] = 0;
  out_frame[1] = 0;
  out_frame[2] = 0;
  out_frame[3] = 0;

  // unsigned int
  frame_leght_byte = 0;
  if ( hits == 0 ) {
    frame_leght_byte = 1;

  } else if ( hits == 1 ) {
    frame_leght_byte = 7;
  } else if ( hits == 2 || hits == 3 ) {
    frame_leght_byte = 8;
  } else if ( hits == 4 || hits == 5 ) {
    frame_leght_byte = 9;
  } else if ( hits == 6 || hits == 7 ) {
    frame_leght_byte = 10;
  } else if ( hits == 8 || hits == 9 ) {
    frame_leght_byte = 11;
  } else if ( hits == 10 || hits == 11 ) {
    frame_leght_byte = 12;
  } else if ( hits > 11 ) {
    frame_leght_byte = 13;
  }
  // reduce two TDC
  if ( edac && hits > 10 ) { frame_leght_byte = 12; }
  // reduce by 1 to follow FW implementation
  frame_leght_byte = frame_leght_byte - 1;

  // fill frame length in output frame
  // use frame start at MSB
  out_frame[0] = ( frame_leght_byte & 0xF ) << 28;
  if ( msgLevel( MSG::DEBUG ) ) debug() << " frame after length = " << std::bitset<32>( out_frame[0] ) << endmsg;
  // copy hitsmap is non empty

  if ( hits > 0 ) {
    // out_frame[0] |= (synch_frame[0] & 0xFFFFFFF0) <<4;
    out_frame[0] |= ( synch_frame[0] >> 4 );
    if ( msgLevel( MSG::DEBUG ) ) debug() << " first frame = " << std::bitset<32>( out_frame[0] ) << endmsg;

    out_frame[1] = ( synch_frame[0] & 0xF ) << 28;
    out_frame[1] |= ( synch_frame[1] >> 4 );
    if ( msgLevel( MSG::DEBUG ) ) debug() << " second frame = " << std::bitset<32>( out_frame[1] ) << endmsg;
    out_frame[2] = ( synch_frame[1] & 0xF ) << 28;
    out_frame[2] |= ( synch_frame[2] >> 4 );
    if ( msgLevel( MSG::DEBUG ) ) debug() << " third frame = " << std::bitset<32>( out_frame[2] ) << endmsg;
    out_frame[3] = ( synch_frame[2] & 0xF ) << 28;
  }
}
