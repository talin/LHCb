/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/MuonCoord.h"
#include "MuonDAQ/IMuonRawBuffer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"

#include "Detector/Muon/Layout.h"

#include "LHCbAlgs/Transformer.h"

#include <string>

namespace LHCb {

  using MuonLayout = Detector::Muon::Layout;

  /** @class MuonRec MuonRec.h
   *  This is the muon reconstruction algorithm
   *  This just crosses the logical strips back into pads
   *
   *  @author David Hutchcroft
   *  @date   22/03/2002
   */
  class MuonRec : public Algorithm::Transformer<MuonCoords( RawBank::View const&, DeMuonDetector const& ),
                                                DetDesc::usesConditions<DeMuonDetector>> {
  public:
    MuonRec( const std::string& name, ISvcLocator* pSvcLocator );
    MuonCoords operator()( RawBank::View const&, DeMuonDetector const& ) const override;

  private:
    /// Copy Detector::Muon::TileID from digits to coords with no logical map (1:1 copy)
    void addCoordsNoMap( MuonCoords& coords, const std::vector<std::pair<Detector::Muon::TileID, unsigned int>>& digit,
                         int station, int region ) const;

    /// Copy Detector::Muon::TileID from digits to coord by crossing the digits
    void addCoordsCrossingMap( MuonCoords&                                                         coords,
                               const std::vector<std::pair<Detector::Muon::TileID, unsigned int>>& digit, int station,
                               int region, DeMuonDetector const& ) const;

    /// fills in the two readout layouts by querying the DeMuonRegion
    std::pair<MuonLayout, MuonLayout> makeStripLayouts( int station, int region, DeMuonDetector const& ) const;

    ToolHandle<IMuonRawBuffer> m_muonBuffer{this, "MuonRawBuffer", "MuonRawBuffer"};
    /// Count errors occuring during reconstruction (duplicated coords)
    mutable Gaudi::Accumulators::Counter<> m_Exccounter{this, "ExcCounter"};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::MuonRec, "MuonRec" )

LHCb::MuonRec::MuonRec( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, {{"RawBanks", {}}, {"MuonDetectorLocation", DeMuonLocation::Default}},
                   {"OutputLocation", MuonCoordLocation::MuonCoords} ) {}

LHCb::MuonCoords LHCb::MuonRec::operator()( const LHCb::RawBank::View& rawBanks,
                                            DeMuonDetector const&      muonDetector ) const {
  // need to loop over input vector of MuonDigits
  // and make output vectors of MuonCoords one for each station
  auto decoding = m_muonBuffer->getTileAndTDC( rawBanks, *muonDetector.getDAQInfo() );

  if ( msgLevel( MSG::DEBUG ) ) debug() << decoding.size() << " digits in input " << endmsg;

  MuonCoords coords;
  for ( int station = 0; station < muonDetector.stations(); station++ ) {
    for ( int region = 0; region < muonDetector.regions(); region++ ) {

      // get mapping of input to output from region
      // in fact we are reversing the conversion done in the digitisation
      int NLogicalMap = muonDetector.getLogMapInRegion( station, region );
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " station and region " << station << " " << region << " maps " << NLogicalMap << endmsg;

      if ( 1 == NLogicalMap ) {
        // straight copy of the input + making SmartRefs to the MuonDigits
        addCoordsNoMap( coords, decoding, station, region );
      } else {
        // need to cross the input strips to get output strips
        addCoordsCrossingMap( coords, decoding, station, region, muonDetector );
      }
    }
  }
  return coords;
}

// Adding entries to coords 1 to 1 from digits, need to make the references
void LHCb::MuonRec::addCoordsNoMap( LHCb::MuonCoords&                                                         coords,
                                    const std::vector<std::pair<LHCb::Detector::Muon::TileID, unsigned int>>& digits,
                                    int station, int region ) const {
  for ( auto iD = digits.begin(); iD != digits.end(); iD++ ) {
    if ( ( iD->first ).station() == static_cast<unsigned int>( station ) &&
         ( iD->first ).region() == static_cast<unsigned int>( region ) ) {
      // make the coordinate to be added to coords
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " digit tile " << iD->first << endmsg;

      auto current = std::make_unique<MuonCoord>( iD->first, iD->second );

      // need to clear the layer and readout bits
      Detector::Muon::TileID pad = iD->first;
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << pad << endmsg;

      // as no change between digit and coord in this mapping key is the same
      try {
        coords.insert( current.get(), pad );
        current.release();
      } catch ( const GaudiException& exc ) {

        error() << "The error is caused by the duplication of  pad " << pad << endmsg;
        error() << "It is likely due to data corruption " << endmsg;
        ++m_Exccounter;
      }
    }
  }
}

void LHCb::MuonRec::addCoordsCrossingMap(
    LHCb::MuonCoords& coords, const std::vector<std::pair<LHCb::Detector::Muon::TileID, unsigned int>>& digits,
    int station, int region, DeMuonDetector const& muonDet ) const {
  // get local MuonLayouts for strips
  auto [layoutOne, layoutTwo] = makeStripLayouts( station, region, muonDet );

  // seperate the two types of logical channel, flag if used with the pair
  std::vector<std::pair<std::pair<Detector::Muon::TileID, unsigned int>, bool>> typeOnes;
  std::vector<std::pair<std::pair<Detector::Muon::TileID, unsigned int>, bool>> typeTwos;
  for ( auto it = digits.begin(); it != digits.end(); it++ ) {
    if ( it->first.station() == static_cast<unsigned int>( station ) &&
         it->first.region() == static_cast<unsigned int>( region ) ) {
      if ( it->first.layout() == layoutOne ) {
        typeOnes.emplace_back( *it, false );
      } else if ( it->first.layout() == layoutTwo ) {
        typeTwos.emplace_back( *it, false );
      } else {
        error() << "MuonDigits in list are not compatable with expected shapes" << *it << endmsg;
      }
    }
  }
  // now cross the two sets of channels
  // sorry about this std::pair stuff but it is the easiest way of matching
  // a bool to each digit
  for ( auto iOne = typeOnes.begin(); iOne != typeOnes.end(); iOne++ ) {
    for ( auto iTwo = typeTwos.begin(); iTwo != typeTwos.end(); iTwo++ ) {
      // who said C++ did not make lovely transparent code?
      Detector::Muon::TileID pad = iOne->first.first.intercept( iTwo->first.first );
      if ( pad.isValid() ) {
        // have a pad to write out
        // make the coordinate to be added to coords
        auto current = std::make_unique<MuonCoord>( pad, iOne->first.first, iTwo->first.first, iOne->first.second,
                                                    iTwo->first.second );

        // as no change between digit and coord in this mapping key is the same

        try {
          coords.insert( current.get() );
          current.release();
        } catch ( const GaudiException& exc ) {

          error() << "The error is caused by the duplication of  pad " << pad << endmsg;
          error() << "It is likely due to data corruption " << endmsg;
          ++m_Exccounter;
        }
        // set flags to used on iOne and iTwo
        iOne->second = true;
        iTwo->second = true;

        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << " Made a crossed pad " << pad << " from " << ( iOne->first ) << " and " << ( iTwo->first )
                    << endmsg;
      }
    }
  }

  // now copy across directly all strips that have not yet been used
  for ( auto iOne = typeOnes.begin(); iOne != typeOnes.end(); iOne++ ) {
    if ( !( iOne->second ) ) {
      // no crossing map here
      Detector::Muon::TileID pad = ( iOne->first ).first;

      // make the coordinate to be added to coords
      auto current = std::make_unique<MuonCoord>( iOne->first.first, iOne->first.second );

      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " Found an uncrossed pad type 1 " << pad << endmsg;

      try {
        coords.insert( current.get() );
        current.release();
      } catch ( const GaudiException& exc ) {

        error() << "The error is caused by the duplication of  pad " << pad << endmsg;
        error() << "It is likely due to data corruption " << endmsg;
        ++m_Exccounter;
      }
    }
  }

  for ( auto iTwo = typeTwos.begin(); iTwo != typeTwos.end(); iTwo++ ) {
    if ( !( iTwo->second ) ) {
      // no crossing map here
      Detector::Muon::TileID pad = iTwo->first.first;

      // make the coordinate to be added to coords
      auto current = std::make_unique<MuonCoord>( iTwo->first.first, iTwo->first.second );

      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " Found an uncrossed pad type 2 " << pad << endmsg;
      try {
        coords.insert( current.get() );
        current.release();
      } catch ( const GaudiException& exc ) {
        error() << "The error is caused by the duplication of  pad " << pad << endmsg;
        error() << "It is likely due to data corruption " << endmsg;
        ++m_Exccounter;
      }
    }
  }
}

std::pair<LHCb::MuonLayout, LHCb::MuonLayout>
LHCb::MuonRec::makeStripLayouts( int station, int region, DeMuonDetector const& muonDetector ) const {
  unsigned int x1 = muonDetector.getLayoutX( 0, station, region );
  unsigned int y1 = muonDetector.getLayoutY( 0, station, region );
  unsigned int x2 = muonDetector.getLayoutX( 1, station, region );
  unsigned int y2 = muonDetector.getLayoutY( 1, station, region );
  return {{x1, y1}, {x2, y2}};
}
