/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "MuonDAQ/MuonDAQDefinitions.h"

#include <algorithm>
#include <array>

/**
 *  @author Alessia Satta
 *  @date   2020-05-06
 */
using namespace Muon::DAQ;

namespace MuonSynchFrameMask {
  constexpr ShortType MaskTime = ( ( ( (unsigned int)1 ) << 4 ) - 1 );
} // namespace MuonSynchFrameMask

class MuonSynchFrame final {
public:
  inline void setHit( unsigned int pos, bool flag ) {
    m_hitmap[pos] = flag;
    m_isEmpty     = false;
  };

  inline void setTime( unsigned int pos, unsigned int num ) {
    m_TDCdata[pos] = (num)&MuonSynchFrameMask::MaskTime;
    m_isEmpty      = false;
  };
  void                setHitAndTime( unsigned int pos, unsigned int num );
  inline unsigned int getTime( unsigned int pos ) const { return m_TDCdata[pos]; };

  inline bool hasFiredHit( unsigned int pos ) const { return m_hitmap[pos]; };

  inline bool getHitTime( unsigned int pos ) const { return m_TDCdata[pos]; };
  inline bool isEmpty() const { return m_isEmpty; };

  void      getFullSynchFrame( ShortType frame[8] );
  ShortType getSynchFrame( ShortType frame[3], bool edac );

private:
  // to get the 240 length of 48 hits map + 48*4 TDC max info
  std::array<bool, 48>      m_hitmap{};  // all elements are set to false
  std::array<ShortType, 48> m_TDCdata{}; // all elements are set to 0
  bool                      m_isEmpty{true};
};
