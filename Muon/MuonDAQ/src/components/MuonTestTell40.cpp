/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PrHits.h"
#include "MuonDet/DeMuonDetector.h"

#include "LHCbAlgs/Consumer.h"

/**
 *  @author Eric Cogneras, Luca Pescatore
 *  @date   2012-07-05
 */
struct MuonTestTell40 : LHCb::Algorithm::Consumer<void( const MuonHitContainer& )> {
  MuonTestTell40( const std::string& name, ISvcLocator* pSvcLocator );

  void operator()( const MuonHitContainer& hits ) const override;
};

DECLARE_COMPONENT( MuonTestTell40 )

MuonTestTell40::MuonTestTell40( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, KeyValue{"HitContainer", MuonHitContainerLocation::Default} ) {}

void MuonTestTell40::operator()( const MuonHitContainer& hits ) const {
  // Count signal, noise and spillover digits
  for ( auto s = 0; s < 4; s++ ) {
    auto sta_hits = hits.station( s ).hits();
    for ( auto h = sta_hits.begin(); h < sta_hits.end(); h++ ) { info() << "muon test " << h->tile() << endmsg; }
  }

  info() << "size of container 0 " << ( hits.station( 0 ) ).hits().size() << endmsg << "size of container 1 "
         << ( hits.station( 1 ) ).hits().size() << endmsg << "size of container 2 "
         << ( hits.station( 2 ) ).hits().size() << endmsg << "size of container 3 "
         << ( hits.station( 3 ) ).hits().size() << endmsg;
}
