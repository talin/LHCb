/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <cstdint>

/*
 *  @author Alessia Satta
 *  @date   2020-05-23
 */
namespace Muon::DAQ {

  using LongType = std::uint64_t;

  /// DAQ short type definition
  using ShortType = std::uint32_t;
  using ByteType  = uint8_t;

} // namespace Muon::DAQ
