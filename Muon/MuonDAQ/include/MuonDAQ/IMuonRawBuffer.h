/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/Muon/TileID.h"

#include "Event/MuonODEData.h"
#include "Event/MuonPPEventInfo.h"
#include "Event/MuonTell1Header.h"
#include "Event/RawBank.h"
#include "Event/RawBankReadoutStatus.h"
#include "GaudiKernel/IAlgTool.h"
#include "MuonDet/MuonDAQHelper.h"

#include <utility>
#include <vector>

namespace LHCb {
  /**
   *  Interface for the tools to convert Detector::Muon::TileID to coordinates
   *
   *  @author David Hutchcroft
   *  @date   11/03/2002
   */
  struct IMuonRawBuffer : extend_interfaces<IAlgTool> {

    /** static interface identification
     *  @return unique interface identifier
     */
    DeclareInterfaceID( IMuonRawBuffer, 5, 0 );

    using TileAndADCVector = std::vector<std::pair<Detector::Muon::TileID, unsigned int>>;

    virtual TileAndADCVector getTileAndTDC( RawBank::View const& raw, MuonDAQHelper const& ) const = 0;
  };
} // namespace LHCb
