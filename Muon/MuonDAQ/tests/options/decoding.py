###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (LHCbApp, HLTControlFlowMgr, createODIN,
                           HiveDataBrokerSvc, HiveWhiteBoard)
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile
from Configurables import LHCb__UnpackRawEvent as UnpackRawEvent
from Configurables import MuonRawInUpgradeToHits
from PRConfig.TestFileDB import test_file_db
from Gaudi.Configuration import ERROR
from DDDB.CheckDD4Hep import UseDD4Hep

app = LHCbApp()

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    iovProd = IOVProducer("ReserveIOVDD4hep", ODIN='DAQ/ODIN')
    DD4hepSvc(DetectorList=["/world", "Muon"])
    # No up-to-date tags for the new yaml DB yet so just use master
    app.CondDBtag = "master"
else:
    from Configurables import UpdateManagerSvc
    from Configurables import LHCb__DetDesc__ReserveDetDescForEvent as IOVProducer
    UpdateManagerSvc(WithoutBeginEvent=True)
    iovProd = IOVProducer()

app.EvtMax = 100
app.EnableHive = True
app.Scheduler = "HLTControlFlowMgr"
app.ThreadPoolSize = 1

HiveWhiteBoard("EventDataSvc", EventSlots=1)

HiveDataBrokerSvc(
    OutputLevel=ERROR,
    DataProducers=[
        FetchDataFromFile('ReadRawEvent', DataKeys=['/Event/DAQ/RawEvent']),
        UnpackRawEvent(
            'UnpackRawEvent',
            BankTypes=['ODIN'],
            RawEventLocation='/Event/DAQ/RawEvent',
            RawBankLocations=['/Event/DAQ/RawBanks/ODIN']),
        createODIN(RawBanks='DAQ/RawBanks/ODIN'),
        UnpackRawEvent(
            'UnpackRawEventMuon',
            BankTypes=['Muon'],
            RawEventLocation='/Event/DAQ/RawEvent',
            RawBankLocations=['/Event/DAQ/RawBanks/Muon']),
        iovProd,
    ])
HLTControlFlowMgr(
    MemoryPoolSize=512 * 1024,  # 512 KiB
    CompositeCFNodes=[('muon_decoding', 'LAZY_AND', ['MuonRawInUpgradeToHits'],
                       True)])

test_file_db['upgrade_Sept2022_minbias_0fb_md_xdigi'].run(configurable=app)
