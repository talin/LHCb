###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Muon/MuonDAQ
------------
#]=======================================================================]

gaudi_add_library(MuonDAQLib
    SOURCES
        src/MuonHLTDigitFormat.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            LHCb::DAQEventLib
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::MuonDetLib
)

gaudi_add_module(MuonDAQ
    SOURCES
        src/components/MuonDigitToRawBuffer.cpp
        src/components/MuonDigitToTell40RawBuffer.cpp
        src/components/MuonRawBuffer.cpp
        src/components/MuonRawInUpgradeToHits.cpp
        src/components/MuonRawToCoord.cpp
        src/components/MuonRawToHits.cpp
        src/components/MuonRec.cpp
        src/components/MuonSynchFrame.cpp
        src/components/MuonTestTell40.cpp
        src/components/MuonTell40Decoding.cpp
    LINK
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::DAQEventLib
        LHCb::DAQKernelLib
        LHCb::DAQUtilsLib
        LHCb::DetDescLib
        LHCb::DigiEvent
        LHCb::LHCbKernel
        LHCb::LHCbAlgsLib
        LHCb::LHCbMathLib
        LHCb::MuonDAQLib
        LHCb::MuonDetLib
        LHCb::RecEvent
        Rangev3::rangev3
)

gaudi_add_dictionary(MuonDAQDict
    HEADERFILES dict/MuonDAQDict.h
    SELECTION dict/MuonDAQDict.xml
    LINK LHCb::MuonDAQLib
)

if (NOT USE_DD4HEP)
  gaudi_add_tests(QMTest)
endif()
