/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/MsgStream.h"

// LHCbKernel
#include "Kernel/RichRadIntersection.h"
#include "Kernel/RichRadiatorType.h"

// Math
#include "GaudiKernel/Transform3DTypes.h"

// Det Desc
#include "DetDesc/ConditionKey.h"

// Utils
#include "RichUtils/AllocateCount.h"

// Detectors
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich2.h"

// STL
#include <algorithm>
#include <array>
#include <vector>

namespace Rich::Utils {

  //-----------------------------------------------------------------------------
  /** @class RadIntersects RichRadIntersects.h
   *
   *  Utility to find intersections with the RICH radiators
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2019-11-29
   */
  //-----------------------------------------------------------------------------

  class RadIntersects final {

  public:
    // construtors

    /// From radiators
    RadIntersects( const Detector::Rich1& r1,                  // RICH1 gas volume
                   const Detector::Rich2& r2 )                 // RICH2 gas volume
        : m_radiators{nullptr, &r1.radiator(), &r2.radiator()} // Aero hardcoded to off...
    {}

  public:
    /** @brief Finds intersections of a given vector from a given point (entry/exit)
     *
     * @param globalPoint   The start point for the intersection extraplotion
     * @param globalVector  The direction vector for the intersection extraplotion
     * @param radiator      The radiator to find the intersections in
     * @param intersections The found intersections
     *
     * @return The number of intersections
     */
    inline unsigned int intersections( const Gaudi::XYZPoint&         globalPoint,  //
                                       const Gaudi::XYZVector&        globalVector, //
                                       const Rich::RadiatorType       radiator,     //
                                       Rich::RadIntersection::Vector& intersections ) const {

      // clear any current intersections
      intersections.clear();

      // Try intersection with the correct volume
      auto R = m_radiators[radiator];
      if ( R ) {
        Gaudi::XYZPoint entry, exit;
        if ( R->intersectionPoints( globalPoint, globalVector, entry, exit ) ) {
          // save this intersection
          intersections.emplace_back( entry, globalVector, exit, globalVector, R );
        }
      }

      // return the number of intersections
      return intersections.size();
    }

  private:
    /// Radiators for each RICH
    RadiatorArray<const Detector::Radiator*> m_radiators = {{}};

  public:
    // conditions handling

    /// name string
    static constexpr auto MyName() noexcept { return "Rich::Utils::RadIntersects"; }

    /// Default conditions name
    inline static const std::string DefaultConditionKey =
        DeRichLocations::derivedCondition( "RadiatorIntersects-Handler" );

    /// Creates a condition derivation
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent ) {
      // Assume parent algorithm has one and only one input of the correct type...
      return addConditionDerivation( parent, parent->template inputLocation<RadIntersects>() );
    }

    /// static generator method
    static auto generate( const Detector::Rich1& r1, //
                          const Detector::Rich2& r2 ) {
      auto msgSvc = Gaudi::svcLocator()->service<IMessageSvc>( "MessageSvc" );
      assert( msgSvc );
      MsgStream log( msgSvc, MyName() );
      log << MSG::DEBUG << "Update triggered" << endmsg;
      return RadIntersects{r1, r2};
    }

    /// Creates a condition derivation for the given key
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key ) {
      if ( parent->msgLevel( MSG::DEBUG ) ) {
        parent->debug() << "RadIntersects::addConditionDerivation : Key=" << key << endmsg;
      }
      Detector::Rich1::addConditionDerivation( parent );
      Detector::Rich2::addConditionDerivation( parent );
      return parent->addSharedConditionDerivation( {Detector::Rich1::DefaultConditionKey,  //
                                                    Detector::Rich2::DefaultConditionKey}, // input conditions
                                                   std::move( key ),                       //
                                                   &generate );
    }

  private:
    /// Allocation tracking
    Rich::AllocateCount<RadIntersects> m_track_instances;
  };

} // namespace Rich::Utils
