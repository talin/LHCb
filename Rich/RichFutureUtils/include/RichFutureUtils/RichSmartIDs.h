/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <array>
#include <memory>

// RichDet
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich2.h"
#include "RichDetectors/RichPD.h"
#include "RichDetectors/RichPDInfo.h"

// Det Desc
#include "DetDesc/ConditionKey.h"
#include "DetDesc/IConditionDerivationMgr.h"

// Kernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSide.h"
#include "Kernel/RichSmartID.h"

// Utils
#include "RichUtils/AllocateCount.h"
#include "RichUtils/RichException.h"
#include "RichUtils/RichMap.h"
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichSIMDTypes.h"
#include "RichUtils/RichSmartIDSorter.h"

// Gaudi
#include "GaudiKernel/Point3DTypes.h"

namespace Rich::Utils {

  /** @class RichSmartIDs RichSmartIDs.h
   *
   *  Utility helper class for RichSmartIDs
   *
   *  @author Chris Jones
   *  @date   25/11/2019
   */
  class RichSmartIDs final : public LHCb::SIMD::AlignedBase<LHCb::SIMD::VectorAlignment> {

  public:
    // constructors

    /// Constructor from RICH detector elements
    RichSmartIDs( const Detector::PDInfo& pdInfo, //
                  const Detector::Rich1&  rich1,  //
                  const Detector::Rich2&  rich2 )
        : m_pdInfo{&pdInfo} //
        , m_riches{&rich1, &rich2} {}

  public:
    // types

    // detectors
    using PD = Detector::RichBase::PD; ///< PD Type to use

    // SIMD types
    using FP         = Rich::SIMD::DefaultScalarFP; ///< Default scalar floating point type
    using SIMDFP     = SIMD::FP<FP>;                ///< Default vector floating point type
    using SIMDVector = SIMD::Vector<FP>;            ///< Default vector Vector class
    using SIMDPoint  = SIMD::Point<FP>;             ///< Default vector Point class

  private:
    // methods

    // get the RICH for a given type
    [[nodiscard]] auto rich( const Rich::DetectorType rtype ) const {
      const auto dR = m_riches[rtype];
      assert( dR );
      return dR;
    }

    // get the RICH for a given SmartID
    [[nodiscard]] auto rich( const LHCb::RichSmartID ID ) const { return rich( ID.rich() ); }

    /// Get the PD panel for the given RICH and side
    [[nodiscard]] auto panel( const Rich::DetectorType rtype, const Rich::Side side ) const {
      const auto dP = rich( rtype )->pdPanel( side );
      assert( dP );
      return dP;
    }

    /// Get the PD panel for the given SmartID
    [[nodiscard]] auto panel( const LHCb::RichSmartID ID ) const { return panel( ID.rich(), ID.panel() ); }

    /// Get the position for a given SmartID and associated Rich PD object
    inline auto _globalPosition( const LHCb::RichSmartID smartID, //
                                 const PD*               dePD ) const {
      return dePD->detectionPoint( smartID );
    }

    /// Get the position for a given SmartID.
    inline auto _globalPosition( const LHCb::RichSmartID smartID ) const {
      return panel( smartID )->detectionPoint( smartID );
    }

  public:
    // methods

    // Converts a RichSmartID channel identification into a position in global LHCb coordinates.
    inline auto globalPosition( const LHCb::RichSmartID id ) const { return _globalPosition( id ); }

    // Finds the average position of a cluster of RichSmartIDs, in global LHCb coordinates
    Detector::PD::DetPtn globalPosition( const Rich::PDPixelCluster& cluster ) const;

    // Finds the average positions of a vector of clusters, in global LHCb coordinates
    [[nodiscard]] LHCb::STL::Vector<Gaudi::XYZPoint> globalPositions( const Rich::PDPixelCluster::Vector& clusters,
                                                                      const bool ignoreClusters = false ) const;

    // Converts an PD RichSmartID identification into a position in global LHCb coordinates.
    Gaudi::XYZPoint pdPosition( const LHCb::RichSmartID pdid ) const;

    // Computes the global position coordinate for a given position in local
    [[nodiscard]] auto globalPosition( const Gaudi::XYZPoint&   localPoint, //
                                       const Rich::DetectorType rich,       //
                                       const Rich::Side         side ) const {
      return panel( rich, side )->pdPanelToGlobal() * localPoint;
    }

    // Converts a position in global coordinates to the corresponding RichSmartID
    bool smartID( const Gaudi::XYZPoint& globalPoint, //
                  LHCb::RichSmartID&     smartid ) const;

    // Converts a position in global coordinates to the local coordinate system.
    [[nodiscard]] Gaudi::XYZPoint globalToPDPanel( const Gaudi::XYZPoint& globalPoint ) const;

    // Converts a SIMD position in global coordinates to the local coordinate system
    [[nodiscard]] SIMDPoint globalToPDPanel( const Rich::DetectorType rich, //
                                             const SIMDPoint&         globalPoint ) const;

    // Converts a SIMD position in global coordinates to the local coordinate system
    [[nodiscard]] auto globalToPDPanel( const Rich::DetectorType rich, //
                                        const Rich::Side         side, //
                                        const SIMDPoint&         globalPoint ) const {
      return panel( rich, side )->globalToPDPanelSIMD() * globalPoint;
    }

  public:
    // conditions handling

    /// name string
    static constexpr auto MyName() noexcept { return "Rich::Utils::RichSmartIDs"; }

    /// Default conditions name
    inline static const std::string DefaultConditionKey = DeRichLocations::derivedCondition( "RichSmartIDs-Handler" );

    /// Static generator method
    static auto generate( const Detector::PDInfo& pds, //
                          const Detector::Rich1&  r1,  //
                          const Detector::Rich2&  r2 ) {
      auto msgSvc = Gaudi::svcLocator()->service<IMessageSvc>( "MessageSvc" );
      assert( msgSvc );
      MsgStream log( msgSvc, MyName() );
      log << MSG::DEBUG << "Update triggered" << endmsg;
      return RichSmartIDs{pds, r1, r2};
    }

    /// Creates a condition derivation
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent ) {
      // Assume parent algorithm has one and only one input of the correct type...
      return addConditionDerivation( parent, parent->template inputLocation<RichSmartIDs>() );
    }

    /// Creates a condition derivation for the given key
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key ) {
      // The detector objects
      Detector::PDInfo::addConditionDerivation( parent );
      Detector::Rich1::addConditionDerivation( parent );
      Detector::Rich2::addConditionDerivation( parent );
      // create derived object
      if ( parent->msgLevel( MSG::DEBUG ) ) {
        parent->debug() << "RichSmartIDs::addConditionDerivation : Key=" << key << endmsg;
      }
      return parent->addSharedConditionDerivation( {Detector::PDInfo::DefaultConditionKey, // input conditions
                                                    Detector::Rich1::DefaultConditionKey,  // locations
                                                    Detector::Rich2::DefaultConditionKey}, //
                                                   std::move( key ),                       // output location
                                                   &generate );
    }

  private:
    // data

    /// PD Info
    const Detector::PDInfo* m_pdInfo = nullptr;

    /// Pointers to RICH1 and RICH2
    Rich::DetectorArray<const Detector::RichBase*> m_riches = {{}};

  private:
    /// Allocation tracking
    AllocateCount<RichSmartIDs> m_track_instances;
  };

} // namespace Rich::Utils
