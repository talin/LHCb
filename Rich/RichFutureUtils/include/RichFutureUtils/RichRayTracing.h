/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// LHCbKernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSide.h"
#include "Kernel/RichSmartID.h"
#include "Kernel/RichTraceMode.h"

// MathCore
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

// RichUtils
#include "RichFutureUtils/RichGeomPhoton.h"
#include "RichFutureUtils/RichMirrorFinder.h"
#include "RichFutureUtils/RichSIMDMirrorData.h"
#include "RichUtils/AllocateCount.h"
#include "RichUtils/RichRayTracingUtils.h"
#include "RichUtils/RichSIMDRayTracing.h"
#include "RichUtils/RichSIMDTypes.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Kernel
#include "Kernel/RichSide.h"
#include "Kernel/RichSmartID.h"
#include "Kernel/RichTraceMode.h"

// Rich Detectors
#ifdef USE_DD4HEP
#  include "Detector/Rich/DeRichBeampipe.h"
#else
#  include "RichDet/DeRichBeamPipe.h"
#endif
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich2.h"
#include "RichDetectors/RichMirror.h"

// Gaudi
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SystemOfUnits.h"

// Det Desc
#include "DetDesc/ConditionKey.h"

// STL
#include <array>
#include <cassert>
#include <cmath>
#include <iomanip>
#include <memory>
#include <vector>

namespace Rich::Utils {

  //-----------------------------------------------------------------------------
  /** @class RayTracing RichRayTracing.h
   *
   *  Utility to ray trace photons through the RICH system.
   *
   *  @author Chris Jones
   *  @date   2019-11-27
   */
  //-----------------------------------------------------------------------------

  class RayTracing final : public LHCb::SIMD::AlignedBase<LHCb::SIMD::VectorAlignment> {

  public:
    // types

    // SIMD types
    using FP            = SIMD::DefaultScalarFP;            ///< Default scalar floating point type
    using SIMDFP        = SIMD::FP<FP>;                     ///< Default vector floating point type
    using SIMDVector    = SIMD::Vector<FP>;                 ///< Default vector Vector class
    using SIMDPoint     = SIMD::Point<FP>;                  ///< Default vector Point class
    using SIMDPointVect = std::pair<SIMDPoint, SIMDVector>; ///< Pair of Points and Directions

    /// SIMD Result class
    using Result = RayTracingUtils::SIMDResult;

  public:
    // constructors

    /// Constructor from dependent detector elements and options
    RayTracing( const Detector::Rich1&     rich1,                     //
                const Detector::Rich2&     rich2,                     //
                const Utils::MirrorFinder& mirrFinder,                //
                const bool                 ignoreSecMirrs    = false, //
                const DetectorArray<bool>  treatSecMirrsFlat = {true, false} );

  public:
    // vector methods

    /** For a given detector, ray-traces a given set of directions from a given point to
     *  the photo detectors. */
    RayTracing::Result::Vector                                //
    traceToDetector( const Gaudi::XYZPoint&       startPoint, //
                     SIMD::STDVector<SIMDVector>& startDirs,  //
                     const Rich::DetectorType     rich,       //
                     const LHCb::RichTraceMode    mode = LHCb::RichTraceMode() ) const;

    /** For a given detector, ray-traces a given set of directions from a given point to
     *  the photo detectors. */
    decltype( auto )                                           //
    traceToDetector( const Gaudi::XYZPoint&        startPoint, //
                     SIMD::STDVector<SIMDVector>&  startDirs,  //
                     const LHCb::RichTrackSegment& trSeg,      //
                     const LHCb::RichTraceMode     mode = LHCb::RichTraceMode() ) const {
      return traceToDetector( startPoint, startDirs, trSeg.rich(), mode );
    }

    /** For a given detector, ray-traces a given set of directions from a given set
     *  of points to the photo detectors. */
    RayTracing::Result::Vector                                        //
    traceToDetector( SIMD::STDVector<SIMDPointVect>& startPointsDirs, //
                     const Rich::DetectorType        rich,            //
                     const LHCb::RichTraceMode       mode = LHCb::RichTraceMode() ) const;

  public:
    // scalar methods

    /// For a given detector, raytraces a given direction from a given point to
    /// the photo detectors. Returns the result in the form of a RichGeomPhoton
    decltype( auto ) traceToDetector( const Rich::DetectorType  rich,                               //
                                      const Gaudi::XYZPoint&    startPoint,                         //
                                      const Gaudi::XYZVector&   startDir,                           //
                                      Future::GeomPhoton&       photon,                             //
                                      const LHCb::RichTraceMode mode       = LHCb::RichTraceMode(), //
                                      const Rich::Side          forcedSide = Rich::top              //
                                      ) const {
      // temporary working objects
      Gaudi::XYZPoint  tmpPos( startPoint );
      Gaudi::XYZVector tmpDir( startDir );
      // Do the ray tracing
      return _traceToDetector( rich, startPoint, tmpPos, tmpDir, photon, mode, forcedSide );
    }

    /// For a given detector, raytraces a given direction from a given point to
    /// the photo detectors.
    decltype( auto ) traceToDetector( const Rich::DetectorType  rich,                               //
                                      const Gaudi::XYZPoint&    startPoint,                         //
                                      const Gaudi::XYZVector&   startDir,                           //
                                      Gaudi::XYZPoint&          hitPosition,                        //
                                      const LHCb::RichTraceMode mode       = LHCb::RichTraceMode(), //
                                      const Rich::Side          forcedSide = Rich::top              //
                                      ) const {
      Future::GeomPhoton photon;
      // need to think if this can be done without creating a temp RichGeomPhoton ?
      const auto sc = traceToDetector( rich, startPoint, startDir, photon, mode, forcedSide );
      hitPosition   = photon.detectionPoint();
      return sc;
    }

    /// Raytraces from a point in the detector panel back to the spherical mirror
    /// returning the mirror intersection point and the direction a track would
    /// have in order to hit that point in the detector panel.
    bool traceBackFromDetector( const Gaudi::XYZPoint&  startPoint, //
                                const Gaudi::XYZVector& startDir,   //
                                Gaudi::XYZPoint&        endPoint,   //
                                Gaudi::XYZVector&       endDir ) const;

  private:
    // methods

    /// Do the ray tracing
    LHCb::RichTraceMode::RayTraceResult _traceToDetector( const Rich::DetectorType  rich,       //
                                                          const Gaudi::XYZPoint&    startPoint, //
                                                          Gaudi::XYZPoint&          tmpPos,     //
                                                          Gaudi::XYZVector&         tmpDir,     //
                                                          Future::GeomPhoton&       photon,     //
                                                          const LHCb::RichTraceMode mode,       //
                                                          const Rich::Side          forcedSide ) const;

    /// Ray trace from given position in given direction off both mirrors
    bool reflectBothMirrors( const Rich::DetectorType  rich,      //
                             Gaudi::XYZPoint&          position,  //
                             Gaudi::XYZVector&         direction, //
                             Future::GeomPhoton&       photon,    //
                             const LHCb::RichTraceMode mode,      //
                             const Rich::Side          fSide ) const;

    /// Access the associated mirror finder
    [[nodiscard]] const Utils::MirrorFinder& mirrorFinder() const noexcept { return *m_mirrorFinder; }

  public:
    // conditions handling

    /// name string
    static constexpr auto MyName() noexcept { return "Rich::Utils::RayTracing"; }

    /// Default conditions name
    inline static const std::string DefaultConditionKey = DeRichLocations::derivedCondition( "RichRayTracing-Handler" );

    /// Creates a condition derivation
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent ) {
      // Assume parent algorithm has one and only one input of the correct type...
      return addConditionDerivation( parent, parent->template inputLocation<RayTracing>() );
    }

    /// Static generator method
    static auto generate( const Detector::Rich1& r1, //
                          const Detector::Rich2& r2, //
                          const MirrorFinder&    mf ) {
      auto msgSvc = Gaudi::svcLocator()->service<IMessageSvc>( "MessageSvc" );
      assert( msgSvc );
      MsgStream log( msgSvc, MyName() );
      log << MSG::DEBUG << "Update triggered" << endmsg;
      return RayTracing{r1, r2, mf};
    }

    /// Creates a condition derivation for the given key
    template <typename PARENT>
    static auto addConditionDerivation( PARENT*                     parent,          ///< Pointer to parent algorithm
                                        LHCb::DetDesc::ConditionKey ray_tracing_key, ///< location for derived condition
                                        LHCb::DetDesc::ConditionKey mirror_finder_key =
                                            Utils::MirrorFinder::DefaultConditionKey ///< Mirror finder location
    ) {

      // The detector objects
      Detector::Rich1::addConditionDerivation( parent );
      Detector::Rich2::addConditionDerivation( parent );

      // Mirror Finder
      Utils::MirrorFinder::addConditionDerivation( parent, mirror_finder_key );

      // Now the ray tracing ...
      if ( parent->msgLevel( MSG::DEBUG ) ) {
        parent->debug() << "RayTracing::addConditionDerivation : Key=" << ray_tracing_key << endmsg;
      }
      return parent->addSharedConditionDerivation( {Detector::Rich1::DefaultConditionKey, // input conditions locations
                                                    Detector::Rich2::DefaultConditionKey, //
                                                    std::move( mirror_finder_key )},      //
                                                   std::move( ray_tracing_key ),          // output location
                                                   &generate );
    }

  private:
    // data

    /// Rich1 and Rich2 pointers
    DetectorArray<const Detector::RichBase*> m_rich{{nullptr, nullptr}};

    /// Mirror finder
    const Utils::MirrorFinder* m_mirrorFinder = nullptr;

    /// Number of primary mirror rows in each RICH
    DetectorArray<int> m_sphMirrorSegRows{{0, 0}};
    /// Number of primary mirror columns in each RICH
    DetectorArray<int> m_sphMirrorSegCols{{0, 0}};
    /// Number of secondary mirror rows in each RICH
    DetectorArray<int> m_secMirrorSegRows{{0, 0}};
    /// Number of secondary mirror columns in each RICH
    DetectorArray<int> m_secMirrorSegCols{{0, 0}};

    /// Average spherical mirror RoCs for each RICH
    // alignas( LHCb::SIMD::VectorAlignment ) DetectorArray<SIMDFP> m_SphMirrRoC;
    // Workaround for dd4hep memory alignment bug
    // https://gitlab.cern.ch/lhcb/LHCb/-/issues/226
    std::unique_ptr<DetectorArray<SIMDFP>> m_SphMirrRoC;

    /// Flag to ignore secondary mirrors (useful for test beam work)
    bool m_ignoreSecMirrs = false;

    /// Flag to control if the secondary mirrors are treated as if they are completely flat
    DetectorArray<bool> m_treatSecMirrsFlat = {true, false};

  private:
    /// Allocation tracking
    AllocateCount<RayTracing> m_track_instances;
  };

} // namespace Rich::Utils
