/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//---------------------------------------------------------------------------------
/** @file RichDecodedData.h
 *
 *  Header file for Rich::Future::DAQ::DecodedData
 *
 *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   2003-11-06
 */
//---------------------------------------------------------------------------------

#pragma once

// STL
#include <algorithm>
#include <utility>
#include <vector>

// LHCbKernel
#include "Kernel/RichSmartID.h"

// RICH
#include "RichUtils/RichDAQDefinitions.h"

// Boost
#include <boost/container/static_vector.hpp>

/// New DAQ namespace
namespace Rich::Future::DAQ {

  /** @class PDInfo RichFutureUtils/RichDecodedData.h
   *  Decoded PD information
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   20/04/2007
   */
  class PDInfo final {

  public:
    /// SmartIDs container type
    using SmartIDs = boost::container::static_vector<LHCb::RichSmartID, LHCb::RichSmartID::MaPMT::TotalPixels>;

  public:
    /// Constructor from PD data bank information
    PDInfo( const LHCb::RichSmartID pdID ) : m_pdID( pdID ) {}

    /**  Access the PD ID (LHCb::RichSmartID) for this PD
     *   @attention It is possible this PD ID is invalid.
     *              Users should check for validity using the method
     *              RichSmartID::isvalid()
     */
    [[nodiscard]] const LHCb::RichSmartID pdID() const& noexcept { return m_pdID; }

    /// set the Level1Input
    void setPdID( const LHCb::RichSmartID& input ) noexcept { m_pdID = input; }

    /// Read access to the RichSmartIDs for the hit pixels in this PD
    [[nodiscard]] const SmartIDs& smartIDs() const& { return m_smartIds; }

    /// Write access to the RichSmartIDs for the hit pixels in this PD
    SmartIDs& smartIDs() & { return m_smartIds; }

    /// Move the smart IDs
    SmartIDs&& smartIDs() && { return std::move( m_smartIds ); }

  public:
    /// Container type
    using Vector = boost::container::static_vector<PDInfo, LHCb::RichSmartID::MaPMT::MaxPDsPerModule>;

  private:
    /// The RichSmartID for this PD
    LHCb::RichSmartID m_pdID;
    /// The decoded pixels in this PD
    SmartIDs m_smartIds;
  };

  /// PD Module Data
  class ModuleData final : public PDInfo::Vector {
  public:
    /// Constructor from module ID
    ModuleData( const Rich::DAQ::PDModuleNumber mID ) : m_mID( mID ) {}

    /// Module number
    [[nodiscard]] const Rich::DAQ::PDModuleNumber moduleNumber() const noexcept { return m_mID; }

  public:
    /// Container type
    using Vector = std::vector<ModuleData>;

  private:
    /// The module number
    Rich::DAQ::PDModuleNumber m_mID;
  };

  /// Decoded Data
  class DecodedData final : public DetectorArray<PanelArray<ModuleData::Vector>> {

  public:
    /// Default Constructor. Reserve sizes in panel data vectors.
    DecodedData() {
      // reserve sizes for each RICH/panel
      for ( const auto rich : {Rich::Rich1, Rich::Rich2} ) {
        for ( auto& p : data()[rich] ) { p.reserve( LHCb::RichSmartID::MaPMT::MaxModulesPerRICH[rich] ); }
      }
    }

  public:
    /// Returns the total number of hits in the decoded data for the given RICH detector
    [[nodiscard]] auto nTotalHits( const Rich::DetectorType rich ) const noexcept { return m_nTotalHits[rich]; }

    /// Returns the overall total number of RICH hits in the decoded data
    [[nodiscard]] auto nTotalHits() const noexcept { return nTotalHits( Rich::Rich1 ) + nTotalHits( Rich::Rich2 ); }

    /// Append to the number of hits for each RICH
    void addToTotalHits( const DetectorArray<unsigned int>& nHits ) noexcept {
      for ( std::size_t i = 0; i < nHits.size(); ++i ) { m_nTotalHits[i] += nHits[i]; }
    }

    /// Append to the number of hits for given RICH
    void addToTotalHits( const Rich::DetectorType rich, const unsigned int nHits = 1 ) noexcept {
      m_nTotalHits[rich] += nHits;
    }

    /// Returns the total number of active PDs in the decoded data for the given RICH
    [[nodiscard]] auto nActivePDs( const Rich::DetectorType rich ) const noexcept { return m_nActivePDs[rich]; }

    /// Returns the overall total number of active PDs in the decoded data
    [[nodiscard]] auto nActivePDs() const noexcept { return nActivePDs( Rich::Rich1 ) + nActivePDs( Rich::Rich2 ); }

    /// Append to the number of active PDs for each RICH
    void addToActivePDs( const DetectorArray<unsigned int>& nPDs ) noexcept {
      for ( std::size_t i = 0; i < nPDs.size(); ++i ) { m_nActivePDs[i] += nPDs[i]; }
    }

    /// Append to the number of active PDs for the given RICH detector
    void addToActivePDs( const Rich::DetectorType rich, const unsigned int nPDs = 1 ) noexcept {
      m_nActivePDs[rich] += nPDs;
    }

  private:
    /// The total hit count for each RICH detector
    DetectorArray<unsigned int> m_nTotalHits = {{0, 0}};
    /// The total active PD count for each RICH detector
    DetectorArray<unsigned int> m_nActivePDs = {{0, 0}};
  };

  /// L1Map data locations
  namespace DecodedDataLocation {
    /// Default Location in TES for the decoded data
    inline const std::string Default = "Raw/Rich/DecodedData/RICH1RICH2";
  } // namespace DecodedDataLocation

} // namespace Rich::Future::DAQ
