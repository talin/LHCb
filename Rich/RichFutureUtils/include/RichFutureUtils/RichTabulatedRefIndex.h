/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Rich Detector
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich1Gas.h"
#include "RichDetectors/Rich2.h"
#include "RichDetectors/Rich2Gas.h"

// Kernel
#include "Kernel/RichParticleIDType.h"
#include "Kernel/RichRadiatorType.h"

// Maths
#include "LHCbMath/FastMaths.h"

// Utils
#include "RichInterfaces/IRichParticleProperties.h"
#include "RichUtils/AllocateCount.h"
#include "RichUtils/RichTrackSegment.h"

// Det Desc
#include "DetDesc/ConditionKey.h"

// STL
#include <array>
#include <cassert>
#include <limits>
#include <memory>

namespace Rich::Utils {

  //-----------------------------------------------------------------------------
  /** @class TabulatedRefIndex RichTabulatedRefIndex.h
   *
   *  Tool to calculate the effective refractive index for
   *  a given radiator. An implementation that uses the tabulated
   *  information from the XML.
   *
   *  @author Chris Jones
   *  @date   2017-01-19
   */
  //-----------------------------------------------------------------------------

  class TabulatedRefIndex final {

  public:
    // constructors

    /// Constructor from detector info
    TabulatedRefIndex( const Detector::Rich1&      rich1,     //
                       const Detector::Rich2&      rich2,     //
                       const RadiatorArray<float>& minPhotEn, //
                       const RadiatorArray<float>& maxPhotEn, //
                       const ParticleArray<float>& masses )
        : m_riches{&rich1, &rich2}
        , m_radiators{nullptr, &rich1.radiator(), &rich2.radiator()}
        , m_minPhotEn( minPhotEn )
        , m_maxPhotEn( maxPhotEn )
        , m_particleMass( masses ) {
      // compute nominal sat. CK theta values
      for ( const auto rad : Rich::radiators() ) {
        const auto refIn = refractiveIndex( rad );
        m_nomSatCKT[rad] = ( refIn > 1 ? LHCb::Math::fast_acos( 1.0f / refIn ) : 0 );
      }
    }

  public:
    // access methods

    /// Calculates the refractive index for a given radiator type at a
    /// given energy
    [[nodiscard]] float refractiveIndex( const Rich::RadiatorType rad, //
                                         const float              energy ) const {
      const auto deR = m_radiators[rad];
      return ( deR ? deR->refractiveIndex( energy ) : 0 );
    }

    /// Calculates the average refractive index for a given radiator type
    /// for a given range of photon energies.
    [[nodiscard]] float refractiveIndex( const Rich::RadiatorType rad,       //
                                         const float              energyBot, //
                                         const float              energyTop ) const {
      const auto rich = ( rad == Rich::Rich2Gas ? Rich::Rich2 : Rich::Rich1 );
      return refractiveIndex( rad,
                              m_riches[rich]->nominalPDQuantumEff()->meanX( energyBot, energyTop ) / Gaudi::Units::eV );
    }

    /// Calculates the average refractive index for a given radiator type
    /// for a all visable photon energies.
    [[nodiscard]] float refractiveIndex( const Rich::RadiatorType rad ) const {
      return refractiveIndex( rad, meanPhotonEnergy( rad ) );
    }

    /// Calculates the average refractive index for a given set of radiator intersections
    /// for all visable photon energies.
    [[nodiscard]] float refractiveIndex( const Rich::RadIntersection::Vector& intersections, const float energy ) const;

    /// Calculates the average refractive index for a given set of radiator intersections
    /// for all visable photon energies
    [[nodiscard]] float refractiveIndex( const Rich::RadIntersection::Vector& intersections ) const;

    /// Calculates the refractive index R.M.S. for a given set of radiator intersections
    /// for all visable photon energies.
    [[nodiscard]] float refractiveIndexRMS( const Rich::RadIntersection::Vector& intersections ) const;

    /// Calculates the refractive index S.D. for a given set of radiator intersections
    /// for all visable photon energies.
    [[nodiscard]] float refractiveIndexSD( const Rich::RadIntersection::Vector& intersections ) const;

    /// Returns the threshold momentum for a given hypothesis in a given radiator
    [[nodiscard]] float thresholdMomentum( const Rich::ParticleIDType id, const Rich::RadiatorType rad ) const;

    /// Calculates the threshold momentum for a given mass hypothesis
    [[nodiscard]] float thresholdMomentum( const Rich::ParticleIDType id, const LHCb::RichTrackSegment& trSeg ) const;

    /// Calculates the nominal saturated Cherenkov Theta value for given radiator
    [[nodiscard]] float nominalSaturatedCherenkovTheta( const Rich::RadiatorType rad ) const noexcept {
      return m_nomSatCKT[rad];
    }

  private:
    // methods

    /// get the average mean photon energy for given radiator
    [[nodiscard]] float meanPhotonEnergy( const Rich::RadiatorType rad ) const noexcept {
      return 0.5f * ( m_maxPhotEn[rad] + m_minPhotEn[rad] );
    }

  private:
    // data

    /// Pointers to RICHes
    DetectorArray<const Detector::RichBase*> m_riches = {{}};

    /// Pointers to RICH radiator detector elements
    RadiatorArray<const Detector::Radiator*> m_radiators = {{}};

    /// The minimum photon energies
    RadiatorArray<float> m_minPhotEn{{std::numeric_limits<float>::signaling_NaN()}};

    /// The maximum photon energies
    RadiatorArray<float> m_maxPhotEn{{std::numeric_limits<float>::signaling_NaN()}};

    /// Array containing particle masses
    ParticleArray<float> m_particleMass = {{std::numeric_limits<float>::signaling_NaN()}};

    /// Cached values of nominal saturated CK theta values for each radiator
    RadiatorArray<float> m_nomSatCKT = {{std::numeric_limits<float>::signaling_NaN()}};

    /// Static pointer to RICH particle properties
    inline static const IParticleProperties* s_partProps{nullptr};

  public:
    // conditions handling

    /// name string
    static constexpr auto MyName() noexcept { return "Rich::Utils::TabulatedRefIndex"; }

    /// Default conditions name
    inline static const std::string DefaultConditionKey = DeRichLocations::derivedCondition( "RichRefIndex-Handler" );

    /// Creates a condition derivation
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent ) {
      // Assume parent algorithm has one and only one input of the correct type...
      return addConditionDerivation( parent, parent->template inputLocation<TabulatedRefIndex>() );
    }

    /// Static generator function
    static auto generate( const Detector::Rich1& rich1, //
                          const Detector::Rich2& rich2 ) {
      assert( s_partProps );
      auto msgSvc = Gaudi::svcLocator()->service<IMessageSvc>( "MessageSvc" );
      assert( msgSvc );
      MsgStream log( msgSvc, MyName() );
      log << MSG::DEBUG << "Update triggered" << endmsg;
      return TabulatedRefIndex{rich1,                          // RICH1
                               rich2,                          // RICH2
                               s_partProps->minPhotonEnergy(), // minimum CK photon energies
                               s_partProps->maxPhotonEnergy(), // maximum CK photon energies
                               s_partProps->masses()};         // particle masses
    }

    /// Creates a condition derivation for the given key
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key ) {
      assert( parent );
      // The detector objects
      Detector::Rich1::addConditionDerivation( parent );
      Detector::Rich2::addConditionDerivation( parent );
      if ( parent->msgLevel( MSG::DEBUG ) ) {
        parent->debug() << "TabulatedRefIndex::addConditionDerivation : Key=" << key << endmsg;
      }
      // set static particle properties pointer
      // this is a bit of a hack. Works for now, but should find a better solution to getting
      // the photon parameters into the TabulatedRefIndex initialisation
      assert( !s_partProps || s_partProps == parent->richPartProps() );
      s_partProps = parent->richPartProps();
      // create the condition derivation
      return parent->addSharedConditionDerivation( {Detector::Rich1::DefaultConditionKey,  // inputs
                                                    Detector::Rich2::DefaultConditionKey}, //
                                                   std::move( key ),                       // output
                                                   &generate );
    }

  private:
    /// Allocation tracking
    AllocateCount<TabulatedRefIndex> m_track_instances;
  };

} // namespace Rich::Utils
