/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "RichFutureKernel/RichAlgBase.h"

#include "LHCbAlgs/Consumer.h"

#include "RichDet/DeRichLocations.h"
#include "RichDetectors/Condition.h"
#include "RichDetectors/Handle.h"
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich2.h"
#include "RichDetectors/RichPDInfo.h"

#include "RichFutureDAQ/RichTel40CableMapping.h"

#include "RichFutureUtils/RichDecodedData.h"
#include "RichFutureUtils/RichMirrorFinder.h"
#include "RichFutureUtils/RichSmartIDs.h"

#include "boost/format.hpp"

#include <mutex>
#include <string>
#include <utility>
#include <vector>

using namespace Rich::Future::DAQ;
using namespace Rich::Detector;

namespace Rich::Future {

#ifdef USE_DD4HEP
  using DR1 = LHCb::Detector::DeRich1;
  using DR2 = LHCb::Detector::DeRich2;
#else
  using DR1 = DeRich1;
  using DR2 = DeRich2;
#endif

  class TestDBAccess final
      : public LHCb::Algorithm::Consumer<void( const DR1&, const DR2& ),
                                         LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, DR1, DR2>> {
  public:
    TestDBAccess( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,                                    //
                    {KeyValue{"R1Loc", DeRichLocations::location<DR1>()}, //
                     KeyValue{"R2Loc", DeRichLocations::location<DR2>()}} ) {}

    void operator()( const DR1& r1, const DR2& r2 ) const override {
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "R1 " << &r1 << " name='" << r1.name() << "' access()=" << r1.access() << endmsg;
        debug() << "R2 " << &r2 << " name='" << r2.name() << "' access()=" << r2.access() << endmsg;
      }
    }
  };

  namespace {
    template <typename R>
    struct TestDervCondA {
      inline static auto key() { return DeRichLocations::derivedCondition<R>( "TestDervCondA" ); }
      TestDervCondA( const R& d ) : r( d ) {}
      Rich::Detector::Handle<R> r;
    };
    template <typename R>
    struct TestDervCondB {
      inline static auto key() { return DeRichLocations::derivedCondition<R>( "TestDervCondB" ); }
      TestDervCondB( const R& d ) : r( d ) {}
      Rich::Detector::Handle<R> r;
    };
    template <typename DR, typename C, typename PARENT>
    void addTestCond( PARENT* parent ) {
      parent->addConditionDerivation( {DeRichLocations::location<DR>()}, C::key(), [p = parent]( const DR& d ) {
        p->info() << "Creating derived condition from DetLoc='" << DeRichLocations::location<DR>() //
                  << " CondKey='" << C::key() << "'" << endmsg;
        return C{d};
      } );
    }
  } // namespace

  class TestDerivedElem final
      : public LHCb::Algorithm::Consumer<void( const TestDervCondA<DR1>&, const TestDervCondA<DR2>&, //
                                               const TestDervCondB<DR1>&, const TestDervCondB<DR2>& ),
                                         LHCb::DetDesc::usesBaseAndConditions<AlgBase<>,                              //
                                                                              TestDervCondA<DR1>, TestDervCondA<DR2>, //
                                                                              TestDervCondB<DR1>, TestDervCondB<DR2>>> {
  public:
    TestDerivedElem( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,                                  //
                    {KeyValue{"R1DervALoc", TestDervCondA<DR1>::key()}, //
                     KeyValue{"R2DervALoc", TestDervCondA<DR2>::key()},
                     KeyValue{"R1DervBLoc", TestDervCondB<DR1>::key()}, //
                     KeyValue{"R2DervBLoc", TestDervCondB<DR2>::key()}} ) {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] {
        addTestCond<DR1, TestDervCondA<DR1>>( this );
        addTestCond<DR2, TestDervCondA<DR2>>( this );
        addTestCond<DR1, TestDervCondB<DR1>>( this );
        addTestCond<DR2, TestDervCondB<DR2>>( this );
      } );
    }

    void operator()( const TestDervCondA<DR1>& d1a, const TestDervCondA<DR2>& d2a, //
                     const TestDervCondB<DR1>& d1b, const TestDervCondB<DR2>& d2b ) const override {
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "R1 " << d1a.r << " name='" << d1a.r->name() << "' access()=" << d1a.r->access() << endmsg;
        debug() << "R2 " << d2a.r << " name='" << d2a.r->name() << "' access()=" << d2a.r->access() << endmsg;
        debug() << "R1 " << d1b.r << " name='" << d1b.r->name() << "' access()=" << d1b.r->access() << endmsg;
        debug() << "R2 " << d2b.r << " name='" << d2b.r->name() << "' access()=" << d2b.r->access() << endmsg;
      }
    }
  };

  class TestDerivedDetObjects final
      : public LHCb::Algorithm::Consumer<
            void( const Detector::Rich1&,  //
                  const Detector::Rich2&,  //
                  const Detector::PDInfo&, //
                  const Rich::Utils::MirrorFinder& ),
            LHCb::DetDesc::usesBaseAndConditions<AlgBase<>,                        //
                                                 Detector::Rich1, Detector::Rich2, //
                                                 Detector::PDInfo, const Rich::Utils::MirrorFinder&>> {
  public:
    /// Standard constructor
    TestDerivedDetObjects( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"Rich1", Detector::Rich1::conditionKey()},
                     KeyValue{"Rich2", Detector::Rich2::conditionKey()},
                     KeyValue{"RichPDInfo", Detector::PDInfo::DefaultConditionKey},
                     KeyValue{"RichMirrorFinder", Rich::Utils::MirrorFinder::DefaultConditionKey}} ) {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] {
        Detector::Rich1::addConditionDerivation( this );
        Detector::Rich2::addConditionDerivation( this );
        Detector::PDInfo::addConditionDerivation( this );
        Utils::MirrorFinder::addConditionDerivation( this );
      } );
    }

    void operator()( const Detector::Rich1& r1, const Detector::Rich2& r2, //
                     const Detector::PDInfo& pdInfo, const Rich::Utils::MirrorFinder& mirrFinder ) const override {
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << r1 << endmsg;
        debug() << r2 << endmsg;
        debug() << pdInfo << endmsg;
        debug() << mirrFinder << endmsg;
      }
    }
  };

  class TestRadiatorIntersections final
      : public LHCb::Algorithm::Consumer<
            void( const Detector::Rich1&, const Detector::Rich2& ),
            LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, Detector::Rich1, Detector::Rich2>> {
  public:
    /// Standard constructor
    TestRadiatorIntersections( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"Rich1", Detector::Rich1::DefaultConditionKey},
                     KeyValue{"Rich2", Detector::Rich2::DefaultConditionKey}} ) {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] {
        Detector::Rich1::addConditionDerivation( this );
        Detector::Rich2::addConditionDerivation( this );
      } );
    }

    void operator()( const Detector::Rich1& r1, const Detector::Rich2& r2 ) const override {

      using TrajData                     = std::pair<Gaudi::XYZPoint, Gaudi::XYZVector>;
      const std::vector<TrajData> r1Traj = {{{4.8339, -29.1795, 990}, {0.00492716, -0.0340466, 1}},
                                            {{-98.3335, 17.5605, 990}, {-0.160749, 0.0358936, 1}},
                                            {{2.9279, -9.9783, 990}, {0.00322774, -0.0103524, 1}},
                                            {{-40.8416, 14.2797, 990}, {-0.0587384, 0.00969204, 1}},
                                            {{197.9, -182.212, 990}, {0.204637, -0.188133, 1}}};
      const std::vector<TrajData> r2Traj = {{{-211.711, -52.4182, 9450}, {-0.0398052, -0.00558938, 1}},
                                            {{877.05, 91.0958, 9500}, {0.285066, 0.00889497, 1}},
                                            {{2933.82, -1818.83, 9450}, {0.44724, -0.189439, 1}},
                                            {{1567.06, -230.439, 9500}, {0.280539, -0.0236485, 1}},
                                            {{-671.092, -96.1549, 9450}, {-0.133261, -0.0101545, 1}}};

      auto testIntersects = [&]( const auto& trajs, const auto& rad ) {
        for ( const auto& t : trajs ) {
          Gaudi::XYZPoint entry, exit;
          const auto      ok = rad.intersectionPoints( t.first, t.second, entry, exit );
          info() << endmsg;
          info() << "Trajectory " << t.first << " " << t.second << endmsg;
          info() << " -> intersects = " << ok << endmsg;
          info() << " -> entry      = " << entry << endmsg;
          info() << " -> exit       = " << exit << endmsg;
        }
      };

      testIntersects( r1Traj, r1.radiator() );
      testIntersects( r2Traj, r2.radiator() );
    }
  };

  // Example of algorithm accessing conditions
  struct TestConds
      : LHCb::Algorithm::Consumer<void( const Rich::Detector::Condition& ),
                                  LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, Rich::Detector::Condition>> {
    // constructor
    TestConds( const std::string& name, ISvcLocator* loc )
        : Consumer{name, loc, {KeyValue{"CondPath", Tel40CableMapping::ConditionPaths[0]}}} {}

    void operator()( const Rich::Detector::Condition& cond ) const override {
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "NumberOfLinks=" << condition_param<int>( cond, "NumberOfLinks" ) << endmsg;
      }
    }
  };

  // Test RICH decoding and SmartIDs
  struct TestDecodeAndIDs
      : LHCb::Algorithm::Consumer<void( const DAQ::DecodedData&, //
                                        const Rich::Utils::RichSmartIDs& ),
                                  LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, Rich::Utils::RichSmartIDs>> {
    // Standard constructor
    TestDecodeAndIDs( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    {KeyValue{"DecodedDataLocation", DAQ::DecodedDataLocation::Default},
                     // input conditions data
                     KeyValue{"RichSmartIDs", Rich::Utils::RichSmartIDs::DefaultConditionKey}} ) {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] { Rich::Utils::RichSmartIDs::addConditionDerivation( this ); } );
    }

    void operator()( const DAQ::DecodedData& data, const Rich::Utils::RichSmartIDs& smartIDs ) const override {
      static std::once_flag run_once;
      std::call_once( run_once, [&]() {
        // RICHes loop
        for ( const auto& rD : data ) {
          // RICH panels
          for ( const auto& pD : rD ) {
            // PD modules
            for ( const auto& mD : pD ) {
              // PDs per module
              for ( const auto& PD : mD ) {
                // PD ID
                const auto pdID = PD.pdID();
                if ( pdID.isValid() ) {
                  // loop over IDs
                  for ( const auto& id : PD.smartIDs() ) {
                    const auto gPos = smartIDs.globalPosition( id );
                    const auto lPos = smartIDs.globalToPDPanel( gPos );
                    // print to 2 d.p. only to avoid small diffs between DetDesc and dd4hep
                    info() << id
                           << boost::format( " GPos=( %.2f, %.2f, %.2f ) LPos=( %.2f, %.2f, %.2f )" ) //
                                  % gPos.X() % gPos.Y() % gPos.Z()                                    //
                                  % lPos.X() % lPos.Y() % lPos.Z()
                           << endmsg;
                  }
                } else {
                  warning() << "INVALID PD ID " << pdID << endmsg;
                }
              }
            }
          }
        }
      } );
    }
  };

  DECLARE_COMPONENT( TestDBAccess )
  DECLARE_COMPONENT( TestDerivedElem )
  DECLARE_COMPONENT( TestDerivedDetObjects )
  DECLARE_COMPONENT( TestRadiatorIntersections )
  DECLARE_COMPONENT( TestConds )
  DECLARE_COMPONENT( TestDecodeAndIDs )

} // namespace Rich::Future
