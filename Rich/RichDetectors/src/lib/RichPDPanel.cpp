/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "RichDetectors/RichPDPanel.h"

#include <limits>

using namespace Rich::Detector;

PDPanel::RayTStructSIMD                                       //
PDPanel::detIntersectSIMD( const SIMDPoint&          pGlobal, //
                           const SIMDVector&         vGlobal, //
                           const LHCb::RichTraceMode mode ) const {

  // data to return
  auto  data   = RayTStructSIMD{};
  auto& res    = std::get<SIMDRayTResult::Results>( data );
  auto& hitPos = std::get<SIMDPoint>( data );
  auto& pds    = std::get<SIMDPDs>( data );
  auto& ids    = std::get<SIMDSmartIDs>( data );

#ifndef USE_LOCAL_RICH_DET_PLANE_INTERSECT_METHOD
  // With DetDesc just refer back to detector object which will
  // support both new and 'classic' PMTs, whereas the local implementation
  // here is only correct for the dd4hep and new DetDesc PMTs.
  // In principle could perform a runtime check on the DetDesc PMT type
  // and use the local implementation for the newer PMTs, but its
  // probably not worth the effort.

  DeRichPDPanel::SIMDPDs oldPDs;
  assert( oldPDs.size() == ids.size() );
  res = ( mode.detPlaneBound() >= LHCb::RichTraceMode::DetectorPlaneBoundary::RespectPDTubes
              ? get()->PDWindowPointSIMD( pGlobal, vGlobal, hitPos, ids, oldPDs, mode )
              : get()->detPlanePointSIMD( pGlobal, vGlobal, hitPos, ids, oldPDs, mode ) );
  for ( std::size_t i = 0; i < oldPDs.size(); ++i ) { pds[i] = dePD( ids[i] ); }

#else
  // For DD4HEP implement functionality locally

  // result code. Defaults to outside the panel.
  res = SIMDRayTResult::Results( (unsigned int)LHCb::RichTraceMode::RayTraceResult::OutsidePDPanel );

  // panel intersection in global coords
  hitPos = getPanelInterSection( pGlobal, vGlobal );

  // set hit position to plane intersection in local frame
  const auto panelIntersection = globalToLocalSIMD() * hitPos;

  // panel acceptance limits
  const auto pmask = isInAcceptance( panelIntersection );
  if ( any_of( pmask ) ) {

    // Get the PDs for this point. Note lookup finder (intentionally) does not
    // apply any min/max panel boundary limits. These should be handled here.
    pds = getPDs( panelIntersection );

    // PD acceptance check enabled ?
    const bool checkPDAcpt = ( mode.detPlaneBound() >= LHCb::RichTraceMode::DetectorPlaneBoundary::RespectPDTubes );

    // As dealing with pointers need to resort to scalar loop at this point.
    for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
      // set final status word
      res[i] = ( mode.detPlaneBound() >= LHCb::RichTraceMode::DetectorPlaneBoundary::RespectPDPanel
                     ? pmask[i] ? LHCb::RichTraceMode::RayTraceResult::InPDPanel
                                : LHCb::RichTraceMode::RayTraceResult::OutsidePDPanel
                     : LHCb::RichTraceMode::RayTraceResult::InPDPanel );
      if ( !pmask[i] ) {
        pds[i] = nullptr;
      } else {
        if ( pds[i] ) {
          ids[i] = pds[i]->pdSmartID();
          if ( checkPDAcpt ) {
            const FP x = panelIntersection.X()[i];
            const FP y = panelIntersection.Y()[i];
            if ( pds[i]->isInAcceptance( x, y ) ) { res[i] = LHCb::RichTraceMode::RayTraceResult::InPDTube; }
          }
        } else {
          ids[i] = panelSmartID();
        }
      }
    }

  } // panel acceptance mask check

#endif

  // Finally return the data tuple
  return data;
}

// For scalar methods use the SIMD method internally
// Note this will not be as efficient as properly using the SIMD methods,
// but eventually the scalar calls should be fully depreciated anyway so this
// is just a short term placeholder.

namespace {
  /// Converts a SIMD ray tracing tuple object to a scalar version
  auto simdToScalarRayT( PDPanel::RayTStructSIMD&& simd_data ) {
    auto        data    = PDPanel::RayTStruct{};
    const auto& ptns    = std::get<0>( simd_data );
    std::get<0>( data ) = {ptns.x()[0], ptns.y()[0], ptns.z()[0]};
    std::get<1>( data ) = std::get<1>( simd_data )[0];
    std::get<2>( data ) = std::get<2>( simd_data )[0];
    std::get<3>( data ) = LHCb::RichTraceMode::RayTraceResult( (int)std::get<3>( simd_data )[0] );
    return data;
  }
} // namespace

PDPanel::RayTStruct                                       //
PDPanel::detIntersect( const Gaudi::XYZPoint&    pGlobal, //
                       const Gaudi::XYZVector&   vGlobal, //
                       const LHCb::RichTraceMode mode ) const {
  return simdToScalarRayT( detIntersectSIMD( SIMDPoint( pGlobal ), SIMDVector( vGlobal ), mode ) );
}
