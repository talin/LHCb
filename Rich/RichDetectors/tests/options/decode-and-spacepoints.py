###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
from Gaudi.Configuration import *
from GaudiConfig.ControlFlow import seq
from Configurables import GaudiSequencer
from Configurables import Rich__Future__TestDecodeAndIDs as TestDecodeAndIDs
from Configurables import Rich__Future__TestDerivedDetObjects as TestDerivedDetObjects
from Configurables import createODIN, FPEAuditor
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile
from Configurables import Rich__Future__RawBankDecoder as RichDecoder
from Configurables import LHCb__UnpackRawEvent as UnpackRawEvent

# Event numbers
nEvents = 1
EventSelector().PrintFreq = 1

# Auditors
AuditorSvc().Auditors += ["FPEAuditor"]
FPEAuditor().ActivateAt = ["Execute"]

# The overall sequence. Filled below.
all = GaudiSequencer("All", MeasureTime=False)

# Finally set up the application
app = ApplicationMgr(
    TopAlg=[all],
    EvtMax=nEvents,
    ExtSvc=['ToolSvc', 'AuditorSvc'],
    AuditAlgorithms=True)

# Are we using DD4Hep ?
from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    dd4hep = DD4hepSvc(DetectorList=["/world", "Rich1", "Rich2"])
    app.ExtSvc += [dd4hep]

# --------------------------------------------------------------------------------------

# Fetch required data from file
fetcher = FetchDataFromFile('FetchDSTData')
fetcher.DataKeys = ['Trigger/RawEvent', 'Rich/RawEvent']
all.Members += [fetcher]

# Unpack the ODIN raw event
all.Members += [
    UnpackRawEvent(
        'UnpackODIN',
        BankTypes=['ODIN'],
        RawBankLocations=['DAQ/RawBanks/ODIN'])
]

# Build ODIN
odinDecode = createODIN("ODINFutureDecode")
all.Members += [odinDecode]

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer
    all.Members += [IOVProducer("ReserveIOVDD4hep", ODIN=odinDecode.ODIN)]
# decoding
# Unpack the RICH raw banks
all.Members += [
    UnpackRawEvent(
        'UnpackRich',
        BankTypes=['Rich'],
        RawEventLocation="Rich/RawEvent:DAQ/RawEvent",
        RawBankLocations=['DAQ/RawBanks/Rich'])
]
richDecode = RichDecoder("RichFutureDecode")
all.Members += [richDecode]

# scalar positions
all.Members += [TestDecodeAndIDs("TestDecodeAndIDs")]
