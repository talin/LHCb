###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
from Configurables import LHCbApp, DDDBConf
from GaudiConf import IOHelper

LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"
LHCbApp().DDDBtag = "upgrade/master"

from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    DDDBConf().GeometryVersion = 'trunk'
    # https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/merge_requests/26
    LHCbApp().CondDBtag = "jonrob/all-pmts-active"
else:
    from Configurables import CondDB
    CondDB().setProp("Upgrade", True)
    LHCbApp().CondDBtag = "upgrade/master"

data = [
    # Older sample created using 2019 DDDB tag
    # "PFN:root://eoslhcb.cern.ch//eos/lhcb/user/j/jonrob/data/MC/Upgrade/NewPMTsSE/13104011/XDST/Brunel-Std-Upgrade-PmtArrayUpdate-SmtID-Pythia8-lumi20-Aug2019-0-000-100.xdst"
    # Private sample created by Bartosz using DDDB tag "upgrade/dddb-20220111"
    "PFN:root://eoslhcb.cern.ch//eos/lhcb/user/j/jonrob/data/MC/Run3/BM-20220111-DDDB/30000000/XDST/Brunel_100.xdst"
]
IOHelper('ROOT').inputFiles(data, clear=True)
FileCatalog().Catalogs = ['xmlcatalog_file:out.xml']
