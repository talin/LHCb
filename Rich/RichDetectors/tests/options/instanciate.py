###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# --------------------------------------------------------------------------------------

from Gaudi.Configuration import *
from GaudiConfig.ControlFlow import seq
from Configurables import LHCbApp, GaudiSequencer, DDDBConf
import os
from Configurables import Rich__Future__TestDBAccess as TestDBAccess
from Configurables import Rich__Future__TestDerivedElem as TestDerivedElem
from Configurables import Rich__Future__TestDerivedDetObjects as TestDerivedDetObjects
from Configurables import Rich__Future__TestConds as TestConds
from Configurables import createODIN, FPEAuditor
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile

# Event numbers
nEvents = 1
EventSelector().PrintFreq = 1

# Just to initialise
DDDBConf()
LHCbApp()

# Auditors
AuditorSvc().Auditors += ["FPEAuditor"]
FPEAuditor().ActivateAt = ["Execute"]

# The overall sequence. Filled below.
all = GaudiSequencer("All", MeasureTime=False)

# Finally set up the application
app = ApplicationMgr(
    TopAlg=[all],
    EvtMax=nEvents,
    ExtSvc=['ToolSvc', 'AuditorSvc'],
    AuditAlgorithms=True)

# Are we using DD4Hep ?
from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    dd4hep = DD4hepSvc(DetectorList=["/world", "Rich1", "Rich2"])
    app.ExtSvc += [dd4hep]

# --------------------------------------------------------------------------------------

# Fetch required data from file
fetcher = FetchDataFromFile('FetchDSTData')
fetcher.DataKeys = ['Trigger/RawEvent', 'Rich/RawEvent']
all.Members += [fetcher]

# Unpack the ODIN raw event
from Configurables import LHCb__UnpackRawEvent as UnpackRawEvent

all.Members += [
    UnpackRawEvent(
        'UnpackODIN',
        BankTypes=['ODIN'],
        RawEventLocation='Trigger/RawEvent',
        RawBankLocations=['DAQ/RawBanks/ODIN'])
]

odinDecode = createODIN("ODINFutureDecode")
all.Members += [odinDecode]

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer
    all.Members += [IOVProducer("ReserveIOVDD4hep", ODIN=odinDecode.ODIN)]

# Trivial Tests for DetDesc/DD4Hep access
all.Members += [
    TestDerivedDetObjects("TestDerivedDets", OutputLevel=1),
    TestConds("TestConds", OutputLevel=1)
]
