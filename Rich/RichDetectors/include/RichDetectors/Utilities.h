/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "DetDesc/IConditionDerivationMgr.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "Kernel/STLExtensions.h"

#include <array>
#include <memory>
#include <ostream>
#include <type_traits>
#include <utility>
#include <vector>

#ifndef FORWARD_TO_DET_OBJ
#  define FORWARD_TO_DET_OBJ( method )                                                                                 \
    template <typename... ARGS>                                                                                        \
    inline decltype( auto ) method( ARGS&&... args ) const {                                                           \
      return get()->method( std::forward<ARGS>( args )... );                                                           \
    }
#endif

namespace Rich::Detector {

  /// Get a scalar parameter value from an SIMD parameter
  template <typename TYPE>
  inline auto scalar( const TYPE& t ) noexcept {
    return typename TYPE::value_type( t[0] );
  }

  /// Convert a vector to an array of the same contained type and size
  template <typename OUTTYPE, std::size_t N, typename INTYPE = OUTTYPE>
  inline auto toarray( const std::vector<INTYPE>& v ) {
    ASSUME( v.size() == N );
    std::array<OUTTYPE, N> a{};
    std::copy( v.begin(), v.end(), a.begin() );
    return a;
  }

  /// Convert a transform to SIMD version
  template <typename TRANS, typename SIMDTRANS>
  inline void toSIMDTrans( const TRANS& t, SIMDTRANS& simdT ) noexcept {
    // Extract the scalar parameters
    typename TRANS::Scalar xx{0}, xy{0}, xz{0}, dx{0}, yx{0}, yy{0};
    typename TRANS::Scalar yz{0}, dy{0}, zx{0}, zy{0}, zz{0}, dz{0};
    t.GetComponents( xx, xy, xz, dx, yx, yy, yz, dy, zx, zy, zz, dz );
    // transfer to SIMD version
    using FP = typename SIMDTRANS::Scalar;
    simdT.SetComponents( FP( xx ), FP( xy ), FP( xz ), FP( dx ), //
                         FP( yx ), FP( yy ), FP( yz ), FP( dy ), //
                         FP( zx ), FP( zy ), FP( zz ), FP( dz ) );
  }

  /// Nicely formatted printout for transformations (better than ROOT's)
  template <typename STREAM, typename TRANS>
  STREAM& printTransform( STREAM& s, const TRANS& t ) {
    auto mend = []() {
      if constexpr ( std::is_same_v<MsgStream, STREAM> ) {
        return endmsg;
      } else {
        return "\n";
      }
    };
    // Extract the scalar parameters
    typename TRANS::Scalar xx{0}, xy{0}, xz{0}, dx{0}, yx{0}, yy{0};
    typename TRANS::Scalar yz{0}, dy{0}, zx{0}, zy{0}, zz{0}, dz{0};
    t.GetComponents( xx, xy, xz, dx, yx, yy, yz, dy, zx, zy, zz, dz );
    // print
    s << "[ " << xx << " " << xy << " " << xz << " " << dx << mend();
    s << "  " << yx << " " << yy << " " << yz << " " << dy << mend();
    s << "  " << zx << " " << zy << " " << zz << " " << dz << " ]";
    return s;
  }

} // namespace Rich::Detector
