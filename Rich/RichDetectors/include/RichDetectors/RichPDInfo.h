/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Det Desc
#include "DetDesc/ConditionKey.h"
#include "DetDesc/IConditionDerivationMgr.h"

// utils
#include "RichUtils/AllocateCount.h"
#include "RichUtils/RichSmartIDSorter.h"

// local
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich2.h"
#include "RichDetectors/RichPD.h"

// LHCbKernel
#include "Kernel/RichSmartID32.h"

// STL
#include <array>
#include <cassert>
#include <ostream>
#include <string>

namespace Rich::Detector {

  //-----------------------------------------------------------------------------
  /** @class PDInfo RichPDInfo.h
   *
   *  Helper class for accessing rich PD information
   *
   *  @author Chris Jones
   *  @date   2021-11-30
   */
  //-----------------------------------------------------------------------------

  class PDInfo final {

  public:
    // accessors

    /// Access the RICH object for a given SmartID
    inline auto rich( const LHCb::RichSmartID pdID ) const noexcept {
      const auto rich = pdID.rich();
      assert( m_riches[rich] );
      return m_riches[rich];
    }

    /// Access the PD panel for a given Smart ID
    inline auto pdPanel( const LHCb::RichSmartID pdID ) const noexcept { return rich( pdID )->pdPanel( pdID.panel() ); }

    /// Access the PD object for a given Smart ID
    inline auto dePD( const LHCb::RichSmartID pdID ) const noexcept { return pdPanel( pdID )->dePD( pdID ); }

    /// Is a given PD active ?
    inline bool pdIsActive( const LHCb::RichSmartID ) const noexcept {
      // TO DO: Implement this if/when required.
      return true;
    }

    /// Is a 'large' (H Type) PD
    inline auto isLargePD( const LHCb::RichSmartID pdID ) const noexcept { return pdPanel( pdID )->isLargePD( pdID ); }

    /// Returns a list of all (active and inactive) PDs identified by their RichSmartID
    inline const auto& allPDRichSmartIDs() const noexcept { return m_allPDSmartIDs; }

  public:
    /// Default constructor
    PDInfo() = default;

    /// Constructor from Rich Detectors
    PDInfo( const Detector::Rich1& rich1, //
            const Detector::Rich2& rich2 ) {

      // Cache pointers to detectors
      m_riches[Rich::Rich1] = &( rich1 );
      m_riches[Rich::Rich2] = &( rich2 );

      // clear current storage
      m_allPDSmartIDs.clear();
      // reserve guess
      m_allPDSmartIDs.reserve( LHCb::RichSmartID::MaPMT::TotalModules * LHCb::RichSmartID::MaPMT::MaxPDsPerModule );

      // Form the vector of all PD IDs from the RICH{1,2} objects
      for ( const auto* rich : m_riches ) {
        // loop over panels
        for ( const auto& p : rich->pdPanels() ) {
          // loop over modules in panel
          for ( const auto& m : p.pdModules() ) {
            // loop over PD in this module
            for ( const auto& pd : m ) {
              // if valid fill ID
              if ( pd.get() ) { m_allPDSmartIDs.emplace_back( pd.get()->pdSmartID() ); }
            }
          }
        }
      }

      // finally sort the PD list
      SmartIDSorter::sortByRegion( m_allPDSmartIDs );
    }

  private:
    // data

    /// cache the Rich1 and Rich2 objects
    Rich::DetectorArray<const Rich::Detector::RichBase*> m_riches;

    /// List of all PD RichSmartIDs
    LHCb::RichSmartID::Vector m_allPDSmartIDs;

  public:
    // messaging

    /// My name
    static constexpr auto MyName() noexcept { return "Rich::Detector::RichPDInfo"; }

    /// Overload ostream operator
    friend inline auto& operator<<( std::ostream& s, const PDInfo& ) {
      // ToDo : Print something useful here.
      return s << "[ " << MyName() << " ]";
    }

  private:
    /// Allocation tracking
    Rich::AllocateCount<PDInfo> m_track_instances;

  public:
    // conditions handling

    /// Default conditions name
    inline static const std::string DefaultConditionKey = DeRichLocations::derivedCondition( "RichPDInfo" );

    /// Static generator function
    static auto generate( const Detector::Rich1& r1, //
                          const Detector::Rich2& r2 ) {
      auto msgSvc = Gaudi::svcLocator()->service<IMessageSvc>( "MessageSvc" );
      assert( msgSvc );
      MsgStream log( msgSvc, MyName() );
      log << MSG::DEBUG << "Update triggered" << endmsg;
      return PDInfo{r1, r2};
    }

    /// Creates a condition derivation for the given key
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key = DefaultConditionKey ) {
      // Need to first add the required RICH detectors
      Detector::Rich1::addConditionDerivation( parent );
      Detector::Rich2::addConditionDerivation( parent );
      // Now instanciate the helper derived condition
      if ( parent->msgLevel( MSG::DEBUG ) ) {
        parent->debug() << "RichPDInfo::addConditionDerivation : Key=" << key << endmsg;
      }
      return parent->addSharedConditionDerivation( {Detector::Rich1::DefaultConditionKey,  // input conditions
                                                    Detector::Rich2::DefaultConditionKey}, //
                                                   std::move( key ),                       // output condition location
                                                   &generate );
    }
  };

} // namespace Rich::Detector
