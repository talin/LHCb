/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Det Desc
#include "DetDesc/ConditionKey.h"

// LHCbKernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSide.h"
#include "Kernel/RichSmartID.h"

// RichDet (temporary)
#include "RichDet/DeRichLocations.h"

// eventually should be moved elsewhere
#include "RichDet/Rich1DTabProperty.h"

// LHCbMath
#include "LHCbMath/FastMaths.h"
#include "LHCbMath/SIMDTypes.h"

// Utils
#include "RichUtils/AllocateCount.h"
#include "RichUtils/RichMirrorSegPosition.h"
#include "RichUtils/RichSIMDRayTracing.h"
#include "RichUtils/RichSIMDTypes.h"
#include "RichUtils/ZipRange.h"

// Local
#include "RichDetectors/RichBeamPipe.h"
#include "RichDetectors/RichMirror.h"
#include "RichDetectors/RichPD.h"
#include "RichDetectors/RichPDPanel.h"
#include "RichDetectors/Utilities.h"

// Gaudi
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/MsgStream.h"

// STL
#include <array>
#include <cassert>
#include <memory>
#include <optional>
#include <ostream>
#include <sstream>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

namespace Rich::Detector {

  namespace details {

    /// Common base class for all Riches
    template <typename BASEDETELEM>
    class alignas( LHCb::SIMD::VectorAlignment ) RichBase
        : public LHCb::SIMD::AlignedBase<LHCb::SIMD::VectorAlignment> {

    public:
      // types

      /// expose underlying detector element type
      using BaseDetElem = BASEDETELEM;

      /// Type for container of allocated mirror objects
      using Mirrors = std::vector<std::shared_ptr<const Mirror>>;

      /// Type for PD Panel storage
      using PDPanels = PanelArray<PDPanel>;

      /// The PD type to use in public API
      using PD = Rich::Detector::PD;

      /// Beampipe
      using BeamPipe = Rich::Detector::BeamPipe;

      /// type for SIMD ray tracing result
      using SIMDRayTResult = PDPanel::SIMDRayTResult;
      /// scalar FP type for SIMD objects
      using FP = PDPanel::FP;
      /// SIMD float type
      using SIMDFP = PDPanel::SIMDFP;
      /// Array of PD pointers
      using SIMDPDs = PDPanel::SIMDPDs;
      /// Array of SmartIDs
      using SIMDSmartIDs = PDPanel::SIMDSmartIDs;
      /// SIMD Point
      using SIMDPoint = PDPanel::SIMDPoint;

      /// Tabulated function type
      using TabFunc = const Rich::TabulatedFunction1D;

    private:
      // data

      /// RICH type
      Rich::DetectorType m_type = Rich::InvalidDetector;

      /// Underlying detector element
      const BASEDETELEM* m_rich = nullptr;

      /// Nominal primary mirror radius of curvature
      alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_sphMirrorRadiusSIMD{};

      /// Nominal planes for each panel
      Rich::PanelArray<Gaudi::Plane3D> m_nominalPlanes = {{}};
      /// SIMD Nominal planes for each panel
      alignas( LHCb::SIMD::VectorAlignment ) PanelArray<Rich::SIMD::Plane<FP>> m_nominalPlanesSIMD = {{}};

      /// The nominal normal vector of the flat mirror planes
      Rich::PanelArray<Gaudi::XYZVector> m_nominalNormals = {{}};
      /// SIMD nominal normal vector of the flat mirror planes
      alignas( LHCb::SIMD::VectorAlignment ) PanelArray<Rich::SIMD::Vector<FP>> m_nominalNormalsSIMD = {{}};

      /// The nominal centres of curvature of the spherical mirrors
      Rich::PanelArray<Gaudi::XYZPoint> m_nominalCentresOfCurvature = {{}};
      /// The nominal centres of curvature of the spherical mirrors
      alignas( LHCb::SIMD::VectorAlignment ) PanelArray<Rich::SIMD::Point<FP>> m_nominalCentresOfCurvatureSIMD = {{}};

      int m_nSphMirrorSegRows{0}; ///< number of spherical mirror rows
      int m_nSphMirrorSegCols{0}; ///< number of spherical mirror columns
      int m_nSecMirrorSegRows{0}; ///< number of secondary mirror rows
      int m_nSecMirrorSegCols{0}; ///< number of secondary mirror columns

      /// PD quantum efficiency
      std::shared_ptr<TabFunc> m_nominalPDQuantumEff;
      /// spherical mirror reflectivity
      std::shared_ptr<TabFunc> m_nominalSphMirrorRefl;
      /// secondary mirror reflectivity
      std::shared_ptr<TabFunc> m_nominalSecMirrorRefl;
      /// absorption length of the quartz gas window
      std::shared_ptr<TabFunc> m_gasWinAbsLength;

      /// Owned beampipe object
      alignas( LHCb::SIMD::VectorAlignment ) std::optional<BeamPipe> m_beampipe;

    protected:
      // data

      /// The owned PD Panel objects, for each detector side
      alignas( LHCb::SIMD::VectorAlignment ) PDPanels m_panels;

      /// The owned primary mirror objects
      Mirrors m_primaryMirrors;

      /// The owned secondary mirror objects
      Mirrors m_secondaryMirrors;

    public:
      // constructors

#ifdef USE_DD4HEP
      /// Constructor from DD4HEP object
      template <typename DET>
      RichBase( const DET& det )
          : m_type( det.rich() ) //
          , m_rich( det.access() )
          , m_panels( {PDPanel( m_type, Rich::firstSide, det.PhDetPanel( Rich::firstSide ) ), //
                       PDPanel( m_type, Rich::secondSide, det.PhDetPanel( Rich::secondSide ) )} ) {

        // owned mirrors
        m_primaryMirrors   = convertMirrors( det.primaryMirrors() );
        m_secondaryMirrors = convertMirrors( det.secondaryMirrors() );

        // misc. parameters
        m_nSphMirrorSegRows = det.nSphMirrorSegRows();
        m_nSphMirrorSegCols = det.nSphMirrorSegCols();
        m_nSecMirrorSegRows = det.nSecMirrorSegRows();
        m_nSecMirrorSegCols = det.nSecMirrorSegCols();

        // tabulated properties
        m_nominalPDQuantumEff  = std::make_shared<TabFunc>( det.nominalPDQuantumEff() );
        m_nominalSphMirrorRefl = std::make_shared<TabFunc>( det.nominalSphMirrorRefl() );
        m_nominalSecMirrorRefl = std::make_shared<TabFunc>( det.nominalSecMirrorRefl() );
        m_gasWinAbsLength      = std::make_shared<TabFunc>( det.gasWinAbsLength() );

        // The nominal mirror parameters
        PanelArray<std::array<double, 4>> planeParams{{}};
        PanelArray<std::size_t>           nMirrors{{}};
        for ( const auto& m : m_secondaryMirrors ) {
          assert( Rich::InvalidSide != m->side() );
          // params for this side
          auto& params = planeParams[m->side()];
          // get the plane for this mirror segment
          const auto& p = m->centreNormalPlane();
          params[0] += p.A();
          params[1] += p.B();
          params[2] += p.C();
          params[3] += p.D();
          ++nMirrors[m->side()];
        }
        for ( const auto side : Rich::sides() ) {
          assert( nMirrors[side] > 0 );
          const Gaudi::Plane3D p( planeParams[side][0] / nMirrors[side], //
                                  planeParams[side][1] / nMirrors[side], //
                                  planeParams[side][2] / nMirrors[side], //
                                  planeParams[side][3] / nMirrors[side] );
          m_nominalPlanes[side]                 = p;
          m_nominalPlanesSIMD[side]             = Rich::SIMD::Plane<FP>( (FP)p.A(), (FP)p.B(), (FP)p.C(), (FP)p.D() );
          m_nominalNormals[side]                = p.Normal();
          m_nominalNormalsSIMD[side]            = p.Normal();
          const auto c                          = det.nominalCentreOfCurvature( side );
          m_nominalCentresOfCurvature[side]     = c;
          m_nominalCentresOfCurvatureSIMD[side] = c;
        }
        m_sphMirrorRadiusSIMD = SIMDFP( det.sphMirrorRadius() );
        // beampipe
        using namespace LHCb::Detector::detail;
        if ( Rich::Rich1 == m_type ) {
          m_beampipe.emplace( dd4hep_param<double>( "Rh1BeamPipeConeBeginInLHCbZ" ),
                              dd4hep_param<double>( "Rh1BeamPipeConeEndInLHCbZ" ),
                              dd4hep_param<double>( "Rh1BeamPipeConeOuterRad1" ),
                              dd4hep_param<double>( "Rh1BeamPipeConeOuterRad2" ) );

        } else {
          const auto coneStartZ = dd4hep_param<double>( "Rh2BeamHoleConeZBeginInLHCb" );
          const auto coneSizeZ  = dd4hep_param<double>( "Rh2BeamHoleConeZSize" );
          m_beampipe.emplace( coneStartZ, coneStartZ + coneSizeZ,
                              dd4hep_param<double>( "Rh2MirrBeamHoleConeUpstreamRadius" ),
                              dd4hep_param<double>( "Rh2MirrBeamHoleConeDownstreamRadius" ) );
        }
      }
#else
      /// Constructor from DetDesc object
      template <typename DET>
      RichBase( const DET& det )
          : m_type( det.rich() )                                   //
          , m_rich( &det )                                         //
          , m_panels( {PDPanel( *det.pdPanel( Rich::firstSide ) ), //
                       PDPanel( *det.pdPanel( Rich::secondSide ) )} ) {
        // mirror parameters
        m_sphMirrorRadiusSIMD = det.sphMirrorRadiusSIMD();
        for ( const auto side : Rich::sides() ) {
          m_nominalPlanes[side]                 = det.nominalPlane( side );
          m_nominalPlanesSIMD[side]             = det.nominalPlaneSIMD( side );
          m_nominalNormals[side]                = det.nominalNormal( side );
          m_nominalNormalsSIMD[side]            = det.nominalNormalSIMD( side );
          m_nominalCentresOfCurvature[side]     = det.nominalCentreOfCurvature( side );
          m_nominalCentresOfCurvatureSIMD[side] = det.nominalCentreOfCurvatureSIMD( side );
        }
        // owned mirrors
        m_primaryMirrors   = convertMirrors( det.primaryMirrors() );
        m_secondaryMirrors = convertMirrors( det.secondaryMirrors() );
        // misc. parameters
        m_nSphMirrorSegRows = det.template param<int>( "SphMirrorSegRows" );
        m_nSphMirrorSegCols = det.template param<int>( "SphMirrorSegColumns" );
        m_nSecMirrorSegRows = det.template param<int>( "SecMirrorSegRows" );
        m_nSecMirrorSegCols = det.template param<int>( "SecMirrorSegColumns" );
        // tabulated properties
        m_nominalPDQuantumEff  = det.nominalPDQuantumEff();
        m_nominalSphMirrorRefl = det.nominalSphMirrorRefl();
        m_nominalSecMirrorRefl = det.nominalSecMirrorRefl();
        m_gasWinAbsLength      = det.gasWinAbsLength();
        // beampipe
        m_beampipe.emplace( *det.beampipe() );
      }
#endif

    private:
      // methods

      /// Get access to the underlying object
      inline auto get() const noexcept {
        assert( m_rich );
        return m_rich;
      }

      /// Creates wrapped instances of the mirror objects
      template <typename MIRRORS>
      inline Mirrors convertMirrors( MIRRORS&& in_mirrs ) const {
        Mirrors out_mirrs;
        out_mirrs.reserve( in_mirrs.size() );
        for ( const auto& m : in_mirrs ) { out_mirrs.emplace_back( std::make_unique<const Mirror>( m ) ); }
        return out_mirrs;
      }

    public:
      // Accesssors
      // Methods listed here are still being forwarded to the underlying detector element object

      /// Access a parameter
      template <typename TYPE>
      inline auto param( std::string name ) const {
        return get()->template param<TYPE>( std::move( name ) );
      }

    public:
      // Accessors

      /// Beampipe
      inline const auto& beampipe() const noexcept { return m_beampipe.value(); }

      /// primary mirrors
      inline const auto& primaryMirrors() const noexcept { return m_primaryMirrors; }

      /// secondary mirrors
      inline const auto& secondaryMirrors() const noexcept { return m_secondaryMirrors; }

      // nominal (average) PD QE curve
      inline auto nominalPDQuantumEff() const noexcept { return m_nominalPDQuantumEff.get(); }

      /// nominal (average) primary mirror reflectivity curve
      inline auto nominalSphMirrorRefl() const noexcept { return m_nominalSphMirrorRefl.get(); }

      /// nominal (average) secondary mirror reflectivity curve
      inline auto nominalSecMirrorRefl() const noexcept { return m_nominalSecMirrorRefl.get(); }

      /// Absorption length of the gas quartz window
      inline auto gasWinAbsLength() const noexcept { return m_gasWinAbsLength.get(); }

      /**
       * Method to find the row/column of a spherical mirror segment.  It can
       * be used to test if the mirror segment is at the edge or not
       * @return Position (row/column) for this spherical mirror segment
       */
      inline auto sphMirrorSegPos( const int mirrorNumber ) const {
        Rich::MirrorSegPosition mirrorPos;
        int                     row = mirrorNumber / m_nSphMirrorSegCols;
        if ( row >= m_nSphMirrorSegRows ) { row -= m_nSphMirrorSegRows; }
        mirrorPos.row    = row;
        mirrorPos.column = ( mirrorNumber % m_nSphMirrorSegCols );
        return mirrorPos;
      }

      /**
       * Method to find the row/column of a flat mirror segment. It can be used to
       * test if the mirror segment is at the edge or not
       *
       * @return Position (row/column) for this flat mirror segment
       */
      inline auto secMirrorSegPos( const int mirrorNumber ) const {
        Rich::MirrorSegPosition mirrorPos;
        int                     row = mirrorNumber / m_nSecMirrorSegCols;
        if ( row >= m_nSecMirrorSegRows ) { row -= m_nSecMirrorSegRows; }
        mirrorPos.row    = row;
        mirrorPos.column = ( mirrorNumber % m_nSecMirrorSegCols );
        return mirrorPos;
      }

      /// number of spherical mirror rows
      inline auto nSphMirrorSegRows() const noexcept { return m_nSphMirrorSegRows; }
      /// number of spherical mirror columns
      inline auto nSphMirrorSegCols() const noexcept { return m_nSphMirrorSegCols; }
      /// number of secondary mirror rows
      inline auto nSecMirrorSegRows() const noexcept { return m_nSecMirrorSegRows; }
      /// number of secondary mirror columns
      inline auto nSecMirrorSegCols() const noexcept { return m_nSecMirrorSegCols; }

      /// The RICH type
      inline auto rich() const noexcept { return m_type; }

      /// Returns the RICH side a given point lies in
      template <typename POINT>
      inline auto side( const POINT& point ) const noexcept {
        return side( point.x(), point.y() );
      }

      // Returns the RICH side a given (x,y) point lies in
      template <typename TYPE>
      inline auto side( const TYPE x, const TYPE y ) const noexcept {
        if constexpr ( std::is_arithmetic<TYPE>::value ) {
          // Scalar
          return ( Rich::Rich1 == rich() ? ( y < 0 ? Rich::bottom : Rich::top )
                                         : ( x < 0 ? Rich::right : Rich::left ) );
        } else {
          // SIMD
          using Sides = Rich::SIMD::Sides;
          Sides sides( (int)Rich::firstSide ); // R1 top or R2 left
          // update as needed to R1 bottom or R2 right
          // Is there a better way to do ??
          if ( Rich::Rich1 == rich() ) {
            sides( LHCb::SIMD::simd_cast<Sides::mask_type>( y < TYPE::Zero() ) ) = Sides( (int)Rich::secondSide );
          } else {
            sides( LHCb::SIMD::simd_cast<Sides::mask_type>( x < TYPE::Zero() ) ) = Sides( (int)Rich::secondSide );
          }
          return sides;
        }
      }

      /// 'Nominal' Spherical mirror radius
      inline auto sphMirrorRadius() const noexcept { return m_sphMirrorRadiusSIMD[0]; }

      /// 'Nominal' Spherical mirror radius (SIMD)
      inline auto sphMirrorRadiusSIMD() const noexcept { return m_sphMirrorRadiusSIMD; }

      /**
       * Returns the nominal centres of curvature of the spherical mirror for
       * this Rich for the given sides
       *
       * @param sides Which sides : top, bottom (Rich1), left, right (Rich2)
       * @return The nominal centre of curvature
       */
      inline auto nominalCentreOfCurvature( const Rich::SIMD::Sides& sides ) const noexcept {
        using namespace Rich::SIMD;
        using mask_type = Point<FP>::Scalar::mask_type;
        // Start by making CoCs for each side
        const auto& CoC1( nominalCentreOfCurvatureSIMD( Rich::firstSide ) );
        const auto& CoC2( nominalCentreOfCurvatureSIMD( Rich::secondSide ) );
        // local copy of X,Y,Z for first side
        auto X = CoC1.X();
        auto Y = CoC1.Y();
        auto Z = CoC1.Z();
        // mask for side 2
        const auto m = LHCb::SIMD::simd_cast<mask_type>( sides == Rich::SIMD::Sides( (int)Rich::secondSide ) );
        // update values for side 2
        X( m ) = CoC2.X();
        Y( m ) = CoC2.Y();
        Z( m ) = CoC2.Z();
        // return the final result
        return Point<FP>( X, Y, Z );
      }

      /// Access CoC for given side(s)
      inline const auto& nominalCentreOfCurvature( const Rich::Side side ) const noexcept {
        return m_nominalCentresOfCurvature[side];
      }

      /// Access CoC (SIMD) for given side
      inline const auto& nominalCentreOfCurvatureSIMD( const Rich::Side side ) const noexcept {
        return m_nominalCentresOfCurvatureSIMD[side];
      }

      /// Access nominal plane for given side
      inline const auto& nominalPlane( const Rich::Side side ) const noexcept { return m_nominalPlanes[side]; }

      /**
       * Returns the nominal flat mirror plane for this Rich
       *
       * @param sides Which sides : top, bottom (Rich1), left, right (Rich2)
       * @return The nominal flat mirror plane
       */
      inline auto nominalPlane( const Rich::SIMD::Sides& sides ) const noexcept {
        using namespace Rich::SIMD;
        using mask_type = Point<FP>::Scalar::mask_type;
        // start with the SIMD planes for each side
        const auto& P1( nominalPlaneSIMD( Rich::firstSide ) );
        const auto& P2( nominalPlaneSIMD( Rich::secondSide ) );
        // local copy of A,B,C,D for first side
        auto A = P1.A();
        auto B = P1.B();
        auto C = P1.C();
        auto D = P1.D();
        // mask for side 2
        const auto m = LHCb::SIMD::simd_cast<mask_type>( sides == Rich::SIMD::Sides( (int)Rich::secondSide ) );
        // update values for side 2
        A( m ) = P2.A();
        B( m ) = P2.B();
        C( m ) = P2.C();
        D( m ) = P2.D();
        // return the SIMD plane object
        return Plane<FP>( A, B, C, D );
      }

      /// Access nominal plane for a given detector side (SIMD)
      inline const auto& nominalPlaneSIMD( const Rich::Side side ) const noexcept { return m_nominalPlanesSIMD[side]; }

      /// Returns the nominal normal vector of the flat mirror plane for this Rich
      inline const auto& nominalNormal( const Rich::Side side ) const noexcept { return m_nominalNormals[side]; }

      /// Returns the SIMD nominal normal vector of the flat mirror plane for this Rich
      inline const auto& nominalNormalSIMD( const Rich::Side side ) const noexcept {
        return m_nominalNormalsSIMD[side];
      }

      /// Access PD Panels
      inline const auto& pdPanels() const noexcept { return m_panels; }

      /// Access PD Panel for a given side
      inline auto pdPanel( const Rich::Side panel ) const noexcept { return &( pdPanels()[panel] ); }

    public:
      // ray tracing

      /// Ray trace a given direction with the given PD panel (scalar)
      inline auto rayTrace( const Rich::Side          side,    //
                            const Gaudi::XYZPoint&    pGlobal, //
                            const Gaudi::XYZVector&   vGlobal, //
                            const LHCb::RichTraceMode mode ) const {
        return pdPanel( side )->detIntersect( pGlobal, vGlobal, mode );
      }

      /// Ray trace a given direction with the given PD panel (SIMD)
      inline auto rayTrace( const Rich::Side              side,    //
                            const Rich::SIMD::Point<FP>&  pGlobal, //
                            const Rich::SIMD::Vector<FP>& vGlobal, //
                            const LHCb::RichTraceMode     mode ) const {
        return pdPanel( side )->detIntersectSIMD( pGlobal, vGlobal, mode );
      }

      /// Ray trace a given direction with the correct PD panel (SIMD)
      inline PDPanel::RayTStructSIMD rayTrace( const Rich::SIMD::Sides&      sides,   //
                                               const Rich::SIMD::Point<FP>&  pGlobal, //
                                               const Rich::SIMD::Vector<FP>& vGlobal, //
                                               const LHCb::RichTraceMode     mode ) const {
        using namespace LHCb::SIMD;

        // If all sides are the same, shortcut to a single call
        // hopefully the most common situation ...

        // side 1 mask
        const auto m1 = ( sides == Rich::SIMD::Sides( (int)Rich::firstSide ) );
        if ( all_of( m1 ) ) { return rayTrace( Rich::firstSide, pGlobal, vGlobal, mode ); }

        // side 2 mask
        const auto m2 = ( sides == Rich::SIMD::Sides( (int)Rich::secondSide ) );
        if ( all_of( m2 ) ) { return rayTrace( Rich::secondSide, pGlobal, vGlobal, mode ); }

        // we have a mixture... So must run both and merge..
        // Is there a better way to handle this ... ?

        // call for the first side
        auto res1 = rayTrace( Rich::firstSide, pGlobal, vGlobal, mode );
        // call for the second side
        auto res2 = rayTrace( Rich::secondSide, pGlobal, vGlobal, mode );

        // merge results2 into the returned results

        auto&      hitPosition  = std::get<SIMDPoint>( res1 );
        auto&      hitPosition2 = std::get<SIMDPoint>( res2 );
        const auto fm2          = LHCb::SIMD::simd_cast<SIMDFP::mask_type>( m2 );
        SIMDFP     hx( hitPosition.x() ), hy( hitPosition.y() ), hz( hitPosition.z() );
        hx( fm2 )   = hitPosition2.x();
        hy( fm2 )   = hitPosition2.y();
        hz( fm2 )   = hitPosition2.z();
        hitPosition = {hx, hy, hz};

        // copy m2 values from res2 to res1
        std::get<SIMDRayTResult::Results>( res1 )( m2 ) = std::get<SIMDRayTResult::Results>( res2 );

        // scalar loop for non-Vc types
        auto& smartID  = std::get<SIMDSmartIDs>( res1 );
        auto& smartID2 = std::get<SIMDSmartIDs>( res2 );
        auto& PDs      = std::get<SIMDPDs>( res1 );
        auto& PDs2     = std::get<SIMDPDs>( res2 );
        for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
          if ( m2[i] ) {
            smartID[i] = smartID2[i];
            PDs[i]     = PDs2[i];
          }
        }

        // return
        return res1;
      }

    protected:
      /// messaging
      template <typename STREAM>
      STREAM& fillStream( STREAM& s ) const {
        auto mend = []() {
          if constexpr ( std::is_same_v<MsgStream, STREAM> ) {
            return endmsg;
          } else {
            return "\n";
          }
        };
        s << "[ " << mend()                            //
          << " NomRoC=" << sphMirrorRadius() << mend() //
          << " NomCoCs=" << nominalCentreOfCurvature( Rich::firstSide ) << ","
          << nominalCentreOfCurvature( Rich::secondSide ) << mend() //
          << " NomPlaneNorms=" << nominalNormal( Rich::firstSide ) << "," << nominalNormal( Rich::secondSide )
          << mend() //
          << " NomPlaneD=" << nominalPlane( Rich::firstSide ).D() << "," << nominalPlane( Rich::secondSide ).D()
          << mend()                                                                                                 //
          << " nSphMirrorSegRows=" << nSphMirrorSegRows() << " nSphMirrorSegCols=" << nSphMirrorSegCols() << mend() //
          << " nSecMirrorSegRows=" << nSecMirrorSegRows() << " nSecMirrorSegCols=" << nSecMirrorSegCols() << mend() //
          << " nominalPDQuantumEff=" << *nominalPDQuantumEff() << mend()                                            //
          << " nominalSphMirrorRefl=" << *nominalSphMirrorRefl() << mend()                                          //
          << " nominalSecMirrorRefl=" << *nominalSecMirrorRefl() << mend()                                          //
          << " gasWinAbsLength=" << *gasWinAbsLength() << mend();
        s << " " << primaryMirrors().size() << " Primary Mirrors :-" << mend();
        for ( const auto& m : primaryMirrors() ) { s << " -> " << *m << mend(); }
        s << " " << secondaryMirrors().size() << " Secondary Mirrors :-" << mend();
        for ( const auto& m : secondaryMirrors() ) { s << " -> " << *m << mend(); }
        s << " " << pdPanels().size() << " PD Panels :-" << mend();
        for ( const auto& p : pdPanels() ) {
          s << " -> " << p << mend();
          // print all PDs
          for ( const auto& pdMod : p.pdModules() ) {
            bool newMod = true;
            for ( const auto& pd : pdMod ) {
              if ( pd.get() ) {
                if ( newMod ) {
                  const auto id = pd.get()->pdSmartID();
                  s << "  -> PD Module " << id.pdMod() << " " << id.panelID()
                    << " Module(Col:NInCol)=" << id.panelLocalModuleColumn() << ":" << id.columnLocalModuleNum()
                    << mend();
                  newMod = false;
                }
                s << "   -> " << *pd.get() << mend();
              }
            }
          }
        }
        s << " " << beampipe() << mend();
        using Names = std::vector<std::string>;
        // Yes, some CF4 parameters are being read from DeRich1. For some reason this is how things
        // are set up for DetDesc. Once DetDesc is dropped they can be moved back to DeRich2.
        const auto dnames = ( Rich::Rich1 == rich() ? Names{"SellC4F10F1Param",
                                                            "SellC4F10F2Param",
                                                            "SellC4F10E1Param",
                                                            "SellC4F10E2Param",
                                                            "SellCF4F1Param",
                                                            "SellCF4F2Param",
                                                            "SellCF4E1Param",
                                                            "SellCF4E2Param",
                                                            "GasMolWeightC4F10Param",
                                                            "RhoEffectiveSellC4F10Param",
                                                            "GasMolWeightCF4Param",
                                                            "SellLorGasFacParam",
                                                            "RhoEffectiveSellCF4Param",
                                                            "Rich1GasQuartzWindowThickness",
                                                            "RichPmtPixelXSize",
                                                            "RichPmtPixelYSize",
                                                            "RichGrandPmtPixelXSize",
                                                            "RichGrandPmtPixelYSize",
                                                            "RichPmtAnodeXSize",
                                                            "RichPmtAnodeYSize",
                                                            "RichGrandPmtAnodeXSize",
                                                            "RichGrandPmtAnodeYSize",
#ifdef USE_DD4HEP
                                                            "Rh1PhDetSupXSize",
                                                            "Rh1PhDetSupYSize",
#endif
                                                            "Rich1PmtDetPlaneZInPmtPanel",
                                                            "Rich2PmtDetPlaneZInPmtPanel"}
                                                    : Names{"Rich2GasQuartzWindowThickness",
#ifdef USE_DD4HEP
                                                            "Rh2PDPanelSizeX", "Rh2PDPanelSizeY"
#endif
                                                      } );
        for ( const auto& n : dnames ) {
          s << " Parameter<double>         '" << n << "' = " << param<double>( n ) << mend();
        }
        const auto inames = ( Rich::Rich1 == rich() ? Names{"RichPmtNumPixelCol", "RichPmtNumPixelRow"} : Names{} );
        for ( const auto& n : inames ) {
          s << " Parameter<int>            '" << n << "' = " << param<int>( n ) << mend();
        }
        const auto dvnames =
            ( Rich::Rich1 == rich() ? Names{"Rich1PMTModulePlaneHalfSize", "Rich2MixedPMTModulePlaneHalfSize"}
                                    : Names{} );
#ifndef USE_DD4HEP
        for ( const auto& n : dvnames ) {
          s << " Parameter<vector<double>> '" << n << "' = " << param<std::vector<double>>( n ) << mend();
        }
#endif

        return s << "]";
      }
    };

    /// templated class for each top level Rich object
    template <Rich::DetectorType RICH, typename DETELEM, typename BASEDETELEM, typename RADIATOR>
    class alignas( LHCb::SIMD::VectorAlignment ) RichX final : public details::RichBase<BASEDETELEM> {

    public:
      // types

      // expose base class types
      using DetElem     = DETELEM;
      using BaseDetElem = BASEDETELEM;
      using Base        = details::RichBase<BASEDETELEM>;
      // Shortcut to this type
      using MyType = RichX<RICH, DETELEM, BASEDETELEM, RADIATOR>;

    private:
      /// Owned radiator object
      alignas( LHCb::SIMD::VectorAlignment ) RADIATOR m_rad;

    public:
      /// Access the owned radiator
      const auto& radiator() const noexcept { return m_rad; }

    private:
      /// Allocation tracking
      AllocateCount<MyType> m_track_instances;

    public:
      // constructors

      /// Constructor from detector element
      RichX( const DETELEM& deRich )
          : details::RichBase<BASEDETELEM>( deRich ) //
          , m_rad( deRich.radiatorGas() ) {}

    public:
      // messaging

      /// name string
      static constexpr auto MyName() noexcept {
        return ( RICH == Rich::Rich1 ? "Rich::Detector::Rich1" : //
                     RICH == Rich::Rich2 ? "Rich::Detector::Rich2" : "UNDEFINED" );
      }

      /// Overload ostream operator
      friend inline auto& operator<<( MsgStream& s, const MyType& r ) {
        s << MyName() << " ";
        r.fillStream( s );
        return s << " " << r.radiator();
      }

      /// Overload ostream operator
      friend inline auto& operator<<( std::ostream& s, const MyType& r ) {
        s << MyName() << " ";
        r.fillStream( s );
        return s << " " << r.radiator();
      }

    public:
      // conditions handling

      /// Construct conditions name
      inline static auto conditionKey( std::string tag = "DerivedDet" ) noexcept {
        return DeRichLocations::derivedCondition<DETELEM>( std::move( tag ) );
      }

      /// Default conditions name
      inline static const std::string DefaultConditionKey = conditionKey();

      /// static generator function
      static auto generate( const DETELEM& r ) {
        auto msgSvc = Gaudi::svcLocator()->service<IMessageSvc>( "MessageSvc" );
        assert( msgSvc );
        MsgStream log( msgSvc, MyName() );
        log << MSG::DEBUG << "Update triggered" << endmsg;
        return RichX<RICH, DETELEM, BASEDETELEM, RADIATOR>( r );
      }

      /// Creates a condition derivation for the given key
      template <typename PARENT>
      static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key = conditionKey() ) {
        assert( parent );
        assert( !key.empty() );
        if ( parent->msgLevel( MSG::DEBUG ) ) {
          parent->debug() << MyName() << "::addConditionDerivation : " << DeRichLocations::location<DETELEM>()
                          << " Key='" << key << "'" << endmsg;
        }
        return parent->addSharedConditionDerivation( //
            {DeRichLocations::location<DETELEM>()},  // input condition locations
            std::move( key ),                        // output derived condition location
            &generate );
      }
    };

  } // namespace details

#ifdef USE_DD4HEP
  using RichBase = details::RichBase<LHCb::Detector::DeRich>;
#else
  using RichBase = details::RichBase<DeRich>;
#endif

} // namespace Rich::Detector
