/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// LHCbMath
#include "LHCbMath/FastMaths.h"
#include "LHCbMath/SIMDTypes.h"
#include "RichUtils/RichSIMDTypes.h"

// local
#include "RichDetectors/Utilities.h"

// STL
#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdint>
#include <optional>
#include <type_traits>
#include <utility>

namespace Rich::Detector {

  /** Basic beampipe object for fast intersection checks
   *  Simplied to a conical section around the z axis in global coordinates.
   *  Strictly not 100% correct (slight tilt in RICH2) but good enough and
   *  allows the checks to be fast without need for reference frame
   *  transformations etc. */
  class alignas( LHCb::SIMD::VectorAlignment ) BeamPipe final
      : public LHCb::SIMD::AlignedBase<LHCb::SIMD::VectorAlignment> {

  public:
    // types

    /// Enum describing the various possible types of intersection
    enum BeamPipeIntersectionType : std::uint8_t {
      NoIntersection = 0, ///< Did not intersect the beam pipe at all
      FrontAndBackFace,   ///< Entered via the front face and left via the back face
      FrontFaceAndCone,   ///< Entered via the front face and left via the cone surface
      BackFaceAndCone,    ///< Entered via the cone surafece and left via the backface
      ConeOnly            ///< Entered via the cone surafece and left via the cone surface
    };

    /// SIMD array for beam intersection types
    using SIMDBeamIntersects = Rich::SIMD::STDArray<BeamPipeIntersectionType>;

    /// SIMD float type
    using SIMDFP = Rich::SIMD::FP<Rich::SIMD::DefaultScalarFP>;

  private:
    /// Get the beam pipe R at the given z position
    template <typename TYPE>
    inline auto getRatZ( const TYPE z ) const noexcept {
      // Use linear parameterisation  R = a.z + b
      if constexpr ( std::is_arithmetic_v<TYPE> ) {
        return ( scalar( m_a ) * z ) + scalar( m_b );
      } else {
        return ( m_a * z ) + m_b;
      }
    }

    /// Propagate a given trajectory (POINT,VECTOR pair) to a given z
    template <typename POINT, typename VECTOR, typename FP = typename POINT::Scalar>
    inline auto propagateToZ( const POINT& ptn, const VECTOR& dir, const FP& z ) const noexcept {
      // Distance in z
      const auto zDiff = z - ptn.z();
      // form new point at requested shift in z
      return ptn + ( ( zDiff / dir.z() ) * dir );
    }

  public:
    /// Constructor from conical section parameters
    BeamPipe( const double startZ, const double endZ, //
              const double startR, const double endR )
        : m_startZ( startZ )
        , m_endZ( endZ )
        , m_startR2( startR * startR )
        , m_endR2( endR * endR )
        , m_zmin( startZ )
        , m_zmax( endZ )
        , m_r2min( startR * startR )
        , m_r2max( endR * endR )
        , m_rmin( startR )
        , m_rmax( endR ) {
      m_a = SIMDFP( m_rmin - m_rmax ) / SIMDFP( m_zmin - m_zmax );
      m_b = SIMDFP( m_rmin ) - ( m_a * SIMDFP( m_zmin ) );
    }

    /// Constructor from a detector element object
    template <typename DETELEM>
    BeamPipe( const DETELEM& bp ) : BeamPipe( bp.startZ(), bp.endZ(), bp.startRadius(), bp.endRadius() ) {}

  public:
    /** Gets the intersection points for a given trajectory
     *  for now only need to support scalar version here.
     *  SIMD support could be added if/when needed */
    inline auto intersectionPoints( const Gaudi::XYZPoint& start,      //
                                    const Gaudi::XYZPoint& end,        //
                                    Gaudi::XYZPoint&       entryPoint, //
                                    Gaudi::XYZPoint&       exitPoint ) const {

      // Default to fully outside the cone
      BeamPipeIntersectionType intType = NoIntersection;

      // direction vector
      const auto dir = ( end - start );

      // Track is represented by the linear equations   y(z) = m.z + c
      //                                                x(z) = n.z + d
      const auto zDiffInv = 1.0 / ( start.z() - end.z() );
      const auto m        = ( start.y() - end.y() ) * zDiffInv;
      const auto n        = ( start.x() - end.x() ) * zDiffInv;
      const auto c        = start.y() - ( m * start.z() );
      const auto d        = start.x() - ( n * start.z() );

      // Compute z position at which P.O.C.A. occurs, clamped to valid z range
      // computed by finding z which makes d(r^2)/dz = 0
      // where r is the track radial distance from the z axis, i.e. r^2 = x^2 + y^2
      const auto closestZ = std::clamp( -1.0 * ( ( d * n ) + ( m * c ) ) / ( ( n * n ) + ( m * m ) ), m_zmin, m_zmax );
      // check the poca to see if we need to perform more detailed checks.
      // get poca z position, clamped to the min max range for given RICH
      const auto pocaPtn = propagateToZ( start, dir, closestZ );
      const auto pocaR2  = std::pow( pocaPtn.x(), 2 ) + std::pow( pocaPtn.y(), 2 );
      // Get beampipe R at this z position
      const auto curPipeR  = getRatZ( closestZ );
      const auto isfarAway = ( pocaR2 > ( curPipeR * curPipeR ) );
      if ( !isfarAway ) {

        // Cone distance from beam line R^2 = x^2 + y^2  is linear, R = a.z + b
        // Functor to compute the intersection(s) (if they exist) with the beam pipe cone
        // algorithm then solves for z when R^2 = x^2 + y^2 (track) which leads to a quadratic
        // equation that either has two real solutions (when the track intersections the
        // cone, or no real solutions when it does not.
        // Returns a pair of z values for where the track crosses the sides, sorted in z.
        // Note both values are optional, as even if there is a mathematical solution
        // yield real values, if they outside the z range of the cone segment no value is returned.
        // quadratic equation is    A.z^2 + B.z + C = 0
        // where                    A = n^2 + m^2 - a^2
        //                          B = 2( n.d - m.c - a.b )
        //                          C = d^2 + c^2 - b^2
        auto intersectConeSides = [&]() {
          // could maybe cache some of the parameters here later on for performance..
          const auto A = ( ( n * n ) + ( m * m ) - scalar( m_a * m_a ) );
          const auto B = 2.0 * ( ( n * d ) + ( m * c ) - scalar( m_a * m_b ) );
          const auto C = ( d * d ) + ( c * c ) - scalar( m_b * m_b );
          // Do we have real solutions ?
          const auto XX2 = ( B * B ) - ( 4.0 * A * C );
          // return values are optional (i.e. no solutions are possible)
          std::pair<std::optional<double>, std::optional<double>> z_vals;
          if ( XX2 >= 0 ) {
            const auto XX    = std::sqrt( XX2 );
            const auto denom = 0.5 / A;
            auto       z1    = ( -B - XX ) * denom;
            auto       z2    = ( -B + XX ) * denom;
            // ensure sorted
            if ( z1 > z2 ) { std::swap( z1, z2 ); }
            // only set the values if they are in the valid z range for the beam pipe
            if ( z1 > m_zmin && z1 < m_zmax ) { z_vals.first = z1; }
            if ( z2 > m_zmin && z2 < m_zmax ) { z_vals.second = z2; }
          }
          return z_vals;
        };

        // get the starting values for the entry and exit points
        entryPoint = propagateToZ( start, dir, m_zmin );
        exitPoint  = propagateToZ( start, dir, m_zmax );

        // Are these points inside the cone radius at these points ?
        const auto entryR2   = std::pow( entryPoint.x(), 2 ) + std::pow( entryPoint.y(), 2 );
        const auto exitR2    = std::pow( exitPoint.x(), 2 ) + std::pow( exitPoint.y(), 2 );
        const auto isInStart = entryR2 < m_r2min;
        const auto isInEnd   = exitR2 < m_r2max;

        if ( isInStart && isInEnd ) {
          // fully inside the cone
          intType = FrontAndBackFace;
        } else if ( isInStart && !isInEnd ) {
          const auto z_sides = intersectConeSides();
          if ( z_sides.second.has_value() ) {
            // hits front face and passes out through side
            intType = FrontFaceAndCone;
            // Move exit point to cone side intersection point
            exitPoint = propagateToZ( start, dir, z_sides.second.value() );
          }
        } else if ( !isInStart && isInEnd ) {
          const auto z_sides = intersectConeSides();
          if ( z_sides.first.has_value() ) {
            // hits side and passes out through end face
            intType = BackFaceAndCone;
            // Move entry point to cone side intersection point
            entryPoint = propagateToZ( start, dir, z_sides.first.value() );
          }
        } else {
          // test if the track hits the cone at all
          const auto z_sides = intersectConeSides();
          if ( z_sides.first.has_value() && z_sides.second.has_value() ) {
            // goes in and out of the cone side
            intType = ConeOnly;
            // move both points
            entryPoint = propagateToZ( start, dir, z_sides.first.value() );
            exitPoint  = propagateToZ( start, dir, z_sides.second.value() );
          } else {
            // no intersections in the valid z range
            entryPoint = {0, 0, 0};
            exitPoint  = {0, 0, 0};
          }
        }

      } else {
        // track is far far away from beam pipe so skip any further checks
        entryPoint = {0, 0, 0};
        exitPoint  = {0, 0, 0};
      }

      return intType;
    }

    /** Tests if a given direction, given by start and end pints, intersects the pipe.
     *  Similar to, but simplier than, intersectionPoints as it does not have to
     *  provide the 3D intersection points, just if the trajectory intersects or not. */
    template <typename POINT>
    inline auto testForIntersection( const POINT& start, const POINT& end ) const {

      // FP type (scalar or SIMD)
      using FP = typename POINT::Scalar;

      // direction vector
      const auto dir = ( end - start );

      // Track is represented by the linear equations   y(z) = m.z + c
      //                                                x(z) = n.z + d
      const auto zDiffInv = FP( 1.0 ) / ( start.z() - end.z() );
      const auto m        = ( start.y() - end.y() ) * zDiffInv;
      const auto n        = ( start.x() - end.x() ) * zDiffInv;
      const auto c        = start.y() - ( m * start.z() );
      const auto d        = start.x() - ( n * start.z() );

      // Compute z position at which P.O.C.A. occurs, clamped to valid z range
      // computed by finding z which makes d(r^2)/dz = 0
      // where r is the track radial distance from the z axis, i.e. r^2 = x^2 + y^2
      const auto closestZ = std::clamp( FP( -1.0 ) * ( ( d * n ) + ( m * c ) ) / ( ( n * n ) + ( m * m ) ), //
                                        FP( m_zmin ), FP( m_zmax ) );

      // Check R value at POCA to see if inside beam pipe
      const auto pocaPtn  = propagateToZ( start, dir, closestZ );
      const auto pocaR2   = ( pocaPtn.x() * pocaPtn.x() ) + ( pocaPtn.y() * pocaPtn.y() );
      const auto curPipeR = getRatZ( closestZ );
      return pocaR2 <= ( curPipeR * curPipeR );
    }

  private:
    /// messaging
    template <typename STREAM>
    STREAM& fillStream( STREAM& s ) const {
      return s << "{ BeamPipe |"
               << " StartZ=" << m_zmin << " EndZ=" << m_zmax << " StartR=" << m_rmin << " EndR=" << m_rmax << " }";
    }

  public:
    // messaging

    /// Overload MsgStream operator
    friend inline auto& operator<<( MsgStream& s, const BeamPipe& bp ) { return bp.fillStream( s ); }

    /// Overload ostream operator
    friend inline auto& operator<<( std::ostream& s, const BeamPipe& bp ) { return bp.fillStream( s ); }

  private:
    // data

    /// z position of the start of the cone
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_startZ{};
    /// z position of the end of the cone
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_endZ{};
    /// Cone radius^2 at start z
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_startR2{};
    /// Cone radius^2 at end z
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_endR2{};
    // beam pipe cone parameterisation | R = a.z + b
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_a{};
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_b{};

    // cache of scalar values
    double m_zmin{};
    double m_zmax{};
    double m_r2min{};
    double m_r2max{};
    double m_rmin{};
    double m_rmax{};
  };

} // namespace Rich::Detector
