/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/System.h"

// STL
#include <mutex>
#include <optional>
#include <set>
#include <string>
#include <type_traits>

namespace Rich {

  /// debugging memory management issues
  template <typename TYPE>
  struct AllocateCount {
  private:
    static constexpr auto MyName() noexcept { return TYPE::MyName(); }
    auto&                 msg() const {
      static std::once_flag           run_once;
      static std::optional<MsgStream> msg;
      std::call_once( run_once, [&]() {
        auto msgSvc = Gaudi::svcLocator()->service<IMessageSvc>( "MessageSvc" );
        assert( msgSvc );
        msg.emplace( msgSvc, MyName() );
      } );
      return msg.value();
    }
    auto& info() const { return msg() << MSG::INFO; }
    auto& warning() const { return msg() << MSG::WARNING; }
    auto& debug() const { return msg() << MSG::DEBUG; }
    auto& verbose() const { return msg() << MSG::VERBOSE; }

  public:
    AllocateCount() { track( "Construct     " ); }
    AllocateCount( const AllocateCount& from ) { track( "Copy construct", &from ); }
    AllocateCount( AllocateCount&& from ) { track( "Move construct", &from ); }
    AllocateCount& operator=( const AllocateCount& from ) {
      track( "Copy assign   ", &from );
      return *this;
    }
    AllocateCount& operator=( AllocateCount&& from ) {
      track( "Move assign   ", &from );
      return *this;
    }
    ~AllocateCount() { untrack(); }

  private:
    struct Instances : public std::set<const AllocateCount*> {
      using std::set<const AllocateCount*>::set;
      ~Instances() {
        if ( !this->empty() ) {
          // Do not use MessageService here as happens at very end after application has shutdown.
          std::cerr << "RichAllocateCount     ERROR Still " << this->size() << " allocated " << MyName()
                    << " dervived condition objects !!" << std::endl;
        }
      }
    };

  private:
    auto& lock() {
      static std::mutex instances_lock;
      return instances_lock;
    }
    auto& instances() {
      static Instances instances;
      return instances;
    }
    void check() {
      const std::size_t max_expected_size = 10;
      if ( instances().size() > max_expected_size ) {
        warning() << "Unusually high number of active " << MyName() << " condition slices (" << instances().size()
                  << ">" << max_expected_size << ")" << endmsg;
      }
    }
    void track( const std::string_view op, const AllocateCount* from = nullptr ) {
      std::scoped_lock{lock()};
      instances().insert( this );
      verbose() << op << " " << this;
      if ( from ) { verbose() << " (from " << from << ")"; }
      verbose() << " : Currently " << instances().size() << " active objects" << endmsg;
      check();
    }
    void untrack() {
      std::scoped_lock{lock()};
      instances().erase( this );
      verbose() << "Destructed " << this << " : Currently " << instances().size() << " active objects" << endmsg;
      check();
    }
  };

} // namespace Rich
