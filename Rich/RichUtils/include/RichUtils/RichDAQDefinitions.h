/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//---------------------------------------------------------------------------------
/** @file RichDAQDefinitions.h
 *
 *  Header file for RICH DAQ general definitions
 *
 *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   2003-11-06
 */
//---------------------------------------------------------------------------------

#pragma once

// GaudiKernel
#include "GaudiKernel/HashMap.h"
#include "GaudiKernel/Kernel.h"

// Kernel
#include "Kernel/RichSmartID.h"
#include "Kernel/RichSmartIDHashFuncs.h"

// STL
#include <algorithm>
#include <cassert>
#include <cstdint>
#include <map>
#include <sstream>
#include <type_traits>
#include <vector>

namespace Rich::DAQ {

  //---------------------------------------------------------------------------------

  /// DAQ 64 bit word type
  using LLongType = std::uint64_t;

  /// DAQ long type definition
  using LongType = std::uint32_t;

  /// DAQ short type definition
  using ShortType = std::uint32_t;

  /// DAQ index type definition
  using IndexType = std::uint32_t;

  //---------------------------------------------------------------------------------

  //---------------------------------------------------------------------------------
  /** @class NumericType RichUtils/RichDAQDefinitions.h
   *
   *  Simple class with numeric characteristics but some additional type safety
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   11/11/2005
   */
  //---------------------------------------------------------------------------------
  template <class TYPE>
  class NumericType {
  public:
    /// The underlying type
    using Type = TYPE;

  public:
    /// Default Constructor
    NumericType() = default;
    /// Constructor
    explicit constexpr NumericType( const TYPE id ) noexcept : m_id( id ) {}
    /// Retrieve the full value
    inline constexpr auto data() const noexcept { return m_id; }
    /// Operator ==
    inline constexpr bool operator==( const NumericType<TYPE>& id ) const noexcept { return id.data() == this->data(); }
    /// Operator !=
    inline constexpr bool operator!=( const NumericType<TYPE>& id ) const noexcept { return id.data() != this->data(); }
    /// Operator <
    inline constexpr bool operator<( const NumericType<TYPE>& id ) const noexcept { return this->data() < id.data(); }
    /// Operator >
    inline constexpr bool operator>( const NumericType<TYPE>& id ) const noexcept { return this->data() > id.data(); }

  public:
    /// Overload output to ostream
    friend inline std::ostream& operator<<( std::ostream& os, const NumericType<TYPE>& id ) {
      // 8-bit types are char underneath so cast to int when printing
      if constexpr ( std::is_same_v<TYPE, std::int8_t> ) {
        return os << static_cast<std::int16_t>( id.data() );
      } else if constexpr ( std::is_same_v<TYPE, std::uint8_t> ) {
        return os << static_cast<std::uint16_t>( id.data() );
      } else {
        return os << id.data();
      }
    }
    /// Operator std::string
    inline operator std::string() const {
      std::ostringstream s;
      s << *this;
      return s.str();
    }

  public:
    /// Print the word in Hex
    inline std::ostream& hexDump( std::ostream& os ) const {
      std::ostringstream hexW;
      hexW << std::hex << data();
      std::string tmpW = hexW.str();
      if ( tmpW.size() < 8 ) { tmpW = std::string( 8 - tmpW.size(), '0' ) + tmpW; }
      return os << tmpW;
    }
    /// Bits dump
    inline std::ostream& bitsDump( std::ostream&      os,                          //
                                   const unsigned int nBits  = 8 * sizeof( TYPE ), //
                                   const std::string  spacer = " " ) const {
      for ( int iCol = nBits - 1; iCol >= 0; --iCol ) { os << spacer << isBitOn( iCol ); }
      return os;
    }
    /// Raw dump of the word
    inline std::ostream& rawDump( std::ostream&      os,                          //
                                  const unsigned int nBits  = 8 * sizeof( TYPE ), //
                                  const std::string  spacer = "" ) const {
      hexDump( os );
      os << ":";
      return bitsDump( os, nBits, spacer );
    }

  protected:
    /// Update the internal data
    inline constexpr void setData( const TYPE id ) noexcept { m_id = id; }
    /// test if a given bit is  'on'
    inline constexpr bool isBitOn( const Rich::DAQ::IndexType pos ) const noexcept {
      return ( 0 != ( m_id & ( 1 << pos ) ) );
    }

  private:
    /// Define the invalid value
    inline static constexpr TYPE INVALID() {
      if constexpr ( std::is_signed_v<TYPE> ) {
        return -1;
      } else {
        return 0;
      }
    }

  public:
    /// Check if value is valid
    inline constexpr bool isValid() const noexcept {
      if constexpr ( std::is_signed_v<TYPE> ) {
        return m_id != INVALID();
      } else {
        // unsigned types always valid
        return true;
      }
    }
    /// Set as invalid
    inline constexpr void setInvalid() noexcept { setData( INVALID() ); }

  private:
    TYPE m_id{INVALID()}; ///< The data value
  };

  //---------------------------------------------------------------------------------

  /** @class EventID RichUtils/RichDAQDefinitions.h
   *
   *  The Event ID.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   24/01/2007
   */
  class EventID final : public NumericType<LLongType> {
  public:
    /// Default constructor
    EventID() = default;
    /// Copy Constructor
    EventID( const EventID& ) = default;
    /// Constructor from value and number of bits
    template <class NUMTYPE>
    EventID( const NUMTYPE id, const ShortType aBits ) noexcept
        : NumericType<Type>( static_cast<Type>( id ) ), m_nActiveBits( aBits ) {}
    /// Constructor from value
    template <class NUMTYPE>
    explicit EventID( const NUMTYPE id ) noexcept : EventID( id, 8 * sizeof( NUMTYPE ) ) {}
    /// Return the number of active bits
    inline auto activeBits() const noexcept { return m_nActiveBits; }
    /// Set the number of active bits
    inline void setActiveBits( const ShortType bits ) noexcept { m_nActiveBits = bits; }

  public:
    /// Overloaded output to ostream
    friend inline std::ostream& operator<<( std::ostream& evtID_os, const EventID& id ) {
      evtID_os << "[ ID=" << id.data();
      evtID_os << " Hex=";
      id.hexDump( evtID_os );
      evtID_os << " Bits(" << id.activeBits() << ")=";
      id.bitsDump( evtID_os, id.activeBits(), "" );
      return evtID_os << " ]";
    }

  public:
    /// Operator == that takes into account the correct number of bits
    inline bool operator==( const EventID& id ) const noexcept {
      // Compute which how many bits the words should in common, so we only compare these
      const auto lowBits = std::min( this->activeBits(), id.activeBits() );
      const auto mask    = ( ( 1 << lowBits ) - 1 );
      // compare the bits and return
      return ( ( this->data() & mask ) == ( id.data() & mask ) );
    }
    /// Operator != that takes into account the correct number of bits
    inline bool operator!=( const EventID& id ) const noexcept { return !this->operator==( id ); }

  private:
    /// Number of sensitive bits in this EventID
    ShortType m_nActiveBits{8 * sizeof( Type )};
  };

  /** @class BXID RichUtils/RichDAQDefinitions.h
   *
   *  The BX ID.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   24/01/2007
   */
  class BXID final : public NumericType<LongType> {
  public:
    /// Default constructor
    BXID() = default;
    /// Copy Constructor
    BXID( const BXID& ) = default;
    /// Constructor from value and number of bits
    template <class NUMTYPE>
    BXID( const NUMTYPE id, const ShortType aBits )
    noexcept : NumericType<Type>( static_cast<Type>( id ) ), m_nActiveBits( aBits ) {}
    /// Constructor from value
    template <class NUMTYPE>
    explicit BXID( const NUMTYPE id ) noexcept : BXID( id, 8 * sizeof( NUMTYPE ) ) {}
    /// Return the number of active bits
    inline auto activeBits() const noexcept { return m_nActiveBits; }
    /// Set the number of active bits
    inline void setActiveBits( const ShortType bits ) noexcept { m_nActiveBits = bits; }

  public:
    /// Overloaded output to ostream
    friend inline std::ostream& operator<<( std::ostream& os, const BXID& id ) {
      os << "[ ID=" << id.data();
      os << " Hex=";
      id.hexDump( os );
      os << " Bits(" << id.activeBits() << ")=";
      id.bitsDump( os, id.activeBits(), "" );
      return os << " ]";
    }

  public:
    /// Operator == that takes into account the correct number of bits
    inline bool operator==( const BXID& id ) const noexcept {
      // Compute which how many bits the words should in common, so we only compare these
      const auto lowBits = std::min( this->activeBits(), id.activeBits() );
      const auto mask    = ( ( 1 << lowBits ) - 1 );
      // compare the bits and return
      return ( ( this->data() & mask ) == ( id.data() & mask ) );
    }
    /// Operator != that takes into account the correct number of bits
    inline bool operator!=( const BXID& id ) const noexcept { return !this->operator==( id ); }

  private:
    /// Number of sensitive bits in this BXID
    ShortType m_nActiveBits{8 * sizeof( Type )};
  };

  /** @class PDHardwareID RichUtils/RichDAQDefinitions.h
   *
   *  The (numeric) PD hardware ID. Unique to each PD and can be
   *  used to locate its physical properties, such as Q.E. curves.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   11/11/2005
   */
  class PDHardwareID final : public NumericType<LongType> {
  public:
    /// Use base constructors
    using NumericType<Type>::NumericType;
  };

  /** @class PDCopyNumber RichUtils/RichDAQDefinitions.h
   *
   *  The Geant4 copy number equivalent. A different way to locate PDs.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   24/07/2008
   */
  class PDCopyNumber final : public NumericType<ShortType> {
  public:
    /// Use base constructors
    using NumericType<Type>::NumericType;
  };

  /** @class PDPanelIndex RichUtils/RichDAQDefinitions.h
   *
   *  PD Index within a panel. Does not neccessary run concurrently.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   24/07/2008
   */
  class PDPanelIndex final : public NumericType<ShortType> {
  public:
    /// Use base constructors
    using NumericType<Type>::NumericType;
  };

  /** @class PDModuleNumber RichUtils/RichDAQDefinitions.h
   *
   *  PD Module Number
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   27/06/2020
   */
  class PDModuleNumber final : public NumericType<std::int32_t> {
  public:
    /// Use base constructors
    using NumericType<Type>::NumericType;
  };

  /** @class SourceID RichUtils/RichDAQDefinitions.h
   *
   *  Tel40 Source ID
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   27/06/2020
   */
  class SourceID final : public NumericType<std::int16_t> {

  public:
    /// Type for data fields
    using FieldType = std::uint16_t;

  private:
    /// The partition values for RICH1 and RICH2
    enum Partition { RICH1 = 4, RICH2 = 9 };

  private:
    using I                                 = FieldType;
    static constexpr const I BitsPayload    = 10; ///< The Tel40 specific number
    static constexpr const I BitsSide       = 1;  ///< The SIDE bit
    static constexpr const I BitsPartition  = 5;  ///< The Partition number
    static constexpr const I ShiftPayload   = 0;
    static constexpr const I ShiftSide      = ShiftPayload + BitsPayload;
    static constexpr const I ShiftPartition = ShiftSide + BitsSide;
    static constexpr const I MaskPayload    = ( I )( ( I( 1 ) << BitsPayload ) - I( 1 ) ) << ShiftPayload;
    static constexpr const I MaskSide       = ( I )( ( I( 1 ) << BitsSide ) - I( 1 ) ) << ShiftSide;
    static constexpr const I MaskPartition  = ( I )( ( I( 1 ) << BitsPartition ) - I( 1 ) ) << ShiftPartition;

  private:
    /// Set the given data into the given field
    constexpr void set( const FieldType value, const FieldType shift, const FieldType mask ) noexcept {
      setData( ( ( value << shift ) & mask ) | ( data() & ~mask ) );
    }

  public:
    /// Get the partition
    inline constexpr auto partition() const noexcept {
      return FieldType( ( data() & MaskPartition ) >> ShiftPartition );
    }

    /// Get the RICH type
    inline constexpr auto rich() const noexcept {
      const auto part = partition();
      return ( part == Partition::RICH1 ? Rich::Rich1 :     //
                   part == Partition::RICH2 ? Rich::Rich2 : //
                       Rich::InvalidDetector );
    }

    /// Get the detector Side
    inline constexpr auto side() const noexcept { return Rich::Side( ( data() & MaskSide ) >> ShiftSide ); }

    /// Get the SourceID payload
    inline constexpr auto payload() const noexcept { return FieldType( ( data() & MaskPayload ) >> ShiftPayload ); }

    /// Check ID is valid
    inline constexpr bool isValid() const noexcept {
      return ( this->NumericType<Type>::isValid() && rich() != Rich::InvalidDetector );
    }

  public:
    /// Use base constructors
    using NumericType<Type>::NumericType;

    /// Constructor from individual data fields
    constexpr SourceID( const Rich::DetectorType r, const Rich::Side s, const FieldType p ) noexcept {
      set( ( Rich::Rich1 == r ? RICH1 : RICH2 ), ShiftPartition, MaskPartition );
      set( s, ShiftSide, MaskSide );
      set( p, ShiftPayload, MaskPayload );
      assert( r == rich() );
      assert( s == side() );
      assert( p == payload() );
    }

  public:
    /// Overload output to ostream
    friend inline std::ostream& operator<<( std::ostream& os, const SourceID& id ) {
      os << "[ " << id.data();
#ifndef NDEBUG
      os << " | ";
      id.rawDump( os );
#endif
      os << " | Partition=" << id.partition();
      if ( id.isValid() ) {
        os << " " << id.rich() << " " << Rich::text( id.rich(), id.side() );
      } else {
        os << " INVALID";
      }
      os << " | ID=" << id.payload() << " ]";
      return os;
    }
  };

  /** @class Tel40Connector RichUtils/RichDAQDefinitions.h
   *
   *  Tel40 Connector
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   27/06/2020
   */
  class Tel40Connector final : public NumericType<std::int8_t> {
  public:
    /// Use base constructors
    using NumericType<Type>::NumericType;
  };

  /** @class PDMDBID RichUtils/RichDAQDefinitions.h
   *
   *  PDMDB ID
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   27/06/2020
   */
  class PDMDBID final : public NumericType<std::int8_t> {
  public:
    /// Use base constructors
    using NumericType<Type>::NumericType;
  };

  /** @class PDMDBFrame RichUtils/RichDAQDefinitions.h
   *
   *  PDMDB Frame Number
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   27/06/2020
   */
  class PDMDBFrame final : public NumericType<std::int8_t> {
  public:
    /// Use base constructors
    using NumericType<Type>::NumericType;
  };

  /** @class FrameBitIndex RichUtils/RichDAQDefinitions.h
   *
   *  Bit index (0-86) in a data frame
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   27/06/2020
   */
  class FrameBitIndex final : public NumericType<std::int8_t> {
  public:
    /// Use base constructors
    using NumericType<Type>::NumericType;
  };

  /** @class ElementaryCell RichUtils/RichDAQDefinitions.h
   *
   *  EC number (0-3)
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   27/06/2020
   */
  class ElementaryCell final : public NumericType<std::int8_t> {
  public:
    /// Use base constructors
    using NumericType<Type>::NumericType;
  };

  /** @class PMTInEC RichUtils/RichDAQDefinitions.h
   *
   *  PMT numnber in a EC
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   27/06/2020
   */
  class PMTInEC final : public NumericType<std::int8_t> {
  public:
    /// Use base constructors
    using NumericType<Type>::NumericType;
  };

  /** @class AnodeIndex RichUtils/RichDAQDefinitions.h
   *
   *  PMT anode index (0-63)
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   27/06/2020
   */
  class AnodeIndex final : public NumericType<std::int8_t> {
  public:
    /// Use base constructors
    using NumericType<Type>::NumericType;
  };

  //--------------------------------------------------------------------------------------

  /// Vector of PD Hardware IDs
  using PDHardwareIDs = std::vector<PDHardwareID>;

  //--------------------------------------------------------------------------------------

  /** @enum BankVersion
   *
   *  Enumeration for the RICH DAQ Level1 bank versions
   *
   *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
   *  @date   20/12/2004
   */
  enum BankVersion {
    /// Undefined
    UndefinedBankVersion = -1,
    /// Simple version. Just streams SmartIDs
    StreamSmartIDs = 3,
    /// 'Production' PMT version (to be checked if this is the actual value)
    MaPMT1 = 10
  };

  //---------------------------------------------------------------------------------

} // namespace Rich::DAQ

//---------------------------------------------------------------------------------

// Hash functions
// Needed in order to allow these classes to be used as keys in Hash maps
// CRJ : Are these really needed ?

#ifdef __GNUC__
// namespace __gnu_cxx
namespace std {

  /// PDHardwareID hash function
  template <>
  struct hash<Rich::DAQ::PDHardwareID> {
    inline size_t operator()( Rich::DAQ::PDHardwareID id ) const { return (size_t)id.data(); }
  };
  /// PDHardwareID hash function
  template <>
  struct hash<Rich::DAQ::PDHardwareID&> {
    inline size_t operator()( Rich::DAQ::PDHardwareID id ) const { return (size_t)id.data(); }
  };
  /// PDHardwareID hash function
  template <>
  struct hash<const Rich::DAQ::PDHardwareID> {
    inline size_t operator()( const Rich::DAQ::PDHardwareID id ) const { return (size_t)id.data(); }
  };
  /// PDHardwareID hash function
  template <>
  struct hash<const Rich::DAQ::PDHardwareID&> {
    inline size_t operator()( const Rich::DAQ::PDHardwareID id ) const { return (size_t)id.data(); }
  };

  /// PDCopyNumber hash function
  template <>
  struct hash<Rich::DAQ::PDCopyNumber> {
    inline size_t operator()( Rich::DAQ::PDCopyNumber id ) const { return (size_t)id.data(); }
  };
  /// PDCopyNumber hash function
  template <>
  struct hash<Rich::DAQ::PDCopyNumber&> {
    inline size_t operator()( Rich::DAQ::PDCopyNumber id ) const { return (size_t)id.data(); }
  };
  /// PDCopyNumber hash function
  template <>
  struct hash<const Rich::DAQ::PDCopyNumber> {
    inline size_t operator()( const Rich::DAQ::PDCopyNumber id ) const { return (size_t)id.data(); }
  };
  /// PDCopyNumber hash function
  template <>
  struct hash<const Rich::DAQ::PDCopyNumber&> {
    inline size_t operator()( const Rich::DAQ::PDCopyNumber id ) const { return (size_t)id.data(); }
  };

} // namespace std

namespace GaudiUtils {

  /// PDHardwareID Hash function
  template <>
  struct Hash<Rich::DAQ::PDHardwareID> {
    inline size_t operator()( Rich::DAQ::PDHardwareID id ) const noexcept { return (size_t)id.data(); }
  };
  /// PDHardwareID Hash function
  template <>
  struct Hash<Rich::DAQ::PDHardwareID&> {
    inline size_t operator()( Rich::DAQ::PDHardwareID id ) const noexcept { return (size_t)id.data(); }
  };
  /// PDHardwareID Hash function
  template <>
  struct Hash<const Rich::DAQ::PDHardwareID> {
    inline size_t operator()( const Rich::DAQ::PDHardwareID id ) const noexcept { return (size_t)id.data(); }
  };
  /// PDHardwareID Hash function
  template <>
  struct Hash<const Rich::DAQ::PDHardwareID&> {
    inline size_t operator()( const Rich::DAQ::PDHardwareID id ) const noexcept { return (size_t)id.data(); }
  };

  /// PDCopyNumber Hash function
  template <>
  struct Hash<Rich::DAQ::PDCopyNumber> {
    inline size_t operator()( Rich::DAQ::PDCopyNumber id ) const noexcept { return (size_t)id.data(); }
  };
  /// PDCopyNumber Hash function
  template <>
  struct Hash<Rich::DAQ::PDCopyNumber&> {
    inline size_t operator()( Rich::DAQ::PDCopyNumber id ) const noexcept { return (size_t)id.data(); }
  };
  /// PDCopyNumber Hash function
  template <>
  struct Hash<const Rich::DAQ::PDCopyNumber> {
    inline size_t operator()( const Rich::DAQ::PDCopyNumber id ) const noexcept { return (size_t)id.data(); }
  };
  /// PDCopyNumber Hash function
  template <>
  struct Hash<const Rich::DAQ::PDCopyNumber&> {
    inline size_t operator()( const Rich::DAQ::PDCopyNumber id ) const noexcept { return (size_t)id.data(); }
  };

} // namespace GaudiUtils
#endif // GNU
