/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichCommonBase.h
 *
 *  Header file for RICH base class : Rich::CommonBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2005-08-27
 */
//-----------------------------------------------------------------------------

#pragma once

// Gaudi
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SerializeSTL.h"

// Forward declarations
class DeRich;

// Some defines for debug/verbose messages...
#ifndef _ri_debug
#  define _ri_debug                                                                                                    \
    if ( msgLevel( MSG::DEBUG ) ) debug()
#endif
#ifndef _ri_verbo
#  define _ri_verbo                                                                                                    \
    if ( msgLevel( MSG::VERBOSE ) ) verbose()
#endif

namespace Rich {
  using GaudiUtils::operator<<;

  //-----------------------------------------------------------------------------
  /** @class CommonBase RichCommonBase.h RichKernel/RichCommonBase.h
   *
   *  Base class providing common functionality for all RICH tools and algorithms
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2005-08-27
   */
  //-----------------------------------------------------------------------------

  template <class PBASE>
  class CommonBase : public PBASE {

  public:
    /// Standard algorithm-like constructor
    CommonBase( const std::string& name, ISvcLocator* pSvcLocator );

    /// Standard tool-like constructor
    CommonBase( const std::string& type, const std::string& name, const IInterface* parent );

    /// Standard Converter-like Constructor
    CommonBase( long storage_type, const CLID& class_type, ISvcLocator* svc = nullptr );

  public:
    /** Returns the full location of the given object in the Data Store
     *
     *  @param pObj Data object
     *
     *  @return Location of given data object
     */
    inline std::string objectLocation( const DataObject* pObj ) const {
      return ( !pObj ? "Null DataObject !" : ( pObj->registry() ? pObj->registry()->identifier() : "UnRegistered" ) );
    }

  protected: // methods
    /// Pointer to Job Options Service
    inline Gaudi::Interfaces::IOptionsSvc& optsSvc() const noexcept { return this->serviceLocator()->getOptsSvc(); }

    /** Generate a context specific TES location for the given Default location
     *  @param loc The Default TES location
     *  @return The context specific TES location for the input location
     */
    std::string contextSpecificTES( const std::string& loc ) const;

    /** Propagate a list oj job options from one object to another
     *  @param from_name The name of the object to get the options from
     *  @param to_name   The name of the oject to copy the options to
     *  @param options   List of options to copy. If empty, all options are copied.
     *  @param overwrite If true, options will be over-written in the target object
     *                   if they are already set
     *  @return StatusCode indicating if the options where correctly copied
     */
    StatusCode propagateJobOptions( const std::string&              from_name, //
                                    const std::string&              to_name,   //
                                    const std::vector<std::string>& options   = std::vector<std::string>(),
                                    const bool                      overwrite = false ) const;
  };
} // namespace Rich
