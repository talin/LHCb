/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichCommonBase.h
 *
 *  Header file for RICH base class : Rich::CommonBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2005-08-27
 */
//-----------------------------------------------------------------------------

#pragma once

// Gaudi
#include "Gaudi/Accumulators.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SerializeSTL.h"

// DetDesc
#include "DetDesc/GenericConditionAccessorHolder.h"

// Utils
#include "RichFutureUtils/RichMessaging.h"
#include "RichUtils/RichSIMDTypes.h"

// STL
#include <utility>

namespace Rich {
  using GaudiUtils::operator<<;

  namespace Future {

    //-----------------------------------------------------------------------------
    /** @class CommonBase RichCommonBase.h RichFutureKernel/RichCommonBase.h
     *
     *  Base class providing common functionality for all RICH tools and algorithms
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   2005-08-27
     */
    //-----------------------------------------------------------------------------

    template <class PBASE>
    class CommonBase : public PBASE, public LHCb::SIMD::AlignedBase<LHCb::SIMD::VectorAlignment> {

    public:
      // Inherit base class constructors
      using PBASE::PBASE;

    public:
      /** Initialization of the algorithm after creation
       *
       * @return The status of the initialization
       * @retval StatusCode::SUCCESS Initialization was successful
       * @retval StatusCode::FAILURE Initialization failed
       */
      virtual StatusCode initialize() override;

      /** Finalization of the algorithm before deletion
       *
       * @return The status of the finalization
       * @retval StatusCode::SUCCESS Finalization was successful
       * @retval StatusCode::FAILURE Finalization failed
       */
      virtual StatusCode finalize() override;

    public:
      /** Returns the full location of the given object in the Data Store
       *
       *  @param pObj Data object
       *
       *  @return Location of given data object
       */
      inline std::string objectLocation( const DataObject* pObj ) const {
        return ( !pObj ? "Null DataObject !" : ( pObj->registry() ? pObj->registry()->identifier() : "UnRegistered" ) );
      }

      /** @brief Forced release of a particular tool
       *
       *  Tools are automatically released during finalisation, so this method
       *  only need be used to release a tool early, before finalisation.
       *
       *  @param pTool  Pointer to the tool to be released
       */
      template <typename TOOL>
      inline void releaseTool( TOOL*& pTool ) const {
        if ( pTool ) {
          if ( this->msgLevel( MSG::DEBUG ) ) {
            this->debug() << " Forced release for tool '" << pTool->name() << "'" << endmsg;
          }
          this->release( pTool );
          pTool = nullptr;
        } else {
          this->warning() << "Attempt to release a NULL Tool pointer" << endmsg;
        }
      }

      /// Load an object from the given store
      template <typename TOOL, typename SERVICE>
      decltype( auto ) acquire( SERVICE svc, std::string loc ) {
        DataObject* tmp = nullptr;
        auto        sc  = svc->retrieveObject( std::move( loc ), tmp );
        return ( sc ? dynamic_cast<TOOL*>( tmp ) : nullptr );
      }

    protected:
      /// Define the messenger entity
      inline auto messenger() const noexcept { return this; }

    protected:
      // definitions
      template <MSG::Level LEVEL>
      using MsgCounter     = Gaudi::Accumulators::MsgCounter<LEVEL>;
      using VerboseCounter = MsgCounter<MSG::VERBOSE>;
      using DebugCounter   = MsgCounter<MSG::DEBUG>;
      using WarningCounter = MsgCounter<MSG::WARNING>;
      using ErrorCounter   = MsgCounter<MSG::ERROR>;
    };

  } // namespace Future
} // namespace Rich
