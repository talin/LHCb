###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rich/RichFutureDAQ
------------------
#]=======================================================================]

gaudi_add_library(RichFutureDAQLib
    SOURCES
        src/lib/EncodeTel40Data.cpp
        src/lib/RichPDMDBDecodeMapping.cpp
        src/lib/RichPDMDBEncodeMapping.cpp
        src/lib/RichTel40CableMapping.cpp
    LINK
        PUBLIC
            Boost::headers
            Gaudi::GaudiKernel
            LHCb::DAQEventLib
            LHCb::DetDescLib
            LHCb::LHCbKernel
            LHCb::RichDetectorsLib
            LHCb::RichDetLib
            LHCb::RichFutureUtils
            LHCb::RichUtils
)

gaudi_add_module(RichFutureDAQ
    SOURCES
        src/component/RichRawBankDecoder.cpp
    LINK
        Boost::headers
        Gaudi::GaudiKernel
        LHCb::DAQEventLib
        LHCb::DetDescLib
        LHCb::LHCbAlgsLib
        LHCb::LHCbKernel
        LHCb::RichDetLib
        LHCb::RichFutureDAQLib
        LHCb::RichFutureKernel
        LHCb::RichFutureUtils
        LHCb::RichUtils
)
