#!/usr/bin/env python3

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
import io
import pandas
import requests
from parse import parse


def xmlStart(filename, mode='w', output_dir="output"):
    os.makedirs(output_dir, exist_ok=True)
    f = open(output_dir + "/" + filename, mode)
    f.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
    f.write("<!DOCTYPE DDDB SYSTEM \"git:/DTD/structure.dtd\">\n\n")
    f.write(
        "<!-- WARNING - This file is auto-generated. DO NOT update by hand. -->\n\n"
    )
    f.write("<DDDB>\n\n")
    return f


def xmlEnd(f):
    f.write("</condition>\n")
    f.write("</DDDB>\n")
    f.close()


def writeParamVector(f,
                     data,
                     name,
                     desc,
                     type='int',
                     pad_size=6,
                     max_per_line=12):
    f.write("<paramVector name=\"" + name + "\" type=\"" + type +
            "\" comment=\"" + desc + "\">")
    iLine = 0
    for i in data:
        if iLine == 0:
            f.write("\n")
        f.write(str(i).ljust(pad_size, ' ') + ' ')
        iLine += 1
        if iLine == max_per_line:
            iLine = 0
    f.write("\n</paramVector>\n\n")


def writeParam(f, data, name, desc, type='int'):
    f.write("<param name=\"" + name + "\" type=\"" + type + "\" comment=\"" +
            desc + "\">\n")
    f.write(str(data))
    f.write("\n</param>\n\n")


def listprint(title, data):
    print(title, '[', end='')
    for d in data:
        print(' {:2}'.format(d), end='')
    print(' ]')


# Creates the readout mappings for PDMs
def pdm_readout_mapping(pmt_type):

    # anodes per PMT. same for R and H
    nAnodes = 64
    # bits in a link data frame
    nFrameBits = 86
    # links per PDMDB
    nFramesPerDB = 6

    if pmt_type == 'R':
        # PMTs
        pmts = ['A', 'B', 'C', 'D']
    elif pmt_type == 'H':
        # PMTs
        pmts = ['H']

    # Convert from Steve's [A,B,C,D] to [1,2,3,4]
    pmtStoI = {'U': -1, 'A': 0, 'B': 1, 'C': 3, 'D': 2, 'H': 0}
    #pmtStoI = { 'U':-1, 'A':'A', 'B':'B', 'C':'C', 'D':'D' }

    # Read the input data from Steve's Excel file
    mapping_file = 'excel/PDMDB-EC-pins.xlsx'
    print("\nReading data from", mapping_file)
    expmt = {}
    for pmt in pmts:
        print("\n -> Read sheet for PMT", pmt)
        if pmt == 'H':
            expmt[pmt] = pandas.read_excel(mapping_file, sheet_name='EC-H')
        else:
            num = str(pmtStoI[pmt])
            expmt[pmt] = pandas.read_excel(
                mapping_file, sheet_name='PMT' + num + "(" + pmt + ")")
        print(expmt[pmt])

    # create empty data blocks for bits in each frame
    # data for each bit is (EC,PMT,Anode)
    frame_data = {}

    # Check frame data has default entry for given frame
    def init_frame(frame):
        # Make list with nFrameBit copies of default (unused) data
        if not frame in frame_data.keys():
            frame_data[frame] = [(-1, 'U', -1)] * nFrameBits

    # Checks data is valid
    def check(frame, bit, bit_data):
        if bit >= nFrameBits or bit < 0:
            print("WARNING : bit value out of range", bit)
            return False
        if frame_data[frame][bit][1] != 'U':
            print("WARNING : frame", frame, "bit", bit, "already set to",
                  frame_data[frame][bit], "new setting", bit_data)
            return False
        return True

    # Loop over pmts
    allok = True
    for pmt in pmts:

        # A,D PMTs are in links 0-5, C,B in 6-11...
        frame_offset = 0
        if pmt == 'C' or pmt == 'B': frame_offset = 6

        # Loop over the 64 anodes in each PMT
        for tabentry in range(nAnodes):

            # Read anode number from table
            anode = expmt[pmt]['Anode'][tabentry]
            # Steve's sheet numbers from 1. Convert to index (number from 0)
            anode = anode - 1

            # Loop over elementry cells
            for ec in range(4):

                # Read frame number from excel, add offset for second PDMDB
                frame = expmt[pmt]['Frame.EC' +
                                   str(ec)][tabentry] + frame_offset
                init_frame(frame)

                # bit in data stream
                bit = expmt[pmt]['Bit.EC' + str(ec)][tabentry]

                # PDMDB1 has EC number flipped [0,1,2,3] -> [3,2,1,0]
                flipped_ec = (3 - ec if frame_offset > 0 else ec)
                #flipped_ec = ec

                # check not already assigned
                bit_data = (flipped_ec, pmt, anode)
                ok = check(frame, bit, bit_data)
                allok = allok and ok

                # If all OK update data for this frame and bit.
                if ok: frame_data[frame][bit] = bit_data

    if not allok:
        print("Problem decoding Excel file")
        exit(0)

    # Extracted lists of data, for ECs, PMTs and Anodes for each frame
    ECs = {}
    PMTs = {}
    Anodes = {}
    PDMDBs = []

    # Loop over frames(links) and print mapping for each frame data bit
    # in human readable format
    for frame in sorted(frame_data.keys()):

        # make entries for this frame
        ECs[frame] = []
        PMTs[frame] = []
        Anodes[frame] = []

        # Loop over bits in each frame
        fdata = frame_data[frame]
        for bit in range(nFrameBits):

            # Set various data
            ec = fdata[bit][0]
            pmt = pmtStoI[fdata[bit][1]]
            anode = fdata[bit][2]

            # Fill list for this link
            ECs[frame] += [ec]
            PMTs[frame] += [pmt]
            Anodes[frame] += [anode]

            pdmdb = int(frame / nFramesPerDB)
            if not pdmdb in PDMDBs: PDMDBs += [pdmdb]

    # padding size
    pad_size = 4

    # Create XML conditions
    f_decode = xmlStart(
        filename="PMT_" + pmt_type + "_PDMDB_DecodeMap.xml",
        output_dir="output/pdms")

    f_decode.write(
        "<!-- EC    numbered from 0 (0-3).  -1 indicates unused bit. -->\n")
    f_decode.write(
        "<!-- PMT   numbered from 0 (0-3).  -1 indicates unused bit. -->\n")
    f_decode.write(
        "<!-- Anode numbered from 0 (0-63). -1 indicates unused bit. -->\n\n")

    f_decode.write("<condition classID=\"5\" name=\"PDMDB_" + pmt_type +
                   "_DecodePixelMap\">\n\n")

    writeParamVector(f_decode, PDMDBs, 'ActivePDMDBs', 'List of active PDMDBs')

    framesPerPDMDB = {}
    for pdmdb in PDMDBs:
        framesPerPDMDB[pdmdb] = []
    for frame in sorted(frame_data.keys()):
        framesPerPDMDB[int(
            frame / nFramesPerDB)] += [int(frame % nFramesPerDB)]
    for pdmdb, frames in framesPerPDMDB.items():
        title = "PDMDB%1i%s_ActiveFrames" % (pdmdb, pmt_type)
        writeParamVector(
            f_decode, frames, title,
            'List of active PDMDB data frames (links) for ' + title)

    # Print bits -> detector info (decoding)
    print("Data for %s type modules\n" % pmt_type)
    for frame in sorted(frame_data.keys()):
        pdmdb = int(frame / nFramesPerDB)
        localf = int(frame % nFramesPerDB)
        title = "PDMDB%1i%s_Frame%1i" % (pdmdb, pmt_type, localf)
        print(title)
        print("")
        listprint("         ", range(0, len(ECs[frame])))
        listprint("   ECs   ", ECs[frame])
        listprint("   PMTs  ", PMTs[frame])
        listprint("   Anodes", Anodes[frame])
        print("")
        writeParamVector(
            f_decode, ECs[frame], title + '_ECs',
            'Data frame bit to elementary cell mapping for ' + title)
        writeParamVector(f_decode, PMTs[frame], title + '_PMTs',
                         'Data frame bit to PMT mapping for ' + title)
        writeParamVector(f_decode, Anodes[frame], title + '_Anodes',
                         'Data frame bit to anode mapping for ' + title)

    # Close the output file
    xmlEnd(f_decode)

    # Print to file
    f_encode = xmlStart(
        filename="PMT_" + pmt_type + "_PDMDB_EncodeMap.xml",
        output_dir="output/pdms")

    f_encode.write("<!-- PDMDB numbered from 0 (0-1). -->\n")
    f_encode.write("<!-- Frame numbered from 0 (0-5). -->\n")
    f_encode.write("<!-- Bit   numbered from 0 (0-63). -->\n\n")

    f_encode.write("<condition classID=\"5\" name=\"PDMDB_" + pmt_type +
                   "_EncodePixelMap\">\n\n")

    # Create reverse mappings for encoding
    encode_data = {}
    for frame in sorted(frame_data.keys()):
        pdmdb = int(frame / nFramesPerDB)
        localf = int(frame % nFramesPerDB)
        # loop over each bit in frame
        assert len(ECs[frame]) == len(PMTs[frame])
        assert len(ECs[frame]) == len(Anodes[frame])
        for bindex in range(len(ECs[frame])):
            ec = ECs[frame][bindex]
            pmt = PMTs[frame][bindex]
            anode = Anodes[frame][bindex]
            if pmt > -1:
                # Create empty entries as required
                #if not pdmdb in encode_data.keys() : encode_data[pdmdb] = {}
                if not ec in encode_data.keys(): encode_data[ec] = {}
                if not pmt in encode_data[ec].keys():
                    encode_data[ec][pmt] = {
                        "PDMDB": [-1] * nAnodes,
                        "Frame": [-1] * nAnodes,
                        "Bit": [-1] * nAnodes
                    }
                # note anodes already corrected to number from 0
                pmt_data = encode_data[ec][pmt]
                pmt_data["PDMDB"][anode] = pdmdb
                pmt_data["Frame"][anode] = localf
                pmt_data["Bit"][anode] = bindex

    # Condition for list of active ECs
    writeParamVector(f_encode, sorted(encode_data.keys()), 'ActiveECs',
                     'List of active Elementary Cells')

    # Print the mapping
    for ec in sorted(encode_data.keys()):
        print("")
        print("EC", ec)
        # Condition for list of active PMTs in given EC
        writeParamVector(f_encode, sorted(encode_data[ec].keys()),
                         'EC' + str(ec) + '_ActivePMTs',
                         'List of active PMTs in EC' + str(ec))

        for pmt in sorted(encode_data[ec].keys()):
            pmt_data = encode_data[ec][pmt]
            title = 'EC' + str(ec) + '_PMT' + str(pmt)
            print("")
            print(" PMT", pmt)
            print("")
            listprint("       ", range(0, len(pmt_data["PDMDB"])))
            listprint("  PDMDB", pmt_data["PDMDB"])
            listprint("  Frame", pmt_data["Frame"])
            listprint("  Bit  ", pmt_data["Bit"])
            # create XML conditions
            writeParamVector(f_encode, pmt_data["PDMDB"], title + '_PDMDB',
                             'PMT anode to PDMDB mapping for ' + title)
            writeParamVector(f_encode, pmt_data["Frame"], title + '_Frame',
                             'PMT anode to Frame mapping for ' + title)
            writeParamVector(f_encode, pmt_data["Bit"], title + '_Bit',
                             'PMT anode to bit mapping for ' + title)

    # Close the output file
    xmlEnd(f_encode)

    # Return the mappings
    return {"ECs": ECs, "PMTs": PMTs, "Anodes": Anodes}


def cable_maps_sqldb():

    # Detector sides
    rich_sides = ["R1U", "R1D", "R2A", "R2C"]

    #rich_sides = [ "R1U" ]

    # Parse the sd_name string
    def sd_name_parse(sd_name):
        # parse the name string to extract the numerical data
        r = parse("R{}{}.COL{}.PDM{}.PDMDB{}.DTM{}.Link{}.{}", sd_name)
        type = 'R'
        # Row numbers in RICH2 redefined to start from 1
        # For current MC samples which still start at zero rescale for now
        # this should not be done for real data or future MC samples.
        row = int(r[2])
        if int(r[0]) == 2: row = row - 1
        return {
            "RICH": int(r[0]),
            "PANEL": r[1],
            "ROW": row,
            "COL": int(r[3]),
            "PDMDB": int(r[4]),
            "DTM": int(r[5]),
            "LINK": int(r[6]),
            "STATUS": r[7]
        }

    # Get the s/w module number from sd_name string
    def get_module_num(info):
        nColPerRow = 6
        # offsets by RICH and panel
        offsets = {(1, 'U'): 0, (1, 'D'): 66, (2, 'A'): 132, (2, 'C'): 204}
        # return module number
        return offsets[(info["RICH"], info["PANEL"])] + (
            nColPerRow * info["ROW"]) + info["COL"]

    # Deduce PMT type
    def get_pmt_type(info):
        pmtT = 'R'
        if info["RICH"] == 2 and info["COL"] in [0, 1, 4, 5]:
            pmtT = 'H'
        return pmtT

    # get PDMDB link number
    def get_pdmdb_link(info):
        return (2 * info["DTM"]) + info["LINK"]

    def normaliseMPO(mpo):
        # In first (unrealistic) implementation MPO was just [1,2] for the two groups
        # of 12 connections per sourceID. In the real DB they are [1,3] for the first
        # SourceID per Tel40, [5,7] for the second SourceID. As we just need to know
        # which of the two groups we have, map back to [1,2]
        if 1 == mpo:
            return 1
        elif 2 == mpo:
            return 2
        elif 3 == mpo:
            return 2
        elif 5 == mpo:
            return 1
        elif 7 == mpo:
            return 2
        return mpo

    # Read from a local instance. See
    # https://gitlab.cern.ch/lhcb-online/fiberdb
    # For instructions how to start a server via docker.
    db_url = "http://0.0.0.0:8001/fiberdb/"

    # Loop over each row extracting required data
    print("")
    for side in rich_sides:

        # Load the data from the SQL DB via a csv web request
        url = db_url + "fibers_with_source_id.csv?_sort=sd_name&_labels=on&_stream=on&pcie40_name__contains=TEL&sd_name__contains=" + side + "&_size=max"
        print("\nLoading", url, "\n")
        data = pandas.read_csv(
            io.StringIO(requests.get(url).content.decode('utf-8')))
        print(data)

        # list of data for text file
        module_names = []
        tel40_names = []
        module_nums = []
        pdmdb_nums = []
        pdmdb_links = []
        tel40_sourceids = []
        tel40_connectors = []
        tel40_mpos = []
        pmt_types = []
        active_status = []

        # loop over all rows
        for irow in range(data.shape[0]):

            # Get a PDMDB Name string
            pdmdb_name = data['sd_name'][irow]
            module_names += [pdmdb_name]

            # Decode the sd_name into fields
            info = sd_name_parse(pdmdb_name)

            # module number
            module_num = get_module_num(info)
            module_nums += [module_num]

            # PDMDB
            pdmdb_num = info["PDMDB"]
            pdmdb_nums += [pdmdb_num]

            # PDMDB Link
            pdmdb_link = get_pdmdb_link(info)
            pdmdb_links += [pdmdb_link]

            # PMT type
            pmt_type = get_pmt_type(info)
            pmt_types += [pmt_type]

            # Tel40 info
            tel40_name = data["pcie40_name"][irow]
            tel40_names += [tel40_name]
            tel40_mpo = data["pcie40_mpo_port"][irow]
            #tel40_mpo = normaliseMPO(tel40_mpo)
            tel40_mpos += [tel40_mpo]
            tel40_connector = data["pcie40_mpo_fiber"][irow]
            tel40_connectors += [tel40_connector]

            status = info["STATUS"] == 'A'
            active_status += [1 if status else 0]

            # Source ID
            source_id = data["source_id"][irow]
            tel40_sourceids += [source_id]

            print(pdmdb_name, "| PMT Type", pmt_type, "| Module", module_num,
                  "| PDMDB", pdmdb_num, "| Link", pdmdb_link, "| Tel40",
                  tel40_name, "| Tel40-SourceID", source_id,
                  str(format(source_id,
                             '016b')), '| Tel40-connector', tel40_connector,
                  "| Tel40-MPO", tel40_mpo, "| Used-Status", status)

        # Create XML conditions file
        f_map = xmlStart(
            filename=side + "_Tel40CablingMap.xml",
            output_dir="output/cabling/sqldb")

        f_map.write("<!-- Module Numbers numbered from 0 -->\n\n")

        f_map.write("<condition classID=\"5\" name=\"" + side +
                    "_Tel40CablingMap\">\n\n")

        writeParam(f_map, len(pmt_types), 'NumberOfLinks',
                   'Total number of active links for this RICH side')
        writeParamVector(f_map, pmt_types, 'PMTTypes',
                         'PMT type for each Tel40 link', 'string')
        writeParamVector(
            f_map,
            module_names,
            'ModuleNames',
            'PDMDB name for each Tel40 link',
            'string',
            pad_size=38,
            max_per_line=3)
        writeParamVector(f_map, module_nums, 'ModuleNumbers',
                         'PDM module number for each Tel40 link')
        writeParamVector(f_map, pdmdb_nums, 'PDMDBNumbers',
                         'PDMDB number for each Tel40 link')
        writeParamVector(f_map, pdmdb_links, 'PDMDBLinks',
                         'PDMDB link number, for each Tel40 link')
        writeParamVector(
            f_map,
            tel40_names,
            'Tel40Names',
            'Tel40 name for each Tel40 link',
            'string',
            pad_size=16,
            max_per_line=6)
        writeParamVector(f_map, tel40_sourceids, 'Tel40SourceIDs',
                         'Tel40 Source ID for each Tel40 link')
        writeParamVector(f_map, tel40_connectors, 'Tel40SConnectors',
                         'Tel40 MPO connector in MPO for each Tel40 link')
        writeParamVector(f_map, tel40_mpos, 'Tel40MPOs',
                         'Tel40 Tel40 MPO for each Tel40 link')
        writeParamVector(
            f_map, active_status, 'Tel40LinkIsActive',
            'Flag indicating if link is active (1=Active,0=Inactive)')

        # close the text file
        xmlEnd(f_map)


def cable_maps_excel():

    # Detector sides
    rich_sides = ["R1U", "R1D", "R2A", "R2C"]

    #rich_sides = [ "R1U" ]

    # Parse the PDMDB name string
    def dbmdb_parse(pdmdb):
        # parse the name string to extract the numerical data
        r = parse("R{}{}{}.PDMDB{}.{}.{}", pdmdb)
        return {
            "RICH": int(r[0]),
            "PANEL": r[1],
            "ROW": int(r[2]),
            "COL": int(r[4]),
            "TYPE": r[3],
            "PDMDB": int(r[5])
        }

    # Get the s/w module number from PDMDB name
    def get_module_num(pdmdb):
        info = dbmdb_parse(pdmdb)
        nColPerRow = 6
        # offsets by RICH and panel
        offsets = {(1, 'U'): 0, (1, 'D'): 66, (2, 'A'): 132, (2, 'C'): 204}
        # return module number
        return offsets[(info["RICH"], info["PANEL"])] + (
            nColPerRow * info["ROW"]) + info["COL"]

    # Get PDMDB number (0,1)
    def get_pdmdb_num(pdmdb):
        info = dbmdb_parse(pdmdb)
        return info["PDMDB"]

    # get PDMDB link number
    def get_pdmdb_link(lc_con_fe):
        r = parse("LC.{}.{}", lc_con_fe)
        return (2 * int(r[0])) + int(r[1])

    # make up a source ID
    # assuming, for now, one PDM per Source ID
    def make_up_source_id(tel40_name, tel40_mpo):
        r = parse("Rich{}_{}-{}", tel40_name)
        panel_num = {'Up': 0, 'Down': 33, 'A': 66, 'C': 86}
        return panel_num[r[1]] + int(r[2])
        #return (10000*int(r[0])) + (1000*panel_num[r[1]]) + (10*int(r[2]))
        #+ int(tel40_mpo)

    # clean up string names
    def cleanName(name):
        # replace spaces with underscore
        name = name.replace(" ", "_")
        return name

    last_source_id = 0
    last_tel40_name = None

    # Loop over each row extracting required data
    print("")
    for side in rich_sides:

        mapping_file = 'excel/RICHCablingTable-v4.1.xlsx'
        print("\nReading", side, "data from", mapping_file, "\n")

        edata = pandas.read_excel(
            mapping_file, sheet_name='Data ' + side, skiprows=[0])
        print(edata)

        # list of data for text file
        module_names = []
        tel40_names = []
        module_nums = []
        pdmdb_nums = []
        pdmdb_links = []
        tel40_sourceids = []
        tel40_connectors = []
        tel40_mpos = []
        pmt_types = []
        active_status = []

        # Get excel data for this and loop over all rows
        for irow in range(edata.shape[0]):

            # PMT type
            pmt_type = 'R' if side in ["R1U", "R1D"] else edata['Region'][irow]
            pmt_types += [pmt_type]

            # Get the PDMDB name string
            pdmdb_name = edata['PDMDB'][irow]
            lcConFE_name = edata['LC Connector FE'][irow]
            module_names += [pdmdb_name + "-" + lcConFE_name]

            # get module number
            module_num = get_module_num(pdmdb_name)
            module_nums += [module_num]

            # Get PDMDB number within PDM (0,1)
            pdmdb_num = get_pdmdb_num(pdmdb_name)
            pdmdb_nums += [pdmdb_num]

            # Get PDMDB link number
            lc_con_fe = edata['LC Connector FE'][irow]
            pdmdb_link = get_pdmdb_link(lc_con_fe)
            pdmdb_links += [pdmdb_link]

            # Get the Tel40 name
            tel40_name = cleanName(edata['TELL40'][irow])
            tel40_mpo = edata['MPO TELL40'][irow]
            tel40_names += [tel40_name]
            tel40_mpos += [tel40_mpo]
            if not last_tel40_name: last_tel40_name = tel40_name

            # Tel40 connector
            tel40_connector = edata['LC connector T40'][irow]
            tel40_connectors += [tel40_connector]

            # Currently source IDs not known so make one up from Tel40 name and MPO number
            source_id = make_up_source_id(tel40_name, tel40_mpo)
            tel40_sourceids += [source_id]

            # Used status
            if side in ["R1U", "R1D"]:
                status = edata['Used Status'][irow] != "Not used"
            else:
                status = True
            active_status += [1 if status else 0]

            print(pdmdb_name, "| PMT Type", pmt_type, "| Module", module_num,
                  "| PDMDB", pdmdb_num, "| Link", pdmdb_link, "| Tel40",
                  tel40_name, "| Tel40-SourceID", source_id,
                  '| Tel40-connector', tel40_connector, "| Tel40-MPO",
                  tel40_mpo, "| Used-Status", status)

            if source_id <= last_source_id and tel40_name != last_tel40_name:
                print("Problem with source IDs")
                exit(1)
            last_source_id = source_id
            last_tel40_name = tel40_name

        pad_size = 6

        # Create XML conditions file
        f_map = xmlStart(
            filename=side + "_Tel40CablingMap.xml",
            output_dir="output/cabling/excel")

        f_map.write("<!-- Module Numbers numbered from 0 -->\n\n")

        f_map.write("<condition classID=\"5\" name=\"" + side +
                    "_Tel40CablingMap\">\n\n")

        writeParam(f_map, len(pmt_types), 'NumberOfLinks',
                   'Total number of active links for this RICH side')
        writeParamVector(f_map, pmt_types, 'PMTTypes',
                         'PMT type for each Tel40 link', 'string')
        writeParamVector(
            f_map,
            module_names,
            'ModuleNames',
            'PDMDB name for each Tel40 link',
            'string',
            pad_size=26,
            max_per_line=4)
        writeParamVector(f_map, module_nums, 'ModuleNumbers',
                         'PDM module number for each Tel40 link')
        writeParamVector(f_map, pdmdb_nums, 'PDMDBNumbers',
                         'PDMDB number for each Tel40 link')
        writeParamVector(f_map, pdmdb_links, 'PDMDBLinks',
                         'PDMDB link number, for each Tel40 link')
        writeParamVector(
            f_map,
            tel40_names,
            'Tel40Names',
            'Tel40 name for each Tel40 link',
            'string',
            pad_size=16,
            max_per_line=6)
        writeParamVector(f_map, tel40_sourceids, 'Tel40SourceIDs',
                         'Tel40 Source ID for each Tel40 link')
        writeParamVector(f_map, tel40_connectors, 'Tel40SConnectors',
                         'Tel40 MPO connector in MPO for each Tel40 link')
        writeParamVector(f_map, tel40_mpos, 'Tel40MPOs',
                         'Tel40 Tel40 MPO for each Tel40 link')
        writeParamVector(
            f_map, active_status, 'Tel40LinkIsActive',
            'Flag indicating if link is active (1=Active,0=Inactive)')

        # close the text file
        xmlEnd(f_map)


def main():

    # get the readout mappings for the PDMs from Steve's spreadsheet
    pdm_readout_mapping('R')
    pdm_readout_mapping('H')

    # PDM -> Tel40 cable mapping
    cable_maps_excel()
    cable_maps_sqldb()


if __name__ == '__main__':
    main()
