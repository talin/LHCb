/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Kernel
#include "Kernel/RichSmartID.h"

// Gaudi
#include "GaudiKernel/SerializeSTL.h"

// Det Desc
#include "DetDesc/ConditionKey.h"

#ifndef USE_DD4HEP
// Temporary. To check if conditions exist (see below)
#  include "GaudiAlg/GetData.h"
#  include "RichDetectors/Rich1.h"
#endif

// Rich Detector
#include "RichDet/DeRichLocations.h"
#include "RichDetectors/Condition.h"
#include "RichDetectors/Utilities.h"

// RICH Utils
#include "RichUtils/AllocateCount.h"
#include "RichUtils/RichDAQDefinitions.h"

// STL
#include <algorithm>
#include <array>
#include <cassert>
#include <cstdint>
#include <ostream>
#include <set>
#include <string>
#include <vector>

namespace Gaudi {
  class Algorithm;
}

namespace Rich::Future::DAQ {

  // overloads for vectors etc.
  using GaudiUtils::operator<<;

  /// Helper class for RICH PDMDB readout mapping
  class PDMDBEncodeMapping final {

  public:
    // types

    /// Local struct to hold conditions
    struct EncodingConds {
      DetectorArray<const Rich::Detector::Condition*> rTypeConds{{}};
      const Rich::Detector::Condition*                hTypeCond{nullptr};
    };

  public:
    /// Null constructor
    explicit PDMDBEncodeMapping( const Gaudi::Algorithm* parent ) : m_parent( parent ) {}

    /// Constructor from mapping conditions
    PDMDBEncodeMapping( const EncodingConds     C, //
                        const Gaudi::Algorithm* parent = nullptr )
        : m_isInitialised( true ) // set default init status to OK
        , m_parent( parent ) {
      // load the mapping conditions needed for encoding
      checkVersion( C );
      fillRType( C );
      fillHType( C );
    }

  public:
    // data types

    /// The data for each anode
    class AnodeData final {
    public:
      /// The PDMDB number in a module (0,1)
      Rich::DAQ::PDMDBID pdmdb;
      /// The data frame in a PDMDB (0-5)
      Rich::DAQ::PDMDBFrame frame;
      /// The data frame bit (0-85)
      Rich::DAQ::FrameBitIndex bit;

    public:
      /// Default constructor
      AnodeData() = default;
      /// Constructor from values
      AnodeData( const Rich::DAQ::PDMDBID       p, ///< PDMDB ID
                 const Rich::DAQ::PDMDBFrame    f, ///< PDMDB Frame
                 const Rich::DAQ::FrameBitIndex b  ///< Frame bit
                 )
          : pdmdb( p ), frame( f ), bit( b ) {}

    public:
      /// Check if data is valid
      inline constexpr bool isValid() const noexcept { return ( pdmdb.isValid() && frame.isValid() && bit.isValid() ); }

    public:
      /// ostream operator
      friend std::ostream& operator<<( std::ostream& os, const AnodeData& ad ) {
        return os << "{ PDMDB=" << ad.pdmdb << " Frame=" << ad.frame << " Bit=" << ad.bit << " }";
      }
    };

    /// Array of Anode data for a single PMT
    using PmtData = std::array<AnodeData, LHCb::RichSmartID::MaPMT::TotalPixels>;

    /// Array of PMT data for a single R Type elementary cell
    using RTypeECData = std::array<PmtData, LHCb::RichSmartID::MaPMT::RTypePMTsPerEC>;

    /// Array of EC's per R Type module
    using RTypeModuleData = std::array<RTypeECData, LHCb::RichSmartID::MaPMT::ECsPerModule>;

    /// Array of PMT data for a single H Type elementary cell
    using HTypeECData = std::array<PmtData, LHCb::RichSmartID::MaPMT::HTypePMTsPerEC>;

    /// Array of EC's per H Type module
    using HTypeModuleData = std::array<HTypeECData, LHCb::RichSmartID::MaPMT::ECsPerModule>;

    /// R-Type Module data for each RICH
    using RTypeRichData = DetectorArray<RTypeModuleData>;

  private:
    // defines

    /// Number of Anodes per PMT
    static constexpr auto NumAnodes = LHCb::RichSmartID::MaPMT::TotalPixels;

  private:
    // methods

    /// checks mapping version
    void checkVersion( const EncodingConds& C );

    /// fill R Type PMT anode map data
    void fillRType( const EncodingConds& C );

    /// fill H Type PMT anode map data
    void fillHType( const EncodingConds& C );

  public:
    // accessors

    /// name string
    static constexpr auto MyName() noexcept { return "Rich::Future::DAQ::PDMDBEncodeMapping"; }

    /// mapping version
    auto version() const noexcept { return m_mappingVer; }

    /// Access the initialisation state
    inline bool isInitialised() const noexcept { return m_isInitialised; }

    /// Access the Anode data for given channel ID
    const auto& anodeData( const LHCb::RichSmartID id ) const noexcept {
      // Elementary cell in module
      const auto ec = id.elementaryCell();
      // pmt number in EC
      const auto pmt = id.pdNumInEC();
      // Anode index (0-63)
      const auto anode = id.anodeIndex();
      // get PMT type specific data
      const auto& d = ( id.isHTypePMT() ? m_hTypeData.at( ec ).at( pmt ).at( anode )
                                        : m_rTypeData.at( id.rich() ).at( ec ).at( pmt ).at( anode ) );
      assert( d.isValid() );
      return d;
    }

  public:
    // conditions handling

    /// Default conditions name
    inline static const std::string DefaultConditionKey =
        DeRichLocations::derivedCondition( "PDMDBEncodeMapping-Handler" );

    /// Creates a condition derivation
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent ) {
      // Assume parent algorithm has one and only one input of the correct type...
      return addConditionDerivation( parent, parent->template inputLocation<PDMDBEncodeMapping>() );
    }

    /// Creates a condition derivation for the given key
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key ) {
      if ( parent->msgLevel( MSG::DEBUG ) ) {
        parent->debug() << "PDMDBEncodeMapping::addConditionDerivation : Key=" << key << endmsg;
      }
      const std::array<std::string, 3> cond_paths{
#ifdef USE_DD4HEP
          "/world/BeforeMagnetRegion/Rich1:PDMDB_R_EncodePixelMap",
          "/world/AfterMagnetRegion/Rich2:PDMDB_R_EncodePixelMap",
          "/world/AfterMagnetRegion/Rich2:PDMDB_H_EncodePixelMap"
#else
          "/dd/Conditions/ReadoutConf/Rich1/PDMDB_R_EncodePixelMap",
          "/dd/Conditions/ReadoutConf/Rich2/PDMDB_R_EncodePixelMap",
          "/dd/Conditions/ReadoutConf/Rich2/PDMDB_H_EncodePixelMap"
#endif
      };
#ifndef USE_DD4HEP
      // NOTE: CheckData test only needed here to deal with fact
      // not all DB tags currently in use have the required mapping conditions.
      // We detect this here and just return a default uninitialised object.
      // downstream users always check if the object is initialised before using
      // the object, which is only done when the DB tags require it.
      // Once support for the old DB tags is no longer required the test can be removed.
      if ( std::all_of( cond_paths.begin(), cond_paths.end(), //
                        [detSvc = parent->detSvc()]( const auto& c ) {
                          return Gaudi::Utils::CheckData<Rich::Detector::Condition>()( detSvc, c );
                        } ) ) {
#endif
        return parent->addConditionDerivation( std::move( cond_paths ),                             // input
                                               std::move( key ),                                    // output
                                               [p = parent]( const Rich::Detector::Condition& r1Cr, //
                                                             const Rich::Detector::Condition& r2Cr, //
                                                             const Rich::Detector::Condition& r2Ch ) {
                                                 return PDMDBEncodeMapping{EncodingConds{{&r1Cr, &r2Cr}, &r2Ch}, p};
                                               } );
#ifndef USE_DD4HEP
      } else {
        // needs to depend on 'something' so fake a dependency on Rich1
        Detector::Rich1::addConditionDerivation( parent );
        // return an unintialised object
        return parent->addConditionDerivation(
            {Detector::Rich1::DefaultConditionKey}, std::move( key ),
            [p = parent]( const Detector::Rich1& ) { return PDMDBEncodeMapping{p}; } );
      }
#endif
    }

  private:
    /// Set initialisation status
    inline void setIsInitialised( const bool ok = true ) noexcept { m_isInitialised = ok; }

    /// Define the messenger entity
    inline auto messenger() const noexcept {
      assert( m_parent );
      return m_parent;
    }

  private:
    // data

    /// R Type module data
    RTypeRichData m_rTypeData;

    /// H Type module data (only for RICH2)
    HTypeModuleData m_hTypeData;

    /// Flag to indicate initialisation status
    bool m_isInitialised{false};

    /// Mapping version
    int m_mappingVer{-1};

    /// Pointer back to parent algorithm (for messaging)
    const Gaudi::Algorithm* m_parent{nullptr};

  private:
    /// Allocation tracking
    Rich::AllocateCount<PDMDBEncodeMapping> m_track_instances;
  };

} // namespace Rich::Future::DAQ
