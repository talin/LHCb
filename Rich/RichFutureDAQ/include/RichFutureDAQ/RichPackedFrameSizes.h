/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <bitset>
#include <cstdint>
#include <iomanip>
#include <ostream>

// Boost
#include "boost/format.hpp"
#include "boost/format/group.hpp"

namespace Rich::Future::DAQ {

  /// Helper class to pack to 4-bit byte counts into one 8-bit byte
  class PackedFrameSizes final {

  public:
    /// Packed type
    using IntType = std::uint8_t;

  private:
    // Bits for each Size
    static const IntType Bits0 = 4;
    static const IntType Bits1 = 4;
    // shifts
    static const IntType Shift0 = 0;
    static const IntType Shift1 = Shift0 + Bits0;
    // masks
    static const IntType Mask0 = ( IntType )( ( 1 << Bits0 ) - 1 ) << Shift0;
    static const IntType Mask1 = ( IntType )( ( 1 << Bits1 ) - 1 ) << Shift1;
    // max values
    static const IntType Max0 = ( 1 << Bits0 ) - 1;
    static const IntType Max1 = ( 1 << Bits1 ) - 1;

  private:
    /// Set the overall word
    inline void setData( const IntType data ) { m_data = data; }

    /// Set data for given word
    inline void set( const IntType value, //
                     const IntType shift, //
                     const IntType mask ) {
      setData( ( ( value << shift ) & mask ) | ( data() & ~mask ) );
    }

  public:
    /// Constructor from two sizes
    PackedFrameSizes( const IntType size0, const IntType size1 ) {
      set( size0, Shift0, Mask0 );
      set( size1, Shift1, Mask1 );
    }

    /// Contructor from a single word
    explicit PackedFrameSizes( const IntType d ) : m_data( d ) {}

  public:
    /// Set size0
    inline void setSize0( const IntType size0 ) noexcept { set( size0, Shift0, Mask0 ); }

    /// Set size1
    inline void setSize1( const IntType size1 ) noexcept { set( size1, Shift1, Mask1 ); }

    /// set the size for the give word number
    inline void setSize( const IntType size, const IntType word ) noexcept {
      assert( word <= 1 );
      if ( 0 == word ) {
        setSize0( size );
      } else {
        setSize1( size );
      }
    }

  public:
    /// Get the overall data
    inline IntType data() const noexcept { return m_data; }

    /// Get first size word
    inline IntType size0() const noexcept { return ( ( data() & Mask0 ) >> Shift0 ); }

    /// Get second size word
    inline IntType size1() const noexcept { return ( ( data() & Mask1 ) >> Shift1 ); }

    /// Get the total size
    inline auto totalSize() const noexcept { return size0() + size1(); }

  public:
    /// ostream operator
    friend std::ostream& operator<<( std::ostream& os, const PackedFrameSizes& d ) {
      // zero pad its to two digits...
      boost::format twoDigits( "%u" );
      twoDigits.modify_item( 1, boost::io::group( std::setw( 2 ), std::setfill( '0' ) ) );
      return os << "{ " << boost::format( "%02X" ) % (int)d.data()                               //
                << "(" << std::bitset<8>( d.data() ) << ") Size1:" << twoDigits % (int)d.size1() //
                << " Size0:" << twoDigits % (int)d.size0() << " }";
    }

  private:
    /// The data word
    IntType m_data{0};
  };

} // namespace Rich::Future::DAQ
