/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Kernel
#include "Kernel/RichSmartID.h"

// Gaudi
#include "GaudiKernel/SerializeSTL.h"

// Det Desc
#include "DetDesc/ConditionKey.h"
#include "DetDesc/DetectorElement.h"

// Boost
#include "boost/container/static_vector.hpp"

// RICH Utils
#include "RichUtils/AllocateCount.h"
#include "RichUtils/RichDAQDefinitions.h"

#ifndef USE_DD4HEP
// Temporary for DetDesc. To check if conditions exist (see below).
#  include "GaudiAlg/GetData.h"
#  include "RichDetectors/Rich1.h"
#endif

// Rich Detector
#include "RichDet/DeRichLocations.h"
#include "RichDetectors/Condition.h"
#include "RichDetectors/Utilities.h"

namespace Gaudi {
  class Algorithm;
}

// STL
#include <algorithm>
#include <array>
#include <cassert>
#include <cstdint>
#include <map>
#include <ostream>
#include <set>
#include <sstream>
#include <vector>

namespace Rich::Future::DAQ {

  // overloads for vectors etc.
  using GaudiUtils::operator<<;

  /// Helper class for RICH PMT data format encoding
  class Tel40CableMapping final {

  public:
    /// Local struct to hold conditions
    using Conds = std::array<const Rich::Detector::Condition*, Rich::NTotalPDPanels>;

  public:
    /// Null constructor
    explicit Tel40CableMapping( const Gaudi::Algorithm* parent ) : m_parent( parent ) {}

    /// Constructor from LHCb and RICH detector elements or conditions
    Tel40CableMapping( const LHCb::Detector::DeLHCb& lhcb, //
                       const Conds&                  C,    //
                       const Gaudi::Algorithm*       parent = nullptr )
        : m_isInitialised( true ) // Set default init status to OK
        , m_parent( parent ) {
      // load the mapping conditions needed for encoding
      fillCableMaps( lhcb, C );
    }

  public:
    // data types

    /// Struct for storing data for each Tel40 Link
    class Tel40LinkData final {
    public:
      /// RICH SmartID
      LHCb::RichSmartID smartID;
      /// Source ID
      Rich::DAQ::SourceID sourceID;
      /// Tel40 connector
      Rich::DAQ::Tel40Connector connector;
      /// Module Number
      Rich::DAQ::PDModuleNumber moduleNum;
      /// PDMDB number (0,1)
      Rich::DAQ::PDMDBID pdmdbNum;
      /// Link number
      Rich::DAQ::PDMDBFrame linkNum;
      /// PMT type
      bool isHType{false};
      /// Is Link Active
      bool isActive{false};
      /// Module Name
      std::string name{"UNDEFINED"};

    public:
      /// Default constructor
      Tel40LinkData() = default;
      /// Constructor from values
      Tel40LinkData( const std::string&              n,     ///< Tel40 link name
                     const LHCb::RichSmartID         ID,    ///< Cached Smart ID for panel
                     const Rich::DAQ::SourceID       sID,   ///< Source ID
                     const Rich::DAQ::Tel40Connector c,     ///< Tel40 connector
                     const bool                      isH,   ///< Is H type PMT
                     const bool                      act,   ///< Active Link Flag
                     const Rich::DAQ::PDModuleNumber mod,   ///< PDM Module Number
                     const Rich::DAQ::PDMDBID        pdmdb, ///< PDMDB number
                     const Rich::DAQ::PDMDBFrame     link   ///< PDMDB Link (Frame)
                     )
          : smartID( ID )
          , sourceID( sID )
          , connector( c )
          , moduleNum( mod )
          , pdmdbNum( pdmdb )
          , linkNum( link )
          , isHType( isH )
          , isActive( act )
          , name( n ) {}

    public:
      /// Check if data is valid
      inline constexpr bool isValid() const noexcept {
        return ( sourceID.isValid() && connector.isValid() && moduleNum.isValid() && pdmdbNum.isValid() );
      }

    public:
      /// ostream operator
      friend std::ostream& operator<<( std::ostream& os, const Tel40LinkData& td ) {
        return os << "{ " << td.name << " " << td.smartID << " Module=" << td.moduleNum << " PDMDB=" << td.pdmdbNum
                  << " PDMDB-Link=" << td.linkNum << " Active=" << td.isActive << " SourceID=" << td.sourceID
                  << " Tel40-Connector=" << td.connector << " IsH=" << td.isHType << " }";
      }
    };

    /// Max number of links(frames) per PDMDB
    static constexpr const std::size_t MaxLinksPerPDMDB = 6;

    /// Number of PDMDBs per module
    static constexpr const std::size_t PDMDBPerModule = 2;

    /// Number of Tel40 connections per MPO
    static constexpr const std::size_t ConnectionsPerTel40MPO = 12;

    /// Maximum number of active Tel40 MPOs per Source ID
    static constexpr const std::size_t MaxNumberMPOsPerSourceID = 2;

    /// Maximum Number of connections per Tel40
    static constexpr const std::size_t MaxConnectionsPerTel40 = MaxNumberMPOsPerSourceID * ConnectionsPerTel40MPO;

    /// Array of Tel40 for each link in a PDMDB
    using PDMDBLinkData = std::array<Tel40LinkData, MaxLinksPerPDMDB>;

    /// Array of LinkData for each PDMDB in a module
    using PDMDBData = std::array<PDMDBLinkData, PDMDBPerModule>;

    /// Tel40 data for each Module
    using ModuleTel40Data = std::array<PDMDBData, LHCb::RichSmartID::MaPMT::TotalModules>;

    /// Map of active Tel40 Links, per source ID
    using LinksPerSourceID = std::map<Rich::DAQ::SourceID, std::set<Rich::DAQ::Tel40Connector>>;

    /// Array of Tel40 data structs for each connection
    class Tel40Connections final : public boost::container::static_vector<Tel40LinkData, MaxConnectionsPerTel40> {
    public:
      /// Flag to indicate if at least one link is inactive
      bool hasInactiveLinks{false};
      /// Total number of active links;
      std::size_t nActiveLinks{0};
    };

    /// connection data for each SourceID
    using Tel40SourceIDs = DetectorArray<PanelArray<std::vector<Tel40Connections>>>;

  private:
    // methods

    /// fill Tel40 cable map data
    void fillCableMaps( const LHCb::Detector::DeLHCb& lhcb, const Conds& C );

  public:
    // accessors

    /// Access the initialisation state
    inline bool isInitialised() const noexcept { return m_isInitialised; }

    /// Access the Tel40 Link data for given channel ID
    const auto& tel40Data( const LHCb::RichSmartID     id,    // PD ID
                           const Rich::DAQ::PDMDBID    pdmdb, // PDMDB ID
                           const Rich::DAQ::PDMDBFrame frame  // PDMDB Frame
                           ) const noexcept {
      // module number
      const auto modN = id.pdMod();
      // return tel40 data
      const auto& data = m_tel40ModuleData.at( modN ).at( pdmdb.data() ).at( frame.data() );
      assert( data.isValid() );
      // finally return
      return data;
    }

    /// Get the active links per source ID
    const auto& linksPerSourceID() const noexcept { return m_linksPerSourceID; }

    /// Access the Tel40 connection data for a given SourceID
    const auto& tel40Data( const Rich::DAQ::SourceID sID ) const {
      assert( sID.isValid() );
      return m_tel40ConnData.at( sID.rich() ).at( sID.side() ).at( sID.payload() );
    }

    /// mapping version
    inline auto version() const noexcept { return m_mappingVer; }

    /// name string
    static constexpr auto MyName() noexcept { return "Rich::Future::DAQ::Tel40CableMapping"; }

  public:
    // Conditions handling

    /// Default conditions name
    inline static const std::string DefaultConditionKey =
        DeRichLocations::derivedCondition( "Tel40CableMapping-Handler" );

    /// paths to the various mapping conditions for each RICH/panel
    inline static const std::array<std::string, Rich::NTotalPDPanels> ConditionPaths{
#ifdef USE_DD4HEP
        "/world/BeforeMagnetRegion/Rich1:R1U_Tel40CablingMap", //
        "/world/BeforeMagnetRegion/Rich1:R1D_Tel40CablingMap", //
        "/world/AfterMagnetRegion/Rich2:R2A_Tel40CablingMap",  //
        "/world/AfterMagnetRegion/Rich2:R2C_Tel40CablingMap"
#else
        "/dd/Conditions/ReadoutConf/Rich1/R1U_Tel40CablingMap", //
        "/dd/Conditions/ReadoutConf/Rich1/R1D_Tel40CablingMap", //
        "/dd/Conditions/ReadoutConf/Rich2/R2A_Tel40CablingMap", //
        "/dd/Conditions/ReadoutConf/Rich2/R2C_Tel40CablingMap"
#endif
    };

    /// Creates a condition derivation
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent ) {
      // Assume parent algorithm has one and only one input of the correct type...
      return addConditionDerivation( parent, parent->template inputLocation<Tel40CableMapping>() );
    }

    /// Creates a condition derivation for the given key
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key ) {
      if ( parent->msgLevel( MSG::DEBUG ) ) {
        parent->debug() << "Tel40CableMapping::addConditionDerivation : Key=" << key << endmsg;
      }
#ifndef USE_DD4HEP
      // NOTE: CheckData test only needed here to deal with fact
      // not all DB tags currently in use have the required mapping conditions.
      // We detect this here and just return a default uninitialised object.
      // downstream users always check if the object is initialised before using
      // the object, which is only done when the DB tags require it.
      // Once support for the old DB tags is no longer required the test can be removed.
      if ( std::all_of( ConditionPaths.begin(), ConditionPaths.end(), //
                        [detSvc = parent->detSvc()]( const auto& c ) {
                          return Gaudi::Utils::CheckData<Rich::Detector::Condition>()( detSvc, c );
                        } ) ) {
#endif
        return parent->addConditionDerivation( std::array{LHCb::standard_geometry_top,              //
                                                          ConditionPaths[0], ConditionPaths[1],     //
                                                          ConditionPaths[2], ConditionPaths[3]},    //
                                               std::move( key ),                                    // output
                                               [p = parent]( const LHCb::Detector::DeLHCb&    lhcb, //
                                                             const Rich::Detector::Condition& r1U,  //
                                                             const Rich::Detector::Condition& r1D,  //
                                                             const Rich::Detector::Condition& r2A,  //
                                                             const Rich::Detector::Condition& r2C ) {
                                                 return Tel40CableMapping{lhcb, Conds{&r1U, &r1D, &r2A, &r2C}, p};
                                               } );
#ifndef USE_DD4HEP
      } else {
        // needs to depend on 'something' so fake a dependency on Rich1
        Detector::Rich1::addConditionDerivation( parent );
        // return an unintialised object
        return parent->addConditionDerivation(
            {Detector::Rich1::DefaultConditionKey}, std::move( key ),
            [p = parent]( const Detector::Rich1& ) { return Tel40CableMapping{p}; } );
      }
#endif
    }

  private:
    /// Set initialisation status
    inline void setIsInitialised( const bool ok = true ) noexcept { m_isInitialised = ok; }

    /// Define the messenger entity
    inline auto messenger() const noexcept {
      assert( m_parent );
      return m_parent;
    }

  private:
    // data

    /// Tel40 connection mapping data
    Tel40SourceIDs m_tel40ConnData;

    /// Tel40 Module Mapping data
    ModuleTel40Data m_tel40ModuleData;

    /// Active links per source ID
    LinksPerSourceID m_linksPerSourceID;

    /// Flag to indicate initialisation status
    bool m_isInitialised{false};

    /// Mapping version
    int m_mappingVer{-1};

    /// Pointer back to parent algorithm (for messaging)
    const Gaudi::Algorithm* m_parent{nullptr};

  private:
    /// Allocation tracking
    Rich::AllocateCount<Tel40CableMapping> m_track_instances;
  };

} // namespace Rich::Future::DAQ
