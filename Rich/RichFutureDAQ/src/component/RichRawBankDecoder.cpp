/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi Array properties ( must be first ...)
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Rich Kernel
#include "RichFutureKernel/RichAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichUtils/RichException.h"
#include "RichUtils/RichHashMap.h"
#include "RichUtils/RichMap.h"
#include "RichUtils/RichSmartIDSorter.h"

// RICH DAQ
#include "RichFutureDAQ/RichPDMDBDecodeMapping.h"
#include "RichFutureDAQ/RichPackedFrameSizes.h"
#include "RichFutureDAQ/RichTel40CableMapping.h"

// Event model
#include "Event/RawEvent.h"

// Detectors
#include "RichDetectors/RichPDInfo.h"

// Boost
#include "boost/format.hpp"

// Old 32 bit SmartID
#include "Kernel/RichSmartID32.h"

// STD
#include <algorithm>
#include <array>
#include <bitset>
#include <cassert>
#include <cstdint>
#include <limits>
#include <memory>
#include <numeric>
#include <set>
#include <sstream>
#include <string>
#include <type_traits>

using namespace Rich::Future::DAQ;
using namespace Rich::DAQ;

#define daq_debug( ... ) ri_debug( __VA_ARGS__ );
#define daq_verbo( ... ) ri_verbo_dbgonly( __VA_ARGS__ );
//#define daq_verbo( ... ) ri_verbo( __VA_ARGS__ );

namespace Rich::Future {

  namespace {

    /// Output data
    using OutData = Rich::Future::DAQ::DecodedData;

    /// Returns the RawBank version enum for the given bank
    inline auto bankVersion( const LHCb::RawBank& bank ) noexcept {
      return static_cast<Rich::DAQ::BankVersion>( bank.version() );
    }

    /// Test if a given bit in a word is set on
    template <std::uint8_t BIT, typename TYPE>
    inline constexpr bool isBitOn( const TYPE data ) noexcept {
      return ( 0 != ( data & ( TYPE( 1 ) << BIT ) ) );
    }

    /// Test if a given bit in a word is set on
    template <typename TYPE, typename POS>
    inline constexpr bool isBitOn( const TYPE data, //
                                   const POS  pos ) noexcept {
      return ( 0 != ( data & ( TYPE( 1 ) << pos ) ) );
    }

    /// Set a bit in a word off
    template <std::uint8_t BIT, typename TYPE>
    inline constexpr void setBitOff( TYPE& data ) noexcept {
      data &= ~( TYPE( 1 ) << BIT );
    }

    /// Type for local decoded SmartID cache
    using DecodedIDs = LHCb::RichSmartID::Vector;

  } // namespace

  /** @class RawBankDecoder RichRawBankDecoder.h
   *
   *  RICH Raw bank decoder.
   *
   *  @author Chris Jones
   *  @date   2016-09-21
   */
  class RawBankDecoder final
      : public LHCb::Algorithm::Transformer<OutData( const LHCb::RawBank::View&, //
                                                     const Detector::PDInfo&,    //
                                                     const Tel40CableMapping&,   //
                                                     const PDMDBDecodeMapping& ),
                                            LHCb::DetDesc::usesBaseAndConditions<AlgBase<>,         //
                                                                                 Detector::PDInfo,  //
                                                                                 Tel40CableMapping, //
                                                                                 PDMDBDecodeMapping>> {
  public:
    // framework

    /// Standard constructor
    RawBankDecoder( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data inputs
                       {{"RawBanks", "DAQ/RawBanks/Rich"},
                        // conditions input
                        KeyValue{"RichPDInfo", Detector::PDInfo::DefaultConditionKey},
                        KeyValue{"Tel40CableMapping", Tel40CableMapping::DefaultConditionKey + "-" + name},
                        KeyValue{"PDMDBDecodeMapping", PDMDBDecodeMapping::DefaultConditionKey + "-" + name}},
                       // output data
                       {KeyValue{"DecodedDataLocation", DecodedDataLocation::Default}} ) {}

    /// Initialize
    StatusCode initialize() override {
      // Initialise base class
      auto sc = Transformer::initialize();
      if ( !sc ) return sc;

      // force debug messages
      // sc = setProperty( "OutputLevel", MSG::VERBOSE );

      // derived conditions
      Tel40CableMapping::addConditionDerivation( this );
      PDMDBDecodeMapping::addConditionDerivation( this );
      Detector::PDInfo::addConditionDerivation( this );

      // report inactive RICHes
      if ( !m_richIsActive[Rich::Rich1] ) { info() << "Decoding for RICH1 disabled" << endmsg; }
      if ( !m_richIsActive[Rich::Rich2] ) { info() << "Decoding for RICH2 disabled" << endmsg; }
      if ( !m_sideIsActive[Rich::firstSide] ) { info() << "Decoding for Panel:0 disabled" << endmsg; }
      if ( !m_sideIsActive[Rich::secondSide] ) { info() << "Decoding for Panel:1 disabled" << endmsg; }

      return sc;
    }

    /// Algorithm execution via transform
    OutData operator()( const LHCb::RawBank::View& rawBanks,  //
                        const Detector::PDInfo&    pdInfo,    //
                        const Tel40CableMapping&   tel40Maps, //
                        const PDMDBDecodeMapping&  pdmdbMaps ) const override;

  private:
    /// Decoding for MaPMT1 version
    void decodeToSmartIDs_MaPMT1( const LHCb::RawBank&      bank,      //
                                  const Tel40CableMapping&  tel40Maps, //
                                  const PDMDBDecodeMapping& pdmdbMaps, //
                                  DecodedIDs&               decodedIDs ) const;

    /// Decoding for streamed RichSmartIDs version
    void decodeToSmartIDs_StreamIDs( const LHCb::RawBank&    bank,   //
                                     const Detector::PDInfo& pdInfo, //
                                     DecodedIDs&             decodedIDs ) const;

    /// Fill the output decoded data from a list of Smart IDs
    void fillDecodedData( const DecodedIDs& decodedIDs, //
                          OutData&          decodedData ) const;

    /** Print the given RawBank as a simple hex dump
     *  @param bank The RawBank to dump out
     *  @param os   The Message Stream to print to
     */
    void dumpRawBank( const LHCb::RawBank& bank, MsgStream& os ) const;

  private:
    // properties

    /// Flag to turn on/off decoding of each RICH detector (default is both on)
    Gaudi::Property<Rich::DetectorArray<bool>> m_richIsActive{this, "Detectors", {true, true}};

    /// Flag to turn on/off decoding of specific panels in each RICH
    Gaudi::Property<Rich::PanelArray<bool>> m_sideIsActive{this, "Panels", {true, true}};

    /// Flag to activate the raw printout of each Rawbank
    Gaudi::Property<bool> m_dumpBanks{this, "DumpRawBanks", false};

    /** Turn on/off detailed error messages.
     *  VERY verbose in case of frequent errors... */
    Gaudi::Property<bool> m_verboseErrors{this, "VerboseErrors", false};

  private:
    // messaging

    /// error reading flatlist
    mutable WarningCounter m_flatListReadWarn{this, "Invalid RichSmartID read from FlatList data format"};

    /// Bank decoding error
    mutable ErrorCounter m_rawReadErr{this, "Error decoding RawBank"};

    /// Null RawBank pointer
    mutable ErrorCounter m_nullRawBankErr{this, "Retrieved null pointer to RawBank"};

    /// Magic pattern error
    mutable ErrorCounter m_magicErr{this, "Magic pattern mis-match"};

    /// Not a RICH bank error
    mutable ErrorCounter m_notRichErr{this, "Not a RICH bank"};

    /// Too few header words
    mutable ErrorCounter m_bankSizeTooSmall{this, "Bank Size too small"};

    /// Bank Size MisMatch
    mutable ErrorCounter m_bankSizeMisMatchErr{this, "Decoded bank size mis-match"};

    /// PMT small/large flag mis-match
    mutable ErrorCounter m_pmtSLFlagMismatch{this, "Small/Large PD flag mis-match"};

    /// Invalid SourceID
    mutable ErrorCounter m_invalidSourceID{this, "Invalid Tel40 SourceID"};

    /// Invalid Tel40 connection data
    mutable ErrorCounter m_invalidTel40ConData{this, "Invalid Tel40 connection data"};
  };

} // namespace Rich::Future

using namespace Rich::Future;

//=============================================================================
OutData RawBankDecoder::operator()( const LHCb::RawBank::View& richBanks, //
                                    const Detector::PDInfo&    pdInfo,    //
                                    const Tel40CableMapping&   tel40Maps, //
                                    const PDMDBDecodeMapping&  pdmdbMaps ) const {

  // Get the banks for the Rich
  daq_debug( "Starting decoding of ", richBanks.size(), " RICH Tel40 bank(s)", endmsg );

  // working container for decoded Smart IDs
  // Define here to share allocation across all Source IDs
  DecodedIDs decodedIDs;
  // Pre-allocate minimum 'default' size
  decodedIDs.reserve( 1536 );

  // Make the data map to return
  OutData decodedData;

  // Loop over data banks
  for ( const auto* const bank : richBanks ) {

    // test bank is OK
    if ( !bank ) {
      ++m_nullRawBankErr;
    } else {

      // if configured, dump raw event before decoding
      if ( msgLevel( MSG::VERBOSE ) ) {
        dumpRawBank( *bank, verbose() );
      } else if ( m_dumpBanks ) {
        dumpRawBank( *bank, info() );
      }

      // basic checks
      bool bankOK = true;
      // Check this is a RICH bank
      if ( bank->type() != LHCb::RawBank::Rich ) {
        ++m_notRichErr;
        bankOK = false;
      }
      // check bank magic word
      if ( LHCb::RawBank::MagicPattern != bank->magic() ) {
        ++m_magicErr;
        bankOK = false;
      }

      // If OK, carry on to actual version specific decoding
      if ( bankOK ) {

        // clear the ID vector for this bank
        decodedIDs.clear();

        // decoding might throw exceptions so catch and report
        try {
          // Get bank version
          const auto version = bankVersion( *bank );
          // Check version to dispatch to the correct decoding
          if ( MaPMT1 == version ) {
            // real PMT bank
            decodeToSmartIDs_MaPMT1( *bank, tel40Maps, pdmdbMaps, decodedIDs );
          } else if ( StreamSmartIDs == version ) {
            // simple 'streamed RichSmartID' version (MC only)
            decodeToSmartIDs_StreamIDs( *bank, pdInfo, decodedIDs );
          } else {
            throw Rich::Exception( "Unknown RICH Tel40 version number " + std::to_string( version ) );
          }
          // Finally, fill the output data from the list of SmartIDs
          fillDecodedData( decodedIDs, decodedData );
        }

        // catch and report any exceptions during decoding
        catch ( const GaudiException& expt ) {
          // count errors
          ++m_rawReadErr;
          daq_verbo( expt.message(), endmsg );
          // dump the full bank
          if ( m_verboseErrors ) { dumpRawBank( *bank, error() ); }
        }

      } // BANK ok
    }

  } // bank loop

  // return the fill map
  return decodedData;
}

//=============================================================================

void RawBankDecoder::decodeToSmartIDs_MaPMT1( const LHCb::RawBank&      bank,      //
                                              const Tel40CableMapping&  tel40Maps, //
                                              const PDMDBDecodeMapping& pdmdbMaps, //
                                              DecodedIDs&               decodedIDs ) const {

  // sanity check mappings are properly initialised
  assert( tel40Maps.isInitialised() );
  assert( pdmdbMaps.isInitialised() );

  // Get Tel40 Source ID
  const Rich::DAQ::SourceID tel40ID( bank.sourceID() );
  daq_debug( "MaPMT1 Decoding Tel40 bank ", tel40ID, endmsg );
  if ( !tel40ID.isValid() ) {
    ++m_invalidSourceID;
    std::ostringstream mess;
    mess << "Invalid Source ID " << tel40ID;
    throw Rich::Exception( mess.str() );
  }

  // Data bank size in 8 bit words
  const std::size_t bankSize = bank.size();
  daq_verbo( " -> Bank Size ", bankSize, endmsg );

  // Get the Tel40 DB info for this Source ID
  const auto& connData = tel40Maps.tel40Data( tel40ID );
  daq_verbo( " -> Connection data array size ", connData.size(), endmsg );

  // Expected number of data size words
  const auto nSizeWords = connData.nActiveLinks;

  // ... number of packed words
  const auto nPackedSizeW = ( nSizeWords / 2 ) + ( nSizeWords % 2 );
  daq_debug( " -> Expecting ", connData.nActiveLinks, " active Tel40 Links, ", nPackedSizeW, " packed header words",
             endmsg );
  if ( bankSize < (std::size_t)nPackedSizeW ) {
    ++m_bankSizeTooSmall;
    std::ostringstream mess;
    mess << "Bank Too Small " << tel40ID << " size=" << bankSize;
    throw Rich::Exception( mess.str() );
  }

  // Array to save sizes for each frame
  std::array<PackedFrameSizes::IntType, Tel40CableMapping::MaxConnectionsPerTel40> connSizes{};

  // Decode in 8-bit chunks
  using DT                = std::uint8_t;
  const auto NDataBits    = std::numeric_limits<DT>::digits;
  const auto LastBitIndex = NDataBits - 1;

  // Loop over the first header words to extract the sizes in bytes
  std::size_t     iWord{0}, iPayloadWord{0};
  const DT* const bankStart = bank.begin<DT>();
  const DT*       dataW     = bankStart;      // outside for loop as reused below to decode payload
  const DT* const bankEnd   = bank.end<DT>(); // cache locally
  for ( ; iWord < nPackedSizeW && dataW != bankEnd; ++dataW, ++iWord, iPayloadWord += 2 ) {
    // Extract the sizes from the packed word
    const PackedFrameSizes sizes( *dataW );
    // extract sizes for each packed value
    assert( iPayloadWord < connSizes.size() );
    connSizes[iPayloadWord] = sizes.size1();
    assert( ( iPayloadWord + 1 ) < connSizes.size() );
    connSizes[iPayloadWord + 1] = sizes.size0();
    daq_verbo( "  -> Word ", boost::format( "%02i" ) % iWord, " PackedSizeHeader ", sizes, endmsg );
  }

  // If we have any inactive links need to shift expected data sizes around
  // as padding is at the end, not where the inactive link appears..
  if ( connData.hasInactiveLinks ) {
    daq_debug( " -> Has at least one link inactive -> Correcting for header padding.", endmsg );
    for ( std::size_t iL = 0; iL < connData.size(); ++iL ) {
      if ( !connData[iL].isActive ) {
        daq_debug( "  -> Link ", iL, " is inactive", endmsg );
        for ( auto i = connData.size() - 1; i > iL; --i ) { connSizes[i] = connSizes[i - 1]; }
        connSizes[iL] = 0;
      }
    }
  }

  // Count the total number of payload words.
  // Do this after rearranging for inactive links just to double check we don't
  // loose hits doing this...
  const auto nPayloadWords = std::accumulate( connSizes.begin(), connSizes.end(), 0u );
  daq_debug( " -> Expecting ", nPayloadWords, " payload words", endmsg );

  // Validate the data sizes for each tel40 link extracted from the data
  // is in agreement with the overall bank size
  if ( ( nPackedSizeW + nPayloadWords ) != bankSize ) {
    ++m_bankSizeMisMatchErr;
    std::ostringstream mess;
    mess << "Tel40:" << tel40ID << " BankSize:" << bankSize << " != DecodedSize:" << ( nPackedSizeW + nPayloadWords );
    throw Rich::Exception( mess.str() );
  }

  // Guess at number of hits to be created based on # payload words.
  // Scale factor comes from 'empirical observations'
  decodedIDs.reserve( 8 * nPayloadWords );

  // finally loop over payload words and decode hits
  // note iterator starts from where the above header loop ended...
  std::size_t iLink{0};
  while ( dataW != bankEnd && iLink < connSizes.size() ) {

    daq_verbo( "Link ", iLink, " Size ", (int)connSizes[iLink], endmsg );

    // Do we have any words to decode for this link
    if ( connSizes[iLink] > 0 ) {

      // Get the Tel40 Data for this connection
      const auto& cData = connData[iLink];
      daq_verbo( " -> ", cData, endmsg );
      if ( !cData.isValid() ) {
        ++m_invalidTel40ConData;
        throw Rich::Exception( "Tel40:" + std::to_string( tel40ID.data() ) + " Invalid data for connection " +
                               std::to_string( iLink ) );
      }

      // get the PDMDB data
      const auto& frameData = pdmdbMaps.getFrameData( cData );

      // Loop over the words for this link
      std::uint16_t iW = 0;
      while ( iW < connSizes[iLink] && dataW != bankEnd ) {

        // check MSB for this word
        const auto isNZS = isBitOn<LastBitIndex>( *dataW );
        daq_verbo( "  -> NZS=", isNZS, " iW=", iW, " gW=", ( dataW - bankStart ), //
                   " ", boost::format( "%02X" ) % (int)( *dataW ),                //
                   "(", std::bitset<NDataBits>( *dataW ), ")", endmsg );

        if ( !isNZS ) {
          // ZS decoding... word is bit index

          const std::size_t bitIndex = *dataW;
          daq_verbo( "   -> Bit Index ", bitIndex, endmsg );

          // Check it is in range.
          // Cannot make a hard error as with bit-flips could happen in real data.
          if ( bitIndex < frameData.size() ) {

            // load the anode data for this bit
            const auto& aData = frameData[bitIndex];
            daq_verbo( "    -> ", aData, endmsg );

            // Data 'could' be invalid, e.g. radiation-induced-upsets
            // so cannot make this a hard error
            if ( aData.isValid() ) {
              // make a smart ID
              auto hitID = cData.smartID; // sets RICH, side, module and PMT type
              // Add the PMT and pixel info
              hitID.setPD_EC_PMT( cData.moduleNum.data(), aData.ec.data(), aData.pmtInEC.data() );
              hitID.setAnode_PMT( aData.anode.data() );
              // sanity assert to ensure this hit is not already present
              assert( std::find( decodedIDs.begin(), decodedIDs.end(), hitID ) == decodedIDs.end() );
              daq_verbo( "     -> HIT ", hitID, endmsg );
              decodedIDs.emplace_back( hitID );
            } else {
              daq_verbo( "      -> INVALID", endmsg );
            }
          }

          // move to next word
          ++iW;
          ++dataW;

        } else {
          // NZS decoding...

          // which half of the payload are we in ?
          const bool firstHalf = ( 0 == iW && connSizes[iLink] > 5 );
          // Number of words to decode depends on which half of the payload we are in
          const auto nNZSwords = ( firstHalf ? 6 : 5 );
          // bit offset per half
          const auto halfBitOffset = ( firstHalf ? 39 : 0 );

          // look forward last NZS word and read backwards to match frame bit order
          for ( auto iNZS = nNZSwords - 1; iNZS >= 0; --iNZS ) {

            // read the NZS word
            auto nzsW = *( dataW + iNZS );
            // if word zero clear MSB as this is the NZS flag
            if ( 0 == iNZS ) { setBitOff<LastBitIndex>( nzsW ); }

            daq_verbo( "    -> iNZS=", iNZS, " gW=", iNZS + ( dataW - bankStart ), //
                       " ", boost::format( "%02X" ) % (int)( nzsW ),               //
                       "(", std::bitset<NDataBits>( nzsW ), ")", endmsg );

            // does this word hold any active bits ?
            if ( nzsW > 0 ) {

              // Bit offset for this word
              const auto bitOffset = halfBitOffset + ( NDataBits * ( nNZSwords - 1 - iNZS ) );
              daq_verbo( "     -> Bit Offset ", (int)bitOffset, endmsg );

              // word has data so loop over bits to extract
              for ( auto iLB = 0; iLB < NDataBits; ++iLB ) {
                // is bit on ?
                if ( isBitOn( nzsW, iLB ) ) {

                  // form frame bit value
                  const std::size_t bit = iLB + bitOffset;
                  daq_verbo( "      -> Bit Index ", bit, endmsg );

                  // check bit is in range.
                  // cannot be a hard error as could happen due to e.g. bit flips in real data.
                  if ( bit < frameData.size() ) {
                    // load the anode data for this bit
                    const auto& aData = frameData[bit];
                    daq_verbo( "       -> ", aData, endmsg );
                    // Data 'could' be invalid, e.g. radiation-induced-upsets
                    // so cannot make this a hard error
                    if ( aData.isValid() ) {
                      // make a smart ID
                      auto hitID = cData.smartID; // sets RICH, side, module and PMT type
                      // Add the PMT and pixel info
                      hitID.setPD_EC_PMT( cData.moduleNum.data(), aData.ec.data(), aData.pmtInEC.data() );
                      hitID.setAnode_PMT( aData.anode.data() );
                      // sanity assert to ensure this hit is not already present
                      assert( std::find( decodedIDs.begin(), decodedIDs.end(), hitID ) == decodedIDs.end() );
                      daq_verbo( "        -> HIT ", hitID, endmsg );
                      decodedIDs.emplace_back( hitID );
                    } else {
                      daq_verbo( "         -> INVALID", endmsg );
                    }
                  }

                } // bit is on
              }   // loop over word bits

            } // word has any data

          } // loop over all NZS words

          // Finally skip the read NZS words
          iW += nNZSwords;
          dataW += nNZSwords;
        }
      }

    } // no data for this link, so just move on

    // move to next Tel40 link
    ++iLink;

  } // data word loop

  // Finally assert we reached the end of the bank
  assert( dataW == bankEnd );

  daq_debug( " -> Created ", decodedIDs.size(), " hits from Tel40 bank ", tel40ID, endmsg );
  if ( msgLevel( MSG::DEBUG ) ) {
    SmartIDSorter::sortByRegion( decodedIDs );
    for ( const auto id : decodedIDs ) { daq_debug( "  -> ", id, endmsg ); }
  }
}

//=============================================================================

void RawBankDecoder::decodeToSmartIDs_StreamIDs( const LHCb::RawBank&    bank,   //
                                                 const Detector::PDInfo& pdInfo, //
                                                 DecodedIDs&             decodedIDs ) const {

  // Get Tel40 Source ID
  const auto Tel40ID( bank.sourceID() );
  daq_debug( "FlatList Decoding Tel40 bank ", Tel40ID, endmsg );

  // Data bank size in 32 bit words
  const auto bankSize = bank.size() / 4;

  // reserve size in ID vector for number of hits (bank size here)
  decodedIDs.reserve( bankSize );

  // Loop over bank in 32 bit word chunks
  int lineC( 0 );
  while ( lineC < bankSize ) {

    // Read the smartID direct from the banks
    LHCb::RichSmartID id( LHCb::RichSmartID32( bank.data()[lineC++] ) );

#ifdef USE_DD4HEP
    // If built for DD4HEP apply correction to PMT module numbers to account
    // for different numbering scheme between DD4HEP and DetDesc.
    // We can do this because the encoding (Boole) when built for dd4hep
    // uses the real data format
    // https://gitlab.cern.ch/lhcb/Boole/-/merge_requests/382
    // so we know if we get the Streaming format with DD4HEP it has to be
    // using the DetDesc numbering scheme.
    const Rich::DetectorArray<Rich::PanelArray<LHCb::RichSmartID::DataType>> mod_corr{{{0, 0}, {6, 18}}};
    const auto pdMod      = id.pdMod() + mod_corr[id.rich()][id.panel()];
    const auto pdNumInMod = id.pdNumInMod();
    id.setPD( pdMod, pdNumInMod );
#endif

    // work around for some persistent data without 'large' PMT flag set.
    const bool isLarge = pdInfo.isLargePD( id );
    if ( id.isLargePMT() != isLarge ) {
      ++m_pmtSLFlagMismatch;
      // hack to fix up large PMT flag
      id.setLargePMT( isLarge );
    }

    // Is ID OK ?
    if ( !id.isValid() ) {
      ++m_flatListReadWarn;
    } else {
      // save to the ID list
      decodedIDs.emplace_back( id );
    }

  } // bank loop
}

//=============================================================================

void RawBankDecoder::fillDecodedData( const DecodedIDs& decodedIDs, //
                                      OutData&          decodedData ) const {

  // cache the last info
  PDInfo*                   last_pdInfo = nullptr;
  ModuleData*               last_mInfo  = nullptr;
  LHCb::RichSmartID         last_pdID;
  Rich::DAQ::PDModuleNumber last_mID;

  // static std::size_t maxNIDs{0};
  // if ( decodedIDs.size() > maxNIDs ) {
  //   maxNIDs = decodedIDs.size();
  //   info() << "#IDs = " << maxNIDs << endmsg;
  // }

  // loop over the IDs to save
  for ( const auto id : decodedIDs ) {

    // The RICH and panel
    const auto rich = id.rich();
    const auto side = id.panel();
    if ( !m_richIsActive[rich] || !m_sideIsActive[side] ) { continue; }

    daq_verbo( " -> ", id, endmsg );

    // The module number
    const Rich::DAQ::PDModuleNumber mID( id.pdCol() );

    // The PD ID
    const auto pdID = id.pdID();

    // Get the Module Data vector
    auto& mDataV = ( decodedData[rich] )[side];

    // The info objects to fill for this channel
    PDInfo*     pdInfo = nullptr;
    ModuleData* mInfo  = nullptr;

    // Has PD changed ?
    if ( pdID != last_pdID || !last_pdInfo || !last_mInfo ) {

      // has the module changed ?
      if ( mID != last_mID || !last_mInfo ) {
        // Find module data
        const auto mIt = std::find_if( mDataV.begin(), mDataV.end(), //
                                       [&mID]( const auto& i ) { return mID == i.moduleNumber(); } );
        // If get here, most likely a new module
        if ( mIt == mDataV.end() ) {
          // make a new entry
          mInfo = &mDataV.emplace_back( mID );
          daq_verbo( "   -> New Module ", mID, endmsg );
        } else {
          // use found entry
          mInfo = &( *mIt );
          daq_verbo( "   -> Found Module ", mID, endmsg );
        }
        // update cached info
        last_mID   = mID;
        last_mInfo = mInfo;
      } else {
        // use cached info
        mInfo = last_mInfo;
        daq_verbo( "   -> Cached Module ", mID, endmsg );
      }

      // Find PD data object
      const auto pdIt = std::find_if( mInfo->begin(), mInfo->end(), //
                                      [&pdID]( const auto& i ) { return pdID == i.pdID(); } );
      // If get here most likely new PD
      if ( pdIt == mInfo->end() ) {
        // make a new entry
        pdInfo = &mInfo->emplace_back( pdID );
        // Add to active PD count for current rich
        decodedData.addToActivePDs( rich );
        daq_verbo( "   -> New PD ", pdID, endmsg );
      } else {
        // Use found entry
        pdInfo = &( *pdIt );
        daq_verbo( "   -> Found PD ", pdID, endmsg );
      }
      // update the PD cache
      last_pdID   = pdID;
      last_pdInfo = pdInfo;
    } else {
      // use last PD cache
      pdInfo = last_pdInfo;
      daq_verbo( "   -> Cached PD ", pdID, endmsg );
    }

    // ID vector to fill
    auto& ids = pdInfo->smartIDs();

    // sanity assert to ensure this hit is not already present
    assert( std::find( ids.begin(), ids.end(), id ) == ids.end() );

    // add the hit to the list
    ids.emplace_back( id );

    // count the hits
    decodedData.addToTotalHits( rich );

  } // loop over IDs
}

//=============================================================================

void RawBankDecoder::dumpRawBank( const LHCb::RawBank& bank, MsgStream& os ) const {

  // Get bank version and ID
  const Rich::DAQ::SourceID sID( bank.sourceID() );
  const auto                version = bankVersion( bank );

  // Data bank size in 8-bit words
  const auto bankSize = bank.size();

  const std::string& LINES = "-------------------------------------------------------------------"
                             "-----------------";
  os << LINES << endmsg;
  os << "RawBank version=" << version << " SourceID=" << sID.data() << " datasize(bytes)=" //
     << bankSize << " magic=" << boost::format( "%04X" ) % bank.magic() << endmsg;
  os << LINES << endmsg;

  // Is this an empty bank ?
  if ( bankSize > 0 ) {
    using Dtype                     = std::uint8_t;
    const unsigned int cli_rowwidth = 6;
    std::size_t        iWord        = 0;
    for ( const Dtype* dataW = bank.begin<Dtype>(); dataW != bank.end<Dtype>(); ++dataW ) {
      // start new line
      if ( iWord % cli_rowwidth == 0 ) {
        if ( 0 != iWord ) { os << endmsg; }
        os << boost::format( "%04i | " ) % iWord;
      }
      // print the byte
      os << boost::format( "%02X" ) % (int)( *dataW );
      os << "(" << std::bitset<8>( *dataW ) << ") ";
      // increment count
      ++iWord;
    }
    os << endmsg;

  } else {
    os << "  -> Bank is empty" << endmsg;
  }

  os << LINES << endmsg;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RawBankDecoder )

//=============================================================================
