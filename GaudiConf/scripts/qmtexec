#!/usr/bin/env python
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import os
import re
import shlex
import shutil
import subprocess
import sys
from pathlib import Path
from tempfile import NamedTemporaryFile, mkdtemp

import yaml

from GaudiTesting.BaseTest import RationalizePath
from GaudiConf.QMTest.LHCbTest import LHCbTest


def parse_args():
    parser = argparse.ArgumentParser(
        description=
        "Run the QMTest test file directly without any validation of results")
    parser.add_argument("filename")
    parser.add_argument(
        "--prepare-only",
        action="store_true",
        help=
        "Prepare the test and print the command instead of running it directly",
    )
    parser.add_argument(
        "--chdir",
        "-C",
        action="store_true",
        help="Change to the same working directory as used by ctest",
    )
    return parser.parse_args()


def make_temp(suffix, prepare_only, content):
    tmpfile = NamedTemporaryFile(
        prefix="qmtexec-", suffix=suffix, mode="wt", delete=not prepare_only)
    tmpfile.file.write(content)
    tmpfile.seek(0)
    return tmpfile


def run_qmt_file(filename: str, prepare_only: bool, chdir: bool) -> list[str]:
    """Create an LHCbTest from filename and optionally run the command

    Args:
        filename: QMT file to parse
        prepare_only: if True, print the command instead of running it
    """
    lhcb_test = LHCbTest(filename)

    # Write options.yaml for lbexec if needed
    lbexec_options = lhcb_test.make_lbexec_options()
    if lbexec_options:
        lbexec_yaml = make_temp(".yaml", prepare_only,
                                yaml.safe_dump(lbexec_options))
        lhcb_test.args.insert(1, lbexec_yaml.name)

    # Write options for gaudirun.py if needed
    if lhcb_test.options:
        if re.search(
                r"from\s+Gaudi.Configuration\s+import\s+\*|from\s+Configurables\s+import",
                lhcb_test.options,
        ):
            option_file = make_temp(".py", prepare_only, lhcb_test.options)
        else:
            option_file = make_temp(".opts", prepare_only, lhcb_test.options)
        lhcb_test.args.append(option_file.name)

    # Try to guess the directory conventionally used by qmt
    default_qmt_dir = Path(filename).parent
    while default_qmt_dir.name != "qmtest":
        if default_qmt_dir == default_qmt_dir.parent:
            default_qmt_dir = None
            break
        default_qmt_dir = default_qmt_dir.parent

    work_dir = None
    if chdir:
        if lhcb_test.use_temp_dir:
            work_dir = mkdtemp(prefix="qmtexec-")
        else:
            work_dir = default_qmt_dir

    cwd = os.getcwd()
    if work_dir:
        os.chdir(default_qmt_dir)
    try:
        cmd = []
        cmd_path = shutil.which(lhcb_test.program)
        if not cmd_path and os.path.splitext(lhcb_test.program)[1] == ".py":
            cmd = ["python"]
        cmd += [RationalizePath(cmd_path or lhcb_test.program)]
        cmd += list(map(RationalizePath, lhcb_test.args))
    finally:
        os.chdir(cwd)

    if prepare_only:
        if work_dir:
            print(shlex.join(["cd", str(work_dir)]))
        print(shlex.join(cmd))
    else:
        proc = subprocess.run(cmd, check=False, bufsize=0, cwd=work_dir)
        sys.exit(proc.returncode)


if __name__ == "__main__":
    args = parse_args()
    run_qmt_file(args.filename, args.prepare_only, args.chdir)
