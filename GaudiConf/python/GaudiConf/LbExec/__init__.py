###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__all__ = ("InputProcessTypes", "DataTypeEnum", "HltSourceID", "FileFormats",
           "EventStores", "Options", "main")

import os
import sys

import click

from .configurables import config2opts, do_export
from .options import InputProcessTypes, HltSourceID, DataTypeEnum, FileFormats, EventStores, Options


def main(function,
         options,
         extra_args,
         *,
         dry_run=False,
         export=None,
         app_type="Gaudi::Application"):
    """Run an lbexec-style Gaudi job.

    Args:
        function (callable): A callable that will return the Gaudi configuration
        options (Options): An initialised APP.Options object
        extra_args (list of str): list of strings to add the the call to ``function``
        dry_run (bool): Only generate the configuration and don't actually start the job
        export (str): Filename to write the options out (or ``'-'`` to write to stdout as ``.opts``)
        app_type (str): The ``Gaudi.Application`` ``appType`` to run

    Returns:
        return_code (int): The Gaudi process's return code
    """

    config = function(options, *extra_args)
    opts = config2opts(config)

    if export:
        click.echo(
            click.style("INFO:", fg="green") +
            f" Writing configuration to {export}",
            err=True)
        export_data = do_export(os.path.splitext(export)[-1] or ".opts", opts)
        if export == "-":
            sys.stdout.buffer.write(export_data)
        else:
            with open(export, "wb") as fh:
                fh.write(export_data)

    if dry_run:
        click.echo(
            click.style("INFO:", fg="green") +
            " Not starting the application as this is a dry-run.",
            err=True)
        return 0

    # Ensure that any printout that has been made by the user provided function
    # has been flushed. Without this, non-interactive jobs such as tests end up
    # showing the print out in the middle of the Gaudi application log
    sys.stdout.flush()
    sys.stderr.flush()

    # Run the actual job
    import Gaudi
    opts["ApplicationMgr.JobOptionsType"] = '"NONE"'
    app = Gaudi.Application(opts, appType=app_type)
    return app.run()
