###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import glob
import logging
import math
import re
from contextlib import contextmanager
from enum import Enum
from itertools import product
from typing import Optional

# Workaround for https://gitlab.cern.ch/lhcb/LHCb/-/issues/292
import warnings

with warnings.catch_warnings():
    warnings.simplefilter("ignore", category=SyntaxWarning)
    from pydantic import BaseModel, root_validator, validator, constr

from Gaudi.Configuration import INFO

from DDDB.CheckDD4Hep import UseDD4Hep


class HltSourceID(str, Enum):
    """ possible values of the Hlt source ID used to identify the origin of 
         the data in the various rawbanks produced by Hlt and/or Sprucing
     """
    Hlt1 = "Hlt1"
    Hlt2 = "Hlt2"
    Spruce = "Spruce"


class InputProcessTypes(str, Enum):
    """
    `TurboPass`, `Hlt2` and `Spruce` are standard run-3 input process types.
    The extra types are available to effectively flag special input types
    and be able to apply a filter only to processes where a `DstData` bank is produced (the standard 3 types).
    See discussions at https://gitlab.cern.ch/lhcb/LHCb/-/merge_requests/3990 and related MR for further details.
    """
    Spruce = "Spruce"
    TurboPass = "TurboPass"
    Hlt2 = "Hlt2"  #This option should be removed when all data goes through the Sprucing
    Gen = "Gen"
    Brunel = "Brunel"  # This option should be removed once support for old MC files is fully removed.
    Boole = "Boole"
    Hlt1 = "Hlt1"

    def sourceID(self):
        if self.value == InputProcessTypes.Spruce: return HltSourceID.Spruce
        if self.value == InputProcessTypes.Hlt1: return HltSourceID.Hlt1
        if self.value == InputProcessTypes.Hlt2: return HltSourceID.Hlt2
        if self.value == InputProcessTypes.TurboPass: return HltSourceID.Hlt2
        # raise ValueError("no sourceID known for input process {}".format(self.name))

        # in the remaining caess, there is typically no DstData rawbank, but packed objects in a DST
        # and no decreports -- so this method should not even be called. But it is, and the result
        # is not actually used. Hence raising an exception isn't acceptable (yet!), and we just
        # return something which if ever really used will become an error...
        return 'InvalidNotToBeUsed'


class DataTypeEnum(Enum):
    Upgrade = 'Upgrade'


class FileFormats(str, Enum):
    NONE = "NONE"
    RAW = "RAW"
    ROOT = "ROOT"


class EventStores(str, Enum):
    EvtStoreSvc = "EvtStoreSvc"
    HiveWhiteBoard = "HiveWhiteBoard"


class CompressionAlgs(str, Enum):
    ZLIB = "ZLIB"
    LZMA = "LZMA"
    LZ4 = "LZ4"
    ZSTD = "ZSTD"


class CompressionSettings(BaseModel):
    algorithm: CompressionAlgs = CompressionAlgs.ZSTD
    level: int = 4
    min_buffer_size: Optional[int] = None
    max_buffer_size: Optional[int] = None
    approx_events_per_basket: Optional[int] = None

    def as_gaudi_config(self):
        config = {"GlobalCompression": f"{self.algorithm}:{self.level}"}
        if self.min_buffer_size is not None:
            config["MinBufferSize"] = self.min_buffer_size
        if self.max_buffer_size is not None:
            config["MaxBufferSize"] = self.max_buffer_size
        if self.approx_events_per_basket is not None:
            config["ApproxEventsPerBasket"] = self.approx_events_per_basket
        return config


class Options(BaseModel):
    """Conditions"""
    data_type: DataTypeEnum
    simulation: bool
    dddb_tag: Optional[str] = None
    conddb_tag: Optional[str] = None
    geometry_version: Optional[str] = None
    conditions_version: Optional[str] = None
    """Input"""
    input_files: list[str] = []
    input_type: FileFormats = FileFormats.NONE
    input_raw_format: float = 0.5
    input_process: Optional[InputProcessTypes] = None
    input_manifest_file: Optional[str] = None
    xml_file_catalog: Optional[str] = None
    evt_max: int = -1
    first_evt: int = 0
    # Use an alternative, faster IIOSvc implementation for MDFs.
    use_iosvc: bool = False
    """Output"""
    output_file: Optional[str] = None
    output_type: FileFormats = FileFormats.ROOT
    output_manifest_file: Optional[str] = None
    append_decoding_keys_to_output_manifest: bool = True
    write_decoding_keys_to_git: bool = True
    require_specific_decoding_keys: list[str] = []
    compression: Optional[CompressionSettings] = None
    histo_file: Optional[str] = None
    ntuple_file: Optional[str] = None
    xml_summary_file: Optional[str] = None
    """Processing"""
    n_threads: int = 1
    # defaults to 1.2 * n_threads
    n_event_slots: Optional[int] = None
    # Event store implementation: HiveWhiteBoard (default) or EvtStoreSvc (faster).
    event_store: EventStores = EventStores.HiveWhiteBoard
    # Number of events to pre-fetch if use_iosvc=True, the default value
    # is reasonable for most machines; it might need to be increased for
    # more modern/powerful machines
    buffer_events: int = 20_000
    # Estimated size of the per-event memory pool, zero disables the pool
    memory_pool_size: int = 10 * 1024 * 1024
    # If False, scheduler calls Algorithm::execute instead of
    # Algorithm::sysExecute which breaks some non-functional algorithms
    scheduler_legacy_mode: bool = True
    """Logging"""
    print_freq: int = 10_000
    output_level: int = INFO
    msg_svc_format: str = '% F%35W%S %7W%R%T %0W%M'
    msg_svc_time_format: str = '%Y-%m-%d %H:%M:%S UTC'
    python_logging_level: int = logging.INFO
    """Debugging"""
    # Dump monitoring entities (counters, histograms, etc.)
    monitoring_file: Optional[str] = None

    control_flow_file: Optional[str] = None
    data_flow_file: Optional[str] = None
    phoenix_filename: Optional[str] = None
    preamble_algs: list = []
    # Define list of auditors to run. Possible common choices include
    # "NameAuditor", "MemoryAuditor" or "ChronoAuditor".
    # For a full list see Gaudi documentation.
    auditors: list[str] = []
    event_timeout: Optional[int] = None
    if not UseDD4Hep:
        velo_motion_system_yaml: Optional[str] = None

    @validator("n_event_slots", pre=True, always=True)
    def n_event_slots_default(cls, n_event_slots, values):
        if n_event_slots:
            return n_event_slots
        n_threads = values.get("n_threads", 1)
        return math.ceil(1.2 * n_threads) if n_threads > 1 else 1

    @validator("input_files", pre=True)
    def glob_input_files(cls, input_files):
        if isinstance(input_files, str):
            resolved_input_files = []
            for pattern in _expand_braces(input_files):
                if "*" not in pattern:
                    resolved_input_files.append(pattern)
                    continue
                if pattern.startswith("root://"):
                    raise NotImplementedError("Cannot glob with XRootD URLs")
                matches = glob.glob(pattern, recursive=True)
                if not matches:
                    raise ValueError(
                        f"No input files found matching {pattern!r}")
                resolved_input_files += matches
            return resolved_input_files
        return input_files

    @validator("compression", pre=True)
    def parse_compression_str(cls, compression):
        if isinstance(compression, str):
            alg, level = compression.split(":", 1)
            alg = CompressionAlgs(alg)
            return CompressionSettings(algorithm=alg, level=int(level))
        return compression

    @root_validator()
    def validate_input(cls, values):
        input_type = values.get("input_type", FileFormats.NONE)
        if input_type == FileFormats.NONE:
            if values["use_iosvc"]:
                raise ValueError(
                    "If no input is given 'use_iosvc' must be False")
            if values["evt_max"] < 0:
                raise ValueError(
                    f"When running with input_type={input_type}, 'evt_max' must be >=0"
                )
        elif not values.get("input_files"):
            raise ValueError(
                f"'input_files' is required when input_type={input_type}")
        return values

    class Config:
        use_enum_values = True
        frozen = True
        extra = "forbid"

    @contextmanager
    def apply_binds(self):
        """Context manager to apply binds before the user function is called

        To avoid having to pass properties on the options object down many
        layers of functions applications can use this context manager to bind
        values before the user provided function is called.
        """
        yield

    def finalize(self):
        # HACK: Required for compatibility with the old options object
        pass


def _expand_braces(text):
    """Perform bash-like brace expansion

    See: https://www.gnu.org/software/bash/manual/html_node/Brace-Expansion.html

    There are two notable deviations from the bash behaviour:
     * Duplicates are removed from the output
     * The order of the returned results can differ
    """
    seen = set()
    # HACK: Use a reserved unicode page to substitute patterns like {abc} that
    # don't contain a comma and should therefore have the curly braces preserved
    # in the output
    substitutions = {"\uE000": ""}
    for s in _expand_braces_impl(text, seen, substitutions):
        for k, v in reversed(substitutions.items()):
            s = s.replace(k, v)
        if s:
            yield s


def _expand_braces_impl(text, seen, substitutions):
    int_range_pattern = r"[\-\+]?[0-9]+(\.[0-9]+)?(\.\.[\-\+]?[0-9]+(\.[0-9]+)?){1,2}"
    char_range_pattern = r"([a-z]\.\.[a-z]|[A-Z]\.\.[A-Z])(\.\.[\-\+]?[0-9]+)?"
    patterns = [
        ",",
        r"([^{}]|{})*,([^{}]|{})+",
        r"([^{}]|{})+,([^{}]|{})*",
        int_range_pattern,
        char_range_pattern,
        r"([^{},]|{})+",
    ]
    spans = [
        m.span() for m in re.finditer(rf"{{({'|'.join(patterns)})}}", text)
    ][::-1]
    if len(spans) == 0:
        if text not in seen:
            yield text
        seen.add(text)
        return

    alts = []
    for start, stop in spans:
        alt_full = text[start:stop]
        alt = alt_full[1:-1].split(",")
        is_int_range = re.fullmatch(rf"{{{int_range_pattern}}}", alt_full)
        is_char_range = re.fullmatch(rf"{{{char_range_pattern}}}", alt_full)
        if is_int_range or is_char_range:
            range_args = alt[0].split("..")
            leading_zeros = 0
            if any(
                    len(x) > 1 and x.strip("-")[0] == "0"
                    and x.strip("-") != "0" for x in range_args[:2]):
                leading_zeros = max(map(len, range_args[:2]))
            start, stop = map(int if is_int_range else ord, range_args[:2])
            step = int(range_args[2]) if len(range_args) == 3 else 0
            step = 1 if step == 0 else abs(int(step))
            if stop < start:
                step = -step
            stop = stop + int(step / abs(step))
            alt = [
                f"{s:0{leading_zeros}d}" if is_int_range else chr(s)
                for s in range(start, stop, step)
            ]
        elif len(alt) == 1:
            substitution = chr(0xE000 + len(substitutions))
            substitutions[substitution] = alt_full
            alt = [substitution]
        alts.append(alt)

    for combo in product(*alts):
        replaced = list(text)
        for (start, stop), replacement in zip(spans, combo):
            # Add dummy charactors to prevent brace expansion being applied recursively
            # i.e. "{{0..1}2}" should become "{02}" "{12}" not "02" "12"
            replaced[start:stop] = f"\uE000{replacement}\uE000"

        yield from _expand_braces_impl("".join(replaced), seen, substitutions)
