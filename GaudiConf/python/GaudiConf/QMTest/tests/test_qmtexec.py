###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import subprocess
from pathlib import Path

import pytest

THIS_DIR = Path(__file__).parent
WELCOME_MSG = "Welcome to {}".format(
    os.environ.get("GAUDIAPPNAME", "ApplicationMgr"))


def run(cmd, check=True):
    proc = subprocess.run(cmd, check=False, capture_output=True, text=True)
    if check:
        assert proc.returncode == 0, (proc.stdout, proc.stderr)
    return proc


def test_prepare_only():
    proc = run(["qmtexec", "--prepare-only", str(THIS_DIR / "basic.qmt")])
    assert proc.stdout.endswith("echo -n 'Hello world'\n")
    assert proc.stderr == ""


def test_basic():
    proc = run(["qmtexec", str(THIS_DIR / "basic.qmt")])
    assert proc.stdout == "Hello world"
    assert proc.stderr == ""


def test_failing():
    proc = run(["qmtexec", str(THIS_DIR / "failing.qmt")], check=False)
    assert proc.returncode == 93
    assert proc.stdout == ""
    assert proc.stderr == ""


def test_lbexec(monkeypatch, tmpdir):
    monkeypatch.chdir(THIS_DIR)
    yaml_path = Path(tmpdir) / "options.yaml"
    yaml_path.write_text("output_file: example-extra-yaml.dst")
    monkeypatch.setenv("OPTIONS_YAML", str(yaml_path))

    proc = run(["qmtexec", str(THIS_DIR / "lbexec.qmt")])
    assert WELCOME_MSG in proc.stdout


@pytest.mark.parametrize("qmt_fn", ["1", "2", "3", "4", "5"])
def test_lbexec_python_options(monkeypatch, qmt_fn):
    qmt_file_path = THIS_DIR / f"lbexec_python_options{qmt_fn}.qmt"
    # Some of the tests require the working directory to be changed
    if ">lbexec_example.py:" in qmt_file_path.read_text():
        monkeypatch.chdir(THIS_DIR)
    proc = run(["qmtexec", str(qmt_file_path)])
    assert WELCOME_MSG in proc.stdout


def test_lbexec_failing(monkeypatch, tmpdir):
    monkeypatch.chdir(THIS_DIR)
    yaml_path = Path(tmpdir) / "options.yaml"
    yaml_path.write_text("output_file: wrong_name.dst")
    monkeypatch.setenv("OPTIONS_YAML", str(yaml_path))

    proc = run(["qmtexec", str(THIS_DIR / "lbexec.qmt")], check=False)
    assert proc.returncode != 0
    assert "Unexpected output_file" in proc.stderr


def test_lbexec_testfiledb(monkeypatch):
    monkeypatch.chdir(THIS_DIR)
    run(["qmtexec", str(THIS_DIR / "lbexec_testfiledb.qmt")])


def test_gaudirun_options():
    """Ensure that the options property of qmtfiles is respected"""
    qmtfile = THIS_DIR.parent.parent.parent.parent.parent / "Rich/RichDetectors/tests/qmtest/test-decode-and-spacepoints-2022-data.qmt"
    proc = run(["qmtexec", str(qmtfile)])
    # Ensure that the expected options were ran
    assert "Run_0000248711_HLT20840_20221011-113809-426.mdf" in proc.stdout


def test_set_cwd():
    qmtfile = THIS_DIR.parent.parent.parent.parent.parent / "Hlt/HltServices/tests/qmtest/make_first_tck.qmt"
    run(["qmtexec", "-C", str(qmtfile)])
