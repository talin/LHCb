###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import pytest

from GaudiConf.QMTest import LHCbTest

expectedOutput = '''Missing algos with Counters : CaloAcceptanceBremAlg_Loog_aa8b48c9
  Counters for algo CaloAcceptanceBremAlg_Loog_aa8b48c9
    (CaloAcceptanceBremAlg_Loog_aa8b48c9 ref) #total tracks | 99 | 7036 | 71.071 | 44.729 | 2.0000 | 208.00
    (CaloAcceptanceBremAlg_Loog_aa8b48c9 ref) #tracks in acceptance | 99 | 4972 | 50.222 | 31.969 | 0.0000 | 144.00

Extra algos with Counters : CaloAcceptanceBremAlg_Long_8d87ce69
  Counters for algo CaloAcceptanceBremAlg_Long_8d87ce69
    (CaloAcceptanceBremAlg_Long_8d87ce69 new) #total tracks | 99 | 7036 | 71.071 | 44.729 | 2.0000 | 208.00
    (CaloAcceptanceBremAlg_Long_8d87ce69 new) #tracks in acceptance | 99 | 4972 | 50.222 | 31.969 | 0.0000 | 144.00

Renaming happend on set of algos named after Topo_ThreeBody_Combiner
Algo Topo_ThreeBody_Combiner_92a53505 has been renamed into Topo_ThreeBody_Combiner_1278399f
  Old set of algos and counters :
  Counters for algo Topo_ThreeBody_Combiner_f7e4cf6c
    (Topo_ThreeBody_Combiner_f7e4cf6c ref) # passed | 99 | 23 | ( 23.23232 +- 4.244416)%
    (Topo_ThreeBody_Combiner_f7e4cf6c ref) # passed CombinationCut | 2081 | 489 | ( 23.49832 +- 0.9294325)%
    (Topo_ThreeBody_Combiner_f7e4cf6c ref) # passed CompositeCut | 489 | 248 | ( 50.71575 +- 2.260846)%
    (Topo_ThreeBody_Combiner_f7e4cf6c ref) # passed vertex fit | 489 | 489 | ( 100.0000 +- 0.000000)%
    (Topo_ThreeBody_Combiner_f7e4cf6c ref) Input1 size | 99 | 140 | 1.4141
    (Topo_ThreeBody_Combiner_f7e4cf6c ref) Input2 size | 99 | 1652 | 16.687
  New set of algos and counters :
  Counters for algo Topo_ThreeBody_Combiner_e3c068ea
    (Topo_ThreeBody_Combiner_e3c068ea new) # passed | 99 | 23 | ( 23.23232 +- 4.244416)%
    (Topo_ThreeBody_Combiner_e3c068ea new) # passed CombinationCut | 2081 | 489 | ( 23.49832 +- 0.9294325)%
    (Topo_ThreeBody_Combiner_e3c068ea new) # passed CompositeCut | 489 | 248 | ( 50.71575 +- 2.260846)%
    (Topo_ThreeBody_Combiner_e3c068ea new) # passed vertex fit | 489 | 489 | ( 100.0000 +- 0.000000)%
    (Topo_ThreeBody_Combiner_e3c068ea new) Input1 size | 109 | 140 | 1.4141
    (Topo_ThreeBody_Combiner_e3c068ea new) Input2 size | 99 | 1652 | 16.687
Algo fromV2MuonPIDV1MuonPIDLong_75645bb5 has been renamed into fromV2MuonPIDV1MuonPIDLong_c829e24f
'''


def test_compare():
    with open(os.path.dirname(__file__) +
              "/test_counter_compare.ref") as refFile:
        refLog = refFile.read()
        refBlock = LHCbTest._extract_counter_blocks(
            refLog, LHCbTest._COUNTER_START_RE['Counters'])
    with open(os.path.dirname(__file__) +
              "/test_counter_compare.newref") as newFile:
        newLog = newFile.read()
        newBlock = LHCbTest._extract_counter_blocks(
            newLog, LHCbTest._COUNTER_START_RE['Counters'])
    causes = []
    result = {}
    LHCbTest._compare(refBlock, newBlock, causes, result, 'Counters', {})
    assert (causes == [
        'Missing Counters', 'Extra Counters',
        'Unsafe algorithm renaming in Counters'
    ])
    print(result)
    assert (result == {'CountersMismatch': expectedOutput})
