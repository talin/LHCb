###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import itertools
import logging
import os
import re
import shutil
from collections import OrderedDict
from itertools import takewhile
from pathlib import Path
from tempfile import NamedTemporaryFile

import yaml

import GaudiTesting.QMTTest
from GaudiTesting.BaseTest import FilePreprocessor
from GaudiTesting.BaseTest import h_count_re as HISTO_SUMMARY_RE

from .LHCbExclusions import counter_preprocessor


def _get_new_ref_filename(reference_path):
    """Return the next available filename for a new reference."""
    count = 0
    newrefname = reference_path + '.new'
    while os.path.exists(newrefname):
        count += 1
        newrefname = '{}.~{}~.new'.format(reference_path, count)
    return newrefname


LOG_LINE_RE = re.compile(
    r'^(?P<component>[^. ]+(?:\.[^. ]+)*(?:\.\.\.)?)\s*'
    r'(?P<level>SUCCESS|VERBOSE|  DEBUG|   INFO|  ERROR|  FATAL)'
    # Note: the space after the level might be stripped if \n follows
    r'\s*(?P<message>.*)$',
    flags=re.DOTALL)

CUT_ALGO_NAME_WITH_NB_RE = re.compile('^(\w*\.\.\.)\d*$')

# size in bytes after which job option dumps are not added in results,
# see https://gitlab.cern.ch/lhcb/LHCb/-/issues/187
MAX_JOBOPTSDUMP_SIZE = 1024**2


def getSensitivity(sensitivities, algoName, counterNameRef):
    # default in case nothing given for this algo and counter
    sensitivity = None
    # rework the algo name for cases where is has been cut and
    # extra numbers have been added at the end (collisions management)
    matchObj = re.match(CUT_ALGO_NAME_WITH_NB_RE, algoName)
    if matchObj:
        algoName = matchObj.group(1)
        # check whether we have a dedicated sensibility number
    if algoName in sensitivities and counterNameRef in sensitivities[algoName]:
        sensitivity = sensitivities[algoName][counterNameRef]
    return sensitivity


def _dropSuffix(name):
    """Removes potential hash suffix
       This should also work when suffix is truncated"""
    return re.sub('(_[1234567890abcdef\.]+)?$', '', name)


def _matches(name, prefixes):
    """checks whether an algo full name matches one of the given
       prefixes plus an hash"""
    for p in prefixes:
        if re.fullmatch(p + '(_[1234567890abcdef\.]*)?', name):
            return True
    return False


class GroupMessages(FilePreprocessor):
    """Preprocessor that groups multi-line messages.

    Using this preprocessor only makes sense if it is called with input
    split by lines. If a string is given to `FilePreprocessor.__call__`,
    then the groups will be merged, making this a noop.

    """

    def __processFile__(self, lines, sep=None):
        indices = [i for i, m in enumerate(map(LOG_LINE_RE.match, lines)) if m]
        if not indices:
            return []
        # everything until the first message is one message per line
        indices = list(range(indices[0])) + indices
        if sep is None:
            # try to automatically deduce how lines should be joined
            sep = '' if all(l.endswith('\n') for l in lines) else '\n'
        messages = [
            sep.join(lines[i1:i2])
            for i1, i2 in zip(indices, indices[1:] + [None])
        ]
        return messages


class BlockSkipper(FilePreprocessor):
    """Skip all lines between `start` and `end`.

    Skips blocks of lines identified by a starting line containing the
    substring `start` and an ending line containing the substring `end`.
    Both the starting and the ending line are excluded from the output.
    If `being` is `None`, lines starting from the beginning are skipped.
    If `end` is `None`, all lines until the end are skipped.

    """

    def __init__(self, start, end=None):
        self.start = start
        self.end = end

    def __processFile__(self, lines):
        skipping = self.start is None
        output_lines = []
        for line in lines:
            if self.start is not None and self.start in line:
                skipping = True
            if not skipping:
                output_lines.append(line)
            if self.end is not None and self.end in line:
                skipping = False
        return output_lines


def _extract_ttree_blocks(stdout):
    """Extract lines belonging to a TTree printout."""

    TTREE_START_RE = re.compile(r'^\*+$')
    TTREE_KEEP_RE = re.compile(r'^[^*].*')
    output_lines = []
    keep = False
    for line in stdout.splitlines(True):
        if TTREE_START_RE.match(line):
            output_lines.append(line)
            keep = True
        elif keep:
            output_lines.append(line)
            keep = TTREE_KEEP_RE.match(line)
    return ''.join(output_lines)


def _extract_histo_blocks(stdout):
    """Extract lines belonging to histo summary printout."""

    TABLE_START_RE = re.compile(r'.*SUCCESS\s+(1D|2D|3D|1D profile|2D profile)'
                                r' histograms in directory\s+"(\w*)".*')
    TABLE_KEEP_RE = re.compile(r'^({})|( \| .*)$')
    output_lines = []
    keep = False
    for line in stdout.splitlines(True):
        if HISTO_SUMMARY_RE.match(line):
            output_lines.append(line.rstrip() + '\n')  # strip trailing space
        elif TABLE_START_RE.match(line):
            output_lines.append(line)
            keep = True
        elif keep:
            output_lines.append(line)
            keep = TABLE_KEEP_RE.match(line)
    return ''.join(output_lines)


_COMPONENT_RE = r'^(?P<component>[^. ]+(?:\.[^. ]+)*(?:\.\.\.)?)\s*'

_COUNTER_START_RE = {
    'Counters':
    (_COMPONENT_RE +
     r'(?:SUCCESS|   INFO) Number of counters : (?P<nCounters>\d+)$'),
    '1DHistograms':
    (_COMPONENT_RE +
     r'SUCCESS 1D histograms in directory (?:[^. ]*) : (?P<nCounters>\d+)$'),
    '1DProfiles':
    (_COMPONENT_RE +
     r'SUCCESS 1D profile histograms in directory (?:[^. ]*) : (?P<nCounters>\d+)$'
     ),
}

# default values found in the ref for old counters
oldCountersDefaults = ["", "", "1.0000", "0.0000", "1.0000", "1.0000"]


def _extract_counter_blocks(s, header_pattern, preproc=None):
    """Extract all counter lines from a string containing log lines.

    In case a counter preprocessor is given, it is applied to the counter lines.

    """

    if preproc:
        s = preproc(s)
    lines = s.splitlines() if isinstance(s, str) else s

    blocks = OrderedDict()

    header_re = re.compile(header_pattern)

    # find counter block headers
    headers = [x for x in enumerate(map(header_re.match, lines)) if bool(x[1])]
    for i, m in headers:
        component = m.group('component')

        # take care of the case where several algos have same name
        # (problem being that names are cut when too long)
        if component in blocks:
            candidates = (component + str(i) for i in itertools.count(1))
            component = next(c for c in candidates if c not in blocks)

        blocks[component] = lines[i:(i + int(m.group('nCounters')) + 2)]
        # We might get less than nCounters as some lines might be filtered
        # out by the counter preprocessor. Only keep what is left:
        blocks[component] = blocks[component][:1] + list(
            takewhile(re.compile(r'^ \|[* ].*$').match, blocks[component][1:]))

    return blocks


def splitCounterLine(line):
    # split on '|' character, but respect quoted strings
    return re.findall(r'(?:[^|"]|"(?:\\.|[^"])*")+', line)


def _extract_counters(blocks, comp_type='Counters'):
    """
    Then the lines are parsed and a dictionary is returned where key is the algorithm name and value a list of counters.
    Each counter itself is a list with first element being the counter name and the others counter's values, all as string
    """
    counters = {}
    firstValueIndex = 1 if comp_type == 'Counters' else 2
    for component, lines in blocks.items():
        assert re.match(_COUNTER_START_RE[comp_type], lines[0])
        counters[component] = {}
        # exclude the first two lines which contain message and table header
        for line in lines[2:]:
            # note the horrible hack to handle the fact that /Event/
            # is sometimes omited at the beginning of paths on top
            # master and future branch behave differently on that,
            # so in order to keep a common reference, we have to
            # remove /Event/
            items = [
                v.strip()
                for v in splitCounterLine(line.strip().replace('/Event/', ''))
                if v.strip() != ""
            ]
            counters[component][items[0].strip('"*')] = items[firstValueIndex:]
    return counters


def extract_counters(s, counter_preproc=None, comp_type='Counters'):
    """Parse counter table in a string containing log lines."""
    blocks = _extract_counter_blocks(
        s, _COUNTER_START_RE[comp_type], preproc=counter_preproc)
    return _extract_counters(blocks, comp_type=comp_type)


def _printCounter(algoName, counterName, counters, tag):
    '''prints given counter of given algo to string'''
    return '    (%s %s) %s\n' % (algoName, tag, ' | '.join(
        [counterName] + counters[algoName][counterName]))


def _printCounterDiff(refAlgoName, newAlgoName, counterNameRef, refCounters,
                      counterNameStdout, newCounters):
    '''prints 2 counter lines for reference and new value'''
    return _printCounter(
        refAlgoName, counterNameRef, refCounters, 'ref') + _printCounter(
            newAlgoName, counterNameStdout, newCounters, 'new')


def _printAllCountersForAlgo(algoName, counters, tag, indent=''):
    '''Prints all counters of a given algo'''
    return '%sCounters for algo %s\n' % (indent, algoName) + "".join([
        _printCounter(algoName, counterName, counters, tag)
        for counterName in counters[algoName]
    ])


def _compareAlgoCounters(refAlgoName, newAlgoName, refCounters, newCounters,
                         comp_type, sensitivities):
    """Compare all counters of the 2 given algos and report on differences"""
    msg = ''
    onlyref, onlystdout, counterPairs = _compareCutSets(
        set(refCounters[refAlgoName]), set(newCounters[newAlgoName]))
    algoName = refAlgoName if refAlgoName == newAlgoName else "%s/%s" % (
        refAlgoName, newAlgoName)
    if onlyref or onlystdout:
        msg += 'Different set of ' + comp_type + ' for algo %s\n' % algoName
        msg += ('    Ref has %d ' + comp_type +
                ', found %d of them in stdout\n') % (len(
                    refCounters[refAlgoName]), len(newCounters[newAlgoName]))
        if onlyref:
            msg += '    ' + comp_type + ' in ref and not in stdout : %s\n' % str(
                sorted(list(onlyref)))
        if onlystdout:
            msg += '    ' + comp_type + ' in stdout and not in ref : %s\n' % str(
                sorted(list(onlystdout)))
    headerPrinted = False
    # sorting to make prinout stable across multiple runs
    for counterNameRef, counterNameStdout in sorted(counterPairs):
        # floating point comparison precision
        if not _compareCounterLine(
                refCounters[refAlgoName][counterNameRef],
                newCounters[newAlgoName][counterNameStdout], comp_type,
                getSensitivity(sensitivities, refAlgoName, counterNameRef)):
            if not headerPrinted:
                msg += 'Different content of ' + comp_type + ' for algo %s\n' % algoName
                headerPrinted = True
            msg += _printCounterDiff(refAlgoName, newAlgoName, counterNameRef,
                                     refCounters, counterNameStdout,
                                     newCounters)
    return msg


def _compareCutSets(refCounters, stdoutCounters):
    """
    Compares two sets of counter names that may be incomplete and thus cut before the end !
    outputs a tuple containing:
        - the set of counters in ref and not in stdout, may be empty
        - the set of counters in stdout and not in ref, may be empty
        - a list of matching counters with pairs containing the name as seen in ref and the name as seen in stdout
    """
    onlyref = set([])
    donestdout = set([])
    counterPairs = []
    # go through names in ref and try to find them in stdout
    for refName in refCounters:
        # try full name first
        if refName in stdoutCounters:
            counterPairs.append((refName, refName))
            donestdout.add(refName)
            continue
        # suppose one of the 2 names was cut
        found = False
        for stdoutName in stdoutCounters:
            if stdoutName.startswith(refName) or refName.startswith(
                    stdoutName):
                counterPairs.append((refName, stdoutName))
                donestdout.add(stdoutName)
                found = True
                break
        if found:
            continue
        else:
            onlyref.add(refName)
    return onlyref, stdoutCounters - donestdout, counterPairs


def _compareDifferentCounterSet(comp_type, causes, refCounters, newCounters,
                                sensitivities):
    '''Compares 2 blocks of counters (ref and new) when there are differences in the list
       of algos in each set. Report on the differences
       Tries to be clever by identifying algorithm renames due to change of hashes
    '''
    msg = ''
    refAlgoNames = set(refCounters)
    newAlgoNames = set(newCounters)
    commonAlgs = refAlgoNames.intersection(newAlgoNames)
    onlyRefAlgs = refAlgoNames - commonAlgs
    onlyNewAlgs = newAlgoNames - commonAlgs
    # try to find out if we are only facing an algorithm renaming
    # Let's remove potential hashes from the algo names, and see whether the 2 sets
    # are still different
    refAlgsNoSuf = set([_dropSuffix(name) for name in onlyRefAlgs])
    newAlgsNoSuf = set([_dropSuffix(name) for name in onlyNewAlgs])
    if refAlgsNoSuf != newAlgsNoSuf:
        # somes algos are different even when dropping suffixes, let's report
        if refAlgsNoSuf.difference(newAlgsNoSuf):
            # some algos are missing, let's list their names
            missingAlgs = [
                name for name in refAlgoNames
                if _matches(name, refAlgsNoSuf.difference(newAlgsNoSuf))
            ]
            msg += 'Missing algos with ' + comp_type + ' : ' + ', '.join(
                missingAlgs) + '\n'
            for alg in missingAlgs:
                msg += _printAllCountersForAlgo(alg, refCounters, 'ref', '  ')
            msg += '\n'
            causes.append("Missing " + comp_type)
        if newAlgsNoSuf.difference(refAlgsNoSuf):
            extraAlgs = [
                name for name in newAlgoNames
                if _matches(name, newAlgsNoSuf.difference(refAlgsNoSuf))
            ]
            msg += 'Extra algos with ' + comp_type + ' : ' + ', '.join(
                extraAlgs) + '\n'
            for alg in extraAlgs:
                msg += _printAllCountersForAlgo(alg, newCounters, 'new', '  ')
            msg += '\n'
            causes.append("Extra " + comp_type)
    # now report on algos which only have different hashes a priori
    # first group algos according to prefixes
    commonAlgsNoSuf = refAlgsNoSuf.intersection(newAlgsNoSuf)
    refAlgsByPrefix = {}
    for alg in onlyRefAlgs:
        algNoSuf = _dropSuffix(alg)
        if algNoSuf not in refAlgsByPrefix: refAlgsByPrefix[algNoSuf] = []
        refAlgsByPrefix[algNoSuf].append(alg)
    newAlgsByPrefix = {}
    for alg in onlyNewAlgs:
        algNoSuf = _dropSuffix(alg)
        if algNoSuf not in newAlgsByPrefix: newAlgsByPrefix[algNoSuf] = []
        newAlgsByPrefix[algNoSuf].append(alg)
    # now go through cases and check what we have
    algoSafeRenaming = False
    algoRenaming = False
    algoChanges = False
    for algNoSuf in commonAlgsNoSuf:
        # in case we have one of each, we suppose they match
        if len(refAlgsByPrefix[algNoSuf]) == 1 and len(
                newAlgsByPrefix[algNoSuf]) == 1:
            algoSafeRenaming = True
            refAlg = refAlgsByPrefix[algNoSuf][0]
            newAlg = newAlgsByPrefix[algNoSuf][0]
            diffMsg = _compareAlgoCounters(refAlg, newAlg, refCounters,
                                           newCounters, comp_type,
                                           sensitivities)
            if diffMsg:
                algoChanges = True
            msg += 'Algo %s has been renamed into %s\n' % (refAlg,
                                                           newAlg) + diffMsg
        else:
            msg += 'Renaming happend on set of algos named after %s\n' % algNoSuf

            def find_match(iterable, predicate):
                try:
                    return next(n for n in iterable if predicate(n))
                except StopIteration:
                    return None

            # try to find matching algo in references for each algo in output
            # algo matches if it has identical counters
            na = [(new, ref) for (new, ref) in [(
                alg,
                find_match(
                    refAlgsByPrefix[algNoSuf],
                    lambda ra: _compareAlgoCounters(ra, alg, refCounters, newCounters, comp_type, sensitivities) == ''
                )) for alg in newAlgsByPrefix[algNoSuf]] if ref]
            # print the list of matching algos, as renames
            for alg, ref in na:
                algoSafeRenaming = True
                msg += 'Algo %s has been renamed into %s\n' % (alg, ref)
            # Inform on ref algos with no match
            remainingRefAlgs = [
                alg for alg in refAlgsByPrefix[algNoSuf]
                if not find_match(na, lambda n: n[1] == alg)
            ]
            if remainingRefAlgs:
                algoRenaming = True
                msg += '  Old set of algos and counters :\n'
                for alg in remainingRefAlgs:
                    msg += _printAllCountersForAlgo(alg, refCounters, 'ref',
                                                    '  ')
            # Inform on new algos with no match
            remainingNewAlgs = [
                alg for alg in newAlgsByPrefix[algNoSuf]
                if not find_match(na, lambda n: n[0] == alg)
            ]
            if remainingNewAlgs:
                algoRenaming = True
                msg += '  New set of algos and counters :\n'
                for alg in remainingNewAlgs:
                    msg += _printAllCountersForAlgo(alg, newCounters, 'new',
                                                    '  ')
    if algoRenaming:
        causes.append("Unsafe algorithm renaming in " + comp_type)
    elif algoChanges:
        causes.append("Algo renaming with wrong " + comp_type)
    elif algoSafeRenaming:
        causes.append("Safe algorithms renaming in " + comp_type)
    return msg


def _compareCounterLine(ref, value, comp_type, sensitivity):
    '''Compares 2 lines of a counter/histogram and check whether numbers are "close enough"'''

    # rough check
    if (ref == value):
        return True

    # Handle counters
    if comp_type == "Counters":

        # special case for efficiency counters
        # these lines contain ')%' and look like :
        #  | 10 | 10 |(  100.000 +- 10.0000  )%|...
        if len(ref) >= 3 and ')%' in ref[2]:
            # check only number and sum, the other numbers are only a combination of these
            if ref[0] != value[0]: return False
            if ref[1] != value[1]: return False
            return True
        elif len(value) >= 3 and ')%' in value[2]:
            # we got efficiency counters when the ref has regular ones...
            return False
        else:
            # first check count, always present
            if ref[0] != value[0]:
                # no sensitivity by default
                if sensitivity:
                    # if sensibility given, apply it
                    if _floatDiffer(ref[0], value[0], sensitivity):
                        return False
                else:
                    # no sensitivity by default
                    return False
            # now check number of values present
            if len(ref) != len(value):
                # in case we are missing some compared to the ref, it may be due
                # to the transition to new counters and no update of the ref.
                # In that case, the missing parts must all have predefined values
                # in the ref, except the second one (sum) that should be equal to
                # first one (count)
                if len(ref) > len(value) and len(ref) == 6:
                    if len(value) == 1 and ref[1] != ref[0]:
                        return False
                    for n in range(max(2, len(value)), 6):
                        if ref[n] != oldCountersDefaults[n]:
                            return False
                else:
                    return False
            # and finally the rest of the values, with sensitivity
            for n in range(1, len(value)):
                if _floatDiffer(ref[n], value[n],
                                sensitivity if sensitivity else 0.0001):
                    return False

    # Handle 1D Histos and Profiles
    if comp_type == '1DHistograms' or comp_type == '1DProfiles':

        # First value is count so check without precision
        if ref[0] != value[0]:
            # no sensitivity by default
            if sensitivity:
                # if sensibility given, apply it
                if _floatDiffer(ref[0], value[0], sensitivity):
                    return False
            else:
                # no sensitivity by default
                return False
        # Check rest with given precision
        for n in range(1, len(value)):
            if _floatDiffer(ref[n], value[n],
                            sensitivity if sensitivity else 0.0001):
                return False

    # If get here return OK
    return True


def _floatDiffer(ref, val, sensitivity):
    '''Compares 2 floats according to sensitivity'''
    val = float(val)
    ref = float(ref)
    if ref == 0.0:
        return abs(val) > sensitivity
    else:
        return abs(ref - val) / max(abs(ref), abs(val)) > sensitivity


def _compare(blocks, ref_blocks, causes, result, comp_type, sensitivities):
    """
    Compares values of counters/histograms to the reference file
    blocks: a dict of {component: list of lines}
    causes: the usual QMTest causes
    result: the usual QMTest result
    """
    refCounters = _extract_counters(ref_blocks, comp_type=comp_type)
    newCounters = _extract_counters(blocks, comp_type=comp_type)
    # diff counters
    refAlgoNames = set(refCounters)
    newAlgoNames = set(newCounters)
    msg = ''
    if refAlgoNames != newAlgoNames:
        msg += _compareDifferentCounterSet(comp_type, causes, refCounters,
                                           newCounters, sensitivities)
    # sorting to make prinout stable across multiple runs
    algoWrong = False
    for algoName in sorted(refAlgoNames.intersection(newAlgoNames)):
        msgDiff = _compareAlgoCounters(algoName, algoName, refCounters,
                                       newCounters, comp_type, sensitivities)
        if msgDiff:
            algoWrong = True
            msg += msgDiff
    if algoWrong:
        causes.append("Wrong " + comp_type)
    if msg:
        if type(result) == dict:
            result[comp_type + "Mismatch"] = msg
        else:
            result[comp_type + "Mismatch"] = result.Quote(msg)
        # make sure we create newref file when there are only counters differences


class LHCbTest(GaudiTesting.QMTTest.QMTTest):
    '''Extension of the original QMTTest of Gaudi adding checking of the counters and histograms'''

    def __init__(self, *args, **kwargs):
        """Adds support for creating lbexec's yaml files for qmt tests"""
        self.prerequisites = ""
        self.test_file_db_options_yaml = ""
        self.options_yaml_fn = ""
        self.extra_options_yaml = ""
        super().__init__(*args, **kwargs)

    def XMLParser(self, path):
        super().XMLParser(path)

        import xml.etree.ElementTree as ET
        tree = ET.parse(path)
        for child in tree.getroot():
            if child.attrib["name"] == "prerequisites":
                value = child[0]  # the <set></set>
                self.prerequisites = [
                    el.find("text").text for el in value.findall("tuple")
                ]

    def job_opts_dump_fn(self):
        return self.name + '.joboptsdump'

    def make_lbexec_options(self):
        lbexec_options = {}
        if self.options_yaml_fn:
            options_yaml = Path(os.path.expandvars(
                self.options_yaml_fn)).read_text()
            lbexec_options.update(yaml.safe_load(options_yaml))
        if self.extra_options_yaml:
            lbexec_options.update(yaml.safe_load(self.extra_options_yaml))
        if self.test_file_db_options_yaml:
            from PRConfig.TestFileDB import test_file_db

            tfdb_entry = test_file_db[self.test_file_db_options_yaml]
            lbexec_options = tfdb_entry.make_lbexec_options(
                **lbexec_options).dict(exclude_unset=True)
        return lbexec_options

    def run(self):
        if self.environment is None:
            self.environment = {}

        # Run the test in its own directory if "per-test" is used.
        # Before every run the test directory is cleaned to avoid
        # surprising non-reproducible behaviour when testing locally.
        if self._common_tmpdir and self.use_temp_dir == "per-test":
            self._common_tmpdir = os.path.join(self._common_tmpdir, self.name)
            p = Path(self._common_tmpdir)
            try:
                p.mkdir()
            except FileExistsError:
                shutil.rmtree(p)
                p.mkdir()
            del p

        # always dump the gaudi job options
        self.environment['JOBOPTSDUMPFILE'] = self.job_opts_dump_fn()
        workdir = self.workdir
        if self.use_temp_dir:
            if self._common_tmpdir:
                workdir = self._common_tmpdir
        try:
            os.remove(os.path.join(workdir, self.job_opts_dump_fn()))
        except OSError:
            pass
        # always print GaudiException stack traces
        self.environment['ENABLE_BACKTRACE'] = "1"
        # propagate test name (e.g. to allow customising output filenames)
        self.environment['QMTTEST_NAME'] = self.name
        # allow accessing output of prerequisites without explicit dirname, eg
        #   os.path.join(os.getenv("PREREQUISITE_0", ""), "output.dst")
        for i, p in enumerate(self.prerequisites):
            self.environment[f"PREREQUISITE_{i}"] = "../" + p

        # If required, make an options.yaml file for lbexec
        lbexec_options = self.make_lbexec_options()
        if lbexec_options:
            lbexec_yaml = NamedTemporaryFile(suffix=".yaml", mode="wt")
            lbexec_options = yaml.safe_dump(lbexec_options)
            logging.getLogger("LHCbTest.run").debug(
                "Writing lbexec options to %s:\n%s",
                lbexec_yaml.name,
                lbexec_options,
            )
            lbexec_yaml.file.write(lbexec_options)
            lbexec_yaml.seek(0)
            self.args.insert(1, lbexec_yaml.name)

        # run the test and validation logic
        resultDict = super(LHCbTest, self).run()

        resultDict["lbexec options"] = lbexec_options

        # overwrite the reference file name field with the expanded platform-specific reference.
        if os.path.isfile(self.reference):
            lreference = self._expandReferenceFileName(self.reference)
            resultDict['Output Reference File'] = os.path.relpath(
                lreference, self.basedir)

        return resultDict

    def ValidateOutput(self, stdout, stderr, result):
        try:
            dump = self.job_opts_dump_fn()
            size = os.path.getsize(dump)
            if size < MAX_JOBOPTSDUMP_SIZE:
                with open(dump) as f:
                    result['job_opts_dump'] = result.Quote(f.read())
            else:
                result['job_opts_dump'] = result.Quote(
                    f"\n{os.path.abspath(dump)}\n\n" +
                    "content too big to store: " +
                    f"{size} > {MAX_JOBOPTSDUMP_SIZE}")
        except IOError:
            pass
        try:
            return super(LHCbTest, self).ValidateOutput(stdout, stderr, result)
        except:
            import traceback
            self.causes.append("Exception in validator")
            result["validator_exception"] = result.Quote(
                traceback.format_exc().rstrip())
            return result, self.causes

    def validateWithReference(
            self,
            stdout=None,
            stderr=None,
            result=None,
            causes=None,
            preproc=None,
            counter_preproc=counter_preprocessor,
            sensitivities={},
            exclude_platforms_re=r'^skylake_avx512.*|^x86_64_v4.*'):
        '''Overwrite of the base class method by adding extra checks for counters.
           sensitivities allows to overwrite the default sensitivity of the counter checking (0.0001).
           It is containing a dictionnary with Algorithm name as key and a dictionnary as value
           having counter name a key and sensitivity for that counter as value'''

        # Skip the stdout/counter validation for some platforms.
        # Compared to qmt's "unsupported_platforms", this is a "soft" disabling
        # since other parts of the validators are run (e.g. warning checks).
        platform = self.environment.get("BINARY_TAG", "")
        if re.match(exclude_platforms_re, platform):
            return

        if stdout is None:
            stdout = self.out
        if stderr is None:
            stderr = self.err
        if result is None:
            result = self.result
        if causes is None:
            causes = self.causes

        # call upper class method
        super(LHCbTest, self).validateWithReference(stdout, stderr, result,
                                                    causes, preproc)

        # get reference
        lreference = self._expandReferenceFileName(self.reference)
        if not lreference:
            # BaseTest.validateWithReference should deal with this case
            return
        if os.path.isfile(lreference):
            with open(lreference) as f:
                ref = f.read()
        else:
            # BaseTest.validateWithReference adds "missing ref" to causes so
            # just generate the new ref here.
            ref = ''

        # extract counters from stdout and reference
        blocks = {
            comp_type: _extract_counter_blocks(
                stdout, pattern, preproc=counter_preproc)
            for comp_type, pattern in _COUNTER_START_RE.items()
        }
        if ref:
            ref_blocks = {
                comp_type: _extract_counter_blocks(
                    ref, pattern, preproc=counter_preproc)
                for comp_type, pattern in _COUNTER_START_RE.items()
            }
            for comp_type in ['Counters', '1DHistograms', '1DProfiles']:
                _compare(blocks[comp_type], ref_blocks[comp_type], causes,
                         result, comp_type, sensitivities)

        # construct new reference
        try:
            from StringIO import StringIO  ## for Python 2
        except ImportError:
            from io import StringIO  ## for Python 3
        newref = StringIO()
        # sanitize newlines
        new = stdout.splitlines()
        # write the preprocessed stdout (excludes counter tables)
        if preproc:
            new = preproc(new)
            # check if preprocessor is idempotent
            if preproc(new) != new:
                causes.append('validateWithReference preprocessor is not '
                              'idempotent')
        for l in new:
            newref.write(l.rstrip() + '\n')
        # write counter tables
        for ct_blocks in blocks.values():
            for lines in ct_blocks.values():
                for line in lines:
                    newref.write(line + '\n')
        # write TTrees
        #newref.write(_extract_ttree_blocks(stdout))
        # write Histos
        #newref.write(_extract_histo_blocks(stdout))

        newref = newref.getvalue()
        if causes:
            try:
                # overwrite new ref generated by BaseTest.validateWithReference
                newrefname = os.path.join(self.basedir,
                                          result['New Output Reference File'])
            except KeyError:
                newrefname = _get_new_ref_filename(lreference)

            with open(newrefname, "w") as f:
                f.write(newref)

            result['New Output Reference File'] = os.path.relpath(
                newrefname, self.basedir)
