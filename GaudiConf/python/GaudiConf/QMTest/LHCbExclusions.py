###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiTesting.BaseTest import (normalizeExamples, normalizeEOL,
                                   RegexpReplacer, LineSkipper,
                                   SortGroupOfLines, BlockSkipper)
from DDDB.CheckDD4Hep import UseDD4Hep

gitCondDBFixes = (LineSkipper(
    [
        'CORAL Connection Retrial',
        'INFO Connected to database',
        'INFO Using repository',
        'INFO opening Git repository',
        'INFO using commit',
        'INFO Removing all tools created by ToolSvc',
        'SUCCESS Detector description database',
    ],
    [r'INFO Added successfully Conversion service.*XmlCnvSvc'],
) + RegexpReplacer(
    when='using checked out files',
    orig=r'/[^ ]*(/Det/DetCond)',
    repl=r'/...\1',
))

if UseDD4Hep:
    # take into account spurious differences in stdout when DetDesc is not used
    gitCondDBFixes += LineSkipper([
        'SUCCESS Detector description database',
        'Detector description not requested to be loaded',
        'Event times generated from 0 with steps of 0',
    ])

preprocessor = (
    normalizeExamples + normalizeEOL +  # workaround for gaudi/Gaudi#108
    RegexpReplacer(  # normalize full path to DBASE or PARAM
        orig=r'/[^ :]+/(DBASE|PARAM)/',
        repl=r'\1/',
    ) + RegexpReplacer(  # hide release directories
        when="cern.ch",
        orig=(r'/afs/cern.ch/lhcb/software/(DEV/nightlies|releases)/'
              r'|/cvmfs/lhcb.cern.ch/lib/lhcb'),
        repl=r'',
    ) + RegexpReplacer(  # hide data package versions
        orig=r'((DBASE|PARAM)/([^/]+/)?[^/]+)/v[0-9]+r[0-9]+(p[0-9]+)?/',
        repl=r'\1/vXrYpZ/',
    ) + RegexpReplacer(
        when="at 0x########L",
        orig=r'0x########L',
        repl=r'0x########',
    ) + RegexpReplacer(
        when="Connected to database",
        orig=r'ONLINE[_-][0-9 ]{1,6}(.*)"[^"]*/([0-9A-Z_]{1,8})"',
        repl=r'ONLINE_xxxxxx\1"\2"',
    ) + RegexpReplacer(
        when="Connected to database",
        orig=r'"[^"]*/([0-9A-Z_]{1,8})"',
        repl=r'"\1"',
    ) + RegexpReplacer(  # hide eoslhcb SE directories
        when="eoslhcb.cern.ch",
        orig=(r'/lhcb/swtest/lhcb/|/lhcb/freezer/lhcb/|/lhcb/data/lhcb/'
              r'|/lhcb/cern-swtest/lhcb/'),
        repl=r'/lhcb//lhcb/',
    ) + RegexpReplacer(  # adapt to disuse of SWTEST SE
        when="eoslhcb.cern.ch",
        orig=r'//eos/lhcb/grid/prod/lhcb/',
        repl=r'//eos/lhcb/',
    ) + LineSkipper(
        [
            "EventSelector        INFO Stream:EventSelector.DataStreamTool",
            "INFO Using TAG",
            "TimingAuditor.T",
            "RootDBase.open",
            "INFO Opened magnetic field file",
            "INFO Map scaled by factor",
            "INFO Event times generated from",
            "INFO Connecting to database",
            "INFO Disconnect from database after being idle",
            "INFO Memory has changed from",
            "INFO Memory change after pool release",
            "Memory for the event exceeds 3*sigma",
            "Mean 'delta-memory' exceeds 3*sigma",
            "INFO  'CnvServices':",
            "DEBUG Property ['Name': Value] =  'IsIOBound':False",
            "#properties =",
            "VERBOSE ServiceLocatorHelper::service: found service AlgExecStateSvc",
            "Run numbers generated from 0 every 0 events",
            "############ CONFIGURING RawEventFormatConf!! ###############",
            "INFO  resolving alias TCK/0x",
            "SUCCESS Number of counters : ",
            "INFO Number of counters : ",
            "OMP: Warning #96: Cannot form a team with",
            "OMP: Hint Consider unsetting",
            "INFO Handled \"DataFault\" incidents:",
            "INFO Using conditions location",
            "INFO Using conditions tag",
            "INFO Using detector list",
            "INFO Loading DD4hep Geometry",
            "Changing system of units to ROOT units",
            "Changing system of units to Geant4 units",
            "INFO Current GasParameters :",
            "MagneticFieldGridReader INFO  Opened magnetic field file:",
            # FIXME: Remove this when https://gitlab.cern.ch/lhcb/LHCb/-/issues/293 is fixed:
            "INFO ++ Already processed xml document",
        ],
        regexps=[
            r"HLTControlFlowMgr\s*INFO Memory pool:",
            r"HLTControlFlowMgr\s*INFO Timing table:",
            r"HLTControlFlowMgr\s*INFO .* End of Initialization",
            r"HLTControlFlowMgr\s*INFO Timing (started|stopped) at:",
            r"HLTControlFlowMgr\s*INFO .* Loop over .* Events Finished",
            r"HLTControlFlowMgr\s*INFO Average ticks per millisecond:",
            r"DEBUG Property \['Name': Value\] =  '(Timeline|(Extra|Data)(In|Out)puts)'",
            r"JobOptionsSvc\s*INFO Properties are dumped into",
            r"SUCCESS \dD histograms in directory",
            r"SUCCESS \dD profile histograms in directory",
            r"SequencerTimerTool.*INFO",
            r"TGeoManager.*INFO .* volume UID's in Detector Geometry",
            r".*Using globally Geant4 unit system.*",
        ],
    ) +
    # Functional framework related
    BlockSkipper(
        "ForwardSchedule...   INFO Data Dependencies for Algorithms:",
        "ForwardSchedule...   INFO No unmet INPUT data dependencies were found"
    ) + LineSkipper([
        "HiveSlimEventLo...   INFO",
        "ThreadPoolSvc        INFO no thread init tools attached",
        "AlgResourcePool      INFO",
        "ForwardSchedule...   INFO",
    ]) + RegexpReplacer(
        when="HLTControlFlowMgr    INFO", orig=r'\d+\.*\d*', repl=r'n') +
    RegexpReplacer(
        when="Histograms converted successfully according to request",
        orig=r'"HiveSlimEventLo..."',
        repl=r'"EventLoopMgr      "') +
    # DD4hep related, only there for backward compatibility with DetDesc in the logs
    # Should be dropped when DetDesc is dropped
    LineSkipper(
        [
            "INFO ++ Converted subdetector:", "INFO Created IOV Pool for:run",
            "Set Streamer to dd4hep::OpaqueDataBlock",
            "+++ Processing compact file",
            "*********** Created World volume with size",
            "Patching names of anonymous shapes....",
            "INFO +  Created/Accessed a total of ", "DependencyHandler"
        ],
        regexps=[
            r"INFO \+\+ Loaded \s*\d+ conditions from",
            r"Alignments\s*INFO \+\+ Add dependency:",
            r"Align\s*INFO Alignments:",
            r'VolumeBuilder.*Processing xml document ',
            r'^(Compact|Conditions)Loader',
            r"ConditionsPool",
            r'^DetectorData.*INFO Using repository',
            r'RichGeoTransAux.*Processing and loading Transforms from xml document',
        ],
    ) +
    # Hide specific ODIN type
    RegexpReplacer(
        when="StoreExplorerAlg",
        orig=r'LHCb::ODINImplementation::v7::OD',
        repl=r'LHCb::ODIN',
    ) +
    # Grouping
    SortGroupOfLines(
        r'.*SUCCESS (Exceptions/Errors/Warnings/Infos Statistics :| #WARNINGS   =| #ERRORS   =|List of booked \dD histograms in directory).*'
    ) +
    # TODO To be removed when GaudiAlgorithm is gone and thus this output line is no more present
    SortGroupOfLines(r'CompareTracksTr.*DEBUG Changing \S* to \S*') +
    RegexpReplacer(  # normalize any timing prints
        when="INFO",
        orig=r'\d+\.*\d*(\s+(ns|us|ms|seconds|s|minutes|min)(\s+|\.|$))',
        repl=r'n\1',
    ) +
    # For backward compatibility when dropping global checksum in PackedDataChecksum
    LineSkipper([
        r"Packed data checksum for '' = ",
    ]))

try:
    from DDDB.Configuration import GIT_CONDDBS
    if GIT_CONDDBS:
        preprocessor = preprocessor + gitCondDBFixes
except ImportError:
    pass

# Exclude counters with
counter_preprocessor = LineSkipper([
    ' | "Delta Memory/MB" ',
    ' | "Total Memory/MB" ',
])
