##############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Helpers for configuring an application to read Moore HLT2 output."""
from __future__ import absolute_import
import functools
import os, json
from PyConf.utils import load_file

from PyConf.application import (default_raw_event, default_raw_banks,
                                make_data_with_FetchDataFromFile,
                                ComponentConfig)

from PyConf.dataflow import dataflow_config
from PyConf.components import force_location, setup_component
from PyConf.packing import unpackers_map

from GaudiConf import IOExtension

__all__ = [
    "mc_unpackers", "unpackers", "decoder", "hlt_decisions", "unpack_rawevent",
    "as_configurables"
]


# These are the known types that we can persist
# needed for backwards compatibility with old style .tck.json files
def type_map():
    type_map = {
        "KeyedContainer<LHCb::Particle,Containers::KeyedObjectManager<Containers::hashmap> >":
        "Particles",
        "KeyedContainer<LHCb::ProtoParticle,Containers::KeyedObjectManager<Containers::hashmap> >":
        "ProtoParticles",
        "KeyedContainer<LHCb::Event::v1::Track,Containers::KeyedObjectManager<Containers::hashmap> >":
        "Tracks",
        "KeyedContainer<LHCb::RichPID,Containers::KeyedObjectManager<Containers::hashmap> >":
        "RichPIDs",
        "KeyedContainer<LHCb::MuonPID,Containers::KeyedObjectManager<Containers::hashmap> >":
        "MuonPIDs",
        "KeyedContainer<LHCb::RecVertex,Containers::KeyedObjectManager<Containers::hashmap> >":
        "PVs",
        "KeyedContainer<LHCb::Vertex,Containers::KeyedObjectManager<Containers::hashmap> >":
        "Vertices",
        "KeyedContainer<LHCb::TwoProngVertex,Containers::KeyedObjectManager<Containers::hashmap> >":
        "TwoProngVertices",
        "KeyedContainer<LHCb::CaloHypo,Containers::KeyedObjectManager<Containers::hashmap> >":
        "CaloHypos",
        "KeyedContainer<LHCb::CaloCluster,Containers::KeyedObjectManager<Containers::hashmap> >":
        "CaloClusters",
        "KeyedContainer<LHCb::CaloDigit,Containers::KeyedObjectManager<Containers::hashmap> >":
        "CaloDigits",
        "KeyedContainer<LHCb::CaloAdc,Containers::KeyedObjectManager<Containers::hashmap> >":
        "CaloAdcs",
        "KeyedContainer<LHCb::FlavourTag,Containers::KeyedObjectManager<Containers::hashmap> >":
        "FlavourTags",
        "KeyedContainer<LHCb::WeightsVector,Containers::KeyedObjectManager<Containers::hashmap> >":
        "WeightsVectors",
        "LHCb::Relation1D<LHCb::Particle,LHCb::VertexBase>":
        "P2VRelations",
        "LHCb::Relation1D<LHCb::Particle,LHCb::MCParticle>":
        "P2MCPRelations",
        "LHCb::Relation1D<LHCb::Particle,int>":
        "P2IntRelations",
        "LHCb::Relation1D<LHCb::Particle,LHCb::RelatedInfoMap>":
        "P2InfoRelations",
        "LHCb::RelationWeighted1D<LHCb::ProtoParticle,LHCb::MCParticle,double>":
        "PP2MCPRelations",
        "LHCb::RecSummary":
        "RecSummary",
    }

    return type_map


def load_manifest(fname):
    # needed for backwards compatibility with old style .tck.json files
    __lookup = {
        1006:
        "LHCb::RecSummary",
        1541:
        "KeyedContainer<LHCb::CaloCluster,Containers::KeyedObjectManager<Containers::hashmap> >",
        1542:
        "KeyedContainer<LHCb::CaloDigit,Containers::KeyedObjectManager<Containers::hashmap> >",
        1543:
        "KeyedContainer<LHCb::CaloAdc,Containers::KeyedObjectManager<Containers::hashmap> >",
        1550:
        "KeyedContainer<LHCb::Event::v1::Track,Containers::KeyedObjectManager<Containers::hashmap> >",
        1551:
        "KeyedContainer<LHCb::CaloHypo,Containers::KeyedObjectManager<Containers::hashmap> >",
        1552:
        "KeyedContainer<LHCb::ProtoParticle,Containers::KeyedObjectManager<Containers::hashmap> >",
        1553:
        "KeyedContainer<LHCb::RecVertex,Containers::KeyedObjectManager<Containers::hashmap> >",
        1554:
        "KeyedContainer<LHCb::TwoProngVertex,Containers::KeyedObjectManager<Containers::hashmap> >",
        1555:
        "KeyedContainer<LHCb::WeightsVector,Containers::KeyedObjectManager<Containers::hashmap> >",
        1560:
        "LHCb::Relation1D<LHCb::Particle,LHCb::VertexBase>",
        1561:
        "KeyedContainer<LHCb::RichPID,Containers::KeyedObjectManager<Containers::hashmap> >",
        1562:
        "LHCb::RelationWeighted1D<LHCb::ProtoParticle,LHCb::MCParticle,double>",
        1571:
        "KeyedContainer<LHCb::MuonPID,Containers::KeyedObjectManager<Containers::hashmap> >",
        1581:
        "KeyedContainer<LHCb::Particle,Containers::KeyedObjectManager<Containers::hashmap> >",
        1582:
        "KeyedContainer<LHCb::Vertex,Containers::KeyedObjectManager<Containers::hashmap> >",
        1583:
        "KeyedContainer<LHCb::FlavourTag,Containers::KeyedObjectManager<Containers::hashmap> >",
        1584:
        "LHCb::Relation1D<LHCb::Particle,LHCb::RelatedInfoMap>",
        1591:
        "LHCb::Relation1D<LHCb::Particle,int>",
        1660:
        "LHCb::Relation1D<LHCb::Particle,LHCb::MCParticle>",
    }

    d = json.loads(load_file(fname))
    # check for old-style .tck.json file (note: this actually has _nothing_ to do with the ANNSvc...)
    ann = d.get("HltANNSvc/HltANNSvc", None)
    if ann is not None:
        # this is an old style .tck.json file -- adapt it on the fly
        fix_prefix = lambda x, prefix: prefix + x.removeprefix(prefix + 'p') if x.startswith(prefix + 'p') else x
        manifest = {
            'PackedLocations':
            [(functools.reduce(fix_prefix, ('/Event/Spruce/', '/Event/HLT2/'),
                               loc), __lookup[t % 10000])
             for loc, t in ann['PackedObjectTypes'].items()]
        }
    else:
        manifest = d
    return manifest


def mc_unpackers(input_process='Hlt2', filtered_mc=True, configurables=True):
    """Return a list of unpackers for reading Monte Carlo truth objects

    This must run BEFORE unpackers!!!.

    Args:
        input_process (str): 'Turbo', 'Spruce' or 'Hlt2'.
        filtered_mc (bool): If True, assume Moore saved only a filtered
                            subset of the input MC objects.
        configurables (bool): set to False to use PyConf Algorithm.

    """
    assert input_process == "Hlt2" or input_process == "Turbo" or input_process == "Spruce", 'MC unpacker helper only accepts Turbo, Spruce or Hlt2 process'

    mc_prefix = '/Event/HLT2' if filtered_mc else "/Event"
    if input_process == "Spruce":
        mc_prefix = '/Event/Spruce/HLT2'

    from PyConf.application import make_data_with_FetchDataFromFile
    from PyConf.Algorithms import UnpackMCParticle, UnpackMCVertex
    unpack_mcp = UnpackMCParticle(
        InputName=make_data_with_FetchDataFromFile(
            os.path.join(mc_prefix, "pSim/MCParticles")),
        outputs={
            "OutputName": force_location(
                os.path.join(mc_prefix, "MC/Particles"))
        },
    )

    unpack_mcv = UnpackMCVertex(
        InputName=make_data_with_FetchDataFromFile(
            os.path.join(mc_prefix, "pSim/MCVertices")),
        outputs={
            "OutputName": force_location(
                os.path.join(mc_prefix, "MC/Vertices"))
        },
    )

    unpacker_algs = [unpack_mcp, unpack_mcv]

    if configurables:
        unpacker_algs = as_configurables(unpacker_algs)
    return unpacker_algs


def unpack_rawevent(bank_types=["DstData", "HltDecReports"],
                    input_process='Hlt2',
                    stream="default",
                    raw_event_format=0.5,
                    configurables=False):
    """Return RawBank:Views of banks in RawEvent.

    Args:
        bank_types (list of str): RawBanks to create RawBank:Views of.
        input_process (str): 'Turbo' or 'Spruce' or 'Hlt2' - serves to determine `RawEventLocation` only.
        stream (str): needed post-sprucing as RawEvent is then dependent on stream name
            - drives `RawEventLocation` only.
        raw_event_format (float):
        configurables (bool): set to False to use PyConf Algorithm.
    """
    assert input_process == "Spruce" or input_process == "Turbo" or input_process == "Hlt2", 'Unpacking helper only accepts Turbo, Spruce or Hlt2 processes'

    # todo: if needed, group input bank types by the raw event location the type is expected to be found in
    #       according to raw_event_format...
    bank = bank_location(input_process, stream, "DstData", raw_event_format)

    from PyConf.Algorithms import LHCb__UnpackRawEvent
    unpackrawevent = LHCb__UnpackRawEvent(
        BankTypes=sorted(bank_types),
        RawBankLocations=[
            force_location(f"DAQ/RawBanks/{rb}") for rb in sorted(bank_types)
        ],
        RawEventLocation=bank)

    if configurables:
        unpackrawevent = as_configurables(unpackrawevent)

    return unpackrawevent


def unpackers(locations,
              cfg,
              mappedBuffers,
              input_process='Hlt2',
              configurables=True):
    """Return a list of unpackers for reading reconstructed objects.

    This must run AFTER mc_unpackers if MC data!!!.

    Args:
        input_process (str): 'Turbo' or 'Spruce' or 'Hlt2'.
        cfg:  configuration needed for finding linker locations
        locations: list of packed object locations to unpack
        configurables (bool): set to False to use PyConf Algorithm.
    """
    assert input_process == "Spruce" or input_process == "Turbo" or input_process == "Hlt2", 'Unpacking helper only accepts Turbo, Spruce or Hlt2 processes'

    TES_ROOT = '/Event/HLT2'
    if input_process == "Spruce":
        TES_ROOT = '/Event/Spruce'

    outputs = make_dict(locations, TES_ROOT, cfg)

    unp_map = unpackers_map()
    unpacker_algs = []

    for t, unp in unp_map.items():
        if t in outputs.keys():
            for loc in outputs[t]:
                unpacker = unp(
                    InputName=force_location(
                        getattr(
                            mappedBuffers, 'location', mappedBuffers
                        )  # check for either location or fullKey().key()
                    ),  # BUG / FIXME: why is force_location needed here??? inputName is a data handle!!!
                    outputs={'OutputName': force_location(loc)})

                unpacker_algs += [unpacker]

    if configurables:
        unpacker_algs = as_configurables(unpacker_algs)

    return unpacker_algs


def bank_location(input_process="Hlt2",
                  stream="default",
                  bank="DstData",
                  raw_event_format=0.5):

    if input_process == "Spruce" or input_process == "Turbo":
        bank_location = make_data_with_FetchDataFromFile(stream)
    else:
        bank_location = default_raw_event([bank],
                                          raw_event_format=raw_event_format)

    return bank_location


def decoder(input_process='Hlt2',
            stream='default',
            raw_event_format=0.5,
            configurables=True):
    """Return a DstData raw bank decoder.

    Args:
        input_process (str): 'Turbo' or 'Spruce' or 'Hlt2' - serves to determine `RawEventLocation` only.
        stream (str): needed post-sprucing as RawEvent is then dependent on stream name
            - drives `RawEventLocation` only.
        raw_event_format (float):
        locations: list of packed object locations to unpack
        configurables (bool): set to False to use PyConf Algorithm.
    """
    assert input_process == "Spruce" or input_process == "Turbo" or input_process == "Hlt2", 'Unpacking helper only accepts Turbo, Spruce or Hlt2 processes'

    from PyConf.Algorithms import HltPackedBufferDecoder
    decoder = HltPackedBufferDecoder(
        SourceID=input_process.replace("Turbo", "Hlt2"),
        DecReports=force_location("DAQ/RawBanks/HltDecReports"),
        RawBanks=force_location("DAQ/RawBanks/DstData"),
    )

    if configurables:
        decoder = as_configurables(decoder)

    return decoder


def make_dict(locations, stream, manifest):
    """Return a dictionary of object type: location list
       This is needed to decide which unpacker to use for a given object
    Args:
        stream (str): TES location prefix
        cfg: configuration  needed for finding object types from json file
        locations: list of packed object locations to unpack
    """
    up_map = unpackers_map()
    dct = {k: [] for k in up_map.keys()}

    present = {loc: tpe for (loc, tpe) in manifest['PackedLocations']}

    for loc in locations:
        typ = present.get(loc)
        if typ:
            alias = type_map().get(typ)
            if alias: typ = alias
            if typ not in dct.keys():
                print("Warning: unknown type - ", present.get(loc),
                      " at location ", loc, " -- skipping for now")
            else:
                dct[typ] += [loc]
    return dct


def make_locations(manifest, stream):
    """Return a location list to unpack and decode
    Args:
        stream (str): TES location prefix
        manifest: dict from json file

    """
    locations_to_decode = []
    for (l, t) in manifest['PackedLocations']:
        # if l.startswith(stream + "/p") or l.startswith(stream + "p"):
        if l.startswith(stream) and not l.startswith(stream + "/MC"):
            locations_to_decode.append(force_location(l))
    return locations_to_decode


def hlt_decisions(input_process='Hlt2',
                  stream="default",
                  output_loc="/Event/Hlt/DecReports",
                  raw_event_format=0.5,
                  configurables=True,
                  source='Hlt2'):
    """Return a HltDecReportsDecoder instance for HLT1, HLT2, Spruce or Turbo decisions.

    Args:
        stream (str): needed post-sprucing as RawEvent is then dependent on stream name
            - drives `RawEventLocation` only.
        output_loc (str): TES location to put decoded DecReports.
        raw_event_format (float):
        configurables (bool): set to False to use PyConf Algorithm.
        source (str): 'Hlt1' or 'Hlt2' or 'Spruce'- indicates whether selection stage is HLT1, HLT2 or Spruce
    """
    assert input_process == "Spruce" or input_process == "Turbo" or input_process == "Hlt2", 'Unpacking helper only accepts Turbo, Spruce or Hlt2 processes'

    from PyConf.Algorithms import HltDecReportsDecoder

    decode_hlt = HltDecReportsDecoder(
        name=source + "DecReportsDecoder",
        SourceID=source,
        RawBanks=force_location("DAQ/RawBanks/HltDecReports"),
        outputs={'OutputHltDecReportsLocation': force_location(output_loc)},
    )

    if configurables:
        decode_hlt = as_configurables(decode_hlt)

    return decode_hlt


def do_unpacking(cfg,
                 stream="default",
                 locations=[],
                 input_process='Hlt2',
                 simulation=True,
                 configurables=True,
                 raw_event_format=0.5):
    """Return a list of algorithm that will do a full unpacking of data

    Args:
        input_process (str): 'Turbo' or 'Spruce' or 'Hlt2' - serves to determine `RawEventLocation` only.
        stream (str): needed post-sprucing as RawEvent is then dependent on stream name
            - drives `RawEventLocation` only.
        raw_event_format (float):
        cfg: configuration needed for finding linker locations and packed locations
        locations: list of packed object locations to unpack.
                   if none is given, all locations in PackedObjectLocation are taken
    """
    TES_ROOT = '/Event/HLT2'
    if input_process == "Spruce":
        TES_ROOT = '/Event/Spruce'

    if len(locations) == 0:
        # no locations are specified, so try to unpack everything in PackedObjectLocations
        locations = make_locations(cfg, TES_ROOT)

    assert len(
        locations
    ) > 0, 'Nothing to be decoded, check the locations or configuration file'

    algs = []
    unpack = unpack_rawevent(
        bank_types=["DstData", "HltDecReports"],
        input_process=input_process,
        stream=stream,
        raw_event_format=raw_event_format,
        configurables=configurables)

    algs += [unpack]
    # Get the hlt2 decisions
    algs += [
        hlt_decisions(
            input_process=input_process,
            stream=stream,
            configurables=configurables,
            raw_event_format=raw_event_format,
            source="Hlt2",
            output_loc="/Event/Hlt2/DecReports")
    ]

    # if sprucing, get sprucing decisions
    if input_process == "Spruce" or input_process == "Turbo":
        algs += [
            hlt_decisions(
                input_process=input_process,
                stream=stream,
                configurables=configurables,
                source="Spruce",
                output_loc="/Event/Spruce/DecReports")
        ]

    # decode raw banks to map of packed data buffers
    packed_decoder = decoder(
        stream=stream,
        raw_event_format=raw_event_format,
        input_process=input_process,
        configurables=configurables)

    algs += [packed_decoder]

    # get mc unpackers (shouldn't this be only for simulation?)
    mc_algs = []
    if simulation:
        mc_algs = mc_unpackers(
            input_process=input_process, configurables=configurables)
        algs += mc_algs

    # Finally unpack packed data buffers to objects
    algs += unpackers(
        locations,
        cfg,
        packed_decoder.OutputBuffers,
        input_process=input_process,
        configurables=configurables)
    return algs


def as_configurables(algs):
    """Convert a list of PyConf Algorithm objects to Configurables.

    The returned list is not guaranteed to be in an order which satisfies
    the data dependencies of the algorithms when run sequentially. Any
    tools the Algorithm objects reference will be ignored.

    This is required for the GaudiPython tests (PyConf uses HLT scheduler which has no support for GP),
    ie. we need Configurables not PyConf Algorithm objects
    """
    configuration = dataflow_config()
    if isinstance(algs, list):
        for alg in algs:
            configuration.update(alg.configuration())
        configurables, _ = configuration.apply()
        return configurables
    else:
        configuration.update(algs.configuration())
        configurables, _ = configuration.apply()

        for c in configurables:
            if c.name() == algs.name:
                return c
