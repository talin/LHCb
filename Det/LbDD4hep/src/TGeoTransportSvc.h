/*****************************************************************************\
* (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <DetDesc/IGeometryInfo.h>
#include <DetDesc/ITransportSvc.h>

#include <Core/Material.h>

#include <GaudiKernel/IDataManagerSvc.h>
#include <GaudiKernel/IDataProviderSvc.h>
#include <GaudiKernel/IDataStoreAgent.h>
#include <GaudiKernel/IRegistry.h>
#include <GaudiKernel/Kernel.h>
#include <GaudiKernel/Service.h>
#include <GaudiKernel/StatEntity.h>
#include <GaudiKernel/StatusCode.h>

#include <TGeoManager.h>
#include <TGeoMaterial.h>

#include <iostream>
#include <map>
#include <mutex>
#include <string>

class IDataProviderSvc;
class IMessageSvc;

#include "DetDesc/IDetectorElement.h"
class ISvcLocator;
class GaudiException;

/**
 * Temporary structure for the comparison of the material seen in TGeo and in
 * LHCb
 */
struct MatIntervalCmp {
  Double_t                 start{0};
  Double_t                 end{0};
  Double_t                 radlength{0};
  std::string              materialName;
  TGeoMaterial*            material{nullptr};
  std::vector<std::string> locations;

  Double_t                 LHCbstart{0};
  Double_t                 LHCbend{0};
  Double_t                 LHCbradlength{0};
  std::string              LHCbmaterialName;
  std::vector<std::string> LHCblocations;
};

class TGeoTransportSvc : public extends<Service, ITransportSvc> {

private:
  struct AccelCache {};
  mutable std::any m_accelCache{AccelCache{}};

  /* Method to look up materials from the material data service */
  const LHCb::Detector::Material findMaterial( std::string shortname ) const;

public:
  using base_class::base_class;
  // Create an instance of the accelerator cache
  std::any createCache() const override { return AccelCache{}; }

  StatusCode initialize() override;
  StatusCode finalize() override;

  /** Estimate the distance between 2 points in units
   *  of radiation length units
   *  This method is re-entrant and thus thread safe.
   *  @param point1 first  point
   *  @param point2 second point
   *  @param accelCache Accelerator cache
   *  @param threshold threshold value
   *  @param geometry the geometry to be used
   *  @param geometryGuess a guess for navigation
   */
  virtual double distanceInRadUnits( const Gaudi::XYZPoint& point1, const Gaudi::XYZPoint& point2, std::any& accelCache,
                                     IGeometryInfo const& geometry, double threshold = 0,
                                     IGeometryInfo* geometryGuess = nullptr ) const override;

  /** general method ( returns the "full history" of the volume boundary intersections
   *  with different material properties between 2 points )
   *  This method is re-entrant and thread safe.
   *  @see ILVolume
   *  @see IPVolume
   *  @see ISolid
   *  @see IGeometryInfo
   *  @see Material
   *  @param point   initial point on the line
   *  @param vector  direction vector of the line
   *  @param tickMin minimal value of line paramater
   *  @param tickMax maximal value of line parameter
   *  @param accelCache Accelerator cache
   *  @param threshold threshold value
   *  @param geometry the geometry to be used
   *  @param geometryGuess a guess for navigation
   *  @returns container of intersections
   */
  Intersections intersections( const Gaudi::XYZPoint& point, const Gaudi::XYZVector& vector, const double tickMin,
                               const double tickMax, std::any& accelCache, IGeometryInfo const& geometry,
                               double threshold = 0, IGeometryInfo* geometryGuess = nullptr ) const override;

private:
  void getTGeoIntersections( Gaudi::XYZPoint start, Gaudi::XYZPoint end, std::vector<MatIntervalCmp>& intersepts,
                             TGeoManager* m ) const;
};
