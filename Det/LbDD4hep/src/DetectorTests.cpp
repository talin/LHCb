/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <DetDesc/GenericConditionAccessorHolder.h>
#include <Detector/Calo/DeCalorimeter.h>
#include <Detector/FT/DeFT.h>
#include <Detector/Magnet/DeMagnet.h>
#include <Detector/Muon/DeMuon.h>
#include <Detector/Muon/Namespace.h>
#include <Detector/Rich1/DetElemAccess/DeRich1.h>
#include <Detector/Rich2/DetElemAccess/DeRich2.h>
#include <Detector/UT/DeUT.h>
#include <Detector/VP/DeVP.h>
#include <LHCbAlgs/Consumer.h>
#include <LbDD4hep/IDD4hepSvc.h>

#include <GaudiKernel/System.h>

#include <stdexcept>
#include <string>
#include <type_traits>

namespace LHCb::Det::LbDD4hep::Tests {

  namespace details {
    template <typename DET>
    struct TestDervCond {
      TestDervCond() = default;
      TestDervCond( const DET& ) {}
    };
    template <typename DET>
    std::string location( const std::string& name ) {
      if constexpr ( std::is_same_v<DET, LHCb::Detector::DeRich1> ) {
        return "/world/BeforeMagnetRegion/Rich1:DetElement-Info-IOV";
      } else if constexpr ( std::is_same_v<DET, LHCb::Detector::DeRich2> ) {
        return "/world/AfterMagnetRegion/Rich2:DetElement-Info-IOV";
      } else if constexpr ( std::is_same_v<DET, LHCb::Detector::DeMuon> ) {
        return LHCb::Detector::Muon::Location::Default;
      } else if constexpr ( std::is_same_v<DET, LHCb::Detector::DeVP> ) {
        return "/world/BeforeMagnetRegion/VP:DetElement-Info-IOV";
      } else if constexpr ( std::is_same_v<DET, LHCb::Detector::DeFT> ) {
        return "/world/AfterMagnetRegion/T/FT:DetElement-Info-IOV";
      } else if constexpr ( std::is_same_v<DET, LHCb::Detector::UT::DeUT> ) {
        return LHCb::Detector::UT::location();
      } else if constexpr ( std::is_same_v<DET, LHCb::Detector::DeMagnet> ) {
        return "/world/MagnetRegion/Magnet:DetElement-Info-IOV";
      } else if constexpr ( std::is_same_v<DET, LHCb::Detector::Calo::DeCalorimeter> ) {
        if ( name.find( "Hcal" ) != std::string::npos ) {
          return "/world/DownstreamRegion/Hcal:DetElement-Info-IOV";
        } else if ( name.find( "Ecal" ) != std::string::npos ) {
          return "/world/DownstreamRegion/Ecal:DetElement-Info-IOV";
        } else {
          throw std::runtime_error( "Unknown CALO detector '" + name + "'" );
          return "UNKNOWN";
        }
      }
    }
  } // namespace details

  template <typename DET>
  using base_type = Algorithm::Consumer<void( const details::TestDervCond<DET>& ),
                                        LHCb::DetDesc::usesConditions<details::TestDervCond<DET>>>;

  template <typename DET>
  using KeyValue = typename base_type<DET>::KeyValue;

  template <typename DET>
  class TestDerivedElem final : public base_type<DET> {
  public:
    TestDerivedElem( const std::string& name, ISvcLocator* pSvcLocator )
        : base_type<DET>( name, pSvcLocator, {KeyValue<DET>{"DervLoc", "/world:" + name + "-Dervived-Det-Cond"}} ) {}

    StatusCode initialize() override {
      return base_type<DET>::initialize().andThen( [&]() {
        const auto sc = this->setProperty( "OutputLevel", MSG::DEBUG );
        this->addConditionDerivation( {details::location<DET>( this->name() )},
                                      this->template inputLocation<details::TestDervCond<DET>>(), //
                                      [&]( const DET& d ) {
                                        this->debug() << "Loaded " << System::typeinfoName( typeid( DET ) ) << " at '"
                                                      << details::location<DET>( this->name() ) << "' " << endmsg;
                                        return details::TestDervCond<DET>{d};
                                      } );
        return sc;
      } );
    }

    void operator()( const details::TestDervCond<DET>& ) const override {}
  };

  DECLARE_COMPONENT_WITH_ID( TestDerivedElem<LHCb::Detector::DeRich1>, "TestLoadDD4HEPRich1" );
  DECLARE_COMPONENT_WITH_ID( TestDerivedElem<LHCb::Detector::DeRich2>, "TestLoadDD4HEPRich2" );
  DECLARE_COMPONENT_WITH_ID( TestDerivedElem<LHCb::Detector::DeMuon>, "TestLoadDD4HEPMuon" );
  DECLARE_COMPONENT_WITH_ID( TestDerivedElem<LHCb::Detector::DeVP>, "TestLoadDD4HEPVP" );
  DECLARE_COMPONENT_WITH_ID( TestDerivedElem<LHCb::Detector::DeFT>, "TestLoadDD4HEPFT" );
  DECLARE_COMPONENT_WITH_ID( TestDerivedElem<LHCb::Detector::UT::DeUT>, "TestLoadDD4HEPUT" );
  DECLARE_COMPONENT_WITH_ID( TestDerivedElem<LHCb::Detector::DeMagnet>, "TestLoadDD4HEPMagnet" );
  DECLARE_COMPONENT_WITH_ID( TestDerivedElem<LHCb::Detector::Calo::DeCalorimeter>, "TestLoadDD4HEPEcal" );
  DECLARE_COMPONENT_WITH_ID( TestDerivedElem<LHCb::Detector::Calo::DeCalorimeter>, "TestLoadDD4HEPHcal" );
} // namespace LHCb::Det::LbDD4hep::Tests
