/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <Core/ConditionsRepository.h>
#include <Core/DetectorDataService.h>
#include <Core/MagneticFieldExtension.h>
#include <DD4hep/ConditionDerived.h>
#include <DD4hep/Conditions.h>
#include <DD4hep/ConditionsPrinter.h>
#include <DD4hep/ConditionsProcessor.h>
#include <GaudiKernel/IMessageSvc.h>
#include <GaudiKernel/Kernel.h>
#include <GaudiKernel/Service.h>
#include <GaudiKernel/StatusCode.h>
#include <Kernel/ICondDBInfo.h>
#include <LbDD4hep/IDD4hepSvc.h>
#include <TError.h>
#include <TGeoManager.h>
#include <TString.h>
#include <TSystem.h>
#include <exception>
#include <filesystem>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <utility>
#include <vector>
#include <yaml-cpp/yaml.h>

namespace LHCb::Det::LbDD4hep {

  struct InvalidGeometryLocation : std::invalid_argument {
    using invalid_argument::invalid_argument;
  };
  struct InvalidGeometryVersion : std::invalid_argument {
    using invalid_argument::invalid_argument;
  };
  struct InvalidGeometryMain : std::invalid_argument {
    using invalid_argument::invalid_argument;
  };

  // Pointer to the Gaudi message service needed by handle_message to forward the messages.
  // handle_message is a normal function as we need to pass a pointer to it to ROOT and DD4hep
  static IMessageSvc* gaudiMessageSvc = nullptr;

  class DD4hepSvc : public extends<Service, IDD4hepSvc, ICondDBInfo>, Detector::DetectorDataService {

  private:
    Gaudi::Property<std::string> m_geometryLocation{this, "GeometryLocation", "${DETECTOR_PROJECT_ROOT}/compact",
                                                    "Location of the XML detector description"};
    Gaudi::Property<std::string> m_geometryVersion{this, "GeometryVersion", "",
                                                   "Version of the DD4hep geometry to load"};
    Gaudi::Property<std::string> m_geometryMain{this, "GeometryMain", "", "Compact file to load"};
    Gaudi::Property<std::string> m_conditionsLocation{this, "ConditionsLocation", "git:/lhcb-conditions-database",
                                                      "Location of the XML detector conditions"};
    Gaudi::Property<std::map<std::string, std::string>> m_conditionsOverride{
        this, "ConditionsOverride", {}, "Map of condition overiden and their value as YAML string"};
    Gaudi::Property<std::string>              m_conditionsVersion{this, "ConditionsVersion", "HEAD",
                                                     "Tag for the conditions to load"};
    Gaudi::Property<std::vector<std::string>> m_detectorList{
        this,
        "DetectorList",
        {"/world", "VP", "UT", "FT", "Magnet", "Rich1", "Rich2", "Ecal", "Hcal", "Muon"},
        "List of detectors"};

    Gaudi::Property<int>    m_verboseLevel{this, "VerboseLevel", 0, "ROOT TGeoManager verbose level"};
    Gaudi::Property<double> m_nominalMagnetCurrent{this, "NominalMagnetCurrent", LHCb::Magnet::NominalCurrent,
                                                   "Magnet nominal field current"};

    Gaudi::Property<bool> m_dumpConditions{this, "DumpConditions", false,
                                           "Dumps the conditions to the log after loading each slice"};

    Gaudi::Property<bool> m_useConditionsOverlay{this, "UseConditionsOverlay", false, "Enable conditions overlay"};
    Gaudi::Property<std::string> m_conditionsOverlayInitPath{
        this, "ConditionsOverlayInitPath", "",
        "Initialize conditoins overlay from the files in the specified directory (implies UseConditionsOverlay = "
        "True)"};
    Gaudi::Property<std::vector<std::string>> m_limitedIOVpaths{
        this,
        "LimitedIOVPaths",
        {},
        "List of paths to (online) conditions that must have IOV span limited to exactly one run"};

  public:
    DD4hepSvc( const std::string& name, ISvcLocator* pSvcLocator )
        : base_class( name, pSvcLocator ), DetectorDataService( dd4hep::Detector::getInstance(), {} ) {}

    StatusCode initialize() override;

    StatusCode finalize() override {
      DetectorDataService::finalize();
      gaudiMessageSvc = nullptr;
      return extends::finalize();
    }

    DD4HepSlicePtr get_slice( size_t iov ) override;
    void           drop_slice( size_t iov ) override { return Detector::DetectorDataService::drop_slice( iov ); }
    void           clear_slice_cache() override { return Detector::DetectorDataService::clear_slice_cache(); }
    bool add( LHCb::span<const std::string> inputs, const std::string& output, DD4HepDerivationFunc& func ) override;
    bool identical_derivation( dd4hep::cond::ConditionDependency* d1, dd4hep::cond::ConditionDependency* d2 );
    bool addShared( LHCb::span<const std::string> inputs, const std::string& output,
                    DD4HepDerivationFunc& func ) override;
    void update_alignment( const dd4hep::DetElement& de, const dd4hep::Delta& delta ) override {
      return DetectorDataService::update_alignment( de, LHCb::Detector::detail::toDD4hepUnits( delta ) );
    }
    void write_alignments( std::string_view path ) const override { DetectorDataService::dump_conditions( path ); }
    void load_alignments( std::string_view path ) override { DetectorDataService::load_conditions( path ); }

    void defaultTags( std::vector<LHCb::CondDBNameTagPair>& tags ) const override;
  };

  namespace {
    // make sure the the ROOT logging level is such that the creation of TGeoManager
    // does not log anything on stderr. The verbosity will then be set back to a
    // reasonnable value in DD4hepSvc constructor
    static auto _ = [] {
      TGeoManager::SetVerboseLevel( 0 );
      return 0;
    }();
    /**
     * Creates a dd4hep hash out of an LHCb condition path
     * The DD4hep has has 2 parts : top 32 bits are used to parition conditions
     * and lower 32bits are identifying the conditions themselves within a partition
     * Thus the condition paths in LHCb, when used with DD4hep have 2 parts
     * separated by a ':' and each part is hashed independently to form the
     * final hash
     */
    dd4hep::Condition::key_type makeKey( std::string const& s ) {
      auto colonPos = s.find_first_of( ':' );
      if ( colonPos != std::string::npos ) {
        auto hi  = dd4hep::ConditionKey::itemCode( s.substr( 0, colonPos ) );
        auto low = dd4hep::ConditionKey::itemCode( s.substr( colonPos + 1 ) );
        return ( ( (dd4hep::Condition::key_type)hi ) << 32 ) | ( low & 0xffffffff );
      }
      auto hi  = dd4hep::ConditionKey::itemCode( "/world" );
      auto low = dd4hep::ConditionKey::itemCode( s );
      return ( ( (dd4hep::Condition::key_type)hi ) << 32 ) | ( low & 0xffffffff );
    }
  } // namespace

  const std::map<int, int> ROOT_LEVEL_MAP{
      {kPrint, MSG::DEBUG}, {kInfo, MSG::INFO},      {kWarning, MSG::WARNING}, {kError, MSG::ERROR},
      {kBreak, MSG::FATAL}, {kSysError, MSG::FATAL}, {kFatal, MSG::FATAL},
  };

  int map_to_ROOT_level( int level ) {
    auto m = ROOT_LEVEL_MAP.find( level );
    if ( m != ROOT_LEVEL_MAP.end() ) {
      return m->second;
    } else {
      return MSG::WARNING;
    }
  }

  void handle_message_ROOT( int level, Bool_t /* abort */, const char* location, const char* msg ) {
    if ( gaudiMessageSvc ) {
      gaudiMessageSvc->reportMessage( location, map_to_ROOT_level( level ), msg );
    } else {
      std::cerr << "Unhandled: " << level << " : " << location << ":" << msg << std::endl;
    }
  }

  static size_t handle_message_DD4hep( void* /* par */, dd4hep::PrintLevel lvl, const char* src, const char* msg ) {
    if ( gaudiMessageSvc ) {
      if ( (int)lvl >= gaudiMessageSvc->outputLevel() || (int)lvl >= gaudiMessageSvc->outputLevel( src ) ) {
        gaudiMessageSvc->reportMessage( src, lvl, msg );
        return strlen( msg );
      }
    } else {
      std::cerr << "Unhandled: " << lvl << " : " << src << ":" << msg << std::endl;
    }
    return 0;
  }

  StatusCode DD4hepSvc::initialize() {
    /// initialise the base class
    return extends::initialize().andThen( [&]() {
      namespace fs = std::filesystem;

      /// set the list of detectorNames. Cannot be done in constructor as we use Properties
      m_detectorNames = m_detectorList.value();

      // Setting the verbosity level and redirecting the ROOT and DD4hep logs
      gaudiMessageSvc = msgSvc().get();
      TGeoManager::SetVerboseLevel( m_verboseLevel.value() );
      // Should we use the TGEoManager or get it from the description ?
      // m_description.manager().SetVerboseLevel( m_verboseLevel.value() );
      SetErrorHandler( handle_message_ROOT );
      dd4hep::setPrinter( (void*)msgSvc(), handle_message_DD4hep );
      // needed to allow source specific debug/verbose printout to be handled by handle_message_DD4hep
      dd4hep::setPrintLevel( dd4hep::VERBOSE );

      /// helper to convert a string to a path object expanding environment variables
      auto to_path = []( const std::string& path ) {
        // We use ROOT to expand the variables in the path as it has a convenient
        // function for that.
        TString tmp{path};
        gSystem->ExpandPathName( tmp );
        return fs::path{tmp.View()};
      };

      // sanity checks on arguments
      auto location = to_path( m_geometryLocation );
      if ( m_geometryLocation.empty() || !fs::is_directory( location ) ) {
        throw InvalidGeometryLocation( fmt::format( "'{}' is not a directory", std::string( location ) ) );
      }
      auto version = to_path( m_geometryVersion );
      if ( m_geometryVersion.empty() || !fs::is_directory( location / version ) ) {
        throw InvalidGeometryVersion( fmt::format( "'{}' is not a valid geometry version", std::string( version ) ) );
      }
      auto entry_point = to_path( m_geometryMain );
      if ( m_geometryMain.empty() || !fs::is_regular_file( location / version / entry_point ) ) {
        throw InvalidGeometryMain(
            fmt::format( "'{}' is not a valid geometry main file", std::string( entry_point ) ) );
      }

      /// Load the actual description from Detector
      std::string fname{location / version / entry_point};
      if ( this->msgLevel( MSG::INFO ) ) info() << "Loading DD4hep Geometry: " << fname << endmsg;
      char* argv[1] = {fname.data()};
      m_description.apply( "DD4hep_CompactLoader", 1, argv );

      // If we have the Magnet in the detector, we should configurte it with the location of the data package
      bool hasMagnet =
          ( std::find( m_detectorNames.begin(), m_detectorNames.end(), "Magnet" ) != m_detectorNames.end() );
      if ( hasMagnet ) {
        TString tfieldMapRoot = "${FIELDMAPROOT}/cdf";
        gSystem->ExpandPathName( tfieldMapRoot );
        if ( this->msgLevel( MSG::INFO ) ) info() << "Field map location: " << tfieldMapRoot.Data() << endmsg;
        LHCb::Magnet::setup_magnetic_field_extension( m_description, tfieldMapRoot.Data(),
                                                      m_nominalMagnetCurrent.value() );
      }

      // Now initializing the conditions
      if ( this->msgLevel( MSG::DEBUG ) ) debug() << "Initializing the ConditionsManager " << endmsg;
      TString conditionsLocation( m_conditionsLocation.value() );
      gSystem->ExpandPathName( conditionsLocation );
      if ( this->msgLevel( MSG::INFO ) ) info() << "Using conditions location: " << conditionsLocation.Data() << endmsg;
      if ( this->msgLevel( MSG::INFO ) ) info() << "Using conditions tag: " << m_conditionsVersion.value() << endmsg;
      if ( this->msgLevel( MSG::INFO ) ) info() << "Using detector list: " << m_detectorList.value() << endmsg;

      nlohmann::json config;
      config["repository"] = conditionsLocation.Data();
      config["tag"]        = m_conditionsVersion.value();
      if ( !m_conditionsOverlayInitPath.empty() ) {
        config["overlay"]["path"] = m_conditionsOverlayInitPath.value();
        m_useConditionsOverlay    = true; // this is not really needed, but it is for consistency
      } else {
        config["overlay"] = m_useConditionsOverlay.value();
      }
      config["overrides"] = m_conditionsOverride.value();
      if ( !m_limitedIOVpaths.empty() ) { config["online_conditions"] = m_limitedIOVpaths.value(); }
      DetectorDataService::initialize( config );
    } );
  }

  bool DD4hepSvc::add( LHCb::span<const std::string> inputs, const std::string& output, DD4HepDerivationFunc& func ) {
    // The condition path has 2 parts, separated by a ':'. This corresponds
    // to the 2 parts od the DD4hep hash. Pratically the first one allows to
    // find out the concerned DetElement while the second one is the path of
    // a condition inside that element

    // Change the logic when parsing the condition name: it should be of the form <detector name>:<condition name>
    // if no ":" is present we assume the detector is "/world" and use the string as condition name
    auto [deName, condKey] = LHCb::Detector::parseConditionIdentifier( output );
    auto de                = m_description.detector( std::string{deName} );
    if ( this->msgLevel( MSG::DEBUG ) )
      debug() << "Adding derivation for  " << output << " (" << std::hex << std::uppercase << makeKey( output ) << ")"
              << endmsg;
    auto dependency =
        std::make_unique<dd4hep::cond::ConditionDependency>( de, std::string{condKey}, std::move( func ) );
    for ( auto& input : inputs ) {
      dependency->dependencies.emplace_back( dd4hep::ConditionKey{makeKey( input )} );
      if ( this->msgLevel( MSG::DEBUG ) )
        debug() << output << " -> " << input << " (" << std::hex << std::uppercase << makeKey( input ) << ")" << endmsg;
    }
    auto r = m_all_conditions->addDependency( dependency.release() );
    return ( r.first != 0 );
  }

  bool DD4hepSvc::identical_derivation( dd4hep::cond::ConditionDependency* d1, dd4hep::cond::ConditionDependency* d2 ) {

    // Looking for the  the ID of the current function
    auto* uc1 = dynamic_cast<FPointerConditionUpdateCall*>( d1->callback.get() );
    if ( !uc1 ) {
      if ( this->msgLevel( MSG::DEBUG ) ) debug() << "d1 callback: Not a function pointer" << endmsg;
      return false;
    }
    auto* uc2 = dynamic_cast<FPointerConditionUpdateCall*>( d2->callback.get() );
    if ( !uc2 ) {
      if ( this->msgLevel( MSG::DEBUG ) ) debug() << "d2 callback: Not a function pointer" << endmsg;
      return false;
    }

    if ( uc1->id() != uc2->id() ) {
      if ( this->msgLevel( MSG::DEBUG ) ) debug() << "shared condition: mismatch in callback" << endmsg;
      return false;
    }

    if ( d1->dependencies != d2->dependencies ) {
      if ( this->msgLevel( MSG::DEBUG ) )
        debug() << "shared condition: mismatch in dependency list " << d1->dependencies << " " << d2->dependencies
                << endmsg;
      return false;
    }

    return true;
  }

  bool DD4hepSvc::addShared( LHCb::span<const std::string> inputs, const std::string& output,
                             DD4HepDerivationFunc& func ) {
    // The condition path has 2 parts, separated by a ':'. This corresponds
    // to the 2 parts od the DD4hep hash. Pratically the first one allows to
    // find out the concerned DetElement while the second one is the path of
    // a condition inside that element
    auto [deName, condKey] = LHCb::Detector::parseConditionIdentifier( output );
    auto de                = m_description.detector( std::string{deName} );

    if ( this->msgLevel( MSG::DEBUG ) )
      debug() << "Adding shared derivation for  " << deName << ":" << condKey << endmsg;

    // Preparing the dependency to add
    auto dependency = std::make_unique<dd4hep::cond::ConditionDependency>( de, std::string{condKey},
                                                                           IDD4hepSvc::DD4HepDerivationFunc{func} );
    for ( auto& input : inputs ) { dependency->dependencies.emplace_back( dd4hep::ConditionKey{makeKey( input )} ); }

    // Checking whether this has been defined already
    auto existing = m_all_conditions->derived().find( dependency->target );
    if ( existing != m_all_conditions->derived().end() ) {

      // Now comparing the callbacks
      if ( identical_derivation( dependency.get(), existing->second ) ) {
        if ( this->msgLevel( MSG::DEBUG ) ) debug() << "Shared derivations do match - No need to add again " << endmsg;
        return true;
      } else {
        error() << "Shared derivation: Adding  " << output << " as a different dependency" << endmsg;
        return false;
      }
    }

    // Now register the derivation properly
    auto r = m_all_conditions->addDependency( dependency.release() );
    return ( r.first != 0 );
  }

  IDD4hepSvc::DD4HepSlicePtr DD4hepSvc::get_slice( size_t iov ) {
    if ( this->msgLevel( MSG::DEBUG ) ) debug() << "Getting slice for IOV: " << iov << endmsg;
    IDD4hepSvc::DD4HepSlicePtr slice = Detector::DetectorDataService::get_slice( iov );

    if ( m_dumpConditions ) {

      for ( const std::string& path : m_detectorList ) {
        // Gathering the conditions in a vector
        info() << "= Detector: " << path << endmsg;
        dd4hep::DetElement             de = m_description.detector( path );
        std::vector<dd4hep::Condition> condition_vec;
        dd4hep::cond::conditionsCollector( *slice.get(), condition_vec )( de, 0 );
        for ( auto cond : condition_vec ) {
          std::stringstream cond_str;
          cond_str << "Condition " << std::hex << std::uppercase << cond.key() << " name:" << std::quoted( cond.name() )
                   << " bound:" << cond.is_bound() << " valid:" << cond.isValid() << '\n';
          auto o       = cond.access();
          auto ptr_iov = o->iovData();
          cond_str << ( ptr_iov ? "IOV: " + ptr_iov->str() : "IOV: <UNKNOWN>" ) << '\n';
          if ( cond.isValid() ) {
            auto ptr = cond.ptr();
            cond_str << "Cond type: " << dd4hep::typeName( typeid( *ptr ) ) << '\n';
            cond_str << "Data type: " << cond.data().dataType() << '\n';
            // Custom print for YAML nodes
            if ( cond.is_bound() ) {
              const std::type_info& type = cond.typeInfo();
              if ( type == typeid( YAML::Node ) ) {
                auto node = cond.get<YAML::Node>();
                cond_str << "YAML Content:\n---\n" << node << '\n';
              }
            }
          }
          info() << cond_str.str() << endmsg;
        }
      }
    }
    return slice;
  }

  void DD4hepSvc::defaultTags( std::vector<LHCb::CondDBNameTagPair>& tags ) const {
    tags.emplace_back( "GeometryLocation", m_geometryLocation );
    tags.emplace_back( "GeometryVersion", m_geometryVersion );
    tags.emplace_back( "GeometryMain", m_geometryMain );
    tags.emplace_back( "ConditionLocation", m_conditionsLocation );
    tags.emplace_back( "ConditionVersion", m_conditionsVersion );
  }
} // namespace LHCb::Det::LbDD4hep

DECLARE_COMPONENT( LHCb::Det::LbDD4hep::DD4hepSvc )
