###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import re
import Configurables

import pytest
from GaudiTests import run_gaudi
from subprocess import PIPE

# deduce the detectors list from the available configurables
AVAILABLE_DETECTORS = [
    name.replace("TestLoadDD4HEP", "") for name in Configurables.__all__
    if name.startswith("TestLoadDD4HEP")
]


def config():
    # FIXME: this should be an argument to the function, but it's not yet possible
    #        to invoke `gaudirun.py "module:function('some_value')"`
    detector_to_test = os.environ.get("DETECTOR_TO_TEST")
    if detector_to_test is None:
        exit(
            f"usage: `DETECTOR_TO_TEST=<detector> gaudirun.py .../test_load_det_elems.py:config`"
        )
    if detector_to_test not in AVAILABLE_DETECTORS:
        exit("please, set the environment variable DETECTOR_TO_TEST "
             f"to one of the following values: {AVAILABLE_DETECTORS}")

    from Configurables import (
        ApplicationMgr,
        AuditorSvc,
        DDDBConf,
        EventSelector,
        LHCbApp,
        FPEAuditor,
        MessageSvc,
        LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc,
        LHCb__Det__LbDD4hep__IOVProducer as IOVProducer,
        LHCb__Tests__FakeRunNumberProducer as FRNP,
    )

    # Event numbers
    nEvents = 1
    EventSelector().PrintFreq = 1

    # Just to initialise
    DDDBConf()
    LHCbApp()

    # Auditors
    AuditorSvc().Auditors += ["FPEAuditor"]
    FPEAuditor().ActivateAt = ["Execute"]

    # Only load the detector we want
    DD4hepSvc(DetectorList=["/world", detector_to_test])

    # Formatting the messages
    MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

    # Finally set up the application
    app = ApplicationMgr(
        EvtSel="NONE",
        EvtMax=nEvents,
        ExtSvc=["ToolSvc", "AuditorSvc"],
        AuditAlgorithms=True,
    )

    TestAlg = getattr(Configurables, f"TestLoadDD4HEP{detector_to_test}")

    odin_path = "/Event/DummyODIN"
    app.TopAlg = [
        FRNP("FakeRunNumber", ODIN=odin_path, Start=1000, Step=20),
        IOVProducer("ReserveIOVDD4hep", ODIN=odin_path),
        TestAlg(),
    ]


@pytest.mark.parametrize(
    "detector_to_test",
    AVAILABLE_DETECTORS,
)
def test(detector_to_test):
    """
    Test loading of DD4Hep detector elements.
    """
    env = os.environ.copy()
    env["DETECTOR_TO_TEST"] = detector_to_test
    proc = run_gaudi(
        f"{__file__}:config",
        check=True,
        env=env,
        stdout=PIPE,
        errors='replace')
    assert re.search(f"TestLoadDD4HEP{detector_to_test}.*DEBUG Loaded",
                     proc.stdout)
