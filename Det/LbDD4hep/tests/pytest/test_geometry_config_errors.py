###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import pytest
from subprocess import PIPE
import json

from GaudiTests import run_gaudi


def config():
    # FIXME: this should be an argument to the function, but it's not yet possible
    #        to invoke `gaudirun.py "module:function('some_value')"`
    opts = json.loads(os.environ["TEST_OPTS"])

    from GaudiConfig2 import Configurables as C

    dd4hepsvc = C.LHCb.Det.LbDD4hep.DD4hepSvc(**opts)
    app = C.ApplicationMgr()
    app.ExtSvc.append(dd4hepsvc)
    return [app, dd4hepsvc]


@pytest.mark.parametrize(
    "opts,error_msg",
    [
        ({
            "GeometryLocation": "/some/invalid/location"
        }, "'/some/invalid/location' is not a directory"),
        ({
            "GeometryLocation": "${DETECTOR_PROJECT_ROOT}/compact",
            "GeometryVersion": "dummy-version"
        }, "'dummy-version' is not a valid geometry version"),
        ({
            "GeometryLocation": "${DETECTOR_PROJECT_ROOT}/compact",
            "GeometryVersion": "trunk",
            "GeometryMain": "invalid.xml"
        }, "'invalid.xml' is not a valid geometry main file"),
        ({
            "GeometryLocation": ""
        }, "'' is not a directory"),
        ({
            "GeometryVersion": ""
        }, "'' is not a valid geometry version"),
        ({
            "GeometryVersion": "trunk",
            "GeometryMain": ""
        }, "'' is not a valid geometry main file"),
        ({
            "GeometryLocation":
            "${DETECTOR_PROJECT_ROOT}/compact/trunk/LHCb.xml"
        }, "/compact/trunk/LHCb.xml' is not a directory"),
        ({
            "GeometryVersion": "trunk/LHCb.xml"
        }, "'trunk/LHCb.xml' is not a valid geometry version"),
        ({
            "GeometryVersion": "trunk",
            "GeometryMain": "debug"
        }, "'debug' is not a valid geometry main file"),
    ],
    ids=[
        "location", "version", "main", "empty location", "empty version",
        "empty main", "non-dir location", "non-dir version", "non-file main"
    ],
)
def test_invalid_geometry(opts, error_msg):
    """
    Test different error conditions when initializing DD4hepSvc.
    """
    env = os.environ.copy()
    env["TEST_OPTS"] = json.dumps(opts)
    job = run_gaudi(
        f"{__file__}:config",
        check=False,
        env=env,
        stdout=PIPE,
        stderr=PIPE,
        errors='replace',
    )

    assert job.returncode != 0
    assert error_msg in job.stdout


@pytest.mark.parametrize(
    "opts,msg",
    [
        ({
            "GeometryLocation": "${DETECTOR_PROJECT_ROOT}/compact",
            "GeometryVersion": "trunk",
            "GeometryMain": "LHCb.xml"
        }, "/compact/trunk/LHCb.xml"),
        ({
            "GeometryVersion": "trunk",
            "GeometryMain": "LHCb.xml"
        }, "/compact/trunk/LHCb.xml"),
    ],
    ids=["fully specified", "default location"],
)
def test_valid_geometry(opts, msg):
    """
    Test different error conditions when initializing DD4hepSvc.
    """
    env = os.environ.copy()
    env["TEST_OPTS"] = json.dumps(opts)
    job = run_gaudi(
        f"{__file__}:config",
        check=False,
        env=env,
        stdout=PIPE,
        stderr=PIPE,
        errors='replace',
    )

    assert job.returncode == 0
    assert msg in job.stdout
