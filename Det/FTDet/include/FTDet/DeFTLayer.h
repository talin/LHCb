/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#ifdef USE_DD4HEP
#  include "Detector/FT/DeFTLayer.h"
using DeFTLayer = LHCb::Detector::DeFTLayer;
#else
#  include "DetDesc/DetectorElement.h"
#  include "Detector/FT/FTChannelID.h"
#  include "FTDet/DeFTQuarter.h"
#  include "GaudiKernel/Plane3DTypes.h"

static const CLID CLID_DeFTLayer = 8604;

class DeFTLayer final : public DetDesc::DetectorElementPlus {

public:
  /// Standard constructor
  using DetectorElementPlus::DetectorElementPlus;

  /// Initialization method
  StatusCode initialize() override;

  /** Retrieves reference to class identifier
   *  @return The class identifier for this class
   */
  const CLID& clID() const override;

  /** Another reference to class identifier
   *  @return The class identifier for this class
   */
  static const CLID& classID() { return CLID_DeFTLayer; }

  /** @return layer ID */
  [[nodiscard]] unsigned int layerID() const { return m_layerID; }

  /** Returns the global z position of the layer */
  [[nodiscard]] float globalZ() const { return m_globalZ; }

  /** Returns the xy-plane at z-middle the layer */
  [[nodiscard]] const Gaudi::Plane3D& plane() const { return m_plane; }

  /** Returns the stereo angle of the layer */
  [[nodiscard]] float stereoAngle() const { return m_stereoAngle; }

  /** Returns the dx/dy of the layer (ie. tan(m_stereoAngle)) */
  [[nodiscard]] float dxdy() const { return m_dxdy; }

  /** Returns the dz/dy of the layer (ie. tan of the beam angle) */
  [[nodiscard]] float dzdy() const { return m_dzdy; }

  /** Returns the size of the layer in x */
  [[nodiscard]] float sizeX() const { return m_sizeX; }

  /** Returns the size of the layer in y */
  [[nodiscard]] float sizeY() const { return m_sizeY; }

  /** Const method to return the quarter for a given XYZ point
   * @param  aPoint the given point
   * @return const pointer to detector element
   */
  [[nodiscard]] const DeFTQuarter* findQuarter( const Gaudi::XYZPoint& aPoint ) const;

  /** Find the FT Module where a global point is
   *  @return Pointer to the relevant Module
   */
  [[nodiscard]] const DeFTModule* findModule( const Gaudi::XYZPoint& aPoint ) const;

  /** Const method to return the quarter for a given channel id
   * @param  aChannel  an FT channel id
   * @return pointer to detector element
   */
  [[nodiscard]] const DeFTQuarter* findQuarter( const LHCb::Detector::FTChannelID& aChannel ) const {
    return m_quarters[to_unsigned( aChannel.quarter() )];
  }

  /** Find the FT Module corresponding to the channel id
   *  @return Pointer to the relevant module
   */
  [[nodiscard]] const DeFTModule* findModule( const LHCb::Detector::FTChannelID& aChannel ) const {
    const DeFTQuarter* q = findQuarter( aChannel );
    return q ? q->findModule( aChannel ) : nullptr;
  }

  /** flat vector of quarters
   * @return vector of quarters
   */
  [[nodiscard]] const auto& quarters() const { return m_quarters; }

private:
  /// vector of quarters
  std::array<const DeFTQuarter*, 4> m_quarters{{nullptr, nullptr, nullptr, nullptr}};

  ///< flat vector of modules
  boost::container::static_vector<const DeFTModule*, 24> m_modules;

  unsigned int   m_layerID;     ///< layer ID number
  float          m_globalZ;     ///< Global z position of layer closest to y-axis
  Gaudi::Plane3D m_plane;       ///< xy-plane in the z-middle of the layer
  float          m_dzdy;        ///< dz/dy of the layer (tan of the beam angle)
  float          m_stereoAngle; ///< stereo angle of the layer
  float          m_dxdy;        ///< dx/dy of the layer (ie. tan(m_stereoAngle))
  float          m_sizeX;       ///< Size of the layer in x
  float          m_sizeY;       ///< Size of the layer in y
};
#endif
