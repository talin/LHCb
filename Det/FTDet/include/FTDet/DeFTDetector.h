/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <string>

#include "DeFTLayer.h"
#include "DeFTMat.h"
#include "DeFTModule.h"
#include "DeFTQuarter.h"
#include "DeFTStation.h"

#ifdef USE_DD4HEP
namespace DeFTDetectorLocation {
  // FT location
  inline const std::string Default = "/world/AfterMagnetRegion/T/FT:DetElement-Info-IOV";
} // namespace DeFTDetectorLocation
#  include "Detector/FT/DeFT.h"
using DeFT = LHCb::Detector::DeFT;
#else
namespace DeFTDetectorLocation {
  // FT location defined in the XmlDDDB
  inline const std::string Default = "/dd/Structure/LHCb/AfterMagnetRegion/T/FT";
} // namespace DeFTDetectorLocation
#  include "DetDesc/DetectorElement.h"
#  include "Detector/FT/FTChannelID.h"

/** @class DeFTDetector DeFTDetector.h "FTDet/DeFTDetector.h"
 *
 *  This is the top-level detector element class of the Fibre Tracker (FT).
 *  It has member variables holding pointers to the detector sub-structure
 *  det. elements (Stations, Layers, Quarters, Modules). Provides methods for
 *  the correspondence geometrical point <--> FTChannelID.
 *
 *  An T station (e.g. T1) contains 4 layers (x,u,v,x). The layers
 *  contain 4 quarters each. The quarters have 5 or 6 modules each.
 *  The modules are split in an upper and
 *  lower side.
 *
 *  The numbering scheme for the FT modules in the digitization is:
 *
 *  @verbatim
                                                        ^ Y
          Quarter 3                      Quarter 2      |
       ______________________   ______________________  |
       |  |  |  |  |  |  |  |   |  |  |  |  |  |  |  |  |
       |  |  |  |  |  |  |  |   |  |  |  |  |  |  |  |  |
       |  |  |  |  |  |  |  |   |  |  |  |  |  |  |  |  |
       | 6| 5| 4| 3| 2| 1| 0|   | 0| 1| 2| 3| 4| 5| 6|  |
       |  |  |  |  |  |  |  |   |  |  |  |  |  |  |  |  |
       |  |  |  |  |  |  |  |   |  |  |  |  |  |  |  |  |
       |  |  |  |  |  |  |  |   |  |  |  |  |  |  |  |  |
       |  |  |  |  |  |  |__|   |__|  |  |  |  |  |  |  |
       |--|--|--|--|--|--|__     __|--|--|--|--|--|--|  |
       |  |  |  |  |  |  |  |   |  |  |  |  |  |  |  |  |
       |  |  |  |  |  |  |  |   |  |  |  |  |  |  |  |  |
       |  |  |  |  |  |  |  |   |  |  |  |  |  |  |  |  |
       |  |  |  |  |  |  |  |   |  |  |  |  |  |  |  |  |
       | 6| 5| 4| 3| 2| 1| 0|   | 0| 1| 2| 3| 4| 5| 6|  |
       |  |  |  |  |  |  |  |   |  |  |  |  |  |  |  |  |
       |__|__|__|__|__|__|__|   |__|__|__|__|__|__|__|  |
                                                        |
           Quarter 1                      Quarter 0     |
                                                        |
   <-----------------------------------------------------
   X

 *  @endverbatim
 *
 *  @author Jeroen van Tilburg
 *  @date   2016-07-18
 */

constexpr CLID CLID_DeFTDetector = 8601;

class DeFTDetector final : public DetDesc::DetectorElementPlus {

public:
  static constexpr uint16_t nQuarters = 48;

  /// Standard constructor
  using DetectorElementPlus::DetectorElementPlus;

  /** Initialization method
   *  @return Status of initialization
   */
  StatusCode initialize() override;

  /** Finalization method - delete objects created with new
   *  @return Status of finalization
   */
  StatusCode finalize();

  /** Retrieves reference to class identifier
   *  @return The class identifier for this class
   */
  const CLID& clID() const override;

  /** Another reference to class identifier
   *  @return The class identifier for this class
   */
  static const CLID& classID() { return CLID_DeFTDetector; }

  /** @return Ft version  */
  int version() const { return m_FTversion; }

  /** @return Vector of pointers to the FT Stations */
  const auto& stations() const { return m_stations; }

  /** Find the FT Station where a global point is
   *  @return Pointer to the relevant Station
   */
  const DeFTStation* findStation( const Gaudi::XYZPoint& point ) const;

  /** Find the FT Layer where a global point is
   *  @return Pointer to the relevant Layer
   */
  const DeFTLayer* findLayer( const Gaudi::XYZPoint& point ) const;

  /** Find the FT Module where a global point is
   *  @return Pointer to the relevant Module
   */
  const DeFTQuarter* findQuarter( const Gaudi::XYZPoint& point ) const;

  /** Find the FT Module where a global point is
   *  @return Pointer to the relevant Module
   */
  const DeFTModule* findModule( const Gaudi::XYZPoint& point ) const;

  /** Find the FT Module where a global point is
   *  @return Pointer to the relevant Module
   */
  const DeFTMat* findMat( const Gaudi::XYZPoint& point ) const;

  /** Find the FT Station corresponding to the channel id
   *  @return Pointer to the relevant station
   */
  [[nodiscard]] const DeFTStation* findStation( const LHCb::Detector::FTChannelID& aChannel ) const {
    return m_stations[to_unsigned( aChannel.station() ) - 1u];
  }

  /** Find the FT Layer corresponding to the channel id
   *  @return Pointer to the relevant layer
   */
  [[nodiscard]] const DeFTLayer* findLayer( const LHCb::Detector::FTChannelID& id ) const {
    const DeFTStation* s = findStation( id );
    return s ? s->findLayer( id ) : nullptr;
  }

  /** Find the FT Quarter corresponding to the channel id
   *  @return Pointer to the relevant quarter
   */
  [[nodiscard]] const DeFTQuarter* findQuarter( const LHCb::Detector::FTChannelID& aChannel ) const {
    const DeFTLayer* l = findLayer( aChannel );
    return l ? l->findQuarter( aChannel ) : nullptr;
  }

  /** Find the FT Module corresponding to the channel id
   *  @return Pointer to the relevant module
   */
  [[nodiscard]] const DeFTModule* findModule( const LHCb::Detector::FTChannelID& aChannel ) const {
    const DeFTQuarter* q = findQuarter( aChannel );
    return q ? q->findModule( aChannel ) : nullptr;
  }

  /** Find the FT Mat corresponding to the channel id
   *  @return Pointer to the relevant module
   */
  [[nodiscard]] const DeFTMat* findMat( const LHCb::Detector::FTChannelID& id ) const {
    const DeFTModule* m = findModule( id );
    return m ? m->findMat( id ) : nullptr;
  }

  /** Find the FT Quarter from the sequential number
   *  @return Pointer to the relevant quarter
   */
  const DeFTQuarter* quarter( int iQuarter ) const { return m_quarters[iQuarter]; };

  /**
   * Return a sensitive volume identifier for a given point in the
   * global reference frame. This function is vital for Gauss.
   */
  int sensitiveVolumeID( const Gaudi::XYZPoint& point ) const override {
    const DeFTMat* mat = findMat( point );
    return mat ? mat->sensitiveVolumeID( point ) : -1;
  }

  /** Get a random channelID using a seed between 0 and 1 */
  LHCb::Detector::FTChannelID getRandomChannelFromSeed( const double seed ) const;

  /// Get a random FTChannelID from a pseudoChannel and a seed (useful for the AP noise)
  LHCb::Detector::FTChannelID getRandomChannelFromPseudo( const int pseudoChannel, const double seed ) const;

  /// Get the total number of channels in the FT detector
  int nStations() const { return m_nStations; }

  /// Get the total number of channels in the FT detector
  int nChannels() const { return m_nTotChannels; }

  /// Returns the first mat
  const DeFTMat* firstMat() const { return this->stations()[0]->layers()[0]->quarters()[0]->modules()[0]->mats()[0]; }

  /// Visitor functions
  void applyToAllChildren( const std::function<void( const DeFTStation& )>& func ) const {
    for ( const auto* station : m_stations ) { func( *station ); };
  };

  void applyToAllLayers( const std::function<void( const DeFTLayer& )>& func ) const {
    for ( const auto* station : m_stations ) { station->applyToAllChildren( func ); };
  };
  void applyToAllMats( const std::function<void( const DeFTMat& )>& func ) const {
    for ( const auto* station : m_stations )
      for ( const auto* layer : station->layers() )
        for ( const auto* quarter : layer->quarters() )
          for ( const auto* module : quarter->modules() ) module->applyToAllChildren( func );
  }

private:
  unsigned int m_FTversion = 0;

  /// vector of pointers to stations
  std::array<const DeFTStation*, 3> m_stations{{nullptr, nullptr, nullptr}};
  unsigned int                      m_nStations;

  /// flact vector of pointers to quarters for fast access
  std::array<const DeFTQuarter*, nQuarters> m_quarters;

  unsigned int m_nModulesT1;
  unsigned int m_nModulesT2;
  unsigned int m_nModulesT3;
  unsigned int m_nLayers;
  unsigned int m_nQuarters;
  unsigned int m_nTotQuarters;
  unsigned int m_nChannelsInModule;
  unsigned int m_nTotChannels;

  /// Use a single MsgStream instance (created in initialize)
  std::unique_ptr<MsgStream> m_msg;

}; // end of class
using DeFT = DeFTDetector;

#endif
