/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#ifdef USE_DD4HEP
#  include "Detector/FT/DeFTStation.h"
using DeFTStation = LHCb::Detector::DeFTStation;
#else
#  include "DetDesc/DetectorElement.h"
#  include "Detector/FT/FTChannelID.h"
#  include "FTDet/DeFTLayer.h"

/** @class DeFTStation DeFTStation.h "FTDet/DeFTStation.h"
 *
 *  This is the detector element class of the Fibre Tracker (FT) station.
 *
 *  @author Jeroen van Tilburg
 *  @date   2016-07-18
 */

static const CLID CLID_DeFTStation = 8602;

class DeFTStation final : public DetDesc::DetectorElementPlus {

public:
  /// Standard constructor
  using DetectorElementPlus::DetectorElementPlus;

  /// Initialization method
  StatusCode initialize() override;

  /** Retrieves reference to class identifier
   *  @return The class identifier for this class
   */
  const CLID& clID() const override;

  /** Another reference to class identifier
   *  @return The class identifier for this class
   */
  static const CLID& classID() { return CLID_DeFTStation; }

  /** @return stationID */
  [[nodiscard]] unsigned int stationID() const { return m_stationID; }

  /** @return stationID */
  [[nodiscard]] unsigned int stationIdx() const { return m_stationID - 1; }

  /** @return Vector of pointers to the FT Layers */
  [[nodiscard]] const auto& layers() const { return m_layers; }

  /** Find the FT Layer where a global point is
   *  @return Pointer to the relevant Layer
   */
  [[nodiscard]] const DeFTLayer* findLayer( const Gaudi::XYZPoint& point ) const;

  /** Const method to return the layer for a given channel id
   * @param  aChannel an FT channel id
   * @return pointer to detector element
   */
  [[nodiscard]] const DeFTLayer* findLayer( const LHCb::Detector::FTChannelID& aChannel ) const {
    return m_layers[to_unsigned( aChannel.layer() )];
  }

  /// Visitor functions
  void applyToAllChildren( const std::function<void( const DeFTLayer& )>& func ) const {
    for ( const auto* layer : m_layers ) { func( *layer ); };
  };

private:
  /// array of pointers to layers
  std::array<const DeFTLayer*, 4> m_layers{{nullptr, nullptr, nullptr, nullptr}};

  unsigned int m_stationID; ///< station ID number

}; // end of class
#endif
