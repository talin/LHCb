/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "FTDet/DeFTMat.h"

#include "DetDesc/SolidBox.h"

#include "GaudiKernel/IUpdateManagerSvc.h"

/** @file DeFTMat.cpp
 *
 *  Implementation of class : DeFTMat
 *
 *  @author Jeroen van Tilburg
 *  @date   2016-07-18
 */

//=============================================================================
// classID function
//=============================================================================
const CLID& DeFTMat::clID() const { return DeFTMat::classID(); }

//=============================================================================
// Initialization
//=============================================================================
StatusCode DeFTMat::initialize() {
  MsgStream msg( msgSvc(), "DeFTMat" );

  IDetectorElementPlus* module  = this->parentIDetectorElementPlus();
  IDetectorElementPlus* quarter = module->parentIDetectorElementPlus();
  IDetectorElementPlus* layer   = quarter->parentIDetectorElementPlus();
  IDetectorElementPlus* station = layer->parentIDetectorElementPlus();

  // Get specific parameters from the module
  auto matID     = static_cast<LHCb::Detector::FTChannelID::MatID>( param<int>( "matID" ) );
  auto moduleID  = static_cast<LHCb::Detector::FTChannelID::ModuleID>( module->params()->param<int>( "moduleID" ) );
  auto quarterID = static_cast<LHCb::Detector::FTChannelID::QuarterID>( quarter->params()->param<int>( "quarterID" ) );
  auto layerID   = static_cast<LHCb::Detector::FTChannelID::LayerID>( layer->params()->param<int>( "layerID" ) );
  auto stationID = static_cast<LHCb::Detector::FTChannelID::StationID>( station->params()->param<int>( "stationID" ) );
  LHCb::Detector::FTChannelID aChan( stationID, layerID, quarterID, moduleID, matID, 0u, 0u );
  setElementID( aChan );

  // Get some useful geometric parameters from the database
  m_airGap           = param<double>( "airGap" );
  m_deadRegion       = param<double>( "deadRegion" );
  m_channelPitch     = param<double>( "channelPitch" );
  m_halfChannelPitch = 0.5f * m_channelPitch;
  m_dieGap           = param<double>( "dieGap" );
  m_nChannelsInSiPM  = param<int>( "nChannelsInSiPM" );
  m_nSiPMsInMat      = param<int>( "nSiPMsInMat" );
  m_nDiesInSiPM      = param<int>( "nDiesInSiPM" );

  m_sipmPitch      = m_nChannelsInSiPM * m_channelPitch + m_dieGap + 2 * m_airGap + 2 * m_deadRegion;
  m_nChannelsInDie = m_nChannelsInSiPM / m_nDiesInSiPM;
  m_diePitch       = m_nChannelsInDie * m_channelPitch + m_dieGap;
  m_uBegin         = m_airGap + m_deadRegion - 2.0 * m_sipmPitch;

  updateCache().ignore();
  updMgrSvc()->registerCondition( this, geometry(), &DeFTMat::updateCache );

  m_matContractionParameterVector.reserve( m_nChannelsInSiPM * m_nSiPMsInMat );
  const std::string contractionConditionName = getContractionConditionName( aChan );
  if ( hasCondition( contractionConditionName ) ) {
    m_matContractionCondition = condition( contractionConditionName );
    updMgrSvc()->registerCondition( this, m_matContractionCondition.path(), &DeFTMat::updateMatContraction );
    m_matContractionParameterVector.shrink_to_fit();
    auto sc = updMgrSvc()->update( this );
    if ( sc.isFailure() ) { msg << MSG::ERROR << "issue with updMgrSvc" << endmsg; }
  } else {
    msg << MSG::DEBUG << "Unable to find FT mat contraction conditions for " << getMatName( aChan )
        << ". It will not be possible to correct for temperature distortions." << endmsg;
    m_matContractionParameterVector.shrink_to_fit();
  }

  return StatusCode::SUCCESS;
}

StatusCode DeFTMat::updateCache() {
  // Get the boundaries of the layer
  const SolidBox* box = dynamic_cast<const SolidBox*>( geometryPlus()->lvolume()->solid()->coverTop() );
  m_sizeX             = box->xsize();
  m_sizeY             = box->ysize();
  m_sizeZ             = box->zsize();

  // Get the central points of the fibres at the mirror and at the SiPM locations
  m_mirrorPoint = geometryPlus()->toGlobal( Gaudi::XYZPoint( 0, -0.5f * m_sizeY, 0 ) );
  m_sipmPoint   = geometryPlus()->toGlobal( Gaudi::XYZPoint( 0, +0.5f * m_sizeY, 0 ) );

  // Define the global z position to be at the point closest to the mirror
  m_globalZ = m_mirrorPoint.z();

  // Define the global length in y of the mat
  m_globaldy = m_sipmPoint.y() - m_mirrorPoint.y();

  // Make the plane for the mat
  const Gaudi::XYZPoint g1 = geometryPlus()->toGlobal( Gaudi::XYZPoint( 0., 0., 0. ) );
  const Gaudi::XYZPoint g2 = geometryPlus()->toGlobal( Gaudi::XYZPoint( 1., 0., 0. ) );
  const Gaudi::XYZPoint g3 = geometryPlus()->toGlobal( Gaudi::XYZPoint( 0., 1., 0. ) );
  m_plane                  = Gaudi::Plane3D( g1, g2, g3 );

  // Get the slopes in units of local delta x
  m_ddx = Gaudi::XYZVectorF( g2 - g1 );

  // Get the slopes in units of delta y (needed by PrFTHit, mind the sign)
  Gaudi::XYZVectorF deltaY( g1 - g3 );
  m_dxdy = deltaY.x() / deltaY.y();
  m_dzdy = deltaY.z() / deltaY.y();

  return StatusCode::SUCCESS;
}

StatusCode DeFTMat::updateMatContraction() {
  if ( m_matContractionCondition->exists( "matContraction" ) ) {
    setmatContractionParameterVector( m_matContractionCondition->param<std::vector<double>>( "matContraction" ) );
  } else {
    m_matContractionParameterVector.shrink_to_fit();
  }

  return StatusCode::SUCCESS;
}

// determine which channel+fraction belongs to a local x-position
// (used in the digitization)
std::pair<LHCb::Detector::FTChannelID, float> DeFTMat::calculateChannelAndFrac( float localX ) const {

  // Correct for the starting point of the sensitive area
  float xInMat = localX - m_uBegin;

  // Find the sipm that is hit and the local position within the sipm
  int   hitSiPM = std::clamp( int( xInMat / m_sipmPitch ), 0, m_nSiPMsInMat - 1 );
  float xInSiPM = fma( -m_sipmPitch, hitSiPM, xInMat );

  // Find the die that is hit and the local position within the die
  int   hitDie    = std::clamp( int( xInSiPM / m_diePitch ), 0, m_nDiesInSiPM - 1 );
  float chanInDie = fma( -m_diePitch, hitDie, xInSiPM ) / m_channelPitch;

  // Find the channel that is hit and the local position within the channel
  int   hitChan = std::clamp( int( chanInDie ), 0, m_nChannelsInDie - 1 );
  float frac    = chanInDie - hitChan - 0.5f;

  // Construct channelID
  return std::make_pair( LHCb::Detector::FTChannelID( stationID(), layerID(), quarterID(), moduleID(), matID(), hitSiPM,
                                                      hitChan + ( hitDie * m_nChannelsInDie ) ),
                         frac );
}

// Get the relevant channel boundaries
std::vector<std::pair<LHCb::Detector::FTChannelID, float>>
DeFTMat::calculateChannels( LHCb::Detector::FTChannelID thisChannel, LHCb::Detector::FTChannelID endChannel ) const {
  // Reserve memory
  std::vector<std::pair<LHCb::Detector::FTChannelID, float>> channelsAndLeftEdges;
  channelsAndLeftEdges.reserve( endChannel - thisChannel );

  // Loop over the intermediate channels
  bool keepAdding = true;
  while ( keepAdding ) {
    float channelLeftEdge = localXfromChannel( thisChannel, -0.5f );
    // Add channel and left edge to output vector.
    channelsAndLeftEdges.emplace_back( thisChannel, channelLeftEdge );
    if ( thisChannel == endChannel ) keepAdding = false;
    thisChannel.advance();
  }
  return channelsAndLeftEdges;
}

// Get the relevant channel boundaries
std::vector<std::pair<LHCb::Detector::FTChannelID, float>>
DeFTMat::calculateChannels( const float localEntry, const float localExit,
                            const unsigned int numOfAdditionalChannels ) const {
  // set ordering in increasing local x
  float xBegin = std::min( localEntry, localExit );
  float xEnd   = std::max( localEntry, localExit );

  // Find the first and last channels that are involved
  float xOffset                       = numOfAdditionalChannels * m_channelPitch;
  const auto [thisChannel, fracBegin] = calculateChannelAndFrac( xBegin - xOffset );
  const auto [endChannel, fracEnd]    = calculateChannelAndFrac( xEnd + xOffset );

  // return empty vector when both channels are the same gap
  if ( thisChannel.channelID() == endChannel.channelID() && std::abs( fracBegin ) > 0.5f &&
       std::abs( fracEnd ) > 0.5f && fracBegin * fracEnd > 0.25 )
    return std::vector<std::pair<LHCb::Detector::FTChannelID, float>>();

  return DeFTMat::calculateChannels( thisChannel, endChannel );
}

// Find the local x-position for a given channel+fraction
float DeFTMat::localXfromChannel( const LHCb::Detector::FTChannelID channelID, const float frac ) const {
  float uFromChannel = m_uBegin + ( float( channelID.channel() ) + 0.5f + frac ) * m_channelPitch;
  if ( int( channelID.channel() ) >= m_nChannelsInDie ) uFromChannel += m_dieGap;
  uFromChannel += channelID.sipm() * m_sipmPitch;
  return uFromChannel;
}

// Get the distance between a 3D global point and a channel+fraction
float DeFTMat::distancePointToChannel( const Gaudi::XYZPoint& globalPoint, const LHCb::Detector::FTChannelID channelID,
                                       const float frac ) const {
  Gaudi::XYZPoint localPoint = geometryPlus()->toLocal( globalPoint );
  return localXfromChannel( channelID, frac ) - localPoint.x();
}

// Get the begin and end positions of a fibre
LHCb::LineTraj<double> DeFTMat::trajectory( const LHCb::Detector::FTChannelID channelID, const float frac ) const {
  float           localX = localXfromChannel( channelID, frac );
  Gaudi::XYZPoint mirrorPoint( m_mirrorPoint.x() + localX * m_ddx.x(), m_mirrorPoint.y() + localX * m_ddx.y(),
                               m_mirrorPoint.z() + localX * m_ddx.z() );
  Gaudi::XYZPoint sipmPoint( m_sipmPoint.x() + localX * m_ddx.x(), m_sipmPoint.y() + localX * m_ddx.y(),
                             m_sipmPoint.z() + localX * m_ddx.z() );
  return {mirrorPoint, sipmPoint};
}

// Get the endpoints of the line defined by the hit
std::pair<Gaudi::XYZPointF, Gaudi::XYZPointF> DeFTMat::endPoints( const LHCb::Detector::FTChannelID channelID,
                                                                  const float                       frac ) const {
  float localX = localXfromChannel( channelID, frac );

  Gaudi::XYZVectorF delta( m_ddx * localX );
  return {m_mirrorPoint + delta, m_sipmPoint + delta};
}

// Get mat name as string
std::string DeFTMat::getMatName( LHCb::Detector::FTChannelID channelID ) const {
  // station
  std::string name = "T";
  name += std::to_string( to_unsigned( channelID.station() ) );
  // layer
  switch ( to_unsigned( channelID.layer() ) ) {
  case 0:
    name += "X1";
    break;
  case 1:
    name += "U";
    break;
  case 2:
    name += "V";
    break;
  case 3:
    name += "X2";
    break;
  }
  // quarter
  name += "Q";
  name += std::to_string( to_unsigned( channelID.quarter() ) );
  // module
  name += "M";
  name += std::to_string( to_unsigned( channelID.module() ) );
  // mat
  name += "Mat";
  name += std::to_string( to_unsigned( channelID.mat() ) );
  return name;
}

// Get contraction condition name as string
std::string DeFTMat::getContractionConditionName( LHCb::Detector::FTChannelID channelID ) const {
  return "matContraction" + getMatName( channelID );
}
