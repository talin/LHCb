/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifdef USE_DD4HEP

#  include "Detector/UT/DeUT.h"

using DeUTDetector        = LHCb::Detector::UT::DeUT;
using DeUTLayer           = LHCb::Detector::UT::DeUTLayer;
using DeUTSector          = LHCb::Detector::UT::DeUTSector;
namespace DeUTDetLocation = LHCb::Detector::UT;

#else

#  include "DetDesc/DetectorElement.h"
#  include "Detector/UT/ChannelID.h"
#  include "GaudiKernel/VectorMap.h"
#  include "Kernel/LHCbID.h"
#  include "Kernel/LineTraj.h"
#  include "UTDet/DeUTBaseElement.h"
#  include "UTDet/DeUTLayer.h"
#  include "UTDet/DeUTSector.h"
#  include "UTDet/DeUTSide.h"

#  include "fmt/format.h"

#  include <cassert>
#  include <memory>
#  include <string>
#  include <vector>

namespace DeUTDetLocation {

  /// UT location in transient detector store
  inline const std::string UT = "/dd/Structure/LHCb/BeforeMagnetRegion/UT";

  inline const std::string& location() { return DeUTDetLocation::UT; }

  inline unsigned int detType() { return LHCb::Detector::UT::ChannelID::detType::typeUT; }

} // namespace DeUTDetLocation

/** @class DeUTDetector DeUTDetector.h UTDet/DeUTDetector.h
 *
 *  UT Detector Element class
 *
 *  All UT elements are modification from TT element classes
 *  that were originally written by Matt Needham)
 *
 *  @author Andy Beiter (based on code by Jianchun Wang, Matt Needham)
 *  @date   2018-09-04
 *
 */

constexpr CLID CLID_DeUTDetector = 9301;

namespace {
  constexpr int NBSTATION   = 2;
  constexpr int NBLAYER     = 2;  // nbr layers per station
  constexpr int NBREGION    = 3;  // nbr regions per layer
  constexpr int NBSECTOR    = 98; // nbr sectors per region
  constexpr int NBSIDE      = 2;
  constexpr int NBHALFLAYER = 4; // nbr halflayers per side
  constexpr int NBSTAVE     = 9; // nbr staves per halflayer
  constexpr int NBFACE      = 2; // nbr face per stave
  constexpr int NBMODULE    = 8; // nbr module per face
  constexpr int NBSUBSECTOR = 2; // nbr subsector per module
} // namespace

class DeUTDetector : public DetDesc::DetectorElementPlus {

public:
  using Sectors = std::vector<const DeUTSector*>;
  using Layers  = std::vector<const DeUTLayer*>;
  using Modules = std::vector<const DeUTModule*>;
  using Sides   = std::vector<const DeUTSide*>;
  struct LayerGeom {
    float z{0};
    int   nColsPerSide{0};
    int   nRowsPerSide{0};
    float invHalfSectorYSize{0};
    float invHalfSectorXSize{0};
    float dxDy{0};
  };
  enum class LayerID_t : unsigned int {};
  enum struct Side_t : unsigned int {};
  enum struct Layer_t : unsigned int {};
  enum struct Stave_t : unsigned int {};
  enum struct Face_t : unsigned int {};
  enum struct Module_t : unsigned int {};
  enum struct Sector_t : unsigned int {};

  /** Constructor */
  using DetectorElementPlus::DetectorElementPlus;

  /** Geometry version **/
  enum struct GeoVersion { ErrorVersion = 0, v0, v1 };

  /**
   * Retrieves reference to class identifier
   * @return the class identifier for this class
   */
  static const CLID& classID() { return CLID_DeUTDetector; }

  /**
   * another reference to class identifier
   * @return the class identifier for this class
   */
  const CLID& clID() const override;

  /** initialization method
   * @return Status of initialisation
   */
  StatusCode initialize() override;

  /** @return number of first station */
  [[nodiscard]] unsigned int firstStation() const { return m_firstSide; }

  /** @return number of last station */
  [[nodiscard]] unsigned int lastStation() const { return m_firstSide + m_sides.size() - 1u; }

  /** @return number of stations */
  [[nodiscard]] unsigned int nStation() const { return m_sides.size(); }

  /** @return number of first side */
  [[nodiscard]] unsigned int firstSide() const { return m_firstSide; }

  /** @return number of last side */
  [[nodiscard]] unsigned int lastSide() const { return m_firstSide + m_sides.size() - 1u; }

  /** @return number of sides */
  [[nodiscard]] unsigned int nSide() const { return m_sides.size(); }

  /** Implementation of sensitive volume identifier for a given point in the
      global reference frame. This is the sensor number defined in the xml.
  */
  [[nodiscard]] int sensitiveVolumeID( const Gaudi::XYZPoint& globalPos ) const override;

  /**  locate the side based on a channel id
  @return  side */
  [[nodiscard]] const DeUTSide* findSide( const LHCb::Detector::UT::ChannelID aChannel ) const;

  /** locate side based on a point
  @return side */
  [[nodiscard]] const DeUTSide* findSide( const Gaudi::XYZPoint& point ) const;

  /**
   *  short cut to pick up the side corresponding to a given nickname
   * @param nickname
   * @return side
   */
  [[nodiscard]] const DeUTSide* findSide( std::string_view nickname ) const;

  /**
   *  short cut to pick up the station/side corresponding to a given nickname
   * @param nickname
   * @return layer
   */
  [[nodiscard]] const DeUTLayer* findLayer( std::string_view nickname ) const;

  /**  locate the layer based on a channel id
  @return  layer */
  [[nodiscard]] const DeUTLayer* findLayer( LHCb::Detector::UT::ChannelID aChannel ) const;

  /** locate layer based on a point
   *return layer */
  [[nodiscard]] const DeUTLayer* findLayer( const Gaudi::XYZPoint& point ) const;

  /** check contains channel
   *  @param  aChannel channel
   *  @return bool
   */
  [[nodiscard]] bool contains( LHCb::Detector::UT::ChannelID aChannel ) const;

  /// Workaround to prevent hidden base class function
  [[nodiscard]] bool isValid() const override { return ValidDataObject::isValid(); }

  /// Workaround to prevent hidden base class function
  [[nodiscard]] bool isValid( const Gaudi::Time& t ) const override { return ValidDataObject::isValid( t ); }

  /** check channel number is valid */
  [[nodiscard]] bool isValid( LHCb::Detector::UT::ChannelID aChannel ) const;

  /**
   * access to a given side
   * Note that there is no checks on the validity of the index.
   * use at your own risks
   */
  [[nodiscard]] const DeUTSide& side( unsigned int index ) const { return *m_sides[index]; }

  [[nodiscard]] const DeUTSide& station( unsigned int index ) const { return *m_sides[index]; }

  /** vector of station
   * @return vector of stations
   */
  [[nodiscard]] const Sides& stations() const { return m_sides; }

  /** vector of sides
   * @return vector of sides
   */
  [[nodiscard]] const Sides& sides() const { return m_sides; }

  /** flat vector of sectors
   * @return vector of sectors
   */
  [[nodiscard]] const Sectors& sectors() const { return m_sectors; }

  /**
   * access to a given sector
   * Note that there is no checks on the validity of the index.
   * use at your own risks
   */
  [[nodiscard]] const DeUTSector& sector( unsigned int index ) const { return *m_sectors[index]; }

  /// @return number of sectors
  unsigned int nSectors() const { return m_sectors.size(); }

  /// apply given callable to all layers
  void applyToAllSectors( const std::function<void( DeUTSector const& )>& func ) const {
    for ( auto* sector : m_sectors ) { func( *sector ); }
  }

  /// apply given callable to all layers
  void applyToAllSectorsAllen( const std::function<void( DeUTSector const& )>& func ) const {
    for ( auto* sector : m_sectorsAllen ) { func( *sector ); }
  }

  /// check no sector returns true for the given callable
  bool none_of_sectors( const std::function<bool( DeUTSector const& )>& func ) const {
    for ( auto* sector : m_sectors ) {
      if ( func( *sector ) ) return false;
    }
    return true;
  }

  /** flat vector of sectors in single full layer
   * @return vector of sectors
   */
  const Sectors sectors( LayerID_t layerID ) const {
    assert( layerSizeOK() && "UT Layer size should be 4 for v0 geometry and 8 for v1 geometry" );
    assert( (unsigned int)layerID < NBHALFLAYER && "layerID should be less then 4" );
    const DeUTLayer* layerC   = m_layers[(unsigned int)layerID];
    Sectors          tsectors = layerC->sectors();
    if ( m_layers.size() > 4 ) {
      const DeUTLayer* layerA = m_layers[(unsigned int)layerID + 4];
      tsectors.insert( tsectors.end(), layerA->sectors().begin(), layerA->sectors().end() );
    }
    return tsectors;
  }

  /** check sector to be swapped in old version Geom*/
  bool SectorsSwapped() const {
    float xPos24( 0.0f ), xPos25( 0.0f );
    auto  SectorsInLayer = sectors( LayerID_t{0} );
    xPos24               = SectorsInLayer[107]->globalCentre().X();
    xPos25               = SectorsInLayer[108]->globalCentre().X();
    bool isSwapped       = true;
    if ( m_version == GeoVersion::v0 && ( xPos24 >= xPos25 ) ) isSwapped = false;
    return isSwapped;
  }

  bool layerSizeOK() const {
    bool isOK = ( m_layers.size() == 8 );
    if ( m_version == GeoVersion::v0 ) { isOK = ( m_layers.size() == 4 ); }
    return isOK;
  }

  /** LayerInfo
   * @return LayerGeom (z, nColsPerSide, nRowsPerSide, invHalfSectorYSize, invHalfSectorXSize, dxDy)
   */
  const LayerGeom getLayerGeom( unsigned int layerid ) const {
    LayerGeom    layergoem;
    auto         SectorsInLayer = sectors( LayerID_t{layerid} );
    auto         tStation       = layerid / 2;
    unsigned int keysectorID    = ( tStation == 0 ) ? 31 : 11;
    if ( m_version == GeoVersion::v0 ) { keysectorID = 0; }
    layergoem.z = SectorsInLayer[keysectorID]->globalCentre().z();

    float        YFirstRow         = std::numeric_limits<float>::max();
    float        YLastRow          = std::numeric_limits<float>::lowest();
    float        smallestXLastCol  = std::numeric_limits<float>::max();
    float        smallestXFirstcol = std::numeric_limits<float>::max();
    float        biggestXFirstCol  = std::numeric_limits<float>::lowest();
    unsigned int biggestColumn     = ( tStation == 0 ) ? 16u : 18u;
    unsigned int smallestColumn    = 1u;
    unsigned int topMostRow        = 14u;
    unsigned int bottomMostRow     = 1u;

    // Second pass
    // find x and y values in the corners to deduce the geometry of the layer
    for ( const auto& sector : SectorsInLayer ) {
      // deal with x,y coordinates. Remember the corner coordinates
      const DeUTSector& utSector = dynamic_cast<const DeUTSector&>( *sector );
      auto              column   = utSector.column();
      auto              row      = utSector.row();
      auto              center   = sector->toGlobal( Gaudi::XYZPoint{0, 0, 0} );
      if ( column == smallestColumn ) {
        if ( row == bottomMostRow ) {
          smallestXFirstcol = center.x();
          YFirstRow         = center.y();
        } else if ( row == topMostRow ) {
          biggestXFirstCol = center.x();
          YLastRow         = center.y();
        }
      }
      if ( column == biggestColumn && row == bottomMostRow ) { smallestXLastCol = center.x(); }
    }
    // gather all information into the corresponding LayerInfo object
    auto ncols                   = biggestColumn - smallestColumn + 1;
    auto nrows                   = topMostRow - bottomMostRow + 1;
    layergoem.nColsPerSide       = ncols / 2;
    layergoem.nRowsPerSide       = nrows / 2;
    layergoem.invHalfSectorYSize = 2 * ( nrows - 1 ) / ( YLastRow - YFirstRow );
    layergoem.invHalfSectorXSize = 2 * ( ncols - 1 ) / ( smallestXLastCol - smallestXFirstcol );
    layergoem.dxDy               = ( biggestXFirstCol - smallestXFirstcol ) / ( YLastRow - YFirstRow );

    return layergoem;
  }

  [[nodiscard]] double zStation( unsigned int stationid ) const {
    if ( stationid > 2 ) throw std::runtime_error( "wrong stationID in DeUTDetector::zStation" );
    double m_zUT = ( m_sides[0]->layers()[stationid * 2]->globalCentre().z() +
                     m_sides[0]->layers()[stationid * 2 + 1]->globalCentre().z() ) /
                   2;
    if ( m_version == GeoVersion::v0 ) { m_zUT = m_sides[stationid]->globalCentre().z(); }
    return m_zUT;
  }

  /** flat vector of layers
   * @return vector of layers
   */
  [[nodiscard]] const Layers& layers() const { return m_layers; }

  /**
   * access to a given layer
   * Note that there is no checks on the validity of the index.
   * use at your own risks
   */
  [[nodiscard]] const DeUTLayer& layer( unsigned int index ) const { return *m_layers[index]; }

  /// apply given callable to all layers
  void applyToAllLayers( const std::function<void( DeUTLayer const& )>& func ) const {
    for ( auto* layer : m_layers ) { func( *layer ); }
  }

  /**
   *  short cut to pick up the wafer corresponding to x,y,z
   * @param  aPoint point in global frame
   * @return sector
   */
  [[nodiscard]] const DeUTSector* findSector( const Gaudi::XYZPoint& aPoint ) const;

  /**
   *  short cut to pick up the wafer corresponding to a channel
   * @param  aChannel channel
   * @return sector
   */
  [[nodiscard]] const DeUTSector* findSector( LHCb::Detector::UT::ChannelID aChannel ) const;

  /**
   *  get the sector corresponding to the input channel
   * @param  aChannel channel
   * @return sector
   */
  [[nodiscard]] const DeUTSector& getSector( LHCb::Detector::UT::ChannelID chan ) const {
    auto* sec = getSector( Side_t{chan.side()}, Layer_t{chan.layer()}, Stave_t{chan.stave()}, Face_t{chan.face()},
                           Module_t{chan.module()}, Sector_t{chan.sector()} );
    if ( !sec ) throw std::runtime_error{"DeUTDetector::getSector returned nullptr"};
    return *sec;
  }

  /**
   *  short cut to pick up the wafer corresponding to a given nickname
   * @param nickname
   * @return sector
   */
  [[nodiscard]] const DeUTSector* findSector( std::string_view nickname ) const;

  /** get the next channel left */
  [[nodiscard]] LHCb::Detector::UT::ChannelID nextLeft( LHCb::Detector::UT::ChannelID testChan ) const;

  /** get the next channel right */
  [[nodiscard]] LHCb::Detector::UT::ChannelID nextRight( LHCb::Detector::UT::ChannelID testChan ) const;

  /** get the trajectory
   @return trajectory
  */
  [[nodiscard]] LHCb::LineTraj<double> trajectory( LHCb::LHCbID id, double offset ) const;

  /** get the number of strips in detector*/
  [[nodiscard]] unsigned int nStrip() const { return m_nStrip; }

  /** get the number of layers **/
  [[nodiscard]] unsigned int nLayer() const { return m_layers.size(); }

  /** get the number of readout sectors **/
  [[nodiscard]] unsigned int nReadoutSector() const { return m_sectors.size(); }

  /** number of layers per station **/
  [[nodiscard]] unsigned int nLayersPerStation() const { return nLayer() / nStation(); }

  /** number of layers per side **/
  [[nodiscard]] unsigned int nLayersPerSide() const { return nLayer() / nSide(); }

  /** Geometry version **/
  [[nodiscard]] auto version() const { return m_version; }

  /**
   * fraction active channels
   * @return bool fraction active
   */
  [[nodiscard]] double fractionActive() const;

  [[nodiscard]] const DeUTSector* getSector( Side_t, Layer_t, Stave_t, Face_t, Module_t, Sector_t ) const;

  template <class T>
  StatusCode initializefunc( std::vector<const T*>& m_s ) {
    for ( auto* iChild : childIDetectorElements() ) {
      auto* tStation = dynamic_cast<T*>( iChild );
      if ( tStation ) { m_s.push_back( tStation ); }
    }

    if ( !m_s.empty() ) {
      setFirstSide( m_s.front()->id() );
    } else {
      // no stations - this is an error
      MsgStream msg( msgSvc(), name() );
      msg << MSG::ERROR << "No child elements !" << endmsg;
      return StatusCode::FAILURE;
    }
    return StatusCode::SUCCESS;
  }

  /// apply function to given sectors
  void applyToSectors( const std::vector<LHCb::Detector::UT::ChannelID>& vec,
                       const std::function<void( DeUTSector const& )>&   func ) const;

  /** get list of all disabled sectors */
  Sectors disabledSectors() const;

  /** get list of disabled beetles */
  [[nodiscard]] std::vector<LHCb::Detector::UT::ChannelID> disabledBeetles() const;

  /** beetle as a string */
  std::string uniqueBeetle( const LHCb::Detector::UT::ChannelID& chan ) const {
    auto const sector = findSector( chan );
    return fmt::format( "{}Beetle{}", sector->nickname(), sector->beetle( chan ) );
  };

  /** port */
  std::string uniquePort( const LHCb::Detector::UT::ChannelID& chan ) const {
    const unsigned int port = ( ( chan.strip() - 1u ) / LHCbConstants::nStripsInPort ) + 1u;
    return fmt::format( "{}Port{}", uniqueBeetle( chan ), port );
  };

private:
  /** set the first Side number */
  void setFirstSide( const unsigned int iSide );

  /** set the strip number  */
  DeUTDetector& setNstrip( unsigned int nStrip ) {
    m_nStrip = nStrip;
    return *this;
  }

  Sides                                                  m_sides;
  Sectors                                                m_sectors;
  Sectors                                                m_sectorsAllen;
  std::vector<const DeUTLayer*>                          m_layers;
  GaudiUtils::VectorMap<unsigned int, const DeUTSector*> m_sMap;

  static unsigned int STCBmap( unsigned int i ) {
    constexpr auto a = std::array{0, 2, 4, 5, 6, 7, 8, 9, 10, 12, 14};
    auto j = std::find( a.begin(), a.end(), i ); // the entries are ordered, so could do a binary search, but the number
                                                 // of entries is so small that is probably worse...
    if ( j == a.end() ) throw std::out_of_range( "invalid STCBmap request" );
    return static_cast<unsigned int>( j - a.begin() );
  }
  static unsigned int STCFmap( unsigned int i ) {
    constexpr auto a = std::array{0, 2, 4, 6, 7, 8, 9, 10, 11, 12, 14};
    auto           j = std::find( a.begin(), a.end(), i );
    if ( j == a.end() ) throw std::out_of_range( "invalid STCFmap request" );
    return static_cast<unsigned int>( j - a.begin() );
  }
  static unsigned int STBBmap( unsigned int i ) {
    constexpr auto a = std::array{0, 2, 4, 5, 8, 9, 10, 12, 14};
    auto           j = std::find( a.begin(), a.end(), i );
    if ( j == a.end() ) throw std::out_of_range( "invalid STBBmap request" );
    return static_cast<unsigned int>( j - a.begin() );
  }
  static unsigned int STBFmap( unsigned int i ) {
    constexpr auto a = std::array{0, 2, 4, 6, 7, 10, 11, 12, 14};
    auto           j = std::find( a.begin(), a.end(), i );
    if ( j == a.end() ) throw std::out_of_range( "invalid STBFmap request" );
    return static_cast<unsigned int>( j - a.begin() );
  }
  static unsigned int STABmap( unsigned int i ) {
    constexpr auto a = std::array{0, 2, 4, 8, 10, 12, 14};
    auto           j = std::find( a.begin(), a.end(), i );
    if ( j == a.end() ) throw std::out_of_range( "invalid STABmap request" );
    return static_cast<unsigned int>( j - a.begin() );
  }
  static unsigned int STAFmap( unsigned int i ) {
    constexpr auto a = std::array{0, 2, 4, 6, 10, 12, 14};
    auto           j = std::find( a.begin(), a.end(), i );
    if ( j == a.end() ) throw std::out_of_range( "invalid STAFmap request" );
    return static_cast<unsigned int>( j - a.begin() );
  }
  inline static const unsigned int NTOTSECTOR = 1048;
  inline static const unsigned int NSECTSTA   = 124;
  inline static const unsigned int NSECTSTB   = 138;
  inline static const unsigned int NSECTSTVC  = 22;
  inline static const unsigned int NSECTSTVB  = 18;
  inline static const unsigned int NSECTSTVA  = 14;

private:
  unsigned int m_firstStation = 0u;
  unsigned int m_firstSide    = 0u;
  unsigned int m_nStrip       = 0u;
  GeoVersion   m_version      = GeoVersion::v1;

  /** make flat list of lowest descendents  and also layers */
  void flatten();
  /** offsets on the "flatten" list of sectors in order to have quicker access */
  /** should contain the # of sectors in old/new geommetry **/
  // std::vector<std::size_t> m_offset;
};

inline bool DeUTDetector::contains( const LHCb::Detector::UT::ChannelID aChannel ) const {
  return aChannel.side() < lastSide();
}

inline LHCb::Detector::UT::ChannelID DeUTDetector::nextLeft( const LHCb::Detector::UT::ChannelID aChannel ) const {
  const auto aSector = findSector( aChannel );
  return ( aSector ? aSector->nextLeft( aChannel ) : LHCb::Detector::UT::ChannelID( 0u ) );
}

inline LHCb::Detector::UT::ChannelID DeUTDetector::nextRight( const LHCb::Detector::UT::ChannelID aChannel ) const {
  const auto aSector = findSector( aChannel );
  return ( aSector ? aSector->nextRight( aChannel ) : LHCb::Detector::UT::ChannelID( 0u ) );
}

inline bool DeUTDetector::isValid( const LHCb::Detector::UT::ChannelID aChannel ) const {
  const auto aSector = findSector( aChannel );
  return ( aSector ? aSector->isStrip( aChannel.strip() ) : false );
}

inline const DeUTSector* DeUTDetector::findSector( const LHCb::Detector::UT::ChannelID aChannel ) const {
  auto iter = m_sMap.find( aChannel.uniqueSector() );
  return ( iter != m_sMap.end() ? iter->second : nullptr );
}

#endif
