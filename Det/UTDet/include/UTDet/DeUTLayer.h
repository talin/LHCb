/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/UT/ChannelID.h"

#ifdef USE_DD4HEP

#  include "Detector/UT/DeUTLayer.h"
using DeUTLayer = LHCb::Detector::UT::DeUTLayer;

#else

#  include "UTDet/DeUTBaseElement.h"
#  include "UTDet/DeUTSide.h"

#  include "GaudiKernel/Plane3DTypes.h"

#  include <string>
#  include <vector>

class DeUTSector;
struct DeUTStation;
class DeUTStave;
class DeUTSide;

/** @class DeUTLayer DeUTLayer.h UTDet/DeUTLayer.h
 *
 *  UT Layer/HalfSize Layer detector element
 *
 *  @author Xuhao Yuan (based on code by Andy Beiter, Jianchun Wang, Matt Needham)
 *  @date   2021-03-29
 *
 */

static const CLID CLID_DeUTLayer = 9303;

class DeUTLayer : public DeUTBaseElement {

public:
  using Sectors = std::vector<const DeUTSector*>;

  /** parent type */
  using parent_type = const DeUTSide;

  /** child type */
  using child_type = const DeUTStave;

  /** children */
  using Children = std::vector<child_type*>;

  /** Constructor */
  using DeUTBaseElement::DeUTBaseElement;

  /**
   * Retrieves reference to class identifier
   * @return the class identifier for this class
   */
  static const CLID& classID() { return CLID_DeUTLayer; }

  /**
   * another reference to class identifier
   * @return the class identifier for this class
   */
  const CLID& clID() const override;

  /** initialization method
   * @return Status of initialisation
   */
  StatusCode initialize() override;

  /** layer identifier
   *  @return identifier
   */
  [[nodiscard]] unsigned int id() const { return m_id; }

  [[nodiscard]] auto version() const { return m_version; }

  /** stereo angle
   *  @return identifier
   */
  [[nodiscard]] double angle() const { return m_angle; }

  /** cosine stereo angle
   *  @return identifier
   */
  [[nodiscard]] double cosAngle() const { return m_cosAngle; }

  /** sine stereo angle
   *  @return identifier
   */
  [[nodiscard]] double sinAngle() const { return m_sinAngle; }

  /** print to stream */
  std::ostream& printOut( std::ostream& os ) const override;

  /** print to stream */
  MsgStream& printOut( MsgStream& os ) const override;

  /**  locate stave based on a channel id
  @return  stave */
  [[nodiscard]] const DeUTStave* findStave( const LHCb::Detector::UT::ChannelID aChannel ) const;

  /** locate stave based on a point
  @return stave */
  [[nodiscard]] const DeUTStave* findStave( const Gaudi::XYZPoint& point ) const;

  /** check whether contains
   *  @param  aChannel channel
   *  @return bool
   **/
  [[nodiscard]] bool contains( const LHCb::Detector::UT::ChannelID aChannel ) const override {
    return elementID().layer() == aChannel.layer() && m_parent->contains( aChannel );
  }

  /// apply given callable to all layers
  void applyToAllSectors( const std::function<void( DeUTSector const& )>& func ) const {
    for ( auto* sector : m_sectors ) { func( *sector ); }
  }

  /// return given sector
  [[nodiscard]] const DeUTSector& sector( unsigned int index ) const { return *m_sectors[index]; }

  /** flat vector of sectors
   * @return vector of sectors
   **/
  const Sectors& sectors() const { return m_sectors; }

  /** plane corresponding to the sector
   * @return the plane
   **/
  [[nodiscard]] const Gaudi::Plane3D& plane() const { return m_plane; }

  /**
   * Nickname for the layer
   **/
  [[nodiscard]] const std::string& nickname() const { return m_nickname; }

  /** vector of children **/
  [[nodiscard]] const Children& staves() const { return m_staves; }

  /**
   * fraction active channels
   * @return bool fraction active
   **/
  [[nodiscard]] double fractionActive() const;

  /** ouput operator for class DeUTLayer
   *  @see DeUTLayer
   *  @see MsgStream
   *  @param os     reference to STL output stream
   *  @param aLayer reference to DeUTLayer object
   */
  friend std::ostream& operator<<( std::ostream& os, const DeUTLayer& aLayer ) { return aLayer.printOut( os ); }

  /** ouput operator for class DeUTLayer
   *  @see DeUTLayer
   *  @see MsgStream
   *  @param os     reference to MsgStream output stream
   *  @param aLayer reference to DeUTLayer object
   */
  friend MsgStream& operator<<( MsgStream& os, const DeUTLayer& aLayer ) { return aLayer.printOut( os ); }

private:
  StatusCode cachePlane();

  Sectors        m_sectors;
  std::string    m_nickname;
  Gaudi::Plane3D m_plane;

  unsigned int m_id       = 0u;
  double       m_angle    = 0.0;
  double       m_sinAngle = 0.0;
  double       m_cosAngle = 0.0;
  GeoVersion   m_version  = GeoVersion::v1;

  /** make flat list of lowest descendents  and also layers **/
  void flatten();

  Children     m_staves;
  parent_type* m_parent = nullptr;
};

[[deprecated( "please deref first" )]] inline std::ostream& operator<<( std::ostream& os, const DeUTLayer* aLayer ) {
  return os << *aLayer;
}
[[deprecated( "please deref first" )]] inline MsgStream& operator<<( MsgStream& os, const DeUTLayer* aLayer ) {
  return os << *aLayer;
}

#endif
