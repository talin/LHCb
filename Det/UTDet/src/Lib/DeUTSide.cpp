/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "UTDet/DeUTSide.h"
#include "DetDesc/IGeometryInfo.h"
#include "Detector/UT/ChannelID.h"
#include "Kernel/UTNames.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTLayer.h"
#include <algorithm>
#include <numeric>

using namespace LHCb;

/** @file DeUTSide.cpp
 *
 *  Implementation of class :  DeUTSide
 *
 *  @author Xuhao Yuan (based on code by Andy Beiter, Jianchun Wang, Matt Needham)
 *  @date   2021-03-29
 *
 */

const CLID& DeUTSide::clID() const { return DeUTSide::classID(); }

StatusCode DeUTSide::initialize() {
  // initialize
  return DeUTBaseElement::initialize().andThen( [&] {
    // and the parent
    if ( exists( "sideID" ) ) { // only sideID stored in new DDDB
      m_id = param<int>( "sideID" );
      setElementID( Detector::UT::ChannelID{Detector::UT::ChannelID::detType::typeUT, id(), 0, 0, 0, 0, 0, 0} );
    } else { // old DDDB only stationID stored
      m_version = GeoVersion::v0;
      m_id      = param<int>( "stationID" );
      setElementID( Detector::UT::ChannelID{Detector::UT::ChannelID::detType::typeUT, m_id - 1, 0, 0, 0, 0, 0, 0} );
    }

    m_nickname = UTNames().SideToString( this->elementID() );
    // get the children
    m_layers = getChildren<DeUTLayer>();
  } );
}

std::ostream& DeUTSide::printOut( std::ostream& os ) const {
  os << " Side/Station: " << m_id << std::endl;
  os << " Nickname: " << m_nickname << std::endl;
  return os;
}

MsgStream& DeUTSide::printOut( MsgStream& os ) const {
  os << " Side : " << m_id << endmsg;
  os << " Nickname: " << m_nickname << endmsg;
  return os;
}

const DeUTLayer* DeUTSide::findLayer( const Detector::UT::ChannelID aChannel ) const {
  auto iter =
      std::find_if( m_layers.begin(), m_layers.end(), [&]( const DeUTLayer* l ) { return l->contains( aChannel ); } );
  return iter != m_layers.end() ? *iter : nullptr;
}

const DeUTLayer* DeUTSide::findLayer( const Gaudi::XYZPoint& point ) const {
  auto iter =
      std::find_if( m_layers.begin(), m_layers.end(), [&]( const DeUTLayer* l ) { return l->isInside( point ); } );
  return iter != m_layers.end() ? *iter : nullptr;
}

double DeUTSide::fractionActive() const {
  return std::accumulate( m_layers.begin(), m_layers.end(), 0.0,
                          [&]( double f, const DeUTLayer* l ) { return f + l->fractionActive(); } ) /
         double( m_layers.size() );
}
