/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "UTDet/DeUTFace.h"
#include "DetDesc/IGeometryInfo.h"
#include "Kernel/UTNames.h"
#include "UTDet/DeUTModule.h"
#include "UTDet/DeUTSector.h"
#include "UTDet/DeUTStave.h"
#include <algorithm>
#include <numeric>

using namespace LHCb;

/** @file DeUTFace.cpp
 *
 *  Implementation of class :  DeUTFace
 *
 *  @author Xuhao Yuan
 *  @date   2021-03-29
 *
 */

const CLID& DeUTFace::clID() const { return DeUTFace::classID(); }

StatusCode DeUTFace::initialize() {
  // initialize method
  StatusCode sc = DeUTBaseElement::initialize();

  if ( sc.isFailure() ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to initialize detector element" << endmsg;
  } else {
    if ( !exists( "faceID" ) ) throw std::runtime_error{"Wrong DDDB used. Check if Left-Rgith split in UT."};
    m_version                              = GeoVersion::v1;
    m_parent                               = getParent<DeUTStave>();
    m_type                                 = m_parent->type();
    m_face                                 = param<int>( "faceID" );
    const Detector::UT::ChannelID parentID = m_parent->elementID();
    m_staveRotZ                            = m_parent->staveRotZ();
    Detector::UT::ChannelID chan( Detector::UT::ChannelID::detType::typeUT, parentID.side(), parentID.layer(),
                                  parentID.stave(), m_face, 0, 0, 0 );
    setElementID( chan );
    m_nickname = UTNames().UniqueFaceToString( this->elementID() );
    m_modules  = getChildren<DeUTModule>();
    for ( auto ptrmodule : m_modules ) {
      const auto& vecptrsectors = ptrmodule->sectors();
      m_sectors.insert( m_sectors.end(), vecptrsectors.begin(), vecptrsectors.end() );
    }
  }

  return sc;
}

std::ostream& DeUTFace::printOut( std::ostream& os ) const {
  return os << " Stave : " /* << name() << " type " << m_type << " Det region " << m_detRegion << " Column " << m_column
                              << " side " << m_sideID << " staveID " << m_staveID*/
            << std::endl;
}

MsgStream& DeUTFace::printOut( MsgStream& os ) const {
  return os << " Stave : " /* << name() << " type " << m_type << " Det region " << m_detRegion << " Column " << m_column
                              << " side " << m_sideID << " staveID " << m_staveID*/
            << std::endl;
}

const DeUTSector* DeUTFace::findSector( const Detector::UT::ChannelID aChannel ) const {
  auto iter = std::find_if( m_sectors.begin(), m_sectors.end(),
                            [&]( const DeUTSector* s ) { return s->contains( aChannel ); } );

  return iter != m_sectors.end() ? *iter : nullptr;
}

const DeUTSector* DeUTFace::findSector( const Gaudi::XYZPoint& point ) const {
  auto iter =
      std::find_if( m_sectors.begin(), m_sectors.end(), [&]( const DeUTSector* s ) { return s->isInside( point ); } );
  return iter != m_sectors.end() ? *iter : nullptr;
}

const DeUTModule* DeUTFace::findModule( const Detector::UT::ChannelID aChannel ) const {
  auto iter = std::find_if( m_modules.begin(), m_modules.end(),
                            [&]( const DeUTModule* s ) { return s->contains( aChannel ); } );

  return iter != m_modules.end() ? *iter : nullptr;
}

const DeUTModule* DeUTFace::findModule( const Gaudi::XYZPoint& point ) const {
  auto iter =
      std::find_if( m_modules.begin(), m_modules.end(), [&]( const DeUTModule* s ) { return s->isInside( point ); } );
  return iter != m_modules.end() ? *iter : nullptr;
}

double DeUTFace::fractionActive() const {
  return std::accumulate( m_sectors.begin(), m_sectors.end(), 0.0,
                          []( double f, const DeUTSector* s ) { return f + s->fractionActive(); } ) /
         double( m_sectors.size() );
}
