/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "UTDet/DeUTDetector.h"
#include "DetDesc/IGeometryInfo.h"
#include "GaudiKernel/GaudiException.h"
#include "Kernel/Trajectory.h"
#include "Kernel/UTNames.h"
#include "UTDet/DeUTLayer.h"
#include "UTDet/DeUTSector.h"
#include "UTDet/DeUTSide.h"
#include "UTDet/DeUTStation.h"
#include "UTDet/DeUTStave.h"
#include <algorithm>
#include <cassert>
#include <numeric>

using namespace LHCb;

namespace {
  /// equal by (nick)name
  inline auto equal_by_name( std::string_view name ) {
    return [name]( const auto& obj ) { return obj->nickname() == name; };
  }
} // namespace

/** @file DeUTDetector.cpp
 *
 *  Implementation of class: DeUTDetector
 *
 *  All UT elements are modification for TT element classes
 *  that were originally written by Matt Needham)
 *
 *  @author Xuhao Yuan (based on code by Andy Beiter, Jianchun Wang, Matt Needham)
 *  @date   2021-03-29
 *
 */

const CLID& DeUTDetector::clID() const { return DeUTDetector::classID(); }

StatusCode DeUTDetector::initialize() {
  // init the base class
  StatusCode sc = DetectorElementPlus::initialize();
  if ( sc.isFailure() ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to initialize detector element" << endmsg;
  } else {
    if ( !exists( "ACsplit" ) ) { m_version = GeoVersion::v0; }
    sc = initializefunc<DeUTSide>( m_sides );
    // make a flat list of sectors
    flatten();
    if ( !m_sectors.empty() ) {
      setNstrip( m_sectors.front()->nStrip() * m_sectors.size() );
      m_sMap.reserve( m_sectors.size() );
      for ( auto i : m_sectors ) { m_sMap.insert( i->elementID().uniqueSector(), i ); }
    }
  }
  return sc;
}

void DeUTDetector::setFirstSide( const unsigned int iSide ) { m_firstSide = iSide; }

int DeUTDetector::sensitiveVolumeID( const Gaudi::XYZPoint& point ) const {
  const DeUTSector* sector = findSector( point );
  if ( !sector ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "sensitiveVolumeID: no sensitive volume at " << point << endmsg;
    return -1;
  }
  return sector->elementID();
}

const DeUTSide* DeUTDetector::findSide( const Detector::UT::ChannelID aChannel ) const {

  // return pointer to the side from channel
  auto iter =
      std::find_if( m_sides.begin(), m_sides.end(), [&]( const DeUTSide* s ) { return s->contains( aChannel ); } );
  return iter != m_sides.end() ? *iter : nullptr;
}

const DeUTSide* DeUTDetector::findSide( const Gaudi::XYZPoint& point ) const {

  // return pointer to the side from point in global system
  auto iter = std::find_if( m_sides.begin(), m_sides.end(), [&]( const DeUTSide* s ) { return s->isInside( point ); } );
  return iter != m_sides.end() ? *iter : nullptr;
}

const DeUTLayer* DeUTDetector::findLayer( const Detector::UT::ChannelID aChannel ) const {
  // return pointer to the layer from channel
  auto iter =
      std::find_if( m_layers.begin(), m_layers.end(), [&]( const DeUTLayer* l ) { return l->contains( aChannel ); } );
  return iter != m_layers.end() ? *iter : nullptr;
}

const DeUTLayer* DeUTDetector::findLayer( const Gaudi::XYZPoint& point ) const {
  // return pointer to the layer from point
  auto iter =
      std::find_if( m_layers.begin(), m_layers.end(), [&]( const DeUTLayer* l ) { return l->isInside( point ); } );
  return iter != m_layers.end() ? *iter : nullptr;
}

const DeUTSector* DeUTDetector::findSector( std::string_view nickname ) const {
  // return pointer to the sector from the nickname
  auto iter = std::find_if( m_sectors.begin(), m_sectors.end(), equal_by_name( nickname ) );
  return iter != m_sectors.end() ? *iter : nullptr;
}

const DeUTLayer* DeUTDetector::findLayer( std::string_view nickname ) const {
  // return pointer to the sector from the nickname
  auto iter = std::find_if( m_layers.begin(), m_layers.end(), equal_by_name( nickname ) );
  return iter != m_layers.end() ? *iter : nullptr;
}

DeUTDetector::Sectors DeUTDetector::disabledSectors() const {
  DeUTDetector::Sectors disabled;
  disabled.reserve( m_sectors.size() );
  applyToAllSectors( [&disabled]( DeUTSector const& utSector ) {
    if ( utSector.sectorStatus() == DeUTSector::ReadoutProblems ) { disabled.push_back( &utSector ); }
  } );
  return disabled;
}

std::vector<LHCb::Detector::UT::ChannelID> DeUTDetector::disabledBeetles() const {

  std::vector<LHCb::Detector::UT::ChannelID> disabledBeetles;
  applyToAllSectors( [&disabledBeetles]( DeUTSector const& s ) {
    auto bStatus = s.beetleStatus();
    for ( unsigned int i = 0; i < bStatus.size(); ++i ) {
      if ( bStatus[i] == DeUTSector::ReadoutProblems ) {
        const unsigned int firstStripOnBeetle = ( i * LHCbConstants::nStripsInBeetle ) + 1;
        disabledBeetles.push_back( s.stripToChan( firstStripOnBeetle ) );
      }
    } // i
  } );
  return disabledBeetles;
}

LHCb::LineTraj<double> DeUTDetector::trajectory( LHCb::LHCbID id, double offset ) const {
  // look up the trajectory
  if ( !id.isUT() ) {
    throw GaudiException( "The LHCbID is not of UT type!", "DeUTDetector.cpp", StatusCode::FAILURE );
  }
  const DeUTSector* aSector = findSector( id.utID() );
  if ( !aSector ) { throw GaudiException( "Failed to find sector", "DeUTDetector.cpp", StatusCode::FAILURE ); }
  return aSector->trajectory( id.utID(), offset );
}

double DeUTDetector::fractionActive() const {
  return std::accumulate( m_sectors.begin(), m_sectors.end(), 0.0,
                          [&]( double fA, const DeUTSector* s ) { return fA + s->fractionActive(); } ) /
         double( m_sectors.size() );
}

const DeUTSector* DeUTDetector::findSector( const Gaudi::XYZPoint& aPoint ) const {
  const DeUTSector* aSector = nullptr;
  const DeUTSide*   tSide   = findSide( aPoint );
  if ( tSide ) {
    const DeUTSide*  aSide  = dynamic_cast<const DeUTSide*>( tSide );
    const DeUTLayer* aLayer = aSide->findLayer( aPoint );
    if ( aLayer ) {
      const DeUTStave* aStave = aLayer->findStave( aPoint );
      if ( aStave ) { aSector = aStave->findSector( aPoint ); }
    }
  }
  return aSector;
}

void DeUTDetector::flatten() {
  m_sectors.reserve( 1200 );
  m_sectorsAllen.reserve( 1200 );

  for ( auto ptrs : m_sides ) {
    for ( auto ptrlayer : ptrs->layers() ) {
      m_layers.push_back( ptrlayer );
      ptrlayer->applyToAllSectors( [this]( DeUTSector const& utSector ) {
        m_sectors.push_back( &utSector );
        m_sectorsAllen.push_back( &utSector );
      } );
    }
  }
  if ( m_version == GeoVersion::v0 ) { // for old geo, sectors need to be sorted by new ids
    std::stable_sort( m_sectors.begin(), m_sectors.end(), [&]( const DeUTSector* p1, const DeUTSector* p2 ) {
      return p1->elementID().channelID() < p2->elementID().channelID();
    } );
  }
}

const DeUTSector* DeUTDetector::getSector( Side_t   side,   //
                                           Layer_t  layer,  //
                                           Stave_t  stave,  //
                                           Face_t   face,   //
                                           Module_t module, //
                                           Sector_t sector ) const {

  const DeUTSector* res = nullptr;

  if ( (unsigned int)side < NBSIDE && (unsigned int)layer < NBHALFLAYER && (unsigned int)stave < NBSTAVE &&
       (unsigned int)face < NBFACE && (unsigned int)module < NBMODULE && (unsigned int)sector < NBSUBSECTOR ) {
    const unsigned int id_Layer =
        (unsigned int)side * NTOTSECTOR / 2 + ( ( (unsigned int)layer < 2 )
                                                    ? (unsigned int)layer * NSECTSTA
                                                    : ( 2 * NSECTSTA + ( (unsigned int)layer - 2 ) * NSECTSTB ) );
    const unsigned int id_Stave = ( ( (unsigned int)stave > 0 ) ? NSECTSTVC : 0 ) +
                                  ( ( (unsigned int)stave > 1 ) ? NSECTSTVB : 0 ) +
                                  ( ( (unsigned int)stave > 2 ) ? ( (unsigned int)stave - 2 ) * NSECTSTVA : 0 );
    const unsigned int id_Face = ( ( (unsigned int)stave == 0 ) ? (unsigned int)face * ( NSECTSTVC / 2 ) : 0 ) +
                                 ( ( (unsigned int)stave == 1 ) ? ( (unsigned int)face ) * ( NSECTSTVB / 2 ) : 0 ) +
                                 ( ( (unsigned int)stave > 1 ) ? ( (unsigned int)face ) * ( NSECTSTVA / 2 ) : 0 );
    const unsigned int id_Module = 2 * (unsigned int)module + (unsigned int)sector;
    unsigned int       id_Sector;
    if ( (unsigned int)face == 0 ) {
      if ( (unsigned int)stave == 0 )
        id_Sector = STCBmap( id_Module );
      else if ( (unsigned int)stave == 1 )
        id_Sector = STBBmap( id_Module );
      else
        id_Sector = STABmap( id_Module );
    } else {
      if ( (unsigned int)stave == 0 )
        id_Sector = STCFmap( id_Module );
      else if ( (unsigned int)stave == 1 )
        id_Sector = STBFmap( id_Module );
      else
        id_Sector = STAFmap( id_Module );
    }
    const auto i = id_Sector + id_Face + id_Stave + id_Layer;
    res          = ( i < m_sectors.size() ? m_sectors[i] : nullptr );
  }

  return res;
}

void DeUTDetector::applyToSectors( const std::vector<LHCb::Detector::UT::ChannelID>& vec,
                                   const std::function<void( DeUTSector const& )>&   func ) const {
  DeUTDetector::Sectors sectors;
  sectors.reserve( vec.size() );
  for ( auto i : vec ) {
    auto aSector = findSector( i );
    if ( aSector ) { sectors.push_back( aSector ); }
  }
  // ensure unique list
  sectors.erase( std::unique( sectors.begin(), sectors.end() ), sectors.end() );
  for ( auto* s : sectors ) func( *s );
}
