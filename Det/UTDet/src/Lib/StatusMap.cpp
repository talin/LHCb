/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "UTDet/StatusMap.h"

namespace {
  static const Status::StatusVector s_vec_bs = {DeUTSector::OK, DeUTSector::ReadoutProblems, DeUTSector::Dead,
                                                DeUTSector::OtherFault};
  static const Status::StatusVector s_vec_ps = {DeUTSector::NotBonded};
} // namespace

const Status::StatusVector& Status::validBeetleStates() { return s_vec_bs; }

const Status::StatusVector& Status::protectedStates() { return s_vec_ps; }
