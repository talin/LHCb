/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "UTDet/DeUTLayer.h"
#include "DetDesc/IGeometryInfo.h"
#include "DetDesc/SolidBox.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/UTNames.h"
#include "UTDet/DeUTSide.h"
#include "UTDet/DeUTStave.h"
#include <algorithm>
#include <numeric>

using namespace LHCb;

/** @file DeUTLayer.cpp
 *
 *  Implementation of class :  DeUTLayer
 *
 *  @author Xuhao Yuan (based on code by Andy Beiter, Jianchun Wang, Matt Needham)
 *  @date   2021-03-30
 *
 */

const CLID& DeUTLayer::clID() const { return DeUTLayer::classID(); }

StatusCode DeUTLayer::initialize() {
  // initialize method
  return DeUTBaseElement::initialize()
      .andThen( [&] {
        m_id       = param<int>( "layerID" );
        m_angle    = param<double>( "stereoangle" );
        m_cosAngle = cos( m_angle );
        m_sinAngle = sin( m_angle );
      } )
      .andThen( [&] {
        // cache trajectories
        return registerCondition( this, this->geometry(), &DeUTLayer::cachePlane ).orElse( [&] {
          MsgStream{msgSvc(), name()} << MSG::ERROR << "Failed to register conditions" << endmsg;
        } );
      } )
      .andThen( [&] {
        m_parent  = getParent<DeUTSide>();
        m_version = m_parent->version();
        m_id      = m_id % 4;
        if ( m_version == GeoVersion::v0 ) {
          setElementID( Detector::UT::ChannelID{Detector::UT::ChannelID::detType::typeUT, m_parent->id() - 1,
                                                2 * ( m_parent->id() - 1 ) + id() - 1, 0, 0, 0, 0, 0} );
        } // setup old layer with new ID
        else {
          setElementID(
              Detector::UT::ChannelID{Detector::UT::ChannelID::detType::typeUT, m_parent->id(), id(), 0, 0, 0, 0, 0} );
        }
        m_nickname = UTNames::UniqueLayerToString( this->elementID() );
        m_staves   = getChildren<DeUTStave>();
        flatten();
      } );
}

std::ostream& DeUTLayer::printOut( std::ostream& os ) const {
  os << " Layer: " << m_id << std::endl;
  os << "stereo angle " << m_angle << std::endl << " Nickname: " << m_nickname << std::endl;
  return os;
}

MsgStream& DeUTLayer::printOut( MsgStream& os ) const {
  os << " Layer : " << m_id << endmsg;
  os << "stereo angle " << m_angle << " Nickname: " << m_nickname << endmsg;
  return os;
}

StatusCode DeUTLayer::cachePlane() {
  Gaudi::XYZPoint p1 = globalPoint( 0, 0, 0 );
  Gaudi::XYZPoint p2 = globalPoint( 3 * Gaudi::Units::m, 0, 0 );
  Gaudi::XYZPoint p3 = globalPoint( 0, 3 * Gaudi::Units::cm, 0 );

  m_plane = Gaudi::Plane3D( p1, p2, p3 );

  return StatusCode::SUCCESS;
}

const DeUTStave* DeUTLayer::findStave( const Detector::UT::ChannelID aChannel ) const {
  auto iter =
      std::find_if( m_staves.begin(), m_staves.end(), [&]( const DeUTStave* m ) { return m->contains( aChannel ); } );
  return iter != m_staves.end() ? *iter : nullptr;
}

const DeUTStave* DeUTLayer::findStave( const Gaudi::XYZPoint& point ) const {
  auto iter =
      std::find_if( m_staves.begin(), m_staves.end(), [&]( const DeUTStave* m ) { return m->isInside( point ); } );
  return iter != m_staves.end() ? *iter : nullptr;
}

void DeUTLayer::flatten() {
  for ( auto* tStave : staves() ) {
    const auto& sectors = tStave->sectors();
    std::copy( sectors.begin(), sectors.end(), std::back_inserter( m_sectors ) );
  }
}

double DeUTLayer::fractionActive() const {
  return std::accumulate( m_staves.begin(), m_staves.end(), 0.0,
                          []( double f, const DeUTStave* m ) { return f + m->fractionActive(); } ) /
         double( m_staves.size() );
}
