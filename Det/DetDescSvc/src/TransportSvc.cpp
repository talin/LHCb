/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "TransportSvc.h"

#include "DetDesc/IDetectorElement.h"
#include "DetDesc/IGeometryInfo.h"
#include "DetDesc/IntersectionErrors.h"
#include "DetDesc/VolumeIntersectionIntervals.h"

#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/SmartDataPtr.h"

#include "boost/format.hpp"

#include <sstream>

/**
 *  implementation of class TransportSvc
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
namespace {
  inline std::string logVol( const std::string& name, const unsigned int len,
                             const std::string& prefix = "/dd/Geometry/" ) {
    auto pos = name.find( prefix );
    if ( 0 == pos ) { return logVol( {name, pos + prefix.size()}, len, prefix ); }

    return len < name.size() ? std::string{name, name.size() - len} : name;
  }
} // namespace

StatusCode TransportSvc::initialize() {
  /// initialise the base class
  StatusCode statusCode = Service::initialize();
  if ( statusCode.isFailure() ) return statusCode; // Propagate the error

  /// Locate Detector Data  Service:
  m_detDataSvc = serviceLocator()->service( m_detDataSvc_name, true );
  if ( !m_detDataSvc ) {
    fatal() << " Unable to locate  Detector Data Service=" << m_detDataSvc_name << endmsg;
    return StatusCode::FAILURE;
  }

  if ( msgLevel( MSG::DEBUG ) && !m_protocol ) {
    warning() << "Protocol property is always 'true' when in DEBUG" << endmsg;
    m_protocol = true;
  }

  if ( !DetDesc::IntersectionErrors::service() ) {
    info() << "Initialize the static pointer to DetDesc::IGeometryErrorSvc" << endmsg;
    DetDesc::IntersectionErrors::setService( this );
  }

  if ( !m_protocol ) {
    warning() << "The protocol of geometry errors is DISABLED" << endmsg;
    DetDesc::IntersectionErrors::setService( 0 );
  }

  // recover errors ?
  DetDesc::IntersectionErrors::setRecovery( m_recovery );
  if ( DetDesc::IntersectionErrors::recovery() ) {
    info() << "Recovery of geometry errors is  ENABLED" << endmsg;
  } else {
    info() << "Recovery of geometry errors is DISABLED" << endmsg;
  }
  return StatusCode::SUCCESS;
}

StatusCode TransportSvc::finalize() {
  //

  { // skip map
    always() << " GEOMETRY ERRORS: 'Skip'     map has the size " << m_skip.size() << std::endl;
    if ( !m_skip.empty() ) {
      boost::format fmt1( "   | %1$=55.55s |    | %2$-65.65s |" );
      fmt1 % " Logical Volume ";
      fmt1 % " #          mean             RMS          min              max";
      msgStream() << fmt1.str() << std::endl;
      for ( auto i = m_skip.begin(); m_skip.end() != i; ++i ) {
        {
          std::ostringstream ost;
          i->second.first.printFormatted( ost, "%1$-7d %3$-14.8g %4$14.8g %5$-14.8g %6$14.8g" );
          boost::format fmt2( "   | %1$-55.55s | mm | %2$-65.65s |" );
          fmt2 % logVol( i->first, 55 ) % ost.str();
          msgStream() << fmt2.str() << std::endl;
        }
        {
          std::ostringstream ost;
          i->second.second.printFormatted( ost, "%1$-7d %3$-14.8g %4$14.8g %5$-14.8g %6$14.8g" );
          boost::format fmt2( "   | %1$-55.55s | X0 | %2$-65.65s |" );
          fmt2 % logVol( i->first, 55 ) % ost.str();
          msgStream() << fmt2.str() << std::endl;
        }
      }
    }
  }
  msgStream() << endmsg;

  // recovery map
  {
    always() << " GEOMETRY ERRORS: 'Recover'  map has the size " << m_recover.size() << std::endl;
    if ( !m_recover.empty() ) {
      boost::format fmt1( "   | %1$=55.55s |    | %2$-65.65s |" );
      fmt1 % " Logical Volume ";
      fmt1 % " #          mean             RMS          min              max";
      msgStream() << fmt1.str() << std::endl;
      for ( Map::const_iterator i = m_recover.begin(); m_recover.end() != i; ++i ) {
        {
          std::ostringstream ost;
          i->second.first.printFormatted( ost, "%1$-7d %3$-14.8g %4$14.8g %5$-14.8g %6$14.8g" );
          boost::format fmt2( "   | %1$-55.55s | mm | %2$-65.65s |" );
          fmt2 % logVol( i->first, 55 ) % ost.str();
          msgStream() << fmt2.str() << std::endl;
        }
        {
          std::ostringstream ost;
          i->second.second.printFormatted( ost, "%1$-7d %3$-14.8g %4$14.8g %5$-14.8g %6$14.8g" );
          boost::format fmt2( "   | %1$-55.55s | X0 | %2$-65.65s |" );
          fmt2 % logVol( i->first, 55 ) % ost.str();
          msgStream() << fmt2.str() << std::endl;
        }
      }
    }
  }
  msgStream() << endmsg;

  // codes:
  {
    always() << " GEOMETRY ERRORS: 'Codes'    map has the size " << m_codes.size() << std::endl;
    if ( !m_codes.empty() ) {
      boost::format fmt1( "   | %1$=55.55s | %2$=10.10s |" );
      fmt1 % " Logical Volume ";
      fmt1 % "#errors";
      msgStream() << fmt1.str() << std::endl;
      for ( Map1::const_iterator i = m_codes.begin(); m_codes.end() != i; ++i ) {
        {
          boost::format fmt2( "   | %1$-55.55s | %2$=10d |" );
          fmt2 % logVol( i->first, 55 );
          fmt2 % i->second;
          msgStream() << fmt2.str() << std::endl;
        }
      }
    }
  }
  msgStream() << endmsg;

  if ( this == DetDesc::IntersectionErrors::service() ) {
    info() << "Reset the static pointer to DetDesc::IGeometyrErrorSvc" << endmsg;
    DetDesc::IntersectionErrors::setService( 0 );
  }
  ///
  { // uncatched errors ?
    const unsigned long nErrors = DetDesc::IntersectionErrors::errors();
    if ( 0 != nErrors ) {
      error() << "DetDesc::IntersectionErrors has " << nErrors << " errors " << endmsg;
      error() << "Rerun the job with activated error handling!" << endmsg;
    }
  }
  ///
  {
    /// release all services
    /// release Detector Data  Service
    m_detDataSvc.reset();
  }
  ///
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Service finalised successfully" << endmsg;
  ///
  return Service::finalize();
  ///
}

// ============================================================================
// Implementations of additional methods
// ============================================================================

/** Estimate the distance between 2 points in
 *  units of radiation length units
 *  Similar to distanceInRadUnits but with an additional accelerator
 *  cache for local client storage. This method, unlike distanceInRadUnits
 *  is re-entrant and thus thread safe.
 *  @see ITransportSvc
 *  @param point1 first point
 *  @param point2 second point
 *  @param threshold threshold value
 *  @param geometry the geometry to be used
 *  @param geometryGuess guess for geometry
 *  @return distance in rad length units
 */
double TransportSvc::distanceInRadUnits( const Gaudi::XYZPoint& point1, const Gaudi::XYZPoint& point2,
                                         std::any& accelCache, IGeometryInfo const& geometry, double threshold,
                                         IGeometryInfo* geometryGuess ) const {
  // check for the  distance
  if ( point1 == point2 ) { return 0; }

  // retrieve the history
  const Gaudi::XYZVector Vector( point2 - point1 );

  // initial point on the line
  // direction vector of the line
  // minimal value of the parameter of the line
  // maximal value of the parameter of the line
  // (output) container of intersections
  // threshold value
  // source of the alternative geometry information
  // a guess for navigation
  Intersections local_intersects =
      intersections( point1, Vector, 0.0, 1.0, accelCache, geometry, threshold, geometryGuess );
  //  radiation length in tick units
  const auto RadLength = std::accumulate( local_intersects.begin(), local_intersects.end(), 0.0,
                                          VolumeIntersectionIntervals::AccumulateIntersections() );
  // scale
  const auto TickLength = std::sqrt( Vector.mag2() );
  return RadLength * TickLength;
}

/**
 * simple implementation of TransportSvc::findLocalGI method
 *
 * @author: Vanya Belyaev
 */
IGeometryInfoPlus const* TransportSvc::findLocalGI( const Gaudi::XYZPoint& point1, const Gaudi::XYZPoint& point2,
                                                    IGeometryInfo const* gi, IGeometryInfo const& topGi ) const {
  if ( !gi ) { return nullptr; }

  /// output :-))
  IGeometryInfoPlus const* goodGI = nullptr;

  try {
    /// find the nearest "upper" volume, which contains the  first point
    auto gip = static_cast<IGeometryInfoPlus const*>( gi );
    if ( !gip->isInside( point1 ) ) { return nullptr; }
    IGeometryInfoPlus const* gi1 = gip->belongsTo( point1, -1 );

    /// find the nearest "upper" volume, which contains the second point
    IGeometryInfoPlus const* gi2 = gi1;
    {
      bool loc = false;
      for ( loc = gi2->isInside( point2 ); !loc && nullptr != gi2; gi2 = gi2->parentIGeometryInfoPlus() ) {
        loc = gi2->isInside( point2 );
        if ( loc ) break;
      }
      if ( nullptr == gi2 ) { return nullptr; }
    }

    // Here both points are located, gi2 is a parent of gi1. Get the first
    // parent of gi2 which is a good GI.

    for ( IGeometryInfoPlus const* gl = gi2; nullptr != gl; gl = gl->parentIGeometryInfoPlus() ) {
      if ( nullptr == gl ) return nullptr;
      if ( goodLocalGI( point1, point2, gl ) ) { return gl; }
      if ( &topGi == gl ) return nullptr;
    }
    /// we have failed to find "good" element
    return nullptr; /// RETURN !!!

  } catch ( const GaudiException& Exception ) {
    /// throw new exception:
    std::string message( "TransportSvc::findLocalGI(...), exception caught; Params: " );
    {
      std::ostringstream ost;
      ost << "Point1=" << point1 << "Point2=" << point2;
      message += ost.str();
      Assert( false, message, Exception );
    }
  } catch ( ... ) {
    /// throw new exception:
    std::string message( "TransportSvc::findLocalGI(...), unknown exception caught; Params: " );
    {
      std::ostringstream ost;
      ost << "Point1=" << point1 << "Point2=" << point2;
      message += ost.str();
      Assert( false, message );
    }
  }

  ///
  return goodGI;
}

/**
 * simple implementation of TransportSvc::goodLocalGI method
 *
 * @attention mandatory that at least one point is INSIDE volume!
 */
inline bool TransportSvc::goodLocalGI( const Gaudi::XYZPoint& point1, const Gaudi::XYZPoint& point2,
                                       IGeometryInfo const* gi ) const {
  if ( !gi ) { return false; }
  auto        gip = static_cast<IGeometryInfoPlus const*>( gi );
  const auto* lv  = gip->lvolume();
  if ( !lv ) { return false; }
  if ( lv->isAssembly() ) { return false; }
  ISolid::Tick  tickMin( 0 ), tickMax( 1 );
  ISolid::Ticks local_ticks;
  auto          nInt = lv->solid()->intersectionTicks( gip->toLocalMatrix() * point1,
                                              gip->toLocalMatrix() * Gaudi::XYZVector( point2 - point1 ), tickMin,
                                              tickMax, local_ticks );
  return ( 2 == nInt && tickMin == local_ticks.front() && tickMax == local_ticks.back() );
}

ILVolume::Intersections TransportSvc::intersections( const Gaudi::XYZPoint& point, const Gaudi::XYZVector& vect,
                                                     const double tickMin, const double tickMax, std::any& accelCache,
                                                     IGeometryInfo const& geometry, double threshold,
                                                     IGeometryInfo* guessGeometry ) const {
  ILVolume::Intersections intersept;
  try {
    /// check the input parameters of the line
    if ( tickMin >= tickMax && vect.mag2() <= 0 ) { return intersept; }
    // get the cache
    auto& cache = std::any_cast<AccelCache&>( accelCache );

    Gaudi::XYZPoint point1( point + vect * tickMin );
    Gaudi::XYZPoint point2( point + vect * tickMax );
    // check - if the previous paramaters are the same
    if ( point1 == cache.prevPoint1 && point2 == cache.prevPoint2 && threshold == cache.previousThreshold &&
         &geometry == cache.previousTopGeometry && guessGeometry == cache.previousGuess ) {
      /// use cached container!!!
      intersept.reserve( cache.localIntersections.size() );
      intersept.insert( intersept.begin(), cache.localIntersections.begin(), cache.localIntersections.end() );
      ///
      return intersept;
    }

    /** locate the both points inside one "good"
     *  DetectorElement/GeometryInfo objects
     *
     * "Good" in this context means that
     *
     *  - 1) both points should be "inside"
     *  - 2) it has the associated Logical Volume (it is not "ghost")
     *  - 3) Logical Volume is not an assembly!
     *  - 4) the line between tickMin and tickMax do not cross
     *    the Logical Volume ("no holes between") requirement
     */

    IGeometryInfoPlus const* giLocal = nullptr;
    IGeometryInfoPlus const* gi =
        // try the guess geometry
        ( guessGeometry && ( giLocal = findLocalGI( point1, point2, guessGeometry, geometry ) ) )
            ? static_cast<IGeometryInfoPlus const*>( guessGeometry )
            :
            // try the previous geometry (only if top-geometry matches)
            ( cache.previousGeometry && &geometry == cache.previousTopGeometry &&
              ( giLocal = findLocalGI( point1, point2, cache.previousGeometry, geometry ) ) )
                ?
                // just take the top geometry
                static_cast<IGeometryInfoPlus const*>( cache.previousGeometry )
                : ( giLocal = findLocalGI( point1, point2, &geometry, geometry ) )
                      ?

                      static_cast<IGeometryInfoPlus const*>( &geometry )
                      : nullptr;

    ///
    if ( !giLocal ) throw TransportSvcException( "TransportSvc::(1) unable to locate points", StatusCode::FAILURE );
    if ( !gi ) throw TransportSvcException( "TransportSvc::(2) unable to locate points", StatusCode::FAILURE );

    /// delegate the calculation to the logical volume
    const auto* lv = giLocal->lvolume();
    lv->intersectLine( giLocal->toLocalMatrix() * point, giLocal->toLocalMatrix() * vect, intersept, tickMin, tickMax,
                       threshold );

    /// redefine previous geometry
    cache.previousGeometry = giLocal;

    /// make local copy of all parameters:
    cache.prevPoint1          = point1;
    cache.prevPoint2          = point2;
    cache.previousThreshold   = threshold;
    cache.previousGuess       = guessGeometry;
    cache.previousTopGeometry = &geometry;

    /// intersections
    cache.localIntersections.clear();
    cache.localIntersections.reserve( intersept.size() );
    cache.localIntersections.insert( cache.localIntersections.begin(), intersept.begin(), intersept.end() );

  }

  catch ( const GaudiException& Exception ) {
    /// 1) reset cache:
    auto& cache               = std::any_cast<AccelCache&>( accelCache );
    cache.prevPoint1          = Gaudi::XYZPoint();
    cache.prevPoint2          = Gaudi::XYZPoint();
    cache.previousThreshold   = -1000.0;
    cache.previousGuess       = 0;
    cache.previousTopGeometry = 0;
    cache.localIntersections.clear();

    ///  2) throw new exception:
    std::string message( "TransportSvc::intersection(...), exception caught; Params: " );
    {
      std::ostringstream ost;
      ost << "Point=" << point << ",Vect=" << vect << ",Tick=" << tickMin << "/" << tickMax << "Thrsh=" << threshold;
      if ( guessGeometry ) { ost << "G.Geo!=NULL"; }
      message += ost.str();
      Assert( false, message, Exception );
    }
  } catch ( ... ) {
    /// 1) reset cache:
    auto& cache               = std::any_cast<AccelCache&>( accelCache );
    cache.prevPoint1          = Gaudi::XYZPoint();
    cache.prevPoint2          = Gaudi::XYZPoint();
    cache.previousThreshold   = -1000.0;
    cache.previousGuess       = 0;
    cache.previousTopGeometry = 0;
    cache.localIntersections.clear();

    /// 2) throw new exception:
    std::string message( "TransportSvc::intersection(...), unknown exception caught; Params: " );
    {
      std::ostringstream ost;
      ost << "Point=" << point << ",Vect=" << vect << ",Tick=" << tickMin << "/" << tickMax << "Thrsh=" << threshold;
      if ( guessGeometry ) { ost << "G.Geo!=NULL"; }
      message += ost.str();
      Assert( false, message );
    }
  }
  return intersept;
}

// Create an instance of the accelerator cache
std::any TransportSvc::createCache() const { return AccelCache{}; }

// ============================================================================
// Implementation of interface DetDesc::IGeometryErrorSvc
// ============================================================================
/*  set/reset the current "status" of geometry erorrs
 *  @param sc the status code
 *  @param volume the volume
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-12-14
 */
void TransportSvc::setCode( const StatusCode& sc, const ILVolume* volume ) {
  if ( sc.isSuccess() ) { return; }                            // nothing to do
  if ( !m_protocol && MSG::DEBUG > outputLevel() ) { return; } //

  if ( volume ) {
    ++m_codes[volume->name()];
  } else {
    ++m_codes[""];
  }
}

/*  inspect the potential error in intersections
 *  @param  volume   the problematic volume
 *  @param  point    3D point
 *  @param  vect     3D direction vector
 *  @param  cnt  the problematic container of intersections
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-12-14
 */
void TransportSvc::inspect( const ILVolume* volume, const Gaudi::XYZPoint& pnt, const Gaudi::XYZVector& vect,
                            const ILVolume::Intersections& cnt ) {

  error() << " DetDesc::IntersectionError for LV='" << volume->name() << "'" << endmsg;
  error() << " Local Frame: "
          << " point=" << pnt << "/vector=" << vect << " Tick Length=" << vect.R() << endmsg;

  boost::format fmt1( "| %1$1.1s |%2$=15.15s %3$=15.15s|%4$=27.27s %5$=27.27s|%6$=3s %7$=16.16s | %8$=16.16s |" );
  fmt1 % "?"; // problems ?
  fmt1 % "tickMin";
  fmt1 % "tickMax";
  fmt1 % "point1";
  fmt1 % "point2";
  fmt1 % "#";
  fmt1 % "phys.volume";
  fmt1 % "material";
  error() << fmt1.str() << endmsg;

  for ( auto i = cnt.begin(); cnt.end() != i; ++i ) {
    // check the intersections:
    bool ok = true;
    for ( auto i2 = cnt.begin(); ok && cnt.end() != i2; ++i2 ) {
      if ( i2 == i ) { continue; }
      ok = ( 0 != VolumeIntersectionIntervals::intersect( *i, *i2 ) );
    }
    boost::format fmt( "| %1$1.1s |%2$15.9g %3$-15.9g|%4$27.27s %5$-27.27s|%6$=3d %7$16.16s | %8$16.16s |" );
    fmt % ( ok ? " " : "*" );               // (1) problems?
    fmt % i->first.first;                   // (2) tick min
    fmt % i->first.second;                  // (3) tick max
    fmt % ( pnt + i->first.first * vect );  // (4) the first point
    fmt % ( pnt + i->first.second * vect ); // (5) the second point

    // get the central point:
    Gaudi::XYZPoint       c = pnt + 0.5 * ( i->first.first + i->first.second ) * vect;
    ILVolume::PVolumePath pvs;
    volume->belongsTo( c, 1, pvs ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    if ( pvs.empty() || 0 == pvs.front() ) {
      fmt % -1;
      fmt % "<mother>";
    } else {
      const ILVolume::PVolumes&          volumes = volume->pvolumes();
      ILVolume::PVolumes::const_iterator ipv     = std::find( volumes.begin(), volumes.end(), pvs.front() );
      if ( volumes.end() == ipv ) {
        fmt % -1;
      } else {
        fmt % ( ipv - volumes.begin() );
      }
      fmt % pvs.front()->name();
    }
    const std::string& n = i->second->name();
    if ( 16 >= n.size() ) {
      fmt % n;
    } // (6) the material name
    else {
      fmt % std::string( n, n.size() - 16 );
    } // (6) the material name

    //
    error() << fmt.str() << endmsg;
  }
}

/* report the recovered action in intersections
 *  @param  volume    the problematic volume
 *  @param  material1 the affected material
 *  @param  material2 the affected material
 *  @param  delta    the problematic delta  (non-negative!)
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-12-14
 */
void TransportSvc::recovered( const ILVolume* volume, const Material* material1, const Material* material2,
                              const double delta ) {
  if ( !m_protocol && MSG::DEBUG > outputLevel() ) { return; } // RETURN

  Map::mapped_type& p = m_recover[volume->name()];

  p.first += delta;
  p.second += std::abs( delta / material1->radiationLength() - delta / material2->radiationLength() );
}

/* report the skipped intersection
 *  @param  volume   the problematic volume
 *  @aram   material the affected material
 *  @param  delta    the problematic delta  (non-negative!)
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-12-14
 */
void TransportSvc::skip( const ILVolume* volume, const Material* material, const double delta ) {
  if ( !m_protocol && MSG::DEBUG > outputLevel() ) { return; } // RETURN

  Map::mapped_type& p = m_skip[volume->name()];

  p.first += delta;
  p.second += delta / material->radiationLength();
}

DECLARE_COMPONENT( TransportSvc )
