###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

import os

# Prepare detector description
##############################
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
dd4hepSvc = DD4hepSvc(
    GeometryLocation="${TEST_DBS_ROOT}/DD4TESTCOND/geo.xml",
    GeometryVersion='',
    GeometryMain='',
    ConditionsLocation="file://${TEST_DBS_ROOT}/DD4TESTCOND/",
    DetectorList=["/world"])

# Main application
##################
app = ApplicationMgr(EvtSel="NONE", EvtMax=3, OutputLevel=INFO)

# Configure fake run number
###########################
from Configurables import LHCb__Tests__FakeRunNumberProducer as FET
from Configurables import LHCb__DetCond__Examples__Functional__CondAccessExample as CondAlg
from Configurables import LHCb__DetCond__Examples__Functional__CondAccessExampleWithDerivation as CondAlgDerived
from Configurables import LHCb__DetCond__Examples__Functional__CondAccessExampleWithDerivationSIMD as CondAlgDerivedSIMD
# Add the ReserveIOVDD4hep which creates a fake ODIN bank from the location specified
from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer

odin_path = '/Event/DummyODIN'
app.TopAlg = [
    FET('FakeRunNumber', ODIN=odin_path, Start=42, Step=20),
    IOVProducer('ReserveIOVDD4hep', ODIN=odin_path),
    CondAlg('CondAlg'),
    CondAlgDerived('CondAlgDerived'),
    CondAlgDerivedSIMD('CondAlgDerivedSIMD')
]
