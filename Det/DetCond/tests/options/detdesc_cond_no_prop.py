###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

import os

# Prepare detector description
##############################

from Configurables import CondDB, DDDBConf, GitEntityResolver
GitEntityResolver(
    'GitDDDB',
    PathToRepository=os.path.join(os.environ.get('TEST_DBS_ROOT'), 'TESTCOND'))
DDDBConf(DataType='2016', DbRoot='git:/lhcb.xml', EnableRunStampCheck=False)
CondDB(Tags={'DDDB': ''})


@appendPostConfigAction
def reduce_resolver():
    '''override some settings from DDDBConf'''
    from Configurables import XmlParserSvc, ApplicationMgr
    resolvers = XmlParserSvc().EntityResolver.EntityResolvers
    resolvers[:] = [
        r for r in resolvers if r.name()[8:15] in ('GitDDDB', 'GitOver')
    ]
    appMgr = ApplicationMgr()
    appMgr.ExtSvc = [
        svc for svc in appMgr.ExtSvc if "MagneticFieldSvc" not in str(svc)
    ]


# Configure fake event time
###########################
from Configurables import EventClockSvc, FakeEventTime
ecs = EventClockSvc()
ecs.addTool(FakeEventTime, 'EventTimeDecoder')
# tuned from the content of condition in TESTCOND
ecs.EventTimeDecoder.StartTime = 1442403000000000000
ecs.EventTimeDecoder.TimeStep = 18399600000000000

# Configure algorithms
######################
from Configurables import LHCb__DetCond__Examples__Functional__CondAccessExampleYamlNoProp as CondAlg

# make sure that we do not have a condition property
alg = CondAlg('CondAlg')
assert ("TestCondition" not in alg.getDefaultProperties().values()
        and "TestCondition" not in alg.getProperties().values())

app = ApplicationMgr(EvtSel="NONE", EvtMax=3, OutputLevel=INFO)
app.TopAlg += [alg]
