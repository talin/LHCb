/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#pragma once

#ifdef USE_DD4HEP

#  include "Detector/Muon/DeMuon.h"
using DeMuonDetector = LHCb::Detector::DeMuon;

#else

#  include "MuonDet/DeMuonChamber.h"
#  include "MuonDet/MuonDAQHelper.h"
#  include "MuonDet/MuonFrontEndID.h"
#  include "MuonDet/MuonL1Board.h"
#  include "MuonDet/MuonNamespace.h"
#  include "MuonDet/MuonODEBoard.h"
#  include "MuonDet/MuonReadoutCond.h"
#  include "MuonDet/MuonStationCabling.h"
#  include <tuple>

#  include "DetDesc/DetectorElement.h"
#  include "DetDesc/SolidBox.h"
#  include "MuonDet/MuonNamespace.h"
#  include "MuonDet/MuonTSMap.h"
#  include "MuonDet/MuonUpgradeDAQHelper.h"
#  include "MuonDet/PackMCMuonHit.h"

#  include "Kernel/Packer.h"

#  include <exception>
#  include <memory>
#  include <optional>
#  include <tuple>

class MuonChamberLayout;
class IDataProviderSvc;

static const CLID CLID_DEMuonDetector = 11009;

/**
 *  Detector element class for the muon system
 *
 *  @author Alessia Satta
 *  @author Alessio Sarti
 *  @date   18/10/2005
 */
class DeMuonDetector : public DetDesc::DetectorElementPlus {

public:
  DeMuonDetector() = default;

  /// object identification
  const CLID& clID() const override;

  /// object identification
  static const CLID& classID() { return CLID_DEMuonDetector; }

  // Initialize of Detector Element
  StatusCode initialize() override;

  // returns -1 in case of failure
  int regNum( const double x, const double y, int station ) const;

  // throws std::out_of_range exception in case of Failure
  DeMuonChamber& pos2StChamber( const double x, const double y, int station ) const;

  LHCb::Detector::Muon::TileID Chamber2Tile( int chaNum, int station, int region ) const;

  // throws GaudiException exception in case of Failure
  int sensitiveVolumeID( const Gaudi::XYZPoint& myPoint ) const override;

  unsigned int gapID( int sensDetID ) const {
    return LHCb::Packer::getBit( sensDetID, PackMCMuonHit::maskGapID, PackMCMuonHit::shiftGapID );
  }

  unsigned int chamberID( int sensDetID ) const {
    return LHCb::Packer::getBit( sensDetID, PackMCMuonHit::maskChamberID, PackMCMuonHit::shiftChamberID );
  }

  unsigned int regionID( int sensDetID ) const {
    return LHCb::Packer::getBit( sensDetID, PackMCMuonHit::maskRegionID, PackMCMuonHit::shiftRegionID );
  }

  unsigned int stationID( int sensDetID ) const {
    return LHCb::Packer::getBit( sensDetID, PackMCMuonHit::maskStationID, PackMCMuonHit::shiftStationID );
  }

  unsigned int quadrantID( int sensDetID ) const {
    return LHCb::Packer::getBit( sensDetID, PackMCMuonHit::maskQuadrantID, PackMCMuonHit::shiftQuadrantID );
  }

  bool isM1defined() const { return m_isM1defined; }

  std::string getStationName( int station ) const { return ( m_stations.at( station ) ).first; }
  std::string getRegionName( int station, int region ) const {
    return fmt::format( "{}R{}", getStationName( station ), region + 1 );
  }

  // Returns the list of physical channels for a given chamber. May throw std::logic_error
  // when region and chamber are not given and entry and exit are not in same region/chamber
  std::vector<std::pair<MuonFrontEndID, std::array<float, 4>>>
                                 listOfPhysChannels( Gaudi::XYZPoint my_entry, Gaudi::XYZPoint my_exit, int region, int chamber ) const;
  std::optional<Gaudi::XYZPoint> getPCCenter( MuonFrontEndID fe, int chamber, int station, int region ) const;
  // Returns the station index starting from the z position
  int getStation( const double z ) const;

  // Return the chamber
  DeMuonChamber& getChamber( const int station, const int region, const int chmb ) const;

  // Fills various geometry related info
  void fillGeoInfo();
  void fillGeoArray();

  class TilePosition {
  public:
    TilePosition( double x, double y, double z, double dX, double dY, double dZ )
        : m_position( x, y, z ), m_dX( dX ), m_dY( dY ), m_dZ( dZ ) {}
    auto position() const { return m_position; }
    auto x() const { return m_position.x(); }
    auto y() const { return m_position.y(); }
    auto z() const { return m_position.z(); }
    auto dX() const { return m_dX; }
    auto dY() const { return m_dY; }
    auto dZ() const { return m_dZ; }

  private:
    ROOT::Math::XYZPoint m_position;
    double               m_dX, m_dY, m_dZ;
  };
  std::optional<TilePosition> position( LHCb::Detector::Muon::TileID tile ) const;

  void CountDetEls();

  int stations() const { return m_nbStations; }
  int regions() const { return m_regions; }
  int regions( int stations ) const {
    // Number of regions in each station
    return m_regsperSta[stations];
  }
  double getStationZ( const int station ) const { return m_stationZ.at( station ); }

  int    gapsInRegion( const int station, const int region ) const { return m_gapPerRegion[station * 4 + region]; };
  int    gapsPerFE( const int station, const int region ) const { return m_gapPerFE[station * 4 + region]; };
  int    readoutInRegion( const int station, const int region ) const { return m_readoutNumber[station * 4 + region]; };
  int    mapInRegion( const int station, const int region ) const { return m_LogMapPerRegion[station * 4 + region]; };
  double areaChamber( const int station, const int region ) const { return m_areaChamber[station * 4 + region]; };
  unsigned int getPhChannelNX( const int readout, const int station, const int region ) const {
    return m_phChannelNX[readout][station * 4 + region];
  };
  unsigned int getPhChannelNY( const int readout, const int station, const int region ) const {
    return m_phChannelNY[readout][station * 4 + region];
  };
  float getPadSizeX( const int station, const int region ) const { return m_padSizeX[station * 4 + region]; };

  float        getPadSizeY( const int station, const int region ) const { return m_padSizeY[station * 4 + region]; };
  unsigned int getReadoutType( const int ireadout, const int station, const int region ) const {
    return m_readoutType[ireadout][station * 4 + region];
  };
  unsigned int chamberInRegion( const int station, const int region ) const {
    return m_chamberPerRegion[station * 4 + region];
  };
  unsigned int getLogMapInRegion( const int station, const int region ) const {
    return m_LogMapPerRegion[station * 4 + region];
  };
  unsigned int getLogMapRType( const int ireadout, const int station, const int region ) const {
    return m_LogMapRType[ireadout][station * 4 + region];
  };
  unsigned int getLogMapMergex( const int ireadout, const int station, const int region ) const {
    return m_LogMapMergex[ireadout][station * 4 + region];
  };
  unsigned int getLogMapMergey( const int ireadout, const int station, const int region ) const {
    return m_LogMapMergey[ireadout][station * 4 + region];
  };
  unsigned int getLayoutX( const int ireadout, const int station, const int region ) const {
    return m_layoutX[ireadout][station * 4 + region];
  };
  unsigned int getLayoutY( const int ireadout, const int station, const int region ) const {
    return m_layoutY[ireadout][station * 4 + region];
  };
  float getSensAreaX( const int station, const int region ) const { return m_sensitiveAreaX[station * 4 + region]; };
  float getSensAreaY( const int station, const int region ) const { return m_sensitiveAreaY[station * 4 + region]; };
  float getSensAreaZ( const int station, const int region ) const { return m_sensitiveAreaZ[station * 4 + region]; };

  double getInnerX( const int station ) const { return m_stationBox[station][0]; };
  double getInnerY( const int station ) const { return m_stationBox[station][1]; };
  double getOuterX( const int station ) const { return m_stationBox[station][2]; };
  double getOuterY( const int station ) const { return m_stationBox[station][3]; };

  // Return the number of the first chamber from the Detector::Muon::TileID
  int Tile2FirstChamberNumber( const LHCb::Detector::Muon::TileID aTile ) const;

  // Returns the chamber's region number from a Hit
  int Hit2ChamberRegionNumber( const Gaudi::XYZPoint myPoint ) const;

  bool upgradeReadout() const { return m_upgradeReadout; }

  // TODO add here some checks for initialization state (old/new conditions compat)?
  MuonDAQHelper* getDAQInfo() {
    if ( m_upgradeReadout )
      throw GaudiException( "Using Muon conditions with upgraded readout: cannot use getDAQInfo()", "DeMuonDetector",
                            StatusCode::FAILURE );
    return &( m_daqHelper );
  };
  const MuonDAQHelper* getDAQInfo() const {
    if ( m_upgradeReadout )
      throw GaudiException( "Using Muon conditions with upgraded readout: cannot use getDAQInfo()", "DeMuonDetector",
                            StatusCode::FAILURE );
    return &( m_daqHelper );
  };
  MuonUpgradeDAQHelper* getUpgradeDAQInfo() {
    if ( !m_upgradeReadout )
      throw GaudiException( "Using Muon conditions with Run 1-2 readout: cannot use getUpgradeDAQInfo()",
                            "DeMuonDetector", StatusCode::FAILURE );
    return &( m_upgradeDaqHelper );
  };
  const MuonUpgradeDAQHelper* getUpgradeDAQInfo() const {
    if ( !m_upgradeReadout )
      throw GaudiException( "Using Muon conditions with Run 1-2 readout: cannot use getUpgradeDAQInfo()",
                            "DeMuonDetector", StatusCode::FAILURE );
    return &( m_upgradeDaqHelper );
  };

  const MuonReadoutCond& getReadoutCond( const std::string& id ) const {
    if ( MuonReadoutCond* cond = dynamic_cast<MuonReadoutCond*>( condition( id + "Readout" ).target() ) ) {
      return *cond;
    }
    throw GaudiException( fmt::format( "readout condition for {} is not a MuonReadoutCond", id ), name(),
                          StatusCode::FAILURE );
  }
  const MuonReadoutCond& getReadoutCond( int station, int region ) const {
    return getReadoutCond( getRegionName( station, region ) );
  }

private:
  /// returns pair chamberNumber, regNum. May throw std::out_of_range
  std::tuple<int, int> Hit2ChamberNumber( Gaudi::XYZPoint myPoint, int station ) const;

  /// returns triplet gapNumber, chamberNumber, regNum. May throw std::out_of_range
  std::tuple<int, int, int> Hit2GapNumber( Gaudi::XYZPoint myPoint, int station ) const;

  /// returns pair chamberNumber, regNum. May throw std::out_of_range
  std::tuple<int, int> Pos2ChamberNumber( const double x, const double y, const double z ) const;

  /// May throw std::out_of_range
  LHCb::Detector::Muon::TileID Pos2ChamberTile( const double x, const double y, const double z ) const;

  /// May throw std::out_of_range
  DeMuonChamber& Pos2StChamberPointer( const double x, const double y, int station ) const;

  // Useful constants
  static const unsigned int partition      = 20;
  static const unsigned int maxReadoutType = 2;

  // Answer the question: is "station" a filter?
  inline bool testForFilter( const IDetectorElementPlus* station ) const;

private:
  // My vector of Chamber pointers
  std::vector<DeMuonChamber*> m_ChmbPtr;

  // Chamber Layout
  std::unique_ptr<MuonChamberLayout> m_chamberLayout;

  // How many stations and regions
  int m_nbStations = 0;
  int m_regions    = 0;
  int m_regsperSta[5];

  // the stations themselves
  std::vector<std::pair<std::string, int>> m_stations;

  // geometry info
  int          m_readoutNumber[partition];
  unsigned int m_chamberPerRegion[partition];
  unsigned int m_gapPerRegion[partition];
  unsigned int m_LogMapPerRegion[partition];
  unsigned int m_readoutType[maxReadoutType][partition];
  unsigned int m_LogMapRType[maxReadoutType][partition];
  unsigned int m_LogMapMergex[maxReadoutType][partition];
  unsigned int m_LogMapMergey[maxReadoutType][partition];
  unsigned int m_phChannelNX[maxReadoutType][partition];
  unsigned int m_phChannelNY[maxReadoutType][partition];

  float  m_padSizeX[partition];
  float  m_padSizeY[partition];
  int    m_gapPerFE[partition];
  int    m_layoutX[maxReadoutType][partition];
  int    m_layoutY[maxReadoutType][partition];
  double m_areaChamber[partition];
  float  m_sensitiveAreaX[partition];
  float  m_sensitiveAreaY[partition];
  float  m_sensitiveAreaZ[partition];
  double m_stationBox[5][4];
  //  double m_stationZ[5];
  std::vector<double>  m_stationZ;
  MuonDAQHelper        m_daqHelper;
  MuonUpgradeDAQHelper m_upgradeDaqHelper;
  bool                 m_isM1defined    = false;
  bool                 m_upgradeReadout = false;
};

inline int DeMuonDetector::getStation( const double z ) const {
  // station index starting from z position (in mm)
  int    idX = 0;
  double s_size( 400. );
  //  double s_off[5] = {12100,15200,16400,17600,18800};
  double offset = /* DDDB cords - s_off = */ 70; // Shouldn't be zero?

  int nsta = m_stationZ.size();
  for ( idX = 0; idX < nsta; idX++ ) {
    if ( std::abs( z - m_stationZ.at( idX ) + offset ) < s_size ) break;
  }

  return idX;
}

// Answer the question, is "station" a muon filter?
inline bool DeMuonDetector::testForFilter( const IDetectorElementPlus* station ) const {
  std::string_view stationName = station->name();

  // Check if the "station" isn't actually a filter
  return stationName.find( "/MF" ) != stationName.npos;
}

#endif
