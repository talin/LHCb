/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonDet/MuonUpgradeDAQHelper.h"
#include "DetDesc/Condition.h"

#include "GaudiKernel/SmartDataPtr.h"

#include "fmt/format.h"

#include "sstream"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonUpgradeDAQHelper
//
// 2021-07-24 : Alessia Satta
//-----------------------------------------------------------------------------
using namespace LHCb;

namespace {
  template <typename T>
  auto logic_error( T&& x ) {
    return GaudiException( std::forward<T>( x ), "MuonUpgradeDAQHelper", StatusCode::FAILURE );
  }
} // namespace

void MuonUpgradeDAQHelper::initSvc( IDataProviderSvc* detSvc ) {
  for ( unsigned int i = 0; i < MuonUpgradeDAQHelper_maxNODENumber; i++ ) { m_mapTileInNODE[i].reserve( 192 ); }
  m_detSvc = detSvc;

  initDAQMaps();
}

//------------------------------------------------------------------------
//
// Steering routine to retrieve the information
// about cabling in the condition database
//
//------------------------------------------------------------------------

void MuonUpgradeDAQHelper::resetAll() {
  for ( unsigned int t = 0; t < MuonUpgradeDAQHelper_maxTell40Number; t++ ) {
    for ( unsigned int p = 0; p < MuonUpgradeDAQHelper_maxTell40PCINumber; p++ ) {
      for ( unsigned int l = 0; l < MuonUpgradeDAQHelper_linkNumber; l++ ) {
        m_ODENumberNoHole[t][p][l]        = 0;
        m_ODEFrameNumberNoHole[t][p][l]   = 0;
        m_ODENumberWithHole[t][p][l]      = 0;
        m_ODEFrameNumberWithHole[t][p][l] = 0;
        m_Tell40LinkToActiveLink[t][p][l] = 0;
      }
      m_Tell40PCINumberOfActiveLink[t][p] = 0;
    }
    m_which_stationIsTell40[t] = 0;
  }
  for ( unsigned int node = 0; node < MuonUpgradeDAQHelper_maxNODENumber; node++ ) {
    for ( unsigned int nlink = 0; nlink < MuonUpgradeDAQHelper_frameNumber; nlink++ ) {
      m_Tell40Number[node][nlink]        = 0;
      m_Tell40PCINumber[node][nlink]     = 0;
      m_Tell40PCILinkNumber[node][nlink] = 0;
    }
  }
}

void MuonUpgradeDAQHelper::initDAQMaps() {
  // Perform sanity checks about number of TELL1s and ODEs
  resetAll();
  // TELL1 sanity checks

  initTELL40();
  // ODE sanity checks

  initNODE();
  // Ititialize TELL1/ODE maps

  initMaps();

  initSourceID();
}

//------------------------------------------------------------------------
// Perform sanity cheks on number of configured TELL1
// in CondDB
//------------------------------------------------------------------------
void MuonUpgradeDAQHelper::initTELL40() {
  unsigned int totTell40 = 0;
  for ( int station = 0; station < m_nStations; station++ ) { // loop over stations
    // parse the xml file and load the parameters
    std::string cablingBasePath = getBasePath( getStationName( station ) ); // hardwired at the moment
    std::string cablingPath     = cablingBasePath + "Cabling";              // path to the Cabling Condition for station

    SmartDataPtr<MuonUpgradeStationCabling> cabling( m_detSvc, cablingPath );
    if ( 0 == cabling ) { throw logic_error( fmt::format( "{} not found in XmlDDDB", cablingPath ) ); }
    m_NumberOfTell40Boards[station] = cabling->getNumberOfTell40Board(); // update the TELL40 counter
    totTell40 += m_NumberOfTell40Boards[station];
    // check that all tell40 numers are below the max number
    for ( unsigned int iTell = 0; iTell < m_NumberOfTell40Boards[station]; iTell++ ) {
      std::string Tell40Path = cablingBasePath + cabling->getTell40Name( iTell );
      // parse the xml file and load the parameters
      SmartDataPtr<MuonTell40Board> tell40( m_detSvc, Tell40Path );
      if ( tell40->Tell40Number() > MuonUpgradeDAQHelper_maxTell40Number ) {
        throw logic_error( fmt::format( "tell40Sequential number greater than allowed {} {}", tell40->Tell40Number(),
                                        MuonUpgradeDAQHelper_maxTell40Number ) );
      }
      m_which_stationIsTell40[tell40->Tell40Number() - 1] = (unsigned int)tell40->getStation() - 1;
    }
  } // end of loop over stations

  if ( totTell40 > MuonUpgradeDAQHelper_maxTell40Number ) {
    throw logic_error( "Number of retrieved TELL40 in CondDB exceeds max allowed in software" );
  }
}

//------------------------------------------------------------------------
// Add sourceID in Tell40PCI info: the offset is defined to be 0x68 for A side 0x70 for C side
// in  https://edms.cern.ch/ui/file/2100937/5/edms_2100937_raw_data_format_run3.pdf.
//------------------------------------------------------------------------

void MuonUpgradeDAQHelper::initSourceID() {
  for ( int station = 0; station < m_nStations; station++ ) { // loop over stations

    // load the parameters
    std::string                             cablingBasePath = getBasePath( getStationName( station ) );
    std::string                             cablingPath     = cablingBasePath + "Cabling";
    std::string                             ODEBasePath     = "NODEBoard";
    SmartDataPtr<MuonUpgradeStationCabling> cabling( m_detSvc, cablingPath );
    if ( 0 == cabling ) { throw logic_error( fmt::format( "{} not found in XmlDDDB", cablingPath ) ); }
    for ( int iTell = 0; iTell < cabling->getNumberOfTell40Board(); iTell++ ) {
      std::string Tell40Path = cablingBasePath + cabling->getTell40Name( iTell );
      // parse the xml file and load the parameters
      SmartDataPtr<MuonTell40Board> tell40( m_detSvc, Tell40Path );
      if ( 0 == tell40 ) { throw logic_error( fmt::format( "{} not found in XmlDDDB", Tell40Path ) ); }
      long TNumber = tell40->Tell40Number();

      for ( int iPCI = 0; iPCI < tell40->numberOfPCI(); iPCI++ ) {

        std::string                 pcipath = cablingBasePath + tell40->getPCIName( iPCI );
        SmartDataPtr<MuonTell40PCI> pciboard( m_detSvc, pcipath );
        if ( 0 == pciboard ) {
          throw logic_error( fmt::format( "{} not found in XmlDDDB", pcipath ) );
        } else {
          for ( int iLink = 0; iLink < 24; iLink++ ) {
            long node = pciboard->getODENumber( iLink );
            if ( node > 0 ) {
              std::string node_number;
              node_number                           = std::to_string( node );
              std::string                 totalPath = cablingBasePath + ODEBasePath + node_number;
              SmartDataPtr<MuonNODEBoard> nodeboard( m_detSvc, totalPath );
              // after the first nODE the sourceID is defined so skip the rest of nODEs
              long quadrant = nodeboard->quadrant();
              long sourceID = ( TNumber - 1 ) * 2 + iPCI;
              if ( quadrant == 0 || quadrant == 1 ) {
                sourceID = sourceID + 0x6800;
                if ( sourceID > 0x6FFF ) {
                  throw GaudiException( "sourceID too big", "MuonUpgradeDAQHelper::initSourceID", StatusCode::FAILURE );
                }
              } else {
                sourceID = sourceID + 0x7000;
                if ( sourceID > 0x77FF ) {
                  throw GaudiException( "sourceID too big", "MuonUpgradeDAQHelper::initSourceID", StatusCode::FAILURE );
                }
              }
              pciboard->setSourceID( sourceID );
              // fill also local variable to store it
              m_sourceID[TNumber - 1][iPCI] = sourceID;
              break;
            }
          }
        }
      }
    }
  }
}
//------------------------------------------------------------------------
// Perform sanity cheks on number of configured ODE
// in CondDB
//------------------------------------------------------------------------
void MuonUpgradeDAQHelper::initNODE() {

  bool connectedODE[MuonUpgradeDAQHelper_maxNODENumber];
  bool connectedLinkODE[MuonUpgradeDAQHelper_maxNODENumber][4];
  for ( int station = 0; station < m_nStations; station++ ) {
    m_startNODEInStation[station] = 999;
    m_stopNODEInStation[station]  = 0;
    for ( unsigned int r = 0; r < 4; r++ ) {
      for ( unsigned int q = 0; q < 4; q++ ) {
        m_ODENameSRQStart[station][r][q] = 999;
        m_ODENameSRQStop[station][r][q]  = 0;
      }
    }
  }
  for ( unsigned int i = 0; i < MuonUpgradeDAQHelper_maxNODENumber; i++ ) {
    connectedODE[i] = false;
    for ( int j = 0; j < 4; j++ ) { connectedLinkODE[i][j] = false; }
  }

  for ( int station = 0; station < m_nStations; station++ ) { // loop over stations
    // load the parameters
    std::string                             cablingBasePath = getBasePath( getStationName( station ) );
    std::string                             cablingPath     = cablingBasePath + "Cabling";
    SmartDataPtr<MuonUpgradeStationCabling> cabling( m_detSvc, cablingPath );
    if ( 0 == cabling ) { throw logic_error( fmt::format( "{} not found in XmlDDDB", cablingPath ) ); }
    for ( int iTell = 0; iTell < cabling->getNumberOfTell40Board(); iTell++ ) {
      std::string Tell40Path = cablingBasePath + cabling->getTell40Name( iTell );
      // parse the xml file and load the parameters
      SmartDataPtr<MuonTell40Board> tell40( m_detSvc, Tell40Path );
      if ( 0 == tell40 ) { throw logic_error( fmt::format( "{} not found in XmlDDDB", Tell40Path ) ); }
      for ( int iPCI = 0; iPCI < tell40->numberOfPCI(); iPCI++ ) {
        std::string                 pcipath = cablingBasePath + tell40->getPCIName( iPCI );
        SmartDataPtr<MuonTell40PCI> pciboard( m_detSvc, pcipath );
        if ( 0 == pciboard ) {
          throw logic_error( fmt::format( "{} not found in XmlDDDB", pcipath ) );
        } else {
          for ( int iLink = 0; iLink < 24; iLink++ ) {
            long node = pciboard->getODENumber( iLink );
            for ( long r = 0; r < 4; r++ ) {
              long tsx                = pciboard->getTSLayoutX( r );
              long tsy                = pciboard->getTSLayoutY( r );
              m_TSlayoutX[station][r] = tsx;
              m_TSlayoutY[station][r] = tsy;
            }
            if ( node > 0 ) {

              connectedODE[node - 1] = 1;
              if ( node < m_startNODEInStation[station] ) m_startNODEInStation[station] = node;
              if ( node > m_stopNODEInStation[station] ) m_stopNODEInStation[station] = node;
              long nlink = pciboard->getLinkNumber( iLink );
              if ( connectedLinkODE[node - 1][nlink] > 0 ) {
                throw logic_error(
                    fmt::format( "same link {} for node {} already connected now in {}", nlink, node, pcipath ) );
              } else {
                connectedLinkODE[node - 1][nlink] = 1;
              }
            }
          }
        }
      } // end of loop over PCI
    }   // end of loop over TELL40s
  }     // end of loop over stations
  unsigned int NODECounter = 0;
  for ( unsigned int i = 0; i < MuonUpgradeDAQHelper_maxNODENumber; i++ ) {
    if ( connectedODE[i] ) NODECounter++;
  }

  if ( NODECounter > MuonUpgradeDAQHelper_maxNODENumber ) {
    throw logic_error(
        fmt::format( " Number of retrieved NODEs in CondDB exceeds max allowed in software: {}", NODECounter ) );
  }

  for ( int station = 0; station < m_nStations; station++ ) {
    // check thatNODE number are sequantially assigned
    if ( m_stopNODEInStation[station] < m_startNODEInStation[station] ) {
      throw logic_error( "NODE number not assigned sequantially correct the maos" );
    }
    for ( int intstation = station + 1; intstation < m_nStations; intstation++ ) {
      if ( m_stopNODEInStation[station] > m_startNODEInStation[intstation] ) {
        throw logic_error( "NODE number not assigned sequantially correct the maos" );
      }
    }
  }
}

std::string MuonUpgradeDAQHelper::getBasePath( std::string statname ) const { return basePath.at( statname ); }
std::string MuonUpgradeDAQHelper::getStationName( int sta ) const { return stationName[sta]; }

StatusCode MuonUpgradeDAQHelper::test() {
  for ( int station = 0; station < m_nStations; station++ ) {
    std::string cablingBasePath = getBasePath( getStationName( station ) );
    std::string ODEBasePath     = "NODEBoard";
    for ( unsigned int i = m_startNODEInStation[station]; i <= m_stopNODEInStation[station]; i++ ) {
      std::string node_number;
      node_number           = std::to_string( i );
      std::string totalPath = cablingBasePath + ODEBasePath + node_number;
    }
  }
  return StatusCode::SUCCESS;
}

//------------------------------------------------------------------------
//  load TELL1/ODE pad mapping parameters
//------------------------------------------------------------------------
void MuonUpgradeDAQHelper::initMaps() {
  for ( int station = 0; station < m_nStations; station++ ) {
    std::string cablingBasePath = getBasePath( getStationName( station ) );
    std::string ODEBasePath     = "NODEBoard";
    for ( unsigned int i = m_startNODEInStation[station]; i <= m_stopNODEInStation[station]; i++ ) {
      std::string node_number;
      node_number                           = std::to_string( i );
      std::string                 totalPath = cablingBasePath + ODEBasePath + node_number;
      SmartDataPtr<MuonNODEBoard> nodeboard( m_detSvc, totalPath );
      if ( 0 == nodeboard ) {
        throw logic_error( fmt::format( "{} not found in XmlDDDB", totalPath ) );
      } else {
        m_fullNODEPath[i - 1] = cablingBasePath + ODEBasePath + node_number;
      }
    }
  }
  // set the correspondance among NODE link and Tell40 input
  for ( int station = 0; station < m_nStations; station++ ) { // loop over stations
    // load the parameters
    std::string                             cablingBasePath = getBasePath( getStationName( station ) );
    std::string                             cablingPath     = cablingBasePath + "Cabling";
    SmartDataPtr<MuonUpgradeStationCabling> cabling( m_detSvc, cablingPath );
    if ( 0 == cabling ) { throw logic_error( fmt::format( "{} not found in XmlDDDB", cablingPath ) ); }
    for ( int iTell = 0; iTell < cabling->getNumberOfTell40Board(); iTell++ ) {
      std::string Tell40Path = cablingBasePath + cabling->getTell40Name( iTell );
      // parse the xml file and load the parameters
      SmartDataPtr<MuonTell40Board> tell40( m_detSvc, Tell40Path );
      if ( 0 == tell40 ) { throw logic_error( fmt::format( "{} not found in XmlDDDB", Tell40Path ) ); }
      long TNumber = tell40->Tell40Number();

      for ( int iPCI = 0; iPCI < tell40->numberOfPCI(); iPCI++ ) {
        unsigned int                noHoleCounter = 0;
        std::string                 pcipath       = cablingBasePath + tell40->getPCIName( iPCI );
        SmartDataPtr<MuonTell40PCI> pciboard( m_detSvc, pcipath );
        if ( 0 == pciboard ) {
          throw logic_error( fmt::format( "{} not found in XmlDDDB", pcipath ) );
        } else {
          long         PNumber             = pciboard->Tell40PCINumber();
          unsigned int active_link_per_PCI = 0;
          for ( int iLink = 0; iLink < 24; iLink++ ) {
            long node = pciboard->getODENumber( iLink );
            if ( node > 0 ) {
              long nlink                                                  = pciboard->getLinkNumber( iLink );
              m_Tell40Number[node - 1][nlink]                             = TNumber;
              m_Tell40PCINumber[node - 1][nlink]                          = PNumber;
              m_Tell40PCILinkNumber[node - 1][nlink]                      = iLink;
              m_ODENumberNoHole[TNumber - 1][PNumber][noHoleCounter]      = node;
              m_ODEFrameNumberNoHole[TNumber - 1][PNumber][noHoleCounter] = nlink;
              m_ODENumberWithHole[TNumber - 1][PNumber][iLink]            = node;
              m_ODEFrameNumberWithHole[TNumber - 1][PNumber][iLink]       = nlink;
              m_Tell40LinkToActiveLink[TNumber - 1][PNumber][iLink]       = noHoleCounter;
              noHoleCounter++;
              active_link_per_PCI++;
              m_Tell40PCINumberOfActiveLink[TNumber - 1][iPCI] = active_link_per_PCI;
            }
          }
        }
      } // end of loop over PCI
    }   // end of loop over TELL40s
  }     // end of loop over stations

  // fill the map of which  Detector::Muon::TileID is inside an ODE channel
  for ( int station = 0; station < m_nStations; station++ ) {

    unsigned int region          = 0;
    unsigned int unique_quadrant = 99;
    std::string  cablingBasePath = getBasePath( getStationName( station ) );
    std::string  ODEBasePath     = "NODEBoard";
    for ( unsigned int i = m_startNODEInStation[station]; i <= m_stopNODEInStation[station]; i++ ) {
      std::string  node_number;
      unsigned int ODE_channels             = 0;
      node_number                           = std::to_string( i );
      std::string                 totalPath = cablingBasePath + ODEBasePath + node_number;
      SmartDataPtr<MuonNODEBoard> nodeboard( m_detSvc, totalPath );

      if ( 0 == nodeboard ) {
        throw logic_error( fmt::format( "{} not found in XmlDDDB", totalPath ) );
      } else {
        m_ODENameInECS[nodeboard->getNODESerialNumber() - 1] = nodeboard->ECSName();
        region                                               = nodeboard->region();
        unique_quadrant                                      = nodeboard->quadrant();
        if ( m_ODENameSRQStart[station][region][unique_quadrant] > nodeboard->getNODESerialNumber() )
          m_ODENameSRQStart[station][region][unique_quadrant] = nodeboard->getNODESerialNumber();
        if ( m_ODENameSRQStop[station][region][unique_quadrant] < nodeboard->getNODESerialNumber() )
          m_ODENameSRQStop[station][region][unique_quadrant] = nodeboard->getNODESerialNumber();

        unsigned int TSLayoutX = nodeboard->getTSLayoutX();
        unsigned int TSLayoutY = nodeboard->getTSLayoutY();
        for ( int iTS = 0; iTS < nodeboard->getTSNumber(); iTS++ ) {
          std::string                    TSPath = cablingBasePath + nodeboard->getTSName( iTS );
          SmartDataPtr<MuonUpgradeTSMap> myts( m_detSvc, TSPath );
          if ( 0 == myts ) {
            throw logic_error( fmt::format( "{} not found in XmlDDDB", TSPath ) );
          } else {
            unsigned int quadrant = nodeboard->getTSQuadrant( iTS );
            unsigned int TSGridX  = nodeboard->getTSGridX( iTS );
            unsigned int TSGridY  = nodeboard->getTSGridY( iTS );
            //    int iOdeChan = 0;
            int iusedch = 0;
            for ( int isynch = 0; isynch < myts->synchChSize(); isynch++ ) {
              ODE_channels++;
              if ( myts->synchChUsed( isynch ) ) {
                unsigned int                 layout       = myts->layoutOutputChannel( iusedch );
                unsigned int                 layoutX      = myts->gridXLayout( layout );
                unsigned int                 layoutY      = myts->gridYLayout( layout );
                unsigned int                 digitOffSetX = layoutX * TSGridX;
                unsigned int                 digitOffSetY = layoutY * TSGridY;
                unsigned int                 digitX       = digitOffSetX + myts->gridXOutputChannel( iusedch );
                unsigned int                 digitY       = digitOffSetY + myts->gridYOutputChannel( iusedch );
                LHCb::Detector::Muon::Layout lay( TSLayoutX * layoutX,
                                                  TSLayoutY * layoutY ); // layout in terms of H- or V- strips
                Detector::Muon::TileID       muontile( station, lay, region, quadrant, digitX, digitY );
                m_mapTileInNODE[nodeboard->getNODESerialNumber() - 1].push_back( muontile );
                iusedch++;
              } else {
                // empty tiles for unused channels
                Detector::Muon::TileID muontile;
                m_mapTileInNODE[nodeboard->getNODESerialNumber() - 1].push_back( muontile );
              }
            }
          }
        }
      }
    }
  }

  // now fill the Tell40 frame maps
  for ( unsigned int t = 0; t < MuonUpgradeDAQHelper_maxTell40Number; t++ ) {
    for ( unsigned int p = 0; p < MuonUpgradeDAQHelper_maxTell40PCINumber; p++ ) {
      for ( unsigned int l = 0; l < MuonUpgradeDAQHelper_linkNumber; l++ ) {
        unsigned int ode   = m_ODENumberNoHole[t][p][l];
        unsigned int frame = m_ODEFrameNumberNoHole[t][p][l];
        if ( ode > 0 ) {
          for ( unsigned int ch = 0; ch < MuonUpgradeDAQHelper_ODEFrameSize; ch++ ) {
            Detector::Muon::TileID pippo = m_mapTileInNODE[ode - 1][frame * 48 + ch];
            if ( pippo.isValid() ) {
              m_mapStationOfLink[t][p][l] = pippo.station();
              m_mapRegionOfLink[t][p][l]  = pippo.region();
              m_mapQuarterOfLink[t][p][l] = pippo.quarter();
            }
            m_mapTileInTell40[t][p][(l)*MuonUpgradeDAQHelper_ODEFrameSize + ch] = pippo;
          }
        }
      }
    }
  }
}

LHCb::Detector::Muon::TileID MuonUpgradeDAQHelper::findTS( const LHCb::Detector::Muon::TileID digit ) const {
  unsigned int station = digit.station();
  unsigned int region  = digit.region();

  unsigned int                 TSLayoutX = getTSLayoutX( station, region );
  unsigned int                 TSLayoutY = getTSLayoutY( station, region );
  LHCb::Detector::Muon::Layout TSLayout( TSLayoutX, TSLayoutY );
  return TSLayout.contains( digit );
}

std::string MuonUpgradeDAQHelper::findODEPath( LHCb::Detector::Muon::TileID TS ) const {
  unsigned int station  = TS.station();
  unsigned int region   = TS.region();
  unsigned int quadrant = TS.quarter();
  int          odeStart = m_ODENameSRQStart[station][region][quadrant];
  int          odeEnd   = m_ODENameSRQStop[station][region][quadrant] + 1;

  for ( int ode = odeStart; ode < odeEnd; ode++ ) {
    std::string                 odePath = m_fullNODEPath[ode - 1];
    SmartDataPtr<MuonNODEBoard> odeBoard( m_detSvc, odePath );
    if ( odeBoard->isTSContained( TS ) ) return odePath;
  }
  return "";
}

int MuonUpgradeDAQHelper::findODENumber( LHCb::Detector::Muon::TileID TS ) const {
  unsigned int station  = TS.station();
  unsigned int region   = TS.region();
  unsigned int quadrant = TS.quarter();
  int          odeStart = m_ODENameSRQStart[station][region][quadrant];
  int          odeEnd   = m_ODENameSRQStop[station][region][quadrant] + 1;

  for ( int ode = odeStart; ode < odeEnd; ode++ ) {
    std::string                 odePath = m_fullNODEPath[ode - 1];
    SmartDataPtr<MuonNODEBoard> odeBoard( m_detSvc, odePath );
    if ( odeBoard->isTSContained( TS ) ) return odeBoard->getNODESerialNumber();
  }
  return 0;
}

unsigned int MuonUpgradeDAQHelper::findTSPosition( std::string ODEPath, LHCb::Detector::Muon::TileID TSTile ) const {
  unsigned int quadrant = TSTile.quarter();
  unsigned int gridx    = TSTile.nX();
  unsigned int gridy    = TSTile.nY();

  SmartDataPtr<MuonNODEBoard> ode( m_detSvc, ODEPath );

  for ( int TS = 0; TS < ode->getTSNumber(); TS++ ) {
    if ( (unsigned int)ode->getTSQuadrant( TS ) == quadrant ) {
      if ( (unsigned int)ode->getTSGridX( TS ) == gridx ) {
        if ( (unsigned int)ode->getTSGridY( TS ) == gridy ) { return TS; }
      }
    }
  }
  throw logic_error( "error in finding TS postion " );
}

std::string MuonUpgradeDAQHelper::findTSPath( std::string ODEPath, long TSPosition, int station ) const {

  SmartDataPtr<MuonNODEBoard> ode( m_detSvc, ODEPath );

  std::string base   = getBasePath( getStationName( station ) );
  std::string TSPath = ode->getTSName( TSPosition );

  return base + TSPath;
}

unsigned int MuonUpgradeDAQHelper::findDigitInTS( std::string TSPath, LHCb::Detector::Muon::TileID TSTile,
                                                  LHCb::Detector::Muon::TileID digit, bool hole ) const {
  SmartDataPtr<MuonUpgradeTSMap> TS( m_detSvc, TSPath );
  unsigned int                   TSLayoutX = TSTile.layout().xGrid();
  unsigned int                   TSLayoutY = TSTile.layout().yGrid();

  unsigned int layoutX = digit.layout().xGrid();
  unsigned int layoutY = digit.layout().yGrid();

  // calculate the relative position of the digit in the TS
  unsigned int xScaleFactor = layoutX / TSLayoutX;
  unsigned int yScaleFactor = layoutY / TSLayoutY;

  unsigned int gridx = digit.nX() - xScaleFactor * TSTile.nX();
  unsigned int gridy = digit.nY() - yScaleFactor * TSTile.nY();

  int layout = -1;
  // calculate the relative position of the digit in the TS
  for ( int i = 0; i < TS->numberOfLayout(); i++ ) {
    if ( (unsigned int)TS->gridXLayout( i ) == xScaleFactor && (unsigned int)TS->gridYLayout( i ) == yScaleFactor ) {
      layout = i;
    }
  }
  if ( layout < 0 ) { logic_error( fmt::format( "Layout in TS {}", TS->numberOfLayout() ) ); }

  unsigned int ch_no_hole = 0;
  for ( int i = 0; i < TS->synchChSize(); i++ ) {
    if ( TS->synchChUsed( i ) ) {
      if ( TS->layoutOutputChannel( ch_no_hole ) == layout ) {
        if ( (unsigned int)TS->gridXOutputChannel( ch_no_hole ) == gridx ) {
          if ( (unsigned int)TS->gridYOutputChannel( ch_no_hole ) == gridy ) { return hole ? i : ch_no_hole; }
        }
      }
      ch_no_hole++;
    }
  }
  throw logic_error( fmt::format( "error in finding digit in TS tile {} local to TS. "
                                  "Position in  TS should be {} {} {} {} {} {}. "
                                  "Layout should be {} {} {}",
                                  TSTile, gridx, gridy, layout, digit, layoutX, layoutY, xScaleFactor, yScaleFactor,
                                  TSPath ) );
}
void MuonUpgradeDAQHelper::DAQaddressInODE( LHCb::Detector::Muon::TileID digitTile, unsigned int& ODENumber,
                                            unsigned int& frame, unsigned int& channelInFrame ) const {
  // find TS
  bool                         hole    = true;
  unsigned int                 station = digitTile.station();
  LHCb::Detector::Muon::TileID TSTile  = findTS( digitTile );
  if ( !TSTile.isValid() ) { throw std::invalid_argument( "the digit is not contained in TS: wrong TSTile" ); }
  ODENumber = findODENumber( TSTile );
  if ( ODENumber < 1 ) {
    throw std::invalid_argument( fmt::format( "the TS is not contained in NODEs: ODENumber={} <1", ODENumber ) );
  }
  std::string                    odePath           = m_fullNODEPath[ODENumber - 1];
  unsigned int                   TSposition        = findTSPosition( odePath, TSTile );
  std::string                    TSpath            = findTSPath( odePath, TSposition, station );
  unsigned int                   digitPositionInTS = findDigitInTS( TSpath, TSTile, digitTile, hole );
  SmartDataPtr<MuonUpgradeTSMap> TSMap( m_detSvc, TSpath );
  unsigned int                   digitInODE          = TSposition * TSMap->synchChSize();
  unsigned int                   DigitOutputPosition = digitInODE + digitPositionInTS;
  frame                                              = DigitOutputPosition / MuonUpgradeDAQHelper_ODEFrameSize;
  channelInFrame                                     = DigitOutputPosition % MuonUpgradeDAQHelper_ODEFrameSize;
}

StatusCode MuonUpgradeDAQHelper::DAQaddressInTELL40( LHCb::Detector::Muon::TileID digitTile, long& Tell40Number,
                                                     unsigned int& Tell40PCINumber,
                                                     unsigned int& Tell40PCILinkNumber ) const {
  StatusCode   sc;
  unsigned int ODENumber = 0;
  unsigned int frame     = 999;
  unsigned int channelInFrame;
  DAQaddressInODE( digitTile, ODENumber, frame, channelInFrame );
  Tell40Number        = m_Tell40Number[ODENumber - 1][frame];
  Tell40PCINumber     = m_Tell40PCINumber[ODENumber - 1][frame];
  Tell40PCILinkNumber = m_Tell40PCILinkNumber[ODENumber - 1][frame];
  return StatusCode::SUCCESS;
}

LHCb::Detector::Muon::TileID MuonUpgradeDAQHelper::TileInTell40Frame( long Tell40Number, unsigned int Tell40PCINumber,
                                                                      unsigned int Tell40PCILinkNumber,
                                                                      unsigned int chInFrame ) const {
  unsigned int linkNoHole = m_Tell40LinkToActiveLink[Tell40Number - 1][Tell40PCINumber][Tell40PCILinkNumber];
  return m_mapTileInTell40[Tell40Number - 1][Tell40PCINumber]
                          [linkNoHole * MuonUpgradeDAQHelper_ODEFrameSize + chInFrame];
}

void MuonUpgradeDAQHelper::getSynchConnection( unsigned int NODE, unsigned int frame, long& Tell40Number,
                                               unsigned int& Tell40PCINumber,
                                               unsigned int& Tell40PCILinkNumber ) const {

  if ( NODE < 1 || frame > 3 ) {
    Tell40Number        = 0;
    Tell40PCINumber     = 0;
    Tell40PCILinkNumber = 0;
  }
  Tell40Number        = m_Tell40Number[NODE - 1][frame];
  Tell40PCINumber     = m_Tell40PCINumber[NODE - 1][frame];
  Tell40PCILinkNumber = m_Tell40PCILinkNumber[NODE - 1][frame];
}

void MuonUpgradeDAQHelper::getODENumberAndSynch( long Tell40Number, unsigned int Tell40PCINumber,
                                                 unsigned int Tell40PCILinkNumber, int& ODENumber,
                                                 int& SynchNumber ) const {
  ODENumber   = -1;
  SynchNumber = -1;

  if ( Tell40Number > 0 && Tell40Number < MuonUpgradeDAQHelper_maxTell40Number ) {
    if ( Tell40PCINumber == 0 || Tell40PCINumber == 1 ) {
      if ( Tell40PCILinkNumber < MuonUpgradeDAQHelper_linkNumber ) {
        ODENumber   = m_ODENumberNoHole[Tell40Number - 1][Tell40PCINumber][Tell40PCILinkNumber];
        SynchNumber = m_ODEFrameNumberNoHole[Tell40Number - 1][Tell40PCINumber][Tell40PCILinkNumber];
      }
    }
  }
}
