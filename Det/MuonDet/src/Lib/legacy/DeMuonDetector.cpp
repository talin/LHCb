/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MuonDet/DeMuonDetector.h"
#include "DetDesc/IGeometryInfo.h"
#include "DetDesc/IPVolume.h"
#include "DetDesc/SolidBox.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "Kernel/STLExtensions.h"
#include "MuonDet/DeMuonRegion.h"
#include "MuonDet/MuonChamberGrid.h"
#include "MuonDet/MuonChamberLayout.h"

// Maximum number of chambers allowed per region
// Initialize the maximum number of allowed chambers per region
namespace {
  constexpr const std::array<int, 4> MaxRegions = {12, 24, 48, 192}; // this can go to DDDB
}

const CLID& DeMuonDetector::clID() const { return DeMuonDetector::classID(); }

StatusCode DeMuonDetector::initialize() {
  return DetectorElementPlus::initialize().andThen( [&]() -> StatusCode {
    // Check if this is an upgrade DB
    {
      SmartDataPtr<MuonUpgradeStationCabling> cabling( dataSvc(), DeMuonLocation::Cabling + "/M2Upgrade/Cabling" );
      m_upgradeReadout = cabling != 0;
      if ( !m_upgradeReadout ) {
        SmartDataPtr<DetectorElementPlus> muonSys( dataSvc(), DeMuonLocation::Default );
        // TODO replace muonSys->childIDetectorElements() with this->...
        if ( !muonSys ) {
          throw GaudiException( "Error retrieving the muon detector", "DeMuonDetector::initialize",
                                StatusCode::FAILURE );
        }
        for ( auto* itSt : muonSys->childIDetectorElements() ) {
          std::string name = itSt->name();
          // Test if M1 is defined in the DB
          m_isM1defined |= ( name.find( "/M1" ) != name.npos );
          if ( m_isM1defined ) break;
        }
      }
    }

    // Initializing the Layout in chamber coordinates
    MuonLayout R1( 1, 1 ), R2( 1, 2 ), R3( 1, 4 ), R4( 2, 8 ); // also this can go to DDDB
    m_chamberLayout.reset( new MuonChamberLayout( R1, R2, R3, R4, m_isM1defined ) );

    m_ChmbPtr = m_chamberLayout->fillChambersVector( *this );

    // Initialize vectors containing Detector informations
    CountDetEls();

    // fill geo info
    fillGeoInfo();
    fillGeoArray();

    if ( m_upgradeReadout ) {
      m_upgradeDaqHelper.initSvc( dataSvc() );
    } else {
      m_daqHelper.initSvc( dataSvc(), msgSvc() );
    }

    // fill list of readout conditions
    for ( int i = 0; i < regions(); ++i ) {
      auto rname = fmt::format( "{}R{}Readout", getStationName( i / 4 ), ( i % 4 ) + 1 );
      createCondition( rname, "Conditions/ReadoutConf/Muon/" + rname );
    }

    return StatusCode::SUCCESS;
  } );
}

std::tuple<int, int, int> DeMuonDetector::Hit2GapNumber( Gaudi::XYZPoint myPoint, int station ) const {
  // This methods sets the gap = 0 if only the station is provided [HitPoint.z() = 0]
  auto [chamberNumber, regNum]     = Hit2ChamberNumber( myPoint, station );
  DeMuonChamber&  theChmb          = getChamber( station, regNum, chamberNumber );
  Gaudi::XYZPoint myPointInChFrame = theChmb.geometryPlus()->toLocal( myPoint );

  // Is the chamber returned containing the hit?
  auto pointInGasGap = theChmb.isPointInGasGap( myPointInChFrame );
  if ( !pointInGasGap ) throw GaudiException( "No Gap found", "DeMuonDetector", StatusCode::FAILURE );
  return std::tuple<int, int, int>{pointInGasGap->number, chamberNumber, regNum};
}

std::tuple<int, int> DeMuonDetector::Hit2ChamberNumber( Gaudi::XYZPoint myPoint, int station ) const {
  double x = myPoint.x();
  double y = myPoint.y();
  double z = myPoint.z();

  // Returning the most likely chamber
  int chamberNumber;
  int regNum;
  m_chamberLayout->chamberMostLikely( x, y, station, chamberNumber, regNum );
  if ( regNum >= 0 && chamberNumber >= 0 ) {

    // Providing all 3 numbers identifies a chamber
    IGeometryInfoPlus* geoChm = ( getChamber( station, regNum, chamberNumber ) ).geometryPlus();

    // For calls not providing hit z take the z of chamber center
    if ( !z ) { myPoint.SetZ( geoChm->toGlobal( Gaudi::XYZPoint( 0, 0, 0 ) ).z() ); }

    // Is the chamber returned containing the hit?
    if ( geoChm->isInside( myPoint ) ) return {chamberNumber, regNum};

    // Find the vector of chambers near the one under investigation
    auto x_ref = geoChm->toGlobal( Gaudi::XYZPoint( 0, 0, 0 ) ).x();
    auto y_ref = geoChm->toGlobal( Gaudi::XYZPoint( 0, 0, 0 ) ).y();

    int x_sgn( 0 ), y_sgn( 0 );
    // Avoid unnecessary checks for resolution problems
    if ( std::abs( x - x_ref ) > 0.01 ) { x_sgn = (int)( ( x - x_ref ) / std::abs( x - x_ref ) ); }
    if ( std::abs( y - y_ref ) > 0.01 ) { y_sgn = (int)( ( y - y_ref ) / std::abs( y - y_ref ) ); }

    std::vector<int> regs;
    std::vector<int> myChams;
    m_chamberLayout->returnChambers( station, x_ref, y_ref, x_sgn, y_sgn, regs, myChams );

    // Loops on found chambers
    int idx = 0;
    for ( int tmpChn : myChams ) {
      int tmpRen = regs.at( idx );
      idx++;
      // Accessing Geometry Info
      IGeometryInfoPlus* geoOthChm = ( getChamber( station, tmpRen, tmpChn ) ).geometryPlus();
      if ( geoOthChm ) {
        // For calls not providing hit z take the z of chamber center
        if ( !z ) { myPoint.SetZ( geoOthChm->toGlobal( Gaudi::XYZPoint( 0, 0, 0 ) ).z() ); }
        if ( geoOthChm->isInside( myPoint ) ) return {tmpChn, tmpRen};
      } else {
        throw std::logic_error( "Could not find Geometry info of a given chamber!" );
      }
    }
  }

  int msta{0};
  // Getting stations
  for ( auto* itSt : childIDetectorElements() ) {
    if ( testForFilter( itSt ) ) continue;

    if ( msta == station ) {
      // Getting regions
      for ( auto* itSide : itSt->childIDetectorElements() ) {
        int mreg = 0;
        for ( auto* itRg : itSide->childIDetectorElements() ) {
          // Getting chambers
          int mchm = 0;
          for ( auto* itCh : itRg->childIDetectorElements() ) {
            IGeometryInfoPlus* geoAllChm = itCh->geometryPlus();

            // For calls not providing hit z take the z of chamber center
            if ( !z ) { myPoint.SetZ( geoAllChm->toGlobal( Gaudi::XYZPoint( 0, 0, 0 ) ).z() ); }
            if ( geoAllChm->isInside( myPoint ) ) {
              // Returns the correct region and chamber number
              DeMuonChamber* pCh = dynamic_cast<DeMuonChamber*>( itCh );
              return {pCh->chamberNumber(), mreg};
            }
            mchm++;
          }
          // Chamber Loop
          mreg++;
        } // Region Loop
      }   // side loop
    }
    msta++;
  } // Stations Loop

  // Nothing found
  throw std::out_of_range( "No Chamber Found" );
}

int DeMuonDetector::regNum( const double x, const double y, int station ) const {
  Gaudi::XYZPoint hitPoint( x, y, 0 );
  try {
    auto [chamberNumber, regNum] = Hit2ChamberNumber( hitPoint, station );
    return regNum;
  } catch ( std::exception& ) { return -1; }
}

DeMuonChamber& DeMuonDetector::pos2StChamber( const double x, const double y, int station ) const {
  Gaudi::XYZPoint hitPoint( x, y, 0 );
  auto [chamberNumber, regNum] = Hit2ChamberNumber( hitPoint, station );
  return getChamber( station, regNum, chamberNumber );
}

DeMuonChamber& DeMuonDetector::Pos2StChamberPointer( const double x, const double y, int station ) const {
  // Hit Z is not know (giving only the station).
  // Take the chamber Z to update the hit later on.
  auto [chamberNumber, regNum] = Hit2ChamberNumber( Gaudi::XYZPoint{x, y, 0}, station );
  return getChamber( station, regNum, chamberNumber );
}

std::tuple<int, int> DeMuonDetector::Pos2ChamberNumber( const double x, const double y, const double z ) const {
  // Z is know/provided.
  return Hit2ChamberNumber( Gaudi::XYZPoint{x, y, z}, getStation( z ) );
}

LHCb::Detector::Muon::TileID DeMuonDetector::Pos2ChamberTile( const double x, const double y, const double z ) const {
  // Return the chamber number
  auto [dumChmb, reg] = Pos2ChamberNumber( x, y, z );
  // Convert chamber number into a tile
  return m_chamberLayout->tileChamberNumber( getStation( z ), reg, dumChmb );
}

void DeMuonDetector::CountDetEls() {

  int msta( 0 ), mallreg( 0 );

  // Getting stations
  for ( auto* itSt : childIDetectorElements() ) {
    if ( testForFilter( itSt ) ) continue;
    if ( itSt ) msta++;

    // Getting regions
    const auto& sides = itSt->childIDetectorElements();
    const auto& regs  = sides.front()->childIDetectorElements();
    auto        mreg =
        std::count_if( begin( regs ), end( regs ), []( const IDetectorElementPlus* rg ) -> bool { return rg; } );
    m_regsperSta[msta - 1] = mreg;
    mallreg += mreg;

  } // Stations Loop

  m_nbStations = msta;
  // these are the so-called partitions i.e. regions summed over stations
  m_regions = mallreg;
}

DeMuonChamber& DeMuonDetector::getChamber( const int station, const int region, const int chmb ) const {
  int            encode = 276 * station + chmb + std::accumulate( MaxRegions.begin(), MaxRegions.begin() + region, 0 );
  DeMuonChamber* myPtr  = m_ChmbPtr.at( encode );
  if ( !myPtr ) throw std::out_of_range( "DeMuonDetector::getChamber : no chamber found" );
  return *myPtr;
}

std::vector<std::pair<MuonFrontEndID, std::array<float, 4>>>
DeMuonDetector::listOfPhysChannels( Gaudi::XYZPoint my_entry, Gaudi::XYZPoint my_exit, int region, int chamber ) const {
  int station  = getStation( my_entry.z() );
  int station1 = getStation( my_exit.z() );

  // in cse rion and chamber are not given, compute them from my_entry and my_exit
  if ( ( region == -1 ) || ( chamber == -1 ) ) {
    // Hit entry
    auto [chamberNumber, regNum] = Hit2ChamberNumber( my_entry, station );
    // Hit exit
    auto [chamberNumber1, regNum1] = Hit2ChamberNumber( my_exit, station1 );
    if ( chamberNumber != chamberNumber1 || regNum != regNum1 ) {
      throw std::logic_error( "Hit entry and exit are in different chambers! Returning an empty list." );
    }
    chamber = chamberNumber;
    region  = regNum;
  }

  // Getting the chamber pointer.
  DeMuonChamber& myCh = getChamber( station, region, chamber );
  // check if the retrieved chamber is OK
  Gaudi::XYZPoint midInCh =
      ( myCh.geometryPlus() )
          ->toLocal( Gaudi::XYZPoint( ( ( my_entry.x() + my_exit.x() ) / 2 ), ( ( my_entry.y() + my_exit.y() ) / 2 ),
                                      ( ( my_entry.z() + my_exit.z() ) / 2 ) ) );
  Gaudi::XYZPoint entryInCh     = ( myCh.geometryPlus() )->toLocal( my_entry );
  Gaudi::XYZPoint exitInCh      = ( myCh.geometryPlus() )->toLocal( my_exit );
  auto            pointInGasGap = myCh.isPointInGasGap( midInCh );
  if ( !pointInGasGap ) { return {}; }

  int gapCnt = pointInGasGap->number;

  // Gap Geometry info
  // This is OK ONLY if you want to access x and y informations.
  // Otherwise you need to go down to the volume of gas gap inside
  IPVolume* gasLayer = myCh.getGasGapLayer( gapCnt );

  // Retrieve the chamber box dimensions
  const SolidBox* box = dynamic_cast<const SolidBox*>( gasLayer->lvolume()->solid() );
  double          dx  = box->xHalfLength();
  double          dy  = box->yHalfLength();

  // Refer the distances to Local system [should be the gap]
  Gaudi::XYZPoint new_entry = gasLayer->toLocal( entryInCh );
  Gaudi::XYZPoint new_exit  = gasLayer->toLocal( exitInCh );

  Gaudi::XYZPoint lowerleft  = myCh.geometryPlus()->toGlobal( gasLayer->toMother( Gaudi::XYZPoint{-dx, -dy, 0} ) );
  Gaudi::XYZPoint lowerright = myCh.geometryPlus()->toGlobal( gasLayer->toMother( Gaudi::XYZPoint{dx, -dy, 0} ) );
  Gaudi::XYZPoint upperleft  = myCh.geometryPlus()->toGlobal( gasLayer->toMother( Gaudi::XYZPoint{dx, dy, 0} ) );
  //  Gaudi::XYZPoint upperright  = geoCh->toGlobal(  Gaudi::XYZPoint {-dx,dy,0});

  // Define relative dimensions
  double mod_xen( 0 ), mod_yen( 0 ), mod_xex( 0 ), mod_yex( 0 );
  if ( dx && dy ) {
    if ( ( lowerleft.x() < lowerright.x() ) && ( lowerleft.y() < upperleft.y() ) ) {
      mod_xen = ( new_entry.x() + dx ) / ( 2 * dx );
      mod_yen = ( new_entry.y() + dy ) / ( 2 * dy );
      mod_xex = ( new_exit.x() + dx ) / ( 2 * dx );
      mod_yex = ( new_exit.y() + dy ) / ( 2 * dy );
    } else if ( lowerleft.x() < lowerright.x() && ( lowerleft.y() > upperleft.y() ) ) {
      mod_xen = ( new_entry.x() + dx ) / ( 2 * dx );
      mod_yen = ( -new_entry.y() + dy ) / ( 2 * dy );
      mod_xex = ( new_exit.x() + dx ) / ( 2 * dx );
      mod_yex = ( -new_exit.y() + dy ) / ( 2 * dy );
    } else if ( lowerleft.x() > lowerright.x() && ( lowerleft.y() > upperleft.y() ) ) {
      throw std::logic_error( " Should never enter here " );
    } else if ( lowerleft.x() > lowerright.x() && ( lowerleft.y() < upperleft.y() ) ) {
      throw std::logic_error( " Should never enter here " );
    }
  } else {
    throw std::logic_error( "Null chamber dimensions. Returning an empty list." );
  }

  // Getting the grid pointer
  Condition*       aGrid   = myCh.condition( myCh.getGridName() );
  MuonChamberGrid* theGrid = dynamic_cast<MuonChamberGrid*>( aGrid );
  if ( !theGrid ) { throw std::logic_error( "No grid found. Returning an empty list." ); }
  // Gets list of channels and convert relative distances into absolute ones
  auto myPair = theGrid->listOfPhysChannels( mod_xen, mod_yen, mod_xex, mod_yex );
  for ( auto& [myFE, myVec] : myPair ) {
    assert( myVec.size() == 4 );
    myFE.setLayer( gapCnt / 2 );
    for ( auto&& [i, my] : LHCb::range::enumerate( myVec ) ) {
      my *= 2 * static_cast<float>( i % 2 ? dy : dx );
      // Added resolution effect
      if ( std::abs( my ) < 0.0001 ) my = 0;
    }
  };
  return myPair;
}

std::optional<DeMuonDetector::TilePosition> DeMuonDetector::position( LHCb::Detector::Muon::TileID tile ) const {
  return m_chamberLayout->position( tile );
}

std::optional<Gaudi::XYZPoint> DeMuonDetector::getPCCenter( MuonFrontEndID fe, int chamber, int station,
                                                            int region ) const {

  DeMuonChamber& myCh = getChamber( station, region, chamber );

  IPVolume* myGap = myCh.getGasGapLayer( 0 );

  const SolidBox*  box           = dynamic_cast<const SolidBox*>( myGap->lvolume()->solid() );
  double           dx            = box->xHalfLength();
  double           dy            = box->yHalfLength();
  Condition*       aGrid         = myCh.condition( myCh.getGridName() );
  MuonChamberGrid* theGrid       = static_cast<MuonChamberGrid*>( aGrid );
  double           xcenter_norma = -1;
  double           ycenter_norma = -1;
  StatusCode       sc            = theGrid->getPCCenter( fe, xcenter_norma, ycenter_norma );
  if ( sc.isFailure() ) return {};
  double xcenter_gap = xcenter_norma * 2 * dx - dx;
  double ycenter_gap = ycenter_norma * 2 * dy - dy;

  Gaudi::XYZPoint loc( xcenter_gap, ycenter_gap, 0 );
  Gaudi::XYZPoint refInCh = myGap->toMother( loc );
  return myCh.geometryPlus()->toGlobal( refInCh );
}

LHCb::Detector::Muon::TileID DeMuonDetector::Chamber2Tile( int chaNum, int station, int region ) const {
  return m_chamberLayout->tileChamberNumber( station, region, chaNum );
}

void DeMuonDetector::fillGeoInfo() {
  int station = 0;
  int Side    = 0;
  int region  = 0;

  // fix was in MuonBasicGeometry
  int stationNumber = 0;
  for ( auto* itSt : childIDetectorElements() ) {
    if ( testForFilter( itSt ) ) continue;
    const std::string&     name              = ( itSt )->name();
    std::string::size_type allsize           = name.size();
    std::string::size_type findLastSeparator = name.rfind( "/" );
    std::string            stationName       = name.substr( findLastSeparator + 1, allsize );
    m_stations.emplace_back( stationName, stationNumber );
    stationNumber++;
    auto path = DeMuonLocation::Cabling + "/" + name.substr( name.size() - 2 ) + "/Cabling";
    updMgrSvc()->registerCondition( &m_daqHelper, path, &MuonDAQHelper::updateLUT );
  }

  for ( auto* itSt : childIDetectorElements() ) { // loop over detector elements

    // Check if the "station" isn't actually a filter
    if ( testForFilter( itSt ) ) continue;

    // get station z-coordinates
    IGeometryInfoPlus* geoSt  = itSt->geometryPlus();
    Gaudi::XYZPoint    globSt = geoSt->toGlobal( Gaudi::XYZPoint( 0, 0, 0 ) );
    m_stationZ.push_back( globSt.z() );

    // now crawl down to the chambers

    Side = 0;

    for ( auto* itSide : itSt->childIDetectorElements() ) { // loop over sides

      region = 0;

      for ( auto* itRg : itSide->childIDetectorElements() ) {

        auto* itCh = itRg->childIDetectorElements().front();

        // apparently it loops only over the first chamber to get general information
        // can it be made equivalently using begin() iterator ?
        DeMuonChamber* chPt = dynamic_cast<DeMuonChamber*>( itCh );
        int            gaps = 0;
        double         area = 0;

        for ( auto pvIterator = ( ( ( itCh->geometryPlus() )->lvolume() )->pvBegin() );
              pvIterator != ( ( ( itCh->geometryPlus() )->lvolume() )->pvEnd() ); pvIterator++ ) {

          const ILVolume* geoCh    = ( *pvIterator )->lvolume();
          std::string     lvolname = geoCh->name();
          size_t          pos;
          pos                  = lvolname.find_last_of( "/" );
          std::string mystring = lvolname.substr( pos + 1 );

          if ( mystring == "lvGasGap" ) { // these are the gas gaps
            for ( auto pvGapIterator = ( geoCh->pvBegin() ); pvGapIterator != ( geoCh->pvEnd() ); pvGapIterator++ ) {
              if ( !( ( *pvGapIterator )->lvolume()->sdName().empty() ) ) {
                const ILVolume* geoGap = ( *pvGapIterator )->lvolume();
                // Retrieve the chamber box dimensions
                const SolidBox* box                    = dynamic_cast<const SolidBox*>( geoGap->solid() );
                float           dx                     = box->xHalfLength();
                float           dy                     = box->yHalfLength();
                float           dz                     = box->zHalfLength();
                m_sensitiveAreaX[station * 4 + region] = 2 * dx;
                m_sensitiveAreaY[station * 4 + region] = 2 * dy;
                m_sensitiveAreaZ[station * 4 + region] = 2 * dz;
                area                                   = 4 * dx * dy;
                m_areaChamber[station * 4 + region]    = area;
                gaps++;
              }
            }
          }
        }

        m_gapPerRegion[station * 4 + region] = gaps;
        m_gapPerFE[station * 4 + region]     = gaps / 2;

        Condition*       bGrid   = ( chPt )->condition( chPt->getGridName() );
        MuonChamberGrid* theGrid = static_cast<MuonChamberGrid*>( bGrid );

        // the grid condition is the size of physical readout channels for a certain
        // readout type (i.e. wires or cathode pads) normalized to the
        // number of physical channels of the same type in the chamber
        // By convention 0=wires, 1=cathode pads

        int nreadout = 1;
        if ( theGrid->getGrid2SizeY() > 1 ) nreadout = 2;
        m_readoutNumber[station * 4 + region] = nreadout;
        for ( int i = 0; i < nreadout; i++ ) {
          if ( i == 0 ) {
            m_phChannelNX[i][station * 4 + region] = theGrid->getGrid1SizeX();
            m_phChannelNY[i][station * 4 + region] = theGrid->getGrid1SizeY();
            m_readoutType[i][station * 4 + region] = ( theGrid->getReadoutGrid() )[i];
          }
          if ( i == 1 ) {
            m_phChannelNX[i][station * 4 + region] = theGrid->getGrid2SizeX();
            m_phChannelNY[i][station * 4 + region] = theGrid->getGrid2SizeY();
            m_readoutType[i][station * 4 + region] = ( theGrid->getReadoutGrid() )[i];
          }
        }
        int maps                                = ( theGrid->getMapGrid() ).size() / 2;
        m_LogMapPerRegion[station * 4 + region] = maps;
        if ( nreadout == 1 ) {
          for ( int i = 0; i < maps; i++ ) {
            m_LogMapRType[i][station * 4 + region] = m_readoutType[0][station * 4 + region];
            m_LogMapMergex[i][station * 4 + region] =
                ( theGrid->getMapGrid() )[i * 2]; // these are the log OR of the physical channels
            m_LogMapMergey[i][station * 4 + region] =
                ( theGrid->getMapGrid() )[i * 2 + 1]; // these are the log OR of the physical channels
          }
        } else if ( nreadout == 2 ) {
          for ( int i = 0; i < maps; i++ ) {
            m_LogMapRType[i][station * 4 + region]  = m_readoutType[i][station * 4 + region];
            m_LogMapMergex[i][station * 4 + region] = ( theGrid->getMapGrid() )[i * 2];
            m_LogMapMergey[i][station * 4 + region] = ( theGrid->getMapGrid() )[i * 2 + 1];
          }
        }

        int chamber                              = itRg->childIDetectorElements().size();
        m_chamberPerRegion[station * 4 + region] = chamber * 2;
        region++;
      }
      Side++;
    }
    station++;
  } // end of loop over detector elements

  // initialization by hand of the logical layout in the different regions
  if ( m_isM1defined ) {
    m_layoutX[0][0]  = 24;
    m_layoutX[0][1]  = 24;
    m_layoutX[0][2]  = 24;
    m_layoutX[0][3]  = 24;
    m_layoutX[0][4]  = 48;
    m_layoutX[0][5]  = 48;
    m_layoutX[0][6]  = 48;
    m_layoutX[0][7]  = 48;
    m_layoutX[1][4]  = 8;
    m_layoutX[1][5]  = 4;
    m_layoutX[1][6]  = 2;
    m_layoutX[1][7]  = 2;
    m_layoutX[0][8]  = 48;
    m_layoutX[0][9]  = 48;
    m_layoutX[0][10] = 48;
    m_layoutX[0][11] = 48;
    m_layoutX[1][8]  = 8;
    m_layoutX[1][9]  = 4;
    m_layoutX[1][10] = 2;
    m_layoutX[1][11] = 2;
    m_layoutX[0][12] = 12;
    m_layoutX[0][13] = 12;
    m_layoutX[0][14] = 12;
    m_layoutX[0][15] = 12;
    m_layoutX[1][13] = 4;
    m_layoutX[1][14] = 2;
    m_layoutX[1][15] = 2;
    m_layoutX[0][16] = 12;
    m_layoutX[0][17] = 12;
    m_layoutX[0][18] = 12;
    m_layoutX[0][19] = 12;
    m_layoutX[1][17] = 4;
    m_layoutX[1][18] = 2;
    m_layoutX[1][19] = 2;

    m_layoutY[0][0]  = 8;
    m_layoutY[0][1]  = 8;
    m_layoutY[0][2]  = 8;
    m_layoutY[0][3]  = 8;
    m_layoutY[0][4]  = 1;
    m_layoutY[0][5]  = 2;
    m_layoutY[0][6]  = 2;
    m_layoutY[0][7]  = 2;
    m_layoutY[1][4]  = 8;
    m_layoutY[1][5]  = 8;
    m_layoutY[1][6]  = 8;
    m_layoutY[1][7]  = 8;
    m_layoutY[0][8]  = 1;
    m_layoutY[0][9]  = 2;
    m_layoutY[0][10] = 2;
    m_layoutY[0][11] = 2;
    m_layoutY[1][8]  = 8;
    m_layoutY[1][9]  = 8;
    m_layoutY[1][10] = 8;
    m_layoutY[1][11] = 8;
    m_layoutY[0][12] = 8;
    m_layoutY[0][13] = 2;
    m_layoutY[0][14] = 2;
    m_layoutY[0][15] = 2;
    m_layoutY[1][13] = 8;
    m_layoutY[1][14] = 8;
    m_layoutY[1][15] = 8;
    m_layoutY[0][16] = 8;
    m_layoutY[0][17] = 2;
    m_layoutY[0][18] = 2;
    m_layoutY[0][19] = 2;
    m_layoutY[1][17] = 8;
    m_layoutY[1][18] = 8;
    m_layoutY[1][19] = 8;
  } else if ( m_upgradeReadout ) {
    m_layoutX[0][0]  = 48; // M2 vstrips
    m_layoutX[0][1]  = 48; // M2 vstrips
    m_layoutX[0][2]  = 48; // M2 vstrips
    m_layoutX[0][3]  = 48; // M2 vstrips
    m_layoutX[1][0]  = 8;  // M2 hstrips
    m_layoutX[1][1]  = 4;  // M2 hstrips
    m_layoutX[1][2]  = 0;  // M2 hstrips
    m_layoutX[1][3]  = 0;  // M2 hstrips
    m_layoutX[0][4]  = 48; // M3 vstrips
    m_layoutX[0][5]  = 48; // M3 vstrips
    m_layoutX[0][6]  = 48; // M3 vstrips
    m_layoutX[0][7]  = 48; // M3 vstrips
    m_layoutX[1][4]  = 8;  // M3 hstrips
    m_layoutX[1][5]  = 4;  // M3 hstrips
    m_layoutX[1][6]  = 2;  // M3 hstrips
    m_layoutX[1][7]  = 2;  // M3 hstrips
    m_layoutX[0][8]  = 12; // M4 vstrips
    m_layoutX[0][9]  = 12; // M4 vstrips
    m_layoutX[0][10] = 12; // M4 vstrips
    m_layoutX[0][11] = 12; // M4 vstrips
    m_layoutX[1][8]  = 0;  // M4 hstrips
    m_layoutX[1][9]  = 4;  // M4 hstrips
    m_layoutX[1][10] = 2;  // M4 hstrips
    m_layoutX[1][11] = 2;  // M4 hstrips
    m_layoutX[0][12] = 12; // M5 vstrips
    m_layoutX[0][13] = 12; // M5 vstrips
    m_layoutX[0][14] = 12; // M5 vstrips
    m_layoutX[0][15] = 12; // M5 vstrips
    m_layoutX[1][12] = 0;  // M5 hstrips
    m_layoutX[1][13] = 4;  // M5 hstrips
    m_layoutX[1][14] = 2;  // M5 hstrips
    m_layoutX[1][15] = 0;  // M5 hstrips

    m_layoutY[0][0]  = 1;  // M2 vstrips
    m_layoutY[0][1]  = 2;  // M2 vstrips
    m_layoutY[0][2]  = 8;  // M2 vstrips
    m_layoutY[0][3]  = 8;  // M2 vstrips
    m_layoutY[1][0]  = 8;  // M2 hstrips
    m_layoutY[1][1]  = 16; // M2 hstrips
    m_layoutY[1][2]  = 0;  // M2 hstrips
    m_layoutY[1][3]  = 0;  // M2 hstrips
    m_layoutY[0][4]  = 1;  // M3 vstrips
    m_layoutY[0][5]  = 2;  // M3 vstrips
    m_layoutY[0][6]  = 2;  // M3 vstrips
    m_layoutY[0][7]  = 2;  // M3 vstrips
    m_layoutY[1][4]  = 8;  // M3 hstrips
    m_layoutY[1][5]  = 8;  // M3 hstrips
    m_layoutY[1][6]  = 8;  // M3 hstrips
    m_layoutY[1][7]  = 8;  // M3 hstrips
    m_layoutY[0][8]  = 8;  // M4 vstrips
    m_layoutY[0][9]  = 2;  // M4 vstrips
    m_layoutY[0][10] = 2;  // M4 vstrips
    m_layoutY[0][11] = 2;  // M4 vstrips
    m_layoutY[1][8]  = 0;  // M4 vstrips
    m_layoutY[1][9]  = 8;  // M4 hstrips
    m_layoutY[1][10] = 8;  // M4 hstrips
    m_layoutY[1][11] = 8;  // M4 hstrips
    m_layoutY[0][12] = 8;  // M5 vstrips
    m_layoutY[0][13] = 2;  // M5 vstrips
    m_layoutY[0][14] = 2;  // M5 vstrips
    m_layoutY[0][15] = 8;  // M5 vstrips
    m_layoutY[1][12] = 0;  // M5 hstrips
    m_layoutY[1][13] = 8;  // M5 hstrips
    m_layoutY[1][14] = 8;  // M5 hstrips
    m_layoutY[1][15] = 0;  // M5 hstrips
  } else {
    m_layoutX[0][0]  = 48; // M2 vstrips
    m_layoutX[0][1]  = 48; // M2 vstrips
    m_layoutX[0][2]  = 48; // M2 vstrips
    m_layoutX[0][3]  = 48; // M2 vstrips
    m_layoutX[1][0]  = 8;  // M2 hstrips
    m_layoutX[1][1]  = 4;  // M2 hstrips
    m_layoutX[1][2]  = 2;  // M2 hstrips
    m_layoutX[1][3]  = 2;  // M2 hstrips
    m_layoutX[0][4]  = 48; // M3 vstrips
    m_layoutX[0][5]  = 48; // M3 vstrips
    m_layoutX[0][6]  = 48; // M3 vstrips
    m_layoutX[0][7]  = 48; // M3 vstrips
    m_layoutX[1][4]  = 8;  // M3 hstrips
    m_layoutX[1][5]  = 4;  // M3 hstrips
    m_layoutX[1][6]  = 2;  // M3 hstrips
    m_layoutX[1][7]  = 2;  // M3 hstrips
    m_layoutX[0][8]  = 12; // M4 vstrips
    m_layoutX[0][9]  = 12; // M4 vstrips
    m_layoutX[0][10] = 12; // M4 vstrips
    m_layoutX[0][11] = 12; // M4 vstrips
    m_layoutX[1][9]  = 4;  // M4 hstrips
    m_layoutX[1][10] = 2;  // M4 hstrips
    m_layoutX[1][11] = 2;  // M4 hstrips
    m_layoutX[0][12] = 12; // M5 vstrips
    m_layoutX[0][13] = 12; // M5 vstrips
    m_layoutX[0][14] = 12; // M5 vstrips
    m_layoutX[0][15] = 12; // M5 vstrips
    m_layoutX[1][13] = 4;  // M5 hstrips
    m_layoutX[1][14] = 2;  // M5 hstrips
    m_layoutX[1][15] = 2;  // M5 hstrips

    m_layoutY[0][0]  = 1; // M2 vstrips
    m_layoutY[0][1]  = 2; // M2 vstrips
    m_layoutY[0][2]  = 2; // M2 vstrips
    m_layoutY[0][3]  = 2; // M2 vstrips
    m_layoutY[1][0]  = 8; // M2 hstrips
    m_layoutY[1][1]  = 8; // M2 hstrips
    m_layoutY[1][2]  = 8; // M2 hstrips
    m_layoutY[1][3]  = 8; // M2 hstrips
    m_layoutY[0][4]  = 1; // M3 vstrips
    m_layoutY[0][5]  = 2; // M3 vstrips
    m_layoutY[0][6]  = 2; // M3 vstrips
    m_layoutY[0][7]  = 2; // M3 vstrips
    m_layoutY[1][4]  = 8; // M3 hstrips
    m_layoutY[1][5]  = 8; // M3 hstrips
    m_layoutY[1][6]  = 8; // M3 hstrips
    m_layoutY[1][7]  = 8; // M3 hstrips
    m_layoutY[0][8]  = 8; // M4 vstrips
    m_layoutY[0][9]  = 2; // M4 vstrips
    m_layoutY[0][10] = 2; // M4 vstrips
    m_layoutY[0][11] = 2; // M4 vstrips
    m_layoutY[1][9]  = 8; // M4 hstrips
    m_layoutY[1][10] = 8; // M4 hstrips
    m_layoutY[1][11] = 8; // M4 hstrips
    m_layoutY[0][12] = 8; // M5 vstrips
    m_layoutY[0][13] = 2; // M5 vstrips
    m_layoutY[0][14] = 2; // M5 vstrips
    m_layoutY[0][15] = 2; // M5 vstrips
    m_layoutY[1][13] = 8; // M5 hstrips
    m_layoutY[1][14] = 8; // M5 hstrips
    m_layoutY[1][15] = 8; // M5 hstrips
  }

  // fill pad sizes
  for ( int stat = 0; stat < m_nbStations; stat++ ) {
    for ( int reg = 0; reg < m_regsperSta[stat]; reg++ ) {
      unsigned int part = stat * 4 + reg;
      // one readout
      if ( m_readoutNumber[part] == 1 ) {
        // 1 map
        if ( m_LogMapPerRegion[part] == 1 ) {
          // already pads...
          m_padSizeX[part] = ( m_sensitiveAreaX[part] / m_phChannelNX[0][part] ) * m_LogMapMergex[0][part];
          m_padSizeY[part] = ( m_sensitiveAreaY[part] / m_phChannelNY[0][part] ) * m_LogMapMergey[0][part];

        } else if ( m_LogMapPerRegion[part] == 2 ) {
          int mgx = 0;
          int mgy = 0;
          mgx =
              ( m_LogMapMergex[0][part] > m_LogMapMergex[1][part] ) ? m_LogMapMergex[1][part] : m_LogMapMergex[0][part];
          mgy =
              ( m_LogMapMergey[0][part] > m_LogMapMergey[1][part] ) ? m_LogMapMergey[1][part] : m_LogMapMergey[0][part];
          m_padSizeX[part] = ( m_sensitiveAreaX[part] / m_phChannelNX[0][part] ) * mgx;
          m_padSizeY[part] = ( m_sensitiveAreaY[part] / m_phChannelNY[0][part] ) * mgy;
        }
      } else if ( m_readoutNumber[part] == 2 ) {
        // the convention is always anode first...
        int mgx          = 0;
        int mgy          = 0;
        mgx              = m_LogMapMergex[0][part];
        mgy              = m_LogMapMergey[1][part];
        m_padSizeX[part] = ( m_sensitiveAreaX[part] / m_phChannelNX[0][part] ) * mgx;
        m_padSizeY[part] = ( m_sensitiveAreaY[part] / m_phChannelNY[1][part] ) * mgy;
      }
    }
  }
}

void DeMuonDetector::fillGeoArray() {
  MuonLayout layoutInner = m_chamberLayout->layout( 0 );
  MuonLayout layoutOuter = m_chamberLayout->layout( 3 );
  int        station     = 0;

  for ( auto itSt : childIDetectorElements() ) {
    // get the dimensions of the inner rectangular
    if ( testForFilter( itSt ) ) continue;
    double minX = 100000;
    double minY = 100000;
    for ( unsigned int nx = 0; nx < layoutInner.xGrid(); nx++ ) {
      for ( int qua = 0; qua < 4; qua++ ) {
        LHCb::Detector::Muon::TileID chTile( station, layoutInner, 0, qua, nx, layoutInner.yGrid() );
        int                          chNumber = m_chamberLayout->getChamberNumber( chTile );
        DeMuonChamber&               ch       = getChamber( station, 0, chNumber );
        IPVolume*                    myGapVol = ch.getGasGapLayer( 0 );

        IGeometryInfoPlus* geoCh = ch.geometryPlus();

        // Retrieve the chamber box dimensions
        const SolidBox* box   = dynamic_cast<const SolidBox*>( myGapVol->lvolume()->solid() );
        double          dx    = box->xHalfLength();
        double          dy    = box->yHalfLength();
        Gaudi::XYZPoint glob1 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( -dx, -dy, 0 ) ) );
        Gaudi::XYZPoint glob2 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( -dx, dy, 0 ) ) );
        Gaudi::XYZPoint glob3 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( dx, -dy, 0 ) ) );
        Gaudi::XYZPoint glob4 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( dx, dy, 0 ) ) );
        if ( std::abs( glob1.y() ) < minY ) minY = std::abs( glob1.y() );
        if ( std::abs( glob2.y() ) < minY ) minY = std::abs( glob2.y() );
        if ( std::abs( glob3.y() ) < minY ) minY = std::abs( glob3.y() );
        if ( std::abs( glob4.y() ) < minY ) minY = std::abs( glob4.y() );
      }
    }

    for ( unsigned int ny = 0; ny < layoutInner.yGrid(); ny++ ) {
      for ( int qua = 0; qua < 4; qua++ ) {
        LHCb::Detector::Muon::TileID chTile( station, layoutInner, 0, qua, layoutInner.xGrid(), ny );
        int                          chNumber = m_chamberLayout->getChamberNumber( chTile );
        DeMuonChamber&               ch       = getChamber( station, 0, chNumber );
        IPVolume*                    myGapVol = ch.getGasGapLayer( 0 );

        IGeometryInfoPlus* geoCh = ch.geometryPlus();

        // Retrieve the chamber box dimensions
        const SolidBox* box = dynamic_cast<const SolidBox*>( myGapVol->lvolume()->solid() );

        double          dx    = box->xHalfLength();
        double          dy    = box->yHalfLength();
        Gaudi::XYZPoint glob1 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( -dx, -dy, 0 ) ) );
        Gaudi::XYZPoint glob2 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( -dx, dy, 0 ) ) );
        Gaudi::XYZPoint glob3 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( dx, -dy, 0 ) ) );
        Gaudi::XYZPoint glob4 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( dx, dy, 0 ) ) );
        if ( std::abs( glob1.x() ) < minX ) minX = std::abs( glob1.x() );
        if ( std::abs( glob2.x() ) < minX ) minX = std::abs( glob2.x() );
        if ( std::abs( glob3.x() ) < minX ) minX = std::abs( glob3.x() );
        if ( std::abs( glob4.x() ) < minX ) minX = std::abs( glob4.x() );
      }
    }

    m_stationBox[station][0] = minX;
    m_stationBox[station][1] = minY;
    // now the dimsnion of the outer parr...
    double maxX = 0;
    double maxY = 0;
    for ( unsigned int nx = 0; nx < 2 * layoutOuter.xGrid(); nx++ ) {
      for ( int qua = 0; qua < 4; qua++ ) {

        LHCb::Detector::Muon::TileID chTile( station, layoutOuter, 3, qua, nx, 2 * layoutOuter.yGrid() - 1 );
        int                          chNumber = m_chamberLayout->getChamberNumber( chTile );
        DeMuonChamber&               ch       = getChamber( station, 3, chNumber );
        IPVolume*                    myGapVol = ch.getGasGapLayer( 0 );

        IGeometryInfoPlus* geoCh = ch.geometryPlus();

        // Retrieve the chamber box dimensions
        const SolidBox* box = dynamic_cast<const SolidBox*>( myGapVol->lvolume()->solid() );

        double          dx    = box->xHalfLength();
        double          dy    = box->yHalfLength();
        Gaudi::XYZPoint glob1 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( -dx, -dy, 0 ) ) );
        Gaudi::XYZPoint glob2 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( -dx, dy, 0 ) ) );
        Gaudi::XYZPoint glob3 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( dx, -dy, 0 ) ) );
        Gaudi::XYZPoint glob4 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( dx, dy, 0 ) ) );

        if ( std::abs( glob1.y() ) > maxY ) maxY = std::abs( glob1.y() );
        if ( std::abs( glob2.y() ) > maxY ) maxY = std::abs( glob2.y() );
        if ( std::abs( glob3.y() ) > maxY ) maxY = std::abs( glob3.y() );
        if ( std::abs( glob4.y() ) > maxY ) maxY = std::abs( glob4.y() );
      }
    }

    for ( unsigned int ny = 0; ny < 2 * layoutOuter.yGrid(); ny++ ) {
      for ( int qua = 0; qua < 4; qua++ ) {
        LHCb::Detector::Muon::TileID chTile( station, layoutOuter, 3, qua, 2 * layoutOuter.xGrid() - 1, ny );
        int                          chNumber = m_chamberLayout->getChamberNumber( chTile );
        DeMuonChamber&               ch       = getChamber( station, 3, chNumber );
        IPVolume*                    myGapVol = ch.getGasGapLayer( 0 );

        IGeometryInfoPlus* geoCh = ch.geometryPlus();

        // Retrieve the chamber box dimensions
        const SolidBox* box = dynamic_cast<const SolidBox*>( myGapVol->lvolume()->solid() );

        double          dx    = box->xHalfLength();
        double          dy    = box->yHalfLength();
        Gaudi::XYZPoint glob1 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( -dx, -dy, 0 ) ) );
        Gaudi::XYZPoint glob2 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( -dx, dy, 0 ) ) );
        Gaudi::XYZPoint glob3 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( dx, -dy, 0 ) ) );
        Gaudi::XYZPoint glob4 = geoCh->toGlobal( myGapVol->toMother( Gaudi::XYZPoint( dx, dy, 0 ) ) );
        if ( std::abs( glob1.x() ) > maxX ) maxX = std::abs( glob1.x() );
        if ( std::abs( glob2.x() ) > maxX ) maxX = std::abs( glob2.x() );
        if ( std::abs( glob3.x() ) > maxX ) maxX = std::abs( glob3.x() );
        if ( std::abs( glob4.x() ) > maxX ) maxX = std::abs( glob4.x() );
      }
    }

    m_stationBox[station][2] = maxX;
    m_stationBox[station][3] = maxY;

    station++;
  }
}

int DeMuonDetector::sensitiveVolumeID( const Gaudi::XYZPoint& myPoint ) const {
  // retrieve station,region,chamber,gap:
  int nsta                = getStation( myPoint.z() );
  auto [ngap, nchm, nreg] = Hit2GapNumber( myPoint, nsta );

  // retrieve the quadrant:
  LHCb::Detector::Muon::TileID tile = Pos2ChamberTile( myPoint.x(), myPoint.y(), myPoint.z() );
  unsigned int                 nqua = tile.quarter();
  // pack the integer:
  return ( nqua << PackMCMuonHit::shiftQuadrantID ) | ( nsta << PackMCMuonHit::shiftStationID ) |
         ( nreg << PackMCMuonHit::shiftRegionID ) | ( nchm << PackMCMuonHit::shiftChamberID ) |
         ( ngap << PackMCMuonHit::shiftGapID );
}

int DeMuonDetector::Tile2FirstChamberNumber( LHCb::Detector::Muon::TileID aTile ) const {
  std::vector<int> chamberVector = m_chamberLayout->Tile2ChamberNum( aTile );
  return getChamber( aTile.station(), aTile.region(), chamberVector.front() ).chamberNumber();
}

int DeMuonDetector::Hit2ChamberRegionNumber( Gaudi::XYZPoint myPoint ) const {
  unsigned int istat = getStation( myPoint.z() );
  return Pos2StChamberPointer( myPoint.x(), myPoint.y(), istat ).regionNumber();
}
