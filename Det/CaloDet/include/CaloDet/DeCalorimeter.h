/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifdef USE_DD4HEP

#  include "Detector/Calo/DeCalorimeter.h"
#  include "GaudiKernel/StatusCode.h"
using DeCalorimeter             = LHCb::Detector::Calo::DeCalorimeter;
using CaloPin                   = LHCb::Detector::Calo::Pin;
using CaloLed                   = LHCb::Detector::Calo::Led;
namespace DeCalorimeterLocation = LHCb::Detector::Calo::DeCalorimeterLocation;
namespace CaloPlane             = LHCb::Detector::Calo;
namespace CaloCellQuality       = LHCb::Detector::Calo::CellQuality;
using Tell1Param                = LHCb::Detector::Calo::Tell1Param;
using CellParam                 = LHCb::Detector::Calo::CellParam;

namespace LHCb::Detector::Calo {
  inline StatusCode parse( CaloPlane::Plane& e, std::string const& s ) {
    using namespace CaloPlane;
    for ( auto i : {Plane::Front, Plane::ShowerMax, Plane::Middle, Plane::Back} ) {
      if ( s == toString( i ) ) {
        e = i;
        return StatusCode::SUCCESS;
      }
    }
    return StatusCode::FAILURE;
  }
} // namespace LHCb::Detector::Calo

#else

// ============================================================================
// STD & STL
// ============================================================================
#  include <algorithm>
#  include <iostream>
#  include <map>
#  include <vector>
// ============================================================================
// GaudiKernel
// ============================================================================
#  include "GaudiKernel/MsgStream.h"
#  include "GaudiKernel/Plane3DTypes.h"
#  include "GaudiKernel/Point3DTypes.h"
#  include "GaudiKernel/Transform3DTypes.h"
// ============================================================================
// Det/DetDesc
// ============================================================================
#  include "DetDesc/DetectorElement.h"
#  include "DetDesc/IGeometryInfo.h"
// ============================================================================
// Kernel/LHCbKernel
// ============================================================================
#  include "Detector/Calo/CaloCellCode.h"
#  include "Detector/Calo/CaloCellID.h"
#  include "Kernel/meta_enum.h"
// ============================================================================
// Calo/CaloKernel
// ============================================================================
#  include "CaloKernel/CaloException.h"
#  include "CaloKernel/CaloVector.h"
// ============================================================================
// Det/CaloDet
// ============================================================================
#  include "CaloDet/CLIDDeCalorimeter.h"
#  include "CaloDet/CaloCardParams.h"
#  include "CaloDet/CaloLed.h"
#  include "CaloDet/CaloPin.h"
#  include "CaloDet/CardParam.h"
#  include "CaloDet/CellParam.h"
#  include "CaloDet/Tell1Param.h"
// ============================================================================
/// forward declarations
// ============================================================================
class MsgStream;
class DeSubCalorimeter;
class DeSubSubCalorimeter;
// ============================================================================
/// definition of calorimeter planes
namespace CaloPlane {
  // ==========================================================================
  /// definition of calorimeter planes
  enum Plane { Front = 0, Middle = 1, ShowerMax = 2, Back = 3 };

  inline std::string toString( Plane e ) {
    switch ( e ) {
    case Plane::Front:
      return "Front";
    case Plane::ShowerMax:
      return "ShowerMax";
    case Plane::Middle:
      return "Middle";
    case Plane::Back:
      return "Back";
    default:
      throw "Not a correct plane in Calo";
    }
  }

  inline StatusCode parse( Plane& e, std::string const& s ) {
    for ( auto i : {Plane::Front, Plane::ShowerMax, Plane::Middle, Plane::Back} ) {
      if ( s == toString( i ) ) {
        e = i;
        return StatusCode::SUCCESS;
      }
    }
    return StatusCode::FAILURE;
  }

  inline std::ostream& toStream( Plane e, std::ostream& os ) { return os << std::quoted( toString( e ), '\'' ); }

  inline std::ostream& operator<<( std::ostream& s, Plane e ) { return toStream( e, s ); }

  // ==========================================================================
} //                                                 end of namespace CaloPlane
// ============================================================================
/// Locations of DeCalorimeter detector elements in the TDS
namespace DeCalorimeterLocation {
  inline const std::string Spd  = "/dd/Structure/LHCb/DownstreamRegion/Spd";
  inline const std::string Prs  = "/dd/Structure/LHCb/DownstreamRegion/Prs";
  inline const std::string Ecal = "/dd/Structure/LHCb/DownstreamRegion/Ecal";
  inline const std::string Hcal = "/dd/Structure/LHCb/DownstreamRegion/Hcal";
} // namespace DeCalorimeterLocation
// ============================================================================
/** @class DeCalorimeter CaloDet/DeCalorimeter.h
 *
 *  Calorimeter detector element class.
 *
 *  @author Olivier Callot Olivier.Callot@cern.ch
 *  @author Vanya Belyaev  Ivan.Belyaev@itep.ru
 */
class DeCalorimeter : public DetDesc::DetectorElementPlus {
public:
  using SubCalos = std::vector<const DeSubCalorimeter*>;
  ///  Constructors
  DeCalorimeter( const std::string& name = "" );
  ///  object identification
  static const CLID& classID() { return CLID_DeCalorimeter; }
  ///  object identification
  const CLID& clID() const override;
  ///  printout to std::ostream
  std::ostream& printOut( std::ostream& s = std::cerr ) const override;
  ///  printout to MsgStream
  MsgStream& printOut( MsgStream& ) const override;
  ///  initialization method
  StatusCode initialize() override;

  ///  if initialized in a proper way?
  inline bool isInitialized() const { return m_initialized; }

  // geometry setters (temp)
  void setXSize( const double xSize ) { m_xSize = xSize; }
  void setYSize( const double ySize ) { m_ySize = ySize; }
  void setZSize( const double zSize ) { m_zSize = zSize; }
  void setZOffset( const double zOffset ) { m_zOffset = zOffset; }

  //----------
  // getters
  //--------

  // general
  //--------
  const std::string& caloName() const { return m_caloDet; }
  /// calorimeter index, @see namespace Calo::CellCode
  LHCb::Detector::Calo::CellCode::Index index() const { return m_caloIndex; }
  // accessing the geometry parameters
  //----------------------------------
  // get constant access to subcalorimeters
  const SubCalos& subCalos() const { return m_subCalos; }
  double          xSize() const { return m_xSize; };
  double          ySize() const { return m_ySize; };
  double          zSize() const { return m_zSize; };
  double          zOffset() const { return m_zOffset; };
  unsigned int    numberOfAreas() const { return m_nArea; };
  // reference plane in the global frame
  Gaudi::Plane3D        plane( const double zLocal ) const;
  inline Gaudi::Plane3D plane( const Gaudi::XYZPoint& point ) const;
  inline Gaudi::Plane3D plane( const CaloPlane::Plane pos ) const;

  // accessing the calibration parameters
  //-------------------------------------
  double maxEtInCenter( unsigned int reg = 0 ) const {
    return ( reg < m_maxEtInCenter.size() ) ? m_maxEtInCenter[reg] : m_maxEtInCenter[0];
  };
  double maxEtSlope( unsigned int reg = 0 ) const {
    return ( reg < m_maxEtSlope.size() ) ? m_maxEtSlope[reg] : m_maxEtSlope[0];
  };
  double pedestalShift() const { return m_pedShift; };
  double pinPedestalShift() const { return m_pinPedShift; };
  double L0EtGain() const { return m_l0Et; };
  double coherentNoise() const { return m_cNoise; };
  double incoherentNoise() const { return m_iNoise; };
  double stochasticTerm() const { return m_stoch; };
  double gainError() const { return m_gainE; };

  // for simulation only
  int    zSupMethod() const { return (int)m_zSupMeth; };
  double zSupThreshold() const { return m_zSup; };
  double zSupNeighbourThreshold() const { return m_zSupNeighbour; };
  double spdThreshold( const LHCb::Detector::Calo::CellID& id ) const {
    return ( mipDeposit() != 0 && caloName() == "SpdDet" ) ? cellGain( id ) / 2. / mipDeposit() : 0.;
  };
  double l0Threshold() const { return m_l0Thresh; };
  double mipDeposit() const { return m_mip; };
  double dynamicsSaturation() const { return m_dyn; };
  double fractionFromPrevious() const { return m_prev; };
  double numberOfPhotoElectrons( unsigned int area = 0 ) const { return ( area < m_phe.size() ) ? m_phe[area] : 0.; };
  double l0EtCorrection( unsigned int area = 0 ) const { return ( area < m_l0Cor.size() ) ? m_l0Cor[area] : 1.; };
  double activeToTotal() const { return m_activeToTotal; };

  // accessing the hardware parameter(s)
  // ---------------------------------
  unsigned int adcMax() const { return m_adcMax; };

  // accessing the reconstruction parameter(s)
  double zShowerMax() const { return m_zShowerMax; };

  // accessing readout parameters
  //-----------------------------
  unsigned int numberOfCells() const { return m_cells.size() - numberOfPins(); };
  unsigned int numberOfCards() const { return m_feCards.size(); };
  unsigned int numberOfTell1s() const { return m_tell1Boards.size(); };
  unsigned int numberOfPins() const { return m_pins.size(); };
  unsigned int numberOfLeds() const { return m_leds.size(); };
  unsigned int numberOfInvalidCells() const {
    return std::count_if( m_cells.begin(), m_cells.end(),
                          []( const CellParam& c ) { return !c.cellID().isPin() && !c.valid(); } );
  };
  unsigned int pinArea() const { return m_pinArea; };
  ///  Cell Parameters
  bool   valid( const LHCb::Detector::Calo::CellID& ) const;
  double cellX( const LHCb::Detector::Calo::CellID& ) const;
  double cellY( const LHCb::Detector::Calo::CellID& ) const;
  double cellZ( const LHCb::Detector::Calo::CellID& ) const;
  double cellSize( unsigned int area ) const;
  double cellSize( const LHCb::Detector::Calo::CellID& ) const;
  double cellSine( const LHCb::Detector::Calo::CellID& ) const;
  double cellGain( const LHCb::Detector::Calo::CellID& ) const;
  // convert ADC to energy in MeV for a given cellID
  double cellEnergy( int adc, LHCb::Detector::Calo::CellID id ) const {
    double offset = isPinId( id ) ? pinPedestalShift() : pedestalShift();
    return cellGain( id ) * ( (double)adc - offset );
  };
  // reverse operation : convert energy in MeV to ADC
  double cellADC( double energy, LHCb::Detector::Calo::CellID id ) const {
    double offset = isPinId( id ) ? pinPedestalShift() : pedestalShift();
    double gain   = cellGain( id );
    return ( gain > 0 ) ? floor( energy / gain + offset + 0.5 ) : 0;
  };
  bool isSaturated( double energy, LHCb::Detector::Calo::CellID id ) const {
    return cellADC( energy, id ) + 256 >= adcMax();
  }

  double                cellTime( const LHCb::Detector::Calo::CellID& ) const;
  const Gaudi::XYZPoint cellCenter( const LHCb::Detector::Calo::CellID& ) const;
  const CaloNeighbors&  neighborCells( const LHCb::Detector::Calo::CellID& ) const;
  const CaloNeighbors&  zsupNeighborCells( const LHCb::Detector::Calo::CellID& ) const;
  bool                  hasQuality( const LHCb::Detector::Calo::CellID&, CaloCellQuality::Flag flag ) const;
  bool                  isDead( const LHCb::Detector::Calo::CellID& ) const;
  bool                  isNoisy( const LHCb::Detector::Calo::CellID& ) const;
  bool                  isShifted( const LHCb::Detector::Calo::CellID& ) const;
  bool                  hasDeadLED( const LHCb::Detector::Calo::CellID& ) const;
  bool                  isVeryNoisy( const LHCb::Detector::Calo::CellID& ) const;
  bool                  isVeryShifted( const LHCb::Detector::Calo::CellID& ) const;
  // from cellId to  serial number and vice-versa
  int                          cellIndex( const LHCb::Detector::Calo::CellID& ) const;
  LHCb::Detector::Calo::CellID cellIdByIndex( const unsigned int ) const;
  // from cell to FEB
  bool isReadout( const LHCb::Detector::Calo::CellID& ) const;
  int  cardNumber( const LHCb::Detector::Calo::CellID& ) const;
  int  cardRow( const LHCb::Detector::Calo::CellID& ) const;
  int  cardColumn( const LHCb::Detector::Calo::CellID& ) const;
  void cardAddress( const LHCb::Detector::Calo::CellID& ID, int& card, int& row, int& col ) const;
  //  FEB card
  int                          nCards() const { return m_feCards.size(); }
  int                          downCardNumber( const int card ) const;
  int                          leftCardNumber( const int card ) const;
  int                          cornerCardNumber( const int card ) const;
  int                          previousCardNumber( const int card ) const;
  void                         cardNeighbors( const int card, int& down, int& left, int& corner ) const;
  int                          validationNumber( const int card ) const;
  int                          selectionType( const int card ) const;
  int                          cardArea( const int card ) const;
  int                          cardFirstRow( const int card ) const;
  int                          cardLastColumn( const int card ) const;
  int                          cardLastRow( const int card ) const;
  int                          cardFirstColumn( const int card ) const;
  int                          cardFirstValidRow( const int card ) const;
  int                          cardLastValidRow( const int card ) const;
  int                          cardFirstValidColumn( const int card ) const;
  int                          cardLastValidColumn( const int card ) const;
  LHCb::Detector::Calo::CellID firstCellID( const int card ) const;
  LHCb::Detector::Calo::CellID lastCellID( const int card ) const;
  LHCb::Detector::Calo::CellID cardCellID( const int card, const int row, const int col ) const;
  int                          cardCrate( const int card ) const;
  int                          cardSlot( const int card ) const;
  int                          cardCode( const int card ) const;
  const std::vector<LHCb::Detector::Calo::CellID>& cardChannels( const int card ) const;
  const std::vector<LHCb::Detector::Calo::CellID>& pinChannels( const LHCb::Detector::Calo::CellID ) const;
  const std::vector<LHCb::Detector::Calo::CellID>& ledChannels( const int led ) const;
  const std::vector<int>&                          pinLeds( const LHCb::Detector::Calo::CellID ) const;
  const std::vector<int>&                          cellLeds( const LHCb::Detector::Calo::CellID ) const;
  int                                              cardIndexByCode( const int crate, const int slot ) const;
  int                                              cardToTell1( const int card ) const;
  std::vector<int>                                 sourceID2FEBs( const int source_id ) const;
  int                                              getFEBindex( const int card ) const;
  //
  const std::map<int, std::vector<int>>& getSourceIDsMap() const { return m_source_ids; }
  // from validation to Hcal FEB
  int validationToHcalFEB( const int validation, const unsigned int slot ) const;
  // Tell1s
  int                 nTell1s() const { return m_tell1Boards.size(); }
  Tell1Param::FECards tell1ToCards( const int tell1 ) const;
  int                 nSourceIDs() const { return m_source_ids.size(); }

  // CardParam/Tell1Param/CellParam
  CardParam cardParam( const int card ) const {
    auto i =
        std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
    int ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
    return ( ind != -1 && ind < nCards() ) ? m_feCards[ind] : CardParam();
  }
  Tell1Param tell1Param( const int tell1 ) const {
    return ( tell1 < nTell1s() ) ? m_tell1Boards[tell1] : Tell1Param( -1 );
  }
  CellParam cellParam( LHCb::Detector::Calo::CellID id ) const { return m_cells[id]; }
  CaloPin   caloPin( LHCb::Detector::Calo::CellID id ) const { return m_pins[id]; }
  CaloLed   caloLed( const int led ) const { return ( led < (int)m_leds.size() ) ? m_leds[led] : -1; }
  //  More complex functions
  LHCb::Detector::Calo::CellID             Cell( const Gaudi::XYZPoint& point ) const;
  const CellParam*                         Cell_( const Gaudi::XYZPoint& point ) const;
  LHCb::Detector::Calo::CellCode::CaloArea Area( const Gaudi::XYZPoint& point ) const;
  // Collections
  const CaloVector<CellParam>&   cellParams() const { return m_cells; }
  const CaloVector<CaloPin>&     caloPins() const { return m_pins; }
  const std::vector<CaloLed>&    caloLeds() const { return m_leds; }
  const std::vector<CardParam>&  cardParams() const { return m_feCards; }
  const std::vector<Tell1Param>& tell1Params() const { return m_tell1Boards; }

  // PIN flag
  bool isParasiticCard( const int card ) const {
    auto i =
        std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
    int ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
    return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].isParasitic() : false;
  };
  bool isPmtCard( const int card ) const {
    auto i =
        std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
    int ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
    return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].isPmtCard() : false;
  };
  bool isPinCard( const int card ) const {
    auto i =
        std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
    int ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
    return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].isPinCard() : false;
  };
  bool isPinTell1( const int tell1 ) const { return ( tell1 < nTell1s() ) ? m_tell1Boards[tell1].readPin() : false; };
  bool isPinId( LHCb::Detector::Calo::CellID id ) const { return (unsigned)m_pinArea == id.area(); }
  // pileUp subtraction parameters
  int pileUpSubstractionMethod() const { return m_puMeth; }
  int pileUpSubstractionBin() const { return m_puBin; }
  int pileUpSubstractionMin() const { return m_puMin; }

  double getCalibration( const LHCb::Detector::Calo::CellID& ) const;

private:
  ///  Initialization method for building the cells/cards/tell1/PIN layout
  StatusCode buildCells();
  StatusCode buildSourceID2FEBs();
  StatusCode buildCards();
  StatusCode buildTell1s();
  StatusCode buildMonitoring();
  StatusCode getCalibration();
  StatusCode getPileUpOffset();
  StatusCode getL0Calibration();
  StatusCode getLEDReference();
  StatusCode getQuality();
  StatusCode getNumericGains();

private:
  DeCalorimeter( const DeCalorimeter& ) = delete;
  DeCalorimeter& operator=( const DeCalorimeter& ) = delete;

  // Calo id
  std::string                           m_caloDet;
  LHCb::Detector::Calo::CellCode::Index m_caloIndex = LHCb::Detector::Calo::CellCode::Index::Undefined;

  // init flag
  bool m_initialized = false;

  // geometry
  double       m_YToXSizeRatio = std::numeric_limits<double>::signaling_NaN();
  double       m_xSize         = std::numeric_limits<double>::signaling_NaN();
  double       m_ySize         = std::numeric_limits<double>::signaling_NaN();
  double       m_zSize         = std::numeric_limits<double>::signaling_NaN();
  double       m_zOffset       = std::numeric_limits<double>::signaling_NaN();
  SubCalos     m_subCalos;
  unsigned int m_nArea{};

  // hardware
  int    m_adcMax{};
  int    m_coding{};
  int    m_centralHoleX{};
  int    m_centralHoleY{};
  int    m_maxRowCol{};
  int    m_firstRowUp{};
  double m_centerRowCol = std::numeric_limits<double>::signaling_NaN();

  // calibration
  std::vector<double> m_maxEtInCenter;
  std::vector<double> m_maxEtSlope;
  double              m_pedShift      = std::numeric_limits<double>::signaling_NaN();
  double              m_pinPedShift   = std::numeric_limits<double>::signaling_NaN();
  double              m_l0Et          = std::numeric_limits<double>::signaling_NaN();
  double              m_activeToTotal = std::numeric_limits<double>::signaling_NaN();
  double              m_stoch         = std::numeric_limits<double>::signaling_NaN();
  double              m_gainE         = std::numeric_limits<double>::signaling_NaN();
  double              m_cNoise        = std::numeric_limits<double>::signaling_NaN();
  double              m_iNoise        = std::numeric_limits<double>::signaling_NaN();
  double              m_zSupMeth      = std::numeric_limits<double>::signaling_NaN();
  double              m_zSup          = std::numeric_limits<double>::signaling_NaN();
  double              m_zSupNeighbour = std::numeric_limits<double>::signaling_NaN();
  double              m_mip           = std::numeric_limits<double>::signaling_NaN();
  double              m_dyn           = std::numeric_limits<double>::signaling_NaN();
  double              m_prev          = std::numeric_limits<double>::signaling_NaN();
  double              m_l0Thresh      = std::numeric_limits<double>::signaling_NaN();
  std::vector<double> m_phe;
  std::vector<double> m_l0Cor;
  int                 m_puMeth = -1;
  int                 m_puBin  = 0;
  int                 m_puMin  = 0;

  // reconstruction
  double m_zShowerMax = std::numeric_limits<double>::signaling_NaN();

  // readout
  int m_pinArea = -1;

  ///  Collections
  std::vector<LHCb::Detector::Calo::CellID> m_empty;
  CaloVector<CellParam>                     m_cells;
  CaloVector<CaloPin>                       m_pins;
  std::vector<CardParam>                    m_feCards;
  std::vector<Tell1Param>                   m_tell1Boards;
  std::vector<CaloLed>                      m_leds;
  std::vector<int>                          m_pinTell1s;
  std::map<int, std::vector<int>>           m_valCards;
  std::map<int, std::vector<int>>           m_source_ids;
  std::vector<double>                       m_cellSizes;

  // conditions
  SmartRef<Condition> m_pileUp;
  SmartRef<Condition> m_calib;
  SmartRef<Condition> m_l0calib;
  SmartRef<Condition> m_gain;
  SmartRef<Condition> m_quality;
  SmartRef<Condition> m_LEDReference;
  SmartRef<Condition> m_reco;
  SmartRef<Condition> m_hardware;
  SmartRef<Condition> m_readout;
  SmartRef<Condition> m_monitor;
  SmartRef<Condition> m_numericGains;
  bool                loadCondition( SmartRef<Condition>& cond, std::string name, bool mandatory = false );

  StatusCode updHardware();
  StatusCode updGeometry();
  StatusCode updGain();
  StatusCode updCalib();
  StatusCode updL0Calib();
  StatusCode updLEDReference();
  StatusCode updQuality();
  StatusCode updReadout();
  StatusCode updMonitor();
  StatusCode updReco();
  StatusCode updNumGains();
  StatusCode updPileUp();
  // ===========================================================================
  /** ouput operator for class DeCalorimeter
   *  @see DeCalorimeter
   *  @param os reference to standard STL/STD output stream
   *  @param de reference to DeCalorimeter object
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   */
  friend std::ostream& operator<<( std::ostream& os, const DeCalorimeter& de ) { return de.printOut( os ); }
  // ===========================================================================
  // ===========================================================================
  /** ouput operator for class DeCalorimeter
   *  @see DeCalorimeter
   *  @see MsgStream
   *  @param os reference to Gaudi message output stream
   *  @param de reference to DeCalorimeter object
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   */
  friend MsgStream& operator<<( MsgStream& os, const DeCalorimeter& de ) { return de.printOut( os ); }
  // ===========================================================================
};

// ===========================================================================
/** ouput operator for class DeCalorimeter
 *  @see DeCalorimeter
 *  @param os reference to standard STL/STD output stream
 *  @param de pointer to DeCalorimeter object
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 */
inline std::ostream& operator<<( std::ostream& os, const DeCalorimeter* de ) {
  return de ? ( os << *de ) : ( os << " DeCalorimeter* points to NULL!" << std::endl );
}
// ===========================================================================

// ===========================================================================
/** ouput operator for class DeCalorimeter
 *  @see DeCalorimeter
 *  @see MsgStream
 *  @param os reference to Gaudi message output stream
 *  @param de pointer to DeCalorimeter object
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 */
inline MsgStream& operator<<( MsgStream& os, const DeCalorimeter* de ) {
  return de ? ( os << *de ) : ( os << " DeCalorimeter* points to NULL!" << endmsg );
}
// ===========================================================================

// ===========================================================================
//  validity flag for the cell
// ===========================================================================
inline bool DeCalorimeter::valid( const LHCb::Detector::Calo::CellID& ID ) const { return m_cells[ID].valid(); }
// ===========================================================================
//  Quality flag for the cell
// ===========================================================================
inline bool DeCalorimeter::hasQuality( const LHCb::Detector::Calo::CellID& ID, CaloCellQuality::Flag flag ) const {
  int quality = m_cells[ID].quality();
  return flag == CaloCellQuality::OK ? quality == 0 : ( ( quality & flag ) != 0 );
}
inline bool DeCalorimeter::isDead( const LHCb::Detector::Calo::CellID& ID ) const {
  return hasQuality( ID, CaloCellQuality::Dead );
}
inline bool DeCalorimeter::isNoisy( const LHCb::Detector::Calo::CellID& ID ) const {
  return hasQuality( ID, CaloCellQuality::Noisy );
}
inline bool DeCalorimeter::isShifted( const LHCb::Detector::Calo::CellID& ID ) const {
  return hasQuality( ID, CaloCellQuality::Shifted );
}
inline bool DeCalorimeter::hasDeadLED( const LHCb::Detector::Calo::CellID& ID ) const {
  return hasQuality( ID, CaloCellQuality::DeadLED );
}
inline bool DeCalorimeter::isVeryNoisy( const LHCb::Detector::Calo::CellID& ID ) const {
  return hasQuality( ID, CaloCellQuality::VeryNoisy );
}
inline bool DeCalorimeter::isVeryShifted( const LHCb::Detector::Calo::CellID& ID ) const {
  return hasQuality( ID, CaloCellQuality::VeryShifted );
}
// ===========================================================================
//  x-position of center of the cell
// ===========================================================================
inline double DeCalorimeter::cellX( const LHCb::Detector::Calo::CellID& ID ) const { return m_cells[ID].x(); }
//  ===========================================================================
//  y-position of center of the cell
//  ===========================================================================
inline double DeCalorimeter::cellY( const LHCb::Detector::Calo::CellID& ID ) const { return m_cells[ID].y(); }

// ===========================================================================
//  z-position of center of the cell
// ===========================================================================
inline double DeCalorimeter::cellZ( const LHCb::Detector::Calo::CellID& ID ) const { return m_cells[ID].z(); }

// ===========================================================================
//  cell size
// ===========================================================================
inline double DeCalorimeter::cellSize( unsigned int area ) const { return m_cellSizes[area]; }
inline double DeCalorimeter::cellSize( const LHCb::Detector::Calo::CellID& ID ) const { return m_cells[ID].size(); }

// ===========================================================================
//  sine function for given cell
// ===========================================================================
inline double DeCalorimeter::cellSine( const LHCb::Detector::Calo::CellID& ID ) const { return m_cells[ID].sine(); }

// ===========================================================================
//  PM gain for given cell
// ===========================================================================
inline double DeCalorimeter::cellGain( const LHCb::Detector::Calo::CellID& ID ) const { return m_cells[ID].gain(); }

// ===========================================================================
//  Timing for for given cell
// ===========================================================================
inline double DeCalorimeter::cellTime( const LHCb::Detector::Calo::CellID& ID ) const { return m_cells[ID].time(); }

// ===========================================================================
//  cell center
// ===========================================================================
inline const Gaudi::XYZPoint DeCalorimeter::cellCenter( const LHCb::Detector::Calo::CellID& ID ) const {
  return m_cells[ID].center();
}

// ============================================================================
//  list of neighbour cells
// ============================================================================
inline const CaloNeighbors& DeCalorimeter::neighborCells( const LHCb::Detector::Calo::CellID& ID ) const {
  return m_cells[ID].neighbors();
}

// ============================================================================
//  list of neighbour cells
// ============================================================================
inline const CaloNeighbors& DeCalorimeter::zsupNeighborCells( const LHCb::Detector::Calo::CellID& ID ) const {
  return m_cells[ID].zsupNeighbors();
}

// ============================================================================
//  From ID to cell serial number and vice-versa
// ============================================================================
inline int DeCalorimeter::cellIndex( const LHCb::Detector::Calo::CellID& ID ) const { return m_cells.index( ID ); }

// ============================================================================
//  From ID to cell serial number and vice-versa
// ============================================================================
inline LHCb::Detector::Calo::CellID DeCalorimeter::cellIdByIndex( const unsigned int num ) const {
  return ( ( num < m_cells.size() ) ? ( m_cells.begin() + num )->cellID() : LHCb::Detector::Calo::CellID() );
}

inline bool DeCalorimeter::isReadout( const LHCb::Detector::Calo::CellID& ID ) const {
  return ( m_cells[ID].cardNumber() >= 0 ) ? true : false;
}
// ============================================================================
//  card number
// ============================================================================
inline int DeCalorimeter::cardNumber( const LHCb::Detector::Calo::CellID& ID ) const {
  return m_cells[ID].cardNumber();
}
// ============================================================================
//  card row
// ============================================================================
inline int DeCalorimeter::cardRow( const LHCb::Detector::Calo::CellID& ID ) const { return m_cells[ID].cardRow(); }
// ============================================================================
//  card column
// ============================================================================
inline int DeCalorimeter::cardColumn( const LHCb::Detector::Calo::CellID& ID ) const {
  return m_cells[ID].cardColumn();
}
// ============================================================================
//  card address
// ============================================================================
inline void DeCalorimeter::cardAddress( const LHCb::Detector::Calo::CellID& ID, int& card, int& row,
                                        int& column ) const {
  card   = m_cells[ID].cardNumber();
  row    = m_cells[ID].cardRow();
  column = m_cells[ID].cardColumn();
}

// ============================================================================
//  down card number
// ============================================================================
inline int DeCalorimeter::downCardNumber( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;

  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].downNumber() : -1;
}

// ============================================================================
//  left card number
// ============================================================================
inline int DeCalorimeter::leftCardNumber( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].leftNumber() : -1;
}

// ============================================================================
//  corder card number
// ============================================================================
inline int DeCalorimeter::cornerCardNumber( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].cornerNumber() : -1;
}

// ============================================================================
//  previous card number
// ============================================================================
inline int DeCalorimeter::previousCardNumber( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].previousNumber() : -1;
}

// ============================================================================
//  validation card number
// ============================================================================
inline int DeCalorimeter::validationNumber( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].validationNumber() : -1;
}

// ============================================================================
//  selection board type
// ============================================================================
inline int DeCalorimeter::selectionType( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].selectionType() : -1;
}

// ============================================================================
//  card neighbours
// ============================================================================
inline void DeCalorimeter::cardNeighbors( const int card, int& down, int& left, int& corner ) const {
  down   = -1;
  left   = -1;
  corner = -1;
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  if ( ind != -1 && ind < nCards() ) {
    down   = m_feCards[ind].downNumber();
    left   = m_feCards[ind].leftNumber();
    corner = m_feCards[ind].cornerNumber();
  }
}

// ============================================================================
//  card area
// ============================================================================
inline int DeCalorimeter::cardArea( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].area() : -1;
}

// ============================================================================
//  card first row
// ============================================================================
inline int DeCalorimeter::cardFirstRow( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].firstRow() : -1;
}

// ============================================================================
//  card first column
// ============================================================================
inline int DeCalorimeter::cardFirstColumn( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].firstColumn() : -1;
}

// ============================================================================
//  card last row
// ============================================================================
inline int DeCalorimeter::cardLastRow( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].lastRow() : -1;
}

// ============================================================================
//  card last column
// ============================================================================
inline int DeCalorimeter::cardLastColumn( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].lastColumn() : -1;
}

// ============================================================================
//  valid boundaries :  first/last col/row with a valid cell
// ============================================================================
inline int DeCalorimeter::cardFirstValidRow( const int card ) const {
  LHCb::Detector::Calo::CellID cell = firstCellID( card );
  if ( valid( cell ) ) return cell.row();

  for ( int irow = cardFirstRow( card ); irow <= cardLastRow( card ); ++irow ) {
    for ( int icol = cardFirstColumn( card ); icol <= cardLastColumn( card ); ++icol ) {
      LHCb::Detector::Calo::CellID test( cell.calo(), cell.area(), irow, icol );
      if ( valid( test ) ) return irow;
    }
  }
  return -1;
}

inline int DeCalorimeter::cardFirstValidColumn( const int card ) const {
  LHCb::Detector::Calo::CellID cell = firstCellID( card );
  if ( valid( cell ) ) return cell.col();
  for ( int irow = cardFirstRow( card ); irow <= cardLastRow( card ); ++irow ) {
    for ( int icol = cardFirstColumn( card ); icol <= cardLastColumn( card ); ++icol ) {
      LHCb::Detector::Calo::CellID test( cell.calo(), cell.area(), irow, icol );
      if ( valid( test ) ) return icol;
    }
  }
  return -1;
}

inline int DeCalorimeter::cardLastValidRow( const int card ) const {
  LHCb::Detector::Calo::CellID cell = lastCellID( card );
  if ( valid( cell ) ) return cell.row();

  for ( int irow = cardLastRow( card ); irow >= cardFirstRow( card ); --irow ) {
    for ( int icol = cardLastColumn( card ); icol >= cardFirstColumn( card ); --icol ) {
      LHCb::Detector::Calo::CellID test( cell.calo(), cell.area(), irow, icol );
      if ( valid( test ) ) return irow;
    }
  }
  return -1;
}
inline int DeCalorimeter::cardLastValidColumn( const int card ) const {
  LHCb::Detector::Calo::CellID cell = lastCellID( card );
  if ( valid( cell ) ) return cell.col();

  for ( int irow = cardLastRow( card ); irow >= cardFirstRow( card ); --irow ) {
    for ( int icol = cardLastColumn( card ); icol >= cardFirstColumn( card ); --icol ) {
      LHCb::Detector::Calo::CellID test( cell.calo(), cell.area(), irow, icol );
      if ( valid( test ) ) return icol;
    }
  }
  return -1;
}

// ===========================================================================
//  ID of the bottom left cell, of the top right cell, of the specified cell
// ===========================================================================
inline LHCb::Detector::Calo::CellID DeCalorimeter::firstCellID( const int card ) const {

  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;

  return ( ind != -1 && ind < nCards() )
             ? LHCb::Detector::Calo::CellID( m_caloIndex, m_feCards[ind].area(), m_feCards[ind].firstRow(),
                                             m_feCards[ind].firstColumn() )
             : LHCb::Detector::Calo::CellID();
}

// ===========================================================================
//  ID of the bottom left cell, of the top right cell, of the specified cell
// ===========================================================================
inline LHCb::Detector::Calo::CellID DeCalorimeter::lastCellID( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() )
             ? LHCb::Detector::Calo::CellID( m_caloIndex, m_feCards[ind].area(),
                                             m_feCards[ind].firstRow() + nRowCaloCard - 1,
                                             m_feCards[ind].firstColumn() + nColCaloCard - 1 )
             : LHCb::Detector::Calo::CellID();
}

// ===========================================================================
//  ID of the bottom left cell, of the top right cell, of the specified cell
// ===========================================================================
inline LHCb::Detector::Calo::CellID DeCalorimeter::cardCellID( const int card, const int row, const int col ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() )
             ? LHCb::Detector::Calo::CellID( m_caloIndex, m_feCards[ind].area(), m_feCards[ind].firstRow() + row,
                                             m_feCards[ind].firstColumn() + col )
             : LHCb::Detector::Calo::CellID();
}

// ============================================================================
//  FEcard -> Tell1
// ============================================================================
inline int DeCalorimeter::cardToTell1( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].tell1() : -1;
}
// ============================================================================
//  Validation -> Hcal FEB
// ============================================================================
inline int DeCalorimeter::validationToHcalFEB( const int validation, const unsigned int slot ) const {
  auto it = m_valCards.find( validation );
  if ( it != m_valCards.end() ) {
    const auto& hcalFe = it->second;
    if ( slot < hcalFe.size() ) return hcalFe[slot];
  }
  return -1;
}

// ============================================================================
//  Source_ID -> Vector of FEB
// ============================================================================
inline std::vector<int> DeCalorimeter::sourceID2FEBs( const int source_id ) const {
  auto it = m_source_ids.find( source_id );
  if ( it != m_source_ids.end() ) {
    const auto& vec = it->second;
    return vec;
  }
  return {};
}

// ============================================================================
//  FEB number -> FEB vector index (m_feCards)
// ============================================================================
inline int DeCalorimeter::getFEBindex( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ind;
}

// ============================================================================
//  Tell1 -> FECards
// ============================================================================
inline Tell1Param::FECards DeCalorimeter::tell1ToCards( const int tell1 ) const {
  return ( tell1 >= 0 && tell1 < nTell1s() ) ? m_tell1Boards[tell1].feCards() : Tell1Param::FECards{};
}

// ============================================================================
//  card Id & content
// ============================================================================
inline int DeCalorimeter::cardCrate( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].crate() : -1;
}

inline int DeCalorimeter::cardSlot( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].slot() : -1;
}

inline int DeCalorimeter::cardCode( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].code() : -1;
}

inline const std::vector<LHCb::Detector::Calo::CellID>& DeCalorimeter::cardChannels( const int card ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(), [&]( const CardParam& c ) { return c.number() == card; } );
  int  ind = i != m_feCards.end() ? i - m_feCards.begin() : -1;
  return ( ind != -1 && ind < nCards() ) ? m_feCards[ind].ids() : m_empty;
}

inline const std::vector<LHCb::Detector::Calo::CellID>&
DeCalorimeter::pinChannels( const LHCb::Detector::Calo::CellID id ) const {
  return m_pins[id].cells();
}

inline const std::vector<LHCb::Detector::Calo::CellID>& DeCalorimeter::ledChannels( const int led ) const {
  return led < (int)m_leds.size() ? m_leds[led].cells() : m_empty;
}

inline const std::vector<int>& DeCalorimeter::pinLeds( const LHCb::Detector::Calo::CellID id ) const {
  return m_pins[id].leds();
}

inline const std::vector<int>& DeCalorimeter::cellLeds( const LHCb::Detector::Calo::CellID id ) const {
  return m_cells[id].leds();
}

inline int DeCalorimeter::cardIndexByCode( const int crate, const int slot ) const {
  auto i = std::find_if( m_feCards.begin(), m_feCards.end(),
                         [&]( const CardParam& c ) { return c.crate() == crate && c.slot() == slot; } );
  return i != m_feCards.end() ? i - m_feCards.begin() : -1;
}
// ============================================================================
/// Return a reference (tilted) plane
// ============================================================================
inline Gaudi::Plane3D DeCalorimeter::plane( const CaloPlane::Plane pos ) const {
  switch ( pos ) {
  case CaloPlane::Front:
    return plane( m_zOffset - m_zSize / 2. );
  case CaloPlane::ShowerMax:
    return plane( m_zShowerMax );
  case CaloPlane::Middle:
    return plane( m_zOffset );
  case CaloPlane::Back:
    return plane( m_zOffset + m_zSize / 2. );
  default:
    return plane( m_zOffset );
  }
}
// ============================================================================
/// return a 3D-plane, which contain the given 3D-point in the global system
// ============================================================================
inline Gaudi::Plane3D DeCalorimeter::plane( const Gaudi::XYZPoint& global ) const {
  return plane( geometryPlus()->toLocal( global ).Z() );
}

// ============================================================================
/// return Calibration constant for a given CellID
// ============================================================================
inline double DeCalorimeter::getCalibration( const LHCb::Detector::Calo::CellID& ID ) const {
  return m_cells[ID].calibration();
}

#endif
