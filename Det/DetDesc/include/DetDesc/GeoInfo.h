/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/IDetectorElement.h"
#include "DetDesc/IGeometryInfo.h"
#include "DetDesc/ILVolume.h"

#include <string>

using namespace DetDesc;

/**
 *  collection of "creational" methods for
 *  creation of IGeometryInfoPlus objects
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   10/08/2001
 */
namespace GeoInfo {

  /** create "ghost" geometry info object
   *  @exception GeometryInfoException null IDetectorElementPlus pointer
   *  @param de pointer to detector element
   *  @return pointer to IGeometryInfoPlus object
   */
  IGeometryInfoPlus* createGeometryInfo( IDetectorElementPlus* de );

  /** create "orphan" geometry info object
   *  @exception GeometryInfoException null IDetectorElementPlus pointer
   *  @param de pointer to detector element
   *  @param LV name/address fo logical volume
   *  @return pointer to IGeometryInfoPlus object
   */
  IGeometryInfoPlus* createGeometryInfo( IDetectorElementPlus* de, const std::string& LV );

  /** create regular geometry infor element
   *  @exception GeometryInfoException null IDetectorElementPlus pointer
   *  @param de              pointer to detector element
   *  @param LogVol          name of logical volume
   *  @param Support         name of support element
   *  @param ReplicaNamePath replica path/address
   *  @param alignmentPath   address of alignment condition
   */
  IGeometryInfoPlus* createGeometryInfo( IDetectorElementPlus* de, const std::string& LogVol,
                                         const std::string& Support, const std::string& ReplicaNamePath,
                                         const std::string& alignmentPath = "" );

  /** create regular geometry infor element
   *  @exception GeometryInfoException null IDetectorElementPlus pointer
   *  @param de              pointer to detector element
   *  @param LogVol          name of logical volume
   *  @param Support         name of support element
   *  @param ReplicaPath     replica path
   *  @param alignmentPath   address of alignment condition
   */
  IGeometryInfoPlus* createGeometryInfo( IDetectorElementPlus* de, const std::string& LogVol,
                                         const std::string& Support, const ILVolume::ReplicaPath& ReplicaPath,
                                         const std::string& alignmentPath = "" );
} // namespace GeoInfo
