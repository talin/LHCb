/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <DetDesc/Condition.h>
#include <Event/ODIN.h>
#include <Gaudi/Property.h>
#include <GaudiKernel/GaudiException.h>
#include <GaudiKernel/IDataProviderSvc.h>
#include <GaudiKernel/IUpdateManagerSvc.h>
#include <GaudiKernel/Time.h>
#include <LHCbAlgs/Consumer.h>
#include <VPDet/DeVP.h>
#include <XmlTools/IXmlSvc.h>
#include <atomic>
#include <fmt/format.h>
#include <mutex>
#include <string>
#include <tuple>
#include <yaml-cpp/yaml.h>

namespace LHCb {
  namespace DetDesc {
    /// Algorithm to extract the Velo Motion System data from YAML conditions and
    /// push it into the legacy (DetDesc) transient store.
    ///
    /// Since this algorithm has to modify the content of the Detector Transient
    /// Store before the UpdateManagerSvc runs the update, it is mandatory that
    /// it is executed before LHCb::DetDesc::ReserveDetDescForEvent (for example
    /// enforcing the order using a control flow dependency).
    struct VeloMotionSystemFromYaml : LHCb::Algorithm::Consumer<void( const ODIN& )> {
      VeloMotionSystemFromYaml( const std::string& name, ISvcLocator* pSvcLocator )
          : Consumer( name, pSvcLocator, KeyValue( "ODIN", LHCb::ODINLocation::Default ) ) {}
      StatusCode initialize() override {
        return Consumer::initialize().andThen( [this] { return m_xmlSvc.retrieve(); } );
      }
      /// If the run number changed since the previous event, we get the Velo Motion System alignent
      /// condition from the YAML condition directory and update the value in the detector transient
      /// store if needed, in which case we also notify teh UpdateManagerSvc that an update of
      /// downstream conditions is needed too.
      void operator()( const ODIN& odin ) const override {
        // Prevent concurrent executions of the algorithm
        std::lock_guard lock{m_mutex};
        const auto      currentRunNumber = odin.runNumber();
        // We have to synchronize between multiple invocations of the method.
        if ( currentRunNumber != m_latestRunNumber ) {
          // This is not the same run we encountered
          if ( msgLevel( MSG::DEBUG ) ) debug() << "Current run: " << currentRunNumber << endmsg;
          m_latestRunNumber = currentRunNumber;
          // get MotionSystem condition from DTS
          DataObject* obj = nullptr;
          detSvc()
              ->retrieveObject( m_condPath, obj )
              .andThen( [obj, currentRunNumber, this]() {
                if ( Condition* cond = dynamic_cast<Condition*>( obj ) ) {
                  // make sure we are the only ones updating the condition
                  cond->setValidity( Gaudi::Time::epoch(), Gaudi::Time::max() );
                  // extract MotionSystem parameters (as reference so we can update them if needed)
                  std::tuple<double&, double&, double&> cond_values{cond->param<double>( "ResolPosLA" ),
                                                                    cond->param<double>( "ResolPosRC" ),
                                                                    cond->param<double>( "ResolPosY" )};
                  // helper to evaluate string expressions
                  auto eval = [this]( const YAML::Node& node ) { return m_xmlSvc->eval( node.as<std::string>() ); };
                  // load YAML file
                  auto       yaml_file = fmt::format( "{}/{}", m_yamlPath.value(), currentRunNumber );
                  YAML::Node node;
                  try {
                    node = YAML::LoadFile( yaml_file );
                  } catch ( YAML::BadFile const& ) {
                    throw GaudiException( fmt::format( "Failed to load YAML file {}", yaml_file ), name(),
                                          StatusCode::FAILURE );
                  }
                  auto left_pos  = node["MotionVPLeft"]["position"];
                  auto right_pos = node["MotionVPRight"]["position"];
                  // YAML sanity checks
                  if ( !left_pos || !right_pos )
                    throw GaudiException( fmt::format( "missing Velo Motion System data in YAML file {}", yaml_file ),
                                          name(), StatusCode::FAILURE );
                  auto left_y  = eval( left_pos[1] );
                  auto right_y = eval( right_pos[1] );
                  if ( left_y != right_y )
                    throw GaudiException(
                        fmt::format( "inconsistent Velo Motion System Y values in YAML: {}(left) != {}(right)",
                                     left_pos[1].as<std::string>(), right_pos[1].as<std::string>() ),
                        name(), StatusCode::FAILURE );
                  if ( eval( left_pos[2] ) != 0.0 || eval( right_pos[2] ) != 0.0 )
                    throw GaudiException(
                        fmt::format( "invalid Velo Motion System Z values in YAML: {}(left), {}(right) (expected 0)",
                                     left_pos[2].as<std::string>(), right_pos[2].as<std::string>() ),
                        name(), StatusCode::FAILURE );

                  std::tuple yaml_values{eval( left_pos[0] ), eval( right_pos[0] ), left_y};

                  if ( cond_values != yaml_values ) {
                    cond_values = yaml_values;
                    service<IUpdateManagerSvc>( "UpdateManagerSvc" )->invalidate( cond );
                  }
                } else {
                  throw GaudiException( fmt::format( "object at {} is not a Condition instance", m_condPath.value() ),
                                        name(), StatusCode::FAILURE );
                }
              } )
              .orThrow( fmt::format( "failed to access condition {}", m_condPath.value() ), name() );
        }
      };
      Gaudi::Property<std::string> m_yamlPath{
          this,
          "YAMLPath",
          {},
          "Path to the YAML CondDB entry for the Velo Motion System special alignment conditions"};
      Gaudi::Property<std::string> m_condPath{
          this, "Condition", "Conditions/Online/Velo/MotionSystem",
          "Path to the Velo Motion System condition in the DetDesc Detector Transient Store"};

      ServiceHandle<IXmlSvc> m_xmlSvc{this, "XmlCnvSvc", "XmlCnvSvc"};

      /// Record the last run number seen to know if we have to try another update or not
      mutable std::uint32_t m_latestRunNumber{static_cast<std::uint32_t>( -1 )};
      /// Mutex to prevent concurrent executions of the algorithm
      mutable std::mutex m_mutex;
    };
    DECLARE_COMPONENT( VeloMotionSystemFromYaml )

    struct TestBeamSpot : LHCb::Algorithm::Consumer<void( const DeVP& ), LHCb::DetDesc::usesConditions<DeVP>> {
      TestBeamSpot( const std::string& name, ISvcLocator* pSvcLocator )
          : Consumer( name, pSvcLocator, KeyValue( "DeVP", DeVPLocation::Default ) ) {}

      void operator()( const DeVP& vp ) const override {
        auto beamSpot = vp.beamSpot();
        info() << "BeamSpot: (" << beamSpot.x() << ", " << beamSpot.y() << ", " << beamSpot.z() << ")" << endmsg;
      }
    };
    DECLARE_COMPONENT( TestBeamSpot )
  } // namespace DetDesc
} // namespace LHCb
