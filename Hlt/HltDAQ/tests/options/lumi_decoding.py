###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
from Gaudi.Configuration import VERBOSE, DEBUG
from PRConfig.TestFileDB import test_file_db

from PyConf.application import (
    default_raw_event,
    default_raw_banks,
    configure_input,
    configure,
    make_odin,
    CompositeNode,
)
from PyConf.Algorithms import (
    HltRoutingBitsFilter,
    HltLumiSummaryDecoder,
    HltLumiSummaryMonitor,
    PrintHeader,
)
from Configurables import LHCb__UnpackRawEvent

options = test_file_db["hltlumisummary-raw-data-v2"].make_lbexec_options(
    simulation=True,
    dddb_tag="dddb-20210617",
    conddb_tag="sim-20210617-vc-md100",
    python_logging_level=logging.WARNING,
    evt_max=100,
    histo_file='lumi_decoding_histo.root')

configure_input(options)  # must call this before calling default_raw_event
odin = make_odin()

default_raw_event.bind(raw_event_format=0.5)
rb_bank = default_raw_banks('HltRoutingBits', default_raw_event)

rb_filter = HltRoutingBitsFilter(
    RequireMask=(1 << 1, 0, 0), RawBanks=rb_bank, PassOnError=False)
summary = HltLumiSummaryDecoder(
    RawBanks=default_raw_banks("HltLumiSummary"), ).OutputContainerName
monitor = HltLumiSummaryMonitor(Input=summary, ODIN=odin, OutputLevel=VERBOSE)
print_event = PrintHeader(ODINLocation=odin)

top_node = CompositeNode("Top", [rb_filter, print_event, monitor])
configure(options, top_node)

from Configurables import LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV
reserveIOV("reserveIOV").PreloadGeometry = False
