from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import bankKiller, RawEventDump, GaudiSequencer, CombineRawBankViewsToRawEvent
from Configurables import LHCbApp

import os
if os.path.exists("rewritten.mdf"):
    print("EXISTS")
    os.system("rm rewritten.mdf")
else:
    print("NOTHING!")

LHCbApp().EvtMax = 10
dump = RawEventDump(RawBanks=[], DumpData=True)
decoders = GaudiSequencer("Decode", Members=[])
killer = bankKiller(BankTypes=[])
killer2 = bankKiller("Killer2", BankTypes=[])
encoders = GaudiSequencer("Encode", Members=[])

import Configurables
from Configurables import IODataManager
IODataManager().DisablePFNWarning = True


def propertyName(io, t):
    if t == 'Sel' and io == 'Input': return 'SelReports'
    return "{0}Hlt{1}ReportsLocation".format(io, t)


locs = {"Decoder": "Output", "Writer": "Input"}

bankTypes = ["Sel", "Dec"]

confUnp = getattr(Configurables, "LHCb__UnpackRawEvent")("UnpackRawEvent")
confUnp.RawBankLocations = [
    "DAQ/RawBanks/HltSelReports", "DAQ/RawBanks/HltDecReports"
]
confUnp.BankTypes = ["HltSelReports", "HltDecReports"]
decoders.Members.append(confUnp.getFullName())

from itertools import product
for stage, t, (seq, at) in product((1, 2), bankTypes, ((decoders, "Decoder"),
                                                       (encoders, "Writer"))):
    algType = "Hlt{0}Reports{1}".format(t, at)
    algName = "Hlt{0}{1}Reports{2}".format(stage, t, at)
    conf = getattr(Configurables, algType)(algName)
    conf.SourceID = "Hlt{0}".format(stage)

    conf.setProp(
        propertyName(locs[at], t), "Hlt{0}/{1}Reports".format(stage, t))

    if (at == "Decoder"):
        conf.RawBanks = "DAQ/RawBanks/Hlt{0}Reports".format(t)

    if (t == "Sel" and at == "Decoder"):
        conf.setProp("OutputHltObjectSummariesLocation",
                     "Hlt{0}/{1}Reports/Candidates".format(stage, t))
    if (t == "Sel" and at == "Writer"):
        conf.setProp("DecReports", "Hlt{0}/DecReports".format(stage))
        conf.setProp("ObjectSummaries", "Hlt{0}/{1}Reports/Candidates".format(
            stage, t))
        conf.setProp("RawEvent", "/Event/DAQ/Hlt{0}SelEvent".format(stage))
        conf.setProp("OutputView", "/Event/DAQ/Hlt{0}SelView".format(stage))
    if (t == "Dec" and at == "Writer"):
        #Want writers to use RawBank::Views and not alter input RawEvent - see !LHCb3303 MR description
        #For now have to send the HLT1 and HLT2 banks to different *new* RawEvent/RawBank::View output locations
        conf.setProp("OutputRawEvent",
                     "/Event/DAQ/Hlt{0}DecEvent".format(stage))
        conf.setProp("OutputView", "/Event/DAQ/Hlt{0}DecView".format(stage))
        conf.setProp("EncodingKey", 0x010600a2)
        conf.setProp("DecoderMapping", "HltANNSvc")
    if (t == 'Sel' and at == 'Writer'):
        conf.setProp('ANNSvc', 'HltANNSvc')
        conf.setProp('EncodingKey', 0x010600a2)

    conf.OutputLevel = 2
    seq.Members.append(conf.getFullName())

##Temporary for backwards compatibility - merge the HLT1 and HLT2 RawBank::Views back into the default RawEvent
views_to_raw_event = CombineRawBankViewsToRawEvent(
    RawBankViews=[
        "/Event/DAQ/Hlt{0}{1}View".format(i, b)
        for i, b in product((1, 2), bankTypes)
    ],
    RawEvent="/Event/DAQ/NewRawEvent")

encoders.Members.append(views_to_raw_event)

for atype in bankTypes:
    bank = "Hlt" + atype + "Reports"
    dump.RawBanks.append(bank)
    killer.BankTypes.append(bank)

killer2.BankTypes = killer.BankTypes + ["ODIN"]
killer2.DefaultIsKill = True

from Configurables import TCKANNSvc
hltann = TCKANNSvc('HltANNSvc')

appmgr = ApplicationMgr()
appmgr.TopAlg += [dump, decoders, killer, encoders, killer2]
appmgr.ExtSvc += [hltann]

#writer
from GaudiConf import IOHelper
from Configurables import LHCb__MDFWriter
IOHelper('MDF', 'MDF').outStream(
    "rewritten.mdf", LHCb__MDFWriter("writer", Compress=0), writeFSR=False)

#inputfile
from PRConfig import TestFileDB
tf = TestFileDB.test_file_db["2015_raw_full"]
tf.run()
from GaudiConf import IOExtension
IOExtension().inputFiles(tf.filenames, clear=True)
