from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import ApplicationMgr
from Configurables import RawEventDump, GaudiSequencer
from Configurables import LHCbApp
from Configurables import HltSelReportsWriter, HltDecReportsWriter, HltSelReportsDecoder, HltDecReportsDecoder
from Configurables import LHCb__UnpackRawEvent, CombineRawBankViewsToRawEvent
NEW_RAW_EVENT_LOC = "/Event/DAQ/NewRawEvent"

import os
if os.path.exists("rewritten.mdf"):
    print("EXISTS")
    os.system("rm rewritten.mdf")
else:
    print("NOTHING!")

LHCbApp().EvtMax = 10
dump = RawEventDump(RawBanks=[], DumpData=True)
decoders = GaudiSequencer("Decode", Members=[])
encoders = GaudiSequencer("Encode", Members=[])

from Configurables import IODataManager
IODataManager().DisablePFNWarning = True

# FIXME: we first add HltSelReportsWriter, which is Functional and
# creates a new RawEvent. HltDecReportsWriter is not yet functional so
# it can modify the new RawEvent in place.

unpacker = LHCb__UnpackRawEvent("UnpackRawEvent")
unpacker.RawBankLocations = [
    "DAQ/RawBanks/HltSelReports", "DAQ/RawBanks/HltDecReports"
]
unpacker.BankTypes = ["HltSelReports", "HltDecReports"]

dump.RawBanks.append("HltSelReports")
dump.RawBanks.append("HltDecReports")

dec_decoder = HltDecReportsDecoder(
    RawBanks=unpacker.RawBankLocations[1], SourceID="Hlt1")
sel_decoder = HltSelReportsDecoder(
    RawBanks=unpacker.RawBankLocations[0],
    DecReports=dec_decoder.OutputHltDecReportsLocation,
    SourceID="Hlt1")
decoders.Members.append(unpacker)
decoders.Members.append(dec_decoder)
decoders.Members.append(sel_decoder)

sel_writer = HltSelReportsWriter(
    SelReports=sel_decoder.OutputHltSelReportsLocation,
    ObjectSummaries=sel_decoder.OutputHltObjectSummariesLocation,
    DecReports=dec_decoder.OutputHltDecReportsLocation,
    SourceID="Hlt1")

dec_writer = HltDecReportsWriter(
    InputHltDecReportsLocation=dec_decoder.OutputHltDecReportsLocation,
    SourceID="Hlt1")

combine_views = CombineRawBankViewsToRawEvent(
    RawBankViews=[dec_writer.OutputView, sel_writer.OutputView],
    RawEvent=NEW_RAW_EVENT_LOC)

encoders.Members.append(sel_writer)
encoders.Members.append(dec_writer)
encoders.Members.append(combine_views)

from Configurables import HltANNSvc
HltANNSvc().Hlt1SelectionID = {'PV3D': 10103, 'ProtoPV3D': 10117}

ApplicationMgr().TopAlg += [dump, decoders, encoders]

#writer
from GaudiConf import IOHelper
from Configurables import LHCb__MDFWriter
IOHelper('MDF', 'MDF').outStream(
    "rewritten.mdf",
    LHCb__MDFWriter("writer", Compress=0, BankLocation=combine_view.RawEvent),
    writeFSR=False)

#inputfile
from PRConfig import TestFileDB
tf = TestFileDB.test_file_db["2015_raw_full"]
tf.run()
from GaudiConf import IOExtension
IOExtension().inputFiles(tf.filenames, clear=True)
