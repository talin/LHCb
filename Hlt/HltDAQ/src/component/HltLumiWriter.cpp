/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltLumiSummary.h"
#include "Event/RawEvent.h"
#include "Kernel/IIndexedLumiSchemaSvc.h"
#include "Kernel/STLExtensions.h"
#include "LHCbAlgs/Transformer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltLumiWriter
//
//-----------------------------------------------------------------------------

/** @class HltLumiWriter HltLumiWriter.h
 *  Fills the Raw Buffer banks for the LumiSummary
 */
class HltLumiWriter : public LHCb::Algorithm::MultiTransformer<std::tuple<LHCb::RawEvent, LHCb::RawBank::View>(
                                                                   const LHCb::HltLumiSummary& ),
                                                               LHCb::Algorithm::Traits::writeOnly<LHCb::RawEvent>> {

  mutable Gaudi::Accumulators::AveragingCounter<> m_totDataSize{this, "Average event size / 32-bit words"};

public:
  HltLumiWriter( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformer{name,
                         pSvcLocator,
                         KeyValue{"InputBank", LHCb::HltLumiSummaryLocation::Default},
                         {KeyValue{"RawEventLocation", "/Event/DAQ/HltLumiEvent"},
                          KeyValue{"OutputBank", "DAQ/RawBanks/HltLumiSummary"}}} {}

  //=============================================================================
  // Main execution
  //  Fill the data bank, structure depends on encoding key
  //=============================================================================
  std::tuple<LHCb::RawEvent, LHCb::RawBank::View> operator()( const LHCb::HltLumiSummary& summary ) const override {
    if ( summary.extraInfo().find( "encodingKey" ) == summary.extraInfo().end() ) {
      throw std::runtime_error( "HltLumiSummary does not contain an encoding key" );
    }
    auto encodingKey = summary.extraInfo().at( "encodingKey" );
    auto lumi_schema = m_svc->lumiCounters( encodingKey, 0 );

    std::vector<unsigned int> bank;
    bank.resize( lumi_schema.size / 4 );
    for ( auto cntr : lumi_schema.counters ) {
      if ( summary.extraInfo().find( cntr.name ) != summary.extraInfo().end() ) {
        auto val   = summary.extraInfo().at( cntr.name );
        auto shift = cntr.shift;
        auto scale = cntr.scale;
        if ( shift == 0.f && scale == 1.f ) {
          writeCounter( cntr.offset, cntr.size, &bank.front(), val );
        } else {
          auto scaled_val = static_cast<unsigned>( shift + scale * val );
          writeCounter( cntr.offset, cntr.size, &bank.front(), scaled_val );
        }
      } else {
        warning() << "Field " << cntr.name << " ignored by lumiSummary schema " << encodingKey << endmsg;
      }
    }

    LHCb::RawEvent rawEvent;
    // set source, type, version
    rawEvent.addBank( 0, LHCb::RawBank::HltLumiSummary, 2, bank );

    m_totDataSize += bank.size();

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Bank size: " << format( "%4d ", bank.size() ) << "Total Data bank size " << bank.size() << endmsg;
    }

    if ( msgLevel( MSG::VERBOSE ) ) {
      verbose() << "DATA bank : " << endmsg;
      for ( const auto& [k, w] : LHCb::range::enumerate( bank, 1 ) ) {
        verbose() << format( " %8x %11d   ", w, w );
        if ( k % 4 == 0 ) verbose() << endmsg;
      }
      verbose() << endmsg;
    }
    auto view = rawEvent.banks( LHCb::RawBank::HltLumiSummary );

    // without std::move here the RawEvent gets copied which would invalidate the view
    // View creation must be after RawEvent is made
    return {std::move( rawEvent ), std::move( view )};
  }

  void writeCounter( unsigned offset, unsigned size, unsigned* target, unsigned value ) const {
    // Check value fits within size bits
    if ( size < ( 8 * sizeof( unsigned ) ) && value >= ( 1u << size ) ) { return; }

    // Separate offset into a word part and bit part
    unsigned word      = offset / ( 8 * sizeof( unsigned ) );
    unsigned bitoffset = offset % ( 8 * sizeof( unsigned ) );

    // Check size and offset line up with word boundaries
    if ( bitoffset + size > ( 8 * sizeof( unsigned ) ) ) { return; }

    // Apply the value to the matching bits
    unsigned mask = ( ( 1ul << size ) - 1 ) << bitoffset;
    target[word]  = ( target[word] & ~mask ) | ( ( value << bitoffset ) & mask );
  }

private:
  ServiceHandle<IIndexedLumiSchemaSvc> m_svc{this, "DecoderMapping", "HltANNSvc"};
};

DECLARE_COMPONENT( HltLumiWriter )
