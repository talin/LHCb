/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReports.h"
#include "Event/RawEvent.h"
#include "HltSourceID.h"
#include "Kernel/IIndexedANNSvc.h"
#include "LHCbAlgs/Transformer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltDecReportsWriter
//
// 2008-07-26 : Tomasz Skwarnicki
//-----------------------------------------------------------------------------

/** @class DecReportsWriter HltDecReportsWriter.h
 *
 *
 *  @author Tomasz Skwarnicki
 *  @date   2008-07-26
 *
 *  Algorithm to convert HltDecReports container on TES to *new* RawEvent and Raw Bank View
 *
 */

namespace LHCb::Hlt::DAQ {

  class DecReportsWriter
      : public Algorithm::MultiTransformer<std::tuple<RawEvent, RawBank::View>( HltDecReports const& ),
                                           Algorithm::Traits::writeOnly<RawEvent>> {

    /// SourceID to insert in the bank header
    Gaudi::Property<SourceID> m_sourceID{this, "SourceID", SourceID::Dummy};
    Gaudi::Property<bool>     m_verify_decoder_mapping{this, "VerifyDecoderMapping", true};
    Gaudi::Property<unsigned> m_encodingKey{this, "EncodingKey", 0,
                                            "if non-zero, ignore TCK in decreport, and use this value"};

    ServiceHandle<IIndexedANNSvc> m_svc{this, "DecoderMapping", "HltANNSvc"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_removed_old_banks{
        this, " Deleted previously inserted HltDecReports bank ", 50};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_missing_mapping{
        this, " No string key found for trigger decision", 50};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_wrong_mapping{
        this, " Wrong string key found for trigger decision", 50};

  public:
    enum HeaderIDs { kVersionNumber = 3 };

    DecReportsWriter( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer{name,
                           pSvcLocator,
                           // Input
                           KeyValue{"InputHltDecReportsLocation", HltDecReportsLocation::Default},
                           // Outputs
                           {KeyValue{"OutputRawEvent", "/Event/DAQ/HltDecEvent"},
                            KeyValue{"OutputView", "/Event/DAQ/HltDec/View"}}} {};

    std::tuple<RawEvent, RawBank::View> operator()( HltDecReports const& inputSummary ) const override {

      // Create *new* RawEvent
      RawEvent outputrawevent;

      assert( m_sourceID == SourceID::Hlt1 || m_sourceID == SourceID::Hlt2 || m_sourceID == SourceID::Spruce );
      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << " Input: ";
        for ( const auto& rep : inputSummary ) {
          auto decRep = HltDecReport{rep.second.decReport()};
          verbose() << decRep.intDecisionID() << "-" << decRep.decision() << " ";
        }
        verbose() << endmsg;
      }
      // verify the string <-> int mapping that will be done by the decoding later...
      if ( m_verify_decoder_mapping ) {
        const auto& tbl = m_svc->i2s( m_encodingKey, selectionID_for( m_sourceID.value() ) );
        for ( const auto& [name, dec] : inputSummary ) {
          auto i = tbl.find( dec.intDecisionID() );
          if ( i == tbl.end() ) {
            ++m_missing_mapping;
          } else if ( i->second != name ) {
            ++m_wrong_mapping;
          }
        }
      }

      // compose the bank body
      std::vector<unsigned int> bankBody;
      bankBody.reserve( inputSummary.size() + 3 );
      bankBody.push_back( m_encodingKey.value() ); // new in version 3
      bankBody.push_back( inputSummary.configuredTCK() );
      bankBody.push_back( inputSummary.taskID() );
      std::transform( std::begin( inputSummary ), std::end( inputSummary ), std::back_inserter( bankBody ),
                      []( const auto& r ) { return r.second.decReport(); } );

      // order according to the values, essentially orders by intDecisionID
      // this is important since it will put "*Global" reports at the beginning of the bank
      // NOTE: we must skip the first two words (configuredTCK, taskID)
      std::sort( std::next( std::begin( bankBody ), 2 ), std::end( bankBody ) );

      // shift bits in sourceID for the same convention as in HltSelReports
      outputrawevent.addBank( shift<SourceIDMasks::Process>( m_sourceID ), RawBank::HltDecReports, kVersionNumber,
                              bankBody );

      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << " Output:  ";
        verbose() << " VersionNumber= " << kVersionNumber;
        verbose() << " SourceID= " << m_sourceID.value();
        auto i = std::begin( bankBody );
        verbose() << " key: " << *i++ << " ";
        verbose() << " configuredTCK = " << *i++ << " ";
        verbose() << " taskID = " << *i++ << " ";
        while ( i != std::end( bankBody ) ) {
          auto rep = HltDecReport{*i++};
          verbose() << rep.intDecisionID() << "-" << rep.decision() << " ";
        }
        verbose() << endmsg;
      }

      // make sure view creation happens
      //    a) after the RawEvent is fully populated
      //    b) prior to moving RawEvent
      // -- note: cannot use the generic `writeViewFor` mechanism here as that requires
      // the view to be constructible solely from the source, which it isn't here as it
      // somehow needs to know which banktype, i.e. RawBank::HltDecReports in this case,
      // to view...
      auto view = outputrawevent.banks( RawBank::HltDecReports );
      // without std::move here the RawEvent gets copied which would invalidate the view
      return {std::move( outputrawevent ), std::move( view )};
    };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( DecReportsWriter, "HltDecReportsWriter" )

} // namespace LHCb::Hlt::DAQ
