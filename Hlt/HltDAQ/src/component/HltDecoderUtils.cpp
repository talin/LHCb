/***************************************************************************** \
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "HltDecoderUtils.h"
#include "GaudiKernel/GaudiException.h"
#include <algorithm>
#include <vector>

namespace LHCb::Hlt::DAQ {

  namespace {

    template <typename F, typename Raw>
    unsigned int getDatum( SourceID id, const Raw& raw, F f ) {

      auto banks = selectRawBanks( id, raw );
      if ( banks.empty() ) return 0u;

      if ( banks[0]->type() != RawBank::HltDecReports )
        throw GaudiException( "Bank type to get the key or tck should be HltDecReports", __PRETTY_FUNCTION__,
                              StatusCode::FAILURE );

      auto d = f( banks.front() );

      // check if we have more than one bank, and if so, whether there is any
      // disagreement in the TCK amongs them... (which would be bad)
      if ( banks.size() > 1 && std::any_of( std::next( std::begin( banks ) ), std::end( banks ),
                                            [&]( const RawBank* rb ) { return f( rb ) != d; } ) ) {
        throw GaudiException( "got multiple Hlt decreports rawbanks (compatible) with specified sourceid but with "
                              "different header data... ",
                              __PRETTY_FUNCTION__, StatusCode::FAILURE );
      }
      return d;
    }
  } // namespace

  std::vector<const RawBank*> selectRawBanks( SourceID sourceID, RawBank::View rawbanks ) {
    if ( sourceID == SourceID::Dummy )
      throw GaudiException( "Invalid sourceID speciified", __PRETTY_FUNCTION__, StatusCode::FAILURE );

    std::vector<const RawBank*> mybanks;
    mybanks.reserve( rawbanks.size() );
    std::copy_if( std::begin( rawbanks ), std::end( rawbanks ), std::back_inserter( mybanks ),
                  [sourceID]( const RawBank* bank ) {
                    switch ( bank->type() ) {
                    case RawBank::HltSelReports:
                      return sourceID == getSourceID<RawBank::HltSelReports>( *bank );
                    case RawBank::DstData:
                      return bank->version() < 3 ||
                             sourceID == getSourceID<RawBank::DstData>( *bank ); // before version 3, sourceID was not
                                                                                 // recorded in DstData
                    case RawBank::HltDecReports:
                      return sourceID == getSourceID<RawBank::HltDecReports>( *bank );
                    default:
                      throw GaudiException( "Unsupported bank type", __PRETTY_FUNCTION__, StatusCode::FAILURE );
                    }
                  } );
    return mybanks;
  }

  unsigned int tck( SourceID id, const RawEvent& raw, RawBank::BankType type ) {
    auto banks = selectRawBanks( id, type, raw );
    return tck( id, banks );
  }

  unsigned int key( SourceID id, const RawEvent& raw, RawBank::BankType type ) {
    auto banks = selectRawBanks( id, type, raw );
    return key( id, banks );
  }

  unsigned int tck( SourceID id, const RawBank::View& raw ) {
    return getDatum( id, raw, []( const RawBank* rb ) {
      if ( rb && rb->magic() == RawBank::MagicPattern && rb->version() > 0 && rb->size() > 0 ) {
        return rb->range<unsigned int>()[rb->version() < 3 ? 0 : 1];
      }
      return 0u;
    } );
  }

  unsigned int key( SourceID id, const RawBank::View& raw ) {
    return getDatum( id, raw, []( const RawBank* rb ) {
      if ( rb && rb->magic() == RawBank::MagicPattern && rb->size() > 0 ) { return rb->range<unsigned int>().front(); }
      return 0u;
    } );
  }

} // namespace LHCb::Hlt::DAQ
