/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedData.h"
#include "Event/PackedDataBuffer.h"
#include "Event/PackerBase.h"
#include "Event/RawBank.h"
#include "Event/StandardPacker.h"
#include "HltDecoderUtils.h"
#include "Kernel/IIndexedANNSvc.h"
#include "LHCbAlgs/Transformer.h"
#include "RZip.h"

/** @class ...
 *
 * Decoding of PackedObjects from raw event
 * output locations are determined from
 *
 *  @author Sevda Esen
 *  @date   2021-07-15
 */

namespace LHCb::Hlt::DAQ {

  namespace {
    uint32_t to_uint32( LHCb::span<std::byte const, 4> s ) {
      // uhh.. don't worry about endianess/alignment ???
      return *reinterpret_cast<const uint32_t*>( s.data() );
    }
  } // namespace

  class HltPackedBufferDecoder final
      : public Algorithm::Transformer<PackedData::MappedInBuffers( RawBank::View const& )> {

  public:
    // Standard constructor
    HltPackedBufferDecoder( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, {"RawBanks", "/Event/DAQ/RawBanks/DstData"},
                       {"OutputBuffers", "/Event/DAQ/MappedDstData"} ) {}
    struct MetaInfo {
      std::uint32_t           key;
      PackedData::Compression compression;
      span<std::byte>         content;
    };

    std::optional<MetaInfo> getMetaInfo( span<std::byte> payload, const RawBank::View& banks ) const {

      switch ( banks[0]->version() ) {
      case 2:
        return MetaInfo{DAQ::tck( m_sourceID, *m_decreports.get() ), compression( *banks[0] ).value(), payload};
      case 3:
        return MetaInfo{to_uint32( payload.subspan<0, 4>() ),
                        static_cast<PackedData::Compression>( to_uint32( payload.subspan<4, 4>() ) & 0x7 ),
                        payload.subspan<8>()};
      default:
        return std::nullopt;
      }
    }

    StatusCode initialize() override {
      auto sc = Transformer::initialize();
      if ( m_sourceID.value() == DAQ::SourceID::Dummy ) {
        error() << "please explicitly configure SourceID";
        return StatusCode::FAILURE;
      }
      return sc;
    }

    PackedData::MappedInBuffers operator()( RawBank::View const& rawBanksConst ) const override {

      if ( rawBanksConst.empty() ) {
        ++m_no_packed_bank;
        return {};
      }

      const auto* rawBank0 = rawBanksConst.front();
      if ( rawBank0->type() != RawBank::DstData ) {
        throw GaudiException( " Wrong bank type for HltPackedBufferDecoder", name(), StatusCode::FAILURE );
      }
      if ( std::any_of( begin( rawBanksConst ), end( rawBanksConst ),
                        [v = rawBank0->version()]( const RawBank* b ) { return b->version() != v; } ) ) {
        ++m_inconsistent_version;
        return {};
      }

      PackedData::MappedInBuffers buffers{0, rawBank0->version(), m_sourceID.value()};

      auto rawBanks = selectRawBanks( m_sourceID, rawBanksConst );
      if ( rawBanks.empty() ) return buffers;

      std::sort( begin( rawBanks ), end( rawBanks ),
                 []( const RawBank* lhs, const RawBank* rhs ) { return lhs->sourceID() < rhs->sourceID(); } );

      PackedData::ByteBuffer::buffer_type payload;
      payload.reserve( rawBanks.front()->size() * rawBanks.size() );

      for ( auto [i, rawBank] : range::enumerate( rawBanks ) ) {
        if ( getSequenceNumber<RawBank::BankType::DstData>( *rawBank ) != i ) {
          ++m_not_sequential;
          return buffers;
        }

        if ( rawBank->size() == 8 ) ++m_only_header_in_bank;

        // Collect the data into a contiguous buffer
        auto r = rawBank->range<std::byte>();
        payload.insert( end( payload ), begin( r ), end( r ) );
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "Rawbank size " << rawBank->size() << endmsg;
          debug() << "Payload size " << payload.size() << endmsg;
        }
      }

      // Check we know how to decode this version
      auto info = getMetaInfo( payload, rawBanks );
      if ( !info ) {
        ++m_bad_version;
        return buffers;
      }

      if ( info->key == 0 && m_forcedKey.value() == 0 ) ++m_not_overruled_zero_key;

      if ( m_forcedKey.value() != 0 ) {
        if ( info->key != 0 ) ++m_overrule_nonzero_key;
        info->key = m_forcedKey.value();
      }

      PackedData::PackedDataInBuffer buffer{info->key};
      // Decompress the payload and load into a one big buffer
      switch ( info->compression ) {
      case PackedData::Compression::NoCompression: {
        buffer.init( info->content );
        break;
      }
      case PackedData::Compression::ZLIB:
      case PackedData::Compression::LZMA:
      case PackedData::Compression::LZ4:
      case PackedData::Compression::ZSTD: {
        if ( !buffer.init( info->content, true ) ) { ++m_decompression_failure; }
        break;
      }
      default: {
        ++m_unknown_compression;
        break;
      }
      }

      while ( !buffer.eof() ) {
        // keep track of the start of this (sub) buffer
        auto pos = buffer.buffer().pos();

        PackedData::ObjectHeader header{buffer};

        // as we kept track of where we are, we skip over the payload to the next item
        buffer.skip( header.storedSize );

        // create a new buffer, and copy content out of the big buffer
        // starting from (current position - pos ) = size of the buffer
        auto [ibuf, ok] = buffers.try_emplace( header.locationID, buffer, pos );
        if ( !ok ) throw GaudiException( "Duplicate locationID", __PRETTY_FUNCTION__, StatusCode::FAILURE );

        // verify the key is propagated...
        assert( ibuf->second.key() == buffer.key() );
        assert( ibuf->second.key() == info->key );

        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << " added buffer for locationID " << ibuf->first << " with size " << ibuf->second.buffer().size()
                  << " and key " << ibuf->second.key() << endmsg;
        }
      }

      if ( msgLevel( MSG::DEBUG ) ) debug() << buffers << endmsg;

      return buffers;
    }

  private:
    DataObjectReadHandle<RawBank::View> m_decreports{this, "DecReports", "DAQ/RawBanks/HltDecReports"};

    Gaudi::Property<DAQ::SourceID> m_sourceID{this, "SourceID", DAQ::SourceID::Dummy};
    Gaudi::Property<unsigned>      m_forcedKey{this, "ForcedKey", 0,
                                          "if non-zero, ignore TCK in decreport, and use this value"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_overrule_nonzero_key{
        this,
        " HltPackedData has a non-zero encoding key, but is explicitly overruled for decoding -- make sure that this "
        "really what you want"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_not_overruled_zero_key{
        this, " DstData raw bank  has a zero encoding key, and it is not explicitly specified for decoding -- make "
              "sure that this "
              "really what you want"};
    mutable Gaudi::Accumulators::Counter<> m_only_header_in_bank{
        this, " DstData raw bank seems to only contain a header, no other data "};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_bad_version{
        this, "HltPackedData raw bank version is not supported."};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_inconsistent_version{
        this, "HltPackedData not all raw banks have the same version"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_not_sequential{
        this, "Part IDs for HltPackedData banks are not sequential."};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_decompression_failure{this,
                                                                                "Failed to decompress HltPackedData."};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_unknown_compression{this, "Unknown compression method."};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_unknown_packed_location{
        this, "Packed object location not found in ANNSvc. Skipping this link, unpacking may fail"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_packed_bank{
        this, "No HltPackedData raw bank (the DstData bank) in raw event."};
  };

  DECLARE_COMPONENT_WITH_ID( HltPackedBufferDecoder, "HltPackedBufferDecoder" )

} // namespace LHCb::Hlt::DAQ
