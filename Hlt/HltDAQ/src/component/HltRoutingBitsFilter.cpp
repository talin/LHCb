/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "GaudiAlg/FunctionalUtilities.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "LHCbAlgs/FilterPredicate.h"
#include <vector>

//-----------------------------------------------------------------------------
// Implementation file for class : HltRoutingBitsFilter
//
// 2008-07-29 : Gerhard Raven
//-----------------------------------------------------------------------------

class HltRoutingBitsFilter : public LHCb::Algorithm::FilterPredicate<bool( LHCb::RawBank::View const& )> {
public:
  HltRoutingBitsFilter( const std::string& name, ISvcLocator* pSvcLocator );
  bool operator()( LHCb::RawBank::View const& rbbank ) const override;

private:
  Gaudi::Property<std::array<unsigned int, 3>> m_r{this, "RequireMask", {~0u, ~0u, ~0u}};
  Gaudi::Property<std::array<unsigned int, 3>> m_v{this, "VetoMask", {0u, 0u, 0u}};
  Gaudi::Property<bool>                        m_passOnError{this, "PassOnError", true};

  mutable Gaudi::Accumulators::Counter<> m_unexpectedRawbanks{this, "#unexpected number of HltRoutingBits rawbanks"};
  mutable Gaudi::Accumulators::Counter<> m_unexpectedRawbanksSize{this, "#unexpected HltRoutingBits rawbank size"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_accept{this, "#accept"};
};

HltRoutingBitsFilter::HltRoutingBitsFilter( const std::string& name, ISvcLocator* pSvcLocator )
    : FilterPredicate( name, pSvcLocator, KeyValue{"RawBanks", "DAQ/RawBanks/HltRoutingBits"} ) {}

bool HltRoutingBitsFilter::operator()( LHCb::RawBank::View const& rbbank ) const {

  if ( rbbank.size() != 1 ) {
    ++m_unexpectedRawbanks;
    return m_passOnError;
  }
  if ( rbbank[0]->size() != 3 * sizeof( unsigned int ) &&
       rbbank[0]->size() != 4 * sizeof( unsigned int ) // FIXME: should we ever write the 4th word into HltRoutingBits?
  ) {
    ++m_unexpectedRawbanksSize;
    return m_passOnError;
  }
  const unsigned int* data = rbbank[0]->data();

  bool veto = false;
  bool req  = false;
  for ( unsigned i = 0; i < 3 && !veto; ++i ) {
    veto = veto || ( data[i] & m_v[i] );
    req  = req || ( data[i] & m_r[i] );
  }
  bool accept = ( req & !veto );
  m_accept += accept;

  return accept;
};

DECLARE_COMPONENT( HltRoutingBitsFilter )
