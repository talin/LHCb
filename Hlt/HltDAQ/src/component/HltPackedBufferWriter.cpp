/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedData.h"
#include "Event/PackedDataBuffer.h"
#include "Event/RawEvent.h"
#include "HltSourceID.h"
#include "LHCbAlgs/MergingTransformer.h"

/** @class ...
 * Put packed data buffers into a single raw event
 *
 *  @author Sevda Esen
 *  @date   2021-07-15
 */

namespace LHCb::Hlt::DAQ {

  using VOC = Gaudi::Functional::vector_of_const_<PackedData::PackedDataOutBuffer*>;

  class HltPackedBufferWriter final
      : public Algorithm::MergingMultiTransformer<std::tuple<RawEvent, RawBank::View>( VOC const& ),
                                                  Algorithm::Traits::writeOnly<RawEvent>> {

  public:
    // Standard constructor
    HltPackedBufferWriter( const std::string& name, ISvcLocator* pSvcLocator )
        : MergingMultiTransformer( name, pSvcLocator, KeyValues{"PackedContainers", {}},
                                   // Outputs
                                   {KeyValue{"OutputRawEvent", "/Event/DAQ/DstDataEvent"},
                                    KeyValue{"OutputView", "/Event/DAQ/DstData/View"}} ) {}

    // Functional operator
    std::tuple<RawEvent, RawBank::View> operator()( const VOC& buffers ) const override {

      RawEvent rawEvent;

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Requested to write " << buffers.size() << " buffers" << endmsg;
        debug() << "to " << outputLocation() << endmsg;
        for ( const auto& [nbuf, buffer] : range::enumerate( buffers ) ) {
          debug() << "input key: " << buffer->key() << "  " << nbuf << "/" << buffers.size() << " "
                  << inputLocation( nbuf ) << endmsg;
        }
      }

      /// Maximum bank payload size = 65535 (max uint16) - 8 (header) - 3 (alignment)
      constexpr size_t                MAX_PAYLOAD_SIZE{65524};
      unsigned int                    nbuf_uncompressed = 0;
      PackedData::PackedDataOutBuffer bigBuffer{0}; // key will come from components
      bigBuffer.reserve( MAX_PAYLOAD_SIZE );

      for ( const auto& [nbuf, buffer] : range::enumerate( buffers ) ) {
        if ( !buffer || buffer->buffer().size() == 0 ) continue;
        bigBuffer.addBuffer( *buffer );
        assert( bigBuffer.key() == buffer->key() );
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << " writing buffer for location " << inputLocation( nbuf ) << " buffer size "
                  << buffer->buffer().size() << " buffer key: " << buffer->key() << endmsg;
        }
      }

      PackedData::ByteBuffer::buffer_type compressedBuffer;
      compressedBuffer.reserve( 0.5 * bigBuffer.buffer().size() );

      auto compressed = ( m_compression != PackedData::Compression::NoCompression ) &&
                        bigBuffer.compress( m_compression, m_compressionLevel, compressedBuffer );

      PackedData::Compression compression =
          ( compressed ? m_compression.value() : PackedData::Compression::NoCompression );

      // must be a better way to do this...
      if ( bigBuffer.key() == 0 && bigBuffer.buffer().size() > 0 ) {
        ++m_zero_key;
        // throw GaudiException{"about to write a zero encoding key", __PRETTY_FUNCTION__, StatusCode::FAILURE};
      }
      auto preamble_data = std::array<uint32_t, 2>{bigBuffer.key(), static_cast<unsigned int>( compression ) & 0x7};
      auto preamble      = as_bytes( LHCb::span{preamble_data} );

      auto output = compressed ? std::move( compressedBuffer ) : std::move( bigBuffer ).buffer();
      output.insert( output.begin(), preamble.begin(), preamble.end() );

      if ( m_sourceID == SourceID::Dummy )
        throw GaudiException( "Invalid SourceID. Please configure SourceID explicitly", __PRETTY_FUNCTION__,
                              StatusCode::FAILURE );
      uint16_t sourceIDCommon = shift<SourceIDMasks::Process>( m_sourceID.value() );

      if ( msgLevel( MSG::DEBUG ) ) {
        this->debug() << " writing source id " << sourceIDCommon << " buffer size " << bigBuffer.buffer().size()
                      << " output size " << output.size() << endmsg;
      }

      auto chunks = range::chunk( output, MAX_PAYLOAD_SIZE );
      if ( chunks.size() > extract<SourceIDMasks::PartID>( ~uint16_t{0} ) ) {
        ++m_too_large;
        // TODO: record a special payload, 2 4-byts words only...
        auto view = rawEvent.banks( RawBank::DstData );
        return {std::move( rawEvent ), std::move( view )};
      }

      for ( auto [ibank, chunk] : range::enumerate( chunks ) ) {
        rawEvent.addBank( sourceIDCommon | shift<SourceIDMasks::PartID>( ibank ), RawBank::DstData, 3, chunk );
      }

      m_serializedDataSize += bigBuffer.buffer().size();
      m_compressedDataSize += output.size();

      if ( !compressed ) nbuf_uncompressed++;

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Compressed " << buffers.size() - nbuf_uncompressed << " buffers" << endmsg;
        debug() << "Uncompressed " << nbuf_uncompressed << " buffers" << endmsg;
      }

      auto view = rawEvent.banks( RawBank::DstData );
      // without std::move here the RawEvent gets copied which would invalidate the view
      // View creation must be after RawEvent is made
      return {std::move( rawEvent ), std::move( view )};
    }

  private:
    /// Property setting the compression algorithm
    Gaudi::Property<PackedData::Compression> m_compression{this, "Compression", PackedData::Compression::ZSTD};

    /// Property setting the compression level
    Gaudi::Property<int> m_compressionLevel{this, "CompressionLevel", 5};

    mutable Gaudi::Accumulators::StatCounter<> m_serializedDataSize{this, "Size of serialized data"};

    mutable Gaudi::Accumulators::StatCounter<> m_compressedDataSize{this, "Size of compressed data"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_too_large{this, "Packed objects too large to save", 50};

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_zero_key{
        this, "zero encoding key -- readback configration provenance will be lost. Proceed at your own risk", 50};

    Gaudi::Property<SourceID> m_sourceID{this, "SourceID", SourceID::Dummy};
  };

  DECLARE_COMPONENT_WITH_ID( HltPackedBufferWriter, "HltPackedBufferWriter" )

} // namespace LHCb::Hlt::DAQ
