/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltLumiSummary.h"
#include "Event/ODIN.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/StatusCode.h"
#include "Kernel/EventContextExt.h"
#include "Kernel/IANNSvc.h"
#include "Kernel/IIndexedLumiSchemaSvc.h"
#include "LHCbAlgs/Consumer.h"
#include <string>

using BXTypes = LHCb::ODIN::BXTypes;

namespace {
  template <typename T, typename K, typename OWNER>
  void map_emplace( T& t, K&& key, OWNER* owner, std::string const& name, std::string const& title,
                    Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis1 ) {
    t.emplace( std::piecewise_construct, std::forward_as_tuple( key ),
               std::forward_as_tuple( owner, name, title, axis1 ) );
  }

  template <typename T>
  std::string to_string( const T& streamable ) {
    std::ostringstream oss;
    oss << streamable;
    return oss.str();
  }

  const auto AxisBCID = Gaudi::Accumulators::Axis<double>( 3565, -0.5, 3564.5 );
  const auto AxisTime = Gaudi::Accumulators::Axis<double>( 3600, 0, 3600 );
} // namespace

class HltLumiSummaryMonitor final
    : public LHCb::Algorithm::Consumer<void( const LHCb::HltLumiSummary&, const LHCb::ODIN& )> {
public:
  HltLumiSummaryMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer{name, pSvcLocator, {KeyValue{"Input", ""}, KeyValue{"ODIN", ""}}} {}
  void operator()( const LHCb::HltLumiSummary&, const LHCb::ODIN& ) const override;

private:
  int threshold( std::string const& counter ) const {
    auto threshold = m_emptyThresholds.find( counter );
    return threshold != m_emptyThresholds.end() ? threshold->second : 0;
  }

  ServiceHandle<IIndexedLumiSchemaSvc> m_annSvc{this, "DecoderMapping", "HltANNSvc"};

  Gaudi::Property<std::map<std::string, int>> m_emptyThresholds{this,
                                                                "EmptyThresholds",
                                                                {{"SciFiClusters", 90},
                                                                 {"SciFiClustersS1M45", 50},
                                                                 {"SciFiClustersS2M123", 60},
                                                                 {"SciFiClustersS2M45", 30},
                                                                 {"SciFiClustersS3M123", 40},
                                                                 {"SciFiClustersS3M45", 30}}};

  mutable std::map<std::pair<BXTypes, std::string>, Gaudi::Accumulators::Histogram<1>>        m_value;
  mutable std::map<std::pair<BXTypes, std::string>, Gaudi::Accumulators::ProfileHistogram<1>> m_time_mean;
  mutable std::map<std::pair<BXTypes, std::string>, Gaudi::Accumulators::ProfileHistogram<1>> m_time_pnz;

  mutable std::mutex m_lock;
};

DECLARE_COMPONENT( HltLumiSummaryMonitor )

void HltLumiSummaryMonitor::operator()( const LHCb::HltLumiSummary& summary, const LHCb::ODIN& odin ) const {
  auto lock = std::scoped_lock{m_lock};

  if ( m_value.empty() ) {
    auto encodingKey = summary.info( "encodingKey", 0 );
    if ( encodingKey == 0 ) return;
    auto schema = m_annSvc->lumiCounters( encodingKey, 0 );

    for ( auto bx : {BXTypes::NoBeam, BXTypes::Beam1, BXTypes::Beam2, BXTypes::BeamCrossing} ) {
      for ( const auto& info : summary.extraInfo() ) {
        const auto& counter    = info.first;
        auto        counterDef = std::find_if( std::begin( schema.counters ), end( schema.counters ),
                                        [&]( auto const& c ) { return c.name == counter; } );
        if ( counterDef == std::end( schema.counters ) ) {
          throw GaudiException{"Lumi schema and HltLumiSummary are inconsistent", __PRETTY_FUNCTION__,
                               StatusCode::FAILURE};
        }
        const unsigned int maxValue         = 1 << counterDef->size;
        const unsigned int nBins            = std::min( maxValue, 2048u );
        const double       minValueScaled   = -counterDef->shift;
        const double       rangeValueScaled = maxValue * counterDef->scale;
        const double       maxValueScaled   = minValueScaled + rangeValueScaled;
        const double       binSize          = rangeValueScaled / nBins;

        map_emplace(
            m_value, std::make_pair( bx, counter ), this, "value_" + to_string( bx ) + "_" + counter,
            "Counter value for " + counter + "/" + to_string( bx ),
            Gaudi::Accumulators::Axis<double>( nBins, minValueScaled - binSize / 2., maxValueScaled - binSize / 2. ) );
        map_emplace( m_time_mean, std::make_pair( bx, counter ), this, "mean_" + to_string( bx ) + "_" + counter,
                     "Mean value per second for " + counter + "/" + to_string( bx ), AxisTime );
        map_emplace( m_time_pnz, std::make_pair( bx, counter ), this, "pnz_" + to_string( bx ) + "_" + counter,
                     "P(X > " + std::to_string( threshold( counter ) ) + ") per second for " + counter + "/" +
                         to_string( bx ),
                     AxisTime );
      }
    }
  }

  const auto time = ( odin.gpsTime() / 1000000 ) % 3600;
  for ( const auto& [counter, value] : summary.extraInfo() ) {
    const auto key = std::make_pair( odin.bunchCrossingType(), counter );
    ++m_value.at( key )[value];
    m_time_mean.at( key )[time] += value;
    m_time_pnz.at( key )[time] += ( value > threshold( counter ) );
  }
  if ( msgLevel( MSG::VERBOSE ) ) {
    verbose() << "Counters: {";
    for ( const auto& [counter, value] : summary.extraInfo() ) { verbose() << counter << ':' << value << ", "; }
    verbose() << '}' << endmsg;
  }
}
