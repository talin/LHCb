/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltLumiSummary.h"
#include "Event/LumiSummaryOffsets_V1.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Gaudi/Accumulators.h"
#include "Kernel/IIndexedLumiSchemaSvc.h"
#include "Kernel/STLExtensions.h"
#include "LHCbAlgs/Transformer.h"
#include <algorithm>
#include <string>

namespace {
  using lumi_schema_t      = IIndexedLumiSchemaSvc::lumi_schema_t;
  const auto schema_for_v1 = lumi_schema_t{
      LHCb::LumiSummaryOffsets::V1::TotalSize / 8,
      {{"T0Low", LHCb::LumiSummaryOffsets::V1::t0LowOffset, LHCb::LumiSummaryOffsets::V1::t0LowSize},
       {"T0High", LHCb::LumiSummaryOffsets::V1::t0HighOffset, LHCb::LumiSummaryOffsets::V1::t0HighSize},
       {"BCIDLow", LHCb::LumiSummaryOffsets::V1::bcidLowOffset, LHCb::LumiSummaryOffsets::V1::bcidLowSize},
       {"BCIDHigh", LHCb::LumiSummaryOffsets::V1::bcidHighOffset, LHCb::LumiSummaryOffsets::V1::bcidHighSize},
       {"BXType", LHCb::LumiSummaryOffsets::V1::bxTypeOffset, LHCb::LumiSummaryOffsets::V1::bxTypeSize},
       {"GEC", LHCb::LumiSummaryOffsets::V1::GecOffset, LHCb::LumiSummaryOffsets::V1::GecSize},
       {"VeloTracks", LHCb::LumiSummaryOffsets::V1::VeloTracksOffset, LHCb::LumiSummaryOffsets::V1::VeloTracksSize},
       {"VeloVertices", LHCb::LumiSummaryOffsets::V1::VeloVerticesOffset,
        LHCb::LumiSummaryOffsets::V1::VeloVerticesSize},
       {"SciFiClusters", LHCb::LumiSummaryOffsets::V1::SciFiClustersOffset,
        LHCb::LumiSummaryOffsets::V1::SciFiClustersSize},
       {"MuonHitsM2R2", LHCb::LumiSummaryOffsets::V1::M2R2Offset, LHCb::LumiSummaryOffsets::V1::M2R2Size},
       {"MuonHitsM2R3", LHCb::LumiSummaryOffsets::V1::M2R3Offset, LHCb::LumiSummaryOffsets::V1::M2R3Size},
       {"MuonHitsM3R2", LHCb::LumiSummaryOffsets::V1::M3R2Offset, LHCb::LumiSummaryOffsets::V1::M3R2Size},
       {"MuonHitsM3R3", LHCb::LumiSummaryOffsets::V1::M3R3Offset, LHCb::LumiSummaryOffsets::V1::M3R3Size}}};

  unsigned getField( unsigned offset, unsigned size, LHCb::span<const unsigned> target ) {
    // Separate offset into a word part and bit part
    unsigned word      = offset / ( 8 * sizeof( unsigned ) );
    unsigned bitoffset = offset % ( 8 * sizeof( unsigned ) );

    // Check size and offset line up with word boundaries
    if ( bitoffset + size > ( 8 * sizeof( unsigned ) ) ) { return 0; }

    unsigned mask = ( ( 1ul << size ) - 1 );
    return ( ( target[word] >> bitoffset ) & mask );
  }

} // namespace

namespace LHCb {

  /**
   *  Decodes the LumiSummary.
   *
   *  @author Jaap Panman
   *  HenryIII Changed to use Transform Algorithm
   *
   *  @date   2008-08-01
   */

  class HltLumiSummaryDecoder : public Algorithm::Transformer<HltLumiSummary( const RawBank::View& )> {
  public:
    /// Standard constructor
    HltLumiSummaryDecoder( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, KeyValue{"RawBanks", "DAQ/RawBanks/HltLumiSummary"},
                       KeyValue{"OutputContainerName", HltLumiSummaryLocation::Default} ) {}

    HltLumiSummary operator()( const RawBank::View& lumiSummaryBanks ) const override {

      HltLumiSummary hltLumiSummary;

      // Get the buffers associated with the HltLumiSummary
      // Now copy the information from all banks (normally there should only be one)
      if ( lumiSummaryBanks.empty() ) {
        throw GaudiException( "No LumiSummary RawBank in RawEvent. Did you filter on decision or routing bit?", name(),
                              StatusCode::FAILURE );
      }

      auto lumisummaryRawBank0 = lumiSummaryBanks.front();

      if ( lumisummaryRawBank0->type() != RawBank::HltLumiSummary ) {
        throw GaudiException( " Wrong bank type for HltLumiSummaryDecoder", name(), StatusCode::FAILURE );
      }

      for ( const auto& ibank : lumiSummaryBanks ) {
        switch ( ibank->version() ) {
        case 1:
          // initial Run 3 version
          add_to_summary( hltLumiSummary, schema_for_v1, *ibank );
          break;
        case 2: {
          auto encodingKey =
              getField( 0u, 32u, ibank->range<unsigned>() ); // Encoding key is stored in the first 32 bits
          if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "encodingKey :" << encodingKey << endmsg; }
          auto const& schema = m_svc->lumiCounters( encodingKey, 0 );
          add_to_summary( hltLumiSummary, schema, *ibank, encodingKey );
          break;
        }
        default:
          // legacy Run 1+2 LumiSummary structure, or larger than known...
          warning() << "Unsupported HltLumiSummary version: " << format( "%4d", ibank->version() ) << endmsg;
          break;
        }
      }
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << hltLumiSummary << endmsg;

      return hltLumiSummary;
    }

  private:
    void add_to_summary( LHCb::HltLumiSummary& summary, lumi_schema_t const& schema, LHCb::RawBank const& bank,
                         unsigned encodingKey = 0 ) const {
      if ( static_cast<unsigned>( bank.size() ) != schema.size ) {
        warning() << "Bank size incorrect : expected " << format( "%4d", schema.size ) << " for encoding key "
                  << format( "%8x", encodingKey ) << ", found " << format( "%4d", bank.size() ) << endmsg;
        return;
      }
      for ( const auto& [name, size, offset, shift, scale] : schema.counters ) {
        auto value = ( static_cast<double>( getField( offset, size, bank.range<unsigned>() ) ) - shift ) / scale;
        summary.addInfo( name, value );
      }
    };

    ServiceHandle<IIndexedLumiSchemaSvc> m_svc{this, "DecoderMapping", "HltANNSvc"};

    // Statistics, mutable to allow statistics to be kept
    mutable Gaudi::Accumulators::AveragingCounter<> m_totDataSize{this, "Average event size / 32-bit words"};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::HltLumiSummaryDecoder, "HltLumiSummaryDecoder" )
