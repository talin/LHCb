/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/CaloCluster.h"
#include "Event/HltDecReports.h"
#include "Event/HltObjectSummary.h"
#include "Event/HltSelReports.h"
#include "Event/Particle.h"
#include "Event/RawEvent.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "HltDecoderUtils.h"
#include "HltSelRepRBExtraInfo.h"
#include "HltSelRepRBHits.h"
#include "HltSelRepRBObjTyp.h"
#include "HltSelRepRBStdInfo.h"
#include "HltSelRepRBSubstr.h"
#include "HltSelRepRawBank.h"
#include "IReportConvert.h"
#include "Kernel/IIndexedANNSvc.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/bit_cast.h"
#include "boost/format.hpp"
#include <numeric>

namespace LHCb::Hlt::DAQ {
  //-----------------------------------------------------------------------------
  // Implementation file for class : HltSelReportsDecoder
  //
  // 2008-08-01 : Tomasz Skwarnicki
  //-----------------------------------------------------------------------------

  /** @class HltSelReportsDecoder HltSelReportsDecoder.h
   *
   *
   *  @author Tomasz Skwarnicki
   *  @date   2008-08-02
   *
   *  Algorithm to read HltSelReports from Raw Data and create containers on TES
   *
   */

  class HltSelReportsDecoder
      : public LHCb::Algorithm::MultiTransformer<std::tuple<HltSelReports, HltObjectSummary::Container>(
            const RawBank::View& )> {
  public:
    /// Standard constructor
    HltSelReportsDecoder( const std::string& name, ISvcLocator* pSvcLocator );

    ///< Algorithm initialization
    StatusCode initialize() override;

    ///< Algorithm execution
    std::tuple<HltSelReports, HltObjectSummary::Container> operator()( const RawBank::View& ) const override;

  private:
    enum HeaderIDs { kVersionNumber = 12 };
    /// for converting objects in to summaries
    ToolHandle<IReportConvert>          m_conv{this, "ReportConvertTool", "ReportConvertTool"};
    ServiceHandle<IIndexedANNSvc>       m_svc{this, "DecoderMapping", "HltANNSvc"};
    Gaudi::Property<SourceID>           m_sourceID{this, "SourceID", SourceID::Dummy};
    DataObjectReadHandle<RawBank::View> m_decreports{this, "DecReports", "DAQ/RawBanks/HltDecReports"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_inconsistent_size{
        this, "HltSelReportsRawBank internally reported size less than bank size delivered by RawEvent"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_subs_out_of_range{this,
                                                                            "Substructure object index out of range "};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_failed_to_add_name{
        this, "Failed to add Hlt selection to its container"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_duplicate_entry{
        this, "duplicate decoded entry - leaving initial one"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_key_not_found{
        this, "Did not find string key for trigger selection in storage"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_duplicate_sourceID{
        this, "Duplicate sequential Source ID HltSelReports. Aborting"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_unexpected_number_of_banks{
        this, "Did not find the expected number of HltSelReports raw banks. Aborting"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_rb_fails_integrity{
        this, "HltSelReportsRawBank fails integrity check"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_hits_fails_integrity{this,
                                                                               "HltSelRepRBHits fails integrity check"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_obj_fails_integrity{
        this, "HltSelRepRBObjTyp fails integrity check"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_sub_fails_integrity{
        this, "HltSelRepRBSubstr fails integrity check"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_info_fails_integrity{
        this, "HltSelRepRBStdInfo fails integrity check"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_einfo_fails_integrity{
        this, "HltSelRepRBExtraInfo fails integrity check"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_einfo_inconsistent_number{
        this, "HltSelRepRBExtraInfo has number of objects different from HltSelRepRBObjTyp"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_info_inconsistent_number{
        this, "HltSelRepRBStdInfo has number of objects different from HltSelRepRBObjTyp"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_substr_inconsistent_number{
        this, "HltSelRepRBSubstr has number of objects different from HltSelRepRBObjTyp"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_id_not_found{
        this, "Did not find string key for PV-selection-ID in trigger selection in storage"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_index_out_of_range{this, "Hit sequence index out of range"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_too_many_objects{
        this, "Version (99) indicates too many objects were requested to be saved. Returning debugging reports"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_version_unk{
        this, "HltSelReports RawBank version is higher than expected. Will try to decode it anyway"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_rawbank{
        this, "No HltSelReports RawBank for requested SourceID in RawEvent"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_unsupported_clid{this, "StdInfo on unsupported class type"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_einfo_key_not_found{
        this, "String key for Extra Info item in storage not found"};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( HltSelReportsDecoder, "HltSelReportsDecoder" )

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  HltSelReportsDecoder::HltSelReportsDecoder( const std::string& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::MultiTransformer<std::tuple<HltSelReports, HltObjectSummary::Container>(
            const RawBank::View& )>{
            name,
            pSvcLocator,
            KeyValue{"RawBanks", "DAQ/RawBanks/HltSelReports"},
            {KeyValue{"OutputHltSelReportsLocation", LHCb::HltSelReportsLocation::Default},
             KeyValue{"OutputHltObjectSummariesLocation", LHCb::HltSelReportsLocation::Default + "/Candidates"}}} {}

  //=============================================================================
  // Initialize
  //=============================================================================
  StatusCode HltSelReportsDecoder::initialize() {
    auto sc = LHCb::Algorithm::MultiTransformer<std::tuple<HltSelReports, HltObjectSummary::Container>(
        const RawBank::View& )>::initialize();
    if ( !sc ) return sc;

    // check that the 2nd handle has a key which is 1st handle key + "/Candidates"!!!
    //
    // TODO/FIXME: parse the property (yuk. It should have keys and values) , and
    //             get the key... then compare the keys.
    //
    // const auto& summaryLoc = getProperty("OutputHltObjectSummariesLocation");
    // const auto& selrepLoc = getProperty("OutputHltSelReportsLocation");
    // const auto& expected = selrepLoc.toString()+"/Candidates";
    // if ( symmaryLoc.toString() != expected ) {
    //  return Error("value of OutputHltObjectSummariesLocation not consistent", StatusCode::FAILURE);
    //}
    return sc;
  }
  //=============================================================================
  // Main execution
  //=============================================================================
  std::tuple<LHCb::HltSelReports, LHCb::HltObjectSummary::Container> HltSelReportsDecoder::
                                                                     operator()( const LHCb::RawBank::View& rawBanks ) const {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

    // ----------------------------------------------------------
    // get the bank(s) from RawEvent
    // ----------------------------------------------------------
    auto hltselreportsRawBanks = selectRawBanks( m_sourceID, rawBanks );
    if ( hltselreportsRawBanks.empty() ) {
      throw GaudiException( " No HltSelReports RawBank in RawEvent. Not producing any HltSelReports. ", name(),
                            StatusCode::SUCCESS );
    }
    std::tuple<HltSelReports, HltObjectSummary::Container> outputs;
    // output container for Object Summaries
    auto& [output, objectSummaries] = outputs;

    const RawBank* hltselreportsRawBank0 = hltselreportsRawBanks.front();
    if ( hltselreportsRawBank0->type() != RawBank::HltSelReports ) {
      throw GaudiException( " Wrong bank type for HltSelReportsDecoder", name(), StatusCode::FAILURE );
    }

    // Check we know how to decode this version
    // If version is 99, this is the special case of the empty dummy bank
    if ( hltselreportsRawBank0->version() == 99 ) {

      // Get the list of ids and associated candidates
      auto             pBank99 = new unsigned int[hltselreportsRawBank0->size()];
      HltSelRepRawBank hltSelReportsBank99( pBank99 ); // bank assumes ownership!
      std::copy( hltselreportsRawBank0->begin<unsigned int>(), hltselreportsRawBank0->end<unsigned int>(), pBank99 );
      HltSelRepRBHits hitsSubBank99( hltSelReportsBank99.subBankFromID( HltSelRepRBEnums::SubBankIDs::kHitsID ) );
      // Populate map with line name and number of candidates
      LHCb::HltObjectSummary summary;
      auto                   tck_dummy = tck( m_sourceID, *m_decreports.get() );

      bool                                                    settings = ( tck_dummy == 0 );
      GaudiUtils::VectorMap<unsigned int, std::string> const* idmap_dummy = nullptr;
      if ( !settings ) idmap_dummy = &m_svc->i2s( tck_dummy, selectionID_for( m_sourceID.value() ) );

      unsigned int i = hitsSubBank99.seqBegin( 0 );
      while ( i < hitsSubBank99.seqEnd( 0 ) ) {
        int temp1 = hitsSubBank99.location()[i++];
        int temp2 = hitsSubBank99.location()[i++];
        if ( !idmap_dummy )
          summary.addToInfo( idmap_dummy->find( temp1 )->second, temp2 );
        else
          summary.addToInfo( std::to_string( temp1 ), temp2 );
      }

      output.insert( "0#Candidates", std::move( summary ) ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ++m_too_many_objects;
      return outputs;
    }

    if ( hltselreportsRawBank0->version() > kVersionNumber ) ++m_version_unk;
    // put the banks into the right order (in case the data was split across multiple banks...
    std::sort( begin( hltselreportsRawBanks ), end( hltselreportsRawBanks ),
               []( const LHCb::RawBank* lhs, const LHCb::RawBank* rhs ) { return lhs->sourceID() < rhs->sourceID(); } );
    // verify no duplicates...
    auto adj = std::adjacent_find(
        begin( hltselreportsRawBanks ), end( hltselreportsRawBanks ),
        []( const LHCb::RawBank* lhs, const LHCb::RawBank* rhs ) { return lhs->sourceID() == rhs->sourceID(); } );
    if ( adj != end( hltselreportsRawBanks ) ) {
      ++m_duplicate_sourceID;
      return outputs; // TODO: review whether to throw an exception instead
    }

    unsigned int nLastOne = getSequenceNumber<RawBank::BankType::HltSelReports>( *hltselreportsRawBanks.back() );
    if ( nLastOne + 1 != hltselreportsRawBanks.size() ) {
      ++m_unexpected_number_of_banks;
      return outputs; // TODO: review whether to throw an exception instead
    }

    unsigned int bankSize =
        std::accumulate( begin( hltselreportsRawBanks ), end( hltselreportsRawBanks ), 0u,
                         []( unsigned int s, const RawBank* bank ) { return s + bank->range<unsigned int>().size(); } );

    if ( !bankSize ) {
      ++m_no_rawbank;
      return outputs; // TODO: review whether to throw an exception instead
    }

    // need to copy it to local array to concatenate  --- TODO: we could run a decompression such as LZMA at this point
    // as well...
    auto             pBank = new unsigned int[bankSize];
    HltSelRepRawBank hltSelReportsBank( pBank ); // bank assumes ownership
    std::accumulate( begin( hltselreportsRawBanks ), end( hltselreportsRawBanks ), pBank,
                     []( unsigned int* p, const LHCb::RawBank* bank ) {
                       return std::copy( bank->begin<unsigned int>(), bank->end<unsigned int>(), p );
                     } );

    HltSelRepRBHits      hitsSubBank( hltSelReportsBank.subBankFromID( HltSelRepRBEnums::SubBankIDs::kHitsID ) );
    HltSelRepRBObjTyp    objTypSubBank( hltSelReportsBank.subBankFromID( HltSelRepRBEnums::SubBankIDs::kObjTypID ) );
    HltSelRepRBSubstr    substrSubBank( hltSelReportsBank.subBankFromID( HltSelRepRBEnums::SubBankIDs::kSubstrID ) );
    HltSelRepRBStdInfo   stdInfoSubBank( hltSelReportsBank.subBankFromID( HltSelRepRBEnums::SubBankIDs::kStdInfoID ) );
    HltSelRepRBExtraInfo extraInfoSubBank(
        hltSelReportsBank.subBankFromID( HltSelRepRBEnums::SubBankIDs::kExtraInfoID ) );

    // ----------------------------------------- integrity checks -------------------------
    bool         errors  = false;
    bool         exInfOn = true;
    unsigned int nObj = objTypSubBank.numberOfObj();

    if ( bankSize < hltSelReportsBank.size() ) {
      ++m_inconsistent_size;
      errors = true;

    } else {
      if ( auto ic = hltSelReportsBank.integrityCode(); ic ) {
        ++m_rb_fails_integrity;
        errors = true;
      }

      if ( auto ic = hitsSubBank.integrityCode(); ic ) {
        ++m_hits_fails_integrity;
        errors = true;
      }

      if ( auto ic = objTypSubBank.integrityCode(); ic ) {
        ++m_obj_fails_integrity;
        errors = true;
      }

      if ( auto ic = substrSubBank.integrityCode(); ic ) {
        ++m_sub_fails_integrity;
        errors = true;
      }
      if ( nObj != substrSubBank.numberOfObj() ) {
        ++m_substr_inconsistent_number;
        errors = true;
      }

      if ( auto ic = stdInfoSubBank.integrityCode(); ic ) {
        ++m_info_fails_integrity;
        errors = true;
      }
      if ( nObj != stdInfoSubBank.numberOfObj() ) {
        ++m_info_inconsistent_number;
        errors = true;
      }

      if ( auto ic = extraInfoSubBank.integrityCode(); ic ) {
        ++m_einfo_fails_integrity;
        exInfOn = false; // the only non-fatal info corruption
      }
      if ( nObj != extraInfoSubBank.numberOfObj() ) {
        ++m_einfo_inconsistent_number;
        exInfOn = false;
      }
    }

    if ( msgLevel( MSG::VERBOSE ) ) {
      // print created bank and subbanks inside
      verbose() << hltSelReportsBank << endmsg;
      verbose() << HltSelRepRBHits( hltSelReportsBank.subBankFromID( HltSelRepRBEnums::SubBankIDs::kHitsID ) )
                << endmsg;
      verbose() << HltSelRepRBObjTyp( hltSelReportsBank.subBankFromID( HltSelRepRBEnums::SubBankIDs::kObjTypID ) )
                << endmsg;
      verbose() << HltSelRepRBSubstr( hltSelReportsBank.subBankFromID( HltSelRepRBEnums::SubBankIDs::kSubstrID ) )
                << endmsg;
      verbose() << HltSelRepRBStdInfo( hltSelReportsBank.subBankFromID( HltSelRepRBEnums::SubBankIDs::kStdInfoID ) )
                << endmsg;
      verbose() << HltSelRepRBExtraInfo( hltSelReportsBank.subBankFromID( HltSelRepRBEnums::SubBankIDs::kExtraInfoID ) )
                << endmsg;
    }

    if ( errors ) {
      hltSelReportsBank.deleteBank();
      throw GaudiException( "possible data corruption -- not producing any HltSelReports", name(),
                            StatusCode::SUCCESS );
    }

    // -----------------------------------------------------------------
    // create object summaries
    // -----------------------------------------------------------------
    // TODO: check consistency of output location and source ID!!!!
    auto        mykey = key( m_sourceID, *m_decreports.get() );
    const auto& idmap = m_svc->i2s( mykey, selectionID_for( m_sourceID.value() ) );
    const auto& infomap = m_svc->i2s( mykey, Gaudi::StringKey{"InfoID"} );

    // put them in local vector until they are finished
    std::vector<HltObjectSummary*> objects;

    for ( unsigned int iObj = 0; iObj != nObj; ++iObj ) {

      auto hos = std::make_unique<HltObjectSummary>();

      // =========== class ID
      hos->setSummarizedObjectCLID( objTypSubBank.next() );

      // =========== numerical info
      HltObjectSummary::Info infoPersistent;

      //           ============== standard
      HltSelRepRBStdInfo::StdInfo stdInfo = stdInfoSubBank.next();
      if ( stdInfo.size() ) {
        auto clid = hos->summarizedObjectCLID();
        switch ( clid ) {
        case LHCb::CLID_Track:
        case LHCb::CLID_RecVertex:
        case LHCb::CLID_Vertex:
        case LHCb::CLID_RichPID:
        case LHCb::CLID_MuonPID:
        case LHCb::CLID_ProtoParticle:
        case LHCb::CLID_Particle:
        case LHCb::CLID_RecSummary:
        case LHCb::CLID_CaloCluster:
        case LHCb::CLID_CaloHypo:
        case 40:
        case 41:
          m_conv->SummaryFromRaw( &infoPersistent, &stdInfo, clid, hltselreportsRawBank0->version() );
          break;
        case 1: {
          infoPersistent.insert( "0#SelectionID", bit_cast<float>( stdInfo[0] ) );
          if ( stdInfo.size() > 1 ) {
            int  id = (int)( bit_cast<float>( stdInfo[1] ) + 0.1 );
            auto iselName = idmap.find( id );
            if ( iselName == std::end( idmap ) ) {
              ++m_id_not_found;
              infoPersistent.insert( "10#Unknown", bit_cast<float>( stdInfo[1] ) );
            } else
              infoPersistent.insert( "10#" + iselName->second, bit_cast<float>( stdInfo[1] ) );
          }
          for ( unsigned int ipvkeys = 2; ipvkeys < stdInfo.size(); ++ipvkeys ) {
            infoPersistent.insert( "11#" + boost::str( boost::format( "%1$=08X" ) % ( ipvkeys - 2 ) ),
                                   bit_cast<float>( stdInfo[ipvkeys] ) );
          }

        } break;
        default: {

          ++m_unsupported_clid;
          int e = 0;
          for ( const auto& i : stdInfo ) {
            infoPersistent.insert( "z#Unknown.unknown" + std::to_string( e++ ), bit_cast<float>( i ) );
          }
        }
        }
      }

      //           ============== extra

      if ( exInfOn ) {
        for ( const auto& i : extraInfoSubBank.next() ) {
          auto infos = infomap.find( i.first );
          if ( infos != end( infomap ) ) {
            infoPersistent.insert( infos->second, i.second );
          } else {
            ++m_einfo_key_not_found;
          }
        }
      }
      hos->setNumericalInfo( infoPersistent );
      objects.push_back( hos.release() );
    }

    // -----------------------------------------------------------------
    // reloop to add substructure or hits
    // -----------------------------------------------------------------
    for ( unsigned int iObj = 0; iObj != nObj; ++iObj ) {

      HltObjectSummary*&        hos = objects[iObj];
      HltSelRepRBSubstr::Substr sub = substrSubBank.next();

      if ( sub.first ) {
        // hits
        unsigned int              nSeq = hitsSubBank.numberOfSeq();
        std::vector<LHCb::LHCbID> hits;
        for ( const auto& iSeq : sub.second ) {
          if ( iSeq < nSeq ) {
            std::vector<LHCb::LHCbID> hitseq = hitsSubBank.sequence( iSeq );
            //   for bank version zero, first hit in the first sequence was corrupted ------
            //                   for odd number of sequences saved - omit this hit
            if ( iSeq == 0 && hltselreportsRawBank0->version() == 0 && nSeq % 2 == 1 ) {
              hitseq.erase( hitseq.begin() );
            }
            // ------------------------- end fix --------------------------------------------
            if ( hitseq.size() ) { hits.insert( end( hits ), begin( hitseq ), end( hitseq ) ); }
          } else {
            ++m_index_out_of_range;
          }
        }
        // Sort hits to make sure decode(write(X)) keeps the IDs ordered
        // (ordering is relied upon and enforced in HltSelReportsWriter)
        std::sort( begin( hits ), end( hits ) );
        hos->setLhcbIDs( hits );

      } else {
        // pointers
        SmartRefVector<LHCb::HltObjectSummary> thisSubstructure;
        for ( const auto& jObj : sub.second ) {
          if ( jObj < nObj ) {
            thisSubstructure.push_back( &( *( objects[jObj] ) ) );
          } else {
            ++m_subs_out_of_range;
          }
        }
        hos->setSubstructureExtended( thisSubstructure );
        if ( hltselreportsRawBank0->version() < 3 ) { hos->setSubstructure( thisSubstructure ); }
      }

      // give ownership to output
      objectSummaries.push_back( hos );
    }

    // clean-up
    hltSelReportsBank.deleteBank();

    // fix intermix of substructure needed by TisTos tool and by Turbo stream
    // substructure() returns only things needed by TisTos, substructureExetended() all things needed by Turbo
    // logic below is somewhat messy and depends on what was done in HltSelReportsMaker; this is the best we
    //      can do without restructuring rawbanks that don't distinguish between the two types

    //     don't waste time on older version banks which did not have extended info
    if ( hltselreportsRawBank0->version() > 2 ) {
      for ( unsigned int iObj = 0; iObj != nObj; ++iObj ) {

        HltObjectSummary*& hos = objects[iObj];
        if ( hos->summarizedObjectCLID() != LHCb::CLID_Particle ) {
          hos->setSubstructure( hos->substructureExtended() );
        } else {
          // for TisTos need to delete calo clusters from a particle that has a track in substructure

          const auto& sub = hos->substructureExtended();
          // look for a track among substracture
          auto e = std::find_if( sub.begin(), sub.end(), [&]( const LHCb::HltObjectSummary* obj ) {
            return obj && obj->summarizedObjectCLID() == LHCb::CLID_Track;
          } );
          if ( e != sub.end() ) // trackFound
          {
            for ( const auto& elem : sub ) {
              if ( !( elem.target() ) ) continue;
              auto id = elem.target()->summarizedObjectCLID();
              // add only if not calo cluster
              if ( id == LHCb::CLID_CaloCluster || id == LHCb::CLID_CaloHypo ) continue;
              hos->addToSubstructure( elem.target() );
            }
          } else {
            // no track, no worry
            hos->setSubstructure( hos->substructureExtended() );
          }
        }
      }
    }

    // ---------------------------------------------------------
    // ------- special container for selections ----------------
    // ---------------------------------------------------------

    for ( unsigned int iObj = 0; iObj != nObj; ++iObj ) {
      HltObjectSummary*& hos = objects[iObj];
      if ( hos->summarizedObjectCLID() != 1 ) continue;
      auto i = std::find_if( begin( hos->numericalInfo() ), end( hos->numericalInfo() ),
                             []( const auto& info ) { return info.first == "0#SelectionID"; } );
      auto selName = ( i != end( hos->numericalInfo() ) ? idmap.find( (int)( i->second + 0.1 ) ) : end( idmap ) );
      if ( selName != end( idmap ) ) {
        // skip reports of the wrong type
        if ( selName->second.empty() ) continue;

        // clone hos
        HltObjectSummary selSumOut;
        selSumOut.setSummarizedObjectCLID( hos->summarizedObjectCLID() );
        selSumOut.setNumericalInfo( hos->numericalInfo() );
        selSumOut.setSubstructure( hos->substructure() );

        // insert selection into the container
        if ( output.insert( selName->second, selSumOut ) == StatusCode::FAILURE ) {
          // failed because already there -- check if entries are the samee...
          auto prev = output.find( selName->second );
          if ( prev != output.end() && prev->second == selSumOut ) {
            ++m_duplicate_entry;
          } else {
            ++m_failed_to_add_name;
          }
        }
      } else {
        ++m_key_not_found;
      }
    }

    if ( msgLevel( MSG::VERBOSE ) ) {

      verbose() << " ======= HltSelReports size= " << output.size() << endmsg;
      verbose() << output << endmsg;

      verbose() << " ======= HltObjectSummary container size= " << objectSummaries.size() << endmsg;
      for ( const auto& pHos : objectSummaries ) {
        verbose() << " key " << pHos->index();
        auto selby = output.selectedAsCandidateBy( pHos );
        if ( !selby.empty() ) {
          verbose() << " selectedAsCandidateBy= ";
          for ( const auto& i : selby ) verbose() << i << " ";
          auto pvInfo = output.pvSelectionNameAndKey( pHos );
          if ( pvInfo.second > -1 ) {
            verbose() << " pvSelectionName= " << pvInfo.first << " pvKey= " << pvInfo.second << " ";
          }
        }
        verbose() << *pHos << endmsg;
      }
    }
    return outputs;
  }
} // namespace LHCb::Hlt::DAQ

//=============================================================================
