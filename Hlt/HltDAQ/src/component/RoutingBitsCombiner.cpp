/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// from Gaudi
#include "Event/RawEvent.h"
#include "Gaudi/Accumulators.h"
#include "LHCbAlgs/Transformer.h"
#include <bitset>

/** @class RoutingBitsCombiner
 *  @brief Combine Routing bits from Hlt1 and Hlt2
 *
 */

namespace LHCb::Hlt::DAQ {
  class RoutingBitsCombiner
      : public Algorithm::MultiTransformer<std::tuple<LHCb::RawEvent, LHCb::RawBank::View>(
                                               LHCb::RawBank::View const&, LHCb::RawBank::View const& ),
                                           Algorithm::Traits::writeOnly<RawEvent>> {

  public:
    /// Standard constructor
    RoutingBitsCombiner( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer{name,
                           pSvcLocator,
                           {KeyValue{"Hlt1Bits", ""}, KeyValue{"Hlt2Bits", ""}},
                           {KeyValue{"RawEventLocation", ""}, KeyValue{"OutputView", ""}}} {}

    std::tuple<LHCb::RawEvent, LHCb::RawBank::View> operator()( LHCb::RawBank::View const& bits_hlt1,
                                                                LHCb::RawBank::View const& bits_hlt2 ) const override {
      if ( !bits_hlt1.empty() ) {
        if ( bits_hlt1[0]->data()[2] > 0 ) { ++m_hlt2_set_in_hlt1; }
        if ( bits_hlt1[0]->data()[1] > 0 ) { ++m_not_default_hlt1; }
      } else {
        ++m_missing_hlt1;
      }

      if ( !bits_hlt2.empty() ) {
        if ( bits_hlt2[0]->data()[0] > 0 ) {
          throw GaudiException( "This is not an HLT2 Routing Bits bank. Please check your configuration or the data!",
                                "Hlt::RoutingBitsCombiner", StatusCode::FAILURE );
        }
        if ( bits_hlt2[0]->data()[1] > 0 ) { ++m_not_default_hlt2; }
      } else {
        ++m_missing_hlt2;
      }

      // Set the new routing bits.
      unsigned int const                copied_hlt1_bits = !bits_hlt1.empty() ? bits_hlt1[0]->data()[0] : 0;
      unsigned int const                copied_hlt2_bits = !bits_hlt2.empty() ? bits_hlt2[0]->data()[2] : 0;
      const std::array<unsigned int, 3> bits_merged{copied_hlt1_bits, 0, copied_hlt2_bits};

      // Get the raw event, add the routing bits bank.
      LHCb::RawEvent outputRawEvent{};
      outputRawEvent.addBank( 0, RawBank::HltRoutingBits, 0, bits_merged );

      return std::tuple{std::move( outputRawEvent ), outputRawEvent.banks( RawBank::HltRoutingBits )};
    }

  private:
    mutable Gaudi::Accumulators::Counter<> m_missing_hlt1{this, "# missing hlt1 bank"};
    mutable Gaudi::Accumulators::Counter<> m_missing_hlt2{this, "# missing hlt2 bank"};

    // Having HLT2 routing bits set in the bank used to set the HLT1 bits can happen when processing HLT2 data with
    // HLT2.
    mutable Gaudi::Accumulators::Counter<> m_hlt2_set_in_hlt1{this, "hlt2 bits set in hlt1 bank"};

    // Normally the second word is unused. Some MC data unfortunately use them. So just count here.
    mutable Gaudi::Accumulators::Counter<> m_not_default_hlt1{this, "first bits, second word used"};
    mutable Gaudi::Accumulators::Counter<> m_not_default_hlt2{this, "second bits, second word used"};
  };

} // namespace LHCb::Hlt::DAQ

namespace Hlt {
  // keep backwards compatible for now
  struct RoutingBitsCombiner : LHCb::Hlt::DAQ::RoutingBitsCombiner {
    using LHCb::Hlt::DAQ::RoutingBitsCombiner::RoutingBitsCombiner;
  };
  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( Hlt::RoutingBitsCombiner )
} // namespace Hlt
