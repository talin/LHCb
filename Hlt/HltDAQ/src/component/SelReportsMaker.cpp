/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include <memory>
#include <numeric>
#include <string>
#include <vector>

// from Gaudi
#include "Event/HltObjectSummary.h"
#include "Event/RelatedInfoMap.h"
#include "Event/Track.h"
#include "GaudiKernel/VectorMap.h"
#include "LHCbAlgs/MergingTransformer.h"

#include "IReportConvert.h"
#include "Kernel/IIndexedANNSvc.h"
#include "ReportConvertTool.h"

#include "boost/algorithm/string/predicate.hpp"
#include "boost/algorithm/string/replace.hpp"
#include "boost/format.hpp"

#include "GaudiAlg/FunctionalDetails.h"

#include "Event/HltDecReports.h"
#include "Event/HltObjectSummary.h"
#include "Event/HltSelReports.h"
#include "Event/RecSummary.h"

#include "Event/RecVertex.h"

namespace {
  static const Gaudi::StringKey InfoID{"InfoID"};
  static const Gaudi::StringKey RelInfoLocations{"RelInfoLocations"};
  static const Gaudi::StringKey Hlt1ID{"Hlt1SelectionID"};
  static const Gaudi::StringKey Hlt2ID{"Hlt2SelectionID"};
  static const Gaudi::StringKey VectorOfTrack{System::typeinfoName( typeid( std::vector<LHCb::Track> ) )};
  static const Gaudi::StringKey VectorOfRecVertex{System::typeinfoName( typeid( std::vector<LHCb::RecVertex> ) )};
} // namespace

namespace {
  template <typename E>
  const ContainedObject* getFirst( const std::vector<ContainedObject*> sel ) {
    return ( !sel.empty() ) ? dynamic_cast<E>( sel.front() ) : nullptr;
  }
} // namespace

namespace LHCb {
  class CaloCluster;
  class Particle;
  class RecVertex;
  class HltSelReports;
  class HltDecReports;
} // namespace LHCb

/** @class TrackSelReportsMaker TrackSelReportsMaker.h
 *  Algorithm to translate HltSummary  into HltSelResults and associated HltObjectSummaries
 */

namespace SelReports::types {
  using Track_t                   = LHCb::Track;
  using Vertex_t                  = LHCb::RecVertex;
  using DecReports_t              = LHCb::HltDecReports;
  using Output_t                  = std::tuple<LHCb::HltSelReports, LHCb::HltObjectSummary::Container>;
  using In_t                      = Gaudi::Functional::details::vector_of_const_<DataObject*>;
  using MergingMultiTransformer_t = LHCb::Algorithm::MergingMultiTransformer<Output_t( In_t const& )>;
} // namespace SelReports::types

class SelReportsMaker : public SelReports::types::MergingMultiTransformer_t {
public:
  using base = SelReports::types::MergingMultiTransformer_t;

  enum OutputInfoLevel {
    kMinInfoLevel      = 0, ///< lhcbIDs only
    kStandardInfoLevel = 1, ///< lhcbIDs + standard numerical info(object specific)
    kExtraInfoLevel    = 2, ///< lhcbIDs + extra numerical info (no standard info!)
    kMaxInfoLevel      = 3  ///< lhcbIDs + standard + extra numerical info
  };

  enum GlobalSelectionIDs { kHlt1GlobalID = 1, kHlt2GlobalID = 2 };

  /// Standard constructor
  SelReportsMaker( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode                  initialize() override; ///< Algorithm initialization
  SelReports::types::Output_t operator()( SelReports::types::In_t const& in ) const override; ///< Algorithm execution

private:
  ToolHandle<IReportConvert>                            m_conv{this, "ConvTool", "ReportConvertTool"};
  DataObjectReadHandle<SelReports::types::DecReports_t> m_dec_reps{this, "DecReports", ""};
  Gaudi::Property<std::vector<std::string>>             m_selection_names{
      this, "SelectionNames", {}, "Specify the line selection names."};
  Gaudi::Property<std::vector<Gaudi::StringKey>> m_inputcandidatetypes{
      this, "CandidateTypes", {}, "Map of selection name to candidate type."};
  Gaudi::Property<int> m_hlt_stage{this, "HltStage", 1, "Hlt1 or 2?"};

  /// for producing numerical info to be saved on the object
  LHCb::HltObjectSummary::Info infoToSave( const LHCb::Track& object, unsigned int const presentInfoLevel,
                                           bool const turbo ) const;
  LHCb::HltObjectSummary::Info infoToSave( const LHCb::RecVertex& object, unsigned int const presentInfoLevel,
                                           bool const turbo ) const;

  /// for splitting strings
  std::vector<std::string>& split( const std::string&, char, std::vector<std::string>& );
  std::vector<std::string>  split( const std::string&, char );

  /// for converting objects in to summaries

  const LHCb::HltObjectSummary* store_( const LHCb::Track& object, LHCb::HltObjectSummary::Container& objectSummaries,
                                        unsigned int const presentInfoLevel, bool const turbo ) const;
  const LHCb::HltObjectSummary* store_( const LHCb::RecVertex&             object,
                                        LHCb::HltObjectSummary::Container& objectSummaries,
                                        unsigned int const presentInfoLevel, bool const turbo ) const;

  template <typename T>
  const LHCb::HltObjectSummary* store( const ContainedObject* obj, LHCb::HltObjectSummary::Container& objectSummaries,
                                       unsigned int const presentInfoLevel, bool const turbo ) const {
    auto const* tobj = dynamic_cast<const T*>( obj );
    if ( !tobj ) return nullptr;
    auto i = std::find_if( std::begin( objectSummaries ), std::end( objectSummaries ),
                           [&]( const LHCb::HltObjectSummary* hos ) {
                             return hos->summarizedObjectCLID() == tobj->clID() && hos->summarizedObject() == tobj;
                           } );
    return ( ( i != std::end( objectSummaries ) ) && !turbo )
               ? *i
               : store_( *tobj, objectSummaries, presentInfoLevel, turbo );
  }

  int rank( const LHCb::Track& object, unsigned int const presentInfoLevel, bool const turbo ) const;
  int rank( const LHCb::RecVertex& object, unsigned int const presentInfoLevel, bool const turbo ) const;

  /// rank LHCbIDs for selection rank
  int rankLHCbIDs( const std::vector<LHCb::LHCbID>& lhcbIDs ) const;

  /// for trimming output size and disabling output all together (if 0)
  unsigned int maximumNumberOfCandidatesToStore( const std::string& selectionName, bool const debugMode ) const;

  /// set present output parameters
  unsigned int setPresentInfoLevel( const std::string& selectionName, bool const debugMode ) const;

  /// HltANNSvc for making selection names to int selection ID
  ServiceHandle<IIndexedANNSvc> m_hltANNSvc{this, "ANNSvc", "HltANNSvc"};

  // from info id to its name
  GaudiUtils::VectorMap<int, std::string> m_infoIntToName;

  // get trigger selection names //  -- amalgamate into vector<struct>
  struct selectionInfo {
    Gaudi::StringKey id;
    Gaudi::StringKey type;
    int              intId;
    int              maxCand;
    int              maxCandDebug;
  };
  std::vector<selectionInfo> m_selectionInfo;

  /// encoding key
  Gaudi::Property<unsigned int> m_key{this, "EncodingKey", 0u};

  /// enable Turbo level output
  Gaudi::Property<bool> m_enableTurbo{this, "EnableTurbo", true};

  // RelatedInfo
  Gaudi::Property<std::map<std::string, std::vector<std::string>>> m_RelInfoLocationsMap{
      this, "RelatedInfoLocations", {}};

  /// debug event period (global, can't be change per selection)  0=never 1=always e.g. 100=every 100th event
  Gaudi::Property<unsigned int> m_debugPeriod{"DebugEventPeriod", 0};

  /// default max number of candidates for Decision selections (can be overruled by per selection setting)
  Gaudi::Property<unsigned int> m_maxCandidatesDecision{this, "MaxCandidatesDecision", 1000};
  Gaudi::Property<unsigned int> m_maxCandidatesDecisionDebug{this, "MaxCandidatesDecisionDebug", 5000};

  Gaudi::Property<unsigned int> m_maxCandidatesNonDecision{this, "MaxCandidatesNonDecision", 0};
  Gaudi::Property<unsigned int> m_maxCandidatesNonDecisionDebug{this, "MaxCandidatesNonDecisionDebug", 5000};

  /// per selection max number of candidates
  using SelectionSetting = std::map<std::string, int>;
  Gaudi::Property<SelectionSetting> m_maxCandidates{this, "SelectionMaxCandidates", {}};
  Gaudi::Property<SelectionSetting> m_maxCandidatesDebug{this, "SelectionMaxCandidatesDebug", {}};

  /// per selection info level
  Gaudi::Property<SelectionSetting> m_infoLevel{this, "SelectionInfoLevel", {}};
  Gaudi::Property<SelectionSetting> m_infoLevelDebug{this, "SelectionInfoLevelDebug", {}};

  /// default output info level for Decision selections (can be overruled by per selection setting)
  Gaudi::Property<unsigned int> m_infoLevelDecision{this, "InfoLevelDecision", (unsigned int)kMinInfoLevel};
  Gaudi::Property<unsigned int> m_infoLevelDecisionDebug{this, "InfoLevelDecisionDebug", (unsigned int)kMaxInfoLevel};
  Gaudi::Property<unsigned int> m_infoLevelNonDecision{this, "InfoLevelNonDecision", (unsigned int)kMinInfoLevel};
  Gaudi::Property<unsigned int> m_infoLevelNonDecisionDebug{this, "InfoLevelNonDecisionDebug",
                                                            (unsigned int)kMaxInfoLevel};
};

DECLARE_COMPONENT( SelReportsMaker )

//-----------------------------------------------------------------------------
// Implementation file for class : SelReportsMaker
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SelReportsMaker::SelReportsMaker( const std::string& name, ISvcLocator* pSvcLocator )
    : base( name, pSvcLocator, {"Inputs", {}}, {KeyValue{"SelReports", ""}, KeyValue{"ObjectSummaries", ""}} ) {}

//=============================================================================
// Initialization
//=============================================================================

StatusCode SelReportsMaker::initialize() {
  return base::initialize()
      .andThen( [&] { return m_conv.retrieve(); } )
      .andThen( [&]() -> StatusCode {
        if ( m_selection_names.size() != m_inputcandidatetypes.size() ) {
          error() << "Length of SelectionNames and CandidateTypes must match!" << endmsg;
          return StatusCode::FAILURE;
        }
        return StatusCode::SUCCESS;
      } )
      .andThen( [&] { return m_hltANNSvc.retrieve(); } )
      .andThen( [&] {
        // get string-to-int selection ID map
        const auto& selectionNameToIntMap =
            m_hltANNSvc->s2i( m_key, "Hlt" + std::to_string( m_hlt_stage ) + "SelectionID" );
        m_selectionInfo.reserve( m_selection_names.size() );
        for ( auto i = 0u; i < m_selection_names.size(); ++i ) {
          auto im = selectionNameToIntMap.find( m_selection_names[i] );
          if ( im == selectionNameToIntMap.end() ) {
            error() << " selectionName = " << m_selection_names[i] << " not found in HltANNSvc. " << endmsg;
            return StatusCode::FAILURE;
          }
          selectionInfo Info;
          Info.id           = m_selection_names[i];
          Info.intId        = im->second;
          Info.maxCandDebug = maximumNumberOfCandidatesToStore( m_selection_names[i], true );
          Info.maxCand      = maximumNumberOfCandidatesToStore( m_selection_names[i], false );
          Info.type         = m_inputcandidatetypes[i];
          m_selectionInfo.push_back( std::move( Info ) );
        }
        return StatusCode::SUCCESS;
      } )
      .andThen( [&] {
        m_infoIntToName.clear();
        for ( const auto& i : m_hltANNSvc->i2s( m_key, InfoID ) ) {
          if ( i.first <= 65535 ) { m_infoIntToName.insert( i.first, i.second ); }
        }
      } );
}

//=============================================================================
// Main execution
//=============================================================================

SelReports::types::Output_t SelReportsMaker::operator()( SelReports::types::In_t const& type_erased_inputs ) const {
  if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "==> Execute" << endmsg;

  assert( m_selection_names.size() == type_erased_inputs.size() );

  LHCb::HltDecReports const& decReports = *m_dec_reps.get();

  // determine output mode
  bool debugMode = true; // FIXME ( ctx.eventID().event_number() % m_debugPeriod == 0 );

  // create output container and put it on TES
  SelReports::types::Output_t output{};
  auto&                       outputSummary   = std::get<0>( output );
  auto&                       objectSummaries = std::get<1>( output );

  auto rank_selection = [&]( auto const& selection, auto const& selname ) {
    auto const& cand             = selection[0];
    auto        presentInfoLevel = setPresentInfoLevel( selname, debugMode );
    // classify according to first candidate
    return rank( cand, presentInfoLevel, false );
  };

  using ranked_data_t = std::tuple<int, selectionInfo const*, DataObject const*>;
  std::vector<ranked_data_t> ranked_data{};
  ranked_data.reserve( m_selectionInfo.size() );
  for ( auto i = 0u; i < m_selectionInfo.size(); ++i ) {
    auto const& selInfo   = m_selectionInfo[i];
    auto const& name      = selInfo.id;
    auto const* decReport = decReports.decReport( name );
    if ( !decReport ) {
      throw GaudiException( "Decision name " + name + " not in HltDecReports!", this->name(), StatusCode::FAILURE );
    }
    if ( !decReport || !decReport->decision() ) continue;
    if ( type_erased_inputs[i] == nullptr ) {
      // This algorithm is typically run as a 'barrier algorithm', in which case
      // the application is responsible for making sure the line-dependent
      // inputs to this algorithm are present on a positive line decision.
      // We fail fast here in case that condition is not met.
      throw GaudiException( "Positive decision for " + name + " but empty input for SelReports making", this->name(),
                            StatusCode::FAILURE );
    }
    if ( selInfo.type == VectorOfTrack ) {
      auto const& tracks =
          static_cast<AnyDataWrapper<std::vector<LHCb::Track>> const*>( type_erased_inputs[i] )->getData();
      if ( tracks.empty() ) continue;
      ranked_data.emplace_back( rank_selection( tracks, name ), &selInfo, type_erased_inputs[i] );
    } else if ( selInfo.type == VectorOfRecVertex ) {
      auto const& vertices =
          static_cast<AnyDataWrapper<std::vector<LHCb::RecVertex>> const*>( type_erased_inputs[i] )->getData();
      if ( vertices.empty() ) continue;
      ranked_data.emplace_back( rank_selection( vertices, name ), &selInfo, type_erased_inputs[i] );
    } else {
      throw GaudiException( "candidate type for selection " + name + " not supported", this->name(),
                            StatusCode::FAILURE );
    }
  }

  auto rank_less = []( const ranked_data_t& elem1, const ranked_data_t& elem2 ) {
    if ( std::get<0>( elem1 ) != std::get<0>( elem2 ) ) return std::get<0>( elem1 ) < std::get<0>( elem2 );
    // equal ranked_data; now use selection name; decisions come last
    const auto decision_1 = boost::algorithm::ends_with( std::get<1>( elem1 )->id.str(), "Decision" );
    const auto decision_2 = boost::algorithm::ends_with( std::get<1>( elem2 )->id.str(), "Decision" );
    if ( decision_1 != decision_2 ) return decision_2;
    const auto l1 = std::get<1>( elem1 )->id.str().length();
    const auto l2 = std::get<1>( elem2 )->id.str().length();
    return ( l1 != l2 ) ? ( l1 < l2 ) : ( std::get<1>( elem1 )->id < std::get<1>( elem2 )->id );
  };

  // data compression requires that we store objects from early processing stages first
  // order selections accordingly
  // filtering of selections for persistency also happens in this loop

  std::sort( ranked_data.begin(), ranked_data.end(), rank_less );

  auto make_selection_summary = [&]( auto const* selInfo, auto const& data, bool turbo ) {
    // trim number of candidates if too large
    int maxCandidates = debugMode ? selInfo->maxCandDebug : selInfo->maxCand;
    int noc           = std::min( int( data.size() ), maxCandidates );

    LHCb::HltObjectSummary selSumOut;
    selSumOut.setSummarizedObjectCLID( 1 ); // use special CLID for selection summaries (lowest number for sorting to
                                            // the end)
    // integer selection id
    selSumOut.addToInfo( "0#SelectionID", float( selInfo->intId ) );

    auto presentInfoLevel = setPresentInfoLevel( selInfo->id, debugMode );

    // must also save candidates
    int nocc( 0 );

    for ( const auto& c : data ) {
      if ( ++nocc > noc ) break;

      const LHCb::HltObjectSummary* hos =
          store<std::decay_t<decltype( c )>>( &c, objectSummaries, presentInfoLevel, turbo );
      selSumOut.addToSubstructure( hos );
    }

    if ( outputSummary.insert( selInfo->id, selSumOut ) == StatusCode::FAILURE ) {
      fatal() << " Failed to add Hlt selection name " + selInfo->id + " to its container " << endmsg;
    }
  };

  for ( auto const& [rnk, selInfo, obj] : ranked_data ) {
    bool turbo_signature = ( decReports.decReport( selInfo->id )->executionStage() & 0x80 );
    //   // Check if the Hlt line has been marked for Turbo level output
    bool const turbo = turbo_signature && m_enableTurbo;

    if ( selInfo->type == VectorOfTrack ) {
      const auto& tracks = static_cast<AnyDataWrapper<std::vector<LHCb::Track>> const*>( obj )->getData();
      make_selection_summary( selInfo, tracks, turbo );
    } else if ( selInfo->type == VectorOfRecVertex ) {
      auto const& vertices = static_cast<AnyDataWrapper<std::vector<LHCb::RecVertex>> const*>( obj )->getData();
      make_selection_summary( selInfo, vertices, turbo );
    }
  }

  std::transform(
      std::begin( outputSummary ), std::end( outputSummary ), std::back_inserter( objectSummaries ),
      []( LHCb::HltSelReports::Container::const_reference i ) { return new LHCb::HltObjectSummary( i.second ); } );

  // ----------------- printout ------------------------
  if ( msgLevel( MSG::VERBOSE ) ) {

    verbose() << " ======= HltSelReports size= " << outputSummary.size() << endmsg;
    verbose() << outputSummary << endmsg;

    verbose() << " ======= HltObjectSummary container size= " << objectSummaries.size() << endmsg;
    for ( const auto& pHos : objectSummaries ) {
      verbose() << " key " << pHos->index();
      const auto& selby = outputSummary.selectedAsCandidateBy( pHos );
      if ( !selby.empty() ) {
        verbose() << " selectedAsCandidateBy=";
        for ( const auto& i : selby ) verbose() << i << " ";
        std::pair<std::string, int> pvInfo = outputSummary.pvSelectionNameAndKey( pHos );
        if ( pvInfo.second > -1 ) {
          verbose() << " pvSelectionName= " << pvInfo.first << " pvKey= " << pvInfo.second << " ";
        }
      }
      verbose() << *pHos << endmsg;
    }
  }
  return output;
}

// -------------------------------------
// store Track in HltObjectSummary store
// -------------------------------------

const LHCb::HltObjectSummary* SelReportsMaker::store_( const LHCb::Track&                 object,
                                                       LHCb::HltObjectSummary::Container& objectSummaries,
                                                       unsigned int const presentInfoLevel, bool const turbo ) const {
  // not yet in object store; create new one
  LHCb::HltObjectSummary* hos{new HltObjectSummary()};
  hos->setSummarizedObjectCLID( object.clID() );
  hos->setSummarizedObject( &object );

  LHCb::HltObjectSummary::Info theseInfo = infoToSave( object, presentInfoLevel, turbo );
  const auto                   theseHits = object.lhcbIDs();

  auto rbegin1 = theseHits.cend();
  auto rend1   = theseHits.cbegin();
  if ( !theseHits.empty() ) { // required to avoid decrementing iterator for
                              // empty container which triggers ubsan error
    --rbegin1;
    --rend1;
  }

  // look for objects with the same lhcbids
  // if many then the one which leaves least amount info on this one
  LHCb::HltObjectSummary* pBestHos{nullptr};

  // First assume we keep this one
  bool reuse = false;
  pBestHos   = hos;

  // find mutations of the same object
  // leave the one with the most info, should be the Turbo
  if ( !objectSummaries.empty() ) { // required to avoid decrementing iterator for
                                    // empty container which triggers ubsan error
    LHCb::HltObjectSummarys::const_iterator rbegin = objectSummaries.end();
    --rbegin;
    LHCb::HltObjectSummarys::const_iterator rend = objectSummaries.begin();
    --rend;
    for ( auto ppHos = rbegin; ppHos != rend; --ppHos ) {
      LHCb::HltObjectSummary* pHos( *ppHos );
      if ( pHos->summarizedObjectCLID() == object.clID() ) {
        const auto otherHits = pHos->lhcbIDsFlattened();
        bool       different = false;
        if ( otherHits.size() == theseHits.size() ) {
          auto h2 = otherHits.cend();
          --h2;
          for ( auto h1 = rbegin1; h1 != rend1; --h1, --h2 ) {
            if ( h1->lhcbID() != h2->lhcbID() ) {
              different = true;
              break;
            }
          }
          if ( different ) continue;

          // found object of the same type with the same lhcbid content!
          //
          // If:
          // - it contains different info, leave it as it is.
          // - if less info, choose one with the biggest info
          LHCb::HltObjectSummary::Info otherInfo = pHos->numericalInfoFlattened();
          if ( otherInfo.size() > theseInfo.size() ) {
            reuse    = true;
            pBestHos = pHos;
            break;
          }
          if ( otherInfo.size() == theseInfo.size() ) {
            bool diffCalc = false;
            for ( auto i1 = otherInfo.begin(); i1 != otherInfo.end(); ++i1 ) {
              auto i2 = theseInfo.find( i1->first );
              if ( i1->second != i2->second ) {
                diffCalc = true;
                break;
              }
            }
            if ( !diffCalc ) {
              reuse    = true;
              pBestHos = pHos;
              break;
            }
          }
        }
      }
    }
  }

  if ( !reuse ) {
    hos->setLhcbIDs( std::move( theseHits ) );
    hos->setNumericalInfo( theseInfo );
    objectSummaries.push_back( hos );
    return objectSummaries.back();
  } else {
    delete hos;
  }

  return pBestHos;
}

const LHCb::HltObjectSummary* SelReportsMaker::store_( const LHCb::RecVertex&             object,
                                                       LHCb::HltObjectSummary::Container& objectSummaries,
                                                       unsigned int const presentInfoLevel, bool const turbo ) const {
  // not yet in object store; create new one
  auto hos = std::make_unique<LHCb::HltObjectSummary>();
  hos->setSummarizedObjectCLID( object.clID() );
  hos->setSummarizedObject( &object );

  // find mutations of the same object
  LHCb::HltObjectSummary::Info theseInfo = infoToSave( object, presentInfoLevel, turbo );

  if ( !turbo ) { // For Turbo, this saves Refitted PVs, save it and do nothing else
    SmartRefVector<LHCb::HltObjectSummary> thisSubstructure;
    const auto&                            tracks = object.tracks();
    std::transform( std::begin( tracks ), std::end( tracks ), std::back_inserter( thisSubstructure ),
                    [&]( const LHCb::Track* t ) { return store_( *t, objectSummaries, presentInfoLevel, turbo ); } );

    auto rbegin1 = thisSubstructure.cend();
    auto rend1   = thisSubstructure.cbegin();
    if ( !thisSubstructure.empty() ) { // required to avoid decrementing iterator for
                                       // empty container which triggers ubsan error
      --rbegin1;
      --rend1;
    }

    //    look for objects with the same lhcbids
    //      if many then the one which leaves least amount info on this one
    int                     smallestSize = theseInfo.size();
    const HltObjectSummary* pBestHos{nullptr};

    //   couldn't get reverse operator to work
    if ( !objectSummaries.empty() ) { // required to avoid decrementing iterator for
                                      // empty container which triggers ubsan error
      auto rbegin = objectSummaries.end();
      --rbegin;
      auto rend = objectSummaries.begin();
      --rend;
      for ( auto ppHos = rbegin; ppHos != rend; --ppHos ) {
        const LHCb::HltObjectSummary* pHos{*ppHos};
        if ( pHos->summarizedObjectCLID() == object.clID() ) {
          auto otherSubstructure = pHos->substructureFlattened();
          bool different         = false;
          if ( otherSubstructure.size() == thisSubstructure.size() ) {
            auto h2 = otherSubstructure.end();
            --h2;
            for ( auto h1 = rbegin1; h1 != rend1; --h1, --h2 ) {
              if ( ( *h1 ) != ( *h2 ) ) {
                different = true;
                break;
              }
            }
            if ( different ) continue;
            // found object of the same type with the same (flattened) substructure!
            // now check if its Info content is a subset of this one
            LHCb::HltObjectSummary::Info otherInfo = pHos->numericalInfoFlattened();
            if ( otherInfo.size() <= theseInfo.size() ) {
              bool notcontained = false;
              for ( const auto& i1 : otherInfo ) {
                auto i2 = theseInfo.find( i1.first );
                if ( i2 == theseInfo.end() ) {
                  notcontained = true;
                  break;
                }
                if ( i1.second != i2->second ) break;
              }
              if ( notcontained ) continue;
              if ( otherInfo.size() == theseInfo.size() ) {
                smallestSize = 0;
                pBestHos     = pHos;
                // don't need to look any more since subobject has all the info
                break;
              } else {
                int presentSize = theseInfo.size() - otherInfo.size();
                if ( presentSize < smallestSize ) {
                  smallestSize = presentSize;
                  pBestHos     = pHos;
                }
              }
            }
          }
        }
      }
    }
    // found subobject
    if ( pBestHos ) {
      if ( smallestSize == 0 ) {
        theseInfo.clear();
      } else {
        for ( const auto& i1 : pBestHos->numericalInfoFlattened() ) { theseInfo.erase( i1.first ); }
      }
      // set subobject link
      thisSubstructure.clear();
      // set substructure link
      thisSubstructure.push_back( pBestHos );
    }

    hos->setSubstructure( thisSubstructure );
  }
  hos->setNumericalInfo( std::move( theseInfo ) );
  objectSummaries.push_back( hos.release() );
  return objectSummaries.back();
}

//=============================================================================
// fetch info to save; to find object follow summarizedObject pointer
//=============================================================================

LHCb::HltObjectSummary::Info SelReportsMaker::infoToSave( const LHCb::Track& candidate,
                                                          unsigned int const presentInfoLevel,
                                                          bool const         turbo ) const {
  LHCb::HltObjectSummary::Info infoPersistent;
  if ( kMinInfoLevel == presentInfoLevel ) return infoPersistent;

  // if( !(hos.summarizedObject()) )return infoPersistent;
  if ( kExtraInfoLevel & presentInfoLevel ) {
    for ( const auto& ei : candidate.extraInfo() ) {
      if ( ( 0 <= ei.first ) && ( ei.first <= 65535 ) ) {
        auto i = m_infoIntToName.find( ei.first );
        if ( i != std::end( m_infoIntToName ) ) { infoPersistent.insert( i->second, float( ei.second ) ); }
      }
    }
  }
  if ( kStandardInfoLevel & presentInfoLevel ) {
    if ( candidate.nStates() ) m_conv->TrackObject2Summary( &infoPersistent, &candidate, turbo );
  }
  return infoPersistent;
}

LHCb::HltObjectSummary::Info SelReportsMaker::infoToSave( const LHCb::RecVertex& candidate,
                                                          unsigned int const     presentInfoLevel,
                                                          bool const             turbo ) const {
  LHCb::HltObjectSummary::Info infoPersistent;
  if ( kMinInfoLevel == presentInfoLevel ) return infoPersistent;
  if ( kExtraInfoLevel & presentInfoLevel ) {
    for ( const auto& ei : candidate.extraInfo() ) {
      if ( ( 0 <= ei.first ) && ( ei.first <= 65535 ) ) {
        auto i = m_infoIntToName.find( ei.first );
        if ( i != m_infoIntToName.end() ) { infoPersistent.insert( i->second, float( ei.second ) ); }
      }
    }
  }
  if ( kStandardInfoLevel & presentInfoLevel ) {
    m_conv->RecVertexObject2Summary( &infoPersistent, &candidate, turbo );
  }
  return infoPersistent;
}

//=============================================================================
int SelReportsMaker::rank( const SelReports::types::Track_t& object, unsigned int const presentInfoLevel,
                           bool const turbo ) const {
  return 100 * rankLHCbIDs( object.lhcbIDs() ) + infoToSave( object, presentInfoLevel, turbo ).size();
}

int SelReportsMaker::rank( const SelReports::types::Vertex_t& object, unsigned int const presentInfoLevel,
                           bool const turbo ) const {
  return 10000 + infoToSave( object, presentInfoLevel, turbo ).size();
}

//=============================================================================

int SelReportsMaker::rankLHCbIDs( const std::vector<LHCb::LHCbID>& lhcbIDs ) const {
  return std::accumulate( std::begin( lhcbIDs ), std::end( lhcbIDs ), 0, []( int rank, const LHCb::LHCbID& id ) {
    if ( id.isVP() )
      rank |= 8;
    else if ( id.isUT() )
      rank |= 16;
    else if ( id.isFT() )
      rank |= 32;
    else if ( id.isMuon() )
      rank |= 4;
    else if ( id.isCalo() )
      rank |= 2;
    else
      rank |= 1;
    return rank;
  } );
}

//=============================================================================
// qualify trigger selections for persistency
//=============================================================================

unsigned int SelReportsMaker::maximumNumberOfCandidatesToStore( const std::string& selName,
                                                                bool const         debugMode ) const {
  if ( debugMode ) {
    // DEBUG MODE

    // set max number of candidates
    const SelectionSetting& maxCandidatesDebug = m_maxCandidatesDebug.value();
    for ( auto i = maxCandidatesDebug.cbegin(); i != maxCandidatesDebug.cend(); ++i ) {
      if ( selName == i->first ) return i->second;
    }

    // not set specifically for this selection ; use default
    return boost::algorithm::ends_with( selName, "Decision" ) ? m_maxCandidatesDecisionDebug
                                                              : m_maxCandidatesNonDecisionDebug;
  } else {

    // NORMAL MODE

    // set max number of candidates
    const SelectionSetting& maxCandidates = m_maxCandidates.value();
    for ( auto i = maxCandidates.cbegin(); i != maxCandidates.cend(); ++i ) {
      if ( selName == i->first ) return i->second;
    }

    // not set specifically for this selection ; use default
    return boost::algorithm::ends_with( selName, "Decision" ) ? m_maxCandidatesDecision : m_maxCandidatesNonDecision;
  }
}

//=============================================================================
// set output info level for this selection
//=============================================================================

unsigned int SelReportsMaker::setPresentInfoLevel( const std::string& selName, bool const debugMode ) const {
  if ( debugMode ) {
    // DEBUG MODE
    // set max number of candidates
    for ( auto const& [k, v] : m_infoLevelDebug )
      if ( selName == k ) return v;

    // not set specifically for this selection ; use default
    if ( selName.find( "Decision" ) != std::string::npos ) { return m_infoLevelDecisionDebug; }
    return m_infoLevelNonDecisionDebug;
  } else {
    // NORMAL MODE
    // set max number of candidates
    for ( auto const& [k, v] : m_infoLevel )
      if ( selName == k ) return v;

    // not set specifically for this selection ; use default
    if ( selName.find( "Decision" ) != std::string::npos ) { return m_infoLevelDecision; }
    return m_infoLevelNonDecision;
  }
}
