/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/PackedData.h"
#include "Event/RawBank.h"
#include <GaudiKernel/StatusCode.h>
#include <GaudiKernel/StringKey.h>
#include <iomanip>
#include <iostream>
#include <string>
#include <type_traits>

namespace LHCb::Hlt::DAQ {

  namespace v2 {
    /// Bit masks in the SourceID word -- Compression only for bank version < 3, as for run3, online reserves 0xF800!!!
    enum struct SourceIDMasks : uint16_t { PartID = 0x00FF, Reserved = 0x0F00, Compression = 0xE000 };
  } // namespace v2

  inline namespace v3 {
    enum struct SourceIDMasks : uint16_t { PartID = 0x00FF, Process = 0x0700, OnlineReserved = 0xF800 };
  }

  template <auto m>
  uint16_t extract( uint16_t i ) {
    static_assert( std::is_same_v<decltype( m ), v2::SourceIDMasks> ||
                   std::is_same_v<decltype( m ), v3::SourceIDMasks> );
    auto s = __builtin_ctz( static_cast<unsigned>( m ) );
    return ( i & static_cast<unsigned>( m ) ) >> s;
  }

  template <auto m>
  uint16_t shift( uint16_t i ) {
    static_assert( std::is_same_v<decltype( m ), v2::SourceIDMasks> ||
                   std::is_same_v<decltype( m ), v3::SourceIDMasks> );
    auto     s = __builtin_ctz( static_cast<unsigned>( m ) );
    uint16_t v = ( i << s ) & static_cast<unsigned>( m );
    assert( extract<m>( v ) == i );
    return v;
  }

  enum SourceID : unsigned int {
    Dummy     = 0,
    Hlt1      = 1,
    Hlt2      = 2,
    Spruce    = 3,
    Max       = 7,
    BitShift  = 13,
    MinorMask = 0x1FFF,
  };

  StatusCode  parse( SourceID& id, const std::string& s );
  std::string toString( SourceID );

  inline std::ostream& toStream( SourceID id, std::ostream& os ) { return os << std::quoted( toString( id ), '\'' ); }
  inline std::ostream& operator<<( std::ostream& s, SourceID e ) { return toStream( e, s ); }

  const Gaudi::StringKey& selectionID_for( SourceID id );

  /// Return the compression from the SourceID word
  inline std::optional<PackedData::Compression> compression( RawBank const& b ) {
    if ( b.type() != RawBank::BankType::DstData ) return std::nullopt;
    switch ( b.version() ) {
    case 2:
      return static_cast<PackedData::Compression>( extract<v2::SourceIDMasks::Compression>( b.sourceID() ) );
    default:
      return std::nullopt;
    }
  }

  template <RawBank::BankType>
  unsigned int getSequenceNumber( const RawBank& );

  template <>
  inline unsigned int getSequenceNumber<RawBank::HltSelReports>( const RawBank& b ) {
    return b.version() < 12 ? ( b.sourceID() & 0x1FFF ) : extract<v3::SourceIDMasks::PartID>( b.sourceID() );
  }
  template <>
  inline unsigned int getSequenceNumber<RawBank::DstData>( const RawBank& b ) {
    // v2 PartID is the same as v3
    return extract<SourceIDMasks::PartID>( b.sourceID() );
  }

  template <RawBank::BankType>
  SourceID getSourceID( RawBank const& b );

  template <>
  inline SourceID getSourceID<RawBank::HltSelReports>( const RawBank& b ) {
    return b.version() < 12 ? SourceID{( static_cast<unsigned int>( b.sourceID() ) >> 13 )}
                            : SourceID{extract<v3::SourceIDMasks::Process>( b.sourceID() )};
  }
  template <>
  inline SourceID getSourceID<RawBank::DstData>( const RawBank& b ) {
    return b.version() < 3 ? SourceID::Dummy
                           : static_cast<SourceID>( extract<v3::SourceIDMasks::Process>( b.sourceID() ) );
  }
  template <>
  inline SourceID getSourceID<RawBank::HltDecReports>( const RawBank& b ) {
    return b.version() == 1 ? SourceID::Dummy
                            : b.version() == 2 ? SourceID{( static_cast<unsigned int>( b.sourceID() ) >> 13 )}
                                               : SourceID{extract<v3::SourceIDMasks::Process>( b.sourceID() )};
  }

  inline SourceID getSourceID( RawBank const& b ) {
    switch ( b.type() ) {
    case RawBank::HltSelReports:
      return getSourceID<RawBank::HltSelReports>( b );
    case RawBank::HltDecReports:
      return getSourceID<RawBank::HltDecReports>( b );
    case RawBank::DstData:
      return getSourceID<RawBank::DstData>( b );
    default:
      return SourceID::Dummy;
    }
  }

} // namespace LHCb::Hlt::DAQ
