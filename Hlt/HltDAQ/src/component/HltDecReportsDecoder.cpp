/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReport.h"
#include "Event/HltDecReports.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "HltDecoderUtils.h"
#include "HltSourceID.h"
#include "Kernel/IIndexedANNSvc.h"
#include "LHCbAlgs/Transformer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltDecReportsDecoder
//
// 2008-08-02 : Tomasz Skwarnicki
//-----------------------------------------------------------------------------

/** @class HltDecReportsDecoder HltDecReportsDecoder.h
 *
 *
 *  @author Tomasz Skwarnicki
 *  @date   2008-08-02
 *
 *  Algorithm to read HltDecReports from Raw Data and create container on TES
 *
 */

namespace LHCb::Hlt::DAQ {

  class DecReportsDecoder : public Algorithm::Transformer<HltDecReports( const RawBank::View& )> {
  public:
    /// Standard constructor
    DecReportsDecoder( const std::string& name, ISvcLocator* pSvcLocator );

    ///< Algorithm execution
    HltDecReports operator()( const RawBank::View& ) const override;

  private:
    template <typename HDRConverter, typename Range, typename Table>
    int decodeHDR( HDRConverter converter, Range& input, HltDecReports& output, const Table& table ) const;

    Gaudi::Property<SourceID>     m_sourceID{this, "SourceID", SourceID::Dummy};
    Gaudi::Property<unsigned>     m_forcedKey{this, "ForcedKey", 0,
                                          "if non-zero, ignore TCK in decreport, and use this value"};
    ServiceHandle<IIndexedANNSvc> m_svc{this, "DecoderMapping", "HltANNSvc"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_overrule_nonzero_key{
        this, " HltDecReports has a non-zero TCK, but is explicitly overruled for decoding -- make sure that this "
              "really what you want"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_not_overruled_zero_key{
        this, " HltDecReports has a zero TCK, and it is not explicitly specified for decoding -- make sure that this "
              "really what you want"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_bad_version{
        this, " HltDecReports RawBank version # is larger then the known ones.... cannot decode, use newer software "
              "version. "};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_no_key{
        this, " No string key found for trigger decision in storage", 50};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_duplicate_key{
        this, " Duplicate string key found for trigger decision in storage", 50};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_multiple_banks{
        this,
        " More then one HltDecReports RawBanks for requested SourceID in RawEvent. Will only process the first one. ",
        20};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( DecReportsDecoder, "HltDecReportsDecoder" )

  namespace {

    // version 1 layout:
    // decision:  0x        1                      x
    // error:     0x        e                   xxx0
    // #cand:     0x       f0              xxxx 0000
    // stage:     0x     ff00    xxxx xxxx 0000 0000
    // id:        0xffff 0000
    // version 0 layout:
    // decision:  0x        1                      x
    // error:     0x       70              0xxx 0000
    // #cand:     0x     ff80    xxxx xxxx x000 0000
    // stage:     0x        e                   xxx0
    // id:        0xffff 0000
    struct v0_v1 {
      HltDecReport operator()( unsigned int x ) const {
        // ID & decision stay the same
        unsigned int temp = ( x & 0xffff0001 );
        // stage needs to be moved left
        temp |= ( x & 0xe ) << 7;
        // number of candidates -- move & truncate
        unsigned int nc = std::min( ( x >> 7 ) & 0x1ff, 0xfu );
        temp |= nc << 4;
        // error just moves to the right
        temp |= ( x & 0x70 ) >> 3;
        return HltDecReport( temp );
      }
    };

    struct vx_vx {
      HltDecReport operator()( unsigned int x ) const { return HltDecReport( x ); }
    };

    template <typename T>
    auto pop( LHCb::span<T>& s ) {
      auto d = s.front();
      s      = s.subspan( 1 );
      return d;
    }

  } // namespace

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  DecReportsDecoder::DecReportsDecoder( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, KeyValue{"RawBanks", "DAQ/RawBanks/HltDecReports"},
                     KeyValue{"OutputHltDecReportsLocation", HltDecReportsLocation::Default} ) {}
  //=============================================================================
  // Main execution
  //=============================================================================
  HltDecReports DecReportsDecoder::operator()( const LHCb::RawBank::View& rawBanks ) const {

    auto hltdecreportsRawBanks = selectRawBanks( m_sourceID, rawBanks );
    if ( hltdecreportsRawBanks.empty() ) {
      throw GaudiException( " No HltDecReports RawBank with sourceID " + std::to_string( m_sourceID.value() ) +
                                " found",
                            name(), StatusCode::FAILURE );
    }
    if ( hltdecreportsRawBanks.size() != 1 ) ++m_multiple_banks;
    const RawBank* hltdecreportsRawBank = hltdecreportsRawBanks.front();
    if ( hltdecreportsRawBank->type() != LHCb::RawBank::HltDecReports ) {
      throw GaudiException( " Wrong bank type for HltDecReportsDecoder", name(), StatusCode::FAILURE );
    }

    if ( hltdecreportsRawBank->magic() != LHCb::RawBank::MagicPattern ) {
      throw GaudiException( " HltDecReports RawBank has wrong magic number. Return without decoding.", name(),
                            StatusCode::FAILURE );
    }
    if ( hltdecreportsRawBank->version() > 3 ) {
      throw GaudiException(
          " HltDecReports RawBank version # is larger then the known ones.... cannot decode, use newer version.",
          name(), StatusCode::FAILURE );
    }

    // ----------------------------------------------------------
    auto data = hltdecreportsRawBank->range<unsigned int>();

    // create output container
    HltDecReports outputSummary;
    // version 3 has an explicit encoding key in addition to the TCK
    unsigned int key = ( hltdecreportsRawBank->version() > 2 ? pop( data ) : 0u );
    // version 0 has only decreps, version 1 has TCK, taskID, then decreps...
    if ( hltdecreportsRawBank->version() > 0 ) {
      outputSummary.setConfiguredTCK( pop( data ) );
      outputSummary.setTaskID( pop( data ) );
    }
    // --------------------------------- get configuration --------------------
    if ( key != 0 && m_forcedKey != 0u ) ++m_overrule_nonzero_key;
    if ( key == 0 ) key = outputSummary.configuredTCK();
    if ( key == 0 && m_forcedKey == 0u ) ++m_not_overruled_zero_key;

    const auto& tbl =
        m_svc->i2s( m_forcedKey == 0u ? key : m_forcedKey.value(), selectionID_for( m_sourceID.value() ) );

    // ---------------- loop over decisions in the bank body; insert them into the output container
    int err = 0;
    switch ( hltdecreportsRawBank->version() ) {
    case 0:
      err += this->decodeHDR( v0_v1{}, data, outputSummary, tbl );
      break;
    case 1:
    case 2:
    case 3:
      err += this->decodeHDR( vx_vx{}, data, outputSummary, tbl );
      break;
    default:
      ++m_bad_version;
      err += 1;
    }

    if ( msgLevel( MSG::VERBOSE ) ) {
      // debugging info
      verbose() << " ====== HltDecReports container size=" << outputSummary.size() << endmsg;
      verbose() << outputSummary << endmsg;
    }
    if ( err != 0 ) {
      throw GaudiException( " HltDecReports RawBank error during decoding.", name(), StatusCode::FAILURE );
    }
    return outputSummary;
  }

  template <typename HDRConverter, typename R, typename Table>
  int DecReportsDecoder::decodeHDR( HDRConverter converter, R& input, HltDecReports& output,
                                    const Table& table ) const {
    int ret = 0;
    while ( !input.empty() ) {
      auto dec  = converter( pop( input ) );
      auto isel = table.find( dec.intDecisionID() );
      if ( isel == end( table ) ) { // oops missing.
        ++m_no_key;
        ++ret;
      } else {
        output.insert( isel->second, dec )
            .orElse( [&] {
              ++m_duplicate_key;
              ++ret;
            } )
            .ignore();
      }
    }
    return ret;
  }
} // namespace LHCb::Hlt::DAQ
