/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "GaudiKernel/GaudiException.h"
#include "HltSourceID.h"

namespace LHCb::Hlt::DAQ {
  std::vector<const LHCb::RawBank*>        selectRawBanks( SourceID, LHCb::RawBank::View );
  inline std::vector<const LHCb::RawBank*> selectRawBanks( SourceID id, RawBank::BankType type,
                                                           LHCb::RawEvent const& evt ) {
    return selectRawBanks( id, evt.banks( type ) );
  }
  unsigned int tck( SourceID id, const LHCb::RawEvent& event, const LHCb::RawBank::BankType& type );
  unsigned int tck( SourceID id, const LHCb::RawBank::View& banks );

  // for use in HltSelReports decoder -- fetch the key from the corresponding HltDecReport...
  unsigned int key( SourceID id, const LHCb::RawEvent& event, const LHCb::RawBank::BankType& type );
  unsigned int key( SourceID id, const LHCb::RawBank::View& banks );

} // namespace LHCb::Hlt::DAQ
