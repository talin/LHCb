/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// from Gaudi
#include "Event/HltDecReports.h"
#include "Event/ODIN.h"
#include "Event/RawEvent.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/ParsersFactory.h"
#include "LHCbAlgs/Transformer.h"

/** @class RoutingBitsWriter RoutingBitsWriter.h
 *  @brief Write routing bits based on ODIN flags and DecReports into raw event.
 *
 *  The definition of the routing bits is given as a map between the routing bit
 *  and a list of line names contributing to this bit. The combined decision of the
 *  lines is evaluated and used to set a Routing bit. The Routing bits are written to the raw event.
 *  The maximum number of bits is 128.
 *
 *  Example:
 *
 *  Set Routing bit 33 based on Hlt1TrackMVALine and Hlt1TwoTrackMVALine decsion
 *
 *  @code
 *  from PyConf.Algorithms import Hlt_RoutingBitsWriter
 *  Hlt_RoutingBitsWriter(
 *     RoutingBits = { "33" : ["Hlt1TrackMVALine", "Hlt1TwoTrackMVALine"] },
 *     DecReports = <dec_reports>,
 *     ODIN= <odin>,
 *     RawEventLocation = <raw_event>,
 *  )
 *  @endcode
 */

namespace LHCb::Hlt::DAQ {
  class RoutingBitsWriter : public Algorithm::MultiTransformer<std::tuple<LHCb::RawEvent, LHCb::RawBank::View>(
                                                                   LHCb::ODIN const&, LHCb::HltDecReports const& ),
                                                               Algorithm::Traits::writeOnly<RawEvent>> {
    enum { nBits = 3 * sizeof( unsigned int ) * 8 };

    Gaudi::Property<std::map<unsigned int, std::vector<std::string>>> m_bits{
        this, "RoutingBits", {}, [this]( auto& ) {
          for ( const auto& row : m_bits ) {
            auto bit = row.first;
            if ( bit > 127 ) {
              this->error() << "Too large Routing bit defined. Number must be smaller than 128." << endmsg;
              throw GaudiException{"Too large Routing bit defined. Number must be smaller than 128.",
                                   "Hlt::RoutingBitsWriter", StatusCode::FAILURE};
            }
          }
        }};

  public:
    /// Standard constructor
    RoutingBitsWriter( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer{name,
                           pSvcLocator,

                           {KeyValue{"ODIN", ""}, KeyValue{"DecReports", ""}},
                           {KeyValue{"RawEventLocation", "/Event/DAQ/HltRoutingBitsEvent"},
                            KeyValue{"OutputView", "/Event/DAQ/HltRoutingBits"}}} {};

    std::tuple<LHCb::RawEvent, LHCb::RawBank::View>
    operator()( LHCb::ODIN const&, LHCb::HltDecReports const& dec_reports ) const override {

      // Create *new* RawEvent
      LHCb::RawEvent outputrawEvent;

      // TODO: Add evaluators of odin bits

      // The routing bits
      std::array<unsigned int, 3> bits{0, 0, 0};

      // Evaluate the routing bits
      for ( auto const& entry : m_bits ) {
        auto result = std::any_of( begin( entry.second ), end( entry.second ), [&dec_reports]( auto const& line ) {
          auto const* dec_report = dec_reports.decReport( line );
          return dec_report && dec_report->decision() == 1;
        } );
        auto bit    = entry.first;
        int  word   = bit / 32;
        if ( result ) bits[word] |= ( 0x01UL << ( bit - 32 * word ) );
      }

      // Get the raw event and add the routing bits bank.
      outputrawEvent.addBank( 0, LHCb::RawBank::HltRoutingBits, 0, bits );
      return std::tuple{std::move( outputrawEvent ), outputrawEvent.banks( RawBank::HltRoutingBits )};
    }
  };
} // namespace LHCb::Hlt::DAQ

namespace Hlt {
  // keep backwards compatible for now
  struct RoutingBitsWriter : LHCb::Hlt::DAQ::RoutingBitsWriter {
    using LHCb::Hlt::DAQ::RoutingBitsWriter::RoutingBitsWriter;
  };
  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( Hlt::RoutingBitsWriter )
} // namespace Hlt
