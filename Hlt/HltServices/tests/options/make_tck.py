from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from past.builtins import execfile
import os
import argparse
from Configurables import HltGenConfig
from Configurables import GaudiSequencer
from Configurables import DeterministicPrescaler
from Configurables import ConfigCDBAccessSvc
from Configurables import ApplicationMgr
from Configurables import LHCbApp

from DDDB.CheckDD4Hep import UseDD4Hep

parser = argparse.ArgumentParser(usage='usage: %(prog)s prescale')

parser.add_argument("prescale", type=float, help="what prescale")
parser.add_argument(
    "--clear",
    action='store_true',
    help='remove TCK db before creating the TCK')

args = parser.parse_args()

scale = float(args.prescale)

app = LHCbApp()
app.EvtMax = -1
app.DataType = '2016'

if not UseDD4Hep:
    from Configurables import CondDB
    CondDB().LatestGlobalTagByDataTypes = [app.DataType]

# Location of TCK database
TCKData = 'TCKData'
filename = os.path.join(TCKData, 'config.cdb')
if not os.path.exists(TCKData):
    os.makedirs(TCKData)
elif args.clear and os.path.exists(filename):
    # remove the target file if requested
    os.remove(filename)

# TCK access service
accessSvc = ConfigCDBAccessSvc(File=filename, Mode='ReadWrite')

# Sequence, actually only a prescaler
seq = GaudiSequencer("TestSequence")
prescaler = DeterministicPrescaler(
    "TestScaler", SeedName="TestScaler", AcceptFraction=scale)
seq.Members = [prescaler]

# Algorithm to generate the TCK
gen = HltGenConfig(
    ConfigTop=[seq.getName()],
    ConfigSvc=['ToolSvc'],
    ConfigAccessSvc=accessSvc.getName(),
    HltType="LHCb_Test",
    MooreRelease="v1r0",
    Label="Test")

# make sure gen is the very first Top algorithm
ApplicationMgr().TopAlg = [gen.getFullName(), seq]

# Instantiate AppMgr and run some events
from GaudiPython.Bindings import AppMgr

gaudi = AppMgr()
TES = gaudi.evtSvc()

gaudi.initialize()
gaudi.start()
gaudi.stop()
gaudi.finalize()
gaudi.exit()

print('PASSED')
