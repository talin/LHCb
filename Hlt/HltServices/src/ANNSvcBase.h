/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/Service.h"
#include "Kernel/IIndexedANNSvc.h"
#include "Kernel/IIndexedLumiSchemaSvc.h"
#include "Kernel/SynchronizedValue.h"
#include <map>
#include <string>

namespace ANNSvcBase_details {

  inline const std::array<std::string, 5> s_majors{"Hlt1SelectionID", "Hlt2SelectionID", "SpruceSelectionID", "InfoID",
                                                   "PackedObjectLocations"};

  inline int index_major( const Gaudi::StringKey& major ) {
    auto i = std::find( s_majors.begin(), s_majors.end(), major );
    return i == s_majors.end() ? -1 : std::distance( s_majors.begin(), i );
  }

  inline auto reverse( IIndexedANNSvc::map_t const& m ) {
    IIndexedANNSvc::inv_map_t imap;
    for ( auto const& [k, v] : m ) {
      auto [_, ok] = imap.insert( v, k );
      if ( !ok ) throw GaudiException( "unable to invert map", __PRETTY_FUNCTION__, StatusCode::FAILURE );
    }
    return imap;
  }

} // namespace ANNSvcBase_details

class ANNSvcBase : public extends<Service, IIndexedANNSvc, IIndexedLumiSchemaSvc> {

public:
  using extends::extends;

  map_t const& i2s( unsigned int index, const Gaudi::StringKey& major ) const override final {
    auto i = ANNSvcBase_details::index_major( major );
    if ( i < 0 ) throw GaudiException{"bad major key " + major, __PRETTY_FUNCTION__, StatusCode::FAILURE};
    auto ptr = m_maps[i].with_lock( [&]( std::map<unsigned int, map_t>& map ) -> map_t const* {
      auto j = map.find( index );
      if ( j != map.end() ) return &j->second;
      auto [k, ok] = map.emplace( index, fetch( index, major ) );
      return ok ? &k->second : nullptr;
    } );
    if ( !ptr ) throw GaudiException{"Failed to add to map", __PRETTY_FUNCTION__, StatusCode::FAILURE};
    return *ptr;
  }

  inv_map_t const& s2i( unsigned int index, const Gaudi::StringKey& major ) const override final {
    auto i = ANNSvcBase_details::index_major( major );
    if ( i < 0 ) throw GaudiException{"bad major key " + major, __PRETTY_FUNCTION__, StatusCode::FAILURE};
    auto ptr = m_inv_maps[i].with_lock( [&]( std::map<unsigned int, inv_map_t>& imap ) -> inv_map_t const* {
      auto j = imap.find( index );
      if ( j != imap.end() ) return &j->second;
      auto [k, ok] = imap.emplace( index, ANNSvcBase_details::reverse( this->i2s( index, major ) ) );
      return ok ? &k->second : nullptr;
    } );
    if ( !ptr ) throw GaudiException{"Failed to add to map", __PRETTY_FUNCTION__, StatusCode::FAILURE};
    return *ptr;
  }

  lumi_schema_t const& lumiCounters( unsigned key, unsigned version ) const override final {
    auto ptr = m_lumi.with_lock( [&]( std::map<std::pair<unsigned, unsigned>, lumi_schema_t>& lumi ) {
      auto j = lumi.find( {key, version} );
      if ( j != lumi.end() ) return &j->second;
      auto [k, ok] = lumi.emplace( std::pair{key, version}, fetch_lumi( key, version ) );
      return ok ? &k->second : nullptr;
    } );
    if ( !ptr ) throw GaudiException{"Failed to add to lumi schema map", __PRETTY_FUNCTION__, StatusCode::FAILURE};
    return *ptr;
  }

private:
  Gaudi::Property<bool> m_allow_zero_as_key{this, "AllowZeroAsKey", false};

  virtual map_t         fetch( unsigned int tck, const Gaudi::StringKey& major ) const = 0;
  virtual lumi_schema_t fetch_lumi( unsigned int key, unsigned int version ) const     = 0;

  mutable std::array<LHCb::cxx::SynchronizedValue<std::map<unsigned int, map_t>>, ANNSvcBase_details::s_majors.size()>
      m_maps;
  mutable std::array<LHCb::cxx::SynchronizedValue<std::map<unsigned int, inv_map_t>>,
                     ANNSvcBase_details::s_majors.size()>
      m_inv_maps;

  mutable LHCb::cxx::SynchronizedValue<std::map<std::pair<unsigned, unsigned>, lumi_schema_t>> m_lumi;
};
