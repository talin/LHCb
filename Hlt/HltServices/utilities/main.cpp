/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//// TODO: fold class CDB back into ConfigCDBAccessSvc...
#include "boost/algorithm/string/classification.hpp"
#include "boost/algorithm/string/join.hpp"
#include "boost/algorithm/string/predicate.hpp"
#include "boost/algorithm/string/split.hpp"
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/iostreams/copy.hpp"
#include "boost/iostreams/device/array.hpp"
#include "boost/iostreams/filter/bzip2.hpp"
#include "boost/iostreams/filter/gzip.hpp"
#include "boost/iostreams/filter/zlib.hpp"
#include "boost/iostreams/filtering_stream.hpp"
#include "boost/iostreams/slice.hpp"
#include "boost/iostreams/stream.hpp"
#include "boost/program_options.hpp"
#include "boost/property_tree/json_parser.hpp"
#include "boost/property_tree/ptree.hpp"
#include <fcntl.h>
#include <git2.h>
#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "Kernel/ConfigTreeNode.h"
#include "Kernel/PropertyConfig.h"

const auto bad_leaf = Gaudi::Math::MD5::createFromStringRep( "38b39cf06458ca7e5c768c555da93527" );

void check( int error ) {
  if ( error >= 0 ) return;
  const git_error* e = git_error_last();
  printf( "Error %d/%d: %s\n", error, e->klass, e->message );
  exit( error );
}

template <typename K, typename C, typename A>
std::ostream& operator<<( std::ostream& os, std::set<K, C, A> const& s ) {
  os << " { ";
  for ( const auto& i : s ) os << i << " ";
  return os << " }\n";
}

std::ostream& operator<<( std::ostream& s, git_oid const& oid ) {
  // return s << std::string_view{ (const char*)oid.id, sizeof(oid.id) };
  char buffer[GIT_OID_HEXSZ] = {0};
  git_oid_fmt( buffer, &oid );
  return s << "oid: " << std::string_view{buffer, sizeof( buffer )};
}

template <auto traverse = GIT_TREEWALK_POST, typename CB>
int walk_tree( const git_tree* tree, CB* cb ) {
  return git_tree_walk(
      tree, traverse,
      []( const char* root, const git_tree_entry* te, void* ptr ) { return ( *static_cast<CB*>( ptr ) )( root, te ); },
      cb );
}

std::set<Gaudi::Math::MD5> tree_to_md5( git_repository* repo, const git_tree* tree ) {
  struct Payload {
    git_repository*            repo = nullptr;
    std::set<Gaudi::Math::MD5> set  = {};
    int                        operator()( const char* /* root */, const git_tree_entry* te ) {
      if ( GIT_OBJ_BLOB == git_tree_entry_type( te ) ) {
        git_blob* blob = nullptr;
        check( git_blob_lookup( &blob, repo, git_tree_entry_id( te ) ) );
        PropertyConfig     pc;
        std::istringstream in( std::string{(const char*)git_blob_rawcontent( blob ), git_blob_rawsize( blob )} );
        pc.read( in );
        // std::cout << root << git_tree_entry_name( te ) << " -> " << pc.digest() << '\n';
        set.emplace( pc.digest() );
        git_blob_free( blob );
      }
      return 0;
    }
  };
  auto cb = Payload{repo};
  check( walk_tree( tree, &cb ) );
  return std::move( cb.set );
}

template <typename DB>
std::set<Gaudi::Math::MD5> tree_to_md5( DB const& db, const ConfigTreeNode& tn ) {
  std::set<Gaudi::Math::MD5> r;
  if ( tn.leaf() ) {
    auto pc = db.propertyConfig( tn.leaf() );
    if ( !pc ) { std::cerr << "oops -- failed to resolve " << tn.leaf() << '\n'; }
    // std::cout << pc->kind() << "/" << pc->fullyQualifiedName() << " -> " << pc->digest() << '\n';
    r.emplace( pc->digest() );
  }
  for ( const auto& digest : tn.nodes() ) {
    auto node = db.treeNode( digest );
    if ( !node ) std::cerr << "oops -- failed to resolve " << digest << '\n';
    r.merge( tree_to_md5( db, *node ) );
  }
  return r;
}

template <typename DB>
git_oid make_tree( git_repository* repo, DB const& in, ConfigTreeNode const& toplevel ) {
  git_oid    tree_oid{};
  git_index* idx{nullptr};
  check( git_repository_index( &idx, repo ) );
  check( git_index_clear( idx ) );
  check( index_add_treenode_leaves( in, idx, toplevel ) );
  check( git_index_write_tree_to( &tree_oid, idx, repo ) ); // write the tree into the repo
  check( git_index_write( idx ) );
  git_index_free( idx );
  return tree_oid;
}

git_oid commit_tree( git_repository* repo, git_oid const& tree_oid, git_oid const& oid, git_signature const* sig,
                     std::string const& comment ) {
  git_oid   commit2_id{};
  git_tree* tree{nullptr};
  check( git_tree_lookup( &tree, repo, &tree_oid ) ); // same as before
  check( git_commit_create_v( &commit2_id, repo, "HEAD", sig, sig, nullptr, comment.c_str(), tree, 1, &oid ) );
  git_tree_free( tree );
  return commit2_id;
}

template <typename Entry>
git_oid tag_head( git_repository* repo, Entry const& entry, git_signature const* sig ) {
  git_oid     tag_oid{};
  git_object* commit2 = nullptr;
  check( git_revparse_single( &commit2, repo, "HEAD" ) );

  int e = git_tag_create( &tag_oid, repo, entry.tck.c_str(), commit2, sig, entry.comment.c_str(), 0 );
  if ( e == GIT_EEXISTS ) {
    // some (trivial) TCKs appear multiple times -- they _are_ the same, but are available for multiple releases...
    e = git_tag_create( &tag_oid, repo, ( entry.tck + "-" + entry.release ).c_str(), commit2, sig,
                        entry.comment.c_str(), 0 );
  }
  check( e );
  git_object_free( commit2 );
  return tag_oid;
}

git_oid make_branch_and_set_head( git_repository* repo, const std::string& branch_name, git_commit* commit ) {
  git_oid oid{};
  if ( int error = git_reference_name_to_id( &oid, repo, ( "refs/heads/" + branch_name ).c_str() );
       error == GIT_ENOTFOUND ) {
    check( git_reference_name_to_id( &oid, repo, "refs/heads/MyDefaultBranch" ) );
    git_reference* branch = nullptr;
    check( git_commit_lookup( &commit, repo, &oid ) );
    check( git_branch_create( &branch, repo, branch_name.c_str(), commit, 0 ) );
    check( git_reference_symbolic_create( &branch, repo, "HEAD", git_reference_name( branch ), 1,
                                          ( "creating " + branch_name ).c_str() ) );
    check( git_reference_name_to_id( &oid, repo, ( "refs/heads/" + branch_name ).c_str() ) );
    git_reference_free( branch );
  } else {
    check( error );
  }
  check( git_repository_set_head( repo, ( "refs/heads/" + branch_name ).c_str() ) );
  return oid;
}

template <typename T, typename Record>
auto to_( const Record& record ) {
  std::istringstream in( record.value() );
  T                  t;
  in >> t;
  return t;
}

template <typename Record>
int index_add_record( git_index* idx, const Record& record, const std::string& name = "",
                      const std::string& content = "" ) {
  auto            key     = ( !name.empty() ? name : record.key() );
  auto            payload = ( !content.empty() ? content : record.value() );
  git_index_entry entry{};
  entry.ctime.seconds = record.time();
  entry.mtime.seconds = record.time();
  entry.mode          = GIT_FILEMODE_BLOB;
  entry.path          = key.c_str();
  entry.uid           = record.uid();
  entry.file_size     = payload.size();
  // TODO: check that this entry does not yet exist in the index!!!!
  return git_index_add_from_buffer( idx, &entry, payload.data(), payload.size() );
}

template <typename DB>
int index_add_treenode_leaves( DB const& db, git_index* idx, const ConfigTreeNode& tn ) {
  int e = 0;
  if ( tn.leaf() ) {
    auto ipc = db.find( "PC/" + tn.leaf().str() );
    if ( ipc == db.end() ) std::cerr << "oops -- could not find PC " << tn.leaf() << '\n';
    auto pc = to_<PropertyConfig>( *ipc );

    std::ostringstream ss;

    using ptree = boost::property_tree::ptree;
    ptree top;
    top.put( "Name", pc.name() );
    top.put( "Kind", pc.kind() );
    top.put( "Type", pc.type() );
    std::transform( begin( pc.properties() ), end( pc.properties() ),
                    std::back_inserter( top.put_child( "Properties", ptree{} ) ), []( const auto& i ) {
                      return std::pair{i.first, ptree{i.second}};
                    } );
    ptree tools{};
    ptree algs{};
    ptree svcs{};
    ptree auds{};
    ptree unks{};

    auto select = [&]( const std::string& kind ) -> ptree& {
      if ( kind == "IAlgorithm" )
        return algs;
      else if ( kind == "IService" )
        return svcs;
      else if ( kind == "IAlgTool" )
        return tools;
      else if ( kind == "IAuditor" )
        return auds;
      else
        return unks;
    };

    for ( const auto& node_id : tn.nodes() ) {
      auto tn = db.treeNode( node_id );
      if ( !tn ) std::cerr << "oops -- could not find " << node_id << "\n";
      if ( auto leaf = tn->leaf(); leaf ) {
        auto pc2 = db.propertyConfig( leaf );
        if ( !pc2 ) std::cerr << "oops -- could not find PC " << leaf << '\n';
        select( pc2->kind() ).push_back( ptree::value_type{"", pc2->fullyQualifiedName()} );
      }
    }
    if ( !tools.empty() ) top.put_child( "IAlgTools", tools );
    if ( !algs.empty() ) top.put_child( "IAlgorithms", algs );
    if ( !svcs.empty() ) top.put_child( "IServices", svcs );
    if ( !auds.empty() ) top.put_child( "IAuditors", auds );

    write_json( ss, top, true );

    auto e = index_add_record( idx, *ipc, pc.kind() + "/" + pc.fullyQualifiedName(), ss.str().c_str() );
    check( e );
  }
  // loop over nodes, and go recursive
  for ( const auto& node_id : tn.nodes() ) {
    auto itn = db.treeNode( node_id );
    if ( !itn ) std::cerr << "oops -- could not find " << node_id << "\n";
    auto e = index_add_treenode_leaves( db, idx, *itn );
    check( e );
  }
  return e;
}

template <typename T>
std::string convert( const std::string& s ) {
  std::istringstream in( s );
  std::ostringstream out;
  T                  t;
  in >> t;
  out << t;
  return out.str();
}

struct to_json_t {
  std::string operator()( const std::string& s ) const;
  using r2t_t = std::map<std::string, std::string>;
  std::string operator()( const std::tuple<std::string, r2t_t, std::string>& e ) const;

  template <typename K, typename V>
  std::string operator()( const std::pair<K, V>& p ) const;
  template <typename V>
  std::string operator()( const std::map<std::string, V>& m ) const;
  template <typename V>
  std::string operator()( const std::map<Gaudi::Math::MD5, V>& m ) const;
  std::string operator()( const Gaudi::Math::MD5& md5 ) const { return ( *this )( md5.str() ); }
};
inline constexpr to_json_t to_json{};

std::string to_json_t::operator()( const std::string& s ) const {
  std::ostringstream out;
  out << quoted( s );
  return out.str();
}

template <typename K, typename V>
std::string to_json_t::operator()( const std::pair<K, V>& p ) const {
  return to_json( p.first ) + ":" + to_json( p.second );
}

template <typename V>
std::string to_json_t::operator()( const std::map<std::string, V>& m ) const {
  std::vector<std::string> buf;
  buf.reserve( m.size() );
  std::transform( std::begin( m ), std::end( m ), std::back_inserter( buf ), to_json );
  return "{" + boost::algorithm::join( buf, "," ) + "}";
}

template <typename V>
std::string to_json_t::operator()( const std::map<Gaudi::Math::MD5, V>& m ) const {
  std::vector<std::string> buf;
  buf.reserve( m.size() );
  std::transform( std::begin( m ), std::end( m ), std::back_inserter( buf ), to_json );
  return "{" + boost::algorithm::join( buf, "," ) + "}";
}

std::string to_json_t::operator()( const std::tuple<std::string, to_json_t::r2t_t, std::string>& e ) const {
  return "{" + to_json( std::pair{"TCK", std::get<0>( e )} ) + "," +
         to_json( std::pair{"Release2Type", std::get<1>( e )} ) + "," +
         to_json( std::pair{"label", std::get<2>( e )} ) + "}";
}

#include "../src/cdb.h"
#include "../src/tar.h"

namespace io = boost::iostreams;
namespace fs = boost::filesystem;

int compress( std::string& str ) {
  // compress and check if worthwhile...
  std::string compressed;
  compressed.reserve( str.size() );
  io::filtering_streambuf<io::output> filter;
  io::zlib_params                     params;
  params.noheader = true;
  filter.push( io::zlib_compressor( params ) );
  filter.push( io::back_inserter( compressed ) );
  io::copy( boost::make_iterator_range( str ), filter, 8192 );
  bool ok = compressed.size() < str.size(); // yes, it's better!
  if ( ok ) str.swap( compressed );
  return ok ? 3 : 0;
}

std::vector<unsigned char> make_cdb_record( std::string str, uid_t uid, std::time_t t ) {
  auto                       flags = compress( str );
  std::vector<unsigned char> buffer( 12 + str.size(), 0 );
  auto                       buf = std::begin( buffer );
  *buf++                         = 0;     // version number
  *buf++                         = flags; // compression
  *buf++                         = 0;     // reserved;
  *buf++                         = 0;     // reserved;
  assert( sizeof( uid_t ) == 4 );
  *buf++ = ( uid & 0xff );
  *buf++ = ( ( uid >> 8 ) & 0xff );
  *buf++ = ( ( uid >> 16 ) & 0xff );
  *buf++ = ( ( uid >> 24 ) & 0xff );
  assert( sizeof( t ) == 4 ); // seconds since 1970  ( ok for 136 years )
  *buf++ = ( t & 0xff );
  *buf++ = ( ( t >> 8 ) & 0xff );
  *buf++ = ( ( t >> 16 ) & 0xff );
  *buf++ = ( ( t >> 24 ) & 0xff );
  if ( std::distance( std::begin( buffer ), buf ) != 12 ) { std::cerr << "ERROR" << std::endl; }
  auto e = std::copy_n( std::begin( str ), str.size(), buf );
  if ( e != std::end( buffer ) ) { std::cerr << "ERROR" << std::endl; }
  return buffer;
}

struct manifest_entry {
  template <typename TCKS>
  manifest_entry( const std::string& toplevel, const TCKS& tcks, std::string com ) {
    std::vector<std::string> tokens;
    boost::algorithm::split( tokens, toplevel, boost::algorithm::is_any_of( "/" ) );
    assert( tokens.size() == 3 );
    release   = tokens[0];
    type      = tokens[1];
    id        = Gaudi::Math::MD5::createFromStringRep( tokens[2] );
    auto itck = tcks.find( id.str() );
    tck       = ( itck != std::end( tcks ) ) ? itck->second : "<NONE>";
    comment   = std::move( com );
  }
  std::string      release, type, tck, comment;
  Gaudi::Math::MD5 id;

  friend bool operator<( const manifest_entry& lhs, const manifest_entry& rhs ) {
    // can we get MOORE_v9r1 prior to MOORE_v10r1 ???
    // if the string contains a digit at the point where they are different,
    // do a numerical <
    auto order = []( const manifest_entry& m ) { return std::tie( m.release, m.type, m.tck, m.id, m.comment ); };
    return order( lhs ) < order( rhs );
  }
};

std::ostream& operator<<( std::ostream& os, const manifest_entry& e ) {
  return os << e.release << " : " << e.type << " : " << e.tck << " : " << e.id << " : " << e.comment;
}

std::string format_time( std::time_t t ) {
  // gcc4.8 doesn't have std::put_time???
  static char mbstr[100];
  mbstr[0] = 0;
  std::strftime( mbstr, sizeof( mbstr ), "%A %c", std::localtime( &t ) );
  return {mbstr};
}

class TAR {
  std::fstream                          m_file;
  std::map<std::string, std::streamoff> m_index;

public:
  TAR( const std::string& fname ) { m_file.open( fname.c_str(), std::ios::in | std::ios::binary ); }

  bool ok() const { return m_file.good(); }

  class record {
    ConfigTarFileAccessSvc_details::Info m_info;
    std::fstream*                        m_file;

  public:
    record() = default;
    record( std::fstream* file, ConfigTarFileAccessSvc_details::Info info )
        : m_info( std::move( info ) ), m_file( file ){};
    uid_t              uid() const { return m_info.uid; }
    std::time_t        time() const { return m_info.mtime; }
    const std::string& key() const { return m_info.name; }
    std::string        value() const {
      m_file->seekg( 0, std::ios_base::beg );
      io::filtering_istream s;
      if ( m_info.compressed ) s.push( io::gzip_decompressor() );
      s.push( io::slice( *m_file, m_info.offset, m_info.size ) );
      std::string value;
      std::copy( std::istreambuf_iterator<char>( s ), std::istreambuf_iterator<char>(), std::back_inserter( value ) );
      return value;
    }
    unsigned int valuePersistentSize() const { return m_info.size; }
    bool         isTCK() { return key().size() == 22 && key().compare( 0, 12, "Aliases/TCK/" ) == 0; }
    bool         isTopLevel() { return key().back() != '/' && key().compare( 0, 17, "Aliases/TOPLEVEL/" ) == 0; }
    std::string  TCK() { return isTCK() ? key().substr( 12 ) : std::string{}; }
    std::string  topLevel() { return isTopLevel() ? key().substr( 17 ) : std::string{}; }
  };

  class iterator {
    friend class TAR;
    std::fstream*  m_file;
    std::streamoff m_pos;

    // WARNING: advances the position in the file to just after the header...
    ConfigTarFileAccessSvc_details::Info read_info() {
      ConfigTarFileAccessSvc_details::Info info;
      if ( !m_file ) return info;
      m_file->seekg( m_pos, std::ios::beg );
      static ConfigTarFileAccessSvc_details::posix_header header;
      if ( !m_file->read( (char*)&header, sizeof( header ) ) ) {
        m_file = nullptr;
        std::cerr << "unexpected EOF" << std::endl;
      }
      if ( !interpretHeader( *m_file, header, info ) ) {
        // check whether we're at the end of the file: (at least) two all-zero
        // headers)
        if ( !isZero( header ) ) {
          std::cerr << "failed to interpret header" << std::endl;
        } else {
          m_file->read( (char*)&header, sizeof( header ) );
          if ( !isZero( header ) ) { std::cerr << "missing 2nd zero header " << std::endl; }
        }
        m_file = nullptr;
      }
      return info;
    }
    void next() {
      if ( !m_file ) return;
      auto info = read_info(); // read current header, and position at the end of it...
      if ( !m_file ) return;
      size_t skip    = info.size;
      size_t padding = skip % 512;
      if ( padding != 0 ) skip += 512 - padding;
      m_pos = m_file->tellg(); // read_info put us at the end of the CURRENT header
      m_pos += skip;           // m_pos now points at the start of the NEXT header...
    }
    bool is_current_valid() {
      auto info = read_info();
      return m_file && !info.name.empty() && info.name.back() != '/';
    }
    void skip_invalid() {
      while ( m_file && !is_current_valid() ) next();
    }

  public:
    iterator() : m_file{nullptr}, m_pos{0} {}
    iterator( std::fstream* file, std::streamoff pos = 0 ) : m_file{file}, m_pos{pos} { skip_invalid(); }

    record operator*() { // Not quite canonical -- ideally should be record&... and const
      return {m_file, read_info()};
    }
    iterator& operator++() {
      next();
      skip_invalid();
      return *this;
    }

    friend bool operator==( const iterator& lhs, const iterator& rhs ) {
      return lhs.m_file == rhs.m_file && ( !lhs.m_file || lhs.m_pos == rhs.m_pos );
    }
    friend bool operator!=( const iterator& lhs, const iterator& rhs ) {
      return lhs.m_file != rhs.m_file || ( lhs.m_file && lhs.m_pos != rhs.m_pos );
    }
  };

  iterator begin() { return {&m_file}; }
  iterator end() const { return {}; }

  iterator find( const std::string& key ) {
    if ( m_index.empty() ) index();
    auto i = m_index.find( key );
    if ( i == std::end( m_index ) ) return {};
    return {&m_file, i->second};
  }

  iterator findTreeNode( const std::string& hash ) {
    return find( std::string( "ConfigTreeNodes/" ) + hash.substr( 0, 2 ) + "/" + hash );
  }

  void index() {
    for ( auto i = begin(), e = end(); i != e; ++i ) { m_index.emplace( ( *i ).key(), i.m_pos ); }
  }
};

class CDB {
  mutable struct cdb m_db;

public:
  CDB( const std::string& fname ) {
    auto fd = open( fname.c_str(), O_RDONLY );
    if ( cdb_init( &m_db, fd ) < 0 ) cdb_fileno( &m_db ) = -1;
  }
  ~CDB() {
    auto fd = cdb_fileno( &m_db );
    if ( fd >= 0 ) close( fd );
    cdb_free( &m_db );
  }

  class record {
    const void *m_kpos, *m_vpos;
    unsigned    m_klen, m_vlen;

  public:
    record( const void* kpos, const void* vpos, unsigned klen, unsigned vlen )
        : m_kpos{kpos}, m_vpos{vpos}, m_klen{klen}, m_vlen{vlen} {
      assert( vlen >= 12 );
    }
    unsigned int valuePersistentSize() const { return m_vlen; }
    std::string  key() const { return {static_cast<const char*>( m_kpos ), m_klen}; }
    unsigned int flags() const { return *( static_cast<const unsigned char*>( m_vpos ) + 1 ); }
    uid_t        uid() const {
      auto base = static_cast<const unsigned char*>( m_vpos );
      auto f    = [=]( unsigned int i, unsigned int j ) { return uid_t( base[i] ) << j; };
      return f( 4, 0 ) | f( 5, 8 ) | f( 6, 16 ) | f( 7, 24 );
    }
    std::time_t time() const {
      auto base = static_cast<const unsigned char*>( m_vpos );
      auto f    = [=]( unsigned int i, unsigned int j ) { return std::time_t( base[i] ) << j; };
      return f( 8, 0 ) | f( 9, 8 ) | f( 10, 16 ) | f( 11, 24 );
    }
    std::string value() const {
      io::stream<io::array_source> buffer( static_cast<const char*>( m_vpos ) + 12, m_vlen - 12 );
      io::filtering_istream        s;
      switch ( flags() & 0x3 ) {
      case 0:
        break; // do nothing...
      case 2:
        s.push( io::bzip2_decompressor() );
        break;
      case 3: {
        io::zlib_params params;
        params.noheader = true;
        s.push( io::zlib_decompressor( params ) );
      } break;
      default:
        std::cerr << "unknown compression flag" << std::endl;
        return 0;
      }
      s.push( buffer );
      std::string value;
      std::copy( std::istreambuf_iterator<char>( s ), std::istreambuf_iterator<char>(), std::back_inserter( value ) );
      return value;
    }
    bool        isTCK() { return key().compare( 0, 7, "AL/TCK/" ) == 0; }
    bool        isTopLevel() { return key().compare( 0, 12, "AL/TOPLEVEL/" ) == 0; }
    std::string TCK() { return isTCK() ? key().substr( 7 ) : std::string{}; }
    std::string topLevel() { return isTopLevel() ? key().substr( 12 ) : std::string{}; }
  };

  class iterator {
    struct cdb* m_db;
    unsigned    m_cpos;
    bool        atEnd() const { return !m_db; }

  public:
    iterator( struct cdb* parent = nullptr, unsigned cpos = 0 ) : m_db{parent}, m_cpos( cpos ) {
      if ( m_db && !m_cpos ) cdb_seqinit( &m_cpos, m_db );
      ++*this;
    }
    iterator& operator++() {
      if ( m_db && !( cdb_seqnext( &m_cpos, m_db ) > 0 ) ) m_db = nullptr;
      return *this;
    }
    bool   operator==( const iterator& rhs ) const { return atEnd() ? rhs.atEnd() : m_cpos == rhs.m_cpos; }
    bool   operator!=( const iterator& rhs ) const { return atEnd() ? !rhs.atEnd() : m_cpos != rhs.m_cpos; }
    record operator*() const { // Not quite canonical -- ideally should be record&...
      return {cdb_getkey( m_db ), cdb_getdata( m_db ), cdb_keylen( m_db ), cdb_datalen( m_db )};
    }
  };

  iterator begin() const { return {&m_db}; }
  iterator end() const { return {}; }

  iterator find( const std::string& key ) const {
    if ( cdb_find( &m_db, key.c_str(), key.size() ) > 0 ) {
      // Hrmpf. Use inside knowledge of the layout of the cdb structure...
      //        ... so that 'next' will do the right thing...
      unsigned int offset = std::distance( m_db.cdb_mem, static_cast<const unsigned char*>( cdb_getkey( &m_db ) ) );
      return {&m_db, offset - 8};
    }
    return end();
  }

  iterator findTreeNode( const std::string& hash ) const { return find( std::string( "TN/" ) + hash ); }

  std::optional<ConfigTreeNode> treeNode( Gaudi::Math::MD5 const& digest ) const {
    auto i = find( std::string( "TN/" ) + digest.str() );
    if ( i == end() ) return std::nullopt;
    return to_<ConfigTreeNode>( *i );
  }
  std::optional<PropertyConfig> propertyConfig( Gaudi::Math::MD5 const& digest ) const {
    auto i = find( std::string( "PC/" ) + digest.str() );
    if ( i == end() ) return std::nullopt;
    return to_<PropertyConfig>( *i );
  }

  bool ok() const { return cdb_fileno( &m_db ) >= 0; }
};

template <typename DB>
std::multiset<manifest_entry> create_manifest( DB& db ) {
  std::multiset<manifest_entry> manifest;

  // first: get TCK -> id map, and invert.
  // needed so that we can make immutable manifest_entries later...
  std::map<std::string, std::string> tck; // id -> TCK
  for ( auto record : db ) {              // TODO: allow loop with predicate on key...
    if ( !record.isTCK() ) continue;
    auto key = record.key();
    tck.emplace( record.value(), record.TCK() );
  }
  // next: create manifest
  for ( auto record : db ) { // TODO: allow loop with predicate on key...
    if ( !record.isTopLevel() ) continue;
    auto key = record.key();
    // the 'comment' is in the 'label' member of the treenode this alias "points" at
    std::string comment;
    auto        i = db.findTreeNode( key.substr( key.rfind( "/" ) + 1 ) );
    if ( i != std::end( db ) ) {
      auto ctn = to_<ConfigTreeNode>( *i );
      comment  = ctn.label();
      if ( comment.empty() ) {
        std::cerr << "could not locate Label part of " << std::endl;
        std::cerr << ( *i ).value() << std::endl;
      }
    } else {
      std::cerr << "WARNING: could not locate treenode " << key.substr( key.rfind( "/" ) + 1 ) << " for key " << key
                << std::endl;
    }
    manifest.emplace( record.topLevel(), tck, comment );
  }
  return manifest;
}

void dump_manifest( const std::multiset<manifest_entry>& manifest ) {
  std::copy( std::begin( manifest ), std::end( manifest ), std::ostream_iterator<manifest_entry>( std::cout, "\n" ) );
}

void dump_manifest_as_json( const std::multiset<manifest_entry>& manifest ) {
  // build the equivalent to the old-style python _dict used in createTCKManifest:
  // id ->  TCK, { release -> type }, label
  using r2t_t  = std::map<std::string, std::string>;
  using dict_t = std::map<Gaudi::Math::MD5, std::tuple<std::string, r2t_t, std::string>>;
  dict_t dict;
  std::for_each( std::begin( manifest ), std::end( manifest ), [&dict]( const manifest_entry& e ) {
    auto i = dict.find( e.id );
    if ( i != std::end( dict ) ) {
      std::get<1>( i->second ).emplace( e.release, e.type );
    } else {
      dict.emplace( e.id, std::make_tuple( e.tck, r2t_t{{e.release, e.type}}, e.comment ) );
    }
  } );
  std::cout << to_json( dict ) << std::endl;
}

template <typename DB>
void dump_keys( DB& db ) {
  for ( auto record : db ) {
    std::cout << std::setw( 5 ) << record.uid() << "   " << std::setw( 6 ) << record.valuePersistentSize() << "   "
              << std::setw( 34 ) << format_time( record.time() ) << "   " << record.key() << "\n";
  }
  std::cout << std::flush;
}

template <typename DB>
void dump_records( DB& db ) {
  for ( auto record : db ) {
    std::cout << std::setw( 5 ) << record.uid() << "   " << std::setw( 6 ) << record.valuePersistentSize() << "   "
              << std::setw( 34 ) << format_time( record.time() ) << "   " << record.key() << "\n";
    std::cout << record.value() << "\n";
  }
  std::cout << std::flush;
}

void extract_records( CDB& db ) {
  struct timespec t[2];
  t[0].tv_nsec = 0;
  t[1].tv_nsec = 0;
  for ( auto record : db ) {
    auto key = record.key();
    if ( key.compare( 0, 3, "TN/" ) == 0 )
      key = std::string( "ConfigTreeNodes/" ) + key.substr( 3, 2 ) + "/" + key.substr( 3 );
    else if ( key.compare( 0, 3, "PC/" ) == 0 )
      key = std::string( "PropertyConfigs/" ) + key.substr( 3, 2 ) + "/" + key.substr( 3 );
    else if ( key.compare( 0, 3, "AL/" ) == 0 )
      key = std::string( "Aliases/" ) + key.substr( 3 );
    std::cout << " extracting    " << record.key() << " -> " << key << std::endl;
    fs::path fname( key );
    fs::create_directories( fname.parent_path() );
    fs::ofstream fn( fname );
    fn << record.value();
    fn.close();
    t[0].tv_sec = record.time();
    t[1].tv_sec = record.time();
    utimensat( AT_FDCWD, fname.c_str(), t, 0 );
  }
}

void extract_records( TAR& db ) {
  struct timespec t[2];
  t[0].tv_nsec = 0;
  t[1].tv_nsec = 0;
  for ( auto record : db ) {
    auto key = record.key();
    std::cout << " extracting    " << record.key() << " -> " << key << std::endl;
    fs::path fname( key );
    fs::create_directories( fname.parent_path() );
    fs::ofstream fn( fname );
    fn << record.value();
    fn.close();
    t[0].tv_sec = record.time();
    t[1].tv_sec = record.time();
    utimensat( AT_FDCWD, fname.c_str(), t, 0 );
  }
}

void convert_records_to_git( CDB& in, const std::string& oname ) {

  git_libgit2_init();

  git_repository*             repo = nullptr;
  git_repository_init_options opts;
  git_repository_init_options_init( &opts, GIT_REPOSITORY_INIT_OPTIONS_VERSION );
  opts.flags = GIT_REPOSITORY_INIT_MKPATH;
  opts.flags |= GIT_REPOSITORY_INIT_BARE;
  std::cout << " creating git repo " << oname << '\n';
  check( git_repository_init_ext( &repo, oname.c_str(), &opts ) ); // create bare repo

  // create initial commit

  git_signature* sig{nullptr};
  check( git_signature_default( &sig, repo ) );

  git_oid tree_id{};
  {
    git_index* index{nullptr};
    check( git_repository_index( &index, repo ) );
    check( git_index_write_tree( &tree_id, index ) );
    git_index_free( index );
  }

  git_oid commit_id{};
  {
    git_tree* tree{nullptr};
    git_tree_lookup( &tree, repo, &tree_id );
    git_commit_create_v( &commit_id, repo, "HEAD", sig, sig, nullptr, "Initial commit", tree, 0 );
    git_tree_free( tree );
  }
  git_commit* commit = nullptr;
  git_commit_lookup( &commit, repo, &commit_id );

  {
    git_reference* branch = nullptr;
    check( git_branch_create( &branch, repo, "MyDefaultBranch", commit, 0 ) );
    check( git_reference_symbolic_create( &branch, repo, "HEAD", git_reference_name( branch ), 1, "creating branch" ) );
    std::cout << "created initial branch " << git_reference_name( branch ) << '\n';
    git_reference_free( branch );
  }

  int i = 0;
  for ( const auto& entry : create_manifest( in ) ) {

    auto    branch_name = entry.release + "-" + entry.type;
    git_oid branch_oid  = make_branch_and_set_head( repo, branch_name, commit );
    std::cout << i++ << " preparing MD5:" << entry.id << " -> " << branch_name << " " << entry.tck << " "
              << entry.comment << '\n';
    auto itn = in.treeNode( entry.id );
    if ( !itn ) { std::cerr << " could not find " << entry.id << '\n'; }

    git_oid tree_oid   = make_tree( repo, in, *itn );
    git_oid commit2_id = commit_tree( repo, tree_oid, branch_oid, sig,
                                      "adding " + branch_name + "/" + entry.tck + " (" + entry.id.str() + ")" );
    std::cout << "commit: " << commit2_id << '\n';
    if ( entry.tck != "<NONE>" ) tag_head( repo, entry, sig );
  }

  git_signature_free( sig );
  git_repository_free( repo );
  git_libgit2_shutdown();
}

void convert_records_to_cdb( TAR& in, const std::string& oname, bool repack = false ) {
  int             ofd   = open( oname.c_str(), O_RDWR | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH );
  bool            error = ofd < 0;
  struct cdb_make ocdb;
  cdb_make_start( &ocdb, ofd );

  for ( auto record : in ) {
    auto key = record.key();
    if ( key.compare( 0, 16, "ConfigTreeNodes/" ) == 0 ) key.replace( 0, 18, "TN" );
    if ( key.compare( 0, 16, "PropertyConfigs/" ) == 0 ) key.replace( 0, 18, "PC" );
    if ( key.compare( 0, 8, "Aliases/" ) == 0 ) key.replace( 0, 7, "AL" );
    auto v = record.value();
    if ( !repack ) {
      if ( key.compare( 0, 2, "TN" ) == 0 ) {
        v = convert<ConfigTreeNode>( v );
      } else if ( key.compare( 0, 2, "PC" ) == 0 ) {
        v = convert<PropertyConfig>( v );
      }
    }

    auto val = make_cdb_record( v, record.uid(), record.time() );
    if ( cdb_make_add( &ocdb, reinterpret_cast<const unsigned char*>( key.data() ), key.size(), val.data(),
                       val.size() ) != 0 ) {
      // handle error...
      std::cerr << " failure to put key " << key << " : " << errno << " = " << strerror( errno ) << std::endl;
      error = true;
    }
  }
  cdb_make_finish( &ocdb );
  close( ofd );
  if ( error ) fs::remove( oname );
}

void create_cdb( const std::string& base_path, const std::string& oname ) {
  int  ofd   = open( oname.c_str(), O_RDWR | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH );
  bool error = ofd < 0;
  if ( error ) {
    std::cerr << " failure to open file : " << errno << " = " << strerror( errno ) << std::endl;
    return;
  }
  struct cdb_make ocdb;
  cdb_make_start( &ocdb, ofd );

  fs::path base( base_path );
  for ( auto& entry : boost::make_iterator_range( fs::recursive_directory_iterator( base ), {} ) ) {
    if ( fs::is_regular_file( entry.status() ) ) {
      std::string p = entry.path().string();
      std::string original_key{p.substr( base_path.size() + 1, p.size() )};
      std::string key{original_key};
      if ( key.compare( 0, 16, "ConfigTreeNodes/" ) == 0 )
        key.replace( 0, 18, "TN" );
      else if ( key.compare( 0, 16, "PropertyConfigs/" ) == 0 )
        key.replace( 0, 18, "PC" );
      else if ( key.compare( 0, 8, "Aliases/" ) == 0 )
        key.replace( 0, 7, "AL" );
      else {
        std::cerr << " unrecognized key type " << key << std::endl;
        error = true;
        continue;
      }
      std::cout << " adding    " << original_key << " -> " << key << std::endl;

      // Read the entire file
      fs::ifstream      file( entry.path() );
      std::stringstream ss;
      ss << file.rdbuf(); // read the file
      file.close();
      std::string value = ss.str();

      auto val = make_cdb_record( value, 0, std::time( nullptr ) );
      if ( cdb_make_add( &ocdb, reinterpret_cast<const unsigned char*>( key.data() ), key.size(), val.data(),
                         val.size() ) != 0 ) {
        std::cerr << " failure to put key " << key << " : " << errno << " = " << strerror( errno ) << std::endl;
        error = true;
      }
    }
  }

  cdb_make_finish( &ocdb );
  close( ofd );
  if ( error ) fs::remove( oname );
}

std::multiset<manifest_entry> create_manifest( git_repository* ) { return {}; }
void                          dump_keys( git_repository* ) {}
void                          dump_records( git_repository* ) {}
void                          extract_records( git_repository* ) {}
void                          extract_digests( TAR&, const std::string& ) {}
void                          extract_digests( CDB& cdb, const std::string& tck ) {
  auto alias = cdb.find( "AL/TCK/" + tck );
  if ( alias == cdb.end() ) {
    std::cerr << "oops -- cannot locate " << tck << '\n';
    return;
  }
  auto tn = cdb.treeNode( Gaudi::Math::MD5::createFromStringRep( ( *alias ).value() ) );
  if ( !tn ) {
    std::cerr << "oops -- cannot resolve treenode " << ( *alias ).value() << '\n';
    return;
  }
  std::cout << tree_to_md5( cdb, *tn );
}

void extract_digests( git_repository* repo, const std::string& tck ) {
  git_object* obj = nullptr;
  check( git_revparse_single( &obj, repo, ( tck + "^{tree}" ).c_str() ) );
  std::cout << "resolved " << tck << " to tree @ " << *git_object_id( obj ) << '\n';
  git_tree* tree = nullptr;
  check( git_tree_lookup( &tree, repo, git_object_id( obj ) ) );
  git_object_free( obj );
  std::cout << tree_to_md5( repo, tree );
  git_tree_free( tree );
}

namespace po = boost::program_options;

template <typename DB>
void dispatch( const po::variables_map& vm, DB& db ) {
  if ( vm.count( "list-manifest" ) ) dump_manifest( create_manifest( db ) );
  if ( vm.count( "list-manifest-as-json" ) ) dump_manifest_as_json( create_manifest( db ) );
  if ( vm.count( "list-keys" ) ) dump_keys( db );
  if ( vm.count( "list-records" ) ) dump_records( db );
  if ( vm.count( "extract-records" ) ) extract_records( db );
  if ( vm.count( "extract-digests" ) ) extract_digests( db, vm["tck"].as<std::string>() );
}

int main( int argc, char* argv[] ) {
  po::options_description desc( "Allowed options" );
  desc.add_options()( "list-manifest", "dump manifest " )( "list-manifest-as-json", "dump manifest in json format " )(
      "list-keys", "list keys" )( "list-records", "list keys and records" )( "extract-records", "extract records" )(
      "create-cdb", "create cdb from records" )( "convert-to-cdb", "convert to cdb" )(
      "convert-to-git", "convert to git" )( "extract-digests", "extract digests" )(
      "repack-only", "do not reformat records" )( "tck", po::value<std::string>()->default_value( "" ), "tck" )(
      "file", po::value<std::string>()->default_value( "config.cdb" ), "file" );
  po::positional_options_description p;
  p.add( "file", -1 );

  po::variables_map vm;
  po::store( po::command_line_parser( argc, argv ).options( desc ).positional( p ).run(), vm );
  po::notify( vm );

  std::string fname = vm["file"].as<std::string>();
  std::cerr << "opening " << fname << std::endl;

  if ( vm.count( "create-cdb" ) ) {
    create_cdb( ".", fname );
  } else if ( boost::algorithm::ends_with( fname, ".cdb" ) ) {
    CDB db( fname );
    if ( !db.ok() ) return 1;
    dispatch( vm, db );
    if ( vm.count( "convert-to-git" ) ) { convert_records_to_git( db, fname.substr( 0, fname.size() - 4 ) ); }
  } else if ( boost::algorithm::ends_with( fname, ".tar" ) ) {
    TAR db( fname );
    if ( !db.ok() ) return 1;
    dispatch( vm, db );
    if ( vm.count( "convert-to-cdb" ) )
      convert_records_to_cdb( db, fname.substr( 0, fname.size() - 3 ) + "cdb", vm.count( "repack-only" ) > 0 );
  } else if ( boost::algorithm::ends_with( fname, ".git" ) ) {
    git_libgit2_init();
    git_repository* repo = nullptr;
    std::cout << "opening \'" << fname.c_str() << "\'\n";
    check( git_repository_open_bare( &repo, fname.c_str() ) );
    dispatch( vm, repo );
    git_libgit2_shutdown();
  }
  return 0;
}
