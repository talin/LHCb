###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import (
    ConfigurableDummy,
    ExecutionReportsWriter,
    ExecutionReportsMonitor,
    HltDecReportsMonitor,
)
from PyConf.application import ApplicationOptions, configure, configure_input
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.filecontent_metadata import key_registry
from Configurables import GitANNSvc
from Gaudi.Configuration import DEBUG
import json

prescale2 = ConfigurableDummy(name="Prescale2", CFD=2)
prescale3 = ConfigurableDummy(name="Prescale3", CFD=3)

a1 = ConfigurableDummy(name="A1", outKeys=['/Event/a1'], CFD=1)
a2 = ConfigurableDummy(
    name="A2", inpKeys=['/Event/a1'], outKeys=['/Event/a2'], CFD=1)
a3 = ConfigurableDummy(
    name="A3", inpKeys=['/Event/a1'], outKeys=['/Event/a3'], CFD=1)
a4 = ConfigurableDummy(
    name="A4", inpKeys=['/Event/a3'], outKeys=['/Event/a4'], CFD=0)
a5 = ConfigurableDummy(
    name="A5", inpKeys=['/Event/a1'], outKeys=['/Event/a5'], CFD=0)

lines = [
    CompositeNode("line1", [prescale2, a1, a2], force_order=True),
    CompositeNode("line2", [prescale3, a3], force_order=True),
    CompositeNode("line3", [prescale3, a4], force_order=True),
    CompositeNode("line4", [a1, a4, a5], force_order=True),
]

key_registry.add(
    'deadbeef',
    json.dumps({
        "Hlt1SelectionID":
        dict((i, n.name + 'Decision') for i, n in enumerate(lines, 1))
    }))

decision = CompositeNode(
    "decision", lines, combine_logic=NodeLogic.NONLAZY_OR, force_order=False)

reports_writer = ExecutionReportsWriter(
    OutputLevel=DEBUG,
    Persist=[line.name for line in lines],
    TCK=0xdeadbeef,
    ANNSvcKey="Hlt1SelectionID")
dec_reports_monitor = HltDecReportsMonitor(
    Input=reports_writer.DecReportsLocation)
exec_reports_monitor = ExecutionReportsMonitor()

moore = CompositeNode(
    "moore",
    [decision, reports_writer, dec_reports_monitor, exec_reports_monitor],
    combine_logic=NodeLogic.NONLAZY_AND,
    force_order=True)

# Define the application environment and run it
options = ApplicationOptions(_enabled=False)
options.n_threads = 4
options.n_event_slots = 4
options.evt_max = 50
options.monitoring_file = "monitoring.json"
options.histo_file = "histograms.root"
options.input_type = "NONE"
options.dddb_tag = "dummy"
options.conddb_tag = "dummy"
options.write_decoding_keys_to_git = False
options.simulation = False

config2 = configure_input(options)
config = configure(options, moore)
