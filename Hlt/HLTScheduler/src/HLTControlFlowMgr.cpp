/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// @author Niklas Nolte
// much of that code is stolen from Sebastien Ponce's HLTEventLoopMgr
#include "CFNodePropertiesParse.h"
#include "ControlFlowNode.h"
#include "Gaudi/Chrono/ChronoIO.h" // includes "Gaudi/Accumulators.h"
#include "Gaudi/Interfaces/IQueueingEventProcessor.h"
#include "GaudiAlg/FunctionalDetails.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/ConcurrencyFlags.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/DataSvc.h"
#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/IAlgExecStateSvc.h"
#include "GaudiKernel/IAlgorithm.h"
#include "GaudiKernel/IDataBroker.h"
#include "GaudiKernel/IDataSelector.h"
#include "GaudiKernel/IEvtSelector.h"
#include "GaudiKernel/IHiveWhiteBoard.h"
#include "GaudiKernel/Memory.h"
#include "GaudiKernel/SerializeSTL.h"
#include "GaudiKernel/ThreadLocalContext.h"
#include "Kernel/Chrono.h"
#include "Kernel/EventContextExt.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/IAllocationTracker.h"
#include "Kernel/ISchedulerConfiguration.h"
#include "ThreadPool.h"
#include "tbb/task_arena.h"
#include "tbb/task_scheduler_observer.h"
#include <algorithm>
#include <chrono>
#include <condition_variable>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <optional>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include "boost/format.hpp"

#ifdef NDEBUG
#  pragma GCC diagnostic push
#  pragma GCC diagnostic ignored "-Wunused-parameter"
#  define GSL_UNENFORCED_ON_CONTRACT_VIOLATION
#endif
#include <gsl/pointers>
#ifdef NDEBUG
#  pragma GCC diagnostic pop
#endif

class HLTControlFlowMgr final
    : public extends<Service, Gaudi::Interfaces::IQueueingEventProcessor, LHCb::Interfaces::ISchedulerConfiguration> {

public:
  /// Standard Constructor
  using extends::extends;

  StatusCode initialize() override;
  StatusCode start() override;
  StatusCode reinitialize() override { return StatusCode::FAILURE; }
  StatusCode finalize() override;

  StatusCode nextEvent( int maxevt ) override;
  StatusCode executeEvent( EventContext&& evtContext ) override;
  StatusCode executeRun( int maxevt ) override { return nextEvent( maxevt ); }
  StatusCode stopRun() override;

  // Implementation of IQueueingEventProcessor
  EventContext createEventContext() override;

  void push( EventContext&& evtContext ) override;

  bool empty() const override;

  std::optional<ResultType> pop() override;

private:
  /// Declare the root address of the event
  std::optional<IOpaqueAddress*> declareEventRootAddress();

  /// Method to check if an event failed and take appropriate actions
  StatusCode eventFailed( EventContext const& eventContext ) const;

  /// Algorithm promotion
  void promoteToExecuted( EventContext&& eventContext ) const;

  void buildLines();
  // configuring the execution order
  void configureScheduling();
  // build per-thread state-vector
  void buildNodeStates();

  // helper to release context
  inline StatusCode releaseEvtSelContext() {
    StatusCode sc = StatusCode::SUCCESS;
    auto       c  = m_evtSelContext.release();
    if ( c ) {
      if ( m_evtSelector ) {
        sc = m_evtSelector->releaseContext( c );
      } else {
        delete c;
      }
    }
    return sc;
  }

  // helper to create context
  inline StatusCode createEvtSelContext() {
    auto sc = releaseEvtSelContext();
    if ( sc ) {
      decltype( m_evtSelContext )::pointer c = nullptr;
      sc = ( m_evtSelector ? m_evtSelector->createContext( c ) : StatusCode::FAILURE );
      m_evtSelContext.reset( c );
    }
    return sc;
  }

  // functions to create m_printableDependencyTree
  void registerStructuredTree();
  void registerTreePrintWidth();

private:
  int m_nextevt = 0;

  mutable std::atomic<uint16_t> m_failed_evts_detected = 0;
  mutable bool                  m_shutdown_now         = false;

  std::unique_ptr<ThreadPool> m_debug_pool = nullptr;

private:
  Gaudi::Property<std::string> m_histPersName{this, "HistogramPersistency", "", ""};
  Gaudi::Property<std::string> m_evtsel{this, "EvtSel", "", ""};
  Gaudi::Property<int> m_threadPoolSize{this, "ThreadPoolSize", -1, "Size of the threadpool initialised by TBB"};
  Gaudi::Property<int> m_printFreq{this, "PrintFreq", 1, "Print Frequency for the full algorithm tree"};
  Gaudi::Property<std::vector<NodeDefinition>> m_compositeCFProperties{
      this, "CompositeCFNodes", {}, "Specification of composite CF nodes"};
  Gaudi::Property<std::vector<std::string>> m_BarrierAlgNames{
      this, "BarrierAlgNames", {}, "Names of Barrier (Gather) Algorithms"};
  Gaudi::Property<std::vector<std::vector<std::string>>> m_userDefinedEdges{
      this, "AdditionalCFEdges", {}, "Additional Control Flow Edges defined by the User \
                                      (format: [ [before, after], [before2, after2] ])"};
  Gaudi::Property<std::set<std::string>> m_definitelyRunThese{
      this, "PreambleAlgs", {}, "Add algs that do not participate in the control flow but should\
                                   definitely run, like e.g. a callgrindprofile"};

  Gaudi::Property<int> m_startTimeAtEvt{this, "StartTimeAtEvt", -1, "start timing at this event. Counting from 0. \
                                        Default choice is deduced from #slots and #evts \
                                        to be reasonably far away from the beginning of processing"};
  Gaudi::Property<int> m_stopTimeAtEvt{this, "StopTimeAtEvt", -1, "stop timing at this event. Counting from 0. \
                                          Default choice is deduced from #slots and #evts \
                                          to be reasonably far away from the end of processing"};

  Gaudi::Property<int> m_stopAfterNFailures{this, "StopAfterNFailures", 3,
                                            "Stop processing if this number of consecutive event failures happened"};

  Gaudi::Property<std::size_t> m_minNameColWidth{this, "MinNameColumnWidth", 46u,
                                                 "Minimum width of component name comlumn in timing table"};
  Gaudi::Property<std::size_t> m_maxNameColWidth{this, "MaxNameColumnWidth", 100u,
                                                 "Maximum width of component name comlumn in timing table"};
  Gaudi::Property<std::size_t> m_estMemoryPoolSize{
      this, "MemoryPoolSize", 10 * 1024 * 1024,
      "Estimated size of each event-local memory pool, in bytes. Set to zero to disable event-local memory pools."};
  Gaudi::Property<std::string> m_endIncident{this, "EndEventIncident", "",
                                             "Incident to fire at the end of each event (for XMLSummarySvc)"};

  // this property is mainly needed to support execution of old algorithms that need to be called via SysExecute like
  // the ones that inherit from DVCommonBase
  Gaudi::Property<bool> m_EnableLegacyMode{
      this, "EnableLegacyMode", false,
      "Call SysExecute of an algorithm. If false algorithms will be called via execute which is faster."};

  /// Reference to the Event Data Service's IDataManagerSvc interface
  IDataManagerSvc* m_evtDataMgrSvc = nullptr;
  /// Reference to the Event Selector
  IEvtSelector* m_evtSelector = nullptr;
  /// Reference to the Histogram Data Service
  IDataManagerSvc* m_histoDataMgrSvc = nullptr;
  /// Reference to the Histogram Persistency Service
  IConversionSvc* m_histoPersSvc = nullptr;
  /// Reference to the Whiteboard
  IHiveWhiteBoard* m_whiteboard = nullptr;
  /// Reference to the AlgExecStateSvc
  IAlgExecStateSvc* m_algExecStateSvc = nullptr;
  // the used databroker
  IDataBroker* m_databroker = nullptr;

  ServiceHandle<IAllocationTracker> m_alloc_tracker{this, "AllocationTrackerService", "TimingAllocationTracker"};
  ServiceHandle<IIncidentSvc>       m_incidentSvc{this, "IncidentSvc", "IncidentSvc"};

  // the tbb "threadpool"
  std::unique_ptr<tbb::task_arena> m_taskArena;

  /// atomic count of the number of finished events
  mutable std::atomic<uint32_t> m_finishedEvt{0};
  /// condition variable to wake up main thread when we need to create a new event
  mutable std::condition_variable m_createEventCond;
  /// mutex assoiciated with m_createEventCond condition variable
  std::mutex m_createEventMutex;

  /// event selector context
  std::unique_ptr<IEvtSelector::Context> m_evtSelContext;

  // state vectors for each event, once filled, then copied per event
  std::vector<NodeState>                                                                m_NodeStates;
  std::vector<AlgState>                                                                 m_AlgStates;
  std::deque<Gaudi::Accumulators::AveragingCounter<LHCb::chrono::fast_clock::duration>> m_TimingCounters;
  std::deque<Gaudi::Accumulators::BinomialCounter<uint32_t>>                            m_NodeStateCounters;

  // statistics for the per-event memory pool
  // in principle these members could be removed if
  // LHCb::Allocators::Utils::provides_stats_v<LHCb::Allocators::MonotonicBufferResource>
  // is not true (it's constexpr), but that doesn't seem worth the effort
  mutable Gaudi::Accumulators::BinomialCounter<uint32_t, Gaudi::Accumulators::atomicity::full>
                                                                                           m_memoryPoolMultipleAllocations;
  mutable Gaudi::Accumulators::StatCounter<uint64_t, Gaudi::Accumulators::atomicity::full> m_memoryPoolSize,
      m_memoryPoolBlocks, m_memoryPoolCapacity, m_memoryPoolAllocations;

private:
  // all controlflownodes
  std::map<std::string, VNode> m_allVNodes;
  // all nodes to execute in ordered manner
  std::vector<gsl::not_null<BasicNode*>> m_orderedNodesVec;
  // highest node
  VNode* m_motherOfAllNodes = nullptr;

  std::vector<AlgWrapper> m_definitelyRunTheseAlgs;

  // for printing
  // printable dependency tree (will be built during initialize
  std::vector<std::string> m_printableDependencyTree;
  std::vector<std::string> m_AlgNames;
  // map order of print to order of m_NodeStates and m_allVNodes
  std::vector<int> m_mapPrintToNodeStateOrder;
  // maximum width of the dependency tree
  int m_maxTreeWidth{};

  template <typename Resource>
  void fillMemoryPoolStats( Resource* memResource ) const;

  // runtime adding of states to print tree and states
  template <typename Container>
  std::stringstream buildPrintableStateTree( Container const& states ) const;

public:
  std::stringstream
  buildPrintableStateTree( LHCb::Interfaces::ISchedulerConfiguration::State const& state ) const override {
    return buildPrintableStateTree( state.nodes() );
  }
  std::stringstream buildAlgsWithStates( LHCb::Interfaces::ISchedulerConfiguration::State const& ) const override;

  // to be able to check which states belong to which node (from the outside)
  std::map<std::string, int> getNodeNamesWithIndices() const override {
    std::map<std::string, int> names_indices;
    std::transform( m_allVNodes.begin(), m_allVNodes.end(), std::inserter( names_indices, names_indices.end() ),
                    []( const auto& p ) {
                      return std::visit( []( const auto& n ) { return std::pair{n.m_name, n.m_NodeID}; }, p.second );
                    } );
    return names_indices;
  }

private:
  mutable std::mutex             m_doneMutex;
  mutable std::queue<ResultType> m_done;
};

// Instantiation of a static factory class used by clients to create instances of this service
DECLARE_COMPONENT( HLTControlFlowMgr )

// operator<< overloads for variant nodes
std::ostream& operator<<( std::ostream& os, VNode const& node ) { return os << getNameOfVNode( node ); }

// be able to print vectors of ordered nodes correctly
template <typename T>
std::ostream& operator<<( std::ostream& os, gsl::not_null<T*> const& ptr ) {
  return os << *ptr;
}

template <nodeType Type>
std::ostream& operator<<( std::ostream& os, CompositeNode<Type> const& node ) {
  return os << node.m_name;
}

template <typename Key, typename Compare, typename Allocator>
std::ostream& operator<<( std::ostream& os, std::set<Key, Compare, Allocator> const& x ) {
  return GaudiUtils::details::ostream_joiner( os << "{", x, ", " ) << "}";
}

using GaudiUtils::operator<<;

namespace {
  constexpr bool use_debuggable_threadpool = false;

  template <typename F>
  struct EventTask {
    mutable EventContext evtctx;
    F                    f;
    void                 operator()() const { return f( evtctx ); }
  };
  template <typename F>
  EventTask( EventContext&&, F && )->EventTask<F>;

  // observe threads to be able to join in the end
  struct task_observer final : public tbb::task_scheduler_observer {
    task_observer( tbb::task_arena& arena ) : tbb::task_scheduler_observer{arena} { observe( true ); }
    std::atomic<int> m_thread_count{0};
    void             on_scheduler_entry( bool ) override { m_thread_count++; }
    void             on_scheduler_exit( bool ) override { m_thread_count--; }
  };
} // namespace

StatusCode HLTControlFlowMgr::initialize() {
  using Clock = std::chrono::high_resolution_clock;

  info() << "Start initialization" << endmsg;

  auto start_time = Clock::now();

  StatusCode sc = Service::initialize();
  if ( !sc.isSuccess() ) {
    error() << "Failed to initialize Service Base class." << endmsg;
    return StatusCode::FAILURE;
  }

  // Setup access to event data services
  m_evtDataMgrSvc = serviceLocator()->service<IDataManagerSvc>( "EventDataSvc" );
  if ( !m_evtDataMgrSvc ) {
    fatal() << "Error retrieving EventDataSvc interface IDataManagerSvc." << endmsg;
    return StatusCode::FAILURE;
  }

  m_whiteboard = serviceLocator()->service<IHiveWhiteBoard>( "EventDataSvc" );
  if ( !m_whiteboard ) {
    fatal() << "Error retrieving EventDataSvc interface IHiveWhiteBoard." << endmsg;
    return StatusCode::FAILURE;
  }
  // Obtain the IProperty of the ApplicationMgr
  IProperty* appMgrProperty = serviceLocator()->service<IProperty>( "ApplicationMgr" );
  if ( !appMgrProperty ) {
    fatal() << "IProperty interface not found in ApplicationMgr." << endmsg;
    return StatusCode::FAILURE;
  }
  // We do not expect a Event Selector necessarily being declared
  setProperty( appMgrProperty->getProperty( "EvtSel" ) ).ignore();

  if ( m_evtsel.length() == 0 ) {
    m_evtSelector = serviceLocator()->service<IEvtSelector>( "EventSelector" );
  } else if ( m_evtsel == "NONE" ) {
    m_evtSelector = nullptr;
    info() << "Will not use an EventSelector." << endmsg;
  } else {
    m_evtSelector = serviceLocator()->service<IEvtSelector>( m_evtsel );
    if ( !m_evtSelector ) {
      fatal() << "IEventSelector.\"" << m_evtsel << "\" not found." << endmsg;
      return StatusCode::FAILURE;
    }
  }

  // Setup access to histogramming services
  m_histoDataMgrSvc = serviceLocator()->service<IDataManagerSvc>( "HistogramDataSvc" );
  if ( !m_histoDataMgrSvc ) {
    fatal() << "Error retrieving HistogramDataSvc." << endmsg;
    return sc;
  }
  // Setup histogram persistency
  m_histoPersSvc = serviceLocator()->service<IConversionSvc>( "HistogramPersistencySvc" );
  if ( !m_histoPersSvc ) {
    warning() << "Histograms cannot not be saved - though required." << endmsg;
    return sc;
  }

  m_algExecStateSvc = serviceLocator()->service<IAlgExecStateSvc>( "AlgExecStateSvc" );
  if ( !m_algExecStateSvc ) {
    fatal() << "Error retrieving AlgExecStateSvc" << endmsg;
    return StatusCode::FAILURE;
  }

  m_databroker = serviceLocator()->service<IDataBroker>( "HiveDataBrokerSvc" );
  if ( !m_databroker ) {
    fatal() << "Error retrieving HiveDataBrokerSvc" << endmsg;
    return StatusCode::FAILURE;
  }
  // to finalize algorithms at the right time, which is taken care of by the
  // hivedatabroker. it is the one that should be finalized first, thus high
  // priority (10*default_svc_priority is the next highest)
  auto svcMgr = serviceLocator().as<ISvcManager>();
  svcMgr->addService( "HiveDataBrokerSvc", ISvcManager::DEFAULT_SVC_PRIORITY * 11 ).ignore();

  // Clearly inform about the level of concurrency
  info() << "Concurrency level information:" << endmsg;
  info() << " o Number of events slots: " << m_whiteboard->getNumberOfStores() << endmsg;
  info() << " o TBB thread pool size: " << m_threadPoolSize << endmsg;

  // ------------------------------- scheduling -------------------------------------------------------
  // configure the lines
  buildLines();

  // configure the order of nodes
  configureScheduling();

  // build the vector of states (to be copied into each thread)
  buildNodeStates();

  // build the m_printableDependencyTree for monitoring
  registerStructuredTree();
  registerTreePrintWidth();

  // print out the nodes + states
  if ( msgLevel( MSG::DEBUG ) ) {
    auto i = 0u;
    for ( auto const& [_, node] : m_allVNodes ) {
      debug() << std::left << "initialize " << std::setw( 25 ) << node << " with state " << m_NodeStates[i] << endmsg;
      ++i;
    }
  }

  // print out high level nodes + their children
  if ( msgLevel( MSG::DEBUG ) ) {
    for ( auto const& [_, vnode] : m_allVNodes ) {
      std::visit( overload{[&]( auto const& node ) {
                             debug() << std::left << "children of " << std::setw( 12 ) << node.getType() << " "
                                     << std::setw( 25 ) << node.m_name << ": " << node.m_children << endmsg;
                           },
                           []( BasicNode const& ) {}},
                  vnode );
    }
  }
  if ( msgLevel( MSG::DEBUG ) ) debug() << m_compositeCFProperties << endmsg;
  auto end_time = Clock::now();

  info() << "---> End of Initialization. "
         << "This took " << std::chrono::duration_cast<std::chrono::milliseconds>( end_time - start_time ).count()
         << " ms" << endmsg;

  return sc;

  // ------------------------------- scheduling end ---------------------------------------------------
}
StatusCode HLTControlFlowMgr::start() {
  StatusCode sc = Service::start();
  if ( !sc.isSuccess() ) {
    error() << "Failed to start Service Base class." << endmsg;
    return StatusCode::FAILURE;
  }
  if constexpr ( use_debuggable_threadpool ) {
    m_debug_pool = std::make_unique<ThreadPool>( m_threadPoolSize.value() );
  }

  // create th tbb thread pool
  if ( tbb::this_task_arena::current_thread_index() == tbb::task_arena::not_initialized ) {
    m_taskArena = std::make_unique<tbb::task_arena>( m_threadPoolSize.value() + 1 );
  } else {
    m_taskArena = std::make_unique<tbb::task_arena>( tbb::task_arena::attach{} );
  }

  return sc;
}
StatusCode HLTControlFlowMgr::finalize() {

  StatusCode sc = StatusCode::SUCCESS;

  if constexpr ( LHCb::Allocators::Utils::provides_stats_v<LHCb::Allocators::MonotonicBufferResource> ) {
    // Print statistics about the shared per-event memory pool if it was enabled
    if ( m_estMemoryPoolSize > 0 ) {
      constexpr auto denominator = 1024 * 1024;
      info() << "Memory pool: used " << float( m_memoryPoolSize.mean() / denominator ) << " +/- "
             << float( m_memoryPoolSize.meanErr() / denominator )
             << " MiB (min: " << float( m_memoryPoolSize.min() / denominator )
             << ", max: " << float( m_memoryPoolSize.max() / denominator ) << ") in "
             << float( m_memoryPoolBlocks.mean() ) << " +/- " << float( m_memoryPoolBlocks.meanErr() )
             << " blocks (allocated >once in " << float( 100. * m_memoryPoolMultipleAllocations.efficiency() )
             << " +/- " << float( 100. * m_memoryPoolMultipleAllocations.efficiencyErr() )
             << "% events). Allocated capacity was " << float( m_memoryPoolCapacity.mean() / denominator ) << " +/- "
             << float( m_memoryPoolCapacity.meanErr() / denominator )
             << " MiB (min: " << float( m_memoryPoolCapacity.min() / denominator )
             << ", max: " << float( m_memoryPoolCapacity.max() / denominator ) << ") and "
             << m_memoryPoolAllocations.mean() << " +/- " << m_memoryPoolAllocations.meanErr()
             << " (min: " << m_memoryPoolAllocations.min() << ", max: " << m_memoryPoolAllocations.max()
             << ") requests were served" << endmsg;
    }
  }

  // Determine the size of the algorithm name field to use
  // Use max name size, clamped to 'reasonable' range
  const auto maxNameS =
      ( !m_AlgNames.empty()
            ? std::clamp( 2u + std::max_element( m_AlgNames.begin(), m_AlgNames.end(),
                                                 []( auto const& a, auto const& b ) { return a.size() < b.size(); } )
                                   ->size(),
                          m_minNameColWidth.value(), m_maxNameColWidth.value() )
            : m_minNameColWidth.value() );
  const auto sf1 = std::to_string( maxNameS + 5 );
  const auto sf2 = std::to_string( maxNameS + 4 );
  const auto sf3 = std::to_string( maxNameS + 2 );

  auto const total_time =
      std::accumulate( begin( m_TimingCounters ), end( m_TimingCounters ), 0., []( double sum, auto const& ctr ) {
        return sum + static_cast<double>( std::chrono::duration_cast<std::chrono::microseconds>( ctr.sum() ).count() );
      } );

  auto const max_count =
      std::max_element( begin( m_TimingCounters ), end( m_TimingCounters ), []( auto const& ctr1, auto const& ctr2 ) {
        return ctr1.nEntries() < ctr2.nEntries();
      } )->nEntries();

  auto sort_indices = std::vector<std::size_t>( m_TimingCounters.size() );
  std::iota( begin( sort_indices ), end( sort_indices ), 0 );

  std::sort( begin( sort_indices ), end( sort_indices ), [this]( auto const& idx1, auto const& idx2 ) {
    return m_TimingCounters[idx1].sum() > m_TimingCounters[idx2].sum();
  } );

  // print the counters
  info() << "Timing table:" << endmsg;
  info() << boost::format{"\n | Name of Algorithm %|" + sf1 +
                          "t| | Execution Count | Total Time / s  | Avg. Time / us   |\n"};
  info() << boost::format{" | %|-" + sf3 + "." + sf3 + "48s|%|" + sf2 + "t|"} % ( "Sum of all Algorithms" );
  info() << boost::format{"| %15u | %15.3f | %16.3f |"} % max_count % ( total_time / 1e6 ) % ( total_time / max_count )
         << '\n';
  for ( auto const idx : sort_indices ) {
    auto const& ctr  = m_TimingCounters[idx];
    auto const& name = m_AlgNames[idx];
    info() << boost::format{" | %|-" + sf3 + "." + sf3 + "48s|%|" + sf2 + "t|"} % ( "\"" + name + "\"" );
    info() << boost::format{"| %15u | %15.3f | %16.3f |"} % static_cast<double>( ctr.nEntries() ) %
                  ( static_cast<double>( std::chrono::duration_cast<std::chrono::microseconds>( ctr.sum() ).count() ) /
                    1e6 ) %
                  ( static_cast<double>( std::chrono::duration_cast<std::chrono::nanoseconds>( ctr.mean() ).count() ) /
                    1e3 )
           << '\n';
  }
  info() << endmsg;

  // print the counters
  info() << "StateTree: CFNode   #executed  #passed\n"
         << buildPrintableStateTree( m_NodeStateCounters ).str() << endmsg;

  // Save Histograms Now
  if ( m_histoPersSvc ) {
    IDataSelector objects;
    sc = m_histoDataMgrSvc->traverseTree( [&objects]( IRegistry* pReg, int ) {
      DataObject* obj = pReg->object();
      if ( obj && obj->clID() != CLID_StatisticsFile ) {
        objects.push_back( obj );
        return true;
      }
      return false;
    } );
    if ( sc.isSuccess() ) {
      // skip /stat entry!
      sc = std::accumulate( begin( objects ), end( objects ), sc, [&]( StatusCode s, auto const& i ) {
        IOpaqueAddress* pAddr = nullptr;
        auto            iret  = m_histoPersSvc->createRep( i, pAddr );
        if ( iret.isSuccess() ) i->registry()->setAddress( pAddr );
        return s.isFailure() ? s : iret;
      } );
      sc = std::accumulate( begin( objects ), end( objects ), sc, [&]( StatusCode s, auto const& i ) {
        return s.andThen( [&] {
          auto reg = i->registry();
          return m_histoPersSvc->fillRepRefs( reg->address(), i );
        } );
      } );
      if ( sc.isSuccess() ) {
        info() << "Histograms converted successfully according to request." << endmsg;
      } else {
        error() << "Error while saving Histograms." << endmsg;
      }
    } else {
      error() << "Error while traversing Histogram data store" << endmsg;
    }
  }

  releaseEvtSelContext().ignore();

  auto sc2 = Service::finalize();
  return sc.isFailure() ? sc2.ignore(), sc : sc2;
}

EventContext HLTControlFlowMgr::createEventContext() {
  using namespace std::chrono_literals;

  std::unique_lock<std::mutex> lock{m_createEventMutex};
  // The wake up is necessary to avoid dead locks, see also https://gitlab.cern.ch/lhcb/LHCb/-/issues/233.
  while ( !m_createEventCond.wait_for( lock, std::chrono::milliseconds( 100 ),
                                       [&] { return m_whiteboard->freeSlots() > 0; } ) ) {}
  IOpaqueAddress* evt_root_ptr = nullptr;
  if ( m_evtSelector ) {
    auto addr = declareEventRootAddress();
    if ( addr.has_value() and not addr.value() ) { // We ran out of events!
      m_nextevt = -1;                              // Set created event to a negative value: we finished!
      return EventContext{};
    } else if ( not addr.has_value() ) {
      error() << "something went wrong getting the next event root address" << endmsg;
      return EventContext{};
    }
    evt_root_ptr = addr.value();
  }

  // setup evtcontext
  EventContext evtContext{};
  evtContext.set( m_nextevt, m_whiteboard->allocateStore( m_nextevt ) );
  // pointer to the event into the context extension
  evtContext.emplaceExtension<decltype( evt_root_ptr )>( evt_root_ptr );

  m_nextevt++;
  return evtContext;
}

StatusCode HLTControlFlowMgr::executeEvent( EventContext&& evtContext ) {
  using namespace std::chrono_literals;
  push( std::move( evtContext ) );
  std::optional<ResultType> result;
  while ( !( result = pop() ) ) std::this_thread::sleep_for( 1ms );
  return std::get<0>( std::move( *result ) );
}

void HLTControlFlowMgr::push( EventContext&& evtContext ) {
  // Now add event to the task pool
  if ( msgLevel( MSG::VERBOSE ) )
    verbose() << "Event " << evtContext.evt() << " submitting in slot " << evtContext.slot() << endmsg;

  EventTask event_task{
      std::move( evtContext ), [this]( EventContext& evtContext ) {
        StatusCode sc = m_whiteboard->selectStore( evtContext.slot() );
        if ( sc.isFailure() ) {
          fatal() << "Slot " << evtContext.slot() << " could not be selected for the WhiteBoard\n"
                  << "Impossible to create event context" << endmsg;
          throw GaudiException(
              "Slot " + std::to_string( evtContext.slot() ) + " could not be selected for the WhiteBoard", name(), sc );
        }

        // receive the data pointer
        auto evt_root_ptr = evtContext.getExtension<IOpaqueAddress*>();

        // set event root
        if ( not evt_root_ptr ) { // there is no data, we set an empty TES
          auto sc = m_evtDataMgrSvc->setRoot( "/Event", new DataObject() );
          if ( !sc.isSuccess() ) error() << "Error declaring event root DataObject" << endmsg;
        } else {
          auto sc = m_evtDataMgrSvc->setRoot( "/Event", evt_root_ptr );
          if ( !sc.isSuccess() ) error() << "Error setting event root address." << endmsg;
        }

        // Make the LHCb-specific event context extension
        auto& lhcbExt = evtContext.emplaceExtension<LHCb::EventContextExtension>();

        // create a memory pool for algorithms processing this event
        // note: the resource is owned by lhcbExt, memResource is just a non-owning handle
        auto* memResource =
            m_estMemoryPoolSize > 0
                ? lhcbExt.emplaceMemResource<LHCb::Allocators::MonotonicBufferResource>( m_estMemoryPoolSize )
                : nullptr;

        // Save a shortcut directly into the event context
        LHCb::setMemResource( evtContext, memResource );

        // Note that this is a COPY and the scheduler extension will NOT be
        // present in the copy, but the copy WILL hold a shared_ptr to the
        // memory resource.
        Gaudi::Hive::setCurrentContext( evtContext );

        SmartIF<IProperty> appmgr( serviceLocator() );

        // Copy the scheduler states into the event context so they're globally accessible within an event
        // Note that this will trigger the first allocation from the memory resource
        // Also note that these states will not be present in the thread-local EventContext.
        auto& state = lhcbExt.emplaceSchedulerExtension<LHCb::Interfaces::ISchedulerConfiguration::State>(
            std::vector{m_NodeStates.begin(), m_NodeStates.end(), LHCb::Allocators::EventLocal<NodeState>{memResource}},
            std::vector{m_AlgStates.begin(), m_AlgStates.end(), LHCb::Allocators::EventLocal<AlgState>{memResource}} );

        for ( AlgWrapper& toBeRun : m_definitelyRunTheseAlgs ) {
          try {
            const auto start = LHCb::chrono::fast_clock::now();
            toBeRun.execute( evtContext, state.algorithms() );
            m_TimingCounters[toBeRun.m_executedIndex] += LHCb::chrono::fast_clock::now() - start;
          } catch ( ... ) {
            m_algExecStateSvc->updateEventStatus( true, evtContext );
            fatal() << "ERROR: Event failed in Algorithm " << toBeRun.name() << endmsg;
            if ( !Gaudi::setAppReturnCode( appmgr, Gaudi::ReturnCode::AlgorithmFailure ) )
              warning() << "failed to set application return code to " << Gaudi::ReturnCode::AlgorithmFailure << endmsg;
            break;
          }
        }

        for ( gsl::not_null<BasicNode*> bNode : m_orderedNodesVec ) {
          if ( bNode->requested( state.nodes() ) ) {
            bNode->execute( state.nodes(), state.algorithms(), m_TimingCounters, evtContext, m_algExecStateSvc,
                            appmgr );
            bNode->notifyParents( state.nodes() );
          }
        }
        m_algExecStateSvc->updateEventStatus( false, evtContext );

        // printing
        if ( msgLevel( MSG::VERBOSE ) && m_nextevt % m_printFreq == 0 ) {
          verbose() << "StateTree: CFNode   executionCtr|passed\n" << buildPrintableStateTree( state ).str() << endmsg;
          verbose() << buildAlgsWithStates( state ).str() << endmsg;
        }

        // update node state counters
        auto nodes = state.nodes(); // zip doesn't like rvalues... needs to be told this is a 'borrow' type, so ignore
                                    // lifetime of wrapper...
        for ( auto&& [ctr, ns] : Gaudi::Functional::details::zip::range( m_NodeStateCounters, nodes ) )
          if ( ns.executionCtr == 0 ) ctr += ns.passed; // only add when actually executed

        // update scheduler state
        promoteToExecuted( std::move( evtContext ) );
      }};

  if constexpr ( use_debuggable_threadpool ) {
    m_debug_pool->enqueue( std::move( event_task ) );
  } else {
    m_taskArena->enqueue( std::move( event_task ) );
  }
}

bool HLTControlFlowMgr::empty() const {
  return
      // note: this is not synchronized
      m_done.empty() &&
      // note: this is not synchronized either
      ( m_whiteboard->freeSlots() == m_whiteboard->getNumberOfStores() );
}

std::optional<HLTControlFlowMgr::ResultType> HLTControlFlowMgr::pop() {
  std::lock_guard           _{m_doneMutex};
  std::optional<ResultType> out;
  if ( !m_done.empty() ) {
    out = std::move( m_done.front() );
    m_done.pop();
  }
  return out;
}

StatusCode HLTControlFlowMgr::stopRun() {
  // Set the application return code
  auto appmgr = serviceLocator()->as<IProperty>();
  if ( Gaudi::setAppReturnCode( appmgr, Gaudi::ReturnCode::ScheduledStop ).isFailure() ) {
    error() << "Could not set return code of the application (" << Gaudi::ReturnCode::ScheduledStop << ")" << endmsg;
  }
  return StatusCode::SUCCESS;
}

StatusCode HLTControlFlowMgr::nextEvent( int maxevt ) {
  // Calculate runtime
  using Clock = std::chrono::high_resolution_clock;

  // Reset the application return code.
  auto appmgr = serviceLocator()->as<IProperty>();
  Gaudi::setAppReturnCode( appmgr, Gaudi::ReturnCode::Success, true ).ignore();

  // create m_evtSelContext used internally in executeEvent and more
  // precisely in declareEventRootAddress. Cannot be passed through the interface
  // without breaking other schedulers
  if ( m_evtSelector ) {
    auto sc = createEvtSelContext();
    if ( !sc.isSuccess() ) {
      fatal() << "Can not create the event selector Context." << endmsg;
      return sc;
    }
  }

  task_observer taskObsv{*m_taskArena};

  auto shutdown_threadpool = [&]() {
    if constexpr ( !use_debuggable_threadpool ) {
      m_taskArena->terminate();
      while ( taskObsv.m_thread_count > 0 ) // this is our "threads.join()" alternative
        std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
    }
  };

  // start with event 0
  m_nextevt = 0;

  // adjust timing start counter
  if ( m_startTimeAtEvt < 0 || m_startTimeAtEvt > m_stopTimeAtEvt ) {
    if ( maxevt > 0 ) {
      if ( m_startTimeAtEvt < -1 || m_startTimeAtEvt > m_stopTimeAtEvt ) {
        warning() << "StartTimeAtEvt set faulty, changing to .1*maxevt" << endmsg;
      }
      m_startTimeAtEvt = static_cast<int>( .1 * maxevt );
    } else {
      if ( m_startTimeAtEvt < -1 || m_startTimeAtEvt > m_stopTimeAtEvt ) {
        warning() << "StartTimeAtEvt set faulty, changing to 10*evtslots" << endmsg;
      }
      m_startTimeAtEvt = 10 * m_whiteboard->getNumberOfStores();
    }
  }

  // adjust timing stop counter
  if ( maxevt > 0 ) {
    if ( m_stopTimeAtEvt < 0 ) { // default
      if ( m_stopTimeAtEvt < -1 ) { warning() << "StopTimeAtEvt set faulty, changing to .9*maxevt" << endmsg; }
      m_stopTimeAtEvt = static_cast<int>( .9 * maxevt );
    } else if ( m_stopTimeAtEvt > maxevt - 1 ) {
      warning() << "StopTimeAtEvt too big, changing to last event (maxevt - 1)" << endmsg;
      m_stopTimeAtEvt = maxevt - 1;
    }
  } else {
    if ( m_stopTimeAtEvt < 0 ) { // default
      if ( m_stopTimeAtEvt < -1 ) { warning() << "StopTimeAtEvt set faulty, changing to last element" << endmsg; }
      m_stopTimeAtEvt = std::numeric_limits<int>::max();
    }
  }

  info() << "Will measure time between events " << m_startTimeAtEvt.value() << " and " << m_stopTimeAtEvt.value()
         << " (stop might be some events later)" << endmsg;

  info() << "Starting loop on events" << endmsg;
  std::optional<std::pair<decltype( Clock::now() ), uint64_t>> startTimeAndTicks, endTimeAndTicks;

  using namespace std::chrono_literals;

  // setup the allocation tracking machinery
  m_alloc_tracker->reserveSlots( m_whiteboard->getNumberOfStores() );
  // helper to get the total number of RDTSC ticks that have been used in workers
  auto const getTotalWorkerTicks = [this]() {
    return std::accumulate( m_TimingCounters.begin(), m_TimingCounters.end(), uint64_t{},
                            []( uint64_t total, auto const& counter ) { return total + counter.sum().count(); } );
  };

  while ( maxevt < 0 || m_nextevt < maxevt ) {
    if ( m_shutdown_now ) {
      shutdown_threadpool();
      while ( not empty() ) pop();
      return StatusCode::FAILURE;
    }
    if ( m_startTimeAtEvt == m_nextevt ) {
      auto currtime = std::time( nullptr );
      info() << "Timing started at: " << std::put_time( std::localtime( &currtime ), "%T" ) << endmsg;
      m_alloc_tracker->beginTracking();
      startTimeAndTicks.emplace( Clock::now(), getTotalWorkerTicks() );
    }
    if ( m_stopTimeAtEvt == m_nextevt ) {
      endTimeAndTicks.emplace( Clock::now(), getTotalWorkerTicks() );
      m_alloc_tracker->endTracking();
      auto currtime = std::time( nullptr );
      info() << "Timing stopped at: " << std::put_time( std::localtime( &currtime ), "%T" ) << endmsg;
    }
    auto evtContext = createEventContext();
    if ( !evtContext.valid() ) {
      if ( m_nextevt == -1 ) break; // finished
      shutdown_threadpool();
      return StatusCode::FAILURE; // else we have an success --> exit loop
    }
    push( std::move( evtContext ) );
    pop();
  } // end main loop on finished events

  if ( !endTimeAndTicks ) {
    endTimeAndTicks.emplace( Clock::now(), getTotalWorkerTicks() );
    m_stopTimeAtEvt = m_finishedEvt;
    m_alloc_tracker->endTracking();
  }

  shutdown_threadpool();
  while ( not empty() ) pop();

  releaseEvtSelContext().ignore();

  if ( !startTimeAndTicks ) {
    info() << "---> Loop over " << m_finishedEvt << " Events Finished - "
           << " WSS " << System::mappedMemory( System::MemoryUnit::kByte ) * 1. / 1024. << ", timing failed.."
           << endmsg;
  } else {
    const auto totalTime =
        std::chrono::duration_cast<std::chrono::milliseconds>( endTimeAndTicks->first - startTimeAndTicks->first )
            .count();
    const auto timeDiff   = m_stopTimeAtEvt.value() - m_startTimeAtEvt.value();
    const auto evtsPerSec = ( totalTime > 0 ? timeDiff * 1. / totalTime * 1e3 : 0 );
    info() << "---> Loop over " << m_finishedEvt << " Events Finished - "
           << " WSS " << System::mappedMemory( System::MemoryUnit::kByte ) * 1. / 1024. << ", timed " << timeDiff
           << " Events: " << totalTime << " ms"
           << ", Evts/s = " << evtsPerSec << endmsg;
    m_alloc_tracker->incrementEventCount( timeDiff, endTimeAndTicks->second - startTimeAndTicks->second );
  }
  return StatusCode::SUCCESS;
}

std::optional<IOpaqueAddress*> HLTControlFlowMgr::declareEventRootAddress() {

  IOpaqueAddress* pAddr = nullptr;
  if ( m_evtSelContext ) {
    auto sc = m_evtSelector->next( *m_evtSelContext );
    // TODO make a difference between finished and read error in the eventselector
    if ( sc.isSuccess() ) {
      // Create root address and assign address to data service
      sc = m_evtSelector->createAddress( *m_evtSelContext, pAddr );
      if ( !sc.isSuccess() ) {
        error() << "Error creating IOpaqueAddress." << endmsg;
        return {};
      }
    }
    if ( !sc.isSuccess() ) {
      info() << "No more events in event selection " << endmsg;
      return nullptr;
    }
  }
  return pAddr;
}

/**
 * It can be possible that an event fails. In this case this method is called.
 * It dumps the state of the scheduler, drains the actions (without executing
 * them) and events in the queues and returns a failure.
 */
StatusCode HLTControlFlowMgr::eventFailed( EventContext const& eventContext ) const {
  fatal() << "*** Event " << eventContext.evt() << " on slot " << eventContext.slot() << " failed! ***" << endmsg;
  std::ostringstream ost;
  m_algExecStateSvc->dump( ost, eventContext );
  info() << "Dumping Alg Exec State for slot " << eventContext.slot() << ":\n" << ost.str() << endmsg;
  if ( ++m_failed_evts_detected > m_stopAfterNFailures ) { m_shutdown_now = true; }
  return StatusCode::FAILURE;
}

/** Farming this out to a separate template function means that if the if
 *  constexpr branch is not taken, the code inside does not need to be
 *  completely valid.
 */
template <typename Resource>
void HLTControlFlowMgr::fillMemoryPoolStats( Resource* memResource ) const {
  if constexpr ( LHCb::Allocators::Utils::provides_stats_v<Resource> ) {
    if ( memResource ) {
      m_memoryPoolSize += memResource->size();
      m_memoryPoolBlocks += memResource->num_blocks();
      m_memoryPoolCapacity += memResource->capacity();
      m_memoryPoolAllocations += memResource->num_allocations();
      m_memoryPoolMultipleAllocations += ( memResource->num_blocks() > 1 );
    }
  }
}

void HLTControlFlowMgr::promoteToExecuted( EventContext&& eventContext ) const {
  int si = eventContext.slot();

  // Schedule the cleanup of the event
  auto status = m_algExecStateSvc->eventStatus( eventContext );
  if ( status == EventStatus::Success ) {
    m_failed_evts_detected = 0;
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "Event " << eventContext.evt() << " finished (slot " << si << ")." << endmsg;
  } else {
    eventFailed( eventContext ).ignore();
    fatal() << "Failed event detected on " << eventContext << endmsg;
  }

  if constexpr ( LHCb::Allocators::Utils::provides_stats_v<LHCb::Allocators::MonotonicBufferResource> ) {
    // Fill some statistics about the per-event memory pool/arena
    fillMemoryPoolStats( LHCb::getMemResource( eventContext ) );
  }

  // Move the memory resource out of the event context, this prevents the resource being
  // kept alive on the done queue but ensures it lives for longer than the event store..
  auto memResource = eventContext.getExtension<LHCb::EventContextExtension>().removeMemResource();

  // In principle this isn't needed, it should just avoid an invalid pointer being kept
  // in the done queue.
  LHCb::setMemResource( eventContext, nullptr );

  // Other parts of the extension may depend on the memory resource, so better to remove
  // the whole thing before moving the event context onto the done queue.
  // (concretely, the scheduler extension inside the generic extension uses the resource.)
  eventContext.emplaceExtension<bool>( false ); // TODO: add a resetExtension() in Gaudi

  {
    std::lock_guard _{m_doneMutex};
    m_done.emplace( status == EventStatus::Success ? StatusCode::SUCCESS : StatusCode::FAILURE,
                    std::move( eventContext ) );
  }

  // Fire end of event "Incident" (it is considered part of the clearing of the TS)
  if ( m_endIncident.length() > 0 ) { m_incidentSvc->fireIncident( Incident( name(), m_endIncident ) ); }

  auto sc = m_whiteboard->clearStore( si );
  if ( !sc.isSuccess() ) warning() << "Clear of Event data store failed" << endmsg;
  sc = m_whiteboard->freeStore( si );
  if ( !sc.isSuccess() ) error() << "Whiteboard slot " << si << " could not be properly cleared";

  ++m_finishedEvt;

  m_createEventCond.notify_one();
}

// scheduling functionality----------------------------------------------------

void HLTControlFlowMgr::buildLines() { // here lines are configured, filled into the node vector m_allVNodes and
                                       // pointers are adjusted

  std::set<std::string> allChildNames;

  for ( auto const& cfprop : m_compositeCFProperties.value() ) {
    // NodeDef: ["name", "type", ["child1", "child2"], bool ordered]
    // create CompositeNodes with this configuration
    // TODO better dispatch?
    nodeType const nt = nodeTypeNames_inv.at( cfprop.type );
    if ( not[&]() {
           switch ( nt ) {
           case nodeType::LAZY_AND:
             return m_allVNodes
                 .emplace( cfprop.name,
                           CompositeNode<nodeType::LAZY_AND>{cfprop.name, cfprop.children, cfprop.ordered} )
                 .second;
           case nodeType::LAZY_OR:
             return m_allVNodes
                 .emplace( cfprop.name, CompositeNode<nodeType::LAZY_OR>{cfprop.name, cfprop.children, cfprop.ordered} )
                 .second;
           case nodeType::NONLAZY_OR:
             return m_allVNodes
                 .emplace( cfprop.name,
                           CompositeNode<nodeType::NONLAZY_OR>{cfprop.name, cfprop.children, cfprop.ordered} )
                 .second;
           case nodeType::NONLAZY_AND:
             return m_allVNodes
                 .emplace( cfprop.name,
                           CompositeNode<nodeType::NONLAZY_AND>{cfprop.name, cfprop.children, cfprop.ordered} )
                 .second;
           case nodeType::NOT:
             assert( cfprop.children.size() == 1 );
             return m_allVNodes
                 .emplace( cfprop.name, CompositeNode<nodeType::NOT>{cfprop.name, cfprop.children, cfprop.ordered} )
                 .second;
           default:
             throw GaudiException( "nodeType " + nodeTypeNames.at( nt ) + " does not exist.", __func__,
                                   StatusCode::FAILURE );
             return false;
           }
         }() ) {
      throw GaudiException( "node with name " + cfprop.name + " defined twice.", __func__, StatusCode::FAILURE );
    }
    for ( auto& childname : cfprop.children ) { allChildNames.emplace( childname ); }
  }

  // all childs that are not yet in m_allVNodes are obviously BasicNodes, so add them
  for ( std::string const& childname : allChildNames ) {
    m_allVNodes.try_emplace( childname, std::in_place_type<BasicNode>, childname, msgStream() );
  }

  // Now we resolve the names of children to pointers
  childrenNamesToPointers( m_allVNodes );

  // detect the motherOfAllNodes (the highest node)
  auto it = std::find_if( begin( m_allVNodes ), end( m_allVNodes ), [&]( auto const& entry ) {
    return std::all_of( begin( allChildNames ), end( allChildNames ),
                        [&]( std::string_view name ) { return name != entry.first; } );
  } );

  if ( it == end( m_allVNodes ) ) {
    throw GaudiException( "The control flow tree does not have a root, thus cannot be resolved. There needs to be one "
                          "node that is no child of any other node.",
                          __func__, StatusCode::FAILURE );
  }

  m_motherOfAllNodes = &it->second;

  if ( msgLevel( MSG::DEBUG ) ) {
    for ( auto const& [_, vnode] : m_allVNodes ) {
      std::visit( overload{[&]( auto const& node ) {
                             debug() << std::left << "children of " << std::setw( 13 ) << node.getType() << " "
                                     << std::setw( 15 ) << node.m_name << ": " << node.m_children << endmsg;
                           },
                           []( BasicNode const& ) {}},
                  vnode );
    }
  }
}

void HLTControlFlowMgr::configureScheduling() {

  // find all basic nodes by traversing the tree recursively
  auto       allBasics     = reachableBasics( gsl::not_null{m_motherOfAllNodes} );
  auto const allComposites = reachableComposites( gsl::not_null{m_motherOfAllNodes} );

  std::set<std::array<gsl::not_null<VNode*>, 2>> additionalEdges;

  // translate user edges, defined only by names, to edges of pointers to VNodes
  for ( auto& edge : m_userDefinedEdges ) {
    assert( edge.size() == 2 );
    additionalEdges.emplace(
        std::array{gsl::not_null{&m_allVNodes.at( edge[0] )}, gsl::not_null{&m_allVNodes.at( edge[1] )}} );
  }

  using Algorithm = Gaudi::Algorithm;
  // get data dependencies right
  // barrier inputs
  std::vector<std::vector<Algorithm*>> BarrierInputs{m_BarrierAlgNames.size()};
  // fill them

  // note: `algorithmsRequiredFor(name)` will _of course_ include the algorithm instanced named `name`
  std::transform( begin( m_BarrierAlgNames ), end( m_BarrierAlgNames ), begin( BarrierInputs ),
                  [&]( auto const& name ) { return m_databroker->algorithmsRequiredFor( name ); } );

  // which ones are also explicit CF nodes?
  std::vector<std::set<gsl::not_null<VNode*>>> explicitDataDependencies{BarrierInputs.size()};

  for ( std::size_t i = 0; i != BarrierInputs.size(); ++i ) {
    for ( Algorithm const* alg : BarrierInputs[i] ) {
      // because BarrierInputs contains the barrier itself _by construction_ we have to skip it here
      if ( alg->name() == m_BarrierAlgNames[i] ) continue;
      auto node = std::find_if(
          begin( allBasics ), end( allBasics ),
          [tn = alg->type() + "/" + alg->name()]( VNode const* vnode ) { return getNameOfVNode( *vnode ) == tn; } );
      if ( node != end( allBasics ) ) explicitDataDependencies[i].emplace( *node );
    }
  }

  std::vector<Algorithm*> allAlgos; // temporarily save/count all algorithms
  allAlgos.reserve( 1000 );         // whatever, 1000 is nice
  // fill algorithms that should definitely run
  for ( std::string const& algname : m_definitelyRunThese ) {
    for ( Algorithm* alg : m_databroker->algorithmsRequiredFor( algname ) ) {
      if ( std::find( begin( allAlgos ), end( allAlgos ), alg ) == end( allAlgos ) ) {
        m_definitelyRunTheseAlgs.emplace_back( alg, allAlgos.size(), m_EnableLegacyMode );
        allAlgos.emplace_back( alg );
      }
    }
  }

  for ( gsl::not_null<VNode*> vnode : allBasics ) {
    std::visit( overload{[&]( BasicNode& node ) {
                           // plug in data dependencies
                           auto reqAlgs = m_databroker->algorithmsRequiredFor( node.m_name, m_BarrierAlgNames );
                           node.m_RequiredAlgs.reserve( reqAlgs.size() );

                           for ( Algorithm* alg : reqAlgs ) {
                             auto index = std::find( begin( allAlgos ), end( allAlgos ), alg );
                             node.m_RequiredAlgs.emplace_back( alg, std::distance( begin( allAlgos ), index ),
                                                               m_EnableLegacyMode );
                             if ( index == end( allAlgos ) ) { allAlgos.emplace_back( alg ); }
                           }

                           // additional edges for the barrier. Since there is optional input for the barrier, we need
                           // to add additional edges from each input to each output of the barrier to make sure
                           // stuff runs in the right order. Keep in mind that datadependencies are cut away if they
                           // are optional (stopperalg in m_databroker)
                           for ( AlgWrapper const& reqAlg : node.m_RequiredAlgs ) {
                             for ( std::size_t i = 0; i != m_BarrierAlgNames.size(); ++i ) {
                               if ( reqAlg.name() == m_BarrierAlgNames[i] ) {
                                 for ( gsl::not_null<VNode*> explicitDD : explicitDataDependencies[i] ) {
                                   additionalEdges.emplace( std::array{explicitDD, vnode} );
                                 }
                               }
                             }
                           }
                         },
                         []( ... ) {}},
                *vnode );
  }

  m_AlgNames.reserve( allAlgos.size() );
  std::transform( begin( allAlgos ), end( allAlgos ), std::back_inserter( m_AlgNames ),
                  []( auto const* alg ) { return alg->name(); } );

  m_AlgStates.assign( allAlgos.size(), {} );
  m_TimingCounters.resize( allAlgos.size() );

  // end of Data dependency handling

  // fill the m_parent list of all nodes
  addParentsToAllNodes( allComposites );

  // get all unwrapped control flow edges
  auto nodePrerequisites = findAllEdges( gsl::not_null{m_motherOfAllNodes}, additionalEdges );

  // print out the edges
  if ( msgLevel( MSG::DEBUG ) ) debug() << "edges: " << nodePrerequisites << endmsg;

  // print out the edges
  if ( msgLevel( MSG::DEBUG ) ) debug() << "additional edges: " << additionalEdges << endmsg;

  // resolve all CF dependencies
  m_orderedNodesVec = resolveDependencies( allBasics, nodePrerequisites );

  // print out the order
  if ( msgLevel( MSG::DEBUG ) ) debug() << "ordered nodes: " << m_orderedNodesVec << endmsg;
}

void HLTControlFlowMgr::buildNodeStates() {

  m_NodeStates.reserve( m_allVNodes.size() );
  int helper_index = 0;

  for ( auto& [_, vNode] : m_allVNodes ) {
    std::visit( overload{[&]( BasicNode& node ) {
                           node.m_NodeID = helper_index++;
                           m_NodeStates.push_back( NodeState{1, true} );
                         },
                         [&]( auto& node ) {
                           node.m_NodeID = helper_index++;
                           m_NodeStates.push_back( NodeState{static_cast<uint16_t>( node.m_children.size() ), true} );
                         }},
                vNode );
  }

  // prepare counters
  m_NodeStateCounters.resize( 0 );
  m_NodeStateCounters.resize( m_NodeStates.size() );
}

// monitoring and printing functions --------------------------------------------------

// defines the tree to print out, where each element of the vector shall be one line
// we will later append execution and passed states to these lines to print
void HLTControlFlowMgr::registerStructuredTree() {
  assert( m_printableDependencyTree.empty() );
  std::stringstream ss;

  auto print_indented = [&]( VNode const* vnode, int const currentIndent, auto& itself ) -> void {
    // to recursively call this lambda, use auto& itself
    std::visit( overload{[&]( auto const& node ) {
                           std::stringstream ss;
                           ss << std::string( currentIndent, ' ' ) << node.getType() << ": " << node.m_name << " ";
                           m_printableDependencyTree.emplace_back( ss.str() );
                           m_mapPrintToNodeStateOrder.emplace_back( node.m_NodeID ); // to later print it
                           for ( VNode* child : node.m_children ) { itself( child, currentIndent + 1, itself ); }
                         },
                         [&]( BasicNode const& node ) {
                           std::stringstream ss;
                           ss << std::string( currentIndent, ' ' ) << node.m_name << " ";
                           m_printableDependencyTree.emplace_back( ss.str() );
                           m_mapPrintToNodeStateOrder.emplace_back( node.m_NodeID );
                         }},
                *vnode );
  };
  print_indented( m_motherOfAllNodes, 0, print_indented );
}

// determines the size of the largest string in the structured tree vector, to print neatly afterwards
void HLTControlFlowMgr::registerTreePrintWidth() {
  assert( !m_printableDependencyTree.empty() );
  m_maxTreeWidth = ( *std::max_element( begin( m_printableDependencyTree ), end( m_printableDependencyTree ),
                                        []( std::string_view s, std::string_view t ) { return s.size() < t.size(); } ) )
                       .size();
}

// build the full tree
template <typename Container>
std::stringstream HLTControlFlowMgr::buildPrintableStateTree( Container const& states ) const {
  assert( !m_printableDependencyTree.empty() );
  std::stringstream ss;
  for ( auto const& [treeEntry, printToStateIndex] :
        Gaudi::Functional::details::zip::range( m_printableDependencyTree, m_mapPrintToNodeStateOrder ) ) {
    ss << std::left << std::setw( m_maxTreeWidth + 1 ) << treeEntry << states[printToStateIndex] << '\n';
  }
  return ss;
}

// build the AlgState printout
std::stringstream
HLTControlFlowMgr::buildAlgsWithStates( LHCb::Interfaces::ISchedulerConfiguration::State const& state ) const {
  auto              states = state.algorithms();
  std::stringstream ss;
  ss << "AlgsWithStates: Algorithm   isExecuted|filterPassed\n";
  for ( auto const& [name, state] : Gaudi::Functional::details::zip::range( m_AlgNames, states ) ) {
    ss << std::left << std::setw( 20 ) << name << state.isExecuted << '|' << state.filterPassed << '\n';
  }
  return ss;
}
