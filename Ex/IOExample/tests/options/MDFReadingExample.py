###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import ApplicationMgr
from Configurables import LHCb__MDF__IOSvcMM, HiveWhiteBoard, HLTControlFlowMgr, HiveDataBrokerSvc, DDDBConf, LHCb__MDF__IOAlg
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from PRConfig.TestFileDB import test_file_db
from GaudiConf import IOHelper
from DDDB.CheckDD4Hep import UseDD4Hep
import os

# setup core Gaudi
whiteboard = HiveWhiteBoard("EventDataSvc", EventSlots=5, ForceLeaves=True)
eventloopmgr = HLTControlFlowMgr(
    "HLTControlFlowMgr",
    CompositeCFNodes=[('moore', 'LAZY_AND', [],
                       True)],  #sequence to be filled by defineSequence
    ThreadPoolSize=5)
appMgr = ApplicationMgr(
    'ApplicationMgr', EventLoop=eventloopmgr, EvtMax=1000, EvtSel='NONE')
appMgr.ExtSvc.insert(0, whiteboard)
appMgr.ExtSvc.append(MessageSvcSink())

hiveDataBroker = HiveDataBrokerSvc('HiveDataBrokerSvc')

# deal with input files
fileDbEntry = test_file_db["MiniBrunel_2018_MinBias_FTv4_MDF"]
qualifiers = fileDbEntry.qualifiers
iohelper = IOHelper("MDF", None)
iohelper.setupServices()
dddb = DDDBConf(
    'DDDBConf',
    Simulation=qualifiers['Simulation'],
    DataType=qualifiers['DataType'])
if not UseDD4Hep:
    from Configurables import CondDB
    conddb = CondDB(
        'CondDB',
        Upgrade=True,
        Tags={
            "DDDB": qualifiers['DDDB'],
            "SIMCOND": qualifiers['CondDB']
        })

# let's grab 2 files, one regular one (00067189.mdf) and one with compressed events (00146082_00012611_1.mdf)
path = "/tmp/00067189.mdf"
if not os.path.isfile(path):
    os.system(
        "xrdcp -s root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/MiniBrunel/00067189.mdf %s"
        % path)
pathCompressed = "/tmp/00146082_00012611_1.mdf"
if not os.path.isfile(pathCompressed):
    os.system(
        "xrdcp -s root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/MiniBrunel/00146082_00012611_1.mdf %s"
        % pathCompressed)

# we will run for 3000 events, as there are a bit more than 1000 in the compressed file
# and we want to try both compressed and non compressed events
appMgr.EvtMax = 3000

# define algorithms and service we will use for that example
mdfioSvc = LHCb__MDF__IOSvcMM(
    'LHCb::MDF::IOSvcMM', Input=[pathCompressed, path])
fetchData = LHCb__MDF__IOAlg(
    'ReadMDFInput',
    RawEventLocation="/Event/DAQ/RawEvent",
    IOSvc="LHCb::MDF::IOSvcMM")
hiveDataBroker.DataProducers.append(fetchData)
appMgr.TopAlg.append(fetchData)

# and build the sequence
appMgr.EventLoop.CompositeCFNodes[0][2].extend([fetchData.name()])
