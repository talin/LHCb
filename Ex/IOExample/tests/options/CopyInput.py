###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (ApplicationMgr, Gaudi__Hive__FetchLeavesFromFile as
                           FetchLeavesFromFile, LHCbApp)
from GaudiConf import IOHelper
from PRConfig.TestFileDB import test_file_db

ioh = IOHelper()
ioh.setupServices()
ioh.outStream("PFN:LHCbApp_CopyInputStream.dst", "CopyInputStream")

app = LHCbApp(EvtMax=10)
appMgr = ApplicationMgr()
appMgr.TopAlg = [FetchLeavesFromFile()]

test_file_db["upgrade_minbias_2019"].run(configurable=app)
