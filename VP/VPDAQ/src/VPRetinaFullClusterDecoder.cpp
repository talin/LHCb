/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/VP/VPChannelID.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/VPFullCluster.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "Kernel/VPConstants.h"
#include "LHCbAlgs/Transformer.h"
#include "VPDAQ/VPRetinaClusterConstants.h"
#include "VPDet/DeVP.h"
#include "VPKernel/PixelUtils.h"
#include "VPKernel/VeloPixelInfo.h"
#include "VPRetinaTopologyID.h"
#include <array>
#include <iomanip>
#include <tuple>
#include <vector>

namespace VPRetinaFull {
  enum class ErrorCode : StatusCode::code_t {
    BANK_VERSION_UNKNOWN,
    EMPTY_BANK,
    BANK_SIZE_INCONSISTENT,
    BAD_MAGIC,
    BAD_SOURCE_ID
  };
  struct ErrorCategory : StatusCode::Category {
    const char* name() const override { return "VPRetinaDecoder"; }
    bool        isRecoverable( StatusCode::code_t ) const override { return false; }
    std::string message( StatusCode::code_t code ) const override {
      switch ( static_cast<VPRetinaFull::ErrorCode>( code ) ) {
      case ErrorCode::BANK_VERSION_UNKNOWN:
        return "Bank version unknown";
      case ErrorCode::EMPTY_BANK:
        return "Empty bank";
      case ErrorCode::BANK_SIZE_INCONSISTENT:
        return "Bank size and declared number of clusters inconsistent";
      case ErrorCode::BAD_MAGIC:
        return "Wrong magic number for bank";
      case ErrorCode::BAD_SOURCE_ID:
        return "Source ID not in the expected range";
      default:
        return StatusCode::default_category().message( code );
      }
    }
  };
} // namespace VPRetinaFull
STATUSCODE_ENUM_DECL( VPRetinaFull::ErrorCode )
STATUSCODE_ENUM_IMPL( VPRetinaFull::ErrorCode, VPRetinaFull::ErrorCategory )

[[gnu::noreturn]] void throw_exception( VPRetinaFull::ErrorCode ec, const char* tag ) {
  auto sc = StatusCode( ec );
  throw GaudiException{sc.message(), tag, std::move( sc )};
}
#define OOPS( x ) throw_exception( x, __PRETTY_FUNCTION__ )

// Namespace for locations in TES
namespace LHCb {
  namespace VPFullClusterLocation {
    inline const std::string Offsets = "Raw/VP/FullClustersOffsets";
  }
} // namespace LHCb

class VPRetinaFullClusterDecoder
    : public LHCb::Algorithm::MultiTransformer<
          std::tuple<std::vector<LHCb::VPFullCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>(
              const LHCb::RawEvent&, const DeVP& ),
          LHCb::Algorithm::Traits::usesConditions<DeVP>> {

public:
  /// Standard constructor
  VPRetinaFullClusterDecoder( const std::string& name, ISvcLocator* pSvcLocator );

  /// Algorithm execution
  std::tuple<std::vector<LHCb::VPFullCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>
  operator()( const LHCb::RawEvent&, const DeVP& ) const override;

private:
  std::bitset<VP::NModules>                       m_modulesToSkipMask;
  Gaudi::Property<std::vector<unsigned int>>      m_modulesToSkip{this,
                                                             "ModulesToSkip",
                                                             {},
                                                             [=]( auto& ) {
                                                               m_modulesToSkipMask.reset();
                                                               for ( auto i : m_modulesToSkip )
                                                                 m_modulesToSkipMask.set( i );
                                                             },
                                                             Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
                                                             "List of modules that should be skipped in decoding"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nBanks{this, "Number of banks"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClusters{this, "Number of clusters"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersMod14{this, "Number of clusters - Mod14"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersMod15{this, "Number of clusters - Mod15"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersMod16{this, "Number of clusters - Mod16"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersIsolated{this, "Number of clusters from isolated SPs"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersOverflow{this, "Number of clusters from overflowing SPs"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersOverflowMod14{
      this, "Number of clusters from overflowing SPs - Mod 14"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersOverflowMod15{
      this, "Number of clusters from overflowing SPs - Mod 15"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersOverflowMod16{
      this, "Number of clusters from overflowing SPs - Mod 16"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersNeighbor{this, "Number of clusters from SPs w/ neighbors"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersEdge{this, "Number of clusters at matrix edge"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersNotCont{this, "Number of clusters not self contained"};
};

DECLARE_COMPONENT( VPRetinaFullClusterDecoder )

using namespace Pixel;
using namespace VPRetinaCluster;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPRetinaFullClusterDecoder::VPRetinaFullClusterDecoder( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer( name, pSvcLocator,
                        {KeyValue{"RawEventLocation", LHCb::RawEventLocation::VeloCluster},
                         KeyValue{"DEVP", LHCb::Det::VP::det_path}},
                        {KeyValue{"ClusterLocation", LHCb::VPFullClusterLocation::Default},
                         KeyValue{"ClusterOffsets", LHCb::VPFullClusterLocation::Offsets}} ) {}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<std::vector<LHCb::VPFullCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>
VPRetinaFullClusterDecoder::operator()( const LHCb::RawEvent& rawEvent, const DeVP& devp ) const {

  auto result = std::tuple<std::vector<LHCb::VPFullCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>{};
  auto& [clusters, offsets] = result;

  const auto& banks = rawEvent.banks( LHCb::RawBank::VPRetinaCluster );
  m_nBanks += banks.size();
  if ( banks.empty() ) return result;

  const unsigned int version = banks[0]->version();

  // only version 3 or greater, since topology is available only here, and without that
  // is not possible to retrieve the pixels
  if ( ( version != 3 ) && ( version != VPRetinaCluster::c_bankVersion ) )
    OOPS( VPRetinaFull::ErrorCode::BANK_VERSION_UNKNOWN );

  // Since 'clusters` is local, to first preallocate, then count hits per module,
  // and then preallocate per module and move hits might not be faster than adding
  // directly to the PixelModuleHits (which would require more allocations, but
  // not many if we start with a sensible default)
  constexpr unsigned int startSize = 10000U;
  clusters.reserve( startSize );

  uint32_t nclusterMod14         = 0;
  uint32_t nclusterMod15         = 0;
  uint32_t nclusterMod16         = 0;
  uint32_t nclusterIsolated      = 0;
  uint32_t nclusterNeighbor      = 0;
  uint32_t nclusterOverflow      = 0;
  uint32_t nclusterOverflowMod14 = 0;
  uint32_t nclusterOverflowMod15 = 0;
  uint32_t nclusterOverflowMod16 = 0;
  uint32_t nclusterEdge          = 0;
  uint32_t nclusterNotCont       = 0;

  // Loop over VP RawBanks
  for ( const auto* bank : banks ) {
    if ( LHCb::RawBank::MagicPattern != bank->magic() ) OOPS( VPRetinaFull::ErrorCode::BAD_MAGIC );

    const auto   sourceID = bank->sourceID();
    uint32_t     sourceID_masked;
    unsigned int module;

    if ( version == 3 ) {
      if ( static_cast<unsigned int>( bank->sourceID() ) >= VP::NSensors )
        OOPS( VPRetinaFull::ErrorCode::BAD_SOURCE_ID );
      sourceID_masked = sourceID;
      module          = sourceID % VP::NSensorsPerModule + 1;
    } else {
      // TFC parition is in mask 0x1800 and is 0x1800 for A and 0x1000 for C sides
      if ( ( ( sourceID & 0x1800 ) != 0x1800 ) && ( ( sourceID & 0x1800 ) != 0x1000 ) )
        OOPS( VPRetinaFull::ErrorCode::BAD_SOURCE_ID );
      // The sensor index is
      // bits 9..2 - Module ID (from SourceID bits 8..1)
      // bit  1    - DataFlow ID (from SourceID bit 0)
      // bit  0    - SensorID bit 0 for sensor 0 (of pair), 1 for sensor 1
      sourceID_masked = ( sourceID & 0x1FFU ) << 1;
      module          = ( sourceID & 0x1FEU ) >> 1;
    }

    const auto sensor0 = LHCb::Detector::VPChannelID::SensorID( sourceID_masked );                      // bit 0 = 0
    const auto sensor1 = LHCb::Detector::VPChannelID::SensorID( ( ( sourceID & 0x1FFU ) << 1 ) | 0x1 ); // bit 0 = 1

    if ( m_modulesToSkipMask[module - 1] ) continue;

    uint32_t              ncluster;
    auto                  data = bank->range<uint32_t>();
    std::vector<uint32_t> data_copy;
    data_copy.reserve( data.size() );

    //     if ( data.empty() ) OOPS( VPRetinaFull::ErrorCode::EMPTY_BANK );

    //     const uint32_t ncluster = data[0];
    //     if ( data.size() != ncluster + 1 ) OOPS( VPRetinaFull::ErrorCode::BANK_SIZE_INCONSISTENT );
    //     data = data.subspan( 1 );

    //     const auto& ltg = devp.ltg( sensor );

    if ( version == 3 ) {
      if ( data.empty() ) OOPS( VPRetinaFull::ErrorCode::EMPTY_BANK );
      if ( data.size() != data[0] + 1 ) OOPS( VPRetinaFull::ErrorCode::BANK_SIZE_INCONSISTENT );
      for ( uint32_t clu : data.subspan( 1 ) ) {
        if ( clu != 0 ) {
          data_copy.push_back( clu ); // protect against zero clusters.
        }
      }
    } else {
      for ( uint32_t clu : data ) {
        if ( clu != 0 ) {
          data_copy.push_back( clu ); // protect against zero clusters.
        }
      }
    }
    ncluster = data_copy.size();

    const auto& ltg0 = devp.ltg( sensor0 );
    const auto& ltg1 = devp.ltg( sensor1 );

    // Read clusters
    std::transform(
        data_copy.begin(), data_copy.end(), std::back_inserter( clusters ),
        [&]( const uint32_t cluster_word ) -> LHCb::VPFullCluster {
          uint32_t                              cx, cx_frac_half, cx_frac_quarter;
          uint32_t                              cy_frac_half, cy_frac_quarter;
          uint32_t                              iso_flag;
          uint32_t                              fx_int, fy_int;
          float                                 fx, fy;
          LHCb::Detector::VPChannelID::ChipID   chip;
          LHCb::Detector::VPChannelID::RowID    cy;
          LHCb::Detector::VPChannelID::ColumnID ccol;
          LHCb::Detector::VPChannelID::OrfxID   or_fx;
          LHCb::Detector::VPChannelID::OrfyID   or_fy;

          iso_flag = ( cluster_word >> isoBit_shift );

          cx     = ( cluster_word >> ( Row_nbit + fracCol_nbit ) ) & intCol_mask;
          fx_int = ( ( cluster_word >> Row_nbit ) & fracCol_mask );
          fx     = ( ( cluster_word >> Row_nbit ) & fracCol_mask ) / 4.f;
          cy     = LHCb::Detector::VPChannelID::RowID{( cluster_word >> fracRow_nbit ) & intRow_mask};
          fy_int = ( cluster_word & fracRow_mask );
          fy     = ( cluster_word & fracRow_mask ) / 4.f;
          chip   = LHCb::Detector::VPChannelID::ChipID{cx / CHIP_COLUMNS};
          ccol   = LHCb::Detector::VPChannelID::ColumnID{cx % CHIP_COLUMNS};

          cx_frac_half    = ( cluster_word >> 11 ) & 0x1;
          cx_frac_quarter = ( cluster_word >> 10 ) & 0x1;
          or_fx           = LHCb::Detector::VPChannelID::OrfxID{( cx_frac_half | cx_frac_quarter )};

          cy_frac_half    = ( cluster_word >> 1 ) & 0x1;
          cy_frac_quarter = (cluster_word)&0x1;
          or_fy           = LHCb::Detector::VPChannelID::OrfyID{( cy_frac_half | cy_frac_quarter )};

          const float local_x = devp.local_x( cx ) + fx * devp.x_pitch( cx );
          const float local_y = ( to_unsigned( cy ) + 0.5 + fy ) * devp.pixel_size();

          //                       const float gx = ( ltg[0] * local_x + ltg[1] * local_y + ltg[9] );
          //                       const float gy = ( ltg[3] * local_x + ltg[4] * local_y + ltg[10] );
          //                       const float gz = ( ltg[6] * local_x + ltg[7] * local_y + ltg[11] );

          // sensor number set from 22nd bit for position in sensor pair
          LHCb::Detector::VPChannelID::SensorID sensor;
          float                                 gx, gy, gz;
          if ( version == 3 ) {
            sensor = sensor0;
            gx     = ( ltg0[0] * local_x + ltg0[1] * local_y + ltg0[9] );
            gy     = ( ltg0[3] * local_x + ltg0[4] * local_y + ltg0[10] );
            gz     = ( ltg0[6] * local_x + ltg0[7] * local_y + ltg0[11] );

          } else {
            if ( ( ( cluster_word >> sensorID_shift ) & 0x1U ) ) {
              sensor = sensor1;
              gx     = ( ltg1[0] * local_x + ltg1[1] * local_y + ltg1[9] );
              gy     = ( ltg1[3] * local_x + ltg1[4] * local_y + ltg1[10] );
              gz     = ( ltg1[6] * local_x + ltg1[7] * local_y + ltg1[11] );
            } else {
              sensor = sensor0;
              gx     = ( ltg0[0] * local_x + ltg0[1] * local_y + ltg0[9] );
              gy     = ( ltg0[3] * local_x + ltg0[4] * local_y + ltg0[10] );
              gz     = ( ltg0[6] * local_x + ltg0[7] * local_y + ltg0[11] );
            }
          }

          const auto mod = to_unsigned( sensor ) / VP::NSensorsPerModule;
          switch ( mod ) {
          case 14:
            nclusterMod14 += 1;
            break;
          case 15:
            nclusterMod15 += 1;
            break;
          case 16:
            nclusterMod16 += 1;
            break;
          default: /* nothing */;
          }

          if ( ( cluster_word >> isoBit_shift ) && ( ( cluster_word >> isoFlag_shift ) & 0x1 ) ) {
            nclusterIsolated += 1;
          }

          if ( ( cluster_word >> isoBit_shift ) && !( ( cluster_word >> isoFlag_shift ) & 0x1 ) ) {
            nclusterOverflow += 1;
            switch ( mod ) {
            case 14:
              nclusterOverflowMod14 += 1;
              break;
            case 15:
              nclusterOverflowMod15 += 1;
              break;
            case 16:
              nclusterOverflowMod16 += 1;
              break;
            default: /* nothing */;
            }
          }

          if ( !( cluster_word >> isoBit_shift ) ) { nclusterNeighbor += 1; }

          if ( !( cluster_word >> isoBit_shift ) && ( ( cluster_word >> edgeFlag_shift ) & 0x1 ) ) {
            nclusterEdge += 1;
          }

          if ( !( cluster_word >> isoBit_shift ) && !( ( cluster_word >> selfContFlag_shift ) & 0x1 ) ) {
            nclusterNotCont += 1;
          }
          //********************************************************
          // now retrieving pixels from topology ID
          VPRetinaTopologyID                       topo_obj;
          uint32_t                                 vec_pix;
          std::vector<LHCb::Detector::VPChannelID> channelIDs;

          if ( iso_flag ) {
            uint32_t topo_ID = ( cluster_word >> topo2x4_shift ) & topo2x4_mask;
            vec_pix          = topo_obj.topoIDfrac_to_topo2x4( topo_ID, fx_int, fy_int );
            std::bitset<8> bit_vec_pix( vec_pix );

            uint32_t count_y[4] = {0, 0, 0, 0};
            uint32_t count_x[2] = {0, 0};

            for ( uint32_t i = 0; i < 8; i++ ) {
              if ( bit_vec_pix[i] == 1 ) {
                count_y[i % 4]++;
                count_x[i / 4]++;
              }
            }

            uint32_t int_x = unsigned( round( ( ( count_x[1] * 1.f ) / (float)bit_vec_pix.count() ) * 4.f ) / 4.f );
            uint32_t int_y = unsigned(
                round( ( ( count_y[1] * 1.f + count_y[2] * 2.f + count_y[3] * 3.f ) / (float)bit_vec_pix.count() ) *
                       4.f ) /
                4.f );
            // storing pixels, given the centroid location of the cluster
            for ( uint32_t i = 0; i < 8; i++ ) {
              if ( bit_vec_pix[i] == 1 ) {
                auto col_pix = LHCb::Detector::VPChannelID::ColumnID{( cx - int_x + i / 4 ) % CHIP_COLUMNS};
                auto row_pix = LHCb::Detector::VPChannelID::RowID{to_unsigned( cy ) - int_y + i % 4};
                auto chip_id = LHCb::Detector::VPChannelID::ChipID{( cx - int_x + i / 4 ) / CHIP_COLUMNS};
                channelIDs.emplace_back( sensor, chip_id, col_pix, row_pix );
              }
            }

          } // end if iso_flag=1
          else {
            uint32_t topo_ID = ( cluster_word >> topo3x3_shift ) & topo3x3_mask;
            vec_pix          = topo_obj.topoIDfrac_to_topo3x3( to_unsigned( sensor ), topo_ID, fx_int, fy_int );
            std::bitset<9> bit_vec_pix( vec_pix );

            uint32_t count_y[3] = {0, 0, 0};
            uint32_t count_x[3] = {0, 0, 0};

            for ( uint32_t i = 0; i < 9; i++ ) {
              if ( bit_vec_pix[i] == 1 ) {
                count_y[i % 3]++;
                count_x[i / 3]++;
              }
            }

            uint32_t int_x = unsigned(
                round( ( ( count_x[1] * 1.f + count_x[2] * 2.f ) / (float)bit_vec_pix.count() ) * 4.f ) / 4.f );
            uint32_t int_y = unsigned(
                round( ( ( count_y[1] * 1.f + count_y[2] * 2.f ) / (float)bit_vec_pix.count() ) * 4.f ) / 4.f );

            // storing pixels, given the centroid location of the cluster
            for ( uint32_t i = 0; i < 9; i++ ) {
              if ( bit_vec_pix[i] == 1 ) {
                auto col_pix = LHCb::Detector::VPChannelID::ColumnID{( cx - int_x + i / 3 ) % CHIP_COLUMNS};
                auto row_pix = LHCb::Detector::VPChannelID::RowID{to_unsigned( cy ) - int_y + i % 3};
                auto chip_id = LHCb::Detector::VPChannelID::ChipID{( cx - int_x + i / 3 ) / CHIP_COLUMNS};
                channelIDs.emplace_back( sensor, chip_id, col_pix, row_pix );
              }
            }
          } // end if iso_flag = 0

          return {fx,
                  fy,
                  gx,
                  gy,
                  gz,
                  LHCb::Detector::VPChannelID{sensor, chip, ccol, cy, or_fx, or_fy},
                  std::move( channelIDs )};
        } );
    offsets[module] += ncluster;
  } // loop over all banks

  std::partial_sum( offsets.begin(), offsets.end(), offsets.begin() );

  // sorting in phi for even modules
  auto cmp_phi_for_odd_modules = []( const LHCb::VPFullCluster& a, const LHCb::VPFullCluster& b ) {
    return ( a.y() < 0.f && b.y() > 0.f ) ||
           // same y side even and odd modules, check y1/x1 < y2/x2
           ( ( a.y() * b.y() ) > 0.f && ( a.y() * b.x() < b.y() * a.x() ) );
  };

  // sorting in phi for odd modules
  auto cmp_phi_for_even_modules = []( const LHCb::VPFullCluster& a, const LHCb::VPFullCluster& b ) {
    return ( a.y() > 0.f && b.y() < 0.f ) ||
           // same y side even and odd modules, check y1/x1 < y2/x2
           ( ( a.y() * b.y() ) > 0.f && ( a.y() * b.x() < b.y() * a.x() ) );
  };

  auto sort_module = [clusters = std::ref( clusters ), offsets = std::ref( offsets )]( auto id, auto cmp ) {
    std::sort( clusters.get().begin() + offsets.get()[id], clusters.get().begin() + offsets.get()[id + 1], cmp );
  };

  for ( size_t moduleID = 0; moduleID < VeloInfo::Numbers::NModules; ++moduleID ) {
    // In even modules you fall in the branching at -180, 180 degrees, you want to do that continuos
    if ( moduleID % 2 == 1 ) {
      sort_module( moduleID, cmp_phi_for_odd_modules );
    } else {
      sort_module( moduleID, cmp_phi_for_even_modules );
    }
  }

  m_nClusters += offsets.back();
  m_nClustersMod14 += nclusterMod14;
  m_nClustersMod15 += nclusterMod15;
  m_nClustersMod16 += nclusterMod16;
  m_nClustersIsolated += nclusterIsolated;
  m_nClustersOverflow += nclusterOverflow;
  m_nClustersOverflowMod14 += nclusterOverflowMod14;
  m_nClustersOverflowMod15 += nclusterOverflowMod15;
  m_nClustersOverflowMod16 += nclusterOverflowMod16;
  m_nClustersNeighbor += nclusterNeighbor;
  m_nClustersEdge += nclusterEdge;
  m_nClustersNotCont += nclusterNotCont;

  return result;
}
