/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/VP/VPChannelID.h"
#include "Event/RawBank.h"
#include "Event/VPLightCluster.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "Kernel/VPConstants.h"
#include "LHCbAlgs/Transformer.h"
#include "VPDAQ/VPRetinaClusterConstants.h"
#include "VPDet/DeVP.h"
#include "VPKernel/PixelUtils.h"
#include "VPKernel/VeloPixelInfo.h"
#include <array>
#include <iomanip>
#include <tuple>
#include <vector>

namespace VPRetina {
  enum class ErrorCode : StatusCode::code_t {
    BANK_VERSION_UNKNOWN,
    EMPTY_BANK,
    BANK_SIZE_INCONSISTENT,
    BAD_MAGIC,
    BAD_SOURCE_ID,
    WRONG_BANK_TYPE,
  };
  struct ErrorCategory : StatusCode::Category {
    const char* name() const override { return "VPRetinaDecoder"; }
    bool        isRecoverable( StatusCode::code_t ) const override { return false; }
    std::string message( StatusCode::code_t code ) const override {
      switch ( static_cast<VPRetina::ErrorCode>( code ) ) {
      case ErrorCode::BANK_VERSION_UNKNOWN:
        return "Bank version unknown";
      case ErrorCode::EMPTY_BANK:
        return "Empty bank";
      case ErrorCode::BANK_SIZE_INCONSISTENT:
        return "Bank size and declared number of clusters inconsistent";
      case ErrorCode::BAD_MAGIC:
        return "Wrong magic number for bank";
      case ErrorCode::BAD_SOURCE_ID:
        return "Source ID not in the expected range";
      case ErrorCode::WRONG_BANK_TYPE:
        return "RawBank::Type is not VPRetinaCluster";
      default:
        return StatusCode::default_category().message( code );
      }
    }
  };
} // namespace VPRetina
STATUSCODE_ENUM_DECL( VPRetina::ErrorCode )
STATUSCODE_ENUM_IMPL( VPRetina::ErrorCode, VPRetina::ErrorCategory )

[[gnu::noreturn]] void throw_exception( VPRetina::ErrorCode ec, const char* tag ) {
  auto sc = StatusCode( ec );
  throw GaudiException{sc.message(), tag, std::move( sc )};
}
#define OOPS( x ) throw_exception( x, __PRETTY_FUNCTION__ )

// Namespace for locations in TES
namespace LHCb {
  namespace VPClusterLocation {
    inline const std::string Offsets = "Raw/VP/LightClustersOffsets";
  }
} // namespace LHCb

class VPRetinaClusterDecoder
    : public LHCb::Algorithm::MultiTransformer<
          std::tuple<std::vector<LHCb::VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>(
              const LHCb::RawBank::View&, const DeVP& ),
          LHCb::Algorithm::Traits::usesConditions<DeVP>> {

public:
  /// Standard constructor
  VPRetinaClusterDecoder( const std::string& name, ISvcLocator* pSvcLocator );

  /// Algorithm initialization
  // StatusCode initialize() override;

  /// Algorithm execution
  std::tuple<std::vector<LHCb::VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>
  operator()( const LHCb::RawBank::View&, const DeVP& ) const override;

private:
  std::bitset<VP::NModules>                       m_modulesToSkipMask;
  Gaudi::Property<std::vector<unsigned int>>      m_modulesToSkip{this,
                                                             "ModulesToSkip",
                                                             {},
                                                             [=]( auto& ) {
                                                               m_modulesToSkipMask.reset();
                                                               for ( auto i : m_modulesToSkip )
                                                                 m_modulesToSkipMask.set( i );
                                                             },
                                                             Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
                                                             "List of modules that should be skipped in decoding"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nBanks{this, "Number of banks"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClusters{this, "Number of clusters"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersMod14{this, "Number of clusters - Mod14"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersMod15{this, "Number of clusters - Mod15"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersMod16{this, "Number of clusters - Mod16"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersIsolated{this, "Number of clusters from isolated SPs"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersOverflow{this, "Number of clusters from overflowing SPs"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersOverflowMod14{
      this, "Number of clusters from overflowing SPs - Mod 14"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersOverflowMod15{
      this, "Number of clusters from overflowing SPs - Mod 15"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersOverflowMod16{
      this, "Number of clusters from overflowing SPs - Mod 16"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersNeighbor{this, "Number of clusters from SPs w/ neighbors"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersEdge{this, "Number of clusters at matrix edge"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersNotCont{this, "Number of clusters not self contained"};
};

DECLARE_COMPONENT( VPRetinaClusterDecoder )

using namespace Pixel;
using namespace VPRetinaCluster;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPRetinaClusterDecoder::VPRetinaClusterDecoder( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer( name, pSvcLocator, {KeyValue{"RawBanks", {}}, KeyValue{"DEVP", LHCb::Det::VP::det_path}},
                        {KeyValue{"ClusterLocation", LHCb::VPClusterLocation::Light},
                         KeyValue{"ClusterOffsets", LHCb::VPClusterLocation::Offsets}} ) {}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<std::vector<LHCb::VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>
VPRetinaClusterDecoder::operator()( const LHCb::RawBank::View& banks, const DeVP& devp ) const {
  auto result = std::tuple<std::vector<LHCb::VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>{};
  auto& [clusters, offsets] = result;

  m_nBanks += banks.size();
  if ( banks.empty() ) return result;

  if ( banks[0]->type() != LHCb::RawBank::VPRetinaCluster ) OOPS( VPRetina::ErrorCode::WRONG_BANK_TYPE );

  const unsigned int version = banks[0]->version();
  if ( ( version != VPRetinaCluster::c_bankVersion ) && ( version != 2 ) && ( version != 3 ) )
    OOPS( VPRetina::ErrorCode::BANK_VERSION_UNKNOWN );

  // Since 'clusters` is local, to first preallocate, then count hits per module,
  // and then preallocate per module and move hits might not be faster than adding
  // directly to the PixelModuleHits (which would require more allocations, but
  // not many if we start with a sensible default)
  constexpr unsigned int startSize = 10000U;
  clusters.reserve( startSize );

  uint32_t nclusterMod14         = 0;
  uint32_t nclusterMod15         = 0;
  uint32_t nclusterMod16         = 0;
  uint32_t nclusterIsolated      = 0;
  uint32_t nclusterNeighbor      = 0;
  uint32_t nclusterOverflow      = 0;
  uint32_t nclusterOverflowMod14 = 0;
  uint32_t nclusterOverflowMod15 = 0;
  uint32_t nclusterOverflowMod16 = 0;
  uint32_t nclusterEdge          = 0;
  uint32_t nclusterNotCont       = 0;

  // Loop over VP RawBanks
  for ( const auto* bank : banks ) {
    if ( LHCb::RawBank::MagicPattern != bank->magic() ) OOPS( VPRetina::ErrorCode::BAD_MAGIC );

    const auto   sourceID = bank->sourceID();
    uint32_t     sourceID_masked;
    unsigned int module;

    if ( version == 2 || version == 3 ) {
      if ( static_cast<unsigned int>( sourceID ) >= VP::NSensors ) OOPS( VPRetina::ErrorCode::BAD_SOURCE_ID );
      sourceID_masked = sourceID;
      module          = sourceID % VP::NSensorsPerModule + 1;
    } else {
      // TFC parition is in mask 0x1800 and is 0x1800 for A and 0x1000 for C sides
      if ( ( ( sourceID & 0x1800 ) != 0x1800 ) && ( ( sourceID & 0x1800 ) != 0x1000 ) )
        OOPS( VPRetina::ErrorCode::BAD_SOURCE_ID );
      // The sensor index is
      // bits 9..2 - Module ID (from SourceID bits 8..1)
      // bit  1    - DataFlow ID (from SourceID bit 0)
      // bit  0    - SensorID bit 0 for sensor 0 (of pair), 1 for sensor 1
      sourceID_masked = ( sourceID & 0x1FFU ) << 1;
      module          = ( sourceID & 0x1FEU ) >> 1;
    }

    const auto sensor0 = LHCb::Detector::VPChannelID::SensorID( sourceID_masked );                      // bit 0 = 0
    const auto sensor1 = LHCb::Detector::VPChannelID::SensorID( ( ( sourceID & 0x1FFU ) << 1 ) | 0x1 ); // bit 0 = 1

    if ( m_modulesToSkipMask[module - 1] ) continue;

    uint32_t              ncluster;
    auto                  data = bank->range<uint32_t>();
    std::vector<uint32_t> data_copy;
    data_copy.reserve( data.size() );

    if ( version == 2 || version == 3 ) {
      if ( data.empty() ) OOPS( VPRetina::ErrorCode::EMPTY_BANK );
      if ( data.size() != data[0] + 1 ) OOPS( VPRetina::ErrorCode::BANK_SIZE_INCONSISTENT );
      for ( uint32_t clu : data.subspan( 1 ) ) {
        if ( clu != 0 ) {
          data_copy.push_back( clu ); // protect against zero clusters.
        }
      }
    } else {
      for ( uint32_t clu : data ) {
        if ( clu != 0 ) {
          data_copy.push_back( clu ); // protect against zero clusters.
        }
      }
    }
    ncluster = data_copy.size();

    const auto& ltg0 = devp.ltg( sensor0 );
    const auto& ltg1 = devp.ltg( sensor1 );

    // -- Abort if a too large sensor column is decoded
    const auto skip = std::any_of( data_copy.begin(), data_copy.end(), [&]( auto cluster_word ) {
      const auto cx = ( version == 2 ) ? ( cluster_word >> 14 ) & 0x3FF
                                       : ( cluster_word >> ( Row_nbit + fracCol_nbit ) ) & intCol_mask;
      return ( cx >= VP::NSensorColumns );
    } );

    if ( skip ) continue;
    // --

    // Read clusters
    std::transform( data_copy.begin(), data_copy.end(), std::back_inserter( clusters ),
                    [&]( const uint32_t cluster_word ) -> LHCb::VPLightCluster {
                      uint32_t cx, cx_frac_half, cx_frac_quarter, cy_frac_half, cy_frac_quarter;
                      float    fx, fy;
                      LHCb::Detector::VPChannelID::ChipID   chip;
                      LHCb::Detector::VPChannelID::RowID    cy;
                      LHCb::Detector::VPChannelID::ColumnID ccol;
                      LHCb::Detector::VPChannelID::OrfxID   or_fx;
                      LHCb::Detector::VPChannelID::OrfyID   or_fy;

                      if ( version == 2 ) {

                        cx   = ( cluster_word >> 14 ) & 0x3FF;
                        fx   = ( ( cluster_word >> 11 ) & 0x7 ) / 8.f;
                        cy   = LHCb::Detector::VPChannelID::RowID{( cluster_word >> 3 ) & 0xFF};
                        fy   = ( cluster_word & 0x7FF ) / 8.f;
                        chip = LHCb::Detector::VPChannelID::ChipID{cx / CHIP_COLUMNS};
                        ccol = LHCb::Detector::VPChannelID::ColumnID{cx % CHIP_COLUMNS};

                        or_fx = LHCb::Detector::VPChannelID::OrfxID{0};
                        or_fy = LHCb::Detector::VPChannelID::OrfyID{0};

                      } else {

                        cx   = ( cluster_word >> ( Row_nbit + fracCol_nbit ) ) & intCol_mask;
                        fx   = ( ( cluster_word >> Row_nbit ) & fracCol_mask ) / 4.f;
                        cy   = LHCb::Detector::VPChannelID::RowID{( cluster_word >> fracRow_nbit ) & intRow_mask};
                        fy   = ( cluster_word & fracRow_mask ) / 4.f;
                        chip = LHCb::Detector::VPChannelID::ChipID{cx / CHIP_COLUMNS};
                        ccol = LHCb::Detector::VPChannelID::ColumnID{cx % CHIP_COLUMNS};

                        cx_frac_half    = ( cluster_word >> 11 ) & 0x1;
                        cx_frac_quarter = ( cluster_word >> 10 ) & 0x1;
                        or_fx           = LHCb::Detector::VPChannelID::OrfxID{( cx_frac_half | cx_frac_quarter )};

                        cy_frac_half    = ( cluster_word >> 1 ) & 0x1;
                        cy_frac_quarter = (cluster_word)&0x1;
                        or_fy           = LHCb::Detector::VPChannelID::OrfyID{( cy_frac_half | cy_frac_quarter )};
                      }

                      const float local_x = devp.local_x( cx ) + fx * devp.x_pitch( cx );
                      const float local_y = ( to_unsigned( cy ) + 0.5 + fy ) * devp.pixel_size();

                      // sensor number set from 22nd bit for position in sensor pair
                      LHCb::Detector::VPChannelID::SensorID sensor;
                      float                                 gx, gy, gz;
                      if ( version == 2 || version == 3 ) {
                        sensor = sensor0;
                        gx     = ( ltg0[0] * local_x + ltg0[1] * local_y + ltg0[9] );
                        gy     = ( ltg0[3] * local_x + ltg0[4] * local_y + ltg0[10] );
                        gz     = ( ltg0[6] * local_x + ltg0[7] * local_y + ltg0[11] );

                      } else {
                        if ( ( ( cluster_word >> sensorID_shift ) & 0x1U ) ) {
                          sensor = sensor1;
                          gx     = ( ltg1[0] * local_x + ltg1[1] * local_y + ltg1[9] );
                          gy     = ( ltg1[3] * local_x + ltg1[4] * local_y + ltg1[10] );
                          gz     = ( ltg1[6] * local_x + ltg1[7] * local_y + ltg1[11] );
                        } else {
                          sensor = sensor0;
                          gx     = ( ltg0[0] * local_x + ltg0[1] * local_y + ltg0[9] );
                          gy     = ( ltg0[3] * local_x + ltg0[4] * local_y + ltg0[10] );
                          gz     = ( ltg0[6] * local_x + ltg0[7] * local_y + ltg0[11] );
                        }
                      }

                      const auto mod = to_unsigned( sensor ) / VP::NSensorsPerModule;
                      switch ( mod ) {
                      case 14:
                        nclusterMod14 += 1;
                        break;
                      case 15:
                        nclusterMod15 += 1;
                        break;
                      case 16:
                        nclusterMod16 += 1;
                        break;
                      default: /* nothing */;
                      }

                      if ( ( cluster_word >> isoBit_shift ) && ( ( cluster_word >> isoFlag_shift ) & 0x1 ) ) {
                        nclusterIsolated += 1;
                      }

                      if ( ( cluster_word >> isoBit_shift ) && !( ( cluster_word >> isoFlag_shift ) & 0x1 ) ) {
                        nclusterOverflow += 1;
                        switch ( mod ) {
                        case 14:
                          nclusterOverflowMod14 += 1;
                          break;
                        case 15:
                          nclusterOverflowMod15 += 1;
                          break;
                        case 16:
                          nclusterOverflowMod16 += 1;
                          break;
                        default: /* nothing */;
                        }
                      }

                      if ( !( cluster_word >> isoBit_shift ) ) { nclusterNeighbor += 1; }

                      if ( !( cluster_word >> isoBit_shift ) && ( ( cluster_word >> edgeFlag_shift ) & 0x1 ) ) {
                        nclusterEdge += 1;
                      }

                      if ( !( cluster_word >> isoBit_shift ) && !( ( cluster_word >> selfContFlag_shift ) & 0x1 ) ) {
                        nclusterNotCont += 1;
                      }

                      return {1, 1, gx, gy, gz, LHCb::Detector::VPChannelID{sensor, chip, ccol, cy, or_fx, or_fy}};
                    } );
    offsets[module] += ncluster;
  } // loop over all banks

  std::partial_sum( offsets.begin(), offsets.end(), offsets.begin() );

  // sorting in phi for even modules
  auto cmp_phi_for_odd_modules = []( const LHCb::VPLightCluster& a, const LHCb::VPLightCluster& b ) {
    return ( a.y() < 0.f && b.y() > 0.f ) ||
           // same y side even and odd modules, check y1/x1 < y2/x2
           ( ( a.y() * b.y() ) > 0.f && ( a.y() * b.x() < b.y() * a.x() ) );
  };

  // sorting in phi for odd modules
  auto cmp_phi_for_even_modules = []( const LHCb::VPLightCluster& a, const LHCb::VPLightCluster& b ) {
    return ( a.y() > 0.f && b.y() < 0.f ) ||
           // same y side even and odd modules, check y1/x1 < y2/x2
           ( ( a.y() * b.y() ) > 0.f && ( a.y() * b.x() < b.y() * a.x() ) );
  };

  auto sort_module = [clusters = std::ref( clusters ), offsets = std::ref( offsets )]( auto id, auto cmp ) {
    std::sort( clusters.get().begin() + offsets.get()[id], clusters.get().begin() + offsets.get()[id + 1], cmp );
  };

  for ( size_t moduleID = 0; moduleID < VeloInfo::Numbers::NModules; ++moduleID ) {
    // if( msgLevel(MSG::DEBUG)){
    //   debug()<<"Sorting hits in moduleID by phi, usign x,y information "<<moduleID<<endmsg;
    // }
    // In even modules you fall in the branching at -180, 180 degrees, you want to do that continuos
    if ( moduleID % 2 == 1 ) {
      sort_module( moduleID, cmp_phi_for_odd_modules );
    } else {
      sort_module( moduleID, cmp_phi_for_even_modules );
    }
  }

  m_nClusters += offsets.back();
  m_nClustersMod14 += nclusterMod14;
  m_nClustersMod15 += nclusterMod15;
  m_nClustersMod16 += nclusterMod16;
  m_nClustersIsolated += nclusterIsolated;
  m_nClustersOverflow += nclusterOverflow;
  m_nClustersOverflowMod14 += nclusterOverflowMod14;
  m_nClustersOverflowMod15 += nclusterOverflowMod15;
  m_nClustersOverflowMod16 += nclusterOverflowMod16;
  m_nClustersNeighbor += nclusterNeighbor;
  m_nClustersEdge += nclusterEdge;
  m_nClustersNotCont += nclusterNotCont;

  return result;
}
