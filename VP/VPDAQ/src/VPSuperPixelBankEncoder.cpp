/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/VP/VPChannelID.h"
#include "Event/RawEvent.h"
#include "Event/VPDigit.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/RndmGenerators.h"
#include "Kernel/VPConstants.h"
#include "VPDAQ/VPRetinaClusterConstants.h"
#include <algorithm>
#include <array>
#include <random>
#include <string>
#include <vector>

//-----------------------------------------------------------------------------
// Implementation file for class : VPSuperPixelBankEncoder
//
// 2014-03-06 : Karol Hennessy, Kurt Rinnert
//-----------------------------------------------------------------------------

/** @class VPSuperPixelBankEncoder
 * Algorithm to create Super Pixel Raw Bank from VP digits.
 *
 * There is one raw bank per sensor, that is the sensor number (0-207)
 * is the source ID of the bank.
 *
 * This is not the output of the TELL40, this provides input to VPRetinaSPMixer
 * only and should never appear in the MDF/DIGI files.
 *
 * The two sensors per bank output is arranged by VPRetinaSPMixer
 *
 * Each bank has a four byte word header, followed by a four byte
 * Super Pixel word for each Super Pixel on the sensor.
 *
 * The header word is currently simply the number of Super Pixels
 * on the sensor. There are plenty of bits available to encode
 * error conditions and such in the future.
 *
 * The Super Pixel word encoding is the following:
 * Following EDMS 2086526 v.3
 *
 * bit 0-7    Super Pixel Hitmap
 * bit 8-13   Super Pixel Row (0-63)
 * bit 14-22  Super Pixel Column (0-383)
 * bit 23-24  Tile ID on module (note EDMS doc does not define order here)
 * bit 25-30  UNUSED
 * bit 31     Super Pixel isolation flag
 *
 * Super Pixel Pattern bits encode columns and rows as follows:
 *
 * row,y
 *
 *  ^  37
 *  |  26
 *  |  15
 *  |  04
 *  +---> col,x
 *
 * @author Karol Hennessy
 * @author Kurt Rinnert
 * @date   2014-03-06
 */

class VPSuperPixelBankEncoder : public GaudiAlgorithm {

public:
  /// Standard constructor
  VPSuperPixelBankEncoder( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode initialize() override; ///< Algorithm initialization

private:
  /// where to get the digits from
  std::string m_digitLocation;
  /// where to write the clusters to
  std::string m_rawEventLocation;
  /// event counter
  unsigned int m_evt = 0;
  /// per sensor buffers of super pixel words
  std::array<std::vector<unsigned int>, VP::NSensors> m_spBySensor;
  // random number
  mutable Rndm::Numbers m_rndm;
};

namespace {
  // convert channelID to super pixel words
  struct sp_word {
    sp_word( LHCb::Detector::VPChannelID id ) noexcept {
      const unsigned int chip      = to_unsigned( id.chip() );
      const unsigned int row       = to_unsigned( id.row() );
      const unsigned int col       = to_unsigned( id.col() );
      const unsigned int sensorCol = col + 256 * chip;
      const unsigned int spCol     = sensorCol / 2;
      const unsigned int spRow     = row / 4;
      addr                         = ( ( spCol << 6 ) | spRow );
      ix                           = 0x1u << ( ( ( col % 2 ) * 4 ) + row % 4 );
      sensID                       = to_unsigned( id.sensor() ) % 2; // number of sensor in sensor pair (bit 23)
    }
    unsigned int addr;
    unsigned int ix;
    unsigned int sensID;
    unsigned int to_uint() const noexcept { return ( addr << 8 ) | ( sensID << 23 ) | ix; }
  };

} // namespace

DECLARE_COMPONENT( VPSuperPixelBankEncoder )

//=============================================================================
// Constructor
//=============================================================================
VPSuperPixelBankEncoder::VPSuperPixelBankEncoder( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {
  declareProperty( "DigitLocation", m_digitLocation = LHCb::VPDigitLocation::Default );
  declareProperty( "RawEventLocation", m_rawEventLocation = LHCb::RawEventLocation::Default );
}

StatusCode VPSuperPixelBankEncoder::initialize() {
  return GaudiAlgorithm::initialize().andThen( [&] { return m_rndm.initialize( randSvc(), Rndm::Flat( 0., 1. ) ); } );
}

//=============================================================================
//  Execution
//=============================================================================
StatusCode VPSuperPixelBankEncoder::execute() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  ++m_evt;
  const LHCb::VPDigits* digits = getIfExists<LHCb::VPDigits>( m_digitLocation );
  if ( !digits ) { return Error( " ==> There are no VPDigits in TES at " + m_digitLocation ); }

  // Check if RawEvent exists
  LHCb::RawEvent* rawEvent = getIfExists<LHCb::RawEvent>( m_rawEventLocation );
  if ( !rawEvent ) {
    // Create RawEvent
    rawEvent = new LHCb::RawEvent();
    put( rawEvent, m_rawEventLocation );
  }

  for ( auto& s : m_spBySensor ) {
    // initialize
    s = {};
  }

  // Loop over digits create super pixel words and store them.
  // No assumption about the order of digits is made.
  for ( const auto& seed : *digits ) {
    auto& sensor = m_spBySensor[to_unsigned( seed->channelID().sensor() )];
    auto  sp     = sp_word{seed->channelID()};

    auto j = std::find_if( sensor.begin(), sensor.end(),
                           [&]( unsigned int s ) { return ( ( s >> 8 ) & 0x7FFF ) == sp.addr; } );

    if ( j != sensor.end() ) {
      *j |= sp.ix;
    } else {
      sensor.push_back( sp.to_uint() );
    }
  }

  // make list of pairs of sensor data to combine, there are 208 sensors and 104 pairs
  std::array<std::pair<LHCb::span<unsigned int>, LHCb::span<unsigned int>>, 104> dataPairs;

  int total  = 0;
  int sensor = 0;
  for ( LHCb::span<unsigned int> data : m_spBySensor ) {

    total += data.size_bytes();
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "evt " << m_evt << "sensor " << sensor << " sp " << data.size() << endmsg;

    const unsigned int pos = sensor / 2; // integer division to get pair pos
    if ( pos > 103 ) {
      warning() << "Bad sensorID (" << sensor << ") should be 0->207" << endmsg;
      return StatusCode::FAILURE;
    }
    if ( ( sensor & 0x1U ) == 0x0U ) {
      // first sensor in pair [even number]
      dataPairs[pos].first = data;
    } else {
      // second sensor in pair
      dataPairs[pos].second = data;
    }
    sensor++;
  }

  uint32_t sensor0 = 0;
  uint32_t sensor1 = 1;
  for ( auto iPair : dataPairs ) {
    LHCb::span<const uint32_t> bank0 = {};
    LHCb::span<const uint32_t> bank1 = {};
    bank0                            = iPair.first;
    bank1                            = iPair.second;

    std::vector<uint32_t> mixedSP;
    unsigned int          pairBankSize = bank0.size() + bank1.size();
    mixedSP.reserve( pairBankSize );

    // copying words into the combined bank
    for ( auto isp_word = begin( bank0 ); isp_word != end( bank0 ); ++isp_word ) { mixedSP.push_back( ( *isp_word ) ); }

    // copying words into the combined bank
    for ( auto isp_word = begin( bank1 ); isp_word != end( bank1 ); ++isp_word ) { mixedSP.push_back( ( *isp_word ) ); }

    uint32_t n = pairBankSize;
    if ( n > 1 ) { // check there are at least 2 SPs
      for ( uint32_t i = 0; i < n; ++i ) {
        // pick random int from 0 to i inclusive
        uint32_t j = static_cast<int>( m_rndm.shoot() * ( i + 1 ) );
        std::swap( mixedSP[i], mixedSP[j] );
      }
    }

    // see comment above for bit definitions
    uint32_t bankID;
    if ( ( sensor0 & 0x4 ) == 0x0 ) { // c-side has even module numbers (bit 2 is first of module index)
      bankID = 0x1800;                // C-side
    } else {
      bankID = 0x1000; // A-side
    }
    bankID |= ( sensor0 >> 1 ); // left shift by 1 as final bit in each superpixel

    rawEvent->addBank( bankID, LHCb::RawBank::VP, VPRetinaCluster::c_SPBankVersion, mixedSP );

    if ( msgLevel( MSG::VERBOSE ) ) {
      verbose() << format( "Added bank with sourceID 0x%x containing sensors %i and %i", bankID, sensor0, sensor1 )
                << endmsg;
    }
    sensor0 += 2;
    sensor1 += 2;
  } // loop over all data pairs

  if ( msgLevel( MSG::DEBUG ) ) debug() << "total " << total << endmsg;

  return StatusCode::SUCCESS;
}
