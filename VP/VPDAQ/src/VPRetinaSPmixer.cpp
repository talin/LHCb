/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/VP/VPChannelID.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/VPLightCluster.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "Kernel/VPConstants.h"
#include "LHCbAlgs/Transformer.h"
#include "VPDAQ/VPRetinaClusterConstants.h"
#include "VPDet/DeVP.h"
#include "VPKernel/PixelUtils.h"
#include "VPKernel/VeloPixelInfo.h"
#include <algorithm>
#include <array>
#include <iomanip>
#include <iterator>
#include <random>
#include <tuple>
#include <vector>

/** @class VPRetinaSPmixer
 * Algorithm to mix SPs in raw banks.
 *
 * There is one raw bank per pair of sensors
 * See https://edms.cern.ch/document/2086526/3 for details
 * Each pair of sensors on a module side is merged.
 *
 * Each TELL40 (one module) outputs two streams for sensors 0,1 and 2,3
 *
 * The bank SourceID, for Merged SP banks, is
 *
 * bits 15..11 - TFC partition: VELOA = 00010, VELOC = 00011
 * bits 10..9  - Reserved, currently 00
 * bits  8..1  - VELO module number (0-51)
 * bit   0     - DataFlow ID (see below)
 *
 * The DataFlow ID * 2 + sensorID bit = sensor position in the module
 * Note the sensorID is in the SuperPixel word and mixed between the
 * two sensors in the bank.
 *
 * The full sensor index is then
 * bits 9..2 - Module ID (from SourceID bits 8..1)
 * bit  1    - DataFlow ID (from SourceID bit 0)
 * bit  0    - SensorID bit (from SP word bit 23) TODO: check this (note in EDMS not final)
 *
 * @author Federico Lazzari
 * @date   2018-06-20
 *
 */

class VPRetinaSPmixer
    : public LHCb::Algorithm::MultiTransformer<std::tuple<LHCb::RawEvent, LHCb::RawBank::View>(
          const LHCb::RawBank::View& )
                                               /* , LHCb::Algorithm::Traits::writeOnly<LHCb::RawEvent> */> {

public:
  /// Standard constructor
  VPRetinaSPmixer( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization

  /// Algorithm execution
  std::tuple<LHCb::RawEvent, LHCb::RawBank::View> operator()( const LHCb::RawBank::View& ) const override;

private:
  /// mix SP from bank
  std::vector<uint32_t> mixSP( LHCb::span<const uint32_t> bank ) const;

  // random number
  mutable Rndm::Numbers m_rndm;

  Gaudi::Property<unsigned int> m_icf_cut{
      this, "icf_cut", 144,
      "Maximum number of SPs per sensor pair and per event that the Isolation Cluster Flagging (ICF) can take as "
      "input. If the number of SPs is bigger than this value, SPs are not flagged."};
};

using namespace LHCb;

DECLARE_COMPONENT( VPRetinaSPmixer )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPRetinaSPmixer::VPRetinaSPmixer( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer{name,
                       pSvcLocator,
                       {"RawBanks", {}},
                       {{"RawEventLocationMixed", LHCb::RawEventLocation::VeloSPmixed}, {"MixedRawBanks", {}}}} {}

StatusCode VPRetinaSPmixer::initialize() {
  return MultiTransformer::initialize().andThen( [&] { return m_rndm.initialize( randSvc(), Rndm::Flat( 0., 1. ) ); } );
}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<LHCb::RawEvent, LHCb::RawBank::View> VPRetinaSPmixer::
                                                operator()( const LHCb::RawBank::View& rawBanks ) const {
  if ( rawBanks.empty() ) return {};
  if ( rawBanks[0]->type() != LHCb::RawBank::VP ) {
    throw GaudiException( "Wrong type of RawBank", __PRETTY_FUNCTION__, StatusCode::FAILURE );
  }

  const unsigned int version = rawBanks[0]->version();
  if ( version != 2 && version != VPRetinaCluster::c_SPBankVersion ) {
    warning() << "Unsupported raw bank version (" << version << ", only " << VPRetinaCluster::c_bankVersion
              << " compatible)" << endmsg;
    return {};
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Read " << rawBanks.size() << " raw banks from TES" << endmsg;

  unsigned int nBanks = 0;

  RawEvent rawEvent{};
  if ( version == 2 ) {

    // Loop over VP RawBanks
    for ( uint32_t i = 0; i < rawBanks.size(); i++ ) {

      const unsigned int               sensor = rawBanks[i]->sourceID();
      const LHCb::span<const uint32_t> bank = rawBanks[i]->range<uint32_t>();
      bool                             icf_over = false;

      if ( sensor % 2 == 0 ) {
        if ( bank[0] + rawBanks[i + 1]->range<uint32_t>()[0] > m_icf_cut ) { icf_over = true; }
      } else {
        if ( bank[0] + rawBanks[i - 1]->range<uint32_t>()[0] > m_icf_cut ) { icf_over = true; }
      }

      std::vector<uint32_t> mixedSP;
      mixedSP.reserve( bank[0] + 1 );

      for ( const uint32_t sp_word : bank ) {
        if ( icf_over ) {
          mixedSP.push_back( ( sp_word << 1 ) >> 1 );
        } else {
          mixedSP.push_back( sp_word );
        }
      }
      uint32_t n = mixedSP[0];
      if ( n > 1 ) { // check there are at least 2 SPs
        for ( uint32_t i = n - 1; i > 0; --i ) {
          // pick random int from 0 to i inclusive
          uint32_t j = static_cast<int>( m_rndm.shoot() * ( i + 1 ) );
          std::swap( mixedSP[i + 1], mixedSP[j + 1] );
        }
      }

      assert( mixedSP.size() == 1 + mixedSP[0] );

      rawEvent.addBank( sensor, LHCb::RawBank::VP, 2, mixedSP );

      ++nBanks;
    } // loop over all banks

  } else {

    // Loop over VP RawBanks
    for ( auto iterBank : rawBanks ) {
      rawEvent.addBank( iterBank->sourceID(), LHCb::RawBank::VP, VPRetinaCluster::c_SPBankVersion,
                        iterBank->range<uint32_t>() );
      ++nBanks;
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Added " << nBanks << " raw banks of retina clusters to TES" << endmsg;

  auto view = rawEvent.banks( RawBank::VP );
  return {std::move( rawEvent ), std::move( view )};
}
