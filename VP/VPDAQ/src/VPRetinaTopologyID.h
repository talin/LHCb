/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <cstdint>
#include <map>
#include <tuple>
#include <vector>

// pixel indices within 3x3 cluster topology
//
// 2 5 8
// 1 4 7
// 0 3 6

// pixel indices within 2x4 cluster topology
//
// 3 7
// 2 6
// 1 5
// 0 4

/** @class VPRetinaTopologyID VPRetinaTopologyID.h
 *  Class to encode/decode VELO Retina cluster topologies
 *
 *  @author Giovanni Bassi
 *  @date   2022-01-24
 */

class VPRetinaTopologyID final {
public:
  /// Standard constructor
  VPRetinaTopologyID();

  bool                           is3x3cluster( unsigned int topo3x3 );
  bool                           is2x4cluster( unsigned int topo2x4 );
  std::tuple<int, int>           frac3x3cluster( unsigned int topo3x3 );
  std::tuple<int, int>           frac2x4cluster( unsigned int topo2x4 );
  uint32_t                       equivalent_topo3x3( unsigned int topo3x3 );
  std::tuple<uint32_t, uint32_t> breakdown_topo2x4( unsigned int topo2x4 );
  std::vector<uint32_t>          topo3x3_to_topoIDfrac();
  std::vector<uint32_t>          topo2x4_to_topoIDfrac();
  uint32_t                       topoIDfrac_to_topo3x3( uint32_t sensor, uint32_t topoID, uint32_t fx, uint32_t fy );
  uint32_t                       topoIDfrac_to_topo2x4( uint32_t topoID, uint32_t fx, uint32_t fy );

private:
};
