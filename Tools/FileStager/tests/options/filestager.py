###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiConf import IOHelper
from Configurables import LHCbApp, EventSelector
from Configurables import ApplicationMgr, GaudiSequencer
from Configurables import HltRoutingBitsFilter, HltDecReportsDecoder
from Configurables import LHCb__UnpackRawEvent
from Configurables import LoKiSvc
from PRConfig.TestFileDB import test_file_db
from FileStager.Configuration import configureFileStager

input_file = test_file_db["HltDAQ-routingbits_full"].filenames[0]

configureFileStager()

# General configuration
app = LHCbApp()
app.EvtMax = -1
app.DataType = '2016'
app.CondDBtag = 'cond-20160517'
app.DDDBtag = 'dddb-20150724'

EventSelector().PrintFreq = 1000

topSeq = GaudiSequencer("TopSequence")

# Decode HltDecReports and HltRoutingBits
unpacker = LHCb__UnpackRawEvent(
    "UnpackRawEvent",
    RawBankLocations=[
        "DAQ/RawBanks/HltRoutingBits", "DAQ/RawBanks/HltDecReports"
    ],
    BankTypes=["HltRoutingBits", "HltDecReports"])

# Filter nanofied events if the file is HLT2 accepted
nano_filter = HltRoutingBitsFilter(
    'NonNanoFilter', RequireMask=(0x0, 0x0, 0x80000000))

topSeq.Members.append(unpacker)
topSeq.Members += [nano_filter]
for hltname in ["Hlt1", "Hlt2"]:
    topSeq.Members.append(
        HltDecReportsDecoder(
            name='HltDecReportsDecoder/' + hltname + 'DecReportsDecoder',
            #RawBanks=unpacker.RawBankLocations[1],
            SourceID=hltname,
            DecoderMapping="TCKANNSvc",
            OutputHltDecReportsLocation="/Event/" + hltname + "/DecReports"))

app = ApplicationMgr(TopAlg=[topSeq])

LoKiSvc().Welcome = False

IOHelper("MDF").inputFiles([input_file])
