###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
include(${CMAKE_CURRENT_LIST_DIR}/LHCbConfigUtils.cmake)

if(NOT COMMAND lhcb_find_package)
    # Look for LHCb find_package wrapper
    find_file(LHCbFindPackage_FILE LHCbFindPackage.cmake)
    if(LHCbFindPackage_FILE)
        include(${LHCbFindPackage_FILE})
    else()
        # if not found, use the standard find_package
        macro(lhcb_find_package)
            find_package(${ARGV})
        endmacro()
    endif()
endif()

# -- Public dependencies
lhcb_find_package(Gaudi REQUIRED)
# GaudiConfig.cmake does not set GAUDI_PROJECT_ROOT
if(NOT DEFINED GAUDI_PROJECT_ROOT AND Gaudi_DIR MATCHES "^(.*)/InstallArea/")
    set(GAUDI_PROJECT_ROOT "${CMAKE_MATCH_1}")
endif()

if(NOT COMMAND _gaudi_runtime_prepend)
    # _gaudi_runtime_prepend is not available in GaudiToolbox.cmake (introduced in Gaudi 36.1)
    macro(_gaudi_runtime_prepend runtime value)
        get_property(_orig_value TARGET target_runtime_paths PROPERTY runtime_${runtime})
        set_property(TARGET target_runtime_paths PROPERTY runtime_${runtime} ${value} ${_orig_value})
    endmacro()
endif()

# make sure DD4hep does not dictate the CXX standard to use
if(NOT DEFINED CMAKE_CXX_STANDARD AND DEFINED GAUDI_CXX_STANDARD)
    set(CMAKE_CXX_STANDARD ${GAUDI_CXX_STANDARD})
endif()

lhcb_find_package(Detector 1.0 REQUIRED)
if(Detector_PLUGINS_DIR)
    lhcb_env(PREPEND LD_LIBRARY_PATH ${Detector_PLUGINS_DIR})
    # we have to set ROOT_INCLUDE_PATH and PATH too (see https://gitlab.cern.ch/lhcb/Moore/-/issues/417)
    get_filename_component(prefix ${Detector_PLUGINS_DIR} DIRECTORY)
    lhcb_env(PREPEND ROOT_INCLUDE_PATH ${prefix}/include)
    lhcb_env(PREPEND PATH ${prefix}/bin)
endif()
if(NOT DETECTOR_PROJECT_ROOT AND Detector_DIR MATCHES "^(.*)/InstallArea/")
    set(DETECTOR_PROJECT_ROOT "${CMAKE_MATCH_1}")
endif()
# We need DETECTOR_PROJECT_ROOT in the build runtime env (for tests) and it
# does not go there automatically, https://gitlab.cern.ch/lhcb/LHCb/-/issues/167
lhcb_env(PRIVATE SET DETECTOR_PROJECT_ROOT ${DETECTOR_PROJECT_ROOT})

find_package(DD4hep REQUIRED DDCore DDCond)
if(NOT DD4hep_ROOT)
    message(FATAL_ERROR "find_package(DD4hep) did not set DD4hep_ROOT")
endif()
lhcb_env(PREPEND LD_LIBRARY_PATH ${DD4hep_ROOT}/lib)

# Disable hash randomization so that we have reproducible (test) runs.
# Ideally we shouldn't depend on hash() but that's not easy, see !3872 .
# See https://docs.python.org/3/using/cmdline.html#envvar-PYTHONHASHSEED
lhcb_env(SET PYTHONHASHSEED 0)

if(USE_DD4HEP)
    add_compile_definitions(USE_DD4HEP)
endif()

find_package(AIDA REQUIRED)
find_package(Boost 1.62 REQUIRED
    container
    date_time
    filesystem
    headers
    iostreams
    program_options
    regex
    serialization
    thread
)
find_package(CLHEP REQUIRED)
find_package(cppgsl REQUIRED)
find_package(Eigen3 REQUIRED)
find_package(fmt REQUIRED)
find_package(GSL REQUIRED)
find_package(HepMC REQUIRED)
find_package(nlohmann_json REQUIRED)
find_package(OpenSSL REQUIRED Crypto)  # for DetDescSvc
find_package(Python REQUIRED Interpreter Development)
find_package(Rangev3 REQUIRED)
find_package(ROOT 6.20 REQUIRED
    Core
    GenVector
    Hist
    MathCore
    RIO
)
find_package(TBB REQUIRED)
find_package(Vc 1.4.1 REQUIRED)
find_package(VDT REQUIRED)
find_package(XercesC REQUIRED)
find_package(yaml-cpp REQUIRED)

find_package(PkgConfig)
pkg_check_modules(git2 libgit2 REQUIRED IMPORTED_TARGET)  # for GitEntityResolver
pkg_check_modules(zmq libzmq REQUIRED IMPORTED_TARGET)  # for ZeroMQ
pkg_check_modules(sodium libsodium REQUIRED IMPORTED_TARGET)  # for ZeroMQ

find_data_package(FieldMap REQUIRED)
find_data_package(ParamFiles REQUIRED)
find_data_package(PRConfig REQUIRED)
find_data_package(RawEventFormat REQUIRED)
find_data_package(TCK/HltTCK REQUIRED)

# -- Private dependencies
if(WITH_LHCb_PRIVATE_DEPENDENCIES)
    if(BUILD_TESTING)
        find_package(Boost 1.62 REQUIRED unit_test_framework)
        find_package(ROOT 6.20 REQUIRED Tree)
    endif()
endif()
