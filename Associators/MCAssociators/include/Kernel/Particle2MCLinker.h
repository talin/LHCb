/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Linker/LinkedFrom.h"
#include "Linker/LinkedTo.h"

#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Kernel/MCAssociation.h"
#include "Kernel/Particle2MCMethod.h"

#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IAlgManager.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/KeyedObject.h"

/**
 *  Class providing association functionality to MCParticles
 *
 *  template parameter PARENT will typically be a tool or an Algorithm
 *
 *  @author Philippe Charpentier
 *  @date   2004-04-29
 */
template <typename SOURCE, typename PARENT = GaudiAlgorithm>
class Object2MCLinker {
  // Typedef for RangeFrom return type
  using ToRange =
      decltype( std::declval<LinkedTo<LHCb::MCParticle>>().weightedRange( static_cast<SOURCE*>( nullptr ) ) );

public:
  // Typedef for source type
  using Source = SOURCE;

  // Constructors from Algorithm
  Object2MCLinker( const PARENT* myMother, const std::string& algType, const std::string& extension,
                   std::vector<std::string> containerList )
      : m_parent( myMother )
      , m_extension( extension )
      , m_linkerAlgType( algType )
      , m_containerList( std::move( containerList ) ) {}

  Object2MCLinker( const PARENT* myMother ) : m_parent( myMother ) {}

  Object2MCLinker( const PARENT* myMother, const int method, std::vector<std::string> containerList )
      : Object2MCLinker( myMother, Particle2MCMethod::algType[method], Particle2MCMethod::extension[method],
                         std::move( containerList ) ) {}

  Object2MCLinker( const PARENT* myMother, const int method, const std::string& container )
      : Object2MCLinker( myMother, method, std::vector<std::string>( 1, container ) ) {}

  Object2MCLinker( const PARENT* myMother, const std::string& algType, const std::string& extension,
                   const std::string& container )
      : Object2MCLinker( myMother, algType, extension, std::vector<std::string>( 1, container ) ) {}

  StatusCode setAlgorithm( const int method, std::vector<std::string> containerList );

  StatusCode setAlgorithm( const int method, const std::string& container ) {
    return setAlgorithm( method, std::vector<std::string>( 1, container ) );
  }

  StatusCode setAlgorithm( const std::string& algType, const std::string& extension,
                           std::vector<std::string> containerList );

  StatusCode setAlgorithm( const std::string& algType, const std::string& extension, const std::string& container ) {
    return setAlgorithm( algType, extension, std::vector<std::string>( 1, container ) );
  }

  // return a weighted range which can be used as eg.
  //  for ( const auto& [ mc_particle, weight ] : linker.range( particle ) ) {
  //      ...
  //  }
  // Note that the 'value type' of (the items in ) the range is a `std::tuple<LHCb::MCParticle const&, double>`
  ToRange range( const SOURCE* part );

  int associatedMCP( const SOURCE* obj ) {
    auto r = range( obj );
    return std::distance( r.begin(), r.end() );
  }

  bool checkAssociation( const SOURCE* obj, const LHCb::MCParticle* mcPart );

private:
  const std::string& name() const { return m_parent->name(); }
  const std::string& context() const { return m_parent->context(); }

protected:
  MsgStream& debug() { return m_parent->debug(); }
  MsgStream& error() { return m_parent->error(); }
  MsgStream& warning() { return m_parent->warning(); }

private:
  ISvcLocator* svcLocator() const {
    if constexpr ( std::is_base_of_v<AlgTool, PARENT> ) {
      return m_parent->svcLoc();
    } else { // Algorithm
      return m_parent->svcLoc().get();
    }
  }

  const PARENT* m_parent;

protected:
  std::string m_extension;
  std::string m_linkerAlgType;

private:
  IAlgorithm* m_linkerAlg = nullptr;

protected:
  std::vector<std::string> m_containerList{};

private:
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_noAlgTypeErr{m_parent, "No alg type given", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_noIAlgMgrErr{m_parent, "Could not locate IAlgManager", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_algoCreationErr{m_parent, "Could not create algorithm", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_setContextErr{m_parent, "Unable to set Property Context", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_algInitErr{m_parent, "Error in algorithm initialization!", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_setOutTableErr{m_parent, "Unable to set Property OutputTable",
                                                                       10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_setInputErr{m_parent, "Unable to set Property InputData", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_setRITErr{m_parent, "Unable to set Property RootInTES", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_ipropWarn{m_parent, "Unable to get IProperty pointer", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_executing_linker_builder_algo{
      m_parent, "Explicitly Executing Linker Building algorithm", 0};

  LHCb::LinksByKey const* m_links = nullptr;

  // Private methods
protected:
  bool              hasValidParent() const { return m_parent != nullptr; }
  IDataProviderSvc* evtSvc() const { return m_parent->evtSvc(); }
  void              createLinks( const std::string& contName );

private:
  StatusCode locateAlgorithm( const std::string& algType, const std::string& algName, IAlgorithm*& alg,
                              const std::vector<std::string>& inputData );
  StatusCode setAlgInputData( IAlgorithm*& alg, const std::vector<std::string>& inputData );

  std::string containerName( const ContainedObject* obj ) const {
    return ( obj->parent() && obj->parent()->registry() ) ? obj->parent()->registry()->identifier() : std::string{};
  }
};

template <typename OBJ2MCP, typename PARENT = GaudiAlgorithm>
class Object2FromMC : public Object2MCLinker<OBJ2MCP, PARENT> {

  std::vector<LHCb::LinksByKey const*> m_linkerList;

public:
  /// Standard constructor
  template <typename... Args>
  Object2FromMC( Args&&... args ) : Object2MCLinker<OBJ2MCP, PARENT>::Object2MCLinker{std::forward<Args>( args )...} {
    if ( !this->hasValidParent() || this->m_linkerAlgType.empty() ) return;
    m_linkerList.reserve( this->m_containerList.size() );
    for ( const auto& cont : this->m_containerList ) {
      const std::string name = LHCb::LinksByKey::linkerName( cont + this->m_extension );
      this->debug() << "trying to retrieve " << name << endmsg;
      SmartDataPtr<LHCb::LinksByKey> links( this->evtSvc(), name );
      if ( !links ) {
        this->createLinks( cont );
        links = SmartDataPtr<LHCb::LinksByKey>( this->evtSvc(), name );
        if ( !links )
          this->warning() << "failed to retrieve " << name << " after invoking createLinks(" << cont << ")" << endmsg;
      }
      if ( links ) m_linkerList.emplace_back( links );
    }
  }

  bool isAssociated( const KeyedObject<int>* obj ) {
    const OBJ2MCP* part = dynamic_cast<const OBJ2MCP*>( obj );
    if ( part ) return !range( part ).empty();
    const LHCb::MCParticle* mcPart = dynamic_cast<const LHCb::MCParticle*>( obj );
    return mcPart && !range( mcPart ).empty();
  }

  using Object2MCLinker<OBJ2MCP, PARENT>::range;
  auto range( const LHCb::MCParticle* mcPart ) const {
    class InverseRange {
      LHCb::span<LHCb::LinksByKey const* const> m_linkerList;
      const LHCb::MCParticle*                   m_part = nullptr;
      class Sentinel {};
      class Iterator {
        using InnerRange =
            decltype( LinkedFrom<OBJ2MCP>{nullptr}.weightedRange( static_cast<LHCb::MCParticle*>( nullptr ) ) );
        using InnerIter = decltype( std::declval<InnerRange>().begin() );
        LHCb::span<LHCb::LinksByKey const* const> m_linkerList;
        InnerRange m_range = LinkedFrom<OBJ2MCP>{nullptr}.weightedRange( static_cast<LHCb::MCParticle*>( nullptr ) );
        InnerIter  m_current =
            LinkedFrom<OBJ2MCP>{nullptr}.weightedRange( static_cast<LHCb::MCParticle*>( nullptr ) ).begin();
        const LHCb::MCParticle* m_part = nullptr;

      public:
        Iterator( LHCb::span<LHCb::LinksByKey const* const> linkers, const LHCb::MCParticle* p )
            : m_linkerList{linkers}, m_part{p} {
          for ( ; m_range.empty() && !m_linkerList.empty(); m_linkerList = m_linkerList.subspan<1>() ) {
            m_range = LinkedFrom<OBJ2MCP>{m_linkerList.front()}.weightedRange( m_part );
          }
          m_current = m_range.begin();
        }
        Iterator& operator++() {
          if ( m_current != m_range.end() ) {
            ++m_current;
          } else if ( !m_linkerList.empty() ) {
            *this = Iterator{m_linkerList.subspan<1>(), m_part};
          }
          return *this;
        }
        decltype( auto ) operator*() const { return *m_current; }
        bool             operator!=( Sentinel ) const { return !m_linkerList.empty(); }
      };

    public:
      InverseRange( LHCb::span<LHCb::LinksByKey const* const> links, const LHCb::MCParticle* p )
          : m_linkerList{links}, m_part{p} {}
      Iterator         begin() const { return {m_linkerList, m_part}; }
      Sentinel         end() const { return {}; }
      bool             empty() const { return !( begin() != end() ); }
      decltype( auto ) front() const { return *begin(); }
      auto const*      try_front() const {
        auto i = begin();
        return i != end() ? &std::get<0>( *i ) : nullptr;
      }
    };

    return InverseRange{this->m_linkerList, mcPart};
  }
};

#include "Particle2MCLinker.icpp"

/** Linker type for associations between ProtoParticles and MCParticles
 *
 *  @author Philippe Charpentier
 *  @date   2004-04-29
 */
using ProtoParticle2MCLinker = Object2FromMC<LHCb::ProtoParticle>;

/** Linker type for associations between Particles and MCParticles
 *
 *  @author Philippe Charpentier
 *  @date   2004-04-29
 */
using Particle2MCLinker = Object2FromMC<LHCb::Particle>;
