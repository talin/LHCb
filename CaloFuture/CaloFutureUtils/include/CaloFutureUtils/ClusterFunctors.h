/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureDataFunctor.h"
#include "Event/CaloCluster.h"
#include "Event/CaloClusters_v2.h"

/** @namespace ClusterFunctors ClusterFunctors.h CaloFutureUtils/ClusterFunctors.h
 *
 *  collection of useful functors for dealing with
 *  CaloCluster objects
 *
 *  @author Ivan Belyaev
 *  @date   04/07/2001
 */
namespace LHCb {
  namespace Calo::Functor::Cluster {

    /** @fn void throwException( const std::string& message )
     *  throw the exception
     *  @exception CaloException
     *  @param message exception message
     *  @return status code (fictive)
     */
    [[noreturn]] void throwException( const std::string& message );

    /** Calculate the "energy" of the cluster
     *  as a sum of energies of its digits, weighted with energy fractions
     *  @param   cl  pointer to cluster
     *  @return      "energy" of cluster
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   xx/xx/xxxx
     */
    float energy( const CaloCluster* cl );

    /** Useful function to determine, if clusters have at least one common cell.
     *
     *  For invalid arguments return "false"
     *
     *  @param   cl1   pointer to first  cluster
     *  @param   cl2   pointer to second cluster
     *  @return "true" if clusters have at least 1 common cell
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   xx/xx/xxxx
     */
    bool overlapped( const CaloCluster* cl1, const CaloCluster* cl2 );

    /** Useful function to find first common digit from two sequences.
     *  It returns the pair of
     *  iterators (first one for first sequence and
     *  the secons one for second sequences).
     *
     *  "IT" could be either iterator or const_iterator
     *
     *  @param  begin1 iterator pointing to 1st    element of 1st sequence
     *  @param  end1   iterator pointing to last+1 element of 1st sequence
     *  @param  begin2 iterator pointing to 1st    element of 2nd sequence
     *  @param  end2   iterator pointing to last+1 element of 2nd sequence
     *  @return pair of iterators
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   xx/xx/xxxx
     */
    template <class IT>
    decltype( auto ) commonDigit( IT begin1, IT end1, IT begin2, IT end2 ) {
      return clusterCommonDigit( begin1, end1, begin2, end2 );
    }

    /** Useful function to locate the digit within the sequence
     *
     *  "IT" could be either iterator or const_iterator
     *
     *  @param begin iterator pointing to the 1st    element of sequence
     *  @param end   iterator pointing to teh last+1 element of sequence
     *  @param digit pointer to CaloDigit
     *  @return location of digit within the sequence
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     */
    template <class IT>
    decltype( auto ) locateDigit( IT begin, IT end, const CaloDigit* digit ) {
      return clusterLocateDigit( begin, end, digit );
    }

    /** Locate the digit with given status from sequence of digits
     *
     *  "IT" could be either iterator or const_iterator
     *
     *  @param begin  iterator pointing to the 1st    element of sequence
     *  @param end    iterator pointing to teh last+1 element of sequence
     *  @param st     status
     *  @return location of digit within the sequence
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     */
    template <class IT>
    decltype( auto ) locateDigit( IT begin, IT end, CaloDigitStatus::Mask st ) {
      return clusterLocateDigit( begin, end, st );
    }

    /** The simple class/function to get the index of area in Calo
     *  "calo-area" of cluster is defined as "calo-area" index of seed cell
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    class ClusterArea final {
    public:
      /** the only one essential method
       *  @exception CaloException for invalid cluster
       *  @param cluster pointer to CaloFutureCluster object
       *  @return index of calorimeter area for given cluster
       */
      decltype( auto ) operator()( const CaloCluster* cluster ) const {
        if ( !cluster ) { Exception( " CaloCluster* points to NULL! " ); }
        // find seed cell
        auto seed =
            locateDigit( cluster->entries().begin(), cluster->entries().end(), CaloDigitStatus::Mask::SeedCell );
        if ( cluster->entries().end() == seed ) { Exception( " 'SeedCell' is not found!" ); }
        const CaloDigit* digit = seed->digit();
        if ( !digit ) { Exception( " CaloDigit* points to NULL for seed!" ); }
        // get the area
        return digit->cellID().area();
      }

    protected:
      /** throw the exception
       *  @see CaloException
       *  @exception CaloException
       *  @param     message exception message
       *  @return    status code (fictive)
       */
      [[noreturn]] void Exception( const std::string& message ) const { throwException( "ClusterArea() " + message ); }
    };

    /** The simple class/function to get the index of area in Calo
     *  "calo-area" of cluster is defined as "calo-area" index of seed cell
     *
     *  @see ClusterArea
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    inline decltype( auto ) clusterArea( const CaloCluster* cluster ) {
      ClusterArea evaluator;
      return evaluator( cluster );
    }

    /** @class ClusterCaloFuture
     *
     *  The simple class/function to get the index of calorimeter.
     *  Index of calorimeter for clusers
     *  is defined as "calo" index of the seed cell
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    class ClusterCaloFuture final {
    public:
      /** the only one essential method
       *  @exception CaloException for invalid cluster
       *  @param cluster pointer to CaloCluster object
       *  @return index of calorimeter area for given cluster
       */
      auto operator()( const CaloCluster* cluster ) const {
        if ( !cluster ) { Exception( " CaloCluster* points to NULL! " ); }
        // find seed cell
        auto seed =
            locateDigit( cluster->entries().begin(), cluster->entries().end(), CaloDigitStatus::Mask::SeedCell );
        if ( cluster->entries().end() == seed ) { Exception( " 'SeedCell' is not found!" ); }
        const CaloDigit* digit = seed->digit();
        if ( !digit ) { Exception( " CaloDigit* points to NULL for seed!" ); }
        // get the area
        return digit->cellID().calo();
      }

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
      auto operator()( Event::Calo::Clusters::const_reference<simd, behaviour> cluster ) const {
        return cluster.cellID().calo();
      }

    protected:
      /** throw the exception
       *  @see CaloException
       *  @exception CaloException
       *  @param     message exception message
       *  @return    status code (fictive)
       */
      [[noreturn]] void Exception( const std::string& message ) const {
        throwException( "ClusterCaloFuture() " + message );
      }
    };

    /** The simple class/function to get the index of calorimeter.
     *  Index of calorimeter for clusers
     *  is defined as "calo" index of the seed cell
     *
     *  @see ClusterArea
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    inline decltype( auto ) clusterCaloFuture( const CaloCluster* cluster ) {
      ClusterCaloFuture evaluator;
      return evaluator( cluster );
    }

    /** @class  ClusterFromCaloFuture
     *
     *  simple predicate/functor to select cluster from given calorimeter
     *
     *  @see ClusterCaloFuture
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    class ClusterFromCaloFuture final {
    public:
      /** constructor
       *  @see         Calo::CellCode
       *  @exception   CaloException for invalid calorimeter name
       *  @param calo  name of calorimeter (full or abbreviated)
       */
      ClusterFromCaloFuture( Detector::Calo::CellCode::Index calo ) : m_calo{calo} {
        if ( m_calo < 0 ) { Exception( "Wrong Calo Index='" + toString( calo ) + "'" ); }
      }
      /** the only one essential method
       *  @see ClusterCaloFuture
       *  @exception CaloException fro ClusterCaloFuture class
       *  @param cluster pointer to CaloFutureCluster object
       *  @return true if cluster belongs to tehselected calorimter
       */
      template <typename T>
      bool operator()( T&& cluster ) const {
        return m_evaluator( std::forward<T>( cluster ) ) == m_calo;
      }

    protected:
      /** throw the exception
       *  @see CaloException
       *  @exception CaloException
       *  @param     message exception message
       *  @return    status code (fictive)
       */
      [[noreturn]] void Exception( const std::string& message ) const {
        throwException( "ClusterFromCaloFuture() " + message );
      };

    private:
      Detector::Calo::CellCode::Index m_calo = Detector::Calo::CellCode::Index::Undefined;
      ClusterCaloFuture               m_evaluator;
    };

    /** @class  ClusterFromArea
     *
     *  simple predicate/functor to select cluster from given
     *  area in calorimeter
     *
     *  @see ClusterArea
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    class ClusterFromArea final {
    public:
      /** constructor
       *  @param area area index
       */
      ClusterFromArea( unsigned int area ) : m_area( area ) {}
      /** the only one essential method
       *  @see ClusterArea
       *  @exception CaloException from ClusterArea class
       *  @param cluster pointer to CaloCluster object
       *  @return true if cluster belongs to the selected area in calorimter
       */
      bool operator()( const CaloCluster* cluster ) const { return m_evaluator( cluster ) == m_area; }

    private:
      unsigned int m_area{};
      ClusterArea  m_evaluator{};
    };

    /** @class OnTheBoundary
     *
     *  Simple utility to locate clusters, placed on the boundaries
     *  between different calorimeter zones
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    class OnTheBoundary final {
    public:
      /** the only one essential method
       *  @param cluster pointer to CaloCluster object
       *  @return true if cluster is on the boundary,
       *               for empty clusters "false" is returned
       *  @exception CaloException for invalid clusters
       */
      bool operator()( const CaloCluster* cluster ) const {
        if ( !cluster ) { Exception( "CaloCluster* points to NULL!" ); }
        const auto& entries = cluster->entries();
        if ( entries.size() <= 1 ) { return false; } // RETURN !!!
        auto seed = locateDigit( entries.begin(), entries.end(), CaloDigitStatus::Mask::SeedCell );
        if ( entries.end() == seed ) { Exception( "'SeedCell' is not found!" ); }
        const CaloDigit* sd = seed->digit();
        if ( !sd ) { Exception( "CaloDigit* for 'SeedCell' is  NULL!" ); }
        const auto seedArea = sd->cellID().area();
        for ( auto entry = entries.begin(); entries.end() != entry; ++entry ) {
          const CaloDigit* digit = entry->digit();
          if ( !digit ) { continue; }
          if ( seedArea != digit->cellID().area() ) { return true; }
        } // end of loop over all cluyster entries
        //
        return false;
      }

    protected:
      /** throw the exception
       *  @see CaloException
       *  @exception CaloException
       *  @param     message exception message
       *  @return    status code (fictive)
       */
      [[noreturn]] void Exception( const std::string& message ) const {
        throwException( "OnTheBoundary() " + message );
      }
    };

    /** Simple utility to locate clusters, placed on the boundaries
     *  between different calorimeter zones
     *
     *  @see OnTheBoundary
     *  @param cluster pointer to CaloCluster object
     *  @return true if cluster is on the boundary,
     *               for empty clusters "false" is returned
     *  @exception CaloException for invalid clusters
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    inline decltype( auto ) onTheBoundary( const CaloCluster* cluster ) {
      OnTheBoundary evaluator;
      return evaluator( cluster );
    }

    /** @class ZPosition
     *
     *  The simple function to get the cluster z-posiiton as a z-position of
     *  "Seed Cell"
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    class ZPosition final {
    public:
      /** the explicit constructor
       *  @param detector source of calorimeter detector information
       */
      ZPosition( const DeCalorimeter* detector ) : m_detector( detector ) {
        if ( !m_detector ) { Exception( " DeCalorimeter*     points to NULL! " ); }
      }

      /** the only one essential method
       *  @exception CaloException if detector is not valid
       *  @exception CaloException if cluster is NULL
       *  @exception CaloException if cluster is Empty
       *  @exception CaloException if cluster has no SEED cell
       *  @exception CaloException if SEED digit is NULL
       *  @param cluster pointer to CaloCluster object
       *  @return z-position
       */
      auto operator()( const CaloCluster* cluster ) const {
        if ( !cluster ) { Exception( " const CaloCluster* points to NULL! " ); }
        if ( cluster->entries().empty() ) { Exception( " CaloCluster is empty! " ); }
        auto iseed =
            locateDigit( cluster->entries().begin(), cluster->entries().end(), CaloDigitStatus::Mask::SeedCell );
        if ( cluster->entries().end() == iseed ) { Exception( " The Seed Cell is not found! " ); }
        const CaloDigit* seed = iseed->digit();
        if ( !seed ) { Exception( " The Seed Digit points to NULL! " ); }
        return m_detector->cellCenter( seed->cellID() ).z();
      }

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
      auto operator()( Event::Calo::Clusters::reference<simd, behaviour> cluster ) const {
        return m_detector->cellCenter( cluster->cellID() ).z();
      }

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
      auto operator()( Event::Calo::Clusters::const_reference<simd, behaviour> cluster ) const {
        return m_detector->cellCenter( cluster->cellID() ).z();
      }

    protected:
      /** throw the exception
       *  @see CaloException
       *  @exception CaloException
       *  @param     message exception message
       *  @return    status code (fictive)
       */
      [[noreturn]] void Exception( const std::string& message ) const { throwException( "ZPosition() " + message ); }

    private:
      const DeCalorimeter* m_detector = nullptr; ///< source of geometry information
    };

    /** Helpful function to tag the sub cluster according to the
     *  fraction evaluated by "evaluator"
     *
     *  Error codes
     *
     *          - 225 : CaloCluster* points to NULL
     *          - 226 : Entry with status 'SeedCell' is not found
     *          - 227 : Entry with status 'SeedCell' is invalid
     *
     *
     *  @param cluster pointer to CaloCluster object
     *  @param evaluator  evaluator object
     *  @param modify  flag for modification of energy fractions
     *  @param tag     tag to be set for cells with modified fractions
     *
     *  @author Vanya Belyaev Ivan Belyaev
     *  @date   01/04/2002
     */

    template <class EVALUATOR, SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
    StatusCode tagTheSubCluster( Event::Calo::Clusters::Entries<simd, behaviour> entries, const EVALUATOR& evaluator,
                                 bool modify, CaloDigitStatus::Status status, CaloDigitStatus::Status tag ) {
      // find seed digit
      auto seedEntry = locateDigit( entries.begin(), entries.end(), CaloDigitStatus::Mask::SeedCell );
      // check the seed
      if ( entries.end() == seedEntry ) { return StatusCode( 226 ); }
      // loop over all entries

      for ( auto entry : entries ) {
        // reset existing statuses
        entry.removeStatus( tag );
        entry.removeStatus( status );

        // evaluate the fraction
        const auto fraction = evaluator( ( *seedEntry ).cellID(), entry.cellID() );
        if ( 0 >= fraction ) { continue; }
        // update statuses
        entry.addStatus( status );

        if ( !modify ) { continue; }
        // modify the fractions
        entry.setFraction( entry.fraction() * fraction );
        entry.addStatus( tag );
      }
      ///
      return StatusCode::SUCCESS;
    }

    /** Helpful function to untag the sub cluster according to the
     *  fraction evaluated by "evaluator"
     *
     *  Error codes
     *
     *          - 225 : CaloCluster* points to NULL
     *          - 226 : Entry with status 'SeedCell' is not found
     *          - 227 : Entry with status 'SeedCell' is invalid
     *
     *
     *  @param cluster pointer to CaloCluster object
     *  @param evaluator  evaluator object
     *  @param tag     tag to be set for cells with modified fractions
     *
     *  @see CaloCuster
     *  @see tagTheSubCluster
     *
     *  @author Vanya Belyaev Ivan Belyaev
     *  @date   01/04/2002
     */

    template <class EVALUATOR, SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
    StatusCode untagTheSubCluster( Event::Calo::Clusters::Entries<simd, behaviour> entries, const EVALUATOR& evaluator,
                                   const CaloDigitStatus::Status tag ) {
      // find seed digit
      auto seedEntry = locateDigit( entries.begin(), entries.end(), CaloDigitStatus::Mask::SeedCell );
      // check the seed
      if ( entries.end() == seedEntry ) { return StatusCode( 226 ); }
      // loop over all entries
      for ( auto entry : entries ) {
        // reset existing statuses
        entry.addStatus( {CaloDigitStatus::Mask::UseForEnergy, CaloDigitStatus::Mask::UseForPosition,
                          CaloDigitStatus::Mask::UseForCovariance} );
        // tagged ?
        if ( !entry.status().anyOf( tag ) ) continue; // CONTINUE
        // evaluate the fraction
        const auto fraction = evaluator( ( *seedEntry ).cellID(), entry.cellID() );
        if ( 0 >= fraction ) { continue; } // CONTINUE
        // modify the fractions
        entry.setFraction( entry.fraction() / fraction );
        entry.removeStatus( tag );
      }
      //
      return StatusCode::SUCCESS;
    }

  } // namespace Calo::Functor::Cluster

  // backwards compatibility
  namespace ClusterFunctors = Calo::Functor::Cluster;

} // namespace LHCb

// ============================================================================
