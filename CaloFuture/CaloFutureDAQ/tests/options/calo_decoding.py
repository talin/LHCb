###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.application import (configure_input, configure, ApplicationOptions,
                                default_raw_event, default_raw_banks,
                                force_location)
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import CaloFutureRawToDigits
from DDDB.CheckDD4Hep import UseDD4Hep

options = ApplicationOptions(_enabled=False)
config = configure_input(options)

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    config.add(DD4hepSvc(DetectorList=["/world", "Ecal", "Hcal"]))


def make_calo_digits(raw, det):
    adc_alg = CaloFutureRawToDigits(
        name='Future{}ZSup'.format(det),
        RawBanks=default_raw_banks("Calo", raw),
        ErrorRawBanks=default_raw_banks("CaloError", raw),
        outputs={
            'OutputDigitData': force_location('Raw/{}/Digits'.format(det)),
            'OutputReadoutStatusData': None
        })
    return adc_alg.OutputDigitData


decoders = [
    make_calo_digits(default_raw_event, det) for det in ['Ecal', 'Hcal']
]

cf_node = CompositeNode(
    "calo_decoding",
    decoders,
    combine_logic=NodeLogic.LAZY_AND,
    force_order=True)

config.update(configure(options, cf_node))
