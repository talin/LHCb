/*****************************************************************************\
* (c) Copyright 2018-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Detector/Calo/CaloCellID.h"
#include "Event/CaloDigits_v2.h"
#include "Event/RawBank.h"
#include "Event/RawBankReadoutStatus.h"
#include "Gaudi/Accumulators.h"
#include "LHCbAlgs/Transformer.h"
#include "boost/container/small_vector.hpp"
#include <Gaudi/Accumulators/Histogram.h>
#include <fmt/format.h>

namespace {

  enum class zsupMethod_t { none, one_d, two_d };

  const char* toString( const zsupMethod_t& out ) {
    switch ( out ) {
    case zsupMethod_t::none:
      return "NO";
    case zsupMethod_t::one_d:
      return "1D";
    case zsupMethod_t::two_d:
      return "2D";
    }
    throw "IMPOSSIBLE!";
  }
  std::ostream& toStream( zsupMethod_t out, std::ostream& os ) { return os << std::quoted( toString( out ), '\'' ); }
  // std::ostream& operator<<( std::ostream& os, zsupMethod_t out ) { return toStream( out, os ); }

  StatusCode parse( zsupMethod_t& result, std::string_view input ) {
    if ( ( input.front() == '"' || input.back() == '\'' ) && input.front() == input.back() ) {
      input.remove_prefix( 1 );
      input.remove_suffix( 1 );
    }
    for ( zsupMethod_t m : {zsupMethod_t::none, zsupMethod_t::one_d, zsupMethod_t::two_d} ) {
      if ( input == toString( m ) ) {
        result = m;
        return StatusCode::SUCCESS;
      }
    }
    return StatusCode::FAILURE;
  }

  constexpr auto ctrl( unsigned int word ) { return ( word >> 23 ) & 0x1FF; }
  constexpr auto lenTrig( unsigned int word ) { return word & 0x7F; }
  constexpr auto lenADC( unsigned int word ) { return ( word >> 7 ) & 0x7F; }
  constexpr auto code( unsigned int word ) { return ( word >> 14 ) & 0x1FF; }

  const char* label( LHCb::RawBank::BankType b ) {
    assert( b == LHCb::RawBank::EcalE || b == LHCb::RawBank::EcalPacked || b == LHCb::RawBank::HcalE ||
            b == LHCb::RawBank::HcalPacked || b == LHCb::RawBank::Calo );
    switch ( b ) {
    case LHCb::RawBank::EcalE:
    case LHCb::RawBank::HcalE:
      return "short";
    case LHCb::RawBank::EcalPacked:
    case LHCb::RawBank::HcalPacked:
      return "packed";
    case LHCb::RawBank::Calo:
      return "LHCb::RawBank::Calo";
    default:
      throw std::invalid_argument( "label: bad BankType" );
    }
  }

  template <typename T, auto N>
  constexpr auto pop( gsl::span<T, N>& v, bool isLittleEndian = true ) {
    auto first = v[0];
    v          = v.template subspan<1>();
    return isLittleEndian ? first : __builtin_bswap32( first );
  }
  template <typename T, auto N>
  constexpr void advance( gsl::span<T, N>& v, int s ) {
    v = v.subspan( s );
  }

} // namespace

namespace LHCb::Calo {

  class RawToDigits
      : public Algorithm::MultiTransformer<std::tuple<Event::Calo::Digits, RawBankReadoutStatus>(
                                               const LHCb::RawBank::View& banks, const LHCb::RawBank::View& error_banks,
                                               const DeCalorimeter& ),
                                           Algorithm::Traits::usesConditions<DeCalorimeter>> {

  public:
    RawToDigits( const std::string& name, ISvcLocator* pSvcLocator );
    std::tuple<Event::Calo::Digits, RawBankReadoutStatus>
    operator()( const LHCb::RawBank::View&, const LHCb::RawBank::View&, const DeCalorimeter& ) const override;

  private:
    struct Adc {
      Adc( Detector::Calo::CellID id, int adc ) noexcept : id{id}, adc{adc} {}
      Detector::Calo::CellID id;
      int                    adc;
    };
    enum class Decode { PinData, Cell };

    template <Decode>
    std::vector<Adc> decode_( const RawBank&, RawBankReadoutStatus&, const DeCalorimeter& ) const;
    template <Decode>
    std::vector<Adc> decode_v10( int sourceID, span<const unsigned int>, RawBankReadoutStatus&, const DeCalorimeter&,
                                 bool ) const;
    template <Decode>
    std::vector<Adc> decode_v1( int sourceID, span<const unsigned int>, RawBankReadoutStatus&,
                                const DeCalorimeter& ) const;
    template <Decode>
    std::vector<Adc> decode_v2( int sourceID, span<const unsigned int>, RawBankReadoutStatus&,
                                const DeCalorimeter& ) const;
    template <Decode>
    std::vector<Adc> decode_v3( int sourceID, span<const unsigned int>, RawBankReadoutStatus&,
                                const DeCalorimeter& ) const;

    bool checkCards( const DeCalorimeter&, Tell1Param::FECards const& feCards ) const;
    bool checkCards( const DeCalorimeter&, std::vector<int> const& feCards ) const;
    int  findCardbyCode( const DeCalorimeter&, Tell1Param::FECards const& feCards, int code ) const;
    void checkCtrl( int ctrl, int sourceID, RawBankReadoutStatus& status ) const;

    Gaudi::Property<bool>         m_useCalibration{this, "UseCalibrationParams", false};
    Gaudi::Property<bool>         m_useParamsFromDB{this, "UseParamsFromDB", true};
    Gaudi::Property<zsupMethod_t> m_zsupMethod{this, "ZSupMethod",
                                               zsupMethod_t::one_d}; ///< Name of Zero Suppression method
    Gaudi::Property<int>          m_zsupThreshold{this, "ZSupThreshold", -1000, "Initial threshold, in ADC counts"};
    Gaudi::Property<int> m_zsupNeighbour{this, "ZSupNeighbour", -256, "zsup (ADC) for neighbours for 2D method"};
    Gaudi::Property<std::string> m_extension{this, "Extension"};
    Gaudi::Property<bool>        m_packedIsDefault{this, "PackedIsDefault", false};

    Gaudi::Property<bool> m_extraHeader{this, "DetectorSpecificHeader", false};
    Gaudi::Property<bool> m_cleanCorrupted{this, "CleanWhenCorruption", false};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_noBanksCounter{this, "No bank found", 1};
    mutable Gaudi::Accumulators::Counter<>              m_duplicateADCDigits{this, "# duplicate ADC/Digits"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_bankSize_error{this, "Banks with wrong size"};
    mutable Gaudi::Accumulators::Histogram<1>           m_histoWrongSourceID{
        this, "WrongBankSourceID", "Wrong bank size", {225, 22550, 22775}};
  };

  unsigned int fibMask1 = 0xfff000ff;
  unsigned int fibMask2 = 0xf000fff0;
  unsigned int fibMask3 = 0xfff000;

  DECLARE_COMPONENT_WITH_ID( RawToDigits, "CaloFutureRawToDigits" )

  //=============================================================================
  // Standard creator, initializes variables
  //=============================================================================
  RawToDigits::RawToDigits( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformer( name, pSvcLocator,
                          {KeyValue{"RawBanks", "DAQ/RawBanks/Calo"},
                           KeyValue{"ErrorRawBanks", "DAQ/RawBanks/CaloError"},
                           KeyValue{"DetectorLocation", Utilities::DeCaloFutureLocation( name.substr( 6, 4 ) )}},
                          {KeyValue{"OutputDigitData", Utilities::CaloFutureDigitLocation( name.substr( 6, 4 ) )},
                           KeyValue{"OutputReadoutStatusData",
                                    Utilities::CaloFutureRawBankReadoutStatusLocation( name.substr( 6, 4 ) )}} ) {}

  //=============================================================================
  // Main execution
  //=============================================================================
  std::tuple<Event::Calo::Digits, RawBankReadoutStatus> RawToDigits::
                                                        operator()( const RawBank::View& banks, const RawBank::View& error_banks, const DeCalorimeter& calo ) const {
    // ------------------------------------
    // --- Get RawBank AND ReadoutStatus
    // ------------------------------------
    assert( calo.index() == Detector::Calo::CellCode::Index::EcalCalo ||
            calo.index() == Detector::Calo::CellCode::Index::HcalCalo );

    if ( banks.empty() ) {
      ++m_noBanksCounter;
      return {};
    }
    RawBank::BankType bank_type = banks[0]->type();

    if ( msgLevel( MSG::DEBUG ) )
      debug() << " Requested banks of " << label( bank_type ) << " type has been found" << endmsg;

    RawBankReadoutStatus status = RawBankReadoutStatus( bank_type );
    bool                 packed =
        ( bank_type == RawBank::EcalPacked || bank_type == RawBank::HcalPacked || bank_type == RawBank::Calo );

    for ( const auto& b : error_banks ) status.addStatus( b->sourceID(), RawBankReadoutStatus::Status::ErrorBank );

    // check banks integrity + Magic pattern
    boost::container::small_vector<int, 32> sources;
    for ( const auto& b : banks ) {
      if ( !b ) continue;
      sources.push_back( b->sourceID() );
      if ( RawBank::MagicPattern != b->magic() ) {
        error() << "Bad MagicPattern for sourceID " << b->sourceID() << endmsg;
        status.addStatus( b->sourceID(), RawBankReadoutStatus::Status::BadMagicPattern );
      }
    }

    if ( packed ) { // TELL1 format : 1 source per TELL1
      for ( const auto& t : calo.tell1Params() ) {
        bool ok = std::any_of( sources.begin(), sources.end(), [&]( int n ) { return t.number() == n; } );
        status.addStatus( t.number(), ok ? RawBankReadoutStatus::Status::OK : RawBankReadoutStatus::Status::Missing );
      }
    } else { // Offline format : single source 0
      status.addStatus( 0, sources.empty() ? RawBankReadoutStatus::Status::Missing : RawBankReadoutStatus::Status::OK );
    }

    // ------------------------------------
    // ---  Decode the rawBanks
    // ------------------------------------
    std::vector<Adc> dataVec;
    for ( const auto& bank : banks ) {
      //--- check bank size
      if ( ( ( bank->version() < 3 && bank->type() == RawBank::Calo ) || bank->version() > 3 ) &&
           bank->size() != 156 ) {
        ++m_bankSize_error;
        ++m_histoWrongSourceID[bank->sourceID()];
        continue;
      }
      //--- decode the rawbanks
      auto dataVecDec = decode_<Decode::Cell>( *bank, status, calo ); // false is to get data, not pinData
      dataVec.insert( dataVec.end(), dataVecDec.begin(), dataVecDec.end() );

      if ( dataVecDec.empty() && msgLevel( MSG::DEBUG ) )
        debug() << "Error when decoding bank " << bank->sourceID() << " -> incomplete data - May be corrupted"
                << endmsg;
    }

    // ------------------------------------
    // ---  Prepare the output
    // ------------------------------------
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Processing " << dataVec.size() << " Digits." << endmsg;

    enum class Flag : uint8_t { Default, Neighbour, Seed };
    std::vector<Flag> caloFlags( calo.numberOfCells(), Flag::Default );

    // Get zero suppression values from DB
    double zSupThreshold          = 0;
    double zSupNeighbourThreshold = 0;
    double zSupMethod             = 0;

    if ( m_useParamsFromDB ) {
      zSupThreshold          = calo.zSupThreshold();
      zSupNeighbourThreshold = calo.zSupNeighbourThreshold();
      zSupMethod             = calo.zSupMethod();
    } else {
      zSupThreshold          = m_zsupThreshold;
      zSupNeighbourThreshold = m_zsupNeighbour;
      switch ( m_zsupMethod ) {
      case zsupMethod_t::two_d:
        zSupMethod = 2;
        break;
      case zsupMethod_t::one_d:
        zSupMethod = 1;
        break;
      default:
        zSupMethod = 0;
        break;
      }
    }
    if ( zSupMethod == 0 ) fatal() << "Zero suppression method not set: Must be fixed in configuration" << endmsg;

    // == Apply the threshold. If 2DZsup, tag also the neighbours
    for ( const auto& anAdc : dataVec ) {
      int digAdc = anAdc.adc;
      if ( digAdc < zSupThreshold ) continue;
      Detector::Calo::CellID id    = anAdc.id;
      int                    index = calo.cellIndex( id );
      if ( index < 0 ) // it practically canot happen, but it's technically possible and gcc 12 complains
        throw GaudiException( fmt::format( "invalid cell id {}", Gaudi::Utils::toString( id ) ), name(),
                              StatusCode::FAILURE );
      caloFlags[index] = Flag::Seed;
      if ( zSupMethod == 2 ) {
        for ( const auto& neighbour : calo.neighborCells( id ) ) {
          auto& neighFlag = caloFlags[calo.cellIndex( neighbour )];
          if ( neighFlag != Flag::Seed ) neighFlag = Flag::Neighbour;
        }
      }
    }

    if ( msgLevel( MSG::VERBOSE ) ) {
      for ( const auto& anAdc : dataVec ) {
        int digAdc = anAdc.adc;
        if ( digAdc < zSupThreshold ) continue;
        Detector::Calo::CellID id = anAdc.id;
        verbose() << id << format( " Energy adc %4d", digAdc );
        if ( zSupThreshold <= digAdc ) debug() << " seed";
        verbose() << endmsg;
      }
    }

    // write tagged data
    Event::Calo::Digits digits;
    double              pedShift = calo.pedestalShift();
    for ( const auto& anAdc : dataVec ) {
      Detector::Calo::CellID id    = anAdc.id;
      int                    index = calo.cellIndex( id );
      if ( Flag::Default == caloFlags[index] ) continue;
      if ( Flag::Neighbour == caloFlags[index] && anAdc.adc < zSupNeighbourThreshold ) continue;

      double energy = m_useCalibration
                          ? ( double( anAdc.adc ) - pedShift ) * calo.cellGain( id ) * calo.getCalibration( id )
                          : ( double( anAdc.adc ) - pedShift ) * calo.cellGain( id );
      auto ok = digits.try_emplace( id, energy, anAdc.adc );
      if ( !ok ) {
        ++m_duplicateADCDigits; // Duplicate ADC/Digit
        warning() << "Duplicate ADC/Digit for channel " << id << endmsg;
        int card  = calo.cardNumber( id );
        int tell1 = calo.cardToTell1( card );
        // to be completed --- LHCb::RawBankReadoutStatus& status = m_adcTool->status();
        status.addStatus( tell1, RawBankReadoutStatus::Status::DuplicateEntry );
      }

      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << id << " added as " << ( caloFlags[index] == Flag::Neighbour ? "Neighbour." : "Seed." ) << endmsg;
      }
    }

    if ( msgLevel( MSG::DEBUG ) ) { debug() << format( "Have stored %5d CaloDigits.", digits.size() ) << endmsg; }

    return {std::move( digits ), std::move( status )};
  }
  //=============================================================================
  template <RawToDigits::Decode decode>
  std::vector<RawToDigits::Adc> RawToDigits::decode_v10( int sourceID, span<const unsigned int> data,
                                                         RawBankReadoutStatus& status, const DeCalorimeter& calo,
                                                         bool isLittleEndian ) const {

    std::vector<Adc> dataVec;
    dataVec.reserve( decode == Decode::PinData ? calo.numberOfPins() : calo.numberOfCells() );

    // Get the FE-Cards associated to that bank (via condDB)
    std::map<int, std::vector<int>>           map = calo.getSourceIDsMap();
    std::map<int, std::vector<int>>::iterator itr;
    std::vector<int>                          feCards, feCardsNotRead;
    for ( itr = map.begin(); itr != map.end(); ++itr )
      if ( itr->first == sourceID ) feCards = itr->second;
    if ( feCards.empty() &&
         ( ( calo.index() == Detector::Calo::CellCode::Index::EcalCalo && ( sourceID >> 11 ) == 11 ) ||
           ( calo.index() == Detector::Calo::CellCode::Index::HcalCalo &&
             ( sourceID >> 11 ) == 12 ) ) // To avoid unexpected
                                          // errors, since Ecal and
                                          // Hcal have same BankType
    )
      error() << " SourceID : " << sourceID << " not found in CondDB :  Cannot read that bank" << endmsg;

    int nCards = feCards.size();
    if ( msgLevel( MSG::DEBUG ) )
      debug() << nCards << " FE-Cards are expected to be readout : " << feCards << " in Tell40 sourceID " << sourceID
              << endmsg;

    int prevCard = -1;

    for ( int ifeb = 0; ifeb < nCards; ifeb++ ) {

      LHCb::span<const unsigned int> subdata;
      if ( ifeb == 0 ) subdata = data.subspan( 3, 12 ); // Start at 3 to skip LLT bank ...
      if ( ifeb == 1 ) subdata = data.subspan( 15, 12 );
      if ( ifeb == 2 ) subdata = data.subspan( 27, 12 );

      LHCb::span<const unsigned int> subdata_forFiberCheck = subdata;

      int card = feCards.at( ifeb );
      if ( card == 0 ) continue; // no FEB linked

      std::vector<Detector::Calo::CellID> chanID;
      if ( 0 <= card ) {
        chanID = calo.cardChannels( feCards[ifeb] );
      } else {
        chanID.clear();
        feCardsNotRead.push_back( feCards[ifeb] );
        error() << " FE-Card w/ [code : " << card << " ] is not associated with TELL1 bank sourceID : " << sourceID
                << " in condDB :  Cannot read that bank" << endmsg;
        error() << "Warning : previous data may be corrupted" << endmsg;
        if ( m_cleanCorrupted && calo.isPinCard( prevCard ) == ( decode == Decode::PinData ) ) {
          auto hasBadCardNumber = [&]( const Adc& adc ) { return calo.cellParam( adc.id ).cardNumber() == prevCard; };
          dataVec.erase( std::remove_if( dataVec.begin(), dataVec.end(), hasBadCardNumber ), dataVec.end() );
        }
        status.addStatus( sourceID, RawBankReadoutStatus::Status::Incomplete );
        status.addStatus( sourceID, RawBankReadoutStatus::Status::Corrupted );
      }
      prevCard = card;

      // Start readout of the FE-board
      unsigned int pattern  = pop( subdata, isLittleEndian ); // skip LLT
      int          offset   = 0;
      unsigned int lastData = pattern;

      int  nADC       = 0;
      bool isFiberOff = false;

      // ... and readout data
      for ( unsigned int bitNum = 0; 32 > bitNum; bitNum++ ) {

        if ( nADC % 8 == 0 ) { // Check fibers pattern, 1 fiber corresponds to 8 ADC (96b)
          unsigned int pattern1 = pop( subdata_forFiberCheck, isLittleEndian );
          unsigned int pattern2 = pop( subdata_forFiberCheck, isLittleEndian );
          unsigned int pattern3 = pop( subdata_forFiberCheck, isLittleEndian );
          if ( pattern1 == fibMask1 && pattern2 == fibMask2 && pattern3 == fibMask3 )
            isFiberOff = true;
          else
            isFiberOff = false;
        }

        if ( 31 < offset ) {
          offset -= 32;
          if ( !( subdata.size() > 0 ) ) continue;
          lastData = pop( subdata, isLittleEndian );
        }

        int adc;
        if ( offset == 24 )
          adc = ( lastData & 0xff );
        else if ( offset == 28 )
          adc = ( (lastData)&0xf );
        else
          adc = ( ( lastData >> ( 20 - offset ) ) & 0xfff );

        if ( 28 == offset ) { //.. get the extra bits on next word
          if ( !( subdata.size() > 0 ) ) continue;
          lastData = pop( subdata, isLittleEndian );
          int temp = ( lastData >> ( offset - 4 ) ) & 0xFF;
          offset -= 32;
          adc = ( adc << 8 ) + temp;
        }
        if ( 24 == offset ) { //.. get the extra bits on next word
          if ( !( subdata.size() > 0 ) ) continue;
          lastData = pop( subdata, isLittleEndian );
          int temp = ( lastData >> ( offset + 4 ) ) & 0xF;
          offset -= 32;
          adc = ( adc << 4 ) + temp;
        }
        offset += 12;
        adc -= 256;
        ++nADC;

        Detector::Calo::CellID id = ( bitNum < chanID.size() ? chanID[bitNum] : Detector::Calo::CellID() );
        verbose() << " |  SourceID : " << sourceID << " |  FeBoard : " << calo.cardNumber( id )
                  << " |  Channel : " << bitNum << " |  CaloCell " << id << " |  valid ? " << calo.valid( id )
                  << " |  ADC value = " << adc << endmsg;

        //== Keep only valid cells
        if ( 0 != id.index() && !isFiberOff ) {
          Adc temp( id, adc );
          if ( id.isPin() == ( decode == Decode::PinData ) ) dataVec.push_back( temp );
        }
      }
    }

    // Check All cards have been read
    if ( !checkCards( calo, feCardsNotRead ) ) status.addStatus( sourceID, RawBankReadoutStatus::Status::Incomplete );

    return dataVec;
  }
  //=============================================================================
  template <RawToDigits::Decode decode>
  std::vector<RawToDigits::Adc> RawToDigits::decode_v1( int sourceID, span<const unsigned int>      data,
                                                        RawBankReadoutStatus&, const DeCalorimeter& calo ) const {
    std::vector<Adc> dataVec;
    dataVec.reserve( decode == Decode::PinData ? calo.numberOfPins() : calo.numberOfCells() );
    //******************************************************************
    //**** Simple coding, ID + adc in 32 bits.
    //******************************************************************
    while ( !data.empty() ) {
      auto d   = pop( data );
      int  adc = d & 0xFFFF;
      if ( 32767 < adc ) adc |= 0xFFFF0000; //= negative value
      Detector::Calo::CellID cellId( ( d >> 16 ) & 0xFFFF );
      // event dump
      verbose() << " |  SourceID : " << sourceID << " |  FeBoard : " << calo.cardNumber( cellId ) << " |  CaloCell "
                << cellId << " |  valid ? " << calo.valid( cellId ) << " |  ADC value = " << adc << endmsg;

      if ( 0 != cellId.index() && cellId.isPin() == ( decode == Decode::PinData ) ) dataVec.emplace_back( cellId, adc );
    }
    return dataVec;
  }
  //=============================================================================
  template <RawToDigits::Decode decode>
  std::vector<RawToDigits::Adc> RawToDigits::decode_v2( int sourceID, span<const unsigned int> data,
                                                        RawBankReadoutStatus& status,
                                                        const DeCalorimeter&  calo ) const {
    std::vector<Adc> dataVec;
    dataVec.reserve( decode == Decode::PinData ? calo.numberOfPins() : calo.numberOfCells() );
    //******************************************************************
    //**** 1 MHz compression format, Ecal and Hcal
    //******************************************************************
    // Get the FE-Cards associated to that bank (via condDB)
    auto feCards = calo.tell1ToCards( sourceID );
    if ( msgLevel( MSG::DEBUG ) )
      debug() << feCards.size() << " FE-Cards are expected to be readout : " << feCards << " in Tell1 bank " << sourceID
              << endmsg;
    int prevCard = -1;
    // access chanID via condDB
    std::vector<Detector::Calo::CellID> chanID;
    while ( !data.empty() ) {
      // Skip
      unsigned int word = pop( data );
      // Read bank header
      checkCtrl( ctrl( word ), sourceID, status );
      // look for the FE-Card in the Tell1->cards vector
      int card = findCardbyCode( calo, feCards, code( word ) );
      if ( 0 <= card ) {
        chanID = calo.cardChannels( feCards[card] );
        feCards.erase( feCards.begin() + card );
      } else {
        chanID.clear();
        error() << " FE-Card w/ [code : " << code( word )
                << " ] is not associated with TELL1 bank sourceID : " << sourceID
                << " in condDB :  Cannot read that bank" << endmsg;

        error() << "Warning : previous data may be corrupted" << endmsg;
        if ( m_cleanCorrupted && calo.isPinCard( prevCard ) == ( decode == Decode::PinData ) ) {
          auto hasBadCardNumber = [&]( const Adc& adc ) { return calo.cellParam( adc.id ).cardNumber() == prevCard; };
          dataVec.erase( std::remove_if( dataVec.begin(), dataVec.end(), hasBadCardNumber ), dataVec.end() );
        }
        status.addStatus( sourceID, RawBankReadoutStatus::Status::Incomplete );
        status.addStatus( sourceID, RawBankReadoutStatus::Status::Corrupted );
      }
      prevCard = card;

      // Start readout of the FE-board
      // First skip trigger bank ...
      advance( data, ( lenTrig( word ) + 3 ) / 4 ); //== is in bytes, with padding
      std::bitset<32> pattern  = pop( data );
      int             offset   = 0;
      unsigned int    lastData = pop( data );
      // ... and readout data
      for ( unsigned int bitNum = 0; bitNum != pattern.size(); bitNum++ ) {
        if ( offset == 32 ) {
          lastData = pop( data );
          offset -= 32;
        }
        const auto short_code = !pattern.test( bitNum );
        auto       uw         = ( lastData >> offset ) & ( short_code ? 0xF : 0xFFF );
        offset += ( short_code ? 4 : 12 );
        if ( offset > 32 ) { // get the remaining bits from the next word
          lastData = pop( data );
          offset -= 32;
          uw |= ( lastData << ( 12 - offset ) ) & 0xFFF;
        }
        const int adc = static_cast<int>( uw ) - ( short_code ? 8 : 256 );

        Detector::Calo::CellID id = ( bitNum < chanID.size() ? chanID[bitNum] : Detector::Calo::CellID() );

        // event dump
        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << " |  SourceID : " << sourceID << " |  FeBoard : " << calo.cardNumber( id )
                    << " |  Channel : " << bitNum << " |  CaloCell " << id << " |  valid ? " << calo.valid( id )
                    << " |  ADC value = " << adc << endmsg;

        //== Keep only valid cells
        if ( 0 != id.index() && id.isPin() == ( decode == Decode::PinData ) ) dataVec.emplace_back( id, adc );
      }
    }
    // Check All cards have been read
    if ( !checkCards( calo, feCards ) ) status.addStatus( sourceID, RawBankReadoutStatus::Status::Incomplete );

    return dataVec;
  }
  //=============================================================================
  template <RawToDigits::Decode decode>
  std::vector<RawToDigits::Adc> RawToDigits::decode_v3( int sourceID, span<const unsigned int> data,
                                                        RawBankReadoutStatus& status,
                                                        const DeCalorimeter&  calo ) const {
    std::vector<Adc> dataVec;
    dataVec.reserve( decode == Decode::PinData ? calo.numberOfPins() : calo.numberOfCells() );
    //******************************************************************
    //**** 1 MHz compression format, Preshower + SPD
    //******************************************************************

    // Get the FE-Cards associated to that bank (via condDB)
    auto feCards = calo.tell1ToCards( sourceID );
    if ( msgLevel( MSG::DEBUG ) )
      debug() << feCards.size() << " FE-Cards are expected to be readout : " << feCards << " in Tell1 bank " << sourceID
              << endmsg;
    int prevCard = -1;
    while ( !data.empty() ) {
      // Skip
      const unsigned int word = pop( data );
      // Read bank header
      checkCtrl( ctrl( word ), sourceID, status );
      // access chanID via condDB
      // look for the FE-Card in the Tell1->cards vector
      int card = findCardbyCode( calo, feCards, code( word ) );
      if ( card < 0 ) {
        error() << " FE-Card w/ [code : " << code( word )
                << " ] is not associated with TELL1 bank sourceID : " << sourceID
                << " in condDB :  Cannot read that bank" << endmsg;
        error() << "Warning : previous data may be corrupted" << endmsg;
        if ( m_cleanCorrupted && calo.isPinCard( prevCard ) == ( decode == Decode::PinData ) ) {
          auto hasBadCardNumber = [&]( const Adc& adc ) { return calo.cellParam( adc.id ).cardNumber() == prevCard; };
          dataVec.erase( std::remove_if( dataVec.begin(), dataVec.end(), hasBadCardNumber ), dataVec.end() );
        }

        status.addStatus( sourceID,
                          RawBankReadoutStatus::Status::Corrupted | RawBankReadoutStatus::Status::Incomplete );
      }
      const auto& chanID = calo.cardChannels( feCards[card] );
      feCards.erase( feCards.begin() + card );
      prevCard = card;

      // Read the FE-Board
      // skip the trigger bits
      advance( data, ( lenTrig( word ) + 3 ) / 4 ); //== Length in byte, with padding

      // read data
      auto add_data = [&, dump = msgLevel( MSG::VERBOSE )]( unsigned int data ) {
        int          adc = data & 0x3FF;
        unsigned int num = ( data >> 10 ) & 0x3F;

        Detector::Calo::CellID id = ( num < chanID.size() ? chanID[num] : Detector::Calo::CellID{} );
        if ( 0 != id.index() && id.isPin() == ( decode == Decode::PinData ) ) dataVec.emplace_back( id, adc );

        if ( dump ) {
          // event dump
          verbose() << " |  SourceID : " << sourceID << " |  FeBoard : " << calo.cardNumber( id )
                    << " |  Channel : " << num << " |  CaloCell " << id << " |  valid ? " << calo.valid( id )
                    << " |  ADC value = " << adc << endmsg;
        }
      };

      int nAdc = lenADC( word );
      for ( ; nAdc > 1; nAdc -= 2 ) {
        auto w = pop( data );
        add_data( w & 0xFFFF );
        add_data( ( w >> 16 ) & 0xFFFF );
      }
      if ( nAdc == 1 ) add_data( pop( data ) & 0xFFFF );
      assert( nAdc == 0 || nAdc == 1 );
    } //== DataSize
    // Check All cards have been read
    if ( !checkCards( calo, feCards ) ) status.addStatus( sourceID, RawBankReadoutStatus::Status::Incomplete );
    return dataVec;
  }
  //=============================================================================
  // Main method to decode the rawBank
  //=============================================================================
  template <RawToDigits::Decode decode>
  std::vector<RawToDigits::Adc> RawToDigits::decode_( const RawBank& bank, RawBankReadoutStatus& status,
                                                      const DeCalorimeter& calo ) const {
    if ( RawBank::MagicPattern != bank.magic() ) return {}; // do not try to decode when MagicPattern is bad
    // Get bank info
    auto data     = bank.range<unsigned int>();
    int  sourceID = bank.sourceID();

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Decode bank " << &bank << " source " << sourceID << " version " << bank.version() << " size "
              << data.size() << endmsg;

    if ( data.empty() ) {
      status.addStatus( sourceID, RawBankReadoutStatus::Status::Empty );
      return {};
    }

    // skip detector specific header line
    if ( m_extraHeader ) pop( data ); // drop first word

    switch ( bank.version() ) {
    case 1:
      if ( bank.type() == RawBank::Calo )
        return decode_v10<decode>( sourceID, data, status, calo, false ); // Big endian
      else
        return decode_v1<decode>( sourceID, data, status, calo );
    case 2:
      if ( bank.type() == RawBank::Calo )
        return decode_v10<decode>( sourceID, data, status, calo, true ); // Little endian
      else
        return decode_v2<decode>( sourceID, data, status, calo );
    case 3:
      return decode_v3<decode>( sourceID, data, status, calo );
    case 4:
      return decode_v10<decode>( sourceID, data, status, calo, false ); // Big endian
    case 5:
      return decode_v10<decode>( sourceID, data, status, calo, true ); // Little endian
    default:
      warning() << "Bank type " << bank.type() << " sourceID " << sourceID << " has version " << bank.version()
                << " which is not supported" << endmsg;
      return {};
    }
  }

  //========================
  //  Check FE-Cards is PIN
  //========================
  bool RawToDigits::checkCards( const DeCalorimeter& calo, Tell1Param::FECards const& feCards ) const {
    bool check = true;
    for ( auto i : feCards ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << " Unread FE-Cards : " << calo.cardCode( i ) << "  - Is it a PinDiode readout FE-Card ? "
                << calo.isPinCard( i ) << endmsg;
      if ( calo.isPmtCard( i ) ) {
        warning() << " The standard (PMT) FE-Card " << calo.cardCode( i )
                  << " expected in TELL1 bank has not been read !!" << endmsg;
        check = false;
      }
    }
    return check;
  }
  bool RawToDigits::checkCards( const DeCalorimeter& calo, std::vector<int> const& feCards ) const {
    bool check = true;
    for ( auto i : feCards ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << " Unread FE-Cards : " << calo.cardCode( i ) << "  - Is it a PinDiode readout FE-Card ? "
                << calo.isPinCard( i ) << endmsg;
      if ( calo.isPmtCard( i ) ) {
        warning() << " The standard (PMT) FE-Card " << calo.cardCode( i )
                  << " expected in TELL1 bank has not been read !!" << endmsg;
        check = false;
      }
    }
    return check;
  }

  //===========================
  //  Find Card number by code
  //===========================
  int RawToDigits::findCardbyCode( const DeCalorimeter& calo, Tell1Param::FECards const& feCards, int code ) const {
    auto i = std::find_if( feCards.begin(), feCards.end(),
                           [&]( unsigned char ife ) { return code == calo.cardCode( ife ); } );
    if ( i == feCards.end() ) {
      error() << "FE-Card [code : " << code << "] does not match the condDB cabling scheme  " << endmsg;
      return -1;
    }
    if ( msgLevel( MSG::DEBUG ) )
      debug() << " FE-Card [code : " << code << " | crate : " << calo.cardParam( *i ).crate()
              << " slot : " << calo.cardParam( *i ).slot() << "] has been found with (num : " << static_cast<int>( *i )
              << ")  in condDB" << endmsg;
    return std::distance( feCards.begin(), i );
  }

  void RawToDigits::checkCtrl( int ctrl, int sourceID, RawBankReadoutStatus& status ) const {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Control word :" << ctrl << endmsg;
    if ( 0 != ( 0x01 & ctrl ) || 0 != ( 0x20 & ctrl ) || 0 != ( 0x40 & ctrl ) ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Tell1 error bits have been detected in data" << endmsg;
      if ( 0 != ( 0x01 & ctrl ) ) status.addStatus( sourceID, RawBankReadoutStatus::Status::Tell1Error );
      if ( 0 != ( 0x20 & ctrl ) ) status.addStatus( sourceID, RawBankReadoutStatus::Status::Tell1Sync );
      if ( 0 != ( 0x40 & ctrl ) ) status.addStatus( sourceID, RawBankReadoutStatus::Status::Tell1Link );
    }
  }

} // namespace LHCb::Calo
