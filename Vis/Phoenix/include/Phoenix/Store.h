/********************************************************************************\
 * * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/

#pragma once

// Icludes
#include "Gaudi/MonitoringHub.h"
#include "GaudiKernel/Service.h"

#include <algorithm>
#include <map>
#include <string>

#include <mutex>

#include <nlohmann/json.hpp>

/**
 * Entity dealing with the dumping of LHCb event data in .json format
 *
 *
 * Purpose and principles
 *
 * The purpose of this entity is to deal with event data and output them into a file in json format,
 * so they can be visualized inside the Phoenix web-based event display.
 * The json format should be following the Phoenix guidelines that can be found in the following url:
 * https://github.com/HSF/phoenix/blob/master/guides/users.md#using-phoenix-with-your-own-data
 *
 * The entity, the monitoring hub and the associated PhoenixSink will take care of the boiler plate
 * code and the global format of the final json file. The user of this entity only has to provide
 * the tiny piece of json data corresponding to its own case. See details below in "Content of the
 * json data to produce"
 *
 * At run time, the PhoneixSink will create a file named "LHCb_EventDataset.json" by default
 * with your data. This file can be uploaded in the Phoenix web display ("https://lhcb-web-display.app.cern.ch/#/")
 * and your data can be visualized. For non standard visualization, you might need to add some javascript code
 * inside Phoenix itself. This allows to extend Phoenix for custom data
 * Detailed documentation on how and where to do that can be found at
 * "https://github.com/HSF/phoenix/blob/master/guides/developers/event-data-loader.md#handling-new-physics-objects"
 *
 * As this is an open source project supported by amazing people, you can at any time open an issue and ask a question
 * if stuck at any point, and for sure someone will reply and help as soon as possible.
 * "https://github.com/HSF/phoenix/issues"
 *
 *
 * Practical usage
 *
 * In order add new data (e.g. FT hits) to the Phoenix output,
 * the only thing one has to do is add a create a new algorithm using this entity and add it
 * to the options of her/his Gaudi application.
 * Examples are given in the in Vis/PhoenixAlgs package in the Rec project.
 *
 * Overall the code will look like this :
 *
 * ```cpp
 * class DumpMyData final : public Consumer<void( MyData const& )> {
 * public:
 *   DumpMyData( const std::string& name, ISvcLocator* pSvcLocator ) :
 *     Consumer( name, pSvcLocator,
 *               {{"MyDataLocation", "my/data/path"}} ) {}
 *
 *   void operator()( MyData const& myHits ) const override {
 *     m_store.storeEventData( {{"gps time", odin.gpsTime()},
 *                              {"run number", odin.runNumber()},
 *                              {"event number", odin.eventNumber()},
 *                              {"Content", {{"Hits", {{"MyData", myHits}}}}}} );
 *   }
 * private:
 *   mutable Store m_store{this, "MyData_store", "Phoenix:MyData"};
 * };
 * DECLARE_COMPONENT( DumpMyData )
 * ```
 *
 * In practice:
 * 1. You have to get the new data that you want to add the standard way via the functional framework,
 *    here MyData from the given path in the Event store
 * 2. You need to spit out the subpart of the Phoenix JSON concerning your new data via a call to
 *    `storeEventData`. `storeEventData` takes json data as argument but thanks to the use of the
 *    nlohmann library, the conversion between C++ structures and json is almost automatic.
 *    See below for details and also see part on "Content of the json data to produce" for
 *    understanding the content of the data
 * 3. Obviously you declare a member of type Store to hold your json data
 *
 *
 * Buiding json data from C++ structures
 *
 * The nlohmann C++ library is used for the convertion from C++ code to JSON format
 * It allows automatic conversion of standard types : vectors, maps and overall
 * 'std::' is covered
 * It is extensible so that your own types can automatically convert like in the example
 * above for `MyData`. You only need to provide the needed converters under the form of
 * a to_json function. Here is an example of code :
 *
 * ```cpp
 * void to_json( json& j, MyData const& myHits ) {
 *   j = {}; // set resulting json as an empty vector
 *   for ( auto const hit : myHits ) {
 *     // add one entry per hit, each time a dictionnary with channelID and pos keys
 *     j += {{"channelID", hit.channelID()}, {"pos", {hit.x(), hit.y(), hit.z()}}};
 *   }
 * }
 * ```
 *
 * Complete documentation for the library can be found on: ( https://github.com/nlohmann/json )
 *
 *
 * Content of the json data to produce
 *
 * The top level json format mentionned in the phoenix documentation looks like :
 * ```json
 * {
 *   "EVENT_KEY_1": event_object,
 *   "EVENT_KEY_2": event_object,
 *   ...
 *   "EVENT_KEY_N": event_object
 * }
 * ```
 * Where event_object looks like :
 * ```json
 * {
 *   "event number": XXX,
 *   "run number": YYY,
 *   "OBJECT_TYPE_1": {
 *     "COLLECTION_NAME_X" : [ OBJECTS ]
 *     ...
 *   },
 * ...
 * }
 * ```
 *
 * Here `event number` and `run number` identify the event, OBJECT_TYPE_1 is the type of object
 * to render (defining how to render it) and COLLECTION_NAME_X a list of such objects with
 * a meaningful name appearing the the Phoenix viewer.
 * Example of object types supported by phoenix are Tracks, Hits or Vertices.
 *
 * This maps to the code provided above as an example :
 * ```cpp
 *     m_store.storeEventData( {{"gps time", odin.gpsTime()},
 *                              {"run number", odin.runNumber()},
 *                              {"event number", odin.eventNumber()},
 *                              {"Content", {{"Hits", {{"MyData", myHits}}}}}} );
 * ```
 * The mapping is obvious for "event number" and "run number", "HIts" is the OBJECT_TYPE_1
 * and "MyData" the COLLECTION_NAME_X. On top :
 *   - we have an extra "gps time" entry for each event on top of event and run numbers
 *   - these 3 items need to be provided each time you call storeEventData. They will be used
 *     in the PhoenixSink to group the pieces of data coming from different Store objects
 *     for different pieces of the detector
 *   - the format of the data given to StoreEventData must be a dictionnary with keys
 *     "gps time", "event data", "run number" and "Content"
 *   - the "Content" entry must hold a dictionary of OBJECT_TYPE entries
 *   - each OBJECT_TYPE entry (in the example a single one for Hits) is a dictionary
 *     of COLLECTIONS (in the example a single one called "MyData")
 *   - a collection is a list of entries of the proper format. In case of Hits, phoenix
 *     documentation says "hit needs to have a pos attribute [...] Everything else is decoration."
 *   - we give directly myHits C++ object to the nlohmann library as we've defined the
 *     converter of MyData into json above (and note the creation of the pos attribute there)
 *
 * @author Andreas PAPPAS
 * @date 27-07-2021
 */

namespace LHCb::Phoenix {

  class Store {

  private:
    mutable std::mutex                  m_event_data_lock;
    mutable std::vector<nlohmann::json> m_event_data;

    Gaudi::Monitoring::Hub* m_monitoringHub{nullptr};

  public:
    /// registering the Store Entity to the monitoring Hub
    template <typename OWNER>
    Store( OWNER* o, std::string const& name, const std::string storeType )
        : m_monitoringHub( &o->serviceLocator()->monitoringHub() ) {
      m_monitoringHub->registerEntity( o->name(), name, storeType, *this );
    }

    ~Store() { m_monitoringHub->removeEntity( *this ); }

    /// giving access to event data in .json format and returns that.
    nlohmann::json toJSON() const;

    /**
     * A method which will accept event data as input,
     * basically a string in particular format ( json )
     * and will store that data into the m_event_data vector
     *
     * Thread Safe!
     */
    void storeEventData( nlohmann::json ) const;

    /// function resetting internal data
    void reset() const;
  };
} // namespace LHCb::Phoenix
