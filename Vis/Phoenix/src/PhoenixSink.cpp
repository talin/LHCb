/*****************************************************************************\
 * * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/

// Icludes
#include "Gaudi/MonitoringHub.h"
#include "GaudiKernel/Service.h"

#include <algorithm>
#include <deque>
#include <fstream>

#include "Phoenix/Store.h"

/** @class Sink PhoenixSink.cpp
 *
 * Receive and collect multiple event data as input produced from different algorithms (in Rec/Vis) outputing in json
 * format, and output them into a single .json file in the particular format that Phoenix web event display supports
 * Phoenix Documentation: https://github.com/HSF/phoenix/blob/master/guides/users.md#using-phoenix-with-your-own-data
 *
 *
 * @author Andreas PAPPAS
 * @date 22-07-2021
 */

namespace LHCb::Phoenix {

  class Sink : public Service, public Gaudi::Monitoring::Hub::Sink {

  public:
    using Service::Service;

    StatusCode initialize() override {
      return Service::initialize().andThen( [&] { serviceLocator()->monitoringHub().addSink( this ); } );
    }

    StatusCode stop() override {
      auto ok = Service::stop();
      if ( !ok ) return ok;
      std::sort( begin( m_monitoringEntities ), end( m_monitoringEntities ), []( const auto& a, const auto& b ) {
        return std::tie( a.name, a.component ) < std::tie( b.name, b.component );
      } );

      std::map<std::pair<int, int>, nlohmann::json> json_data;

      for ( auto& entity : m_monitoringEntities ) {
        for ( auto& entry : entity.toJSON() ) {
          std::pair<int, int> index{entry["run number"], entry["event number"]};

          // push run, event, gps number per unique event if they exist
          if ( json_data.find( index ) == json_data.end() ) {
            json_data[index]["run number"]          = entry["run number"];
            json_data[index]["event number"]        = entry["event number"];
            json_data[index]["gps time"]            = entry["gps time"];
            json_data[index]["bunch crossing type"] = entry["bunch crossing type"];
            // compute GMT human readable time, will be displayed in Phoenix
            // maybe UTC would be better ? To be reviewed anyway with C++20 where utc_clock is available
            const std::chrono::duration<uint64_t, std::micro>        dur{entry["gps time"]};
            const std::chrono::time_point<std::chrono::system_clock> time{dur};
            std::time_t       ctime = std::chrono::system_clock::to_time_t( time );
            std::tm           tm    = *std::gmtime( &ctime );
            std::stringstream ss;
            ss << std::put_time( &tm, "%Y-%m-%d %H:%M:%S GMT" );
            json_data[index]["time"] = ss.str();
          }

          // push only the core event data (by merging it)
          for ( auto& [k, content] : entry["Content"].items() ) {
            for ( auto& [k2, v] : content.items() ) { json_data[index][k][k2] = v; }
          }
        }
      }

      /// initializing the file stream
      m_ostrm = std::ofstream{m_fileName};

      // finally dump the data into the file
      nlohmann::json j;

      for ( auto& [p, v] : json_data ) {
        j[std::string( "EVENT-" ) + std::to_string( p.first ) + '-' + std::to_string( p.second )] = v;
      }

      m_ostrm << std::setw( m_indentation.value() ) << j << '\n';

      /// push as much data as possible and then flush
      m_ostrm.flush();

      // check for file IO errors
      return m_ostrm.good() ? StatusCode::SUCCESS : StatusCode::FAILURE;
    }

    void registerEntity( Gaudi::Monitoring::Hub::Entity ent ) override {
      if ( std::string_view( ent.type ).substr( 0, 8 ) == "Phoenix:" ) {
        m_monitoringEntities.push_back( std::move( ent ) );
      }
    }

    void removeEntity( Gaudi::Monitoring::Hub::Entity const& ent ) override {
      auto it = std::find( begin( m_monitoringEntities ), end( m_monitoringEntities ), ent );
      if ( it != m_monitoringEntities.end() ) { m_monitoringEntities.erase( it ); }
    }

  private:
    std::deque<Gaudi::Monitoring::Hub::Entity> m_monitoringEntities;

    // used to output to file
    std::ofstream m_ostrm;

    Gaudi::Property<std::string> m_fileName{this, "FileName", "LHCb_EventData.json",
                                            "Name of file where to save event data in json format"};
    Gaudi::Property<int>         m_indentation{this, "IndentationLevel", 2,
                                       "Indentation level for nlohmann library. -1 is compact format, 0 only line "
                                       "breaks, >0 selects actual indentation"};
  };

  DECLARE_COMPONENT( Sink )

} // namespace LHCb::Phoenix
