/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifndef USE_DD4HEP
#  include "DetDesc/ParamValidDataObject.h"
#endif
#include "DetDesc/ConditionKey.h"
#include "DetDesc/DetectorElement.h"
#include "Detector/FT/FTConstants.h"
#include "FTInfo.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/MsgStream.h"
#include <Detector/FT/FTSourceID.h>
#include <Event/RawBank.h>

#include <FTDet/DeFTDetector.h>
#include <array>
#include <optional>
#include <string>
#include <utility>
#include <vector>
#include <yaml-cpp/yaml.h>

#include "assert.h"

namespace FTRawBank = LHCb::Detector::FT::RawBank;

struct SiPMLinkPair final {
  // Set default values to kInvalid
  unsigned int bankNumber = LHCb::Detector::FTChannelID::kInvalidChannel().channelID();
  unsigned int linkIndex  = LHCb::Detector::FTChannelID::kInvalidChannel().channelID();
};

struct FTReadoutMap final {
public:
  FTReadoutMap() = default;

  FTReadoutMap( const Gaudi::Algorithm* parent );

  FTReadoutMap( const Gaudi::Algorithm* parent, const YAML::Node& rInfo, const LHCb::Detector::DeLHCb& deLHCb );

  std::set<unsigned int> compatibleVersions() const;

  void compatible( const unsigned bankVersion ) const;

  template <typename PARENT>
  static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key ) {
    assert( parent );
#ifdef USE_DD4HEP
    const bool hasCond = true;
#else
    const bool hasCond = parent->template existDet<ParamValidDataObject>( LHCb::Pr::FT::Info::cond_path );
#endif
    if ( hasCond ) {
      std::array<std::string, 2> conds = {LHCb::Pr::FT::Info::cond_path, LHCb::standard_geometry_top};
      return parent->addConditionDerivation( conds, std::move( key ),
                                             [parent]( YAML::Node const& c, const LHCb::Detector::DeLHCb& deLHCb ) {
                                               return FTReadoutMap{parent, c, deLHCb};
                                             } );
    } else {
      // Use the FT detector as a proxy
      return parent->addConditionDerivation( {DeFTDetectorLocation::Default}, std::move( key ),
                                             [parent]( DeFT const& ) { return FTReadoutMap{parent}; } );
    }
  }

  // Getters
  unsigned int               nBanks() const { return m_nBanks; }
  LHCb::Detector::FTSourceID sourceID( unsigned int iBank ) const { return m_sourceIDs[iBank]; }
  unsigned int               iSource( unsigned int sourceID ) const {
    auto i = std::find( m_sourceIDs.begin(), m_sourceIDs.end(), sourceID );
    return ( i - m_sourceIDs.begin() );
  }

  // First FTChannelID <-> banknumber
  LHCb::Detector::FTChannelID channelIDShift( unsigned int bankNumber ) const {
    return m_FTBankFirstChannel[bankNumber];
  }

  unsigned int bankNumber( LHCb::Detector::FTChannelID id ) const {
    auto it = std::find_if( m_FTBankFirstChannel.begin(), m_FTBankFirstChannel.end(),
                            [&id]( const auto& firstChan ) { return id < firstChan; } );
    return it - m_FTBankFirstChannel.begin() - 1u;
  }

  // SiPM <-> Link
  SiPMLinkPair findBankNumberAndIndex( LHCb::Detector::FTChannelID sipm ) const {
    for ( unsigned int bankNumber = 0; bankNumber < m_nBanks; ++bankNumber ) {
      for ( unsigned int linkIndex = 0; linkIndex < FTRawBank::BankProperties::NbLinksPerBank; ++linkIndex ) {
        if ( m_linkMap[bankNumber][linkIndex] == sipm ) return {bankNumber, linkIndex};
      }
    }
    assert( 0 && "Did not find the bank number and index for sipm I" );
    return {};
  };

  // Getter for index remapping array for SiPM -> linkID at specific bank number
  std::array<int, FTRawBank::BankProperties::NbLinksPerBank> getSiPMRemapping( unsigned int sourceID ) const {
    return m_SiPMMap[this->iSource( sourceID )];
  };

  // Getter for index remapping array for SiPM -> linkID at specific bank number
  std::array<int, FTRawBank::BankProperties::NbLinksPerBank> getSiPMRemappingFromIndex( unsigned int iRow ) const {
    return m_SiPMMap[iRow];
  };

  // Link <-> SiPM
  LHCb::Detector::FTChannelID getGlobalSiPMID( unsigned int sourceID, unsigned int link ) const {
    auto iRow = iSource( sourceID );
    assert( link < FTRawBank::BankProperties::NbLinksPerBank && "Link numbers should be smaller than 24." );
    return m_linkMap[iRow][link];
  };
  // Link <-> SiPM
  LHCb::Detector::FTChannelID getGlobalSiPMIDFromIndex( unsigned int iSource, unsigned int link ) const {
    return m_linkMap[iSource][link];
  };

private:
  void readFile0( const Gaudi::Algorithm* parent, const YAML::Node& rInfo );
  void readFile1( const Gaudi::Algorithm* parent, const YAML::Node& rInfo, const LHCb::Detector::DeLHCb& deLHCb );

  unsigned int                             m_nBanks;
  std::optional<unsigned int>              m_version;
  std::vector<LHCb::Detector::FTChannelID> m_FTBankFirstChannel;
  std::vector<LHCb::Detector::FTSourceID>  m_sourceIDs;

  std::array<std::array<LHCb::Detector::FTChannelID, FTRawBank::BankProperties::NbLinksPerBank>,
             FTRawBank::BankProperties::NbBanksMax>
      m_linkMap; // m_linkMap[bankNumber][linkID] = siPMID if plugged,  FTChannelID(-1) if not.
  std::array<std::array<int, FTRawBank::BankProperties::NbLinksPerBank>,
             FTRawBank::BankProperties::NbBanksMax>
      m_SiPMMap; // Array of arrays with index order to map SiPMID->linkID

  // Precalculated link information
  std::vector<bool> m_linkOrdering;
};

inline FTReadoutMap::FTReadoutMap( const Gaudi::Algorithm* parent ) {
  parent->debug() << "ReadoutMap condition absent from the conditions database." << endmsg;
  parent->info() << "Conditions DB is compatible with FT bank version 2, 3." << endmsg;
  std::array<int, FTRawBank::BankProperties::NbLinksPerBank> tmp;
  tmp.fill( -1 );
  m_SiPMMap.fill( tmp );
}

inline FTReadoutMap::FTReadoutMap( const Gaudi::Algorithm* parent, const YAML::Node& rInfo,
                                   const LHCb::Detector::DeLHCb& deLHCb ) {
  if ( rInfo["version"] ) {
    m_version = rInfo["version"].as<int>();
    parent->debug() << "Found ReadoutMap version " << *m_version << " in the conditions database." << endmsg;
    parent->info() << "Conditions DB is compatible with FT bank version 7 and 8." << endmsg;
  } else {
    m_version = 0;
    parent->debug() << "Did not find ReadoutMap version parameter in the conditions database. Assuming version 0."
                    << endmsg;
    parent->info() << "Conditions DB is compatible with FT bank version 4, 5, 6." << endmsg;
  }
  std::array<int, FTRawBank::BankProperties::NbLinksPerBank> tmp;
  tmp.fill( -1 );
  m_SiPMMap.fill( tmp );
  switch ( *m_version ) {
  case 0:
    this->readFile0( parent, rInfo );
    break;
  case 1:
    this->readFile1( parent, rInfo, deLHCb );
    break;
  default:
    throw GaudiException( "Unknown readout map version from conditions database : " + std::to_string( *m_version ),
                          __FILE__, StatusCode::FAILURE );
  }
}

inline std::set<unsigned int> FTReadoutMap::compatibleVersions() const {
  if ( !m_version ) {
    return {2u, 3u};
  } else if ( *m_version == 0 ) {
    return {0u, 4u, 5u, 6u};
  } else if ( *m_version == 1 ) {
    return {7u, 8u};
  } else {
    throw GaudiException( "Unknown conditions version: " + std::to_string( *m_version ), __FILE__,
                          StatusCode::FAILURE );
  }
}

inline void FTReadoutMap::compatible( const unsigned int bankVersion ) const {
  auto comp = compatibleVersions();
  if ( !comp.count( bankVersion ) ) {
    std::string s;
    if ( m_version ) {
      s = "Conditions version: " + std::to_string( *m_version );
    } else {
      s = "No readout condition";
    }
    throw GaudiException( s + " is incompatible with bank version " + std::to_string( bankVersion ), __FILE__,
                          StatusCode::FAILURE );
  }
}

// Due to different expected structures in the DDDB for the construction of the readoutmap that is needed,
// 2 different versions of the readFile function have been added:
//
// Decoding version <=6 uses readFile 0
// Decoding version >=7  uses readFile 1
//
// Check have been added to the decoding to see if the correct version of the decoding is used with the
// correct readFile version.
// Version 1 for decoding version >=7
inline void FTReadoutMap::readFile1( const Gaudi::Algorithm* parent, const YAML::Node& rInfo,
                                     const LHCb::Detector::DeLHCb& deLHCb ) {
  // Source IDs
  const auto& sourceIDs = rInfo["sourceIDs"].as<std::vector<int>>();
  m_sourceIDs.clear();
  m_sourceIDs.reserve( sourceIDs.size() );
#ifdef USE_DD4HEP
  // Warding against possibilities that no bank is activated.
  bool ignore = std::none_of( sourceIDs.begin(), sourceIDs.end(),
                              [&]( auto const& id ) { return deLHCb.tell40links( id ).has_value(); } );
  if ( ignore )
    parent->warning() << "No SourceID activated for this run. This may mean that conditions are too old to support "
                         "automatic link/bank deactivation. Activating all banks/links."
                      << endmsg;
  const auto activatedBank = [&deLHCb, &ignore]( int sourceID ) {
    return ( ignore || deLHCb.tell40links( sourceID ).has_value() );
  };
  const auto activatedLink = [&deLHCb, &ignore]( int sourceID, int iLink ) {
    return ( ignore || deLHCb.tell40links( sourceID )->isEnabled( iLink ) );
  };

#else
  // Put this to ignore unused parameter warning
  (void)deLHCb;
  const auto activatedBank = []( int /*sourceID*/ ) { return true; };
  const auto activatedLink = []( int /*sourceID*/, int /*iLink*/ ) { return true; };
#endif
  // Build the source IDs that are actually giving data
  for ( auto sourceID : sourceIDs )
    if ( activatedBank( sourceID ) )
      m_sourceIDs.push_back( LHCb::Detector::FTSourceID{static_cast<unsigned int>( sourceID )} );
  m_nBanks = m_sourceIDs.size();
  if ( m_nBanks != sourceIDs.size() )
    parent->info() << "Deactivated " << sourceIDs.size() - m_nBanks << " banks." << endmsg;

  // Make the map of hardware links with switchings
  const auto& allLinks = rInfo["LinkMap"].as<std::vector<int>>();
  if ( allLinks.size() != sourceIDs.size() * FTRawBank::BankProperties::NbLinksPerBank ) {
    throw GaudiException( "Readout map has a different number of links (" + std::to_string( allLinks.size() ) +
                              ") than expected (" +
                              std::to_string( sourceIDs.size() * FTRawBank::BankProperties::NbLinksPerBank ) + ")",
                          __FILE__, StatusCode::FAILURE );
  }
  unsigned int nLinksDeactivated( 0 );
  for ( unsigned int i = 0; i < m_sourceIDs.size(); i++ ) {
    // Find the corresponding line in the cabling map
    const auto   sourceID = m_sourceIDs[i];
    unsigned int line     = std::find( sourceIDs.begin(), sourceIDs.end(), m_sourceIDs[i] ) - sourceIDs.begin();
    // Start with linkID->SiPMID map
    std::array<LHCb::Detector::FTChannelID, FTRawBank::BankProperties::NbLinksPerBank> temp;
    for ( unsigned int j = 0; j < FTRawBank::BankProperties::NbLinksPerBank; j++ ) {
      temp[j] = ( LHCb::Detector::FTChannelID( allLinks[line * FTRawBank::BankProperties::NbLinksPerBank + j] ) );
      if ( temp[j] != LHCb::Detector::FTChannelID::kInvalidChannel() &&
           ( temp[j].station() != sourceID.station() || temp[j].layer() != sourceID.layer() ||
             temp[j].quarter() != sourceID.quarter() ) ) {
        parent->error() << "Link " << temp[j] << " cannot belong to sourceID " << sourceID << endmsg;
        throw GaudiException( "Incoherent readout map for bank n." + std::to_string( i ), __FILE__,
                              StatusCode::FAILURE );
      }
      if ( !( activatedLink( sourceID, j ) ) && temp[j] != LHCb::Detector::FTChannelID::kInvalidChannel() ) {
        nLinksDeactivated++;
        parent->debug() << "Deactivating link " << temp[j].toString() << endmsg;
        temp[j] = LHCb::Detector::FTChannelID::kInvalidChannel();
      }
    }
    // Now create the SiPMID->linkID index array
    std::array<int, FTRawBank::BankProperties::NbLinksPerBank> temp_SiPM; // SiPMID->linkID
    temp_SiPM.fill( -1 );
    auto sortedMap = temp;
    std::sort( sortedMap.begin(), sortedMap.end() );
    int place( 0 );
    int nInvalid( 0 );
    for ( int i_link = 0; i_link < FTRawBank::BankProperties::NbLinksPerBank; i_link++ ) {
      // Ignore invalids
      if ( sortedMap[i_link] == LHCb::Detector::FTChannelID::kInvalidChannel() ) {
        nInvalid++;
        continue;
      }
      // Look for the i_link-th elements
      int j( 0 );
      for ( ; j < FTRawBank::BankProperties::NbLinksPerBank; j++ ) {
        if ( sortedMap[i_link] == temp[j] ) break;
      }
      temp_SiPM[place] = j;
      place++;
    }
    m_linkMap[i] = temp;               // linkID->SiPMID
    m_SiPMMap[i] = temp_SiPM;          // SiPMID->linkID
    m_linkOrdering.push_back( false ); // TODO: check ordering there
  }
  if ( nLinksDeactivated != 0 ) parent->info() << "Deactivated " << nLinksDeactivated << " links." << endmsg;
}

// Version 0 for decoding versions <=6
inline void FTReadoutMap::readFile0( const Gaudi::Algorithm* parent, const YAML::Node& rInfo ) {
  parent->info() << "Building the readout map with version 0" << endmsg;
  const auto& stations     = rInfo["FTBankStation"].as<std::vector<int>>();
  const auto& layers       = rInfo["FTBankLayer"].as<std::vector<int>>();
  const auto& quarters     = rInfo["FTBankQuarter"].as<std::vector<int>>();
  const auto& firstModules = rInfo["FTBankFirstModule"].as<std::vector<int>>();
  const auto& firstMats    = rInfo["FTBankFirstMat"].as<std::vector<int>>();

  // Construct the first channel attribute
  m_nBanks = stations.size();
  m_FTBankFirstChannel.reserve( m_nBanks );
  for ( unsigned int i = 0; i < m_nBanks; i++ ) {
    m_FTBankFirstChannel.emplace_back(
        LHCb::Detector::FTChannelID::StationID( stations[i] ), LHCb::Detector::FTChannelID::LayerID( layers[i] ),
        LHCb::Detector::FTChannelID::QuarterID( quarters[i] ), LHCb::Detector::FTChannelID::ModuleID( firstModules[i] ),
        LHCb::Detector::FTChannelID::MatID( firstMats[i] ), 0u, 0u );
  }
}
