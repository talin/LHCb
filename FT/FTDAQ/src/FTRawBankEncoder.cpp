/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <bitset>
#include <cstdint>
#include <numeric>
#include <optional>

#include <GaudiAlg/GaudiAlgorithm.h>
#include <LHCbAlgs/Consumer.h>

#include <Event/FTCluster.h>
#include <Event/RawBank.h>
#include <Event/RawEvent.h>

// local
#include <FTDAQ/FTReadoutMap.h>

/** @class FTRawBankEncoder FTRawBankEncoder.cpp
 *  Encode the FTCLusters into raw banks
 *
 *  @author Olivier Callot, Lex Greeven, Louis Henry
 *  @date   2012-05-11
 */

namespace FTRawBank = LHCb::Detector::FT::RawBank;

struct FTRawBankEncoder
    : LHCb::Algorithm::Consumer<void( const LHCb::FTClusters&, const FTReadoutMap& ),
                                LHCb::DetDesc::usesBaseAndConditions<GaudiAlgorithm, FTReadoutMap>> {

  /// Standard constructor
  FTRawBankEncoder( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  void operator()( const LHCb::FTClusters& clusters, const FTReadoutMap& ) const override;

private:
  Gaudi::Property<std::string> m_outputLocation{this, "OutputLocation", LHCb::RawEventLocation::Default};
};

constexpr static int s_nbBanks        = FTRawBank::NbBanksMax;
constexpr static int s_nbLinksPerBank = FTRawBank::NbLinksPerBank;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTRawBankEncoder )

FTRawBankEncoder::FTRawBankEncoder( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"InputLocation", LHCb::FTClusterLocation::Default},
                 KeyValue{"ReadoutMapStorage", "AlgorithmSpecific-" + name + "-ReadoutMap"}} ) {}

StatusCode FTRawBankEncoder::initialize() {
  return Consumer::initialize().andThen(
      [&] { FTReadoutMap::addConditionDerivation( this, inputLocation<FTReadoutMap>() ); } );
}

void FTRawBankEncoder::operator()( const LHCb::FTClusters& clusters, const FTReadoutMap& readoutMap ) const {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Retrieved " << clusters.size() << " clusters" << endmsg;
  LHCb::RawEvent* event = getOrCreate<LHCb::RawEvent, LHCb::RawEvent>( m_outputLocation );

  // Incremented to deal with new numbering scheme
  // Check if encoding version corresponds with readout version.
  int codingVersion = 8;
  readoutMap.compatible( codingVersion );

  //== create the array of arrays of vectors with the proper size...
  std::array<std::vector<uint16_t>, s_nbBanks> sipmData;
  std::array<uint32_t, s_nbBanks>              headerData{};
  std::array<int, s_nbBanks* s_nbLinksPerBank> nClustersPerSipm = {0};
  for ( const auto& cluster : clusters ) {
    if ( cluster->isLarge() > 1 ) continue;

    LHCb::Detector::FTChannelID id           = cluster->channelID();
    LHCb::Detector::FTChannelID globalSiPMID = LHCb::Detector::FTChannelID( id.globalSiPMID() << 7 ); // FIXME:hardcoded
    auto                        bankAndLinkIndex = readoutMap.findBankNumberAndIndex( globalSiPMID );
    unsigned int                bankNumber       = bankAndLinkIndex.bankNumber;
    unsigned int                localLinkIndex   = bankAndLinkIndex.linkIndex;
    // Check if the globalSiPMID is available in the CONDDB. This should in principle never happen.
    if ( ( bankNumber == LHCb::Detector::FTChannelID::kInvalidChannelID() ) ||
         ( localLinkIndex == LHCb::Detector::FTChannelID::kInvalidChannelID() ) ) {
      error() << "Could not find SiPM " << globalSiPMID << " (" << globalSiPMID.channelID() << ") in CONDDB" << endmsg;
    }
    LHCb::Detector::FTChannelID localLinkID     = LHCb::Detector::FTChannelID( localLinkIndex, 0 );
    auto&                       data            = sipmData[bankNumber];
    unsigned int                globalLinkIndex = bankNumber * s_nbLinksPerBank + localLinkIndex;
    nClustersPerSipm[globalLinkIndex]++;
    // Truncate clusters when maximum per SiPM is reached
    if ( ( id.module() > LHCb::Detector::FTChannelID::ModuleID{0} &&
           nClustersPerSipm[globalLinkIndex] > FTRawBank::nbClusFFMaximum ) ||
         ( id.module() == LHCb::Detector::FTChannelID::ModuleID{0} &&
           nClustersPerSipm[globalLinkIndex] > FTRawBank::nbClusMaximum ) ) {
      if ( headerData[bankNumber] >> ( ( localLinkID >> 7 ) & 1 ) == 0 ) {
        headerData[bankNumber] += ( 1u << ( localLinkID >> 7 ) ); // set the truncation bit
      }
      continue;
    }

    data.push_back( ( localLinkIndex << FTRawBank::linkShift ) | ( id.channel() << FTRawBank::cellShift ) |
                    ( cluster->fractionBit() << FTRawBank::fractionShift ) |
                    ( ( cluster->isLarge() > 0 ) << FTRawBank::sizeShift ) );
    if ( msgLevel( MSG::VERBOSE ) ) {
      verbose() << format( "Bank%3d sipm%4d channel %4d frac %3.1f isLarge %2d code %4.4x", bankNumber, localLinkID,
                           id.channel(), cluster->fraction(), cluster->isLarge(), data.back() )
                << endmsg;
    }
  }

  //== Now build the banks: We need to put the 16 bits content into 32 bits words.
  for ( unsigned int iBank = 0; iBank < sipmData.size(); ++iBank ) {
    LHCb::Detector::FTSourceID sourceID = readoutMap.sourceID( iBank );
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "*** Bank " << iBank << " with SourceID: " << sourceID << endmsg;
    auto                      words = sipmData[iBank].size();
    std::vector<unsigned int> bank;
    bank.reserve( ( words + 1 ) / 2 + 1 );
    bank.emplace_back( headerData[iBank] ); // insert the header
    std::optional<unsigned int> buf;

    // Sort the data
    std::sort( sipmData[iBank].begin(), sipmData[iBank].end(), []( uint16_t data1, uint16_t data2 ) {
      auto decompose = []( uint16_t data ) {
        return std::tuple{( data >> FTRawBank::linkShift ) & FTRawBank::linkMask,
                          ( data >> FTRawBank::cellShift ) & FTRawBank::cellMask};
      };
      return decompose( data1 ) < decompose( data2 );
    } ); // close sort

    for ( const auto& cluster : sipmData[iBank] ) {
      if ( !buf ) {
        buf = cluster;
      } else {
        bank.emplace_back( *buf | ( static_cast<unsigned int>( cluster ) << 16 ) ); // FIXME: hardcoded constant
        buf = {};
      }
    }
    if ( buf ) bank.emplace_back( *buf );
    if ( msgLevel( MSG::VERBOSE ) ) {
      for ( const auto& [offset, d] : LHCb::range::enumerate( bank ) ) {
        verbose() << format( "    at %5d data %8.8x", offset, d ) << endmsg;
      }
    }
    event->addBank( sourceID, LHCb::RawBank::FTCluster, codingVersion, bank );
  }
}
