/***************************************************************************** \
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <range/v3/view/subrange.hpp>
#include <range/v3/view/transform.hpp>

#include "Gaudi/Accumulators/Histogram.h"
#include <Gaudi/Property.h>
#include <GaudiAlg/GaudiAlgorithm.h>

#include <DetDesc/Condition.h>
#include <Detector/FT/FTSourceID.h>
#include <Event/FTLiteCluster.h>
#include <Event/RawBank.h>
#include <LHCbAlgs/Transformer.h>

#include <FTDAQ/FTDAQHelper.h>
#include <FTDAQ/FTReadoutMap.h>

#include <algorithm>
#include <netinet/in.h>

#include "Kernel/STLExtensions.h"
#include <GaudiKernel/StdArrayAsProperty.h>

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;
namespace FTRawBank  = LHCb::Detector::FT::RawBank;

/** @class FTRawBankDecoder FTRawBankDecoder.h
 *  Decode the FT raw bank into FTLiteClusters
 *
 *  @author Olivier Callot
 *  @date   2012-05-11
 */
class FTRawBankDecoder
    : public LHCb::Algorithm::Transformer<FTLiteClusters( const EventContext&, const LHCb::RawBank::View&,
                                                          const FTReadoutMap& ),
                                          LHCb::DetDesc::usesBaseAndConditions<GaudiAlgorithm, FTReadoutMap>> {
public:
  /// Standard constructor
  FTRawBankDecoder( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  FTLiteClusters operator()( const EventContext& evtCtx, const LHCb::RawBank::View& banks,
                             const FTReadoutMap& readoutMap ) const override;

private:
  // Force decoding of v5 as v4 for testing of v5 encoding; expert use only
  Gaudi::Property<bool> m_decodeV5AsV4{this, "DecodeV5AsV4", false, "Decode v5 as v4"};

  ConditionAccessor<DeFT> m_det{this, DeFTDetectorLocation::Default};

  Gaudi::Property<std::array<std::vector<double>, LHCb::Detector::FT::nQuartersTotal>> m_ChannelsToSkip{
      this, "ChannelsToSkip", {{}}, "a list of PseudoChannels in Quarters to skip"};

  template <unsigned int version>
  FTLiteClusters decode( const EventContext& evtCtx, LHCb::RawBank::View, const FTReadoutMap& readoutMap,
                         unsigned int nClusters ) const;

  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_misordered{this, "Misordered large cluster"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_corrupt1{
      this, "Possibly corrupt data (type 1). Ignoring the cluster."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_corrupt2{
      this, "Possibly corrupt data (type 2). Ignoring the cluster."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_oddBank{this, "Odd bank."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_emptyBank{this, "Totally empty bank."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_absurdBank{
      this, "Bank containing more clusters than physically possible."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_wrongLocalLink{
      this, "Local link larger than physical limit. Bank may be corrupted."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_wrongLink{
      this, "Received data from a link that should be disabled. Fix the DB."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_nonExistingModule{
      this, "Skipping cluster(s) for non-existing module."};
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_oddBank_hist{
      this,
      "odd_bank",
      "Odd bank error;iBank",
      {FTRawBank::BankProperties::NbBanksMax, -0.5, FTRawBank::BankProperties::NbBanksMax - 0.5}};
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_emptyBank_hist{
      this,
      "empty_bank",
      "Empty bank error;iBank",
      {FTRawBank::BankProperties::NbBanksMax, -0.5, FTRawBank::BankProperties::NbBanksMax - 0.5}};
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_absurdBank_hist{
      this,
      "absurd_bank",
      "Absurd bank error;iBank",
      {FTRawBank::BankProperties::NbBanksMax, -0.5, FTRawBank::BankProperties::NbBanksMax - 0.5}};
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_wrongLocalLink_hist{
      this,
      "wrong_local_link",
      "Wrong local link error;iBank",
      {FTRawBank::BankProperties::NbBanksMax, -0.5, FTRawBank::BankProperties::NbBanksMax - 0.5}};
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_wrongLink_hist{
      this,
      "wrong_link",
      "Wrong link error;iLink",
      {FTRawBank::BankProperties::NbLinksMax, -0.5, FTRawBank::BankProperties::NbLinksMax - 0.5}};
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_misordered_hist{
      this,
      "misordered",
      "Misordered large clusters;iLink",
      {FTRawBank::BankProperties::NbLinksMax, -0.5, FTRawBank::BankProperties::NbLinksMax - 0.5}};
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_corruptCluster1_hist{
      this,
      "corrupt_cluster1",
      "Corrupt cluster (type 1);iLink",
      {FTRawBank::BankProperties::NbLinksMax, -0.5, FTRawBank::BankProperties::NbLinksMax - 0.5}};
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_corruptCluster2_hist{
      this,
      "corrupt_cluster2",
      "Corrupt cluster (type 2);iLink",
      {FTRawBank::BankProperties::NbLinksMax, -0.5, FTRawBank::BankProperties::NbLinksMax - 0.5}};
};

//-----------------------------------------------------------------------------
// Implementation file for class : FTRawBankDecoder
//
// 2012-05-11 : Olivier Callot
//-----------------------------------------------------------------------------

namespace {
  constexpr unsigned channelInBank( short int c ) { return ( c >> FTRawBank::cellShift ); }

  constexpr unsigned getLinkInBank( short int c ) { return ( ( c >> FTRawBank::linkShift ) ); }

  constexpr int cell( short int c ) { return ( c >> FTRawBank::cellShift ) & FTRawBank::cellMaximum; }

  constexpr int fraction( short int c ) { return ( c >> FTRawBank::fractionShift ) & FTRawBank::fractionMaximum; }

  constexpr bool cSize( short int c ) { return ( c >> FTRawBank::sizeShift ) & FTRawBank::sizeMaximum; }

  struct sourceID_t {
    auto operator()( LHCb::RawBank const* b ) const { return b->sourceID() & 0x7FF; }
  };

  constexpr auto by_sourceID = sourceID_t{};

  template <typename Projection>
  auto sorted( LHCb::RawBank::View banks, Projection proj ) {
    auto orderedBanks = std::vector<const LHCb::RawBank*>( banks.begin(), banks.end() );
    std::sort( orderedBanks.begin(), orderedBanks.end(),
               [proj]( const LHCb::RawBank* lhs, const LHCb::RawBank* rhs ) { return proj( lhs ) < proj( rhs ); } );
    return orderedBanks;
  }

} // namespace

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTRawBankDecoder )

FTRawBankDecoder::FTRawBankDecoder( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"RawBanks", "DAQ/RawBanks/FTCluster"},
                    KeyValue{"ReadoutMapStorage", "AlgorithmSpecific-" + name + "-ReadoutMap"}},
                   KeyValue{"OutputLocation", LHCb::FTLiteClusterLocation::Default} ) {}

StatusCode FTRawBankDecoder::initialize() {
  return Transformer::initialize().andThen(
      [&] { FTReadoutMap::addConditionDerivation( this, inputLocation<FTReadoutMap>() ); } );
}

template <>
FTLiteClusters FTRawBankDecoder::decode<0>( const EventContext& evtCtx, LHCb::RawBank::View banks,
                                            const FTReadoutMap& readoutMap, unsigned int nClusters ) const {
  FTLiteClusters clus{nClusters, LHCb::getMemResource( evtCtx )};
  for ( const LHCb::RawBank* bank : banks ) { // Iterates over the banks

    LHCb::Detector::FTChannelID offset  = readoutMap.channelIDShift( bank->sourceID() );
    auto                        quarter = offset.globalQuarterIdx();

    auto make_cluster = [&clus, &quarter]( LHCb::Detector::FTChannelID chan, int fraction, int size ) {
      clus.addHit( std::forward_as_tuple( chan, fraction, size ), quarter );
    };

    // Loop over clusters
    auto range = bank->range<uint16_t>();
    range      = range.subspan( 4 ); // skip first 64b of header;
    if ( !range.empty() && range[range.size() - 1] == 0 )
      range = range.first( range.size() - 1 ); // remove padding at end
    for ( unsigned short int c : range ) {
      c = htons( c );

      auto channel = LHCb::Detector::FTChannelID{offset + channelInBank( c )};
      make_cluster( channel, fraction( c ), cSize( c ) );
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "quarter " << quarter << " channel  " << channel << " size " << cSize( c ) << " fraction "
                << fraction( c ) << endmsg;
    }
  }
  return clus;
}

template <>
FTLiteClusters FTRawBankDecoder::decode<6>( const EventContext& evtCtx, LHCb::RawBank::View banks,
                                            const FTReadoutMap& readoutMap, unsigned int nClusters ) const {
  FTLiteClusters clus{nClusters, LHCb::getMemResource( evtCtx )};
  for ( const LHCb::RawBank* bank : banks ) { // Iterates over the banks
    LHCb::Detector::FTChannelID offset  = readoutMap.channelIDShift( bank->sourceID() );
    auto                        quarter = offset.globalQuarterIdx();

    // Define Lambda functions to be used in loop
    auto make_cluster = [&clus, &quarter]( unsigned int chan, int fraction, int size ) {
      clus.addHit( std::forward_as_tuple( LHCb::Detector::FTChannelID{chan}, fraction, size ), quarter );
    };

    // Make clusters between two channels
    auto make_clusters = [&]( unsigned firstChannel, short int c, short int c2 ) {
      unsigned int widthClus = ( cell( c2 ) - cell( c ) + 2 ); // lastCh-firstCh+4/2

      // fragmented clusters, size > 2*max size
      // only edges were saved, add middles now
      if ( widthClus > 8 ) {
        // add the first edge cluster, and then the middle clusters
        unsigned int i = 0;
        for ( ; i < widthClus - 4; i += 4 ) {
          // all middle clusters will have same size as the first cluster,
          // for max size 4, fractions is always 1
          make_cluster( firstChannel + i, 1, 0 );
        }

        // add the last edge
        unsigned int lastChannel = firstChannel + i + std::floor( ( widthClus - i - 1 ) / 2 ) - 1;
        make_cluster( lastChannel, ( widthClus - 1 ) % 2, 0 );
      } else { // big cluster size upto size 8
        make_cluster( firstChannel + ( widthClus - 1 ) / 2 - 1, ( widthClus - 1 ) % 2, widthClus );

      } // end if adjacent clusters
    };  // End lambda make_clusters

    // loop over clusters
    auto clusters = bank->range<short int>();
    clusters      = clusters.subspan( 2 ); // skip first 32b of header
    if ( !clusters.empty() && clusters.back() == 0 )
      clusters = clusters.first( clusters.size() - 1 ); // Remove padding at the end
    while ( !clusters.empty() ) {                       // loop over the clusters
      auto channel = LHCb::Detector::FTChannelID{offset + channelInBank( clusters[0] )};
      if ( !cSize( clusters[0] ) ) // Not flagged as large
        make_cluster( channel, fraction( clusters[0] ), LHCb::Detector::FT::RawBank::maxClusterWidth );

      else {
        // last cluster in bank or in sipm
        if ( clusters.size() == 1 || getLinkInBank( clusters[0] ) != getLinkInBank( clusters[1] ) ) {
          make_cluster( channel, fraction( clusters[0] ), 0 );
        } else if ( fraction( clusters[0] ) ) {
          if ( cSize( clusters[1] ) && !fraction( clusters[1] ) ) { // this should always be true
            make_clusters( channel, clusters[0], clusters[1] );
            clusters = clusters.subspan( 1 );
          } else { // this should never happen,
            ++m_corrupt1;
          }
        }
      }
      clusters = clusters.subspan( 1 );
    }
  } // end loop over rawbanks
  clus.setOffsets();
  return clus;
}

template <>
FTLiteClusters FTRawBankDecoder::decode<4>( const EventContext& evtCtx, LHCb::RawBank::View banks,
                                            const FTReadoutMap& readoutMap, unsigned int nClusters ) const {
  FTLiteClusters clus{nClusters, LHCb::getMemResource( evtCtx )};
  for ( const LHCb::RawBank* bank : banks ) { // Iterates over the banks
    LHCb::Detector::FTChannelID offset = readoutMap.channelIDShift( bank->sourceID() );
    auto                        first  = bank->begin<short int>() + 2; // skip first 32b of the header
    auto                        last   = bank->end<short int>();

    if ( *( last - 1 ) == 0 && first < last ) --last; // Remove padding at the end

    auto r = ranges::make_subrange( first, last ) |
             ranges::views::transform( [&offset]( unsigned short int c ) -> LHCb::FTLiteCluster {
               return {LHCb::Detector::FTChannelID{offset + channelInBank( c )}, fraction( c ), ( cSize( c ) ? 0 : 4 )};
             } );
    clus.insert( r.begin(), r.end(), offset.globalQuarterIdx() );
  }
  return clus;
}

template <>
FTLiteClusters FTRawBankDecoder::decode<5>( const EventContext& evtCtx, LHCb::RawBank::View banks,
                                            const FTReadoutMap& readoutMap, unsigned int nClusters ) const {
  FTLiteClusters clus{nClusters, LHCb::getMemResource( evtCtx )};
  for ( const LHCb::RawBank* bank : banks ) { // Iterates over the banks
    LHCb::Detector::FTChannelID offset  = readoutMap.channelIDShift( bank->sourceID() );
    auto                        quarter = offset.globalQuarterIdx();

    // Define Lambda functions to be used in loop
    auto make_cluster = [&clus, &quarter]( unsigned chan, int fraction, int size ) {
      clus.addHit( std::forward_as_tuple( LHCb::Detector::FTChannelID{chan}, fraction, size ), quarter );
    };

    // Make clusters between two channels
    auto make_clusters = [&]( unsigned firstChannel, short int c, short int c2 ) {
      unsigned int delta = ( cell( c2 ) - cell( c ) );

      // fragmented clusters, size > 2*max size
      // only edges were saved, add middles now
      if ( delta > LHCb::Detector::FT::RawBank::maxClusterWidth ) {
        // add the first edge cluster, and then the middle clusters
        for ( unsigned int i = LHCb::Detector::FT::RawBank::maxClusterWidth; i < delta;
              i += LHCb::Detector::FT::RawBank::maxClusterWidth ) {
          // all middle clusters will have same size as the first cluster,
          // so re-use the fraction
          make_cluster( firstChannel + i, fraction( c ), 0 );
        }
        // add the last edge
        make_cluster( firstChannel + delta, fraction( c2 ), 0 );
      } else { // big cluster size upto size 8
        unsigned int widthClus = 2 * delta - 1 + fraction( c2 );
        make_cluster( firstChannel + ( widthClus - 1 ) / 2 -
                          int( ( LHCb::Detector::FT::RawBank::maxClusterWidth - 1 ) / 2 ),
                      ( widthClus - 1 ) % 2, widthClus );
      } // end if adjacent clusters
    };  // End lambda make_clusters

    // loop over clusters
    auto it   = bank->begin<short int>() + 2; // skip first 32b of header
    auto last = bank->end<short int>();
    if ( *( last - 1 ) == 0 ) --last; // Remove padding at the end
    for ( ; it < last; ++it ) {       // loop over the clusters
      unsigned short int c       = *it;
      auto               channel = LHCb::Detector::FTChannelID{offset + channelInBank( c )};

      if ( !cSize( c ) || it + 1 == last ) // No size flag or last cluster
        make_cluster( channel, fraction( c ), LHCb::Detector::FT::RawBank::maxClusterWidth );
      else { // Flagged or not the last one.
        unsigned c2 = *( it + 1 );
        if ( cSize( c2 ) && getLinkInBank( c ) == getLinkInBank( c2 ) ) {
          make_clusters( channel, c, c2 );
          ++it;
        } else {
          make_cluster( channel, fraction( c ), LHCb::Detector::FT::RawBank::maxClusterWidth );
        }
      }
    }
  } // end loop over rawbanks
  clus.setOffsets();

  return clus;
}

template <unsigned int vrsn>
FTLiteClusters FTRawBankDecoder::decode( const EventContext& evtCtx, LHCb::RawBank::View banks,
                                         const FTReadoutMap& readoutMap, unsigned int nClusters ) const {
  static_assert( vrsn == 2 || vrsn == 3 || vrsn == 7 || vrsn == 8 );

  if constexpr ( vrsn == 3u || vrsn == 2u ) {

    FTLiteClusters clus{nClusters, LHCb::getMemResource( evtCtx )};
    for ( const LHCb::RawBank* bank : banks ) {
      auto source  = static_cast<unsigned>( bank->sourceID() );
      auto station = LHCb::Detector::FTChannelID::StationID{source / 16 + 1u};
      auto layer   = LHCb::Detector::FTChannelID::LayerID{( source & 12 ) / 4};
      auto quarter = LHCb::Detector::FTChannelID::QuarterID{source & 3};

      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << "source " << source << " station " << to_unsigned( station ) << " layer " << to_unsigned( layer )
                  << " quarter " << to_unsigned( quarter ) << " size " << bank->size() << endmsg;

      auto first = bank->begin<short int>();
      auto last  = bank->end<short int>();

      while ( first != last ) {
        int sipmHeader = *first++;
        if ( first == last && sipmHeader == 0 ) continue; // padding at the end...
        unsigned modulesipm = sipmHeader >> 4;
        auto     module     = LHCb::Detector::FTChannelID::ModuleID{modulesipm >> 4};
        auto     mat        = LHCb::Detector::FTChannelID::MatID{( modulesipm & 15 ) >> 2};
        unsigned sipm       = modulesipm & 3;
        int      nClus      = sipmHeader & 15;

        if ( msgLevel( MSG::VERBOSE ) && nClus > 0 )
          verbose() << " Module " << to_unsigned( module ) << " mat " << to_unsigned( mat ) << " SiPM " << sipm
                    << " nClusters " << nClus << endmsg;

        if ( nClus > std::distance( first, last ) ) {
          warning() << "Inconsistent size of rawbank. #clusters in header=" << nClus
                    << ", #clusters in bank=" << std::distance( first, last ) << endmsg;

          throw GaudiException( "Inconsistent size of rawbank", "FTRawBankDecoder", StatusCode::FAILURE );
        }

        if ( to_unsigned( module ) > 5 ) {
          ++m_nonExistingModule;
          first += nClus;
          continue;
        }

        if constexpr ( vrsn == 3u ) {

          for ( auto it = first; it < first + nClus; ++it ) {
            short int c        = *it;
            unsigned  channel  = c & 127;
            int       fraction = ( c >> 7 ) & 1;
            bool      cSize    = ( c >> 8 ) & 1;

            // not the last cluster
            if ( !cSize && it < first + nClus - 1 ) {
              short int c2     = *( it + 1 );
              bool      cSize2 = ( c2 >> 8 ) & 1;

              if ( !cSize2 ) { // next cluster is not last fragment
                clus.addHit( std::forward_as_tuple(
                                 LHCb::Detector::FTChannelID{station, layer, quarter, module, mat, sipm, channel},
                                 fraction, LHCb::Detector::FT::RawBank::maxClusterWidth ),
                             bank->sourceID() );

                if ( msgLevel( MSG::VERBOSE ) ) {
                  verbose() << format( "size<=4  channel %4d frac %3d size %3d code %4.4x", channel, fraction, cSize,
                                       c )
                            << endmsg;
                }
              } else { // fragmented cluster, last edge found
                unsigned channel2  = c2 & 127;
                int      fraction2 = ( c2 >> 7 ) & 1;

                unsigned int diff = ( channel2 - channel );

                if ( diff > 128 ) {
                  error() << "something went terribly wrong here first fragment: " << channel
                          << " second fragment: " << channel2 << endmsg;
                  throw GaudiException( "There is an inconsistency between Encoder and Decoder!", "FTRawBankDecoder",
                                        StatusCode::FAILURE );
                }
                // fragemted clusters, size > 2*max size
                // only edges were saved, add middles now
                if ( diff > LHCb::Detector::FT::RawBank::maxClusterWidth ) {

                  // add the first edge cluster
                  clus.addHit( std::forward_as_tuple(
                                   LHCb::Detector::FTChannelID{station, layer, quarter, module, mat, sipm, channel},
                                   fraction, 0 ),
                               bank->sourceID() ); // pseudoSize=0

                  if ( msgLevel( MSG::VERBOSE ) ) {
                    verbose() << format( "first edge cluster %4d frac %3d size %3d code %4.4x", channel, fraction,
                                         cSize, c )
                              << endmsg;
                  }

                  for ( unsigned int i = LHCb::Detector::FT::RawBank::maxClusterWidth; i < diff;
                        i += LHCb::Detector::FT::RawBank::maxClusterWidth ) {
                    // all middle clusters will have same size as the first cluster,
                    // so use same fraction
                    clus.addHit( std::forward_as_tuple( LHCb::Detector::FTChannelID{station, layer, quarter, module,
                                                                                    mat, sipm, channel + i},
                                                        fraction, 0 ),
                                 bank->sourceID() );

                    if ( msgLevel( MSG::VERBOSE ) ) {
                      verbose() << format( "middle cluster %4d frac %3d size %3d code %4.4x", channel + i, fraction,
                                           cSize, c )
                                << " added " << diff << endmsg;
                    }
                  }

                  // add the last edge
                  clus.addHit( std::forward_as_tuple(
                                   LHCb::Detector::FTChannelID{station, layer, quarter, module, mat, sipm, channel2},
                                   fraction2, 0 ),
                               bank->sourceID() );

                  if ( msgLevel( MSG::VERBOSE ) ) {
                    verbose() << format( "last edge cluster %4d frac %3d size %3d code %4.4x", channel2, fraction2,
                                         cSize2, c2 )
                              << endmsg;
                  }
                } else { // big cluster size upto size 8
                  unsigned int firstChan = channel - int( ( LHCb::Detector::FT::RawBank::maxClusterWidth - 1 ) / 2 );
                  unsigned int widthClus = 2 * diff - 1 + fraction2;

                  unsigned int clusChanPosition = firstChan + ( widthClus - 1 ) / 2;
                  int          frac             = ( widthClus - 1 ) % 2;

                  // add the new cluster = cluster1+cluster2
                  clus.addHit( std::forward_as_tuple( LHCb::Detector::FTChannelID{station, layer, quarter, module, mat,
                                                                                  sipm, clusChanPosition},
                                                      frac, widthClus ),
                               bank->sourceID() );

                  if ( msgLevel( MSG::VERBOSE ) ) {
                    verbose() << format( "combined cluster %4d frac %3d size %3d code %4.4x", channel, fraction, cSize,
                                         c )
                              << endmsg;
                  }
                } // end if adjacent clusters
                ++it;
              }    // last edge foud
            }      // not the last cluster
            else { // last cluster, so nothing we can do
              clus.addHit( std::forward_as_tuple(
                               LHCb::Detector::FTChannelID{station, layer, quarter, module, mat, sipm, channel},
                               fraction, LHCb::Detector::FT::RawBank::maxClusterWidth ),
                           bank->sourceID() );

              if ( msgLevel( MSG::VERBOSE ) ) {
                verbose() << format( "size<=4  channel %4d frac %3d size %3d code %4.4x", channel, fraction, cSize, c )
                          << endmsg;
              }
            }    // last cluster added
          }      // end loop over clusters in one sipm
        } else { // bank vrsn == 2
          static_assert( vrsn == 2 );
          // normal clustering without any modification to clusters, should work for encoder=2
          for ( auto it = first; it < first + nClus; ++it ) {
            short int c        = *it;
            unsigned  channel  = ( c >> 0 ) & 127;
            int       fraction = ( c >> 7 ) & 1;
            int       cSize    = ( c >> 8 ) & 1;
            clus.addHit(
                std::forward_as_tuple( LHCb::Detector::FTChannelID{station, layer, quarter, module, mat, sipm, channel},
                                       fraction, ( cSize ? 0 : LHCb::Detector::FT::RawBank::maxClusterWidth ) ),
                bank->sourceID() );
          }
        }
        first += nClus;
      } // end loop over sipms
    }   // end loop over rawbanks
    clus.setOffsets();
    return clus;
  }

  if constexpr ( vrsn == 7u || vrsn == 8u ) {

    std::array<boost::container::small_vector<LHCb::FTLiteCluster,
                                              FTRawBank::nbClusMaximum * FTRawBank::BankProperties::NbBanksPerQuarter>,
               LHCb::Detector::FT::nQuartersTotal>
                   clus;
    FTLiteClusters sortedClus{nClusters, LHCb::getMemResource( evtCtx )};
    // Sort the banks now by x
    for ( const LHCb::RawBank* bank : sorted( banks, by_sourceID ) ) { // Iterates over the banks, sorted by SourceID
      auto sourceID = bank->sourceID();
      auto iRow     = readoutMap.iSource( sourceID );
      if ( iRow >= readoutMap.nBanks() ) {
        error() << "Could not find the sourceID " << iRow << " "
                << toString( LHCb::Detector::FTSourceID( bank->sourceID() ) ) << " in the readout map" << endmsg;
        continue;
      }

      auto clusters = bank->range<short int>();
      if ( bank->range<std::byte>().size() % 2 ) {
        ++m_oddBank;
        ++m_oddBank_hist[iRow];
        continue;
      }

      if ( ( vrsn == 7u && clusters.size() < 2 ) || ( vrsn == 8u && clusters.empty() ) ) {
        ++m_emptyBank;
        ++m_emptyBank_hist[iRow];
        continue;
      }

      if ( clusters.size() > FTRawBank::BankProperties::NbLinksPerBank * FTRawBank::nbClusMaximum ) {
        ++m_absurdBank;
        ++m_absurdBank_hist[iRow];
        continue;
      }
      // Vector to temporarily store clusters
      std::array<boost::container::small_vector<LHCb::FTLiteCluster, FTRawBank::nbClusMaximum>,
                 FTRawBank::BankProperties::NbLinksPerBank>
                                  clustersInBankPerLinkID;
      unsigned int                localLinkIndex = 0;
      LHCb::Detector::FTChannelID globalSiPMID;

      // Define Lambda functions to be used in loop
      auto make_cluster = [&globalSiPMID, &clustersInBankPerLinkID, &localLinkIndex]( unsigned chan, int fraction,
                                                                                      int size ) {
        clustersInBankPerLinkID[localLinkIndex].emplace_back( LHCb::Detector::FTChannelID( globalSiPMID + chan ),
                                                              fraction, size );
      };

      // Make clusters between two channels
      auto make_clusters = [&]( unsigned firstChannel, short int c, short int c2 ) {
        unsigned int widthClus = ( cell( c2 ) - cell( c ) + 2 ); // lastCh-firstCh+4/2

        // fragmented clusters, size > 2*max size
        // only edges were saved, add middles now
        if ( widthClus > 2 * LHCb::Detector::FT::RawBank::maxClusterWidth ) {
          // add the first edge cluster, and then the middle clusters
          unsigned int i = 0;
          for ( ; i < widthClus - LHCb::Detector::FT::RawBank::maxClusterWidth;
                i += LHCb::Detector::FT::RawBank::maxClusterWidth ) {
            // all middle clusters will have same size as the first cluster,
            // for max size 4, fractions is always 1
            make_cluster( firstChannel + i, 1, 0 );
          }

          // add the last edge
          unsigned int lastChannel = firstChannel + i + std::floor( ( widthClus - i - 1 ) / 2 ) - 1;
          make_cluster( lastChannel, ( widthClus - 1 ) % 2, 0 );
        } else { // big cluster size upto size 8

          make_cluster( firstChannel + ( widthClus - 1 ) / 2 - 1, ( widthClus - 1 ) % 2, widthClus );

        } // end if adjacent clusters
      };  // End lambda make_clusters

      clusters = clusters.subspan( 2 ); // skip first 32b of header
      if ( !clusters.empty() && clusters.back() == 0 )
        clusters = clusters.first( clusters.size() - 1 ); // Remove padding at the end
      while ( !clusters.empty() ) {                       // loop over the clusters
        unsigned channel = channelInBank( clusters[0] );
        localLinkIndex   = getLinkInBank( clusters[0] );
        if ( localLinkIndex >= FTRawBank::BankProperties::NbLinksPerBank ) {
          ++m_wrongLocalLink;
          ++m_wrongLocalLink_hist[iRow];
          clusters = clusters.subspan( 1 );
          continue;
        }
        globalSiPMID = readoutMap.getGlobalSiPMIDFromIndex( iRow, localLinkIndex );
        if ( globalSiPMID == LHCb::Detector::FTChannelID::kInvalidChannel() ) {
          ++m_wrongLink;
          ++m_wrongLink_hist[iRow * FTRawBank::BankProperties::NbLinksPerBank + localLinkIndex];
          clusters = clusters.subspan( 1 );
          continue;
        }
        unsigned channelInSiPM = LHCb::Detector::FTChannelID( channel ).channel();
        if ( !cSize( clusters[0] ) ) { // Not flagged as large
          make_cluster( channelInSiPM, fraction( clusters[0] ), LHCb::Detector::FT::RawBank::maxClusterWidth );
        } else { // Large cluster

          if constexpr ( vrsn == 7u ) {
            // last cluster in bank or in sipm
            if ( clusters.size() == 1 || getLinkInBank( clusters[0] ) != getLinkInBank( clusters[1] ) ) {
              make_cluster( channelInSiPM, fraction( clusters[0] ), 0 );
            }
            // flagged as first cluster of a large one
            else {
              if ( !cSize( clusters[1] ) ) { // this should always be true
                ++m_corrupt1;
                ++m_corruptCluster1_hist[iRow * FTRawBank::BankProperties::NbLinksPerBank + localLinkIndex];
              } else if ( fraction( clusters[0] ) ) {
                // Contrary to v8, size 4 means that the first fragment of a large cluster always has fraction 0
                ++m_corrupt2;
                ++m_corruptCluster2_hist[iRow * FTRawBank::BankProperties::NbLinksPerBank + localLinkIndex];
              } else if ( clusters[1] < clusters[0] ) {
                // FIXME
                // Clusters should come in order. This is behaviour puts into question the whole concept of large
                // clusters
                //(imagine we got two large clusters and their beginning/end get scrambled)
                // For the time being, we will assume this is a simple local swap and keep the clusters
                ++m_misordered;
                ++m_misordered_hist[iRow * FTRawBank::BankProperties::NbLinksPerBank + localLinkIndex];
                //        make_clusters( channelInSiPM, clusters[1], clusters[0] );
                clusters = clusters.subspan( 1 );
              } else {
                make_clusters( channelInSiPM, clusters[0], clusters[1] );
                clusters = clusters.subspan( 1 );
              }
            }
          }

          if constexpr ( vrsn == 8u ) {
            // last cluster in bank or in sipm
            if ( clusters.size() == 1 || getLinkInBank( clusters[0] ) != getLinkInBank( clusters[1] ) ) {
              make_cluster( channelInSiPM, fraction( clusters[0] ), 0 );
            }
            // flagged as first cluster of a large one
            else if ( fraction( clusters[0] ) ) {
              if ( !cSize( clusters[1] ) || fraction( clusters[1] ) ) {
                // this should never happen: cannot find the end of the large cluster, flagged with Size 1 and
                // Fraction 0
                ++m_corrupt1;
                ++m_corruptCluster1_hist[iRow * FTRawBank::BankProperties::NbLinksPerBank + localLinkIndex];
                clusters = clusters.subspan( 1 );
              } else if ( clusters[1] < clusters[0] ) {
                // FIXME
                // Clusters should come in order. This is behaviour puts into question the whole concept of large
                // clusters
                //(imagine we got two large clusters and their beginning/end get scrambled)
                // For the time being, we will assume this is a simple local swap and keep the clusters
                ++m_misordered;
                ++m_misordered_hist[iRow * FTRawBank::BankProperties::NbLinksPerBank + localLinkIndex];
                //	      make_clusters( channelInSiPM, clusters[1], clusters[0] );
                clusters = clusters.subspan( 1 );
              } else { // this should always be true
                make_clusters( channelInSiPM, clusters[0], clusters[1] );
                clusters = clusters.subspan( 1 );
              }
            }
          }
        }
        clusters = clusters.subspan( 1 );
      }

      const auto& det = m_det.get();

      // Sort clusters within bank using the SiPMID->linkID index array from FTReadoutMap
      auto SiPMindices = readoutMap.getSiPMRemappingFromIndex( iRow );
      for ( auto index : SiPMindices ) {
        if ( index == -1 ) break; // stop when padding is reached
        // Now loop over clusters within link (ordered by definition) and add to object
        auto clusterVector = clustersInBankPerLinkID[index];
        for ( auto clusterData : clusterVector ) {
          if ( msgLevel( MSG::DEBUG ) ) debug() << clusterData.channelID() << endmsg;

          unsigned int globalQuarterIdx = clusterData.channelID().globalQuarterIdx();
          int          pseudoChannelIdx = -1;
          const auto   mod              = det.findModule( clusterData.channelID() );

          if ( mod ) { pseudoChannelIdx = mod->pseudoChannel( clusterData.channelID() ); }
          if ( !m_ChannelsToSkip.empty() ) {
            if ( !m_ChannelsToSkip[globalQuarterIdx].empty() ) {
              if ( std::binary_search( m_ChannelsToSkip[globalQuarterIdx].begin(),
                                       m_ChannelsToSkip[globalQuarterIdx].end(), pseudoChannelIdx ) ) {
                continue;
              }
            }
          }

          clus[clusterData.channelID().globalQuarterIdx()].emplace_back(
              clusterData.channelID(), clusterData.fractionBit(), clusterData.pseudoSize() );
        }
      }
    } // end loop over rawbanks
    // Sort things

    for ( auto& c : clus ) {
      std::sort( c.begin(), c.end(), []( const LHCb::FTLiteCluster& lhs, const LHCb::FTLiteCluster& rhs ) {
        return lhs.channelID() < rhs.channelID();
      } );
    }

    for ( const auto& [iQuarter, clusters] : LHCb::range::enumerate( clus ) )
      for ( auto liteClus : clusters ) sortedClus.addHit( std::forward_as_tuple( liteClus ), iQuarter );

    sortedClus.setOffsets();
    return sortedClus;
  }
}

//=============================================================================
// Main execution
//=============================================================================
FTLiteClusters FTRawBankDecoder::operator()( const EventContext& evtCtx, const LHCb::RawBank::View& banks,
                                             const FTReadoutMap& readoutMap ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Number of raw banks " << banks.size() << endmsg;
  if ( banks.empty() ) return {};

  // Testing the bank version
  unsigned int vrsn    = banks[0]->version();
  auto const   version = ( vrsn == 5 && m_decodeV5AsV4.value() ) ? 4 : vrsn;
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Bank version=v" << vrsn << " with decoding version=v" << version << endmsg;

  // Check if decoding version corresponds with readout version.
  readoutMap.compatible( version );

  // Estimate total number of clusters from bank sizes
  auto clus = [&]( unsigned int nClusters ) {
    switch ( version ) {
    case 0:
      return decode<0>( evtCtx, banks, readoutMap, nClusters );
    case 2:
      return decode<2>( evtCtx, banks, readoutMap, nClusters );
    case 3:
      return decode<3>( evtCtx, banks, readoutMap, nClusters );
    case 4:
      return decode<4>( evtCtx, banks, readoutMap, nClusters );
    case 5:
      return decode<5>( evtCtx, banks, readoutMap, nClusters );
    case 6:
      return decode<6>( evtCtx, banks, readoutMap, nClusters );
    case 7:
      return decode<7>( evtCtx, banks, readoutMap, nClusters );
    case 8:
      return decode<8>( evtCtx, banks, readoutMap, nClusters );
    default:
      throw GaudiException( "Unknown bank version: " + std::to_string( version ), __FILE__, StatusCode::FAILURE );
    };
  }( LHCb::FTDAQ::nbFTClusters( banks ) );

  if ( msgLevel( MSG::VERBOSE ) ) {
    for ( const auto& c : clus.range() )
      verbose() << format( " channel %4u frac %3f size %3u ", c.channelID(), c.fraction(), c.pseudoSize() ) << endmsg;
  }

  assert( std::is_sorted( clus.range().begin(), clus.range().end(),
                          []( const LHCb::FTLiteCluster& lhs, const LHCb::FTLiteCluster& rhs ) {
                            return lhs.channelID() < rhs.channelID();
                          } ) &&
          "Clusters from the RawBanks not sorted. Should be sorted by construction." );
  LHCb::FTDAQ::orderInX( clus );
  return clus;
}
