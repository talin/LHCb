/***************************************************************************** \
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <GaudiAlg/GaudiAlgorithm.h>

#include "Detector/FT/FTConstants.h"

#include "FTDAQ/FTDAQHelper.h"
#include "FTDAQ/FTReadoutMap.h"

#include "Event/FTDigit.h"
#include "Event/FTLiteCluster.h"
#include "Event/RawBank.h"
#include "LHCbAlgs/Transformer.h"

#include <cstddef> //Necessary for bit to int conversion

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;
using FTDigits       = LHCb::FTDigit::FTDigits;
/** @class FTNZSRawBankDecoder FTNZSRawBankDecoder.h
 *  Decode the FTNZS raw bank into FTDigits and FTLiteClusters
 *
 *  @author Lex Greeven, Sevda Esen
 *  @date   2020-02-10
 */
class FTNZSRawBankDecoder
    : public LHCb::Algorithm::MultiTransformer<std::tuple<FTDigits, FTLiteClusters>( const EventContext& evtCtx,
                                                                                     const LHCb::RawBank::View&,
                                                                                     const FTReadoutMap& ),
                                               LHCb::DetDesc::usesBaseAndConditions<GaudiAlgorithm, FTReadoutMap>> {
public:
  /// Standard constructor
  FTNZSRawBankDecoder( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  std::tuple<FTDigits, FTLiteClusters> operator()( const EventContext& evtCtx, const LHCb::RawBank::View& rawBanks,
                                                   const FTReadoutMap& readoutMap ) const override;

private:
  // Location of the readout map in the conditions database
  template <unsigned int version>
  std::tuple<FTDigits, FTLiteClusters> decode( const EventContext& evtCtx, LHCb::RawBank::View, const FTReadoutMap&,
                                               unsigned int nDigits, unsigned int nClusters ) const;

  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_corrupt{this, "Possibly corrupt data. Ignoring the cluster."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_nonExistingModule{
      this, "Skipping digit(s) for non-existing module."};

  static constexpr int m_ADCbits         = 2;             // Each channel sends 2 bits to deliver the ADC count
  static constexpr int m_channelsPerByte = 8 / m_ADCbits; // 8 because byte
  static constexpr int m_byteshift       = 8 - m_ADCbits; // 6 rightmost bits
  static constexpr int m_numBytesPerFrame =
      32; // See https://edms.cern.ch/ui/file/1904563/5/Scifi_TELL40_Data_Processing.pdf
};

//-----------------------------------------------------------------------------
// Implementation file for class : FTNZSRawBankDecoder
//
// 2020-02-10 : Lex Greeven, Sevda Esen
//-----------------------------------------------------------------------------

namespace {
  constexpr unsigned channelInBank( uint16_t c ) { return ( c >> FTRawBank::cellShift ); }

  constexpr int cell( short int c ) { return ( c >> FTRawBank::cellShift ) & FTRawBank::cellMaximum; }

  constexpr int fraction( short int c ) { return ( c >> FTRawBank::fractionShift ) & FTRawBank::fractionMaximum; }

  constexpr bool cSize( short int c ) { return ( c >> FTRawBank::sizeShift ) & FTRawBank::sizeMaximum; }

} // namespace

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTNZSRawBankDecoder )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FTNZSRawBankDecoder::FTNZSRawBankDecoder( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer{
          name,
          pSvcLocator,
          {KeyValue{"RawBank", {}}, KeyValue{"ReadoutMapStorage", "AlgorithmSpecific-" + name + "-ReadoutMap"}},
          {KeyValue{"OutputLocation", LHCb::FTDigitLocation::Default},
           KeyValue{"NZSClusterLocation", LHCb::FTLiteClusterLocation::Default + "NZS"}}} {}

StatusCode FTNZSRawBankDecoder::initialize() {
  return MultiTransformer::initialize().andThen(
      [&] { FTReadoutMap::addConditionDerivation( this, inputLocation<FTReadoutMap>() ); } );
}

template <>
std::tuple<FTDigits, FTLiteClusters>
FTNZSRawBankDecoder::decode<0>( const EventContext& evtCtx, LHCb::RawBank::View banks, const FTReadoutMap& readoutMap,
                                unsigned int nDigits, unsigned int nClusters ) const {
  // Define bank and channel number
  int channelnumber = 0;
  int bankNumber    = -1;

  // Define the containers
  FTDigits       digits{nDigits, LHCb::getMemResource( evtCtx )};
  FTLiteClusters clus{nClusters, LHCb::getMemResource( evtCtx )};
  for ( const LHCb::RawBank* bank : banks ) { // Iterates over the banks
    auto sourceID = bank->sourceID();
    // Arrays to temporarily store clusters and digits
    std::array<boost::container::small_vector<LHCb::FTLiteCluster, FTRawBank::nbClusMaximum>,
               FTRawBank::BankProperties::NbLinksPerBank>
        clustersInBankPerLinkID;
    std::array<boost::container::small_vector<LHCb::FTDigit, FTRawBank::nbDigitMaximum>,
               FTRawBank::BankProperties::NbLinksPerBank>
        digitsInBankPerLinkID;

    // Set localLinkID and increment bankNumber to be used for ReadoutMap
    int localLinkID( -1 );
    bankNumber = readoutMap.iSource( sourceID );
    if ( static_cast<unsigned int>( bankNumber ) == readoutMap.nBanks() ) {
      throw GaudiException( "Wrong bank ID", __PRETTY_FUNCTION__, StatusCode::FAILURE );
    }
    auto range = bank->range<std::byte>();
    // Define Lambda functions to be used in loop
    auto make_digits = [&]( std::byte c, LHCb::Detector::FTChannelID globalSiPMID ) {
      // 1 byte contains 4 channels, so loop 4 times
      for ( int i = 0; i < 4; i++ ) {
        auto bits = ( ( c & std::byte{3 << m_byteshift} ) >> m_byteshift ); // Select last 2 bits and shift 6 places
        auto adc_count = std::to_integer<int>( bits );
        auto chan      = --channelnumber;
        if ( chan < 0 ) error() << "Channel has gone below 0!" << endmsg;
        auto channel = LHCb::Detector::FTChannelID{( globalSiPMID + chan )};

        // Print to see what happened
        if ( msgLevel( MSG::DEBUG ) ) {
          // First bit is second bit of adc count, so shift 6 places
          auto bit1 = std::to_integer<int>( ( c & std::byte{128} ) >> 6 );
          // Second bit is first bit of adc count, so also shift 6 places
          auto bit2 = std::to_integer<int>( ( c & std::byte{64} ) >> 6 );
          if ( adc_count != 0 )
            debug() << "ChannelID: " << channel << " adc count: " << adc_count << " bit1: " << bit1 << " bit2: " << bit2
                    << endmsg;
        }

        // Add digit
        digitsInBankPerLinkID[localLinkID].emplace_back( LHCb::FTDigit( channel, adc_count ) );
        // digits.addHit( std::forward_as_tuple( channel, adc_count ), quarter );

        // Shift left by 2
        c <<= 2;
      }
    }; // end of make_digits

    auto make_cluster = [&]( LHCb::Detector::FTChannelID chan, int fraction, int size ) {
      clustersInBankPerLinkID[localLinkID].emplace_back( LHCb::Detector::FTChannelID( chan ), fraction, size );
      // clus.addHit( std::forward_as_tuple( chan, fraction, size ), quarter );
    };

    // Loop over the bytes until bank is empty
    while ( range.size() != 0 ) {
      localLinkID++;
      // Assert if localLinkID is valid
      assert( ( localLinkID < FTRawBank::BankProperties::NbLinksPerBank ) &&
              "localLinkID exceeded the max number of links per rawbank." );
      // For now skip
      if ( localLinkID >= FTRawBank::BankProperties::NbLinksPerBank ) {
        warning() << "localLinkID " << localLinkID << " is bigger than FTRawBank::BankProperties::NbLinksPerBank "
                  << FTRawBank::BankProperties::NbLinksPerBank << ", skipping..." << endmsg;
        range = range.subspan( 2 * m_numBytesPerFrame );
        continue;
      }
      channelnumber = LHCb::Detector::FT::nChannels;
      if ( msgLevel( MSG::DEBUG ) ) debug() << "localLinkID " << localLinkID << endmsg;

      auto globalSiPMID = readoutMap.getGlobalSiPMIDFromIndex( bankNumber, localLinkID );
      // Check if link is kInvalid and therefore not used
      if ( globalSiPMID == LHCb::Detector::FTChannelID::kInvalidChannel() ) {
        debug() << "BankNumber " << bankNumber << " at localLinkID " << localLinkID << "has globalSiPMID "
                << globalSiPMID << " which is an invalid channel! Skipping..." << endmsg;
        continue;
      }

      // Create a 256 bit bitset and fill with rawbank
      // Convert from bytes to bitset so we can easily decode 9 bits clusters
      std::bitset<8 * m_numBytesPerFrame> total_bit_set;
      int                                 i_bit = 0;

      for ( auto chunk : range.subspan( 0, m_numBytesPerFrame ) ) {
        i_bit++;
        std::bitset<8 * m_numBytesPerFrame> sub_bit_set( std::to_integer<int>( chunk ) );
        total_bit_set = total_bit_set | sub_bit_set;
        if ( i_bit < m_numBytesPerFrame ) total_bit_set <<= 8;
      }

      // Skip the 22 bit header
      total_bit_set <<= 22; // FIXME: hardcoded

      // Contains max of 16 clusters
      for ( int i = 0; i < 16; i++ ) { // FIXME: hardcoded
        // Select the 9 bits for the cluster
        uint16_t cluster_bits =
            ( uint16_t )( total_bit_set >> ( 256 - LHCb::Detector::FT::clusterLength ) ).to_ulong(); // FIXME: hardcoded
        if ( cluster_bits == 0 ) break; // Check if we read all clusters

        // Get channel, fraction, and size
        auto channel = LHCb::Detector::FTChannelID{globalSiPMID + channelInBank( cluster_bits )};
        if ( msgLevel( MSG::DEBUG ) )
          debug() << "cluster bits " << channel << " channelID " << cell( cluster_bits ) << ", fraction bit "
                  << fraction( cluster_bits ) << " and size bit " << ( cluster_bits >> 8 ) << endmsg;
        make_cluster( channel, fraction( cluster_bits ), cSize( cluster_bits ) );
        total_bit_set <<= LHCb::Detector::FT::clusterLength; // Shift total bits 9 so we can read the next cluster
      }
      // 32 bytes correspond to one 256 bit TELL40 frame
      range = range.subspan( m_numBytesPerFrame ); // Skip header and clusters we read
      // Create FTDigits, looping over TELL40 frames
      for ( auto chunk : range.subspan( 0, m_numBytesPerFrame ) ) { make_digits( chunk, globalSiPMID ); }
      range = range.subspan( m_numBytesPerFrame ); // Skip digits we read

    } // end of loop over single rawbank

    // Sort digits and clusters within bank using the SiPMID->linkID index array from FTReadoutTool
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Now sorting digits and clusters" << endmsg;
    for ( auto index : readoutMap.getSiPMRemappingFromIndex( bankNumber ) ) {
      if ( index == -1 ) break; // stop when padding is reached

      // Now loop over clusters within link (ordered by definition) and add to object
      auto clusterVector = clustersInBankPerLinkID[index];
      auto digitVector   = digitsInBankPerLinkID[index];
      // Clusters
      for ( auto clusterData : clusterVector ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Cluster: " << clusterData.channelID() << endmsg;
        clus.addHit(
            std::forward_as_tuple( clusterData.channelID(), clusterData.fractionBit(), clusterData.pseudoSize() ),
            clusterData.channelID().globalQuarterIdx() );
      }
      // Digits
      for ( auto digitData : digitVector ) {
        digits.addHit( std::forward_as_tuple( digitData.channelID(), digitData.adcCount() ),
                       digitData.channelID().globalQuarterIdx() );
      }
    }
  } // end loop over rawbanks

  return {std::move( digits ), std::move( clus )};
  ;
}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<FTDigits, FTLiteClusters> FTNZSRawBankDecoder::
                                     operator()( const EventContext& evtCtx, const LHCb::RawBank::View& banks, const FTReadoutMap& readoutMap ) const {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Number of raw banks " << banks.size() << endmsg;
  if ( banks.empty() ) return {};

  if ( banks[0]->type() != LHCb::RawBank::FTNZS ) {
    throw GaudiException( "Wrong RawBank type", __PRETTY_FUNCTION__, StatusCode::FAILURE );
  }
  // Testing the bank version
  unsigned int version = banks[0]->version();
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Bank version=v" << version << endmsg;

  // Check if decoding version corresponds with readout version.
  readoutMap.compatible( version );

  // Estimate total number of digits from bank sizes
  auto digitsAndClusters = [&]( unsigned int nDigits, unsigned int nClusters ) {
    switch ( version ) {
    case 8: // For now use same version for 8 and 0, this might change later
    case 7: // For now use same version for 7 and 0, this might change later
    case 6: // For now use same version for 6 and 0, this might change later
    case 0:
      return decode<0>( evtCtx, banks, readoutMap, nDigits, nClusters );
    default:
      throw GaudiException( "Unknown decoder version: " + std::to_string( version ), __FILE__, StatusCode::FAILURE );
    };
  }( LHCb::FTDAQ::nbFTDigits( banks ), LHCb::FTDAQ::nbFTClusters( banks ) );

  return digitsAndClusters;
}
